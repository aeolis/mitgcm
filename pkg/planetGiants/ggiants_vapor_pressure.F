#include "GGIANTS_OPTIONS.h"

      REAL*8 FUNCTION esat(T,LH,RV,lnC,alpha,beta)

      IMPLICIT NONE

C== input ==
      _RL T
      _RL LH,RV,lnC,alpha,beta

C== local variables ==
      _RL LHg, RVg

c     convert J/kg to J/g
      LHg=LH/1. _d 3
      RVg=RV/1. _d 3

      esat=exp(lnC+1.0 _d 0/RVg*(-LHg/T+alpha*log(T)+beta/2.0 _d 0*T))
c     convert bar to Pa
      esat=esat*1.0 _d 5

      RETURN
      END

      REAL*8 FUNCTION findTs(P,Ts,Es,nRecord)

      IMPLICIT NONE

C== input ==
      INTEGER nRecord
      _RL P
      _RL Ts(1:nRecord) 
      _RL Es(1:nRecord)

C== local variables ==
      INTEGER i

c make sure the water partial pressure is within the table range
      IF (P.gt.Es(nRecord)) THEN
         P = Es(nRecord)
c assuming Ts termininates when P > Es(nRecord). This 
c is indeed true if the forumla calculating the saturation 
c vapor pressure stands true in deep atmosphere (Es reaches 
c maximum of 11.5 bars at 60bars) 
         WRITE (0,*) 'pH2O=',P, 'Es=',Es(nRecord)
         WRITE (0,*) 'Vapor pressure out of range'
      ENDIF

c     look up T,P table. T is in increasing order
c     P is also in increasing order
      DO i=2, nRecord
       IF ( (P.gt.Es(i-1)) .and. (P.le.Es(i)) ) 
c         use linear interpolation
     &    findTs=Ts(i-1)+(P-Es(i-1))/(Es(i)-Es(i-1))
     &                              *(Ts(i)-Ts(i-1))
          findTs=max(findTs,Ts(i-1))
          findTs=min(findTs,Ts(i  ))
      ENDDO

      RETURN
      END
