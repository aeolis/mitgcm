C--   common physics variables
C     Universal constants
c     Runiv: universal gas constant
c     AN:    Avogadro's number
c     PI0:   Pi
      _RL Runiv, AN, PI0
      PARAMETER ( Runiv= 8314.0 _d 0    )
      PARAMETER ( AN   = 6.022 _d 23    )
      PARAMETER ( PI0  =3.14159265 _d 0 )

C     Atmospheric compositions
c     mH2O: molecular mass of water vapor
c     mH2:  molecular mass of H2
c     mHe:  molecluar mass of He
c     mCH4: molecular mass of methane
c     mNH3: molecular mass of Amonia
c     Runiv: universal gas constant
 
      _RL mH2O, mH2, mHe, mCH4, mNH3
      PARAMETER ( mH2O = 18.0 _d 0    )
      PARAMETER ( mH2  = 2. _d 0      )
      PARAMETER ( mHe  = 2. _d 0      )
      PARAMETER ( mCH4 = 16. _d 0     )
      PARAMETER ( mNH3 = 17. _d 0     )
      
C     Water vapor section
c     LH_H2O: latent heat of water vapor (to solid)
c     R_H2O:  gas constant for water vapor
c     CP_H2OV: heat capacity of water vapor
c     CP_H2OS: heat capacity of water particle
c     Dcoll_H2O: coagulation coefficient for water?
c     rp_H2O:   radius of water droplets
c               opted for an input parameter
c     rho_H2O:  density of liquid water

      _RL LH_H2O, R_H2O, CP_H2OV
      _RL Dcoll_H2O, rho_H2O, CP_H2OS
      _RL lnC_H2O, alpha_H2O, beta_H2O
      PARAMETER ( LH_H2O    = 3.1482 _d 6 )
      PARAMETER ( R_H2O     = Runiv/mH2O  )
      PARAMETER ( CP_H2OV   = 2.08 _d 3   )
      PARAMETER ( CP_H2OS   = 4.18 _d 3   )
      PARAMETER ( Dcoll_H2O = 3.11 _d -10 )
c      PARAMETER ( rp_H2O    = 10.0 _d -6  )
      PARAMETER ( rho_H2O   = 1000.0 _d 0 )
      PARAMETER ( lnC_H2O   = 25.096      )
      PARAMETER ( alpha_H2O = 0.0 _d 0    )
      PARAMETER ( beta_H2O  = -17.4 _d -3 )

C     ammonia vapor section
c     LH_NH3: latent heat of ammonia vapor (to solid)
c     R_NH3:  gas constant for ammonia vapor
c     CP_NH3V: heat capacity of ammonia vapor
c     CP_NH3S: heat capacity of ammonia particle
c     Dcoll_NH3: coagulation coefficient for ammonia?
c     rp_NH3:   radius of ammonia droplets
c     rho_NH3:  density of liquid ammonia

      _RL LH_NH3, R_NH3, CP_NH3V
      _RL Dcoll_NH3, rp_NH3,  rho_NH3, CP_NH3S
      _RL lnC_NH3, alpha_NH3, beta_NH3
      PARAMETER ( LH_NH3    = 2.016 _d 6  )
      PARAMETER ( R_NH3     = Runiv/mNH3  )
      PARAMETER ( CP_NH3V   = 2.19 _d 3   )
      PARAMETER ( CP_NH3S   = 4.70 _d 3   )
      PARAMETER ( Dcoll_NH3 = 3.11 _d -10 )
      PARAMETER ( rp_NH3    = 1.0 _d -6   )
      PARAMETER ( rho_NH3   = 682.0 _d 0  )
      PARAMETER ( lnC_NH3   = 27.863 _d 0 )
      PARAMETER ( alpha_NH3 = -0.888 _d 0 )
      PARAMETER ( beta_NH3  = 0.0 _d 0    )

c     physics constants
      _RL SVP1,SVP2,SVP3,SVPT0
      _RL KARMAN,EOMEG,STBOLT      

      PARAMETER (SVP1=0.6112     )
      PARAMETER (SVP2=17.67      )
      PARAMETER (SVP3=29.65)
      PARAMETER (SVPT0=273.15    )
      PARAMETER (KARMAN=0.4      )
      PARAMETER (EOMEG=7.08822E-5     )
      PARAMETER (STBOLT = 5.670151E-8 )

c     orbital properties
      _RL obliquity, eccentricity, equinox_fraction
      _RL P2SI
      _RL zero_date, semimajoraxis, DPD, DEGRAD
      INTEGER planet_year

      PARAMETER (DEGRAD = ACOS(-1.d0)/180.d0)
#ifdef GGIANTS_JUPITER
      PARAMETER ( P2SI = 0.4135              )
      PARAMETER ( planet_year  = 10473       )
      PARAMETER ( DPD          = 360./10473. )
      PARAMETER ( obliquity    = 3.13        )
      PARAMETER ( eccentricity = 0.0484      )
      PARAMETER ( equinox_fraction = 0.2389  )
      PARAMETER ( zero_date    = 389.6       )
      PARAMETER ( semimajoraxis= 5.204267    )
#endif

#ifdef GGIANTS_SATURN
#endif

#ifdef GGIANTS_URANUS
#endif

#ifdef GGIANTS_NEPTUNE
#endif
