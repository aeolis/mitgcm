C define gas giant planets configurations

      COMMON /GGIANTS_CONFIG/
     &       radFreq, internalQ,
     &       bottomdrag, shapTR,
     &       du_physics, diurnavg, 
     &       LatentHeat, radtran

c radFreq:   frequency to apply radiative transfer
c radtran:   radiative transfer scheme selector
c            1 for Newtonian heating
c            2 for grey radiative transfer
c            3 for KDM radiative transfer (in progress)
c bottomdrag: flag for bottomdrag at the bottom
c            0 for no drag
c            1 for drag independent of latitude
c            2 for drag with minima at equatorial region 
c shapTR: flag for applying shapiro filter to tracers

      _RL radFreq

      INTEGER radtran
      INTEGER bottomdrag
      
      LOGICAL internalQ
      LOGICAL shapTR
      
      LOGICAL du_physics
      LOGICAL diurnavg

      LOGICAL LatentHeat
