#include "PACKAGES_CONFIG.h"
#include "CPP_OPTIONS.h"

#ifndef GGIANTS_PLANET
#define GGIANTS_PLANET
#endif /* GGIANTS_PLANET */


C--//  define which planet to use
#ifndef GGIANTS_JUPITER
#define GGIANTS_JUPITER
#endif /* GGIANTS_JUPITER */

#ifndef GGIANTS_SATURN
#undef GGIANTS_SATURN
#endif /* GGIANTS_SATURN */

#ifndef GGIANTS_URANUS
#undef GGIANTS_URANUS
#endif /* GGIANTS_URANUS */

#ifndef GGIANTS_NEPTUNE
#undef GGIANTS_NEPTUNE
#endif /* GGIANTS_NEPTUNE */


C--// use solar heating or not
#ifndef GGIANTS_RAD
#define GGIANTS_RAD
#endif /* GGIANTS_RAD */

C--// use microphysics or not
C// active water vapor
c// passive ammonia,methane
#ifndef GGIANTS_MPHYS
#define GGIANTS_MPHYS
#endif /* GGIANTS_MPHYS */

#define USE_SBM_CONV

C--// DO NOT CHANGE THE FOLLOWING

#ifndef ALLOW_PTRACERS
#undef GGIANTS_MPHYS 
#endif

C-// Define active tracer cycle
#ifdef GGIANTS_MPHYS
#define WATER_CYCLE
#undef AMMONIA_CYCLE
#endif




C-// This part can't be changed for now // 
#ifdef USE_SBM_CONV
#undef AMMONIA_CYCLE
#endif
