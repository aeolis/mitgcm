#include "GGIANTS_OPTIONS.h"

      SUBROUTINE calc_z_pressure(ze,zc,dze,dzc,pe,pc,th,
     &           ims,ime,jms,jme,kms,kme,
     &           its,ite,jts,jte,kts,kte,
     &           bi,bj)
    
      IMPLICIT NONE
#include "SIZE.h"
#include "EEPARAMS.h"
#include "PARAMS.h"
#include "SURFACE.h"

      INTEGER bi,bj
      INTEGER ims, ime, jms, jme, kms, kme
      INTEGER its, ite, jts, jte, kts, kte
      INTEGER i,j,L

C=====INput======
      _RL pe  (ims:ime, jms:jme, kms:kme)
      _RL pc  (ims:ime, jms:jme, kms:kme)
      _RL th  (ims:ime, jms:jme, kms:kme)
C=====Output=====
      _RL ze  (ims:ime, jms:jme, kms:kme)
      _RL zc  (ims:ime, jms:jme, kms:kme)
      _RL dze (ims:ime, jms:jme, kms:kme)
      _RL dzc (ims:ime, jms:jme, kms:kme)

C.....====Input====......
C.....pe: pressure at the faces of the vertical grid
C.....pc: pressure at the centers of the vertical grid
C.....th: potential temperature at the center of the vertical grid 
C.....====Output===.....
C.....ze: height at the faces of the vertical grid
C.....zc: height at the centers of the vertical grid
C.....dze: layer thickness between faces of the vertical grid
C.....dzc: layer thickness between centers of the vertical grid

C--   calculate dze
      DO i = its, ite
      DO j = jts, jte
      DO L = kts, kte
           dze(i,j,L) = -atm_cp/gravity*
     &         (pe(i,j,L+1)**atm_kappa -
     &          pe(i,j,L  )**atm_kappa)
     &         /atm_po**atm_kappa * th(i,j,L)
      ENDDO
      ENDDO
      ENDDO
C--   calculate ze
      DO i = its, ite
      DO j = jts, jte
      DO L = kts, kte+1
         IF (L .EQ. kts) THEN
            ze(i,j,L) = topoZ(i,j,bi,bj)
         ELSE
            ze(i,j,L) = ze(i,j,L-1) + dze(i,j,L-1)
         ENDIF
      ENDDO
      ENDDO
      ENDDO

C--   calculate zc
      DO i = its, ite
      DO j = jts, jte
         DO L = kts, kte
         IF (L .GE. 2) THEN
            zc(i,j,L) = zc(i,j,L-1)-atm_cp/gravity*
     &      (pe(i,j,L)**atm_kappa - pc(i,j,L-1)**atm_kappa)
     &      /atm_po**atm_kappa * th(i,j,L-1) - atm_cp/gravity*
     &      (pc(i,j,L)**atm_kappa - pe(i,j,L)**atm_kappa)
     &      /atm_po**atm_kappa * th(i,j,L)
         ELSE
            zc(i,j,L) = topoZ(i,j,bi,bj)-atm_cp/gravity*
     &      (pc(i,j,L)**atm_kappa - pe(i,j,L)**atm_kappa)
     &      /atm_po**atm_kappa * th(i,j,L)
         ENDIF
         ENDDO
         zc(i,j,kme) = zc(i,j,kme-1)-atm_cp/gravity*
     &   (pe(i,j,kme)**atm_kappa - pc(i,j,kme-1)**atm_kappa)
     &   /atm_po**atm_kappa * th(i,j,kme-1)
      ENDDO
      ENDDO 
C--   calculate dzc
      DO i = its, ite
      DO j = jts, jte
      DO L = kts, kte
         dzc(i,j,L) = zc(i,j,L+1) - zc(i,j,L)
      ENDDO
      ENDDO
      ENDDO
 
      RETURN
      END 
        

