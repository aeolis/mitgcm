      INTEGER ims,ime, jms,jme, kms,kme,
     &        its,ite, jts,jte, kts,kte,
     &        Nbot, Nrphys, n_species
C--   index for domain size of tiles and number of tiles
      PARAMETER ( ims = 1-OLx   )
      PARAMETER ( ime = sNx+OLx )
      PARAMETER ( jms = 1-OLy   )
      PARAMETER ( jme = sNy+OLy )
      PARAMETER ( its = 1       )
      PARAMETER ( ite = sNx     )
      PARAMETER ( jts = 1       )
      PARAMETER ( jte = sNy     )

c--   domain size and p field to apply radtran
      PARAMETER ( Nbot = 1      )
      PARAMETER ( Nrphys  = Nr-Nbot+1) 
      PARAMETER ( kms  = 1       )
      PARAMETER ( kme  = Nrphys+1)
      PARAMETER ( kts  = 1       )
      PARAMETER ( kte  = Nrphys  )

C--   Number of tracer species (H2O and NH3)
#ifdef AMMONIA_CYCLE
      PARAMETER ( n_species  = 2 )
#else
      PARAMETER ( n_species  = 1 )
#endif /* ALLOW_PTRACERS */

C-- radiative heating      
C RTHRATEN:   total radiative heating in the atmosphere
C hr_ir:      IR radiative heating rate (K/s)
C hr_vis:     Solar radiative heating rate (K/s)
C hr_a_ir:    Atmospheric IR radiative heating rate (K/s)
C hr_a_vis:   Atmospheric Solar radiative heating rate (K/s)
C GSW:        Downward solar radiative heat flux near water cloud top (W/m^2)
C GLW:        Downward IR radiatvive heat flux near water cloud top (W/m^2)
C Albedo:     Cloud top albedo
C EMISS:      Water cloud emissivity
C DUST_ARRAY: Aerosol mass mixing ratio (kg/kg)
C TSK:        water cloud temperature (K)      

      COMMON /rad_trans_phy/
     &                RTHRATEN, GSW, GLW, hr_ir,
     &                hr_a_ir,  hr_vis,   hr_a_vis,
     &                ALBEDO,   DUST_ARRAY,  EMISS, 
     &                TSK

      _RL RTHRATEN  (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL hr_ir     (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL hr_a_ir   (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL hr_vis    (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL hr_a_vis  (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL GSW       (ims:ime,jms:jme, nSx,nSy )
      _RL GLW       (ims:ime,jms:jme, nSx,nSy )
      _RL ALBEDO    (ims:ime,jms:jme, nSx,nSy )
      _RL EMISS     (ims:ime,jms:jme, nSx,nSy )
      _RL DUST_ARRAY(ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL TSK   (ims:ime,jms:jme, nSx,nSy )

C GTHRATEN: total radiative heating in dynamics grid (K/s)

      COMMON /rad_trans_dyn/
     &                GTHRATEN

       _RL GTHRATEN  (ims:ime,jms:jme,1:Nr,nSx,nSy )

c-- pressure and height in radiation (physics) grid
C pephy:  pressure at interfaces of the radiation grid (Pa)
C pcphy:  pressure at centers of the radiation grid (Pa)
C dz8wphy: layer thickness between interfaces of the radiation grid (m) 
C dzcphy:  layer thickness between centers of the radiation grid (m)
C zephy:   Altitude of the interfaces of the radiation grid (m)
C zcphy:   Altitude of the centers of the radiation grid (m)

      COMMON /vphy_cord_phy/
     &        pephy,        pcphy,
     &        dz8wphy, dzcphy, zephy, zcphy
      
      _RL pephy   (ims:ime,jms:jme,kms:kme,  nSx,nSy)
      _RL pcphy   (ims:ime,jms:jme,kms:kme,  nSx,nSy)
      _RL dz8wphy (ims:ime,jms:jme,kms:kme,  nSx,nSy)
      _RL dzcphy  (ims:ime,jms:jme,kms:kme,  nSx,nSy)
      _RL zephy   (ims:ime,jms:jme,kms:kme,  nSx,nSy)
      _RL zcphy   (ims:ime,jms:jme,kms:kme,  nSx,nSy)

C-- solar radiation
C HA:      Hour angle of sun light
C sza0:    mean cosine zenith angle(do not divide by r**2)
C sunfrac: fraction of daylight coverage

      COMMON /solar_rad/
     &       HA,    
     &       sza0,   sunfrac

      _RL HA      (ims:ime,jms:jme, nSx,nSy )
      _RL sza0    (ims:ime,jms:jme, nSx,nSy )
      _RL sunfrac (ims:ime,jms:jme, nSx,nSy )

C-- Bottom drag coefficient in analogous to the Ekman Layer drag
      COMMON /Ekman_drag/
     &        kdrag
       
      _RL kdrag (ims:ime,jms:jme,1:Nr,  nSx,nSy)

C-- pressure field in vertical coordinate
C pedyn:  pressure at interfaces of the dynamics grid (Pa)
C pcdyn:  pressure at centers of the dynamics grid (Pa)
C drFdyn: layer thickness between interfaces of the dynamics grid (Pa)
C drCdyn: layer thickness between centers of the dynamics grid (Pa)
C recip_drFdyn: 1/drFdyn
C recip_drCdyn: 1/drCdyn

      COMMON /pressure_rcord/
     &        pedyn,        pcdyn,
     &        drCdyn,       drFdyn,
     &        recip_drCdyn, recip_drFdyn

      _RL pedyn       (ims:ime,jms:jme,1:Nr+1,nSx,nSy)
      _RL pcdyn       (ims:ime,jms:jme,1:Nr,  nSx,nSy)
      _RL drCdyn      (ims:ime,jms:jme,1:Nr,  nSx,nSy)
      _RL drFdyn      (ims:ime,jms:jme,1:Nr,  nSx,nSy)
      _RL recip_drCdyn(ims:ime,jms:jme,1:Nr,  nSx,nSy)
      _RL recip_drFdyn(ims:ime,jms:jme,1:Nr,  nSx,nSy)

c z-height in vertical dynamics coordinate
C ze:   altitude at the interfaces of the dynamics grid (m)
C zc:   altitude at the centers of the dynamics grid (m)
C dz8w: layer thickness between the interfaces of the dynamics grid (m)
C dzc:  layer thickness between the centers of the dynamics grid (m)

      COMMON /pressure_zcord/
     &        dz8w, dzc, ze, zc

      _RL dz8w(ims:ime,jms:jme,1:Nr+1,nSx,nSy)
      _RL dzc (ims:ime,jms:jme,1:Nr+1,nSx,nSy)
      _RL ze  (ims:ime,jms:jme,1:Nr+1,nSx,nSy)
      _RL zc  (ims:ime,jms:jme,1:Nr+1,nSx,nSy)

c gq4T: source and sink of tracers (tendency only, it is different 
c from gPtr as we only want the part gets involved with latent heating, 
c that is, the part required for calculating heating rate and 
c tendencies due to advection is excluded)

c elocal is the locally total evaporated tracer (kg/kg)
c clocal is the locally total condensed tracer  (kg/kg)
c slocal is the local lost of tracer through settling (kg/kg)

      COMMON /GGIANTS_DYNS/
     &        elocal, clocal, slocal

      _RL elocal(ims:ime,jms:jme,Nr,nSx,nSy,1:n_species)
      _RL clocal(ims:ime,jms:jme,Nr,nSx,nSy,1:n_species)
      _RL slocal(ims:ime,jms:jme,Nr,nSx,nSy,1:n_species)

C H2O_LHR: Latent heating rate of water (K/s)

      COMMON /GGIANTS_VAP_HR/
     &        H2O_LHR, NH3_LHR
      _RL H2O_LHR(ims:ime,jms:jme,Nr,nSx,nSy) 
      _RL NH3_LHR(ims:ime,jms:jme,Nr,nSx,nSy)

C H2O_ESAT: saturation vapor pressure at temperature T_EVAP
C T_EVAP has a resolution of 0.5 K, range from 50K to 550K
      COMMON /ESAT_TEMP/
     &        T_EVAP, H2O_ESAT, NH3_ESAT

      INTEGER nTrecord
      PARAMETER (nTrecord = 1000)

      _RL T_EVAP  (1:nTrecord)
      _RL H2O_ESAT(1:nTrecord)
      _RL NH3_ESAT(1:nTrecord)
