!WRF:MEDIATION_LAYER:PHYSICS
!
!-----------------------------------------------------------------------
! This is essentially the Hadley/Unified Model radiation code modified !
!    to use a correlated-k scheme.  The subroutine hadley is in many   !
!    ways a "harness" which sets up the variables needed in the        !
!    radiation code.  Subroutine flux_calc is the nominal start of the !
!    Hadley scheme.  Note that it is called twice per implementation,  !
!    once for the solar, and once for the IR.                          !
! There will be a loop over 'j', or latitude, doing latitude strips    !
!    one at a time.                                                    !
! In this code, n_layer is at the surface, and 0 is above the top      !
!    layer.                                                            !
!-----------------------------------------------------------------------

! NB - HARDWIRED FOR 46 IR AND 24 VIS INTERVALS

C--    options for radtran schemes?
       INTEGER EDDINGTON
       INTEGER DISCRETE_ORD 
       INTEGER ELSASSER
       INTEGER HEMI_MEAN

       PARAMETER ( EDDINGTON   = 2 )
       PARAMETER ( DISCRETE_ORD= 3 )
       PARAMETER ( ELSASSER    = 12)
       PARAMETER (HEMI_MEAN    = 13)


! This version uses the full Mckay calculations together with the Hadley solver 

       INTEGER ip_scf_solar_up, ip_scf_solar_down    !Index of source coefficient for upward and downward solar beam
       INTEGER ip_scf_ir_1d, ip_scf_ir_2d            !Index of source coefficient for 1st and 2nd difference of Planckian
       INTEGER npd_source_coeff                      !Number of coefficients for two-stream sources
       INTEGER ip_solar, ip_infra_red                !Index for solar and IR loop

       PARAMETER (ip_scf_solar_up=1, ip_scf_solar_down=2)
       PARAMETER (ip_scf_ir_1d=1, ip_scf_ir_2d=2)
       PARAMETER (npd_source_coeff=2)
       PARAMETER (ip_solar=1, ip_infra_red=2)

       REAL*8 tol_machine, tol_div, sqrt_tol_machine !Machine tolerance limits
       REAL*8 log_tol_machine, quad_correct_limit    !Machine tolerance limits

       PARAMETER (tol_machine=2.220446D-16)
       PARAMETER (tol_div=32.d0*tol_machine)
       PARAMETER (sqrt_tol_machine=1.490116d-8)
       PARAMETER (log_tol_machine=-36.0437)
       PARAMETER (quad_correct_limit=6.828d-6)

       INTEGER n_band_ir, n_band_solar            !Number of bands in IR and solar

       PARAMETER (n_band_ir=46, n_band_solar=24)


       REAL*8 solarf (n_band_solar)
       INTEGER nterm (n_band_solar)  
       REAL*8 aterm (4,n_band_solar)
       REAL*8 bterm (4,n_band_solar)
       REAL*8 pexpon (n_band_solar)
       REAL*8 band_wn_ir (n_band_ir+1)
       REAL*8 wnoi (n_band_ir)
       REAL*8 wlni (n_band_ir)
       REAL*8 wnov (n_band_solar)
       REAL*8 wlnv (n_band_solar)

       REAL*8 suntitanmean, fuv, gcp, fh2, fc2h2, fc2h6

       PARAMETER (suntitanmean=9.5388) ! MEAN SUN-TITAN DISTANCE IN AU
       PARAMETER (gcp=0.0018)   ! 22.88/13000.
       PARAMETER (fh2=0.90) ! H2 MIXING RATION FROM COURTIN ET AL.
       PARAMETER (fc2h2=1.8E-6) ! NEW FROM ATHENA OLD= 2.E-6
       PARAMETER (fc2h6=1.2E-5) ! NEW FROM ATHENA OLD= 2.E-5

       INTEGER ntemps
       PARAMETER (ntemps=14)

!CEN - all need to be saved within subroutine where calculated now??
!OR store in .h file??
       COMMON /Titan_ra_vars/
     & piann, piacc, piacn, piahn, piana,
     & piaha, piahc, piaca, reali, ximgi, 
     & realv, ximgv, tmin,  tmax 

       REAL*8 piann (n_band_ir,ntemps)
       REAL*8 piacc (n_band_ir,ntemps)
       REAL*8 piacn (n_band_ir,ntemps)
       REAL*8 piahn (n_band_ir,ntemps)
       REAL*8 piana (n_band_ir,ntemps)
       REAL*8 piaha (n_band_ir,ntemps)
       REAL*8 piahc (n_band_ir,ntemps)
       REAL*8 piaca (n_band_ir,ntemps)

       REAL*8 reali (n_band_ir) 
       REAL*8 ximgi (n_band_ir) 
       REAL*8 realv (n_band_solar) 
       REAL*8 ximgv (n_band_solar) 

       REAL*8 tmin, tmax



       LOGICAL firstcall
       DATA firstcall /.true./

       REAL*8 fhir, fhvis, fhaze, rgas, pi, rhop

       DATA fhir /0.5/                  ! HAZE INFRARED ABSORPTION SCALE FACTOR (X KHARE ETAL 1988)
       DATA fhvis /1.2/                 ! VALUE FOR BEST FIT MODEL
       DATA pi /3.14159265358979323846/
       DATA rgas /8.31432/              ! UNIVERSAL GAS CONSTANT: M SEC-2 AMU K-1 KM
CEN - CHECK!!!
!       DATA fhaze /1.23E-13/           ! KG/M2/SEC;  HAZE PRODUCTION RATE FROM MCKAY ETAL 1989
!       DATA fhaze /1.E-13/             ! KG/M2/SEC; BEST FIT FOR TEMPERATURE (=1.E-14 IN CGS)
       DATA fhaze /1.05E-14/            ! 0.3*3.5e-14
       DATA rhop /1.E4/

       DATA tmax /190./
       DATA tmin /60./


!NOW WITH FUV MULTIPLYING THE FIRST TWO, AND SET TO SOLARF RIGHT AWAY, TO SAVE DOING THIS IN INIT:
! in MKS, W/M2
       DATA solarf /1.313757,  1.701100, 1.051900, 1.173300, 1.082000, 
     &              1.056500,  1.495100, 1.123100, 1.053200, 0.541820,  
     &              0.397630,  0.484210, 0.624540, 0.528390, 0.383250,  
     &              0.372260,  0.360090, 0.339230, 0.315600, 0.292580,  
     &              0.267600,  0.239700, 0.208040, 0.180550/

       DATA  band_wn_ir / 8.00000, 15.0000, 25.0000, 37.5000, 62.5000, 
     &           87.5000, 112.500, 137.500, 162.500, 187.500, 212.500, 
     &           237.500, 262.500, 287.500, 312.500, 337.500, 362.500, 
     &           387.500, 412.500, 437.500, 462.500, 487.500, 512.500, 
     &           537.500, 562.500, 587.500, 612.500, 645.000, 680.676, 
     &           682.838, 685.000, 688.082, 691.163, 694.245, 697.327, 
     &           732.956, 734.717, 736.478, 738.239, 740.000, 745.440, 
     &           750.880, 756.320, 912.800, 921.867, 930.933, 940.000 /   ! Wavelength boundaries in IR

       DATA  wnoi / 11.50000, 20.00000, 31.25000, 50.00000, 
     &       75.00000, 100.0000, 125.0000, 150.0000, 
     &       175.0000, 200.0000, 225.0000, 250.0000, 
     &       275.0000, 300.0000, 325.0000, 350.0000, 
     &       375.0000, 400.0000, 425.0000, 450.0000, 
     &       475.0000, 500.0000, 525.0000, 550.0000, 
     &       575.0000, 600.0000, 628.7500, 662.8380, 
     &       681.7570, 683.9190, 686.5410, 689.6225, 
     &       692.7040, 695.7860, 715.1415, 733.8365, 
     &       735.5975, 737.3585, 739.1195, 742.7200, 
     &       748.1600, 753.6000, 834.5600, 917.3335, 
     &       926.4000, 935.4665 /

       DATA  wlni / 869.5652, 500.0000, 320.0000, 200.0000, 
     &       133.3333, 100.0000, 80.00000, 66.66666, 
     &       57.14286, 50.00000, 44.44444, 40.00000, 
     &       36.36364, 33.33333, 30.76923, 28.57143, 
     &       26.66667, 25.00000, 23.52941, 22.22222, 
     &       21.05263, 20.00000, 19.04762, 18.18182, 
     &       17.39130, 16.66667, 15.90457, 15.08664, 
     &       14.66798, 14.62161, 14.56577, 14.50069, 
     &       14.43618, 14.37224, 13.98325, 13.62701, 
     &       13.59439, 13.56192, 13.52961, 13.46402, 
     &       13.36612, 13.26964, 11.98236, 10.90116, 
     &       10.79447, 10.68985 /

       DATA wnov / 30769.23, 26666.67, 23529.41, 21052.63, 
     &   19047.62, 17391.30, 15625.00, 13986.01, 
     &   12674.27, 11764.71, 11223.34, 10695.19, 
     &   10020.04, 9319.665, 8741.259, 8244.022, 
     &   7739.938, 7241.129, 6738.544, 6238.303, 
     &   5740.528, 5238.345, 4737.091, 4235.493/

       DATA wlnv / 0.3250000, 0.3750000, 0.4250000, 0.4750000, 
     &  0.5250000, 0.5750000, 0.6400000, 0.7150000, 
     &  0.7890000, 0.8500000, 0.8910000, 0.9350000, 
     &  0.9980000, 1.073000, 1.144000, 1.213000, 
     &   1.292000, 1.381000, 1.484000, 1.603000, 
     &   1.742000, 1.909000, 2.111000, 2.361000/

       DATA PEXPON/6*0.0, 
     &  0.000,    0.000,    0.000,    0.149,    0.156,    0.186, 
     &  0.302,    0.097,    1.150,    1.040,    1.030,    1.040,
     &  1.080,    1.070,    1.090,    1.050,    1.050,    0.959/

       DATA NTERM/ 
     &     1.000000, 1.000000, 4.000000, 4.000000, 
     &     4.000000, 3.000000, 4.000000, 4.000000, 
     &     3.000000, 4.000000, 3.000000, 3.000000, 
     &     3.000000, 4.000000, 4.000000, 3.000000, 
     &     3.000000, 4.000000, 3.000000, 4.000000, 
     &     4.000000, 3.000000, 4.000000, 4.000000/

       DATA ATERM/ 
     &     1.000000, 0.000000, 0.000000, 0.000000, 1.000000, 0.000000, 
     &     0.000000, 0.000000, 0.700000, 0.093900, 0.015000, 0.191100, 
     &     0.300030, 0.135014, 0.460546, 0.104410, 0.164416, 0.375038, 
     &     0.295630, 0.164917, 0.444755, 0.355864, 0.199380, 0.000000, 
     &     0.198120, 0.331633, 0.335834, 0.134413, 0.327300, 0.335500, 
     &     0.146800, 0.190400, 0.277055, 0.333367, 0.389578, 0.000000, 
     &     0.126666, 0.416016, 0.364590, 0.092728, 0.286216, 0.492476, 
     &     0.221308, 0.000000, 0.445402, 0.453717, 0.100882, 0.000000, 
     &     0.351017, 0.371854, 0.277129, 0.000000, 0.434400, 0.477200, 
     &     0.077600, 0.010800, 0.292343, 0.374023, 0.242032, 0.091602, 
     &     0.501604, 0.385625, 0.112771, 0.000000, 0.597075, 0.308155, 
     &     0.094771, 0.000000, 0.116916, 0.447207, 0.338013, 0.097864, 
     &     0.541475, 0.367862, 0.090663, 0.000000, 0.468164, 0.212875, 
     &     0.213276, 0.105685, 0.245440, 0.416617, 0.242734, 0.095209, 
     &     0.330423, 0.503512, 0.166065, 0.000000, 0.307538, 0.361097, 
     &     0.232456, 0.098909, 0.115609, 0.353355, 0.413319, 0.117718/

       DATA BTERM/ 
     &   0.000000,        0.000000,        0.000000,        0.000000, 
     &   0.000000,        0.000000,        0.000000,        0.000000, 
     &   0.000000,       1.0956400E-05,  3.2493997E-05,  7.7167999E-05, 
     &   0.000000,       1.3214600E-05,  6.4050000E-05,  5.9766002E-04, 
     &  2.7300000E-06,   3.5475998E-05,  2.1027999E-04,  1.2537000E-03, 
     &  3.5601999E-05,   2.1587999E-04,  4.4660000E-04,   0.000000, 
     &  1.0043600E-04,   3.6413997E-04,  1.3602399E-03,  6.1641997E-03, 
     &  6.2369998E-04,   2.6250000E-03,  1.1059999E-02,  4.0123999E-02, 
     &  7.9687999E-04,   2.7215998E-03,  1.1148199E-02,   0.000000, 
     &  4.3665999E-04,   6.9439998E-03,  3.2157999E-02,  0.4012400, 
     &  1.7836001E-02,   0.1995000,        1.969800,        0.000000, 
     &  1.3183799E-03,   8.6589996E-03,   9.6865997E-02,    0.000000, 
     &  2.6165998E-02,   0.1758400,        2.062200,        0.000000, 
     &  1.9040001E-03,   1.3136200E-02,   5.8897994E-02,    1.971200, 
     &  8.5819996E-04,   1.8508000E-02,   0.3179400,        11.98960, 
     &   0.000000,       8.4881997E-03,   0.5583200,        0.000000, 
     &   0.000000,       8.5189994E-03,   0.6319600,        0.000000, 
     &  3.8065999E-03,   2.7187999E-02,   0.4478600,        21.51800, 
     &   0.000000,       8.1815999E-03,   0.7091000,        0.000000, 
     &  8.8087996E-05,   1.0969000E-02,   0.4988200,        18.01800, 
     &  2.4751998E-02,   0.4319000,        6.510000,        195.1600, 
     &   0.000000,       9.3758004E-03,   0.6444200,        0.000000, 
     &   0.000000,       1.0284400E-02,   0.5555200,        44.68800, 
     &  0.1343020,        5.285000,        71.58200,        3911.600/

