C $Name:  $

C This subroutine performs moist convection adjustment 
C following Frierson (2007): The Dynamics o Idealized Convection Schemes 
C and Their Effect on the Zonally Averaged Tropical Circulation. This is 
C a simplified version of Betts-Miller scheme (SBM).

C Four processes are included: Cloud convection, evaporation (in cloud and 
C subcloud layers), sedimentation in entire cloumn and dry convective adjustment 
C of vapor in the subcloud layers.

C#include "PTRACERS_OPTIONS.h"
#include "GGIANTS_OPTIONS.h"
CBOP
C !ROUTINE: GGIANTS_SBM_ADJ

C !INTERFACE: ==========================================================
      SUBROUTINE GGIANTS_SBM_ADJ(
c                dynamics variables
     &           theta, pTracer, pcdyn, pedyn,
     &           ze, zc, dz8w, dzc, drFdyn, hFacC,
     &           n_phase, ksize, deltaT, 
c                tracer phase variables
     &           clocal, elocal, slocal, DTTEN,
c                physics parameters
     &           atm_po, atm_kappa, atm_Rd, atm_cp,
     &           gravity, mH2, Runiv, AN, 
     &           p_LNB, tau_SBM, 
c                tracer parameters
     &           mTR, LH_TR, R_TR, CP_TRV, CP_TRS,
     &           Dcoll_TR, rp_TR, rho_TR, lnC_TR, 
     &           alpha_TR, beta_TR,
     &           its, ite, jts, jte, kts, kte,
     &           ims, ime, jms, jme, kms, kme,
     &           myIter,myTime,myThid,bi,bj)

C !DESCRIPTION:
C     Adds sources and sinks of passive tracers to the tendency arrays

C !USES: ===============================================================
      IMPLICIT NONE
#include "EEPARAMS.h"

C !INPUT PARAMETERS: ===================================================
C     myIter               :: time-step number
C     myTime               :: model time
C     myThid               :: thread number
      INTEGER myIter, myThid, bi,bj

      _RL myTime

      INTEGER its, ite, jts, jte, kts, kte,
     &        ims, ime, jms, jme, kms, kme
C     n_phase   :: number of phase for each tracer 
C     n_species :: number of tracer species
C     ksize     :: number of dynamics vertical levels (Nr)

C     Needs clean up by defining the tracer properties in 
C     array as well (n_species)

      INTEGER n_phase, ksize

C     Runiv is the universal gas constant
C     Dcoll_TR is the collision diameter of tracer
C     AN is the Avogadro's number
C     p_LNB is the pressure level of neutral buoyancy
C     tau_SBM is the relaxation time for adjustment
C     deltaT is the model time step

      _RL atm_po, atm_kappa, atm_Rd, atm_cp, gravity
      _RL p_LNB, tau_SBM, deltaT

C     mTR and mH2 are molecular mass
C     rp_TR is the radius of the droplet
C     LH_TR is the latent heat of tracer
C     R_TR is the gas constant for tracer vapor
      _RL mH2, Runiv, AN
      _RL mTR, LH_TR, R_TR, CP_TRV, CP_TRS  
      _RL Dcoll_TR, rp_TR, rho_TR   
      _RL lnC_TR, alpha_TR, beta_TR 
      
      _RL theta  (ims:ime, jms:jme, 1:ksize  )
      _RL pTracer(ims:ime, jms:jme, 1:ksize,1:n_phase)
      _RL pcdyn  (ims:ime, jms:jme, 1:ksize  )
      _RL pedyn  (ims:ime, jms:jme, 1:ksize+1)
      _RL ze     (ims:ime, jms:jme, 1:ksize+1) 
      _RL zc     (ims:ime, jms:jme, 1:ksize+1) 
      _RL dz8w   (ims:ime, jms:jme, 1:ksize+1) 
      _RL dzc    (ims:ime, jms:jme, 1:ksize+1)
      _RL drFdyn (ims:ime, jms:jme, 1:ksize  )
      _RL hFacC  (ims:ime, jms:jme, 1:ksize  )
C  !OUTPUT PARAMETERS: ==================================================
      _RL clocal  (ims:ime, jms:jme, 1:ksize)
      _RL elocal  (ims:ime, jms:jme, 1:ksize) 
      _RL slocal  (ims:ime, jms:jme, 1:ksize) 
      _RL DTTEN   (ims:ime, jms:jme, 1:ksize)


C !LOCAL VARIABLES: ====================================================
C     i,j,k :: loop indices
      INTEGER i,j,k, iPhase
      INTEGER k_LCL, k_LNB, L_LNB
C     local function
      _RL esat, findTs
C     Dmix is a constant related to molecular diffusion (see equations)
C     MTRp is the mass of a droplet
C     termP is the pressure at grid centers in vertical
      _RL termP

C     T3D is the true temperature (not potential temperature)
C     esT is the saturation vapor pressure
C     qsV is the saturation volume mixing ratio
C     qsT is the saturation mass mixing ratio
      _RL Rmean   ( ims:ime,jms:jme)
      _RL T3D     ( ims:ime,jms:jme,ksize )
      _RL Tref    ( ims:ime,jms:jme,ksize )
      _RL esT     ( ims:ime,jms:jme,ksize )
      _RL qsT     ( ims:ime,jms:jme,ksize )
      _RL qsV

C     2-D variables
      INTEGER k_base(ims:ime, jms:jme)
      INTEGER k_ctop(ims:ime, jms:jme)
C     1-D variables
      _RL T1d    (1:ksize)
      _RL pc1d   (1:ksize)
      _RL pe1d   (1:ksize+1)
      _RL dp1d   (1:ksize)
      _RL dz1d   (1:ksize)
      _RL Tref1d (1:ksize)
      _RL qref1d (1:ksize)
      _RL ptr1d  (1:ksize,1:n_phase)

      _RL DTDT1d (1:ksize) 
      _RL c1d    (1:ksize) 
      _RL e1d    (1:ksize) 
      _RL s1d    (1:ksize)
CEOP
     
c     initialize index for LCL and LNB
      k_LCL = 0
      k_LNB = 0

c initialize local condensation, evaporation and settling
      DO j=jts,jte
      DO i=its,ite
      DO k=1,ksize
c        DTTEN applies to thermodynamic equation
c        clocal applies only to vapor equation (+)
c        elocal applies to both vapor (+) and liquid (-) equations
c        slocal applies only to liquid equation (+)
         DTTEN (i,j,k) = 0.0 _d 0
         clocal(i,j,k) = 0.0 _d 0
         elocal(i,j,k) = 0.0 _d 0
         slocal(i,j,k) = 0.0 _d 0
      ENDDO
      ENDDO
      ENDDO

c calculate virtual temperature at centers of the vertical grid

      DO k=1,ksize
C       CALL calc_mean_gas_factor(Rmean,pTracer,
C     &      ims,ime,jms,jme,1,ksize,k,n_phase)
      DO j=jts,jte
      DO i=its,ite
       termP=pcdyn(i,j,k)
C       theta(i,j,k) = theta(i,j,k)*Rmean(i,j)
       T3D(i,j,k)=(termP/atm_Po)**atm_kappa*theta(i,j,k)
      ENDDO
      ENDDO
      ENDDO
C ******************************************************
C *** START THE CONVECTIVE ADJUSTMENT SECTION **********
C ******************************************************

C--  **  Determine the initial guess of q_ref and Tref **

C--   Calculate saturation mixing ratio (q_ref) for H2O or NH3
C--   and saturation pseudo-adiabatic temperature

C--   Calculate the saturation equivalent temperature

      DO j=jts,jte
      DO i=its,ite
      DO k=1,ksize

       termP=pcdyn(i,j,k)

C      saturation mmr, only values between LCL and LBN will be 
c      used in the SBM
       esT(i,j,k)=esat( T3D(i,j,k), LH_TR,
     &     R_TR, lnC_TR, alpha_TR, beta_TR)
c      volume mixing ratio
       qsV=esT(i,j,k)/termP
c      mass mixing ratio = eps*qv/(1+(eps-1)*qv)
       qsT(i,j,k)=mTR/mH2*qsV
     &      /(1. _d 0+(mTR/mH2-1. _d 0)*qsV)
      ENDDO
      ENDDO
      ENDDO

c     start i,j loop
      DO j=jts,jte
      DO i=its,ite

c        1D variables for column SMB conv. adj.
         DO k=1, ksize
            Tref1d(k) = T3D   (i,j,k)
            T1d   (k) = T3D   (i,j,k)
            qref1d(k) = qsT   (i,j,k)
            c1d   (k) = clocal(i,j,k)
            e1d   (k) = elocal(i,j,k)
            s1d   (k) = slocal(i,j,k)
          
            dp1d  (k) = drFdyn(i,j,k)
            pc1d  (k) = pcdyn (i,j,k)
            pe1d  (k) = pedyn (i,j,k)
            dz1d  (k) = dz8w  (i,j,k)
            DO iphase = 1, n_phase
               ptr1d(k,iphase) = pTracer(i,j,k,iphase)
            ENDDO
         ENDDO

         pe1d(ksize+1) = pedyn (i,j,ksize+1)
         
c        find cloud base and top. The base is no higher than 
c        the ammonia cloud base ~ 0.6bars. The top is the 
c        tropopause.
         k_LCL = 1
         k_LNB = 1
         k=1
         DO WHILE ( ( ptr1d(k,1) .lt. qref1d(k) )
     &        .and. ( pc1d (k) .gt. 1. _d 5 ) .and. (k<ksize) )
              k_LCL = k_LCL + 1
              k = k + 1
         ENDDO
         DO k=1,ksize
c         define the initial LNB to tropopause
          IF ( pc1d(k) .gt. p_LNB ) THEN
             k_LNB = k_LNB + 1
          ENDIF
         ENDDO

c        update LNB by finding the cross point of pseudo-adiabatic 
c        line and background temperature
         L_LNB=k_LCL
         DO k=k_LCL, ksize-2
c           integrate the pseudo-adiabatic temperature
c           ideally should use qs and T at the interfaces of the 
c           vertical grid
            Tref1d(k+1) = Tref1d(k)-gravity/atm_cp*( 1. + LH_TR*
     &      qref1d(k)/(atm_Rd*T1d(k)) ) / (1. + 
     &      LH_TR**2.*qref1d(k)*mTR/mH2/(atm_Rd*atm_cp*T1d(k)**2.0) )
     &      *  dzc(i,j,k)
         ENDDO
         DO k=k_LCL+1, ksize-3
c           find the cross point and update LNB
            IF ( ( Tref1d(k-1) .gt. T1d(k-1) ) .AND.
     &           ( Tref1d(k  ) .ge. T1d(k  ) ) .AND. 
     &           ( Tref1d(k+1) .lt. T1d(k+1) ) .AND. 
     &           ( Tref1d(k+2) .lt. T1d(k+2) ) )
     &           L_LNB = k  
         ENDDO
c        safety check, LNB > LCL
         IF (L_LNB .le. k_LCL) L_LNB = k_LCL+1
c        make sure LNB is below tropopause
         k_LNB = MIN(k_LNB, L_LNB)

c        call SBM, get temperature and moisture tendencies 
         CALL SBM( T1d, pc1d, pe1d, dp1d, dz1d,
     &             Tref1d, qref1d, ptr1d,
     &             Runiv, AN, mH2, mTR, rho_TR, rp_TR, 
     &             Dcoll_TR, LH_TR, tau_SBM,
     &             gravity, atm_cp, atm_Rd, atm_kappa, deltaT,
     &             k_LCL, k_LNB, ksize, n_phase,
     &             DTDT1d, c1d, e1d, s1d, 
     &             myTime )

          DO k = 1, ksize
c          convert temperature to potential temperature 
           DTTEN (i,j,k) = DTDT1d(k)
     &                   *(atm_po/pcdyn(i,j,k))**atm_kappa
           clocal(i,j,k) = c1d   (k)
           elocal(i,j,k) = e1d   (k)
           slocal(i,j,k) = s1d   (k)  
          ENDDO

           k_base(i,j) = k_LCL
           k_ctop(i,j) = k_LNB

c     end i,j loop
      ENDDO
      ENDDO

      RETURN
      END


C ******************************************************
C *********   SBM COLUMN MOIST CONVECTION  *************
C ******************************************************

      SUBROUTINE SBM( T1d, pc1d, pe1d, dp1d, dz1d,
     &                Tref1d, qref1d, ptr1d,
     &                Runiv, AN, mH2, mTR, rho_TR, rp_TR, 
     &                Dcoll_TR, LH_TR, tau_SBM,
     &                gravity, atm_cp, atm_Rd, atm_kappa, deltaT,
     &                k_LCL, k_LNB, ksize, n_phase,
     &                DTDT1d, c1d, e1d, s1d, 
     &                myTime )

      IMPLICIT NONE
       
C--   ===  INPUT  ===
      INTEGER ksize, n_phase
     
      _RL myTime, tau_SBM

      _RL gravity, atm_cp, atm_Rd, atm_kappa, deltaT

      _RL mH2, Runiv, AN
      _RL mTR, LH_TR, R_TR  
      _RL Dcoll_TR, rp_TR, rho_TR

      _RL T1d    (1:ksize)
      _RL pc1d   (1:ksize)
      _RL pe1d   (1:ksize+1)
      _RL dp1d   (1:ksize)
      _RL dz1d   (1:ksize)
      _RL Tref1d (1:ksize)
      _RL qref1d (1:ksize)
      _RL ptr1d  (1:ksize,1:n_phase)
C--   ===  OUTPUT  ===
      INTEGER k_bot k_top

      _RL DTDT1d (1:ksize) 
      _RL DCDT1d (1:ksize)
      _RL c1d    (1:ksize) 
      _RL e1d    (1:ksize) 
      _RL s1d    (1:ksize)

C--    ===  Local variables  ===
      INTEGER k

      INTEGER k_LCL 
      INTEGER k_LNB 

      INTEGER k_shall, k_cloudtop

      LOGICAL deep_conv, evap_cond
    
      _RL Pq, PT, PR        
      _RL delHq, delHT     
      _RL p_cloudtop
      
      _RL DQDT1d (1:ksize) 
      _RL delT2  (1:ksize) 

c     evaporation in cloud and subcloud layers
      _RL e1d_in (1:ksize)
      _RL e1d_sub(1:ksize)

      _RL DTDTDC (1:ksize)
      _RL DQDTDC (1:ksize)

c--     Initialize variables

      deep_conv = .TRUE.
      evap_cond = .FALSE.

      k_cloudtop = k_LNB
       
      DO k=1, ksize
c       initialize the correction term
        delT2(k)   = 0.0 _d 0
c       initialize the amount of evaporation
        e1d_in(k)  = 0.0 _d 0
        e1d_sub(k) = 0.0 _d 0
c       initialize heating rate/vapor tendency 
c       due to dry convective adjustment
        DTDTDC(k)  = 0.0 _d 0
        DQDTDC(k)  = 0.0 _d 0
c       initialize the total heating rate 
        DTDT1d(k)  = 0.0 _d 0
c       initialize relaxation rate on vapor
        DQDT1d(k)  = 0.0 _d 0
c       initialize precipitation rate
        DCDT1d(k)  = 0.0 _d 0
      ENDDO
        
C--  ** Integerate from P_LCL to P_LNB to find precipitation **

c     precipitation rate due to drying
      Pq = 0.0 _d 0
c     precipitation rate due to cooling
      PT = 0.0 _d 0
c     precipitation rate (advecive, for shallow and deep conv)
      PR = 0.0 _d 0
      DO k=k_LCL,k_LNB
        Pq = Pq+(ptr1d(k,1) - qref1d(k))*dp1d(k)/gravity/tau_SBM
        PT = PT-(atm_cp*(T1d(k)-Tref1d(k))/LH_TR)
     &         *dp1d(k)/gravity/tau_SBM
      ENDDO

C--  ** Find the conditions for deep and shallow convection **
C--  ** and make corrections to the reference temperature   **

c     initialize the change of enthalpy due to warming/cooling
      delHT  = 0.0 _d 0 
c     initialize the change of enthalpy due to drying
c     and corrections to Tref
      delHq  = 0.0 _d 0

ccc--*** IF DEEP CONVECTION ********************************--
      IF (    (Pq .GT. 0.0) .AND. (PT .GT. 0.0)    ) THEN

c        define cloud top
         p_cloudtop = pe1d(k_LNB+1)
         k_cloudtop = k_LNB

ccc--**** IF SHALLOW CONVECTION *****************************--
      ELSEIF ( (Pq .LT. 0.0) .AND. (PT .GT. 0.0)   ) THEN

         k_shall = k_LNB
c        scan from top down to satisfy sum[ (q-q_ref)*dp ] = 0
c        also enforce the condition p_cloudtop < p_LCL (k > k_LCL)
         DO WHILE ( (Pq .LT. 0.0).AND.(k_shall .GT. k_LCL) )
            Pq = Pq - ( ptr1d(k_shall,1)-qref1d(k_shall) )
     &                             * dp1d(k_shall)/gravity
            k_shall = k_shall - 1
         ENDDO

cc       Check if shallow convection indeed occurs
cc       make sure Pq>0 at k_shall. Moist convection occurs 
c        when CAPE > 0. if CAPE > 0 and sum[ (q-q_ref)*dp ] < 0, 
c        no convective adjustment is made

c------- INSPECT SHALLOW CONVECTION -----------
         IF ( Pq .GE. 0.0 ) THEN
           p_cloudtop = pe1d(k_shall+1)
           k_cloudtop = k_shall
         ELSE
           deep_conv=.FALSE.
c------- END INSPECT SHALLOW CONVECTION -------
         ENDIF

c---*** EVAPORATION IN CLOUD OCCURS *************************--
       ELSEIF ( (Pq .LT. 0.0) .AND. (PT .LE. 0.0)   ) THEN
         evap_cond=.TRUE.
         deep_conv=.FALSE.

c---*** NO DEEP CONVECTION OR SHALLOW CONVECTION ************--
       ELSE 
         deep_conv=.FALSE.

ccc--*** END IF DEEP/SHALLOW CONVECTION *********************--
       ENDIF 

ccc--*** DISTRIBUTING MMR AND MAKING CORRECTION TO T_REF
c      shalower shalow convection is an approximation of 
c      deep convection with lowered LNB
       IF (deep_conv) THEN
         DO k=k_LCL,k_cloudtop
c          integrate L(q-qs) and cp*(T-Tref)*dp from p_LCL to p_LNB
           delHq = delHq - LH_TR*(ptr1d(k,1)-qref1d(k))*dp1d(k)
           delHT = delHT - atm_cp*(T1d(k)-Tref1d(k)) * dp1d(k)
c          calculate amount of precipitation
           PR = PR+(ptr1d(k,1) - qref1d(k))*dp1d(k)
         ENDDO

c        average  L*(q-qs)*dp in the cloud
         delHq = delHq / (pe1d(k_LCL) - p_cloudtop)
c        average  cp*(T-Tref)*dp in the cloud
         delHT = delHT / (pe1d(k_LCL) - p_cloudtop)

         DO k=k_LCL,k_cloudtop
c--------- correction to Tref due to latent heat release in deep convection
           delT2(k) = -( delHT + delHq ) / atm_cp
c          calculate vapor and temperature tendencies
           DQDT1d(k) = - (ptr1d(k,1)-qref1d(k))/tau_SBM
           DTDT1d(k) = - (T1d(k)-(Tref1d(k)+delT2(k)))/tau_SBM
c          distribute condensates uniformly in vertical
           DCDT1d(k) = PR/(pe1d(k_LCL) - p_cloudtop)/tau_SBM
         ENDDO
c      no cloud convection, nothing is changed
       ELSE
         DO k=k_LCL,k_cloudtop
           DQDT1d(k) = 0.0 _d 0
           DTDT1d(k) = 0.0 _d 0
           DCDT1d(k) = 0.0 _d 0
         ENDDO
       ENDIF

ccc--*** EVAPORATION ***************************************--
c      Evaporation in cloud (from k_LCL to k_LNB)
       IF ( (.not.deep_conv).and.(evap_cond) ) THEN
          CALL EVAP (
     &         T1d, pc1d, dp1d, ptr1d, qref1d, tau_SBM, 
     &         Runiv, AN, mH2, mTR, rho_TR, rp_TR, 
     &         Dcoll_TR, atm_Rd, gravity, deltaT, n_phase, 
     &         ksize, k_LCL,k_LNB, e1d_in )   
       ENDIF
c     Evaporation in subcloud (from 1 to k_LCL-1)
      CALL EVAP (
     &           T1d, pc1d, dp1d, ptr1d, qref1d, tau_SBM, 
     &           Runiv, AN, mH2, mTR, rho_TR, rp_TR, 
     &           Dcoll_TR, atm_Rd, gravity, deltaT, n_phase, 
     &           ksize, 1,k_LCL-1, e1d_sub )
      DO k=1,ksize
         e1d(k) = e1d_in(k) + e1d_sub(k)
      ENDDO

ccc--*** SEDIMENTATION IN ENTIRE COLUMN *********************--

      CALL SED (
     &           T1d, pc1d, dp1d, dz1d, ptr1d, 
     &           Runiv, mH2, mTR, rho_TR, rp_TR, 
     &           atm_Rd, gravity, deltaT, n_phase, 
     &           ksize,e1d, s1d )
ccc--*** DRY CONVECTION IN SUBCLOUD LAYERS ******************--
      CALL DRY_CONVECT_ADJ(
     &           T1d, pc1d, dp1d, ptr1d, tau_SBM,
     &           Runiv, mH2, mTR, rho_TR, rp_TR, 
     &           atm_Rd, atm_kappa, gravity, deltaT, n_phase, 
     &           k_LCL, ksize, DTDTDC, DQDTDC, myTime)

ccc--*** UPDATE TEMPERATURE/VAPOR TENDENCIES IN 1-D COLUMN **--
c      DTDT1d applies to thermodynamic equation
c      c1d applies only to vapor equation
c      e1d applies to both vapor (+) and liquid (-) equations
c      s1d applies only to liquid equation
       DO k=1,ksize
c                    relaxation     latent cooling   dry convection
          DTDT1d(k) = DTDT1d(k)  - LH_TR*e1d(k)/atm_cp + DTDTDC(k)
c                    relaxation     dry convection
          c1d(k)    = DQDT1d(k)  +   DQDTDC(k)
c                   sedimentation  Precipitation
          s1d(k)    = s1d(k)     +   DCDT1d(k)
c         e1d(k)    = e1d(k) is the final value

       ENDDO

       RETURN
       END
C ******************************************************
C ***************** EVAPORATION  ***********************
C ******************************************************
      SUBROUTINE EVAP (
     &           T1d, pc1d, dp1d, ptr1d, qs1d, tau_SBM, 
     &           Runiv, AN, mH2, mTR, rho_TR, rp_TR, 
     &           Dcoll_TR, atm_Rd, gravity, deltaT, n_phase, 
     &           ksize, k_bot,k_top, e1d )

      IMPLICIT NONE

c === INPUT ===
      INTEGER n_phase
      INTEGER ksize, k_bot,k_top
 
      _RL mTR, rho_TR, rp_TR, Dcoll_TR, tau_SBM
      _RL Runiv, AN, mH2, atm_Rd, gravity, deltaT

      _RL T1d   (1:ksize)
      _RL pc1d  (1:ksize)
      _RL dp1d  (1:ksize)
      _RL ptr1d (1:ksize, 1:n_phase)
      _RL qs1d  (1:ksize)
c === OUTPUT ===
      _RL e1d   (1:ksize)

c === Local variables === 
      INTEGER k
      _RL PI, Dmix, MTRp
 
      _RL recip_taue (1:ksize)

      PI = acos(-1. _d 0)

C----mass of a droplet=4/3*pi*rpw^3*rho_liquid, in kg
      MTRp = 4.0/3.0*PI*rp_TR**3.0*rho_TR

      Dmix = 5.0/(16.0*AN*Dcoll_TR**2.0)*
     &       sqrt(Runiv*mH2/2.0/PI*(mTR+mH2)/mTR)

C-----Convert unit for Dmix from g to kg
      Dmix = Dmix/1000.0 _d 0

C--   Calculate timescale for evaporation      
c     evaporation rate should be comparable to condensation rate
      DO k=k_bot,k_top
         recip_taue(k) = (4.0 _d 0*PI*Dmix*
     &      sqrt(T1d(k))*rp_TR*ptr1d(k,2))/MTRp
         IF (ptr1d(k,1) .lt. qs1d(k) )
     &      e1d(k) = min(ptr1d(k,2), qs1d(k)-ptr1d(k,1))
     &             * min(recip_taue(K), 1.0/deltaT)
      ENDDO

      RETURN
      END

C ******************************************************
C ***************** SEDIMENTATION  *********************
C ******************************************************
      SUBROUTINE SED (T1d, pc1d, dp1d, dz1d, ptr1d, 
     &           Runiv, mH2, mTR, rho_TR, rp_TR, 
     &           atm_Rd, gravity, deltaT, n_phase, 
     &           ksize,e1d, s1d)

      IMPLICIT NONE

C three sedimentation methods, method 1 is simple 
C and conserves tracer mass but not accurate in mass flux calculation
C method 2 is the tracer mass flux form
C method 3 is the tracer mmr flux form
#ifndef qset_meth1
#define qset_meth1
#undef  qset_meth2
#undef  qset_meth3
#endif

c === INPUT ===
      INTEGER n_phase
      INTEGER ksize
 
      _RL mTR, rho_TR, rp_TR
      _RL Runiv, mH2, atm_Rd, gravity, deltaT

      _RL T1d   (1:ksize)
      _RL pc1d  (1:ksize)
      _RL dp1d  (1:ksize)
      _RL dz1d  (1:ksize)
      _RL ptr1d (1:ksize, 1:n_phase)
      _RL qs1d  (1:ksize)
      _RL e1d   (1:ksize)
c === OUTPUT ===
      _RL s1d   (1:ksize)

c === Local variables === 
      INTEGER k
      _RL PI
 
      _RL visdync(1:ksize)
      _RL wsetc  (1:ksize)
      _RL qsettle(1:ksize)

      _RL flux_top, flux_bot
 
      PI = acos(-1. _d 0)


C--   Calculate settling velocity ws that is nearly constant
      DO k=1,ksize
C        calculate dynamic viscosity following Sutherland's formula
         visdync(K) = 8.76 _d -6 * (293.85 _d 0 + 72.0 _d 0)/
     &                   (T1d(K) + 72.0 _d 0) * 
     &                   (T1d(K)/293.85 _d 0)**1.5 _d 0  

         wsetc(K)   = -2.0 _d 0
     &                  * ( rho_TR-pc1d(k)/(atm_Rd*
     &                      T1d(k)) )*gravity*rp_TR**2.0 _d 0/
     &                    (9.0 _d 0*visdync(K))
c        wset should always be less than zero (moving downward). 
c        If the air density is larger than droplet density, then 
c        the droplet is stalled, so wset=0
         wsetc(K) = MIN(wsetc(K), 0.0 _d 0)
      ENDDO

C--   including the settling of particles vertical gradient of qp 
c     and scale height need to recursively add settling droplet to 
c     one layer below if qsettle < 0
#if defined( qset_meth1 )
      DO k=1,ksize
c      needs to remove the evaporated liquid particles from 
c      amount of settling particles 
       qsettle(k) = 
     &  ( ptr1d(k,2) - e1d(k)*deltaT )*wsetc(k) / dz1d(k)
c      lost of liquid tracer in all layers
       s1d(k) = qsettle(k)
      ENDDO
c     no settling in the bottom layer
      s1d(1) = 0.0 _d 0

c     in middle layers
      DO k=ksize, 2, -1
         s1d(k-1) = s1d(k-1) - qsettle(k)*dp1d(k) / dp1d(k-1)
      ENDDO

#elif defined ( qset_meth2 )
      DO k=1,ksize
       qsettle(k) = ( ptr1d(k,2) - e1d(k)*deltaT )*wsetc(k)
      ENDDO      
c     Lost of liquid due to downward flux in top layer 
      flux_top = 0.0 _d 0 + dp1d(ksize)/2. _d 0 * qsettle(ksize)
      flux_bot = ( dp1d(ksize  )/2. _d 0 * qsettle(ksize  ) 
     &            +dp1d(ksize-1)/2. _d 0 * qsettle(ksize-1) )
      s1d(ksize) = - ( flux_top - flux_bot ) / dz1d(ksize) 
     &             / dp1d(ksize) 
c     in middle layers ( 2 to ksize-1 )
      DO k=ksize-1, 2, -1
c        averge flux across interface k+1
         flux_top = ( dp1d(k+1)/2. _d 0 * qsettle(k+1) 
     &               +dp1d(k  )/2. _d 0 * qsettle(k  ) )
c        average flux across interface k
         flux_bot = ( dp1d(k  )/2. _d 0 * qsettle(k  ) 
     &               +dp1d(k-1)/2. _d 0 * qsettle(k-1) )
c        rate change of liquid at center k == - d(dp*q*w)/dp/dz
         s1d(k) = - ( flux_top - flux_bot ) / dz1d(k)
     &          / dp1d(k)
      ENDDO
c     No settling in the bottom layer
      flux_top = ( dp1d(2)/2. _d 0 * qsettle(2) 
     &           + dp1d(1)/2. _d 0 * qsettle(1) ) 
      flux_bot = dp1d(1)/2. _d 0 * qsettle(1) + 0.0 _d 0
      s1d(1) =  - ( flux_top - flux_bot ) / dz1d(1)
     &          / dp1d(1) 
#elif defined ( qset_meth3 )
      DO k=1,ksize
       qsettle(k) = ( ptr1d(k,2) - e1d(k)*deltaT )*wsetc(k)
      ENDDO
c     Lost of liquid due to downward flux in top layer 
      flux_top = 0.0 !qsettle(ksize)
      flux_bot = ( dp1d(ksize  ) * qsettle(ksize  )
     &            +dp1d(ksize-1) * qsettle(ksize-1) )
     &         / ( dp1d(ksize) + dp1d(ksize-1) )
      s1d(ksize) = - ( flux_top - flux_bot ) / dz1d(ksize)
c     in middle layers ( 2 to ksize-1 )
      DO k=ksize-1, 2, -1
c        averge flux across interface k+1
         flux_top = ( dp1d(k+1) * qsettle(k+1)
     &               +dp1d(k  ) * qsettle(k  ) )
     &            / ( dp1d(k+1) + dp1d(k  ) )
c        average flux across interface k
         flux_bot = ( dp1d(k  ) * qsettle(k  )
     &               +dp1d(k-1) * qsettle(k-1) )
     &            / ( dp1d(k  ) + dp1d(k-1) )
c        rate change of liquid at center k == - d(qw)/dz
         s1d(k) = - ( flux_top - flux_bot ) / dz1d(k)
      ENDDO
c     No settling in the bottom layer
      flux_top = ( dp1d(2) * qsettle(2)
     &           + dp1d(1) * qsettle(1) )
     &         / ( dp1d(2) + dp1d(1) ) 
      flux_bot =  0.0 !qsettle(1) 
      s1d(1) =  - ( flux_top - flux_bot ) / dz1d(1)
#endif /* qset_meth */

      RETURN
      END


C ******************************************************
C ********* DRY CONVECTION IN SUBCLOUD LAYERS  *********
C ******************************************************

c     Only adjust the amount of vapor (not liquid droplets)

      SUBROUTINE DRY_CONVECT_ADJ(
     &               T1d, pc1d, dp1d, ptr1d, tau_SBM,
     &               Runiv, mH2, mTR, rho_TR, rp_TR, 
     &               atm_Rd, atm_kappa, gravity, deltaT, n_phase, 
     &               k_top, ksize, DTDTDC, DQDTDC, myTime)

C     !DESCRIPTION: \bv
C     *==========================================================*
C     | SUBROUTINE CONVECTIVE_ADJUSTMENT
C     | o Driver for vertical mixing or similar parameterization
C     *==========================================================*
C     \ev

C     !USES:
      IMPLICIT NONE

      _RL myTime

      EXTERNAL DIFFERENT_MULTIPLE
      LOGICAL  DIFFERENT_MULTIPLE

C     !INPUT/OUTPUT PARAMETERS:
c === INPUT ===
      INTEGER n_phase
      INTEGER ksize, k_top
 
      _RL mTR, rho_TR, rp_TR, tau_SBM
      _RL Runiv, mH2, atm_Rd, atm_kappa, gravity, deltaT

      _RL T1d   (1:ksize)
      _RL pc1d  (1:ksize)
      _RL dp1d  (1:ksize)
      _RL ptr1d (1:ksize, 1:n_phase)

c === OUPUT ===
      _RL DTDTDC(1:ksize)
      _RL DQDTDC(1:ksize)

C     !LOCAL VARIABLES
C     == Local variables ==
C     TatCi --- initial temperature at the center of layer 
C     TatCf --- final   temperature at the center of layer
C     PtrCi --- initial tracers at the center of layer
C     PtrCf --- final   tracers at the center of layer
C     C1fac, C2fac --- factors during calculation
C     dp1d --- layer thickness in pressure (Pa)
C     epsc --- convergence criterion (less stringent than 0)
C     epscmax --- maximum value of epsc in [K]
C     counti --- counter for unstable layers in each column
C     itern --- number of iteration to make a stable column
C     maxiter --- maximum number of itern for each epsc (20 is default)
C     km1 --- bottom layer number
C     ku1 --- layer number at top of the adjusted region (Nr is default)
C     
C     convective adjustment starts from top down (less error than 
C     from bottom up)    

      INTEGER k,km1,ku1
      INTEGER counti, itern, maxiter
      _RL TatCi (1:ksize)
      _RL TatCf (1:ksize)
      _RL C1fac (1:ksize)
      _RL C2fac (1:ksize)

      _RL PtrCi (1:ksize)
      _RL PtrCf (1:ksize)

      _RL one, two, epsc, epscmax

      one  = 1.00 _d 0
      two  = 2.00 _d 0 
      epsc = 0.01 _d 0
      epscmax = 0.5 _d 0

      maxiter = 100
      ku1 = k_top
      km1 = 1

      DO k=1,ksize
         DTDTDC(k) = 0.0
         DQDTDC(k) = 0.0

         PtrCi (k) = 0.0
         PtrCf (k) = 0.0
         TatCi (k) = T1d(k)
         TatCf (k) = T1d(k)
         C1fac (k) = 0.0
         C2fac (k) = 0.0
      ENDDO

C--   START CONVECTIVE ADJUSTMENT OR NOT
      IF ( DIFFERENT_MULTIPLE(tau_SBM,myTime,deltaT)
     &   ) THEN

C--     define factors without change during adjustment

        DO k=ku1,km1+1,-1
           C1fac(k-1)=atm_kappa*(pc1d(k-1)-
     &        pc1d(k))/(pc1d(k-1) + pc1d(k))
           C2fac(k-1)=(one - C1fac(k-1))/(one + C1fac(k-1))
        ENDDO
C       Ci(i,j,1) is not used based on the fornmula
  
C--     initial temperature field and tracer field
        DO k=ku1,km1,-1
         TatCi(k)=T1d(k)
         PtrCi(k)=ptr1d(k,1)
        ENDDO

C=== start checking stability and make adjustment ===

        epsc = 0.01
C--     enable the iterative adjustment by setting counti=1
        counti = 1
        itern  = 0
C--// loop in a single column //-------
        DO WHILE (counti .GT. 0)
C--       reset counter to be zero
          counti=0
          DO k=ku1,km1+1,-1

             IF ( (TatCi(k-1)-TatCi(k)) .GT. 
     &          (C1fac(k-1)*(TatCi(k-1)+TatCi(k))
     &          +epsc) )  THEN

C--            unstable --> 
C--            perform convective adjustment, see CAM Eq. 4.161
C--            Temperature first
               TatCf(k-1) = dp1d(k)/(dp1d(k-1)
     &           +dp1d(k)*C2fac(k-1))*TatCi(k)
     &                   +  dp1d(k-1)/(dp1d(k-1)
     &           +dp1d(k)*C2fac(k-1))*TatCi(k-1)
               TatCf(k  ) = C2fac(k-1)*TatCf(k-1)
C--            replace the initial temperature in layers 
C--            k and k-1 by adjusted temperature
               TatCi(k  ) = TatCf(k  )
               TatCi(k-1) = TatCf(k-1)
C--            Update tracers in unstable region
               PtrCf(k  ) = ( PtrCi(k-1)
     &           *dp1d(k-1)+PtrCi(k)*dp1d(k) )/( dp1d(k-1)+dp1d(k) )
               PtrCf(k-1)=PtrCf(k)
C--            replace the initial tracers in layers 
C--            k and k-1 by adjusted tracers
               PtrCi(k  ) = PtrCf(k  )
               PtrCi(k-1) = PtrCf(k-1)

C--        counter increases if there is still unstable region
               counti=counti+1
             ENDIF
          ENDDO

          itern=itern+1
          IF (itern .EQ. maxiter) THEN 
              epsc=two*epsc
              itern=0
          ENDIF 
          IF (epsc  .GT. epscmax    ) THEN
             STOP 'CAN NOT FINISH CONVECTIVE ADJUSTMENT!'
          ENDIF
        ENDDO
C--// end loop in a single column //---------

C--// move the adjusted temperature and tracers to permanent storage //
C--   now from bottom up
        DO k=km1,ku1
c adjust both temperature and tracers (instantaneous)
         DTDTDC(k)= - ( T1d  (k  ) - TatCi(k) )/deltaT
         DQDTDC(k)= - ( ptr1d(k,1) - PtrCi(k) )/deltaT
        ENDDO

C--   End IF (DIFFERENT_MULTIPLE)
      ENDIF

      RETURN
      END

      SUBROUTINE calc_mean_gas_factor(Rmean,ptr,
     &                 im1,im2,jm1,jm2,km1,km2,K,n_phase)

      IMPLICIT NONE

#include "SIZE.h"
#include "EEPARAMS.h"

#ifdef ALLOW_PTRACERS
#include "PTRACERS_SIZE.h"
#include "PTRACERS_PARAMS.h"
#endif /* ALLOW_PTRACERS */

C--     ====local variables ====
      INTEGER  im1,im2,jm1,jm2,km1,km2,K,n_phase
      INTEGER  I,J,iTracer

      _RL Rmean(im1:im2,jm1:jm2)

      _RL ptr  (im1:im2,jm1:jm2,km1:km2,1:n_phase)
      _RL Rqtot
C.... Rmean is the mean gas constant R_mean/Rd
C.... Rq is (1-eps)/eps, and eps=m(i)/m_d
C.....PTRACERS_active is a switch to make tracer(i) to be 
C     passive (0) or active (1) 

      DO I = im1, im2
      DO J = jm1, jm2
            Rqtot = 0.0 _d 0
            DO iTracer = 1, n_phase
C  calculate sum(Rq(i)*q(i)) at (i,j,k)
               Rqtot = Rqtot + PTRACERS_Rq(iTracer)
     &               * ptr(I,J,K,iTracer)
     &               * PTRACERS_active(iTracer)
            ENDDO
C  calculate the mean gas constant 
C  R_mean/Rd = 1 + sum(Rq(i)*q(i))
            Rmean(I,J) = 1.0 _d 0 + Rqtot
      ENDDO
      ENDDO

      RETURN
      END

