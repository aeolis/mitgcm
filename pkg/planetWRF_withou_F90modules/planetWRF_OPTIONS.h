C==/// the following two header files are mandatory!!!
#include "PACKAGES_CONFIG.h"
#include "CPP_OPTIONS.h"

C--//++++++++++++++++ Section 1 ++++++++++++++++++
C==/// Model Selection:  only change the definition of Mars or Titan ///==
#ifndef WRF_MARS
#define WRF_MARS
#endif /* WRF_MARS */

#ifndef WRF_TITAN
#undef WRF_TITAN
#endif /* WRF_TITAN */
C==/// End of Model Selection ///==


C--//++++++++++++++++ Section 2 ++++++++++++++++++
C==/// Physics Selection (shared by all models) ///==
C-- // the following should not be changed, unless it 
C-- // is very necessary (such as debugging !!!)

#ifndef EM_CORE
#define EM_CORE
#endif /* EM_CORE */

C==/// End of Physics Selection ///==



C--//++++++++++++++++ Section 3 ++++++++++++++++++
C=== /// Start of configurable section for physics /// ===


C--//++++++++++++++++ Section 4 ++++++++++++++++++
C=== /// Fixed definition, not user changeable!!! /// ====


C--// this part can not be modified !!! This overides the user defined 
C--// option if the dependance package does not present. Turning off 
C--// WRF_ENABLE_TRACERS intentionally will turn off the physics processes 
C--// that modify the tracer field (in physics grid), but it won't affect 
C--// the tracer advection as long as ALLOW_PTRACERS is defined (in dynamics grid).
#ifdef ALLOW_PTRACERS

#define WRF_ENABLE_TRACERS 

#else /* ALLOW_PTRACERS */

#undef WRF_ENABLE_TRACERS

#endif /* ALLOW_PTRACERS */

