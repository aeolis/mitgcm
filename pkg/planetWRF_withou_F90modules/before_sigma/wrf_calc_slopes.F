#include "CPP_OPTIONS.h"
      SUBROUTINE WRF_CALC_SLOPES (
     &               htd, alphaslp, gammaslp,            
     &               xlat, xlong, dxc, dyc,
     &               angleCS, angleSN,         
     &               ims , ime , jms , jme , kms , kme , 
     &               its , ite , jts , jte , kts , kte )
 
C! Input and output variables

      IMPLICIT NONE

      INTEGER ims , ime , jms , jme , kms , kme , 
     &        its , ite , jts , jte , kts , kte

      _RL  htd     (ims:ime,jms:jme)  

      _RL  alphaslp(ims:ime,jms:jme) 
      _RL  gammaslp(ims:ime,jms:jme)

      _RL  xlat    (ims:ime,jms:jme)
      _RL  xlong   (ims:ime,jms:jme) 
      _RL  dxc     (ims:ime,jms:jme)
      _RL  dyc     (ims:ime,jms:jme)
      _RL  angleCS (ims:ime,jms:jme)
      _RL  angleSN (ims:ime,jms:jme)


C! Local variables
      _RL  dzdx(ims:ime,jms:jme)
      _RL  dzdy(ims:ime,jms:jme)

      _RL  dzdew(ims:ime,jms:jme)
      _RL  dzdns(ims:ime,jms:jme)


      _RL pi

      INTEGER i, j


      DO j=jms+1,jme-1
      DO i=ims+1,ime-1
         dzdx(i,j)=(htd(i+1,j)-htd(i,j))/dxc(i,j)
         dzdy(i,j)=(htd(i,j+1)-htd(i,j))/dyc(i,j)
      END DO
      END DO

      pi = ACOS(-1.)

      DO j=jms+1,jme-1
      DO i=ims+1,ime-1
         dzdew(i,j) = dzdx(i,j)*angleCS(i,j)-dzdy(i,j)*angleSN(i,j)
         dzdns(i,j) = dzdx(i,j)*angleSN(i,j)+dzdy(i,j)*angleCS(i,j) 
      ENDDO
      ENDDO

      DO j=jms+1,jme-1
      DO i=ims+1,ime-1
          alphaslp(i,j) = ATAN(SQRT(dzdew(i,j)**2 + dzdns(i,j)**2))
          ! Gamma (in radians) is defined so that:
          ! 0     is a slope that increases to the north  (faces south)
          ! pi/2  is a slope that increases to the east   (faces west)
          ! +/-pi is a slope that increases to the south  (faces north)
          ! -pi/2 is a slope that increases to the west   (faces east)
          gammaslp(i,j) = pi/2. - ATAN2(dzdns(i,j),dzdew(i,j))

          IF (gammaslp(i,j).GT.pi) 
     &       gammaslp(i,j) = gammaslp(i,j) - 2.0*pi
          IF (gammaslp(i,j).LT.-pi)
     &       gammaslp(i,j) = gammaslp(i,j) + 2.0*pi
      END DO
      END DO


      RETURN
      END
