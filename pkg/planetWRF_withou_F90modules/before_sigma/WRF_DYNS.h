C $Header: /u/gcmpack/MITgcm/pkg/wrf/wrf_coms.h,v 1.7 2004/10/14 22:11:42 molod Exp $
C $Name:  $

c WRF Exports Common - Physics variables on other grids for export
c ----------------------------------------------------------------------
C the names are different, for example, guphy is equivalent to 
C RUBLTEN, etc.
C
C Radtran tendency: gthraphy
C PBL tendencies: gublphy,  gvblphy,  gthblphy,
C                 gqvblphy, gqcblphy, gqiblphy 

      common /physics_exports/ 
     &               gthraphy,
     &               gublphy,  gvblphy,  gthblphy,
     &               gTrdyn            

      _RL gthraphy(1-OLx:sNx+OLx,1-OLy:sNy+OLy,Nr,Nsx,Nsy)
      _RL gublphy(1-OLx:sNx+OLx,1-OLy:sNy+OLy,Nr,Nsx,Nsy)
      _RL gvblphy(1-OLx:sNx+OLx,1-OLy:sNy+OLy,Nr,Nsx,Nsy)
      _RL gthblphy(1-OLx:sNx+OLx,1-OLy:sNy+OLy,Nr,Nsx,Nsy)
#ifdef WRF_ENABLE_TRACERS
      _RL gTrdyn(1-OLx:sNx+OLx,1-OLy:sNy+OLy,Nr,nSx,nSy,PTRACERS_num)
#else 
      _RL gTrdyn(1-OLx:sNx+OLx,1-OLy:sNy+OLy,Nr,nSx,nSy)
#endif /* WRF_ENABLE_TRACERS */

      common /dynamics_vertical_grid/
     &              pedyn, pcdyn, mup

      _RL pedyn(1-OLx:sNx+OLx,1-OLy:sNy+OLy,Nr+1,nSx,nSy)
      _RL pcdyn(1-OLx:sNx+OLx,1-OLy:sNy+OLy,Nr,nSx,nSy)
      _RL mup(1-OLx:sNx+OLx,1-OLy:sNy+OLy,nSx,nSy)

