! These are from module_ra_valverde.F

      INTEGER np1, np2, nl
      PARAMETER ( np1 = 110 )  ! # data points in Table#1
      PARAMETER ( np2 = 114 )  ! # data points in Table#2
      PARAMETER ( nl  = 151 )  ! number of atmospheric layers in the reference atmosphere file

      COMMON / RA VALVERDE /
     &       co2vmr, o3pvmr, n2vmr, covmr,
     &       tk,     pmb,    nt,    
     &       pnb1,   ef1,    pnb2,  ef2

      REAL*8 co2vmr(nl),o3pvmr(nl),n2vmr(nl),covmr(nl)  ! vol.mixing ratios from the reference file
      REAL*8 tk (nl)     ! temperature in K from reference atmosphere file
      REAL*8 pmb(nl)     ! pressure in milibars from reference file
      REAL*8 nt (nl) 

      REAL*8 x  (nl)     ! dummy variable
      REAL*8 xx (nl)     ! dummy variable

      REAL*8 pnb1(np1)   ! Pressure in table #1
      REAL*8 ef1 (np1)   ! Esc.funct.#1, tabulated  

      REAL*8 pnb2(np2)   ! Pressure in table #2
      REAL*8 ef2 (np2)   ! Esc.funct.#2, tabulated

