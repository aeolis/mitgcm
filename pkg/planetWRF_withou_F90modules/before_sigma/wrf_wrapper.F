C $Header: /u/gcmpack/MITgcm/pkg/planetWRF/wrf_wrapper.F,v 1.35 2009/04/28 18:15:33 jmc Exp $
C $Name:  $

#include "planetWRF_OPTIONS.h"
      subroutine wrf_wrapper (myTime, myIter, myThid)
c-----------------------------------------------------------------------
c  Subroutine wrf_wrapper - 'Wrapper' routine to interface
c        with physics driver.
c        1) Set up "bi, bj loop"  and some timers and clocks.
c        2) Call do_planetWRF - driver for physics which computes tendencies
c        3) Interpolate tendencies to dynamics grid in vertical
c        4) Convert u,v tendencies to C-Grid
c
c Calls: do_planetWRF (get u,v,t,s tend, step tke, etc and tc, etc. forward)
c        phys2dyn (4 calls - all physics tendencies)
c        AtoC (u and v tendencies)
c-----------------------------------------------------------------------
      IMPLICIT NONE
#include "SIZE.h"
#include "WRF_SIZE.h"
#include "GRID.h"
#include "EEPARAMS.h"
#include "PARAMS.h"
#include "SURFACE.h"
#include "DYNVARS.h"
#ifdef WRF_ENABLE_TRACERS
#include "PTRACERS_SIZE.h"
#include "PTRACERS_PARAMS.h"
#include "PTRACERS_FIELDS.h"
#endif /* WRF_ENABLE_TRACERS */
#include "gridalt_mapping.h"
#include "WRF_CONFIG.h"
#include "WRF_PHYS.h"
#include "WRF_DYNS.h"

      INTEGER myIter, myThid
      _RL myTime
      LOGICAL  diagnostics_is_on
      EXTERNAL diagnostics_is_on

c pe on dynamics and physics grid refers to bottom edge
      _RL tempphy(1-OLx:sNx+Olx,1-Oly:sNy+Oly,Nrphys,nSx,nSy)
      _RL tempLdiag(sNx,sNy,Nrphys+1)
      _RL tempLdiag2(sNx,sNy,Nrphys)
      _RL tempdiag(sNx,sNy)

      INTEGER i, j, L, Lbotij, bi, bj, iTracer
      INTEGER im1, im2, jm1, jm2, idim1, idim2, jdim1, jdim2

      idim1 = 1-OLx
      idim2 = sNx+OLx
      jdim1 = 1-OLy
      jdim2 = sNy+OLy
      im1 = 1
      im2 = sNx
      jm1 = 1
      jm2 = sNy

      DO bj = myByLo(myThid), myByHi(myThid)
       DO bi = myBxLo(myThid), myBxHi(myThid)

c Construct the physics grid pressures
C        For the interpolations between physics and dynamics (bottom-up)
        DO j = 1,sNy
         DO i = 1,sNx
          pephy(i,j,1,bi,bj)=(Ro_surf(i,j,bi,bj) + etaH(i,j,bi,bj))
          DO L = 2,Nrphys+1
           pephy(i,j,L,bi,bj)=pephy(i,j,L-1,bi,bj)-dpphys(i,j,L-1,bi,bj)
          ENDDO
c Do not use a zero field as the top edge pressure for interpolation
          IF (pephy(i,j,kme,bi,bj) .LT. 1.e-8)
     .                               pephy(i,j,kme,bi,bj) = 1.e-8
         ENDDO
        ENDDO
C get pressures at the centers of the physics grid
        DO j = 1,sNy
         DO i = 1,sNx
          DO L = 1,Nrphys
             pcphy(i,j,L,bi,bj) = ( pephy(i,j,L,bi,bj)+
     &                     pephy(i,j,L+1,bi,bj) )/2.0
          ENDDO
         ENDDO
        ENDDO
C Build pressures on dynamics grid
        DO j = 1,sNy
         DO i = 1,sNx
          DO L = 1,Nr
           pedyn(i,j,L,bi,bj) = 0.
          ENDDO
         ENDDO
        ENDDO
        DO j = 1,sNy
         DO i = 1,sNx
          Lbotij = ksurfC(i,j,bi,bj)
          IF (Lbotij .NE. 0.)
     .       pedyn(i,j,Lbotij,bi,bj) = 
     .            (Ro_surf(i,j,bi,bj) + etaH(i,j,bi,bj))
         ENDDO
        ENDDO
        DO j = 1,sNy
         DO i = 1,sNx
          Lbotij = ksurfC(i,j,bi,bj)
          DO L = Lbotij+1,Nr+1
#ifdef NONLIN_FRSURF 
           IF ( select_rStar.NE.0 ) THEN
# ifndef DISABLE_RSTAR_CODE
               pedyn(i,j,L,bi,bj) = pedyn(i,j,L-1,bi,bj) -
     .           drF(L-1)* rStarExpC(i,j,bi,bj)*hfacC(i,j,L-1,bi,bj)
# endif /* DISABLE_RSTAR_CODE */
           ELSE
               pedyn(i,j,L,bi,bj) = pedyn(i,j,L-1,bi,bj) -
     .           drF(L-1)*hfacC(i,j,L-1,bi,bj)
           ENDIF
#else
           pedyn(i,j,L,bi,bj) = rF(L)
#endif /* NONLIN_FRSURF */
          ENDDO
c Do not use a zero field as the top edge pressure for interpolation
          IF (pedyn(i,j,Nr+1,bi,bj) .LT. 1.e-8)
     .                               pedyn(i,j,Nr+1,bi,bj) = 1.e-8
         ENDDO
        ENDDO
        DO j = 1,sNy
         DO i = 1,sNx
          Lbotij = ksurfC(i,j,bi,bj)
          DO L = Lbotij,Nr
             pcdyn(i,j,L,bi,bj) = ( pedyn(i,j,L,bi,bj)+
     &                     pedyn(i,j,L+1,bi,bj) )/2.0
          ENDDO
         ENDDO
        ENDDO

       ENDDO
      ENDDO

      CALL TIMER_START ('do_planetWRF          [WRF_WRAPPER]',mythid)
       DO bj = myByLo(myThid), myByHi(myThid)
        DO bi = myBxLo(myThid), myBxHi(myThid)
          CALL do_planetWRF(myTime, myIter, myThid, bi, bj)
        ENDDO
       ENDDO
      CALL TIMER_STOP  ('do_planetWRF          [WRF_WRAPPER]',mythid)

      CALL TIMER_START ('PHYS2DYN              [WRF_WRAPPER]',mythid)
      DO bj = myByLo(myThid), myByHi(myThid)
       DO bi = myBxLo(myThid), myBxHi(myThid)
c Interpolate (A-Grid) physics increments to dynamics grid
C   First flip the physics arrays (which are top-down)
C   into bottom-up arrays for interpolation to dynamics grid

C  radiative heating
        DO j = 1,sNy
         DO i = 1,sNx
          DO L = 1,Nrphys
             tempphy(i,j,L,bi,bj)=RTHRATEN(i,j,L,bi,bj)
          ENDDO
         ENDDO
        ENDDO
        CALL phys2dyn(tempphy,pephy,idim1,idim2,jdim1,jdim2,Nrphys,
     . Nsx,Nsy,im1,im2,jm1,jm2,bi,bj,pedyn,ksurfC,Nr,nlperdyn,gthraphy)

c       if PBL mixing is enabled, convert phys to dyn
        IF (wrf_pbl_physics) THEN
C         u tendency from PBL
          DO j = 1,sNy
           DO i = 1,sNx
            DO L = 1,Nrphys
               tempphy(i,j,L,bi,bj)=RUBLTEN(i,j,L,bi,bj)
            ENDDO
           ENDDO
          ENDDO
          CALL phys2dyn(tempphy,pephy,idim1,idim2,jdim1,jdim2,Nrphys,
     . Nsx,Nsy,im1,im2,jm1,jm2,bi,bj,pedyn,ksurfC,Nr,nlperdyn,gublphy)
C         v tendency from PBL
          DO j = 1,sNy
           DO i = 1,sNx
            DO L = 1,Nrphys
             tempphy(i,j,L,bi,bj)=RVBLTEN(i,j,L,bi,bj)
            ENDDO
           ENDDO
          ENDDO
          CALL phys2dyn(tempphy,pephy,idim1,idim2,jdim1,jdim2,Nrphys,
     . Nsx,Nsy,im1,im2,jm1,jm2,bi,bj,pedyn,ksurfC,Nr,nlperdyn,gvblphy)
C         heating from PBL
          DO j = 1,sNy
           DO i = 1,sNx
            DO L = 1,Nrphys
             tempphy(i,j,L,bi,bj)=RTHBLTEN(i,j,L,bi,bj)
            ENDDO
           ENDDO
          ENDDO
          CALL phys2dyn(tempphy,pephy,idim1,idim2,jdim1,jdim2,Nrphys,
     . Nsx,Nsy,im1,im2,jm1,jm2,bi,bj,pedyn,ksurfC,Nr,nlperdyn,gthblphy)
C        tracer tendencies from PBL and tracer cycle
#ifdef WRF_ENABLE_TRACERS
          DO iTracer = 1, PTRACERS_numInUse
           DO j = 1,sNy
            DO i = 1,sNx
             DO L = 1,Nrphys
              tempphy(i,j,L,bi,bj)=gTrphy(i,j,L,bi,bj,iTracer)
             ENDDO
            ENDDO
           ENDDO
           CALL phys2dyn(tempphy,pephy,idim1,idim2,jdim1,jdim2,Nrphys,
     . Nsx,Nsy,im1,im2,jm1,jm2,bi,bj,pedyn,ksurfC,Nr,nlperdyn,
     . gTrdyn(1-OLx,1-OLy,1,1,1,iTracer))
          ENDDO
#endif /* WRF_ENABLE_TRACERS */

c       end of PBL tendency conversion from phys to dyn
        ENDIF

       ENDDO
      ENDDO

      CALL TIMER_STOP  ('PHYS2DYN              [WRF_WRAPPER]',mythid)

      IF (wrf_pbl_physics) THEN
c Convert gublphy and gvblphy from A-grid to C-grid for use by dynamics
      CALL TIMER_START ('ATOC              [WRF_WRAPPER]',mythid)
      CALL AtoC(myThid,gublphy,gvblphy,maskC,idim1,idim2,jdim1,jdim2,Nr,
     .                        Nsx,Nsy,im1,im2,jm1,jm2,gublphy,gvblphy)
      CALL TIMER_STOP  ('ATOC              [WRF_WRAPPER]',mythid)
      ENDIF


C------ exchange to fill the halo regions
      CALL TIMER_START ('EXCHANGES        [WRF_WRAPPER]',mythid)
c Call the c-grid exchange routine to fill in the halo regions (dth)
      _EXCH_XYZ_RL(gthraphy,myThid)

      IF (wrf_pbl_physics) THEN
c Call the c-grid exchange routine to fill in the halo regions (du,dv)
      CALL exch_uv_xyz_RL(gublphy,gvblphy,.TRUE.,myThid)
c Call the a-grid exchange routine to fill in the halo regions (dth,ds)
      _EXCH_XYZ_RL(gthblphy,myThid)
      ENDIF

#ifdef WRF_ENABLE_TRACERS
c gTrdyn contains tracer tendencies due to PBL mixing and microphysics processes
c summed from physics grid to dynamics grid.
      DO iTracer = 1, PTRACERS_numInUse
       _EXCH_XYZ_RL(gTrdyn(1-OLx,1-OLy,1,1,1,iTracer),myThid)         
      ENDDO
#endif /* WRF_ENABLE_TRACERS */

C  Call the a-grid exchange routine to fill in the halo regions (pressure field)
      _EXCH_XYZ_RL(pcdyn,myThid)
      CALL EXCH_3D_RL(pedyn, Nr+1, myThid )

      CALL TIMER_STOP  ('EXCHANGES        [WRF_WRAPPER]',mythid)

C------ this part is specially designed to include the effect of CO2 cycle 
C------ that changes the surface pressure anomally "eta"


      RETURN
      END
