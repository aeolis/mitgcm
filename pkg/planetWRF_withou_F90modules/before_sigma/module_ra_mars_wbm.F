CWRF:MODEL_LAYER:PHYSICS
C
C  loop over the tiles nSx and nSy
C  The following routines are inside of the loops
C

C  USE module_ra_utils
C  USE module_ra_mars_common
#include "planetWRF_OPTIONS.h"

C  USE module_state_description, ONLY : VALVERDE_LOOKUP, &
C                                       VALVERDE_FIT
C  variables will be passed through arguments

C==================================================================
      SUBROUTINE WBMVIS(RTHRATEN,GSW,XLAT,XLONG,ALBEDO,HA,        
     &               ANGSLOPE,AZMSLOPE,                            
     &               DUST_ARRAY,DHR,diurnavg,                      
     &               rho_phy,T3D,
     &               QV3D,QC3D,QR3D,QI3D,QS3D,QG3D,
     &               P3D,PF3D,pi3D,dz8w,            
     &               R,CP,G,JULIAN,P2SI,                           
     &               hr_vis,hr_a_vis,                              
     &               CO2SNOW_THRESHOLD,co2ice,                     
     &               H2OICE_THRESHOLD,h2oice,                      
     &               sza0,sunfrac,                                 
     &               DECLIN,SOLCON,                                
     &               du_physics,                                   
     &               ids,ide, jds,jde, kds,kde,                    
     &               ims,ime, jms,jme, kms,kme,                    
     &               its,ite, jts,jte, kts,kte,
     &               myTime,myIter, myThid                        )

C------------------------------------------------------------------
      IMPLICIT NONE
C------------------------------------------------------------------
C     == Global variables ===
C     !INPUT/OUTPUT PARAMETERS:
C     == Routine arguments ==
C     myTime - Current time in simulation, to be used for updating Radtran
C     myIter - Current iteration number in simulation
C     myThid - Thread number for this instance of the routine.
      REAL*8 myTime
      INTEGER myIter
      INTEGER myThid

      INTEGER  ids,ide, jds,jde, kds,kde, 
     &         ims,ime, jms,jme, kms,kme, 
     &         its,ite, jts,jte, kts,kte


C------------------------------------------------------------------

C--  Input ---

      INTEGER  du_physics
      LOGICAL  diurnavg
      REAL*8      JULIAN,DECLIN,SOLCON

      REAL*8 P3D       (ims:ime,jms:jme,kms:kme)
      REAL*8 PF3D      (ims:ime,jms:jme,kms:kme)
      REAL*8 pi3D      (ims:ime,jms:jme,kms:kme)
      REAL*8 T3D       (ims:ime,jms:jme,kms:kme)
      REAL*8 QV3D      (ims:ime,jms:jme,kms:kme)
      REAL*8 QC3D      (ims:ime,jms:jme,kms:kme)
      REAL*8 QR3D      (ims:ime,jms:jme,kms:kme)
      REAL*8 QI3D      (ims:ime,jms:jme,kms:kme)
      REAL*8 QS3D      (ims:ime,jms:jme,kms:kme)
      REAL*8 QG3D      (ims:ime,jms:jme,kms:kme)
      REAL*8 rho_phy   (ims:ime,jms:jme,kms:kme)
      REAL*8 dz8w      (ims:ime,jms:jme,kms:kme)
      REAL*8 DUST_ARRAY(ims:ime,jms:jme,kms:kme)
      REAL*8 RTHRATEN  (ims:ime,jms:jme,kms:kme)

      REAL*8 XLAT (ims:ime,jms:jme)
      REAL*8 XLONG(ims:ime,jms:jme)

      REAL*8 ALBEDO  (ims:ime,jms:jme)
      REAL*8 ANGSLOPE(ims:ime,jms:jme)
      REAL*8 AZMSLOPE(ims:ime,jms:jme)

      REAL*8 HA     (ims:ime,jms:jme)
      REAL*8 co2ice (ims:ime,jms:jme)
      REAL*8 h2oice (ims:ime,jms:jme)
      REAL*8 sza0   (ims:ime,jms:jme)
      REAL*8 sunfrac(ims:ime,jms:jme)
C--   in/out --
      REAL*8 GSW    (ims:ime,jms:jme)
      
C--   Outputs --

      REAL*8 hr_vis  (ims:ime,jms:jme,kms:kme)
      REAL*8 hr_a_vis(ims:ime,jms:jme,kms:kme)
  
C--   Other varibles --

      REAL*8 R,CP,G,P2SI,CO2SNOW_THRESHOLD,H2OICE_THRESHOLD,DHR


C====Local Variables====
    
      REAL*8 TTEN2D(ims:ime,kms:kme)
      REAL*8 RHO02D(ims:ime,kms:kme)
      REAL*8 P2D   (ims:ime,kms:kme)
      REAL*8 PF2D  (ims:ime,kms:kme)
      REAL*8 DUST2D(ims:ime,kms:kme)
      REAL*8 DZ2D  (ims:ime,kms:kme)
      REAL*8 T2D   (ims:ime,kms:kme)
      REAL*8 QV2D  (ims:ime,kms:kme)
      REAL*8 QC2D  (ims:ime,kms:kme)
      REAL*8 QR2D  (ims:ime,kms:kme)
      REAL*8 QI2D  (ims:ime,kms:kme)
      REAL*8 QS2D  (ims:ime,kms:kme)
      REAL*8 QG2D  (ims:ime,kms:kme)
 
      REAL*8 hr_vis2d  (ims:ime,kms:kme)
      REAL*8 hr_a_vis2d(ims:ime,kms:kme)
 
      REAL*8 XLAT1D    (ims:ime)
      REAL*8 XLONG1D   (ims:ime)
      REAL*8 ALB1D     (ims:ime)
      REAL*8 GSW1D     (ims:ime)
      REAL*8 HA1D      (ims:ime)
      REAL*8 ALPHASLP1D(ims:ime)
      REAL*8 GAMMASLP1D(ims:ime)
      REAL*8 SZA01D    (ims:ime)
      REAL*8 SUNFRAC1D (ims:ime)

      REAL*8 JULDAYFRAC

      INTEGER I,J,K



C------------------------------------------------------------------
      DO J=jts,MIN(jte,jde-1)

       DO i=its,MIN(ite,ide-1)
          DO k=kts,kte
             TTEN2D(i,k)=0.
             T2D(i,k)   = T3D(i,J,k)
             P2D(i,k)   = P3D(i,J,k)
C            !PF is pressure at the full levels (p8w in the regular code)
             PF2D(i,k)  = PF3D(i,J,k)
             RHO02D(i,k)= rho_phy(i,J,k)
             DZ2D(i,k)  = dz8w(i,J,k)
             DUST2D(i,k)= DUST_ARRAY(i,J,k)
C             IF (P_QV >= P_FIRST_SCALAR) QV2D(i,k)=QV3D(i,k,J)
C             IF (P_QC >= P_FIRST_SCALAR) QC2D(i,k)=QC3D(i,k,J)
C             IF (P_QR >= P_FIRST_SCALAR) QR2D(i,k)=QR3D(i,k,J)
C             IF (P_QI >= P_FIRST_SCALAR) QI2D(i,k)=QI3D(i,k,J)
C             IF (P_QS >= P_FIRST_SCALAR) QS2D(i,k)=QS3D(i,k,J)
C             IF (P_QG >= P_FIRST_SCALAR) QG2D(i,k)=QG3D(i,k,J)
          END DO
          PF2D(i,kte+1)  = PF3D(i,J,kte+1)
          DUST2D(i,kte+1)= DUST_ARRAY(i,J,kte+1)
          XLAT1D(i)     = XLAT(i,J)
          XLONG1D(i)    = XLONG(i,J)
          ALB1D(i)      = ALBEDO(i,J)
C          ! CO2 takes precedence over H2O
          IF (co2ice(i,j) > CO2SNOW_THRESHOLD ) THEN
C             !ALB1D(i) = 0.60
C             ! New numbers based on VL pressure curve fits
             IF (XLAT1D(i) > 0.) THEN
                ALB1D(i) = 0.795      
C               ! North polar CO2 cap
             ELSE
                ALB1D(i) = 0.461      
C               ! South polar CO2 cap
             END IF
          ELSE IF (h2oice(i,j) >  H2OICE_THRESHOLD ) THEN
             ALB1D(i) = 0.25 !c-yl use 0.25 instead of 0.45 to match TSK 80N
          END IF
          HA1D(i)       = HA(i,J)
          ALPHASLP1D(i) = ANGSLOPE(i,J)
          GAMMASLP1D(i) = AZMSLOPE(i,J)
          sza01D(i)     = sza0(i,j)
          sunfrac1D(i)  = sunfrac(i,j)
       END DO

C       ! juldayfrac = fraction of a julian day (from midnight)

       JULDAYFRAC=JULIAN-INT(JULIAN)

       CALL WBMVIS2D(TTEN2D,GSW1D,XLAT1D,XLONG1D,ALB1D,T2D,
     &                QV2D,QC2D,QR2D,QI2D,QS2D,QG2D,
     &                P2D,PF2D,  
     &                hr_vis2d,hr_a_vis2d,                         
     &                diurnavg, sza01d, sunfrac1d,                 
     &                DUST2D,                                      
     &                HA1D, ALPHASLP1D, GAMMASLP1D,                
     &                DHR, RHO02D, DZ2D,                           
     &                R,CP,G,DECLIN,SOLCON,JULDAYFRAC,P2SI,        
     &                du_physics,                                  
     &                ims,ime,kms,kme,its,ite,kts,kte              )

       DO i=its,MIN(ite,ide-1)
          GSW(i,J) = GSW1D(i)
          DO k=kts,kte
             RTHRATEN(i,J,k) = RTHRATEN(i,J,k) + TTEN2D(i,k)/pi3D(i,J,k)
             hr_vis(i,J,k)   = hr_vis2d(i,k)
             hr_a_vis(i,J,k) = hr_a_vis2d(i,k)
          END DO
       END DO

      ENDDO 

      RETURN
      END

C==================================================================
      SUBROUTINE WBMVIS2D(TTEN,GSW,XLAT,XLONG,ALBEDO,T,
     &                 QV,QC,QR,QI,QS,QG,
     &                 P,PF,                    
     &                 hr_vis,hr_a_vis,                             
     &                 diurnavg, sza0, sunfrac,                     
     &                 DUST_ARRAY,                                  
     &                 HA, ALPHASLP, GAMMASLP,                      
     &                 DHR, RHO0, DZ,                               
     &                 R,CP,G,DECLIN,SOLCON,JULDAYFRAC,P2SI,        
     &                 du_physics,                                  
     &                 ims,ime,kms,kme,its,ite,kts,kte              )
C------------------------------------------------------------------
C
C     TO CALCULATE SHORT-WAVE ABSORPTION AND SCATTERING IN CLEAR
C     AIR AND REFLECTION AND ABSORPTION IN DUST LAYERS
C
C------------------------------------------------------------------
      IMPLICIT NONE
C--------------------------------------------------------------------

      INTEGER ims,ime, kms,kme, its,ite, kts,kte
      INTEGER du_physics

      REAL*8 RHO0(ims:ime, kms:kme)
      REAL*8 T   (ims:ime, kms:kme)
      REAL*8 P   (ims:ime, kms:kme)
      REAL*8 PF  (ims:ime, kms:kme)
      REAL*8 DZ  (ims:ime, kms:kme)
      REAL*8 QV  (ims:ime, kms:kme)
      REAL*8 QC  (ims:ime, kms:kme)
      REAL*8 QR  (ims:ime, kms:kme)
      REAL*8 QI  (ims:ime, kms:kme)
      REAL*8 QS  (ims:ime, kms:kme)
      REAL*8 QG  (ims:ime, kms:kme)
      REAL*8 DUST_ARRAY(ims:ime, kms:kme)

      REAL*8 TTEN    (ims:ime, kms:kme)
      REAL*8 hr_vis  (ims:ime, kms:kme)
      REAL*8 hr_a_vis(ims:ime, kms:kme)

      REAL*8 xlat    (ims:ime)
      REAL*8 xlong   (ims:ime)
      REAL*8 albedo  (ims:ime)
      REAL*8 ha      (ims:ime)
      REAL*8 alphaslp(ims:ime)
      REAL*8 gammaslp(ims:ime)
      REAL*8 sza0    (ims:ime)
      REAL*8 sunfrac (ims:ime)

      REAL*8 GSW(ims:ime)

      REAL*8 R,CP,G,DECLIN,SOLCON,JULDAYFRAC,P2SI,DHR

C                ALPHASLP = slope angle, GAMMASLP = slope azimuth


      LOGICAL diurnavg

C
C LOCAL VARS
C

      REAL*8 DLPI        (kms:kme)
      REAL*8 HSW         (kms:kme)
      REAL*8 HSW_DUST    (kms:kme)
      REAL*8 dust_array1d(kms:kme)

      REAL*8 JDAYFRAC, RADIN, TRANS, MU0, COSFAC, RXLAT, TSUNFRAC
      REAL*8 PI, dTdt_0, p0ref, pNLTE, r0
      REAL*8 radius, mubar

      INTEGER i, k, kk
C------------------------------------------------------------------

      PI     = ACOS(-1.)
      dTdt_0 = 1.3/(86400.*P2SI) ! K/s (SI seconds...)
      p0ref  = 700.              ! Pa
      pNLTE  = 0.0075            ! Pa
      r0     = 1.52              ! AU

C    ! Instantaneous distance from the Sun to Mars, in AU
C    !
C    ! Rather than redo the integration of Kepler's equation, take
C    ! advantage of the fact that we have as in input to this subroutine
C    ! the local solar constant which is just the universal solar constant
C    ! (1367.6 W/m^2 at 1 AU) divided by the radius squared.
      radius = SQRT(1367.6/SOLCON)

      DO i = its, ite

       GSW(i)           = 0.
       DO k=kts,kte
          tten(i,k)     = 0.
          hr_vis(i,k)   = 0.
          hr_a_vis(i,k) = 0.
          HSW(k)        = 0.
          hsw_dust(k)   = 0.
       END DO

C       ! mu0 = COS(zenith angle of the sun)
C       ! tsunfrac = fraction of the timestep that the sun is up 

       IF (diurnavg) THEN
          mu0 = sza0(i)
          tsunfrac = sunfrac(i)
       ELSE
          CALL ZENITH(JULDAYFRAC, DHR, DECLIN, XLAT(i), XLONG(i), HA(i),
     &                MU0, TSUNFRAC)
       END IF
       RADIN = SOLCON*TSUNFRAC

C       ! Sample values for testing code
C       !  MU0=0.
C       !  TSUNFRAC = HA / PI

C       ! Don't do any of the shortwave code if the sun isn't up! (mu0 < 0)

C         ! roundoff error
          IF( MU0 > 0.00001) THEN 

          mubar = SQRT((1224.*mu0*mu0+1.)/1225.)

          DO k=kts,kte
             DLPI(k)=PF(i,k)-PF(i,k+1)
             DLPI(k)=1./DLPI(k)
          END DO

C          ! HSW is in K/s
          DO k = kts, kte
             HSW(k) = dTdt_0*((r0/radius)**2) * 
     &                SQRT(mubar*p0ref/p(i,k)) / 
     &                 (1. + pNLTE/p(i,k))
          END DO

C          ! Now adjust for slopes for surface
C
C          ! If not considering slopes, use the following line and don't
C          ! call the subroutine SWSLOPE
C          !cosfac = mu0
          CALL SWSLOPE(cosfac, mu0, juldayfrac, declin, xlat(i), 
     &                 xlong(i), alphaslp(i), gammaslp(i))

          DO k=kts,kte+1   ! note scatter requires z array to run in op dirn
             kk=kte+1-k+kts
             dust_array1d(kk) = dust_array(i,k)
          ENDDO

          CALL SW_AEROSOL_SCATTER(MU0,albedo(i),dust_array1d,
     &                            HSW_DUST,TRANS, kts,kte)

C          ! If dust is turned off for some reason, This definition of "trans"
C          ! will make GSW be calculated properly, as F_solar*(1-albedo)
          IF (du_physics == 0) trans=1.-albedo(i)

          GSW(i)=RADIN*COSFAC*TRANS

          DO K=kts, kte
C             ! Scatter has vertical levels in MM5 order,
C             ! so HSW_DUST needs to be reversed
             kk=kte-k+kts
             hr_a_vis(i,k) = (G/CP)*RADIN*MU0*HSW_DUST(kk)*DLPI(K)
             hr_vis(i,k)   = HSW(k)
             TTEN(I,K) = hsw(k)+hr_a_vis(i,k)
          END DO

       ENDIF  
C      ! mu0 > 0, sun is up 

      END DO


      RETURN
      END 

C==================================================================
      SUBROUTINE WBMIR(RTHRATEN,GLW,XLAT,XLONG,TSK,DUST_ARRAY,        
     &              rho_phy,T3D,
     &              QV3D,QC3D,QR3D,QI3D,QS3D,QG3D,
     &              P3D,PF3D,pi3D,dz8w,R,CP,G,                     
     &              hr_ir,hr_a_ir,FNM,FNP,                         
     &              isothermal_top,nlte_physics,                   
     &              ids,ide, jds,jde, kds,kde,                      
     &              ims,ime, jms,jme, kms,kme,                     
     &              its,ite, jts,jte, kts,kte,
     &              myTime,myIter, myThid                      ) 

C------------------------------------------------------------------
      IMPLICIT NONE
C------------------------------------------------------------------
C     == Global variables ===
C#include "SIZE.h"
C     !INPUT/OUTPUT PARAMETERS:
C     == Routine arguments ==
C     myTime - Current time in simulation, to be used for updating Radtran
C     myIter - Current iteration number in simulation
C     myThid - Thread number for this instance of the routine.
      REAL*8 myTime
      INTEGER myIter
      INTEGER myThid


      INTEGER ids,ide, jds,jde, kds,kde, 
     &        ims,ime, jms,jme, kms,kme, 
     &        its,ite, jts,jte, kts,kte
      LOGICAL isothermal_top
      INTEGER nlte_physics

      REAL*8 P3D       (ims:ime, jms:jme, kms:kme)
      REAL*8 PF3D      (ims:ime, jms:jme, kms:kme)
      REAL*8 pi3D      (ims:ime, jms:jme, kms:kme)
      REAL*8 T3D       (ims:ime, jms:jme, kms:kme)
      REAL*8 QV3D      (ims:ime, jms:jme, kms:kme)
      REAL*8 QC3D      (ims:ime, jms:jme, kms:kme)
      REAL*8 QR3D      (ims:ime, jms:jme, kms:kme)
      REAL*8 QI3d      (ims:ime, jms:jme, kms:kme)
      REAL*8 QS3D      (ims:ime, jms:jme, kms:kme)
      REAL*8 QG3D      (ims:ime, jms:jme, kms:kme)
      REAL*8 rho_phy   (ims:ime, jms:jme, kms:kme)
      REAL*8 dz8w      (ims:ime, jms:jme, kms:kme)
      REAL*8 DUST_ARRAY(ims:ime, jms:jme, kms:kme)

      REAL*8 RTHRATEN( ims:ime, jms:jme, kms:kme )              
      REAL*8 hr_ir   ( ims:ime, jms:jme, kms:kme )
      REAL*8 hr_a_ir ( ims:ime, jms:jme, kms:kme )                  
      REAL*8 XLAT    ( ims:ime, jms:jme )                           
      REAL*8 XLONG   ( ims:ime, jms:jme )
      REAL*8 TSK     ( ims:ime, jms:jme )

      REAL*8 FNM( ims:ime, jms:jme, kms:kme )  
      REAL*8 FNP( ims:ime, jms:jme, kms:kme )

      REAL*8 GLW( ims:ime, jms:jme )                           

      REAL*8 R,CP,G

C
C LOCAL VARS
C

      REAL*8 TTEN2D( ims:ime, kms:kme )
      REAL*8 RHO02D( ims:ime, kms:kme )
      REAL*8 P2D   ( ims:ime, kms:kme )
      REAL*8 PF2D  ( ims:ime, kms:kme )
      REAL*8 DZ2D  ( ims:ime, kms:kme )
      REAL*8 T2D   ( ims:ime, kms:kme )
      REAL*8 QV2D  ( ims:ime, kms:kme )
      REAL*8 QC2D  ( ims:ime, kms:kme )
      REAL*8 QR2D  ( ims:ime, kms:kme )
      REAL*8 QI2D  ( ims:ime, kms:kme )
      REAL*8 QS2D  ( ims:ime, kms:kme )
      REAL*8 QG2D  ( ims:ime, kms:kme )
      REAL*8 DUST2D( ims:ime, kms:kme )
      REAL*8 hr_ir2d  ( ims:ime, kms:kme )
      REAL*8 hr_a_ir2d( ims:ime, kms:kme )    
      REAL*8 FNM2D ( ims:ime, kms:kme ) 
      REAL*8 FNP2D ( ims:ime, kms:kme )

      REAL*8 TSK1D  ( ims:ime )
      REAL*8 GLW1D  ( ims:ime )
      REAL*8 XLAT1D ( ims:ime )
      REAL*8 XLONG1D( ims:ime )

      INTEGER I,J,K

C------------------------------------------------------------------

      DO J=jts,jte

       DO i=its,ite
          DO k=kts,kte
             T2D(i,k)    = T3D(i,J,k)
             P2D(i,k)    = P3D(i,J,k)
             RHO02D(i,k) = rho_phy(i,J,k)
             DZ2D(i,k)   = dz8w(i,J,k)
             PF2D(i,k)   = PF3D(i,J,k)
             DUST2D(i,k) = DUST_ARRAY(i,J,k)
             FNM2D(i,k)  = FNM(i,J,k)
             FNP2D(i,k)  = FNP(i,J,k)
C             IF (P_QV >= P_FIRST_SCALAR) QV2D(i,k)=QV3D(i,k,J)
C             IF (P_QC >= P_FIRST_SCALAR) QC2D(i,k)=QC3D(i,k,J)
C             IF (P_QR >= P_FIRST_SCALAR) QR2D(i,k)=QR3D(i,k,J)
C             IF (P_QI >= P_FIRST_SCALAR) QI2D(i,k)=QI3D(i,k,J)
C             IF (P_QS >= P_FIRST_SCALAR) QS2D(i,k)=QS3D(i,k,J)
C             IF (P_QG >= P_FIRST_SCALAR) QG2D(i,k)=QG3D(i,k,J)
          END DO
          PF2D(i,kte+1)   = PF3D(i,J,kte+1)
          DUST2D(i,kte+1) = DUST_ARRAY(i,J,kte+1)
          XLAT1D(i)  = XLAT(i,J)
          XLONG1D(i) = XLONG(i,J)
          TSK1D(i)   = TSK(i,J)
       END DO

       CALL WBMIR2D(TTEN2D,GLW1D,TSK1D,XLAT1D,XLONG1D,T2D,
     &              QV2D,QC2D,QR2D,QI2D,QS2D,QG2D,
     &              P2D,PF2D, 
     &              DUST2D,FNM2D,FNP2D,hr_ir2d,hr_a_ir2d,           
     &              RHO02D,DZ2D,R,CP,G,isothermal_top,          
     &              nlte_physics,                               
     &              ims,ime,kms,kme,its,ite,kts,kte             )

       DO i=its,ite
          DO k=kts,kte
             hr_ir(i,J,k)    = hr_ir2d(i,k)
             hr_a_ir(i,J,k)  = hr_a_ir2d(i,k)
             RTHRATEN(i,J,k) = RTHRATEN(i,J,k) + TTEN2D(i,k)/pi3D(i,J,k)
          END DO
          GLW(i,j) = GLW1D(i)
       END DO

      ENDDO                         

      RETURN
      END 

C==================================================================
      SUBROUTINE WBMIR2D(TTEN,GLW,TG,XLAT,XLONG,T,
     &                QV,QC,QR,QI,QS,QG,
     &                P,PF,                    
     &                DUST_ARRAY,FNM,FNP,hr_ir,hr_a_ir,            
     &                RHO0,DZ,R,CP,G,isothermal_top,nlte_physics,  
     &                ims,ime,kms,kme,its,ite,kts,kte              )
C------------------------------------------------------------------
C
C     TO CALCULATE LONG WAVE RADIATION
C
C------------------------------------------------------------------
      IMPLICIT NONE
C--------------------------------------------------------------------
      INTEGER ims,ime, kms,kme, its,ite, kts,kte
      LOGICAL isothermal_top
      INTEGER nlte_physics

      REAL*8 RHO0       ( ims:ime, kms:kme )
      REAL*8 T          ( ims:ime, kms:kme )
      REAL*8 P          ( ims:ime, kms:kme )
      REAL*8 PF         ( ims:ime, kms:kme )
      REAL*8 DZ         ( ims:ime, kms:kme )
      REAL*8 QV         ( ims:ime, kms:kme )
      REAL*8 QC         ( ims:ime, kms:kme )
      REAL*8 QR         ( ims:ime, kms:kme )
      REAL*8 QI         ( ims:ime, kms:kme )
      REAL*8 QS         ( ims:ime, kms:kme )
      REAL*8 QG         ( ims:ime, kms:kme )
      REAL*8 DUST_ARRAY ( ims:ime, kms:kme )

      REAL*8 TTEN( ims:ime, kms:kme )

      REAL*8 hr_ir  ( ims:ime, kms:kme )
      REAL*8 hr_a_ir( ims:ime, kms:kme )
    
      REAL*8 XLAT ( ims:ime )
      REAL*8 XLONG( ims:ime )
      REAL*8 TG   ( ims:ime )

      REAL*8 FNM( ims:ime, kms:kme )
      REAL*8 FNP( ims:ime, kms:kme )

      REAL*8 GLW( ims:ime )


      REAL*8 R,CP,G

C
C   Local Variables
C
      REAL*8 HR_DUST    ( kms:kme )
      REAL*8 HR_CO2     ( kms:kme )
      REAL*8 HR_CO2_NLTE( kms:kme )
      REAL*8 T1D        ( kms:kme )
      REAL*8 P1D        ( kms:kme )
      REAL*8 PF1D       ( kms:kme )
      REAL*8 DLPI       ( kms:kme )
      REAL*8 DLPIT      ( kms:kme )
      REAL*8 DUST1D     ( kms:kme )

      REAL*8 bsource    ( kms:kme )
      REAL*8 b_diff     ( kms:kme )

      REAL*8 td1        ( kms:kme )
      REAL*8 tu1        ( kms:kme )
      REAL*8 fup        ( kms:kme )
      REAL*8 fdn        ( kms:kme )
      REAL*8 net_flux   ( kms:kme )
      REAL*8 dep        ( kms:kme,kms:kme )
      REAL*8 td         ( kms:kme,kms:kme )
C vertical variables in the NLTE scheme 
      REAL*8 z_lv       ( kms:kme )
      REAL*8 sigma_lv   ( kms:kme )
      REAL*8 bsource_surf, bsurf_diff, sflx_co2, sflx_dust

      REAL*8 GOVCP, PI

C    ! Altitude of the NLTE transition region in terms of -ln(p),
C    ! where p is in nbars

      REAL*8  zs_lv, zw_lv, pa2bar, bar2pa      

C    ! Width of the NLTE transition region. The above value of zs_lv
C    ! corresponds to approximately 82 km for reference atmosphere 1 in L.-V.

C    ! Pa to bars conversion factor
C    ! bars to Pa conversion factor
      PARAMETER (zs_lv = -5.5)
      PARAMETER (zw_lv =  0.5)
      PARAMETER (pa2bar=1./101325.)
      PARAMETER (bar2pa=101325.)

      INTEGER i, k, kk

      REAL*8 PLANCK_FUNCTION
 
      GOVCP = G/CP

      PI = ACOS(-1.0)
    
      DO i = its, ite

       DO K=kts, kte
          TTEN(i,k) = 0.
          DLPI(K)   = PF(i,K)-PF(i,K+1)
          DLPI(K)   = 1./DLPI(K)
          p1d(k)    = p(i,k)
          pf1d(k)   = pf(i,k)
          t1d(k)    = t(i,k)
       END DO
       pf1d(kte+1)  = pf(i,kte+1)

       DO k = kts, kte
          bsource(k+1) = PLANCK_FUNCTION(t1d(k))
       END DO
       bsource(kts)    = bsource(kts+1)
       bsource_surf    = PLANCK_FUNCTION(tg(i))

       DO k=kts,kte
          b_diff(k) = bsource(k+1) - bsource(k)
       END DO
       bsurf_diff   = bsource_surf - bsource(kts)

       CALL CO2_TRANS_HOURDIN(p1d, pf1d, t1d,    
     &                         tu1, td1, td, g,   
     &                         kts, kte, kms, kme )

C       ! flux boundary conditions:
       fup(kts)   = bsource_surf
       fdn(kte+1) = 0.0

       DO k = kts+1, kte
         fup(k) = bsurf_diff    *tu1(k) + bsource(k)
         fdn(k) = bsource(kte+1)*td1(k) - bsource(k)
       END DO

       DO k = kts+1, kte
          DO kk = kts+2, k
             fup(k) = fup(k) - b_diff(kk-1)*td(kk-1,k)
          END DO
          fup(k)    = fup(k) - b_diff(k)*0.125*(td(k-1,k)+3.0)

          DO kk= k+2, kte+1
            fdn(k)  = fdn(k) - b_diff(kk-1)*td(kk-1,k)
          END DO
          fdn(k)    = fdn(k) - b_diff(k)*0.125*(td(k+1,k)+3.0)
       END DO

C       ! Upward flux at the top boundary:
       fup(kte+1) =  bsurf_diff*tu1(kte+1) + bsource(kte+1)
       DO k = kts+1, kte
          fup(kte+1) = fup(kte+1) - b_diff(k)*td(k,kte+1)
       END DO

C       ! Downward flux at the bottom boundary
       fdn(kts) = bsource(kte+1)*td1(kts) - bsource(kts+1)
       DO k = kts+1, kte
          fdn(kts) = fdn(kts) - b_diff(k)*td(k,kts)
       END DO

       DO k=kts,kte+1
          net_flux(k) = fup(k) + fdn(k)
       END DO

       DO k = kts, kte
          hr_co2(k)=govcp*(net_flux(k)-net_flux(k+1))*dlpi(k)
       ENDDO

       sflx_co2= -fdn(kts)

       ! ---------------------------------------------------------------------
       ! Non Local Thermodynamic Equilibrium (NLTE) adjustment to LW flux

       IF (nlte_physics == 41) THEN
          ! calculate NLTE CO2 cooling rates between 40 and 140 km
          ! using look up tables, VALVERDE_LOOKUP=41
           CALL VALVERDE_CO2_IR_LOOKUP(hr_co2_nlte,pf1d,
     &                                t1d,kms,kme,kts,kte)
          ! calculate NLTE CO2 cooling rates between 40 and 140 km
          ! using analytical fit to table data, VALVERDE_FIT=42
       ELSEIF (nlte_physics == 42) THEN
             CALL VALVERDE_CO2_IR_FIT(hr_co2_nlte,pf1d,
     &                                t1d,kms,kme,kts,kte)
       ELSE
             !WRITE(0,*) 'NLTE_PHYSICS option not found'
             DO k = kts, kte
                hr_co2_nlte(k) = 0.
             END DO
       ENDIF

       ! Combine the LTE and NLTE cooling rates to yield a contnuous profile.
       ! LTE is valid from 0 to about 60 km, above that NLTE becomes important.
CYL only apply NLTE effect when nlte_physics presents
       IF ((nlte_physics==41).or.(nlte_physics ==42)) THEN
           DO k=kts,kte
          ! z_lv is the vertical variable in the original paper
          ! The expression in parenthesis has to be in nanobars
              z_lv(k) = -log(pf1d(k)*pa2bar*1E9)
          ! The blending coefficient (a function of height)
              sigma_lv(k) = 0.5*(1.+tanh((z_lv(k)-zs_lv)/zw_lv))
              hr_co2(k) = hr_co2(k)*(1.-sigma_lv(k)) 
     &                  + hr_co2_nlte(k)*sigma_lv(k)
           END DO
        ENDIF
       ! --------------------------------------------------------------------
C       ! Dust goes here

C       ! LW_AEROSOL_HEAT has vertical indices in MM5 order
       DO k=kts,kte
          kk=kte-k+kts
          t1d(kk)   = t(i,k)
          dlpit(kk) = dlpi(k)
       END DO
       DO k=kts,kte+1
          kk=kte+1-k+kts
          dust1d(kk) = dust_array(i,k)
       END DO
       CALL LW_AEROSOL_HEAT(dlpit, t1d, TG(i), dust1d, HR_DUST, 
     &                      SFLX_DUST, G, CP, kts, kte)

       DO k=kts,kte
          kk=kte-k+kts
          TTEN(i,k)    = HR_CO2(k) + HR_DUST(kk)
          hr_ir(i,k)   = HR_CO2(k)
          hr_a_ir(i,k) = HR_DUST(kk)
C          print *, 'dust',HR_DUST(kk)
       END DO
       GLW(i)         = SFLX_CO2 + SFLX_DUST

      ENDDO

      RETURN
      END

C====================================================================
      REAL*8 FUNCTION PLANCK_FUNCTION(t)
C--------------------------------------------------------------------
      IMPLICIT NONE
C--------------------------------------------------------------------
      REAL*8  t, h, k, c
      REAL*8 planck

      PARAMETER (h = 6.62607E-34) ! J-sec
      PARAMETER (k = 1.38065E-23) ! J/K
      PARAMETER (c = 2.99792E+8) ! m/s

      REAL*8  nu, nu3, dnu, c1, arg
      REAL*8  pi

C result will have units of W/m^2

      pi = ACOS(-1.)

      nu  = 64500.        ! m^-1
      nu3 = nu*nu*nu      ! m^-3
      c1  = 2.*pi*h*c*c   ! J m^2 s^-1
      arg = h*c*nu/k      ! K
      dnu = 86500.-50000. ! m^-1

      planck = c1*nu3*dnu/(EXP(arg/t)-1.)
C--   use function name to pass result (F77 only)
      PLANCK_FUNCTION = planck  

      RETURN
      END

C====================================================================
      SUBROUTINE co2_trans_hourdin( pp, pph, t, tu1, td1, td, g, 
     &                           kts, kte, kms, kme)
C--------------------------------------------------------------------
      IMPLICIT NONE
C--------------------------------------------------------------------
C   pph = pressure at full sigma levels (i.e., borders of vertical levels)
C   pp  = pressure at half sigma levels (i.e., centers of vertical levels)

      INTEGER kts, kte, kms, kme
      REAL*8 pp(kms:kme), pph(kms:kme), t(kms:kme)
      REAL*8 tu1(kms:kme), td1(kms:kme)
      REAL*8 td (kms:kme,kms:kme)
      REAL*8 g

      REAL*8 tzero, diffrac, p0ref, bwt1, bwt2, bwt3
      REAL*8 c1c, c2c, c1w, c2w
      REAL*8 acent(1:4), awing(1:4),
     &    pcoef(1:5), pdcoef(1:5), pcoefw(1:5), pdcoefw(1:5)

      PARAMETER (tzero   = 200.)    ! K
      PARAMETER (diffrac = 1.66)    ! Unitless
      PARAMETER (p0ref   = 1.013E+5) ! Pa
      PARAMETER (bwt1    = 135./365.) ! Fractional part of band
      PARAMETER (bwt2    =  70./365.) ! Fractional part of band
      PARAMETER (bwt3    = 160./365.) ! Fractional part of band
C      ! c1 and c2 have units!!!!  See below for proper use/definition
      PARAMETER (c1c = 0.005)
      PARAMETER (c2c = 0.01)
      PARAMETER (c1w = 0.015)
      PARAMETER (c2w = 0.1)
      DATA acent /0.694E-1,  0.328E-3, 0.544E-2,  
     &            0.596E-5 /
      DATA awing /0.275E-1, -0.705E-3, 0.356E-1, 
     &           -0.326E-4 /
      DATA pcoef /2.882E-5, 1.708E-2, -3.397E-2, 
     &            1.454E-2, 5.438E-1 /
      DATA pdcoef /2.924E-5, 1.732E-2, -3.442E-2, 
     &             1.475E-2, 5.511E-1/
      DATA pcoefw /2.893E-2, 1.906, 3.841, 1.895, 6.004/
      DATA pdcoefw /2.832E-2, 1.893, 3.792, 1.881, 5.977/

      INTEGER k, kk
      
      REAL*8 delp  ( kms:kme )
      REAL*8 ubar  ( kms:kme )
      REAL*8 upbar ( kms:kme )
      REAL*8 ubarw ( kms:kme )
      REAL*8 upbarw( kms:kme ) 
      REAL*8 factor_1, factor_2, exf1, exf2, ueq, ueqw
      REAL*8 udiff, updiff, updiffw, udiffw, tdw, tdc
      REAL*8 deltat

      REAL*8 PADE_EVAL

      factor_1= diffrac/g
      factor_2= factor_1/p0ref

      DO k=kts,kte
         delp(k) = pph(k) - pph(k+1)
      END DO
      
C    ! Integrate downwards to obtain path lengths
      ubar  (kte+1) = 0.
      upbar (kte+1) = 0.
      ubarw (kte+1) = 0.
      upbarw(kte+1) = 0.

      DO k= kte, kts, -1
         deltat = t(k) - tzero

C       ! Calculate path lengths in the band center:
       exf1 = EXP( deltat*( acent(1) + acent(2)*deltat ) )
       exf2 = EXP( deltat*( acent(3) + acent(4)*deltat ) )

       ubar(k)  = ubar(k+1)  + factor_1*exf1 * delp(k)
       upbar(k) = upbar(k+1) + factor_2*exf2 * delp(k)*pp(k)

C       ! Repeat for the wings:                 
       exf1 = EXP( deltat*( awing(1) + awing(2)*deltat ) )
       exf2 = EXP( deltat*( awing(3) + awing(4)*deltat ) )

       ubarw(k)  = ubarw(k+1)  + factor_1*exf1 * delp(k)
       upbarw(k) = upbarw(k+1) + factor_2*exf2 * delp(k)*pp(k)

      END DO

      td1(kte+1) = 1.
      tu1(kts)   = 1.

C    ! Downward paths:
C    ! use top temperature for pade coeffs
      DO k= kts, kte
C       ! equivalent absorber amount:
C       ! The c1, c2 constants are valid when u[p]bar[w] are in g/cm^2 and 
C       ! our u[p]bar[w] are in kg/m^2. Conversion factor: 1 g/cm^2 = 10 kg/m^2
       ueq  = SQRT( 0.1* upbar(k) ) + c1c*( (0.1*ubar (k))**c2c )
       ueqw = SQRT( 0.1*upbarw(k) ) + c1w*( (0.1*ubarw(k))**c2w )

C       ! Pade approximants:  
       tdc = PADE_EVAL(pcoef,  ueq )
       tdw = PADE_EVAL(pcoefw, ueqw)

C       ! Finally, combine with Planck function weighting:
       td1(k)= tdc*bwt2 + tdw*(bwt1+bwt3)
      END DO

C    ! Upward paths:
C    ! Use surface temperature for pade coeffs
      DO k = kts+1, kte+1
       updiff = upbar(kts) - upbar(k)
       udiff  = ubar(kts)  - ubar(k)
C       ! The c1, c2 constants are valid when u[p]bar[w] are in g/cm^2 and 
C       ! our u[p]bar[w] are in kg/m^2. Conversion factor: 1 g/cm^2 = 10 kg/m^2
       ueq    = SQRT( 0.1*updiff ) + c1c*((0.1*udiff)**c2c)
C       ! equivalent absorber amount:
       updiff = upbarw(kts) - upbarw(k)
       udiff  = ubarw(kts)  - ubarw(k)
C       ! The c1, c2 constants are valid when u[p]bar[w] are in g/cm^2 and 
C       ! our u[p]bar[w] are in kg/m^2. Conversion factor: 1 g/cm^2 = 10 kg/m^2
       ueqw   = SQRT( 0.1*updiff ) + c1w*((0.1*udiff)**c2w)

C       ! Pade approximants:
       tdc = PADE_EVAL( pcoef , ueq  )
       tdw = PADE_EVAL( pcoefw, ueqw )

C       ! Finally, combine with Planck function weighting:
       tu1(k)= tdc*bwt2 + tdw*( bwt1+bwt3 )
      END DO

C    ! ----------------  Differential Paths  ----------------:

      DO k = kts, kte+1
         td(k,k)= 1.0
      END DO

      DO k = kts, kte+1
       DO kk = k+1, kte+1
C          ! Center:
          udiff  = ubar(k)  -  ubar(kk)
          updiff = upbar(k) - upbar(kk)
C          ! The c1, c2 constants are valid when u[p]bar[w] are in g/cm^2 and 
C          ! our u[p]bar[w] are in kg/m^2. Conversion factor: 1 g/cm^2=10 kg/m^2
          ueq  = SQRT( 0.1*updiff ) + c1c*((0.1*udiff)**c2c)
C          ! Contribution from the Wings:  
          udiffw  =  ubarw(k) -  ubarw(kk)
          updiffw = upbarw(k) - upbarw(kk) 
C          ! The c1, c2 constants are valid when u[p]bar[w] are in g/cm^2 and 
C          ! our u[p]bar[w] are in kg/m^2. Conversion factor: 1 g/cm^2=10 kg/m^2
          ueqw = SQRT( 0.1*updiffw )+ c1w*((0.1*udiffw)**c2w)

C          ! Pade approximants:  
          tdc = PADE_EVAL( pdcoef,  ueq )
          tdw = PADE_EVAL( pdcoefw, ueqw)

C          ! Planck function weighting:  
          td(k,kk)= tdc*bwt2 + tdw*(bwt1+bwt3)
C          ! td is symmetric
          td(kk,k)= td(k,kk)
       END DO
      END DO


      RETURN
      END 

C!====================================================================
      REAL*8 FUNCTION pade_eval( pc, ueq ) 
C!--------------------------------------------------------------------
      IMPLICIT NONE
C!--------------------------------------------------------------------
      REAL*8 pc(1:5)
      REAL*8 ueq, td

      REAL*8 top, denom

C    ! Pade approximants:  
      top   = pc(1) + ueq*( pc(2) + ueq*(pc(3)      ) )
      denom = pc(1) + ueq*( pc(4) + ueq*(pc(5) + ueq) )
      td    = top / denom

C--   use function name to pass result (F77 only)
      pade_eval = td     

      RETURN
      END 

C!==================================================================
      SUBROUTINE wbmlwinit(RTHRATENLW, restart,          
     &                  ids, ide, jds, jde, kds, kde, 
     &                  ims, ime, jms, jme, kms, kme, 
     &                  its, ite, jts, jte, kts, kte )
C!--------------------------------------------------------------------
      IMPLICIT NONE
C!--------------------------------------------------------------------
      LOGICAL restart
      INTEGER ids, ide, jds, jde, kds, kde,  
     &        ims, ime, jms, jme, kms, kme,  
     &        its, ite, jts, jte, kts, kte
      
      REAL*8 RTHRATENLW( ims:ime, jms:jme, kms:kme )
      INTEGER i, j, k, itf, jtf, ktf

      jtf=MIN(jte,jde-1)
      ktf=MIN(kte,kde-1)
      itf=MIN(ite,ide-1)

      IF(.not.restart)THEN
       DO j=jts,jtf
       DO k=kts,ktf
       DO i=its,itf
          RTHRATENLW(i,j,k)=0.
       END DO
       END DO
       END DO
      END IF

      RETURN
      END 

C!==================================================================
      SUBROUTINE wbmswinit(RTHRATEN, RTHRATENSW, restart, 
     &                  ids, ide, jds, jde, kds, kde,  
     &                  ims, ime, jms, jme, kms, kme,  
     &                  its, ite, jts, jte, kts, kte  )
C!--------------------------------------------------------------------
      IMPLICIT NONE
C!--------------------------------------------------------------------
      LOGICAL restart
      INTEGER ids, ide, jds, jde, kds, kde,  
     %         ims, ime, jms, jme, kms, kme,  
     %         its, ite, jts, jte, kts, kte

      REAL*8 RTHRATEN  ( ims:ime, jms:jme, kms:kme )
      REAL*8 RTHRATENSW( ims:ime, jms:jme, kms:kme )
      INTEGER i, j, k, itf, jtf, ktf

      jtf=MIN(jte,jde-1)
      ktf=MIN(kte,kde-1)
      itf=MIN(ite,ide-1)

      IF(.not.restart)THEN
       DO j=jts,jtf
       DO k=kts,ktf
       DO i=its,itf
          RTHRATEN(i,j,k)=0.
          RTHRATENSW(i,j,k)=0.
       END DO
       END DO
       END DO
      END IF

      RETURN
      END

C!==================================================================

