C!WRF:DRIVER_LAYER:UTIL
C!

CMODULE module_wrf_error
C  INTEGER           :: wrf_debug_level = 0
C  CHARACTER*256     :: wrf_err_message
CCONTAINS


C      LOGICAL FUNCTION wrf_at_debug_level ( level )
C      IMPLICIT NONE
C      INTEGER level
C         wrf_at_debug_level = ( level .LE. wrf_debug_level )
C      RETURN
C      END

      SUBROUTINE init_module_wrf_error
      END

CEND MODULE module_wrf_error

      SUBROUTINE wrf_message( str )
         IMPLICIT NONE
         CHARACTER*(*) str
#if defined( DM_PARALLEL ) && ! defined( STUBMPI) 
         write(0,*) TRIM(str)
#endif
         print*, TRIM(str)
      END

C! intentionally write to stderr only
      SUBROUTINE wrf_message2( str )
         IMPLICIT NONE
         CHARACTER*(*) str
         write(0,*) str
      END

      SUBROUTINE wrf_error_fatal3( file_str, line, str )
C     USE module_wrf_error
         IMPLICIT NONE
         CHARACTER*(*) file_str
         INTEGER line  ! only print file and line if line > 0
         CHARACTER*(*) str
         CHARACTER*256 line_str

         write(line_str,'(i6)') line
#if defined( DM_PARALLEL ) && ! defined( STUBMPI )
         CALL wrf_message( '----------- FATAL CALLED ------------' )
C  ! only print file and line if line is positive
         IF ( line > 0 ) THEN
         CALL wrf_message( 'FATAL CALLED FROM FILE:  '//file_str//'  
     &                     LINE:  '//TRIM(line_str) )
         ENDIF
         CALL wrf_message( str )
         CALL wrf_message( '----------------------------------------' )
#else
         CALL wrf_message2( '----------- FATAL CALLED ------------' )
C  ! only print file and line if line is positive
         IF ( line > 0 ) THEN
         CALL wrf_message( 'FATAL CALLED FROM FILE:  '//file_str//'  
     &                     LINE:  '//TRIM(line_str) )
         ENDIF
         CALL wrf_message2( str )
         CALL wrf_message2( '----------------------------------------' )
#endif
C!  CALL wrf_abort
         STOP
      END

      SUBROUTINE wrf_error_fatal( str )
C     USE module_wrf_error
      IMPLICIT NONE
      CHARACTER*(*) str
      CALL wrf_error_fatal3 ( ' ', 0, str )
      END 

! Check to see if expected value == actual value
! If not, print message and exit.  
      SUBROUTINE wrf_check_error( expected, actual, str, 
     &                            file_str, line )
C     USE module_wrf_error
         IMPLICIT NONE
         INTEGER expected
         INTEGER actual
         CHARACTER*(*) str
         CHARACTER*(*) file_str
         INTEGER line
         CHARACTER (LEN=512) rc_str
         CHARACTER (LEN=512) str_with_rc

         IF ( expected .ne. actual ) THEN
          WRITE (rc_str,*) '  Routine returned error code = ',actual
          str_with_rc = TRIM(str // rc_str)
          CALL wrf_error_fatal3 ( file_str, line, str_with_rc )
         ENDIF
      END

      LOGICAL FUNCTION wrf_at_debug_level ( level )
      IMPLICIT NONE
#include "wrf_error.h"
      INTEGER level
         wrf_at_debug_level = ( level .LE. wrf_debug_level )
      RETURN
      END

