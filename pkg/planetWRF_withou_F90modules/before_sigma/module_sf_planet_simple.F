C!WRF:MODEL_LAYER:PHYSICS
C!
CMODULE module_sf_planet_simple

C  USE module_wrf_error
C  USE module_planet_utilities
#include "planetWRF_OPTIONS.h"
CCONTAINS

CC!----------------------------------------------------------------
      SUBROUTINE SURFACE_PLANETARY_SIMPLE(T3D,
     &             QV3D,
     &             P3D,FLHC,FLQC,  
     &             PSFC,pCO2,XLAND,TMN,HFX,QFX,LH,GRDFLX,TSK,QSFC,    
     &             GSW,GLW,CAPG,THC,SNOWC,EMISS,MAVAIL,          
     &             DELTSM,ROVCP,XLV,DT,IFSNOW,                   
     &             STBOLT,P2SI,XLAT,                             
     &             TSLB,ZS,DZS,num_soil_layers,radiation,        
     &             P1000mb,rho_ground,co2ice_threshold,co2_lheat,
     &             co2ice,co2sublimate,h2oice,h2oice_threshold,  
     &             ground_ice_depth, ground_ice_percentage,      
     &             iplanet,mars_co2_cycle,                           
     &             ids,ide, jds,jde, kds,kde,                    
     &             ims,ime, jms,jme, kms,kme,                    
     &             its,ite, jts,jte, kts,kte                    )
C!----------------------------------------------------------------
      IMPLICIT NONE
C!----------------------------------------------------------------
C!                                                                        
C!     SUBROUTINE SURFACE_PLANET_SIMPLE CALCULATES THE GROUND
C!     TEMPERATURE TENDENCY ACCORDING TO A DIFFUSIVE EQUATION
C!     BASED ON GROUND SURFACE PROPERTIES AND RADIATIVE BALANCE
C!                                                                      
C!----------------------------------------------------------------          
C!-- T3D         temperature (K)
C!-- QV3D        3D water vapor mixing ratio (Kg/Kg)
C!-- P3D         3D pressure (Pa)
C!-- FLHC        exchange coefficient for heat (m/s)
C!-- FLQC        exchange coefficient for moisture (m/s)
C!-- PSFC        surface pressure (Pa)
C!...............partial  pressure of CO2 at surface is added by Y.L.
C!-- pCO2        CO2 partial pressure (Pa)
C!-- XLAND       land mask (1 for land, 2 for water)
C!-- TMN         soil temperature at lower boundary (K)
C!-- HFX         upward heat flux at the surface (W/m^2)
C!-- QFX         upward moisture flux at the surface (kg/m^2/s)
C!-- LH          latent heat flux at the surface (W/m^2)
C!-- TSK         surface temperature (K)
C!-- GSW         downward short wave flux at ground surface (W/m^2)      
C!-- GLW         downward long wave flux at ground surface (W/m^2)
C!-- CAPG        heat capacity for soil (J/K/m^3)
C!-- THC         thermal inertia (Cal/cm/K/s^0.5)
C!-- SNOWC       flag indicating snow coverage (1 for snow cover)
C!-- EMISS       surface emissivity (between 0 and 1)
C!-- DELTSM      time step (second)
C!-- ROVCP       R/CP
C!-- XLV         latent heat of melting (J/kg)
C!-- DT          time step (second)
C!-- IFSNOW      ifsnow=1 for snow-cover effects
C!-- STBOLT      Stefan-Boltzmann constant (W/m^2/K^4)
C!-- TSLB        soil temperature in 5-layer model
C!-- ZS          depths of centers of soil layers
C!-- DZS         thicknesses of soil layers
C!-- num_soil_layers   the number of soil layers
C!-- ids         start index for i in domain
C!-- ide         end index for i in domain
C!-- jds         start index for j in domain
C!-- jde         end index for j in domain
C!-- kds         start index for k in domain
C!-- kde         end index for k in domain
C!-- ims         start index for i in memory
C!-- ime         end index for i in memory
C!-- jms         start index for j in memory
C!-- jme         end index for j in memory
C!-- kms         start index for k in memory
C!-- kme         end index for k in memory
C!-- its         start index for i in tile
C!-- ite         end index for i in tile
C!-- jts         start index for j in tile
C!-- jte         end index for j in tile
C!-- kts         start index for k in tile
C!-- kte         end index for k in tile
C!----------------------------------------------------------------

C====INPUT/OUTPUT====

      INTEGER ids,ide, jds,jde, kds,kde,  
     &        ims,ime, jms,jme, kms,kme,  
     &        its,ite, jts,jte, kts,kte

      INTEGER num_soil_layers, iplanet
      LOGICAL radiation, mars_co2_cycle

      INTEGER IFSNOW

      REAL*8 DT,XLV,ROVCP,DELTSM

      REAL*8 STBOLT
      REAL*8 P1000mb,P2SI,rho_ground,   
     &       co2ice_threshold,co2_lheat, 
     &       h2oice_threshold

      REAL*8 TSLB( ims:ime , jms:jme, 1:num_soil_layers )

      REAL*8 ZS( 1:num_soil_layers ), DZS( 1:num_soil_layers )

      REAL*8 QV3D( ims:ime, jms:jme, kms:kme )
      REAL*8 P3D ( ims:ime, jms:jme, kms:kme )
      REAL*8 T3D ( ims:ime, jms:jme, kms:kme )

      REAL*8 SNOWC ( ims:ime, jms:jme ),
     &       XLAND ( ims:ime, jms:jme ),
     &       EMISS ( ims:ime, jms:jme ),
     &       MAVAIL( ims:ime, jms:jme ),
     &       TMN   ( ims:ime, jms:jme ),
     &       GSW   ( ims:ime, jms:jme ),
     &       GLW   ( ims:ime, jms:jme ),
     &       THC   ( ims:ime, jms:jme )

      REAL*8 HFX   ( ims:ime, jms:jme ),
     &       QFX   ( ims:ime, jms:jme ),
     &       LH    ( ims:ime, jms:jme ),
     &       GRDFLX( ims:ime, jms:jme ),
     &       CAPG  ( ims:ime, jms:jme ),
     &       TSK   ( ims:ime, jms:jme ),
     &       QSFC  ( ims:ime, jms:jme ) 

      REAL*8 PSFC  ( ims:ime, jms:jme )      
      REAL*8 pCO2  ( ims:ime, jms:jme )

      REAL*8 FLHC  ( ims:ime, jms:jme ),
     &       FLQC  ( ims:ime, jms:jme )

      REAL*8 co2ice( ims:ime, jms:jme ),
     &       co2sublimate( ims:ime, jms:jme ),
     &       h2oice( ims:ime, jms:jme )

      REAL*8 xlat( ims:ime, jms:jme ),
     &       ground_ice_depth( ims:ime, jms:jme ),
     &       ground_ice_percentage( ims:ime, jms:jme )

C====Local Variables====

      REAL*8 QV1D( ims:ime )
      REAL*8 P1D ( ims:ime )
      REAL*8 T1D ( ims:ime )
      REAL*8 gkz   ( 1:num_soil_layers ),
     &       adelz ( 1:num_soil_layers ),
     &       bdelz ( 1:num_soil_layers ),
     &       cdelz ( 1:num_soil_layers ),
     &       bdelz1( 1:num_soil_layers ),
     &       bdelz2( 1:num_soil_layers )

      INTEGER I,J,L


      DO l= 1, num_soil_layers-1
         gkz(l)= 2.0/( dzs(l)+dzs(l+1) )
      END DO
      adelz(1)= 0.0
      bdelz(1)= 2.0/( (dzs(1)+dzs(2))*dzs(1) )
      cdelz(1)= 2.0/( (dzs(1)+dzs(2))*dzs(1) )
      bdelz1(1)=1.0/( (dzs(1)+dzs(2))*dzs(1) )
      bdelz2(1)=1.0/( (dzs(1)+dzs(2))*dzs(1) )
      DO l= 2, num_soil_layers-1
         adelz(l)=     gkz(l-1)         /dzs(l)
         bdelz(l)=   ( gkz(l-1)+gkz(l) )/dzs(l)
         cdelz(l)=              gkz(l)  /dzs(l)
         bdelz1(l)= adelz(l)
         bdelz2(l)= cdelz(l)
      END DO


      DO J=jts,jte

         DO i=its,ite
            T1D(i) =T3D(i,j,1)
            QV1D(i)=QV3D(i,j,1)
            P1D(i) =P3D(i,j,1)
         END DO

         CALL SURF_PLANETARY_SIMPLE_1D(T1D,
     &       QV1D,
     &       P1D,FLHC(ims:ime,j),FLQC(ims:ime,j),    
     &       PSFC(ims:ime,j),pCO2(ims:ime,j), XLAND(ims:ime,j),
     &       TMN(ims:ime,j),HFX(ims:ime,j),      
     &       QFX(ims:ime,j),TSK(ims:ime,j),
     &       QSFC(ims:ime,j),GRDFLX(ims:ime,j),     
     &       LH(ims:ime,j),GSW(ims:ime,j),GLW(ims:ime,j),              
     &       CAPG(ims:ime,j),THC(ims:ime,j),
     &       SNOWC(ims:ime,j),EMISS(ims:ime,j),    
     &       MAVAIL(ims:ime,j),DELTSM,ROVCP,XLV,DT,IFSNOW,            
     &       STBOLT,P2SI,rho_ground,co2ice_threshold,co2_lheat,   
     &       co2ice(ims:ime,j),co2sublimate(ims:ime,j),
     &       h2oice(ims:ime,j),     
     &       h2oice_threshold,iplanet,XLAT(ims:ime,j),                
     &       ground_ice_depth(ims:ime,j),
     &       ground_ice_percentage(ims:ime,j),
     &       TSLB(ims:ime,j,1:num_soil_layers),ZS,DZS,num_soil_layers,
     &       radiation,      
     &       P1000mb,gkz,adelz,bdelz,cdelz,bdelz1,bdelz2,         
     &       mars_co2_cycle,                                          
     &       ids,ide, jds,jde, kds,kde,                           
     &       ims,ime, jms,jme, kms,kme,                           
     &       its,ite, jts,jte, kts,kte                        )

      END DO

      RETURN
      END 


C!----------------------------------------------------------------
      SUBROUTINE SURF_PLANETARY_SIMPLE_1D(T1D,
     &             QV1D,
     &             P1D,FLHC,FLQC,
     &             PSFCPA,PCO2PA,XLAND,TMN,HFX,QFX,TSK,QSFC,GRDFLX,   
     &             LH,GSW,GLW,CAPG,THC,SNOWC,EMISS,MAVAIL,      
     &             DELTSM,ROVCP,XLV,DT,IFSNOW,                  
     &             STBOLT,P2SI,rho_ground,co2ice_threshold,     
     &             co2_lheat,co2ice,co2sublimate,h2oice,        
     &             h2oice_threshold,iplanet,XLAT,               
     &             ground_ice_depth,ground_ice_percentage,      
     &             TSLB2D,ZS,DZS,num_soil_layers,radiation,     
     &             P1000mb,gkz,adelz,bdelz,cdelz,bdelz1,bdelz2, 
     &             mars_co2_cycle,                                  
     &             ids,ide, jds,jde, kds,kde,                   
     &             ims,ime, jms,jme, kms,kme,                   
     &             its,ite, jts,jte, kts,kte                  )
C!----------------------------------------------------------------
      IMPLICIT NONE
C!----------------------------------------------------------------
C!                                                                        
C!----------------------------------------------------------------          

      REAL*8 D2TDZ2
      REAL*8 TFROST_CO2, RSAT
      INTEGER ids,ide, jds,jde, kds,kde,  
     &        ims,ime, jms,jme, kms,kme,  
     &        its,ite, jts,jte, kts,kte

      INTEGER num_soil_layers, iplanet 
      LOGICAL radiation, mars_co2_cycle

      INTEGER IFSNOW

      REAL*8 DT,XLV,ROVCP,DELTSM

      REAL*8 STBOLT
      REAL*8 P1000mb,P2SI,rho_ground,   
     &       co2ice_threshold,co2_lheat, 
     &       h2oice_threshold

      REAL*8 TSLB2D( ims:ime , 1:num_soil_layers )

      REAL*8 ZS (1:num_soil_layers),
     &       DZS(1:num_soil_layers)
      REAL*8 gkz   (1:num_soil_layers),
     &       adelz (1:num_soil_layers),
     &       bdelz (1:num_soil_layers),
     &       cdelz (1:num_soil_layers),
     &       bdelz1(1:num_soil_layers),
     &       bdelz2(1:num_soil_layers)

      REAL*8 HFX   ( ims:ime ),
     &       QFX   ( ims:ime ),
     &       LH    ( ims:ime ),
     &       GRDFLX( ims:ime ),
     &       CAPG  ( ims:ime ),
     &       TSK   ( ims:ime ),
     &       QSFC  ( ims:ime )

      REAL*8 SNOWC ( ims:ime ),
     &       XLAND ( ims:ime ),
     &       EMISS ( ims:ime ),
     &       MAVAIL( ims:ime ),
     &       TMN   ( ims:ime ),
     &       GSW   ( ims:ime ),
     &       GLW   ( ims:ime ),
     &       THC   ( ims:ime )

      REAL*8 QV1D  ( ims:ime )
      REAL*8 P1D   ( ims:ime )
      REAL*8 T1D   ( ims:ime )

      REAL*8 PSFCPA( ims:ime )
      REAL*8 PCO2PA( ims:ime )

      REAL*8 FLHC  ( ims:ime ),
     &       FLQC  ( ims:ime )

      REAL*8 co2ice( ims:ime ),
     &       co2sublimate( ims:ime ),
     &       h2oice( ims:ime )

      REAL*8 ground_ice_depth( ims:ime ),
     &       ground_ice_percentage(ims:ime )

      REAL*8 xlat( ims:ime )

C    ! Infrared emissivity of CO2 ice
C    !REAL, PARAMETER :: emiss_ice= 0.8
C    ! Better fits to Viking lander pressure curves
      REAL*8 emiss_co2ice_north
      REAL*8 emiss_co2ice_south 

      PARAMETER ( emiss_co2ice_north = 0.520 )
      PARAMETER ( emiss_co2ice_south = 0.785 )

C    ! Infrared emissivity of H2O ice
      REAL*8 emiss_h2oice
      PARAMETER ( emiss_h2oice= 1.0 )

C    ! Thermal inertias of the ices in the two Martian polar caps:
      REAL*8 TI_MARS_ICE_N
      REAL*8 TI_MARS_ICE_S
 
      PARAMETER ( TI_MARS_ICE_N = 2600 )
      PARAMETER ( TI_MARS_ICE_S = 1168 )

C      PARAMETER ( TI_MARS_ICE_N = 1000.0 )
C      PARAMETER ( TI_MARS_ICE_S = 500.0  )

C    ! Threshold amount of ground ice to begin applying vertically variable
C    ! thermal inertias.  (Right now just a step function from orbitally
C    ! derived values to ice values)
      REAL*8 GROUND_ICE_THRESHOLD
      PARAMETER ( GROUND_ICE_THRESHOLD = 0.09 )

      REAL*8 ADIAG( num_soil_layers ),
     &       BDIAG( num_soil_layers ),
     &       CDIAG( num_soil_layers ),
     &       RHS  ( num_soil_layers ),
     &       GKL  ( num_soil_layers )

      INTEGER I,K,L

      REAL*8 TG0, CRD
      REAL*8 PS1,THG,QSG,HFXT,QFXT,THCON
      REAL*8 RNET, UPFLUX
      REAL*8 t1,ew,q1,ewrhs,emissv,gk
      REAL*8 dtemp, rtlatent, tfrost
C     enable thermal inertia update
      REAL*8 thcv

C    ! Change the following variable to .TRUE. to turn off the hardwired
C    ! minimum temperature for the ground (i.e., the CO2 condensation temp.)
      LOGICAL no_mars_co2_cycle 

C    SELECT CASE(iplanet)
C    CASE (4)  ! Mars
#ifdef WRF_MARS
       no_mars_co2_cycle = .FALSE.

       IF (.not. mars_co2_cycle) 
     &    no_mars_co2_cycle = .TRUE.
#endif /* WRF_MARS */
C    CASE (61) ! Titan
#ifdef WRF_TITAN
       no_mars_co2_cycle = .TRUE.
#endif /* WRF_TITAN */
C    CASE DEFAULT
C       WRITE(wrf_err_message,*) 'ERROR: sf_planet_simple, unavailable planet',&
C            'chosen: ', iplanet
C       CALL wrf_error_fatal ( 300 , wrf_err_message )
C    END SELECT


      DO i=its,ite

C        ! TSK is Temperature at gound sfc
         TG0=TSK(I)

C        ! Mars GCM sub/surface temperature code

         LH(I) = 0.
         IF (iplanet == 61) THEN
C          ! Tokano (1999) has cp=1400 J/K/kg for porous icy regolith
C          ! and rock-ice mixture
           CAPG(I)=1400.0*RHO_GROUND    ! Heat capacity of the soil, J/(m^3 K)
         ELSE
C          ! 837.2 comes from MGCM value of 0.2 cal/g/K for soil heat capacity
C          ! 837.2 = 0.2 cal/g/K * 4.186 J/cal * 1000.g/kg
           CAPG(I)=837.2*RHO_GROUND     ! Heat capacity of the soil, J/(m^3 K)
         END IF

C---     update thermal inertia based on ground ice
C           IF (iplanet == 61) THEN
C              thcv = THC(i)
C           ELSE IF ( co2ice(i) > GROUND_ICE_THRESHOLD )  THEN
C             IF (XLAT(i) > 0.) THEN
C                thcv=TI_MARS_ICE_N
C             ELSE IF (XLAT(i) < 0.) THEN
C                thcv=TI_MARS_ICE_S
C             ELSE
C                thcv=THC(i)
C             END IF 
C           ELSE
C              thcv=THC(i)
C           END IF

        GK=(THC(I)/CAPG(I))**2.   ! CAPG is Cp*Rho, GK is thermal diffusivity
C        change thermal inertia when ground ice exceeds the threshold
C         GK=(thcv/CAPG(I))**2.   ! CAPG is Cp*Rho
         DO l=1,num_soil_layers
             GKL(l) = GK
         END DO
C        ! Calculate diffusive heating rate and put in GRDFLX
         GK = GKL(1)
         GRDFLX(I) = D2TDZ2(ZS,DZS,TSLB2D(I,1:num_soil_layers),GK,
     &               num_soil_layers)

         tfrost = TFROST_CO2(PCO2PA(I))

         dtemp = 100.
C        ! Check to make sure that the difference in temperatures used by the
C        ! radiation calculation (UPFLUX) matches that returned by the diffusion
C        ! updater.  Tolerance is 1 K.
C         t_consistency_check: 
         DO WHILE (dtemp > 1.)

C          ! ADIAG, BDIAG, and CDIAG are modified by TRDSLV1D, so they must
C          ! be regenerated before each call to TRDSLV1D
           DO L= 1, num_soil_layers-1
              GK      = GKL(L)
              ADIAG(L)=     - GK*0.5*DELTSM*ADELZ(L)
              BDIAG(L)= 1.0 + GK*0.5*DELTSM*BDELZ(L)
              CDIAG(L)=     - GK*0.5*DELTSM*CDELZ(L)
           END DO
           GK                      = GKL(num_soil_layers-1)
           CDIAG(num_soil_layers-1)= 
     &          - GK*DELTSM*CDELZ(num_soil_layers-1)

           IF (iplanet == 61) THEN
              emissv = emiss(i)
           ELSE IF ( co2ice(i) > CO2ICE_THRESHOLD )  THEN
             IF (XLAT(i) > 0.) THEN
                emissv=emiss_co2ice_north
             ELSE
                emissv=emiss_co2ice_south
             END IF
C          ! Modify longwave surface emissivity for H2O ice here, if necessary
C          !ELSE IF (h2oice(i) > H2OICE_THRESHOLD) THEN
C          !   emissv=emiss_h2oice
           ELSE
              emissv=emiss(i)
           END IF

C          !
C          C!-----COMPUTE THE SURFACE ENERGY BUDGET:
C          !

           UPFLUX = STBOLT*TG0**4.0
           RNET   = GSW(I)+EMISSV*(GLW(I)-UPFLUX)
           CRD    = 1.0/( CAPG(I)*DZS(1) )
           RHS(1) = DELTSM*CRD*( RNET - HFX(I) ) 
     &              + (2.0-BDIAG(1))*TSLB2D(I,1)-CDIAG(1)*TSLB2D(I,2)
           DO L= 2, num_soil_layers-1
              RHS(L)=    -ADIAG(L)* TSLB2D(I,L-1) + 
     &                (2.0-BDIAG(L))*TSLB2D(I,L)     
     &                 - CDIAG(L) *TSLB2D(I,L+1) 
           END DO


           CALL TRDSLV1D(num_soil_layers-1,ADIAG,BDIAG,CDIAG,RHS)

           dtemp = ABS(RHS(1)-TG0)
           IF (dtemp > 1.) THEN
              TG0=0.5*(RHS(1)+TG0)
           ELSE
              TG0= RHS(1)
              DO L= 1, num_soil_layers-1
                 TSLB2D(I,L)=RHS(L)
              END DO
           END IF

         END DO !t_consistency_check


C       ! Special code for Martian CO2 cycle
C        co2_check: 
         IF (.not.no_mars_co2_cycle) THEN
C          ! If we have turned off the CO2 cycle, we must make sure that we don't
C          ! adjust the surface temperatures to be no colder than the frost
C          ! temperature of CO2.

C          ! Surface CO2 microphysics/snow code
           co2sublimate(i)= 0.0

           rtlatent= 1.0/( CRD*co2_lheat*deltsm )
C          ! co2sublimate is positive (+) if loss from surface
           co2sublimate(i) = ( tg0 - tfrost )*rtlatent ! kg/(m^2 s)
C          ! Save this useful diagnostic in the LH variable for output
           lh(i) = co2sublimate(i)*co2_lheat

C          ! Here we will only consider setting of the surface temperature, tg0
C          ! We will modify the atmospheric CO2 in the microphysics section
C          ! And we will remember how much to change atmospheric CO2 with
C          ! the co2sublimate variable

C          ! Consider condensation/sublimation at the surface

C          ! Surface wanted to be colder than frost temperature
C          ! Steal heat from atmosphere by condensing CO2 on surface, and
C          ! adjust column mass later (in microphysics)
           IF (co2sublimate(i) < 0.) tg0 = tfrost

C          ! Surface is warm enough to sublime any surface ice, if present
           IF (co2sublimate(i) > 0.) THEN
C             ! Is there any ice present on the surface?
              IF (co2ice(i) > 0.) THEN
C                ! 2 cases (partial and complete) for sublimation of surface ice
                 IF (co2sublimate(i)*deltsm < co2ice(i)) THEN
C                   ! Case 1: More than enough ice to accomodate mass flux of CO2
C                   !         but ice remains on surface, so temperature remains
C                   !         at freezing/frost point.
                   tg0 = tfrost
                 ELSE
C                   ! Case 2: Sublime remaining snow and adjust temperature.
C                   !         Temperature change is less than previously
C                   !         calculated because some of the heating is used
C                   !         to sublime CO2.
                    tg0 = tg0 - co2ice(i)*co2_lheat*crd
                    co2sublimate(i) = co2ice(i)/deltsm
                END IF
              ELSE
C                ! There's no ice, so we can't sublimate anything
                 co2sublimate(i) = 0.
              END IF
           END IF
         END IF !co2_check

C        !  Update TSLB(*,1) with surface temperature modifications
         TSLB2D(I,1)=TG0
         TSK(I)=TG0

         IF ((MAVAIL(I).LT.1.e-3) .OR. (iplanet == 61)) THEN
            QSG=0.
         ELSE
            QSG = RSAT(TSLB2D(I,1),PSFCPA(I))
         END IF

         THCON=(P1000mb/PSFCPA(I))**ROVCP
         THG=TSLB2D(I,1)*THCON

         THCON=(P1000mb/P1D(I))**ROVCP
C        ! UPDATE FLUXES FOR NEW GROUND TEMPERATURE
         HFXT=FLHC(I)*(THG-T1D(I)*THCON)
         QFXT=FLQC(I)*(QSG-QV1D(I))
C        ! SUM HFX AND QFX OVER SOIL TIMESTEP
         HFX(I)=HFX(I)+HFXT
         QFX(I)=QFX(I)+QFXT

C        ! The following are as seen in module_sf_slab.F
C        ! QSFC and CHKLOWQ needed by Eta PBL
         IF (FLQC(I) > 0.) THEN
            QSFC(I)=QV1D(I)+QFX(I)/FLQC(I)
         ELSE
C           ! If FLQC=0, then QFXT=0, but does QFX=0???
C           ! I think we are assuming that an FLQC=0 means QFX=0...
            QSFC(I)=QV1D(I)+QFX(I)
         END IF
C       ! We didn't pass in CHKLOWQ, so we'll do this in module_surface_driver
C       ! Other subroutines use CHKLOWQ=1 or 0
C       !CHKLOWQ(I)=MAVAIL(I)

      END DO

      RETURN
      END 

C!================================================================
      REAL*8 FUNCTION D2TDZ2(ZS,DZS,TSLB,GK,n) 
C!----------------------------------------------------------------
C!   Calculate d^2 T_ground/ dZ^2 at the surface (z=0)
C!----------------------------------------------------------------
      IMPLICIT NONE 
C!----------------------------------------------------------------
      INTEGER n !num_soil_layers
      REAL*8 zs(1:n), dzs(1:n), tslb(1:n)
      REAL*8 gk

      REAL*8 z12, z23, z34, z13, z24
      REAL*8 dtdz_1, dtdz_2, dtdz_3
      REAL*8 d2tdz2_0, d2tdz2_1, d2tdz2_2

      z12 = 0.5*(zs(1)+zs(2))
      z23 = 0.5*(zs(2)+zs(3))
      z34 = 0.5*(zs(3)+zs(4))
      z13 = 0.5*(z12+z23)
      z24 = 0.5*(z23+z34)

C     ! DTSLB/Dz, true at the midpoint of the two locations
      dtdz_1 = (tslb(2)-tslb(1))/(zs(2)-zs(1))
      dtdz_2 = (tslb(3)-tslb(2))/(zs(3)-zs(2))
      dtdz_3 = (tslb(4)-tslb(3))/(zs(4)-zs(3))
      d2tdz2_1 = (dtdz_2 - dtdz_1)/(z23-z12) ! true at z13
      d2tdz2_2 = (dtdz_3 - dtdz_2)/(z34-z23) ! true at z24
C     ! We have two 2nd derivatives at two points.
C     ! Use linear interpolation to get the value at z=0
C     ! y = y1*(x-x2)/(x1-x2) + y2*(x-x1)/(x2-x1)
      d2tdz2_0 = -d2tdz2_1*z24/(z13-z24) - d2tdz2_2*z13/(z24-z13)
      d2tdz2 = gk*d2tdz2_0

      RETURN
      END 
C!================================================================
      SUBROUTINE SURF_PLANETARY_SIMPLE_INIT(TSK,TMN,             
     &                 TSLB,ZS,DZS,num_soil_layers,             
     &                 restart, allowed_to_read,                
     &                 ids,ide, jds,jde, kds,kde,               
     &                 ims,ime, jms,jme, kms,kme,               
     &                 its,ite, jts,jte, kts,kte                )
C!----------------------------------------------------------------
      IMPLICIT NONE
C!----------------------------------------------------------------
      LOGICAL restart, allowed_to_read
      INTEGER ids,ide, jds,jde, kds,kde, 
     &        ims,ime, jms,jme, kms,kme, 
     &        its,ite, jts,jte, kts,kte

      INTEGER num_soil_layers

      REAL*8 TSLB( ims:ime , jms:jme, 1:num_soil_layers )

      REAL*8 ZS ( 1:num_soil_layers ),
     &       DZS( 1:num_soil_layers )

      REAL*8 TSK( ims:ime, jms:jme ),
     &       TMN( ims:ime, jms:jme )         
C    !  LOCAR VAR
      INTEGER L,J,I,itf,jtf
C!----------------------------------------------------------------
 
      itf=min0(ite,ide-1)
      jtf=min0(jte,jde-1)

      RETURN
      END

C!-------------------------------------------------------------------          

CEND MODULE module_sf_planet_simple
