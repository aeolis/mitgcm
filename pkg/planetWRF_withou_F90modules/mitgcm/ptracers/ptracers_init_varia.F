C $Header: /u/gcmpack/MITgcm/pkg/ptracers/ptracers_init_varia.F,v 1.10 2012/03/08 17:06:36 jmc Exp $
C $Name:  $

C#include "PTRACERS_OPTIONS.h"
#include "planetWRF_OPTIONS.h"
#include "GAD_OPTIONS.h"

C---+----1----+----2----+----3----+----4----+----5----+----6----+----7-|--+----|
CBOP
C     !ROUTINE: PTRACERS_INIT_VARIA

C     !INTERFACE:
      SUBROUTINE PTRACERS_INIT_VARIA( myThid )

C     !DESCRIPTION:
C     Initialize PTRACERS data structures

C     !USES:
      IMPLICIT NONE

#ifdef ALLOW_PTRACERS

#include "PTRACERS_MOD.h"
#include "SIZE.h"
#include "EEPARAMS.h"
#include "PARAMS.h"
#include "GRID.h"
#include "GAD.h"
#ifdef ALLOW_PLANETWRF
#include "DYNVARS.h"
#endif /* ALLOW_PLANETWRF */
#include "PTRACERS_SIZE.h"
#include "PTRACERS_PARAMS.h"
#include "PTRACERS_START.h"
#include "PTRACERS_FIELDS.h"

#endif /* ALLOW_PTRACERS */

C     !INPUT PARAMETERS:
C     myThid               :: thread number
      INTEGER myThid

#ifdef ALLOW_PTRACERS

C     !LOCAL VARIABLES:
C     i,j,k,bi,bj,iTracer  :: loop indices
      INTEGER i,j,k,bi,bj,iTracer
#ifdef PTRACERS_ALLOW_DYN_STATE
      INTEGER n
#endif
#ifdef ALLOW_PLANETWRF
      _RL temperature
      _RL RSAT
#endif /* ALLOW_PLANETWRF */
CEOP

C     Initialise internal parameter in common block:
      _BEGIN_MASTER( myThid )
      DO iTracer = 1, PTRACERS_num
        PTRACERS_StepFwd(iTracer) = .TRUE.
        PTRACERS_startAB(iTracer) = nIter0 - PTRACERS_Iter0
      ENDDO
      _END_MASTER( myThid )
      _BARRIER

C     Loop over tracers
      DO iTracer = 1, PTRACERS_num

C     Loop over tiles
       DO bj = myByLo(myThid), myByHi(myThid)
        DO bi = myBxLo(myThid), myBxHi(myThid)

C     Initialize arrays in common blocks :
         DO k=1,Nr
          DO j=1-OLy,sNy+OLy
           DO i=1-OLx,sNx+OLx
            pTracer(i,j,k,bi,bj,iTracer) = PTRACERS_ref(k,iTracer)
            gPtr(i,j,k,bi,bj,iTracer)    = 0. _d 0
            gpTrNm1(i,j,k,bi,bj,iTracer) = 0. _d 0
           ENDDO
          ENDDO
         ENDDO
         DO j=1-OLy,sNy+OLy
          DO i=1-OLx,sNx+OLx
           surfaceForcingPTr(i,j,bi,bj,iTracer) = 0. _d 0
          ENDDO
         ENDDO

C#if ( (defined ALLOW_PLANETWRF) && (defined WRF_TRACER_CYCLE) )
c     Initialize water vapor in the Martian atmosphere
c     Remember iTracer=1 is for passive tracer only
c        IF (iTracer.eq.2) THEN
c        DO k=1,Nr
c          DO j=1-OLy,sNy+OLy
c           DO i=1-OLx,sNx+OLx
c            temperature=theta(i,j,k,bi,bj)*
c     &          (rC(k)*hFacC(i,j,k,bi,bj)/atm_po)**atm_kappa
C#ifdef WRF_MARS
c    initialize the total of water vapor mass around 1.2-1.3x10^15g
c    for Viking measurement (Richardson and Wilson 2002).
c            pTracer(i,j,k,bi,bj,iTracer) =  4.5 _d -5
c     &          0.01*RSAT(temperature,rC(k)*hFacC(i,j,k,bi,bj))
C#endif /* WRF_MARS */
c           ENDDO
c          ENDDO
c        ENDDO
c        ENDIF
C#endif /* ALLOW_PLANETWRF */

#ifdef PTRACERS_ALLOW_DYN_STATE
C     Initialize SOM array :
         IF ( PTRACERS_SOM_Advection(iTracer) ) THEN
           DO n = 1,nSOM
            DO k=1,Nr
             DO j=1-OLy,sNy+OLy
              DO i=1-OLx,sNx+OLx
               _Ptracers_som(i,j,k,bi,bj,n,iTracer) = 0. _d 0
              ENDDO
             ENDDO
            ENDDO
           ENDDO
         ENDIF
#endif /* PTRACERS_ALLOW_DYN_STATE */

C     end bi,bj loops
        ENDDO
       ENDDO

C     end of Tracer loop
      ENDDO

C     Now read initial conditions and always exchange
      IF (nIter0.EQ.PTRACERS_Iter0) THEN
       DO iTracer = 1, PTRACERS_numInUse
        IF ( PTRACERS_initialFile(iTracer) .NE. ' ' ) THEN
         CALL READ_FLD_XYZ_RL(PTRACERS_initialFile(iTracer),' ',
     &        pTracer(1-OLx,1-OLy,1,1,1,iTracer),0,myThid)
         _EXCH_XYZ_RL(pTracer(1-OLx,1-OLy,1,1,1,iTracer),myThid)
        ENDIF
       ENDDO
      ENDIF

C     Apply mask
      DO iTracer = 1, PTRACERS_numInUse
       DO bj = myByLo(myThid), myByHi(myThid)
        DO bi = myBxLo(myThid), myBxHi(myThid)
         DO k=1,Nr
          DO j=1-OLy,sNy+OLy
           DO i=1-OLx,sNx+OLx
            IF (maskC(i,j,k,bi,bj).EQ.0.)
     &           pTracer(i,j,k,bi,bj,iTracer)=0. _d 0
           ENDDO
          ENDDO
         ENDDO
        ENDDO
       ENDDO
      ENDDO

C Read from a pickup file if needed
      IF ( nIter0.GT.PTRACERS_Iter0 .OR.
     &    (nIter0.EQ.PTRACERS_Iter0 .AND. pickupSuff.NE.' ')
     &   ) THEN

       CALL PTRACERS_READ_PICKUP( nIter0, myThid )
      ENDIF

#endif /* ALLOW_PTRACERS */

      RETURN
      END
