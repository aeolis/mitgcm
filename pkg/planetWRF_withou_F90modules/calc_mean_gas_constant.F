C-- This routine calculates the mean gas constant for mixtures, 
C-- which will be used to calculate virtual temperature (to include the 
C-- molecular mass and thermal properties of tracer gases). Here the mean 
C-- gas constant is non-dimensional, it indeed gives Rmean=R_mean/Rd, 
C-- where Rd is the major gas components of the atmosphere

C-- Derived from p = sum(p(i)), where p(i)=rho(i)*R(i)*T
C-- and apply q(i) = rho(i)/rho, where rho is mean air density
C-- and p = rho*Rd*T*(1+sum(Rq(i)*q(i))) 
C--       = rho*R_mean*T
C--       = rho*Rd*Tv
C-- where Rq(i)=(1-eps(i))/eps(i)
C-- note here we used q_d = 1-sum(q(i))

C-- Virtual temperature is defined as Tv = R_mean/Rd * T 
C-- Rmean = R_mean/Rd = ( 1+sum(Rq(i)*q(i)))

C-- Model input is the specific humidity, so q(i)=M(i)/M_a
#include "planetWRF_OPTIONS.h"

      SUBROUTINE calc_mean_gas_constant(Rmean,ptr,
     &                 im1,im2,jm1,jm2,km1,km2,K,bi,bj)

      IMPLICIT NONE
#include "SIZE.h"
#include "EEPARAMS.h"
#ifdef WRF_ENABLE_TRACERS
#include "PTRACERS_SIZE.h"
#include "PTRACERS_PARAMS.h"
#endif /* WRF_ENABLE_TRACERS */
C--     ====local variables ====
      INTEGER  im1,im2,jm1,jm2,km1,km2,K,bi,bj
      INTEGER  I,J,iTracer

      _RL Rmean(im1:im2,jm1:jm2)

#ifdef WRF_ENABLE_TRACERS
      _RL ptr  (im1:im2,jm1:jm2,km1:km2,
     &          1:nSx,1:nSy,1:PTRACERS_num)
#else 
      _RL ptr  (im1:im2,jm1:jm2,km1:km2,
     &          1:nSx,1:nSy)
#endif /* WRF_ENABLE_TRACERS */
      _RL Rqtot
C.... Rmean is the mean gas constant R_mean/Rd
C.... Rq is (1-eps)/eps, and eps=m(i)/m_d
C.....PTRACERS_active is a switch to make tracer(i) to be 
C     passive (0) or active (1) 
 
      DO I = im1, im2
      DO J = jm1, jm2
            Rqtot = 0.0 _d 0
#ifdef WRF_ENABLE_TRACERS
            DO iTracer = 1, PTRACERS_numInUse
C  calculate sum(Rq(i)*q(i)) at (i,j,k)
               Rqtot = Rqtot + PTRACERS_Rq(iTracer)
     &               * ptr(I,J,K,bi,bj,iTracer)
     &               * PTRACERS_active(iTracer)
            ENDDO
C  calculate the mean gas constant 
C  R_mean/Rd = 1 + sum(Rq(i)*q(i))
            Rmean(I,J) = 1.0 _d 0 + Rqtot
#else 
            Rmean(I,J) = 1.0 _d 0
#endif /* WRF_ENABLE_TRACERS */
      ENDDO
      ENDDO

      RETURN
      END
