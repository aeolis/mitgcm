c-// water cycle related constants
      INTEGER ndstp                 !c-mm Number of sub-timesteps for regolith code
      REAL*8  grid_factor           !c-mm Grid-spacing coefficient
      REAL*8  pore_size             !c-mm Pore size [m]
      REAL*8  soil_surface_area     !c-mm  Soil surface area (m2/kg)
      REAL*8  tortuosity            !c-mm Soil tortuosity
      REAL*8  heat_flow             !c-mm Areothermal heat flow
      REAL*8  Aw                    !c-mm Vapor pressure constant (Pa)
      REAL*8  Bw                    !c-mm Vapor pressure constant (K)
      REAL*8  bk                    !c-mm Boltzmann constant (J/K)
      REAL*8  eps                   !c-mm Temperature term in adsorbate isotherm
      REAL*8  Ko                    !c-mm Frequency term in adsorbate isotherm (1/Pa)
      REAL*8  mass_co2              !c-mm Mass of one CO2 molecule (kg)
      REAL*8  mass_h2o              !c-mm Mass of one H2O molecule (kg)
      REAL*8  nu                    !c-mm Exponent in adsorbate isotherm
      REAL*8  sbc                   !c-mm Stefan-Boltzmann constant
      REAL*8  diam_h2o              !c-mm Collision diameter of H2O (m)
      REAL*8  air_viscosity         !c-yl Viscosity
      REAL*8  part_diam             !c-yl Droplet diameters
      REAL*8  kdirt                 !c-yl Conductivity of Mars soil

      PARAMETER ( ndstp        = 14         )
      PARAMETER ( grid_factor  = 1.80       )
      PARAMETER ( pore_size    = 5.e-6      )
      PARAMETER ( soil_surface_area = 1.7e4 )
      PARAMETER ( tortuosity   = 5.0        )
      PARAMETER ( heat_flow    = 0.030      )
      PARAMETER ( Aw           = 3.56e12    )
      PARAMETER ( Bw           = 6141.7     )
      PARAMETER ( bk           = 1.38054e-23)
c//-mm Replace these next two values which I got from the original adsorbate code with values pulled from Zent et al. (1993).
c//-mm **Note** There is a slight change in the ss_partition code to account for the change in sign of the eps term.
c//-mm PARAMETER ( eps          = 2573.9    )   !c-mm Temperature term in adsorbate isotherm
c//-mm PARAMETER ( Ko           = 1.57e-8   )   !c-mm Frequency term in adsorbate isotherm (1/Pa)
      PARAMETER ( eps          = -2679.8    )
      PARAMETER ( Ko           = 2.043e-8   )
      PARAMETER ( mass_co2     = 7.306e-26  )
      PARAMETER ( mass_h2o     = 2.988e-26  )
      PARAMETER ( nu           = 0.50       )
      PARAMETER ( sbc          = 5.67e-8    )
      PARAMETER ( diam_h2o     = 5.0e-10    )
      PARAMETER ( air_viscosity= 1.E-5      )
      PARAMETER ( part_diam    = 4.E-6      )
      PARAMETER ( kdirt        = 3.0        )

