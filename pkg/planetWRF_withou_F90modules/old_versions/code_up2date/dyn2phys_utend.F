C $Header: /u/gcmpack/MITgcm/pkg/gridalt/dyn2phys_wind.F,v 1.0 
C $Name:  $

      subroutine dyn2phys_utend(dudyn,udync,pedyn,pcdyn,zcdyn,uphyc,
     . pephy,pcphy,zcphy,im1,im2,jm1,jm2,lmdyn,Nsx,Nsy,idim1,idim2,
     . jdim1,jdim2,bi,bj,Lbot,lmphy,nlperdyn,duphy)
C***********************************************************************
C Purpose:
C   To interpolate an arbitrary quantity from the 'dynamics' eta (pstar)
C               grid to the higher resolution physics grid 
C Algorithm:
C   Routine works one layer (edge to edge pressure) at a time.
C   Dynamics -> Physics retains the dynamics layer mean value,
C
C Input:
C   qdyn..... [im,jm,lmdyn] Arbitrary Quantity on Input Grid
C   pedyn.... [im,jm,lmdyn+1] Pressures at bottom edges of input levels
C   im1,2 ... Limits for Longitude Dimension of Input
C   jm1,2 ... Limits for Latitude  Dimension of Input
C   lmdyn.... Vertical  Dimension of Input
C   Nsx...... Number of processes in x-direction
C   Nsy...... Number of processes in y-direction
C   idim1,2.. Beginning and ending i-values to calculate
C   jdim1,2.. Beginning and ending j-values to calculate
C   bi....... Index of process number in x-direction
C   bj....... Index of process number in x-direction
C   windphy.. [im,jm,lmphy] Magnitude of the wind on the output levels
C   pephy.... [im,jm,lmphy+1] Pressures at bottom edges of output levels
C   lmphy.... Vertical  Dimension of Output
C   nlperdyn. [im,jm,lmdyn] Highest Physics level in each dynamics level
C
C Output:
C   qphy..... [im,jm,lmphy] Quantity at output grid (physics grid)
C
C Notes:
C   1) This algorithm assumes that the output (physics) grid levels
C      fit exactly into the input (dynamics) grid levels
C***********************************************************************
      implicit none
cinterp1 #include "PACKAGES_CONFIG.h"
#include "CPP_OPTIONS.h"
      integer  im1, im2, jm1, jm2, lmdyn, lmphy, Nsx, Nsy
      integer idim1, idim2, jdim1, jdim2, bi, bj
    
c=====input/output======= 
      _RL dudyn (im1:im2,jm1:jm2,lmdyn,Nsx,Nsy)
      _RL udync (im1:im2,jm1:jm2,lmdyn,Nsx,Nsy) 
      _RL pedyn (im1:im2,jm1:jm2,lmdyn+1,Nsx,Nsy)
      _RL pcdyn (im1:im2,jm1:jm2,lmdyn,  Nsx,Nsy)
      _RL zcdyn (im1:im2,jm1:jm2,lmdyn,  Nsx,Nsy)
      _RL pephy (im1:im2,jm1:jm2,lmphy+1,Nsx,Nsy)
      _RL pcphy (im1:im2,jm1:jm2,lmphy+1,Nsx,Nsy)
      _RL zcphy (im1:im2,jm1:jm2,lmphy+1,Nsx,Nsy)
      _RL uphyc (im1:im2,jm1:jm2,lmphy+1,Nsx,Nsy)
      _RL duphy (im1:im2,jm1:jm2,lmphy,  Nsx,Nsy)
      integer nlperdyn(im1:im2,jm1:jm2,lmdyn,Nsx,Nsy)
      integer Lbot(im1:im2,jm1:jm2,Nsx,Nsy)
c=====Local variables====
      integer i,j,L,Lout1,Lout2,Lphy
      integer ii,jj,nphy,Lbotij,Ndim

      _RL ue(im1:im2,jm1:jm2,lmphy)
      _RL dpkedyn, dpkephy

      _RL dpdnw,dpupw,dudznm1,dudznm2,dudzphy

      integer DD,rc
      _RL INDX(1:lmphy)
      _RL BB  (1:lmphy)
      _RL AA  (1:lmphy,1:lmphy)

cinterp1 #include 'SIZE.h'
cinterp1 #include 'EEPARAMS.h'
cinterp1 #include 'PARAMS.h'
C#endif

C define wind on the faces of the physics grid
      do j = jdim1,jdim2
       do i = idim1,idim2
        Lbotij = Lbot(i,j,bi,bj)
        do L = 2, lmphy
         dpdnw=pephy(i,j,L-1,bi,bj)-pephy(i,j,L,bi,bj)
         dpupw=pephy(i,j,L,bi,bj)-pephy(i,j,L+1,bi,bj)
         ue(i,j,L) = (udync(i,j,L-1,bi,bj)*dpdnw
     .             +  udync(i,j,L,bi,bj)*dpupw)
     .             / (dpdnw+dpupw)
        enddo
C these two are not going to be used
        ue(i,j,1)       = 0.0
        ue(i,j,lmphy+1) = udync(i,j,lmphy,bi,bj)
       enddo
      enddo

c do loop for all dynamics (input) levels
      do L = 1,lmdyn
c do loop for all grid points
      do j = jdim1,jdim2
      do i = idim1,idim2
c Check to make sure we are above ground - if not, do nothing
       if(L.ge.Lbot(i,j,bi,bj))then
c Find the physics levels in dynamics level L
          if(L.eq.Lbot(i,j,bi,bj)) then
           Lout1 = 0
          else
           Lout1 = nlperdyn(i,j,L-1,bi,bj)
          endif
          Lout2 = nlperdyn(i,j,L,bi,bj)
c Find the number of physics levels in dynamics level L
          Ndim = Lout2 - Lout1
c initialize the coefficent matrices for each dynamics level L
          do ii = 1, lmphy
             BB(ii) = 0.0 _d 0
             do jj =1, lmphy
                AA(ii,jj) = 0.0 _d 0
             enddo
          enddo 
c Calculate the gradient to keep in this dynamics level L
c Use edge gradient if above the bottom level
c Use edge-to-center gradient if within the bottom level
          if ( (L.gt. Lbot(i,j,bi,bj)).and.(L .lt. lmdyn) ) then
c Two gradients in each dynamics level, lower center-center-upper center 
c dthdpnm1: lower center to center of the grid
             dudznm1 = ( udync(i,j,L,  bi,bj) - udync(i,j,L-1,bi,bj) )
     .               / ( zcdyn(i,j,L,  bi,bj) - zcdyn(i,j,L-1,bi,bj) )
c dthdpnm2: center to upper center of the grid
             dudznm2 = ( udync(i,j,L+1,bi,bj) - udync(i,j,L,  bi,bj) )
     .               / ( zcdyn(i,j,L+1,bi,bj) - zcdyn(i,j,L,  bi,bj) )
          elseif ( L .eq. Lbot(i,j,bi,bj)) then
c bottom of the atmosphere has only one constraint: center to upper center
             dudznm2 = ( udync(i,j,L+1,bi,bj) - udync(i,j,L,  bi,bj) )
     .               / ( zcdyn(i,j,L+1,bi,bj) - zcdyn(i,j,L,  bi,bj) )
c keep the gradient for lower edge to center the same as above
             dudznm1 = dudznm2
          elseif ( L .eq. lmdyn) then
c top of the atmosphere has only one constraint: lower edge to center
             dudznm1 = ( udync(i,j,L,  bi,bj) - udync(i,j,L-1,bi,bj) )
     .               / ( zcdyn(i,j,L,  bi,bj) - zcdyn(i,j,L-1,bi,bj) )
             dudznm2 = dudznm1
          endif
c Calculate the layer thickness in dynamics level L
          dpkedyn = pedyn(i,j,L,bi,bj)-pedyn(i,j,L+1,bi,bj)
c Check if dynamics level L has at least two physics levels, otherwise 
c do not use this method, use weight = 1 instead.
          if ( Ndim .ge. 2 ) then
c Construct Ndim by Ndim system of linear equations to solve
c Note: Lout1 is not equal to 0, it increases with dynamics levels
             
c do the regular way for L smaller than lmdyn, preserve momentum
             if (L .le. lmdyn) then            
              BB(1) = dpkedyn * dudyn(i,j,L,bi,bj)
             endif

             do Lphy = Lout1+2,Lout2
                nphy = Lphy - Lout1
c Calculate the center-to-center gradient in physics grid
                dudzphy = ( uphyc(i,j,Lphy,  bi,bj) - 
     .                      uphyc(i,j,Lphy-1,bi,bj) )
     .                  / ( zcphy(i,j,Lphy,  bi,bj) -
     .                      zcphy(i,j,Lphy-1,bi,bj) )
c if center of the physics grid is LOWER than center of the dynamics grid 
                if ( pcphy(i,j,Lphy,bi,bj) .ge. 
     .                                pcdyn(i,j,L,bi,bj)  ) then
                    BB(nphy) = ( dudznm1 - dudzphy ) * 
     .                         ( zcphy(i,j,Lphy,  bi,bj) -
     .                           zcphy(i,j,Lphy-1,bi,bj) )
c if center of the physics grid is HIGHER than center of the dynamics grid
                else
                    BB(nphy) = ( dudznm2 - dudzphy ) *
     .                         ( zcphy(i,j,Lphy,  bi,bj) -
     .                           zcphy(i,j,Lphy-1,bi,bj) )
                endif
             enddo
c Construct coefficient matrix
c do loop for all physics levels contained in this dynamics level
c treat model top separately, as for BB
             do Lphy = Lout1+1,Lout2
                nphy = Lphy - Lout1
                dpkephy=pephy(i,j,Lphy,bi,bj)-pephy(i,j,Lphy+1,bi,bj)
                if (L .lt. lmdyn) then
                   AA(1,nphy) = dpkephy
                else
                   AA(1,1) = 1.0 _d 0
                endif
                if (nphy+1 . le. Ndim) then
                   AA(nphy+1,nphy  ) = -1.0 _d 0
                   AA(nphy+1,nphy+1) =  1.0 _d 0
                endif
             enddo

c solve the system of linear equations using LU decompostion
             call LUDCMP(AA(1:Ndim,1:Ndim),Ndim,INDX(1:Ndim),DD,rc)
             call LUBKSB( AA(1:Ndim,1:Ndim),Ndim,
     .                    INDX(1:Ndim),BB(1:Ndim) )
c move solutions to permanent storage
             do Lphy = Lout1+1,Lout2
                nphy = Lphy - Lout1
                duphy(i,j,Lphy,bi,bj) = BB(nphy)
             enddo

          elseif ( Ndim .eq. 1) then
c use weight=1 for the case Ndim=1 (physics grid = dynamics grid)
             do Lphy = Lout1+1,Lout2
                duphy(i,j,Lphy,bi,bj) = dudyn(i,j,L,bi,bj)
             enddo

          endif           
c ENDIF above surface
       endif
c End loop in I,J
      enddo
      enddo
c End loop in dynamics level
      enddo

      return
      end
