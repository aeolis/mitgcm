c--//   common physics variables

      REAL*8 mCO2, mH2O, mN2, mAr
      REAL*8 rhowater, rhosnow
      PARAMETER (mCO2     = 43.34   )
      PARAMETER (mH2O     = 18.0152 )
      PARAMETER (mN2      = 28.     )
      PARAMETER (mAr      = 40.     )
      PARAMETER (rhowater = 1000.   )
      PARAMETER (rhosnow  = 100.    )

      REAL*8 RV, XLV , co2_lheat, co2ice_threshold
      PARAMETER (RV               = 461.5   )
      PARAMETER (XLV              = 2.5E6   )
      PARAMETER (co2_lheat        = 5.713E5 )
      PARAMETER (co2ice_threshold = 100.0   )


      REAL*8 DEGRAD
      PARAMETER (DEGRAD = ACOS(-1.d0)/180.d0)

c--//  choose planet, double definition to use iplanet
      INTEGER iplanet

#ifdef WRF_MARS
      
      REAL*8 SVP1,SVP2,SVP3,SVPT0
      REAL*8 EP1,EP2,KARMAN,EOMEG,STBOLT
      REAL*8 P2SI,CO2SNOW_THRESHOLD, H2OICE_THRESHOLD
      REAL*8 DPD
      REAL*8 obliquity, eccentricity, equinox_fraction
      REAL*8 zero_date, semimajoraxis
      REAL*8 rho_ground
      
      INTEGER planet_year

      PARAMETER (SVP1=0.6112     )
      PARAMETER (SVP2=17.67      )
      PARAMETER (SVP3=29.65      )
      PARAMETER (SVPT0=273.15    )
      PARAMETER (EP1=mCO2/mH2O-1.)
      PARAMETER (EP2=mH2O/mCO2   )
      PARAMETER (KARMAN=0.4      )
      PARAMETER (EOMEG=7.08822E-5     )
      PARAMETER (STBOLT = 5.670151E-8 )
      PARAMETER (P2SI = 1.0274912)
      PARAMETER (CO2SNOW_THRESHOLD = 100.)
      PARAMETER (H2OICE_THRESHOLD  = 10. )
      PARAMETER (planet_year  = 669               )
      PARAMETER (DPD          = 360./669.         )
      PARAMETER (obliquity    = 25.19             )
      PARAMETER (eccentricity = 0.09341233        )
      PARAMETER (equinox_fraction = 0.2695        )
      PARAMETER (zero_date    = 488.7045          )
      PARAMETER (semimajoraxis= 1.52366231        )
      PARAMETER (rho_ground   = 1500.             ) 

      PARAMETER (iplanet = 4)

#endif /* WRF_MARS */

#ifdef WRF_TITAN

      REAL*8 SVP1,SVP2,SVP3,SVPT0
      REAL*8 EP1,EP2,KARMAN,EOMEG,STBOLT
      REAL*8 P2SI,CO2SNOW_THRESHOLD, H2OICE_THRESHOLD
      REAL*8 DPD
      REAL*8 obliquity, eccentricity, equinox_fraction
      REAL*8 zero_date, semimajoraxis
      REAL*8 rho_ground

      INTEGER planet_year

      PARAMETER (SVP1=0.6112     )
      PARAMETER (SVP2=17.67      )
      PARAMETER (SVP3=29.65      )
      PARAMETER (SVPT0=273.15    )
      PARAMETER (EP1=mCO2/mH2O-1.)
      PARAMETER (EP2=mH2O/mCO2   )
      PARAMETER (KARMAN=0.4      )
      PARAMETER (EOMEG=4.55937638E-6  )
      PARAMETER (STBOLT = 5.670151E-8 )
      PARAMETER (P2SI = 15.95         )
      PARAMETER (CO2SNOW_THRESHOLD = 1.e30)
      PARAMETER (H2OICE_THRESHOLD  = 1.e30)
      PARAMETER (planet_year  = 686               )
      PARAMETER (DPD          = 360./686.         )
      PARAMETER (obliquity    = 26.7              )
      PARAMETER (eccentricity = 0.056             )
      PARAMETER (equinox_fraction = 0.2098        )
      PARAMETER (zero_date    = 542.06            )
      PARAMETER (semimajoraxis= 9.58256           )
      PARAMETER (rho_ground   = 800.0             )

      PARAMETER (iplanet = 61)

#endif /* WRF_TITAN */
