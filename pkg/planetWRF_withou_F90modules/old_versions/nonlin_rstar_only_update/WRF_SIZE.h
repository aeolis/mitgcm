      INTEGER num_soil_layers
      PARAMETER (num_soil_layers = 12 )

      INTEGER Nrphys
#ifdef ALLOW_GRIDALT

#ifdef WRF_MARS
      PARAMETER (Nrphys = 50 )
#endif /* WRF_MARS */
#ifdef WRF_TITAN
      PARAMETER (Nrphys = 65 )
#endif /* WRF_TITAN */

#else
      PARAMETER (Nrphys = Nr )
#endif /* ALLOW_GRIDALT */

      INTEGER ids,ide, jds,jde, kds,kde,
     &        ims,ime, jms,jme, kms,kme,
     &        its,ite, jts,jte, kts,kte
C--   index for domain size of tiles and number of tiles
      PARAMETER ( ids = 1-OLx   )
      PARAMETER ( ide = sNx+OLx )
      PARAMETER ( jds = 1-OLy   )
      PARAMETER ( jde = sNy+OLy )
      PARAMETER ( kds = 1       )
      PARAMETER ( kde = Nrphys+1)
      PARAMETER ( ims = 1-OLx   )
      PARAMETER ( ime = sNx+OLx )
      PARAMETER ( jms = 1-OLy   )
      PARAMETER ( jme = sNy+OLy )
      PARAMETER ( kms = 1       )
      PARAMETER ( kme = Nrphys+1)
      PARAMETER ( its = 1   )
      PARAMETER ( ite = sNx )
      PARAMETER ( jts = 1   )
      PARAMETER ( jte = sNy )
      PARAMETER ( kts = 1       )
      PARAMETER ( kte = Nrphys  )

