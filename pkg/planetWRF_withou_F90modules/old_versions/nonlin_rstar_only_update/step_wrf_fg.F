C $Header: /u/gcmpack/MITgcm/pkg/wrf/step_wrf_fg.F,v 1.11 2004/10/22 14:52:14 molod Exp $
C $Name:  $

#include "planetWRF_OPTIONS.h"
       subroutine step_wrf_fg (myTime, myIter, myThid, dt)
c-----------------------------------------------------------------------
c  Subroutine step_wrf_fg - 'Wrapper' routine to advance
c        the physics state and make a 'first guess' at the new
c        value. At this point, increment with the physics 
c        tendency only. 
c        Also: Set up "bi, bj loop" and some timers and clocks here.
c Call: step_physics
c-----------------------------------------------------------------------
       implicit none
#include "SIZE.h"
#include "WRF_SIZE.h"
#include "gridalt_mapping.h"
#include "EEPARAMS.h"
#include "DYNVARS.h"
#include "GRID.h"
#ifdef WRF_ENABLE_TRACERS
#include "PTRACERS_SIZE.h"
#include "PTRACERS_PARAMS.h"
#include "PTRACERS_FIELDS.h"
#endif /* WRF_ENABLE_TRACERS */
#include "WRF_PHYS.h"

       integer myIter, myThid 
       _RL myTime

       integer bi, bj
       integer im1, im2, jm1, jm2, idim1, idim2, jdim1, jdim2
       _RL dt

       _RL tempij(sNx,sNy)
       integer i,j,L,iTracer, nTr

       _RL duphy(1-OLx:sNx+Olx,1-Oly:sNy+Oly,Nrphys,nSx,nSy)
       _RL dvphy(1-OLx:sNx+Olx,1-Oly:sNy+Oly,Nrphys,nSx,nSy)
       _RL dthphy(1-OLx:sNx+Olx,1-Oly:sNy+Oly,Nrphys,nSx,nSy)
#ifdef WRF_ENABLE_TRACERS
       _RL dtrphy(1-OLx:sNx+Olx,1-Oly:sNy+Oly,
     .                      Nrphys,nSx,nSy,PTRACERS_numInUse)
       _RL trphystep(1-Olx:sNx+Olx,1-Oly:sNy+Oly,Nrphys,
     .                                       PTRACERS_numInUse)
       _RL dtrphyfake(1-Olx:sNx+Olx,1-Oly:sNy+Oly,Nrphys,
     .                                       PTRACERS_numInUse)
#endif /* WRF_ENABLE_TRACERS */
c sphyfake is used by step_physics, if ptracer not defined
       _RL sphyfake(1-Olx:sNx+Olx,1-Oly:sNy+Oly,Nrphys,1)

       idim1 = 1-OLx
       idim2 = sNx+OLx
       jdim1 = 1-OLy
       jdim2 = sNy+OLy
       im1 = 1
       im2 = sNx
       jm1 = 1
       jm2 = sNy

       do bj = myByLo(myThid), myByHi(myThid)
       do bi = myBxLo(myThid), myBxHi(myThid)

          DO i = im1, im2
           DO j = jm1, jm2
            DO L = 1, Nrphys

               dthphy(i,j,L,bi,bj)= 0.0 _d 0
#ifdef WRF_RAD
     .         + RTHRATEN(i,j,L,bi,bj)
#endif /* WRF_RAD */
#ifdef WRF_PBL
     .         + RTHBLTEN(i,j,L,bi,bj)
#endif /* WRF_PBL */
 
               duphy(i,j,L,bi,bj)= 0.0 _d 0  
#ifdef WRF_PBL
     .         + RUBLTEN(i,j,L,bi,bj)
#endif /* WRF_PBL */

               dvphy(i,j,L,bi,bj)= 0.0 _d 0  
#ifdef WRF_PBL
     .         + RVBLTEN(i,j,L,bi,bj)
#endif /* WRF_PBL */

#ifdef WRF_ENABLE_TRACERS
            do iTracer = 1, PTRACERS_numInUse
               dtrphy(i,j,L,bi,bj,iTracer)=  
     .               gTrphy(i,j,L,bi,bj,iTracer)
            enddo
#endif /* WRF_ENABLE_TRACERS */
            ENDDO
           ENDDO
          ENDDO
    
c  Step forward the physics state using physics tendencies
#ifdef WRF_ENABLE_TRACERS
        nTr = PTRACERS_numInUse
        do iTracer = 1, PTRACERS_numInUse
        do L = 1,Nrphys
        do j = jm1,jm2
        do i = im1,im2
           trphystep(i,j,L,iTracer) = trphy(i,j,L,bi,bj,iTracer)
           dtrphyfake(i,j,L,iTracer) = dtrphy(i,j,L,bi,bj,iTracer)
        enddo
        enddo
        enddo
        enddo
        call step_physics(
     .      uphy (idim1:idim2,jdim1:jdim2,1:Nrphys,bi,bj),
     .      vphy (idim1:idim2,jdim1:jdim2,1:Nrphys,bi,bj),
     .      thphy(idim1:idim2,jdim1:jdim2,1:Nrphys,bi,bj),
     .      trphystep(idim1:idim2,jdim1:jdim2,1:Nrphys,1:nTr),
     .      dt,idim1,idim2,
     .      jdim1,jdim2,
     .      Nrphys,1,sNx,1,sNy,nTr,
     .      duphy(idim1:idim2,jdim1:jdim2,1:Nrphys,bi,bj),
     .      dvphy(idim1:idim2,jdim1:jdim2,1:Nrphys,bi,bj),
     .      dthphy(idim1:idim2,jdim1:jdim2,1:Nrphys,bi,bj),
     .      dtrphyfake(idim1:idim2,jdim1:jdim2,1:Nrphys,1:nTr))
        do iTracer = 1, PTRACERS_numInUse
        do L = 1,Nrphys
        do j = jm1,jm2
        do i = im1,im2
           trphy(i,j,L,bi,bj,iTracer) = trphystep(i,j,L,iTracer)
        enddo
        enddo
        enddo
        enddo
#else
        nTr = 1
        do iTracer = 1, nTr
        do L = 1,Nrphys
        do j = jm1,jm2
        do i = im1,im2
           sphyfake(i,j,L,iTracer) = 0.0 _d 0
        enddo
        enddo
        enddo
        enddo
        call step_physics(
     .      uphy (idim1:idim2,jdim1:jdim2,1:Nrphys,bi,bj),
     .      vphy (idim1:idim2,jdim1:jdim2,1:Nrphys,bi,bj),
     .      thphy(idim1:idim2,jdim1:jdim2,1:Nrphys,bi,bj),
     .      sphyfake(idim1:idim2,jdim1:jdim2,1:Nrphys,1:nTr),
     .      dt,idim1,idim2,
     .      jdim1,jdim2,
     .      Nrphys,1,sNx,1,sNy,nTr,
     .      duphy(idim1:idim2,jdim1:jdim2,1:Nrphys,bi,bj),
     .      dvphy(idim1:idim2,jdim1:jdim2,1:Nrphys,bi,bj),
     .      dthphy(idim1:idim2,jdim1:jdim2,1:Nrphys,bi,bj),
     .      sphyfake(idim1:idim2,jdim1:jdim2,1:Nrphys,1:nTr))
#endif /* WRF_ENABLE_TRACERS */
       enddo
       enddo

       return
       end
