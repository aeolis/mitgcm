C==/// the following two header files are mandatory!!!
#include "PACKAGES_CONFIG.h"
#include "CPP_OPTIONS.h"

C--//++++++++++++++++ Section 1 ++++++++++++++++++
C==/// Model Selection:  only change the definition of Mars or Titan ///==
#ifndef WRF_MARS
#define WRF_MARS
#endif /* WRF_MARS */

#ifndef WRF_TITAN
#undef WRF_TITAN
#endif /* WRF_TITAN */
C==/// End of Model Selection ///==


C--//++++++++++++++++ Section 2 ++++++++++++++++++
C==/// Physics Selection (shared by all models) ///==
C-- // the following should not be changed, unless it 
C-- // is very necessary (such as debugging !!!)
#ifndef WRF_RAD
#define WRF_RAD
#endif /*WRF_RAD*/

#ifndef EM_CORE
#define EM_CORE
#endif /* EM_CORE */

#ifndef WRF_SF
#define WRF_SF
#endif /* WRF_SF */

#ifndef WRF_PBL
#define WRF_PBL
#endif /* WRF_PBL */
C==/// End of Physics Selection ///==



C--//++++++++++++++++ Section 3 ++++++++++++++++++
C=== /// Start of configurable section for physics /// ===

C===// Mars //===
#ifdef WRF_MARS

#define WRF_CO2_CYCLE  /* CO2 Cyle */
c-//Choose one of the following RADTRAN schemes
#undef WRF_MARS_KDM    /* KDM Radtran Scheme */
#define  WRF_MARS_WBM  /* WBM Radtran Scheme */
c-//disable the regolith for water or not
#undef DISABLE_WATER_REG

#endif /* WRF_MARS */ 


C===// Mars or Titan //===
c-//default is WRF_SF_SIMPLE
#define WRF_SF_TGGCM   /* Surface Scheme */
C--// the water cycle could be water on Mars or methane on Titan
#undef WRF_TRACER_CYCLE
C=== /// End of configurable section for Mars physics /// ===



C--//++++++++++++++++ Section 4 ++++++++++++++++++
C=== /// Fixed definition, not user changeable!!! /// ====

#ifndef WRF_MARS
#undef WRF_CO2_CYCLE
#undef WRF_MARS_KDM
#undef WRF_MARS_WBM
#endif /* WRF_MARS */

C--// this part can not be modified !!! This overides the user defined 
C--// option if the dependance package does not present. Turning off 
C--// WRF_ENABLE_TRACERS intentionally will turn off the physics processes 
C--// that modify the tracer field (in physics grid), but it won't affect 
C--// the tracer advection as long as ALLOW_PTRACERS is defined (in dynamics grid).
#ifdef ALLOW_PTRACERS

#define WRF_ENABLE_TRACERS 

#else /* ALLOW_PTRACERS */

#undef WRF_ENABLE_TRACERS
#undef WRF_TRACER_CYCLE

#endif /* ALLOW_PTRACERS */

