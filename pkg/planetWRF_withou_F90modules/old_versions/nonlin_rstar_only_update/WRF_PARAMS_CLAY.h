C--  define the constants used by sfclay 

      REAL*8  VCONVC, CZO, OZO
      PARAMETER (VCONVC = 1.0    )
      PARAMETER (CZO    = 0.0185 )
      PARAMETER (OZO    = 1.59E-5)

      REAL*8  PSIMTB ( 0:1000 )
      REAL*8  PSIHTB ( 0:1000 )
