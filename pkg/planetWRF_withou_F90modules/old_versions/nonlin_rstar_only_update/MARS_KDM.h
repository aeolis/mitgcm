!WRF:MODEL_LAYER:PHYSICS
!c-mm!XREF:MEDIATION_LAYER:PHYSICS
!

!-----------------------------------------------------------------------
! This module contains most parameters needed to operate the Hadley    !
! radiation scheme.  For each variable type, there are two blocks of   !
! variables.  The first block contains fixed values that should never  !
! change.  The second block contains values that can be modified       !
! according to need.                                                   !
!-----------------------------------------------------------------------
!     MODULE module_kdm_parameters

!     USE module_model_constants

      INTEGER  ip_scf_solar_up
      INTEGER  ip_scf_solar_down
      INTEGER  ip_scf_ir_1d
      INTEGER  ip_scf_ir_2d
      INTEGER  npd_source_coeff
      INTEGER  ip_solar
      INTEGER  ip_infra_red
      INTEGER  ip_clear
      INTEGER  ip_dust
! THESE VARIBLES SHOULD REMAIN FIXED
      PARAMETER ( ip_scf_solar_up   = 1) ! Index of source coefficient for upward solar beam
      PARAMETER ( ip_scf_solar_down = 2) ! Index of source coefficient for downward solar beam
      PARAMETER ( ip_scf_ir_1d      = 1) ! Index of source coefficient for 1st difference of Planckian
      PARAMETER ( ip_scf_ir_2d      = 2) ! Index of source coefficient for 2nd difference of Planckian
      PARAMETER ( npd_source_coeff  = 2) ! Number of coefficients for two-stream sources
      PARAMETER ( ip_solar          = 1) ! Index for solar loop = 1 
      PARAMETER ( ip_infra_red      = 2) ! Index for IR loop = 2
      PARAMETER ( ip_clear          = 1) ! Index for loop with clear-sky conditions = 1
      PARAMETER ( ip_dust           = 2) ! Index for loop with dusty conditions = 2


      INTEGER  npd_band
      INTEGER  npd_species
      INTEGER  npd_aerosol_species
      INTEGER  n_band_ir
      INTEGER  n_band_solar
      INTEGER  n_tot_bands
      INTEGER  n_temps
      INTEGER  n_press
      INTEGER  n_terms
      INTEGER  n_mix
      INTEGER  t_offset
      INTEGER  t_spacing
      INTEGER  n_dust_data
! THESE VARIABLES CAN BE MODIFIED
      PARAMETER ( npd_band            = 7) ! Basically equals MAX(n_band_ir,n_band_solar).  Used for setting up arrays.
      PARAMETER ( npd_species         = 2) ! Number of "gases" in atmospere.  One for each variable gas plus one total for fixed gases
      PARAMETER ( npd_aerosol_species = 1) ! Number of "aerosols" in the atmosphere.  One for each species.
      PARAMETER ( n_band_ir           = 7) ! Number of bands in IR
      PARAMETER ( n_band_solar        = 7) ! Number of bands in solar
      PARAMETER ( n_tot_bands = n_band_ir+n_band_solar) ! Number of bands in k-coefficient array
      PARAMETER ( n_temps             = 71) ! Number of temperatures in k-coefficient array
      PARAMETER ( n_press             = 51) ! Number of pressures in k-coefficient array
      PARAMETER ( n_terms             = 32) ! Number of terms in each band.
      PARAMETER ( n_mix               = 6)  ! Number of mixing ratios in k-coefficient array
      PARAMETER ( t_offset            = 49) ! "Starting" temperature (-1) for cp array (to facilitate indexing)
      PARAMETER ( t_spacing           = 5)  ! Spacing between consecutive temperatures in k-table
      PARAMETER ( n_dust_data         = 584)! Number of rows in the Wolff dust parameter data file

      REAL*8   tol_machine
      REAL*8   tol_div
      REAL*8   sqrt_tol_machine
      REAL*8   log_tol_machine
      REAL*8   quad_correct_limit
      REAL*8   zs_lv
      REAL*8   zw_lv
! THESE VARIABLES SHOULD REMAIN FIXED
      PARAMETER ( tol_machine = 2.220446D-16) ! Machine tolerance, also equal to EPSILON(x) (intrinsic) where x is virtually any 
      PARAMETER ( tol_div     = 3.2D+1*tol_machine) ! conceivable number used in this code.  Architecture dependent, but this
      PARAMETER ( sqrt_tol_machine   = 1.490116D-8   ) ! is a pretty good number for most.
      PARAMETER ( log_tol_machine    = -36.0437      )
      PARAMETER ( quad_correct_limit = 6.828D-6      ) ! Equal to EXP(3.3D-01*log_tol_machine)
      PARAMETER ( zs_lv = -5.5) ! Altitude of the NLTE transition region in terms of ALOG(p), where p is in nbar
      PARAMETER ( zw_lv =  0.5) ! Width of the NLTE transition region.  The above value of zs_lv corresponds to
                                ! approximately 82 km for reference atmosphere 1 in Lopez-Valverde.

      REAL*8 band_wn_solar(n_band_solar+1)
      REAL*8 band_wn_ir   (n_band_ir+1   )
      DATA band_wn_solar /2222.22, 3087.37, 4030.63, 5370.57, 
     &                    7651.11, 12500.0, 25000.0, 41666.67/
      
      DATA band_wn_ir   /10.0, 166.667, 416.667, 625.370, 710.020,
     &                   833.333, 1250.0, 2222.22 /  ! Wavelength boundaries in IR (for CFST4)



!      use module_ra_mars_common
!      USE module_kdm_parameters
!      USE module_ra_valverde
!      USE module_state_description, ONLY: VALVERDE_LOOKUP, &
!                                          VALVERDE_FIT,    &
!                                          UV_GONGAL

      ! Make sure the declared header common variables don't interfere
      ! with similarly-named variables in routines that USE this module
      ! by making all variables PRIVATE and then making only the
      ! necessary subroutines PUBLICally available
!      PRIVATE
!      PUBLIC :: mars_kdm, kdminit

! From module_state_description

      INTEGER valverde_lookup
      INTEGER valverde_fit
      INTEGER uv_gongal
!-- ra_nlte_physics=41
      PARAMETER ( valverde_lookup = 41 )
!-- ra_nlte_physics=42
      PARAMETER ( valverde_fit    = 42 )
!-- ra_uv_physics=41
      PARAMETER ( uv_gongal       = 41 )

      INTEGER eddington
      INTEGER discrete_ord
      INTEGER elsasser
      INTEGER hemi_mean
!-- ra_sw_twostream==2
      PARAMETER ( eddington    = 2  )
!-- ra_sw_twostream==3
      PARAMETER ( discrete_ord = 3  )
!-- ra_lw_twostream==12
      PARAMETER ( elsasser     = 12 )
!-- ra_lw_twostream==13
      PARAMETER ( hemi_mean    = 13 )

      COMMON /KDM_INIT/ 
     &        kval_array, solar_flux_band,
     &        rayleigh_coefficient, 
     &        kCext_dust_solar, omega_dust_solar,
     &        asymmetry_dust_solar,
     &        kCext_dust_IR, omega_dust_IR, 
     &        asymmetry_dust_IR,
     &        weights, kCext_dust_ref

      REAL*8 kval_array( n_temps,n_press,n_terms,n_tot_bands,n_mix ) ! Full array of k-coefficients
      REAL*8 solar_flux_band     (n_band_solar)
      REAL*8 rayleigh_coefficient(n_band_solar)
      REAL*8 kCext_dust_solar    (n_band_solar)
      REAL*8 omega_dust_solar    (n_band_solar)
      REAL*8 asymmetry_dust_solar(n_band_solar)
 
      REAL*8 kCext_dust_IR     (n_band_ir)
      REAL*8 omega_dust_IR     (n_band_ir)
      REAL*8 asymmetry_dust_IR (n_band_ir)

      REAL*8 weights(n_terms)
      REAL*8 kCext_dust_ref

!     LOGICAL l_double  ! needs to be moved to data.wrf

      LOGICAL kval_debug 
      PARAMETER ( kval_debug = .false. )                                                             

!-- mars CO2 mixing ration
      REAL*8 co2_mixing_ratio
      PARAMETER ( co2_mixing_ratio = 0.953 )

      REAL*8 pa2bar
      PARAMETER ( pa2bar=1./101325. )
