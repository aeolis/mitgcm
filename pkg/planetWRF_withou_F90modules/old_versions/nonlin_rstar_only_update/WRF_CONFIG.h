C     define WRF configurations

      COMMON /WRF_CONFIG/
     &       cAdjFreqWRF, radFreq, 
     &       prejulian,
     &       mp_physics, du_physics,
     &       ISFTCFLX,
     &       ISFFLX, IFSNOW,
     &       nlte_physics,
     &       radiation, 
     &       remove_extra_vels, 
     &       diurnavg, 
     &       isothermal_top, 
     &       rayleighdrag,
     &       l_double, uv_physics,
     &       sw_correct,
     &       ice_ti_feedback,
     &       perm_h2o_cap,
     &       useShap_damping

C === Mars physics configuration ===
      INTEGER mp_physics
      INTEGER du_physics
      INTEGER ISFTCFLX
      INTEGER ISFFLX
      INTEGER IFSNOW

      LOGICAL radiation   
      LOGICAL remove_extra_vels
C mars KDM stuff, including single or double gaussian quadrature
      LOGICAL l_double
      LOGICAL sw_correct
      LOGICAL ice_ti_feedback
      LOGICAL perm_h2o_cap
      LOGICAL useShap_damping
      INTEGER nlte_physics
      INTEGER uv_physics
C === End of Mars configuration


C === Universal configuration ===
      REAL*8 cAdjFreqWRF
      REAL*8 radFreq
c prejulian is the Julian data before restart
      REAL*8 prejulian

      LOGICAL diurnavg
      LOGICAL isothermal_top
      LOGICAL rayleighdrag

      COMMON /WRF_RESTART/
     &       wrf_mdsio_read_pickup 

      LOGICAL wrf_mdsio_read_pickup

      COMMON /WRF_SURFDATA/
     &       albFile, inertiaFile, 
     &       roughnessFile, sstFile

      CHARACTER*(MAX_LEN_FNAM) albFile
      CHARACTER*(MAX_LEN_FNAM) inertiaFile
      CHARACTER*(MAX_LEN_FNAM) roughnessFile
      CHARACTER*(MAX_LEN_FNAM) sstFile
C === 
