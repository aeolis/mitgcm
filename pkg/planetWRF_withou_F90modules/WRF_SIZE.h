      INTEGER num_soil_layers
      PARAMETER (num_soil_layers = 12 )

      INTEGER ids,ide, jds,jde, kds,kde,
     &        ims,ime, jms,jme, kms,kme,
     &        its,ite, jts,jte, kts,kte
C--   index for domain size of tiles and number of tiles
      PARAMETER ( ids = 1-OLx   )
      PARAMETER ( ide = sNx+OLx )
      PARAMETER ( jds = 1-OLy   )
      PARAMETER ( jde = sNy+OLy )
      PARAMETER ( kds = 1       )
      PARAMETER ( kde = Nr+1    )
      PARAMETER ( ims = 1-OLx   )
      PARAMETER ( ime = sNx+OLx )
      PARAMETER ( jms = 1-OLy   )
      PARAMETER ( jme = sNy+OLy )
      PARAMETER ( kms = 1       )
      PARAMETER ( kme = Nr+1    )
      PARAMETER ( its = 0       )
      PARAMETER ( ite = sNx+1   )
      PARAMETER ( jts = 0       )
      PARAMETER ( jte = sNy+1   )
      PARAMETER ( kts = 1       )
      PARAMETER ( kte = Nr      )

