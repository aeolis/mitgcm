C!WRF:MODEL_LAYER:PHYSICS
C!
CMODULE module_sf_tggcm

CUSE module_model_constants

CCONTAINS
#include "planetWRF_OPTIONS.h"

!----------------------------------------------------------------
      SUBROUTINE TGGCM(T3D,
     &              QV3D,
     &              P3D,FLHC,FLQC,                   
     &              PSFC,XLAND,TMN,HFX,RNET_2D,QFX,LH,GRDFLX,TSK,
     &              QSFC,GSW,GLW,CAPG,THC,SNOWC,EMISS,MAVAIL,    
     &              DELTSM,ROVCP,XLV,DT,IFSNOW,               
     &              STBOLT,P2SI,XLON,XLAT,                            
     &              TSLB,ZS,DZS,num_soil_layers,radiation,       
     &              porosity,soil_den,ss_ice,                    
     &              P1000mb,rhogrnd,co2ice_threshold,co2_lheat, 
     &              co2ice,co2sublimate,h2oice,h2oice_threshold, 
     &              perm_co2_cap,
     &              iplanet,ice_ti_feedback,kdirt, 
     &              ids,ide, jds,jde, kds,kde,                   
     &              ims,ime, jms,jme, kms,kme,                   
     &              its,ite, jts,jte, kts,kte                    )
!----------------------------------------------------------------
      IMPLICIT NONE
!----------------------------------------------------------------
!                                                                        
!     SUBROUTINE TGGCM CALCULATES THE GROUND TEMPERATURE TENDENCY 
!     ACCORDING TO THE GFDL MARS GCM CODE
!                                                                      
!----------------------------------------------------------------          
!-- T3D         temperature (K)
!-- QV3D        3D water vapor mixing ratio (Kg/Kg)
!-- P3D         3D pressure (Pa)
!-- FLHC        exchange coefficient for heat (m/s)
!-- FLQC        exchange coefficient for moisture (m/s)
!-- PSFC        surface pressure (Pa)
!-- XLAND       land mask (1 for land, 2 for water)
!-- TMN         soil temperature at lower boundary (K)
!-- HFX         upward heat flux at the surface (W/m^2)
!-- QFX         upward moisture flux at the surface (kg/m^2/s)
!-- LH          latent heat flux at the surface (W/m^2)
!-- TSK         surface temperature (K)
!-- GSW         downward short wave flux at ground surface (W/m^2)      
!-- GLW         downward long wave flux at ground surface (W/m^2)
!-- CAPG        heat capacity for soil (J/K/m^3)
!-- THC         thermal inertia (Cal/cm/K/s^0.5)
!-- SNOWC       flag indicating snow coverage (1 for snow cover)
!-- EMISS       surface emissivity (between 0 and 1)
!-- DELTSM      time step (second)
!-- ROVCP       R/CP
!-- XLV         latent heat of melting (J/kg)
!-- DT          time step (second)
!-- IFSNOW      ifsnow=1 for snow-cover effects
!-- STBOLT      Stefan-Boltzmann constant (W/m^2/K^4)
!-- TSLB        soil temperature in 5-layer model
!-- ZS          depths of centers of soil layers
!-- DZS         thicknesses of soil layers
!-- POROSITY    porosity of soil layers
!-- SOIL_DEN    bulk density of soil layers (accounting for porosity) (kg/m3)
!-- SS_ICE      density of ice in pore space
!-- num_soil_layers   the number of soil layers
!-- xlon        Geographic longitude (deg)
!-- xlat        Geographic latitude  (deg)
!-- perm_co2_cap     Logical parameter for turning on/off southern CO2 ice cap
!-- ids         start index for i in domain
!-- ide         end index for i in domain
!-- jds         start index for j in domain
!-- jde         end index for j in domain
!-- kds         start index for k in domain
!-- kde         end index for k in domain
!-- ims         start index for i in memory
!-- ime         end index for i in memory
!-- jms         start index for j in memory
!-- jme         end index for j in memory
!-- kms         start index for k in memory
!-- kme         end index for k in memory
!-- its         start index for i in tile
!-- ite         end index for i in tile
!-- jts         start index for j in tile
!-- jte         end index for j in tile
!-- kts         start index for k in tile
!-- kte         end index for k in tile
!----------------------------------------------------------------

C====INPUT/OUTPUT====

      INTEGER ids,ide, jds,jde, kds,kde, 
     &        ims,ime, jms,jme, kms,kme, 
     &        its,ite, jts,jte, kts,kte

      INTEGER num_soil_layers, iplanet
      LOGICAL radiation, ice_ti_feedback

      INTEGER IFSNOW

!
      _RL DT,XLV,ROVCP,DELTSM

      _RL STBOLT
      _RL P1000mb,P2SI,rhogrnd,kdirt,
     &       co2ice_threshold,co2_lheat,
     &       h2oice_threshold

      _RL TSLB( ims:ime , jms:jme, num_soil_layers )   
      _RL CAPG( ims:ime , jms:jme, num_soil_layers )

      _RL SS_ICE  ( ims:ime, jms:jme, num_soil_layers )
      _RL porosity( ims:ime, jms:jme, num_soil_layers )
      _RL soil_den( ims:ime, jms:jme, num_soil_layers )

      _RL ZS (num_soil_layers) 
      _RL DZS(num_soil_layers)

      _RL QV3D( ims:ime, jms:jme, kms:kme ) 

      _RL P3D ( ims:ime, jms:jme, kms:kme ) 
      _RL T3D ( ims:ime, jms:jme, kms:kme )    
!
      _RL SNOWC ( ims:ime, jms:jme )
      _RL XLAND ( ims:ime, jms:jme )
      _RL EMISS ( ims:ime, jms:jme )
      _RL MAVAIL( ims:ime, jms:jme )
      _RL TMN   ( ims:ime, jms:jme )
      _RL GSW   ( ims:ime, jms:jme )
      _RL GLW   ( ims:ime, jms:jme )
      _RL THC   ( ims:ime, jms:jme )
!
      _RL HFX   ( ims:ime, jms:jme )
      _RL QFX   ( ims:ime, jms:jme )
      _RL LH    ( ims:ime, jms:jme )
      _RL GRDFLX( ims:ime, jms:jme )
      _RL TSK   ( ims:ime, jms:jme )
      _RL QSFC  ( ims:ime, jms:jme )
! A variable to help calculate surface heat balance.
      _RL RNET_2D( ims:ime, jms:jme )

      _RL PSFC  ( ims:ime, jms:jme )  
!
      _RL FLHC  ( ims:ime, jms:jme )
      _RL FLQC  ( ims:ime, jms:jme )

      _RL co2ice      ( ims:ime, jms:jme )
      _RL co2sublimate( ims:ime, jms:jme )
      _RL h2oice      ( ims:ime, jms:jme )

      _RL xlon  ( ims:ime, jms:jme )
      _RL xlat  ( ims:ime, jms:jme )

      LOGICAL perm_co2_cap

C====Local Variables====

      _RL QV1D( ims:ime )
      _RL P1D ( ims:ime )
      _RL T1D ( ims:ime )
 
      _RL gkz  ( num_soil_layers )
      _RL adelz( num_soil_layers )
      _RL bdelz( num_soil_layers )
      _RL cdelz( num_soil_layers )
   
      INTEGER I,J,L

      DO l= 1, num_soil_layers-1
         gkz(l)= 2.0/( dzs(l)+dzs(l+1) )
      ENDDO
      adelz(1)= 0.0
      bdelz(1)= 2.0/( (dzs(1)+dzs(2))*dzs(1) )
      cdelz(1)= 2.0/( (dzs(1)+dzs(2))*dzs(1) )
      DO l= 2, num_soil_layers-1
         adelz(l)=     gkz(l-1)         /dzs(l)
         bdelz(l)=   ( gkz(l-1)+gkz(l) )/dzs(l)
         cdelz(l)=              gkz(l)  /dzs(l)
      ENDDO

      DO J=jts,jte

         DO i=its,ite
            T1D(i) =T3D(i,j,1)
            QV1D(i)=QV3D(i,j,1)
            P1D(i) =P3D(i,j,1)
         ENDDO
         CALL TGGCM1D(T1D,QV1D,P1D,FLHC(ims,j),FLQC(ims,j),     
     &        PSFC(ims,j),XLAND(ims,j),TMN(ims,j),HFX(ims,j),    
     &        RNET_2D(ims,j),QFX(ims,j),TSK(ims,j),QSFC(ims,j),  
     &        GRDFLX(ims,j),LH(ims,j),GSW(ims,j),GLW(ims,j),     
     &        CAPG(ims:ime,j,1:num_soil_layers),
     &        THC(ims,j),SNOWC(ims,j),EMISS(ims,j),  
     &        MAVAIL(ims,j),DELTSM,ROVCP,XLV,DT,IFSNOW,       
     &        STBOLT,P2SI,rhogrnd,co2ice_threshold,co2_lheat,    
     &        co2ice(ims,j),co2sublimate(ims,j),h2oice(ims,j),   
     &        h2oice_threshold,iplanet,ice_ti_feedback,
     &        XLON(ims,j),XLAT(ims,j),perm_co2_cap,
!c-mm          TSLB(ims,1,j),ZS,DZS,num_soil_layers,porosity,       &
     &        TSLB(ims:ime,j,1:num_soil_layers),ZS,DZS,num_soil_layers,
     &        porosity(ims:ime,j,1:num_soil_layers),
     &        soil_den(ims:ime,j,1:num_soil_layers),
     &        ss_ice(ims:ime,j,1:num_soil_layers),
     &        kdirt,radiation,           
     &        P1000mb,gkz,adelz,bdelz,cdelz,                     
     &        ids,ide, jds,jde, kds,kde,                        
     &        ims,ime, jms,jme, kms,kme,                       
     &        its,ite, jts,jte, kts,kte,j                       )

      ENDDO

      RETURN
      END

!----------------------------------------------------------------
      SUBROUTINE TGGCM1D(T1D,QV1D,P1D,FLHC,FLQC,                 
     &              PSFCPA,XLAND,TMN,HFX,RNET_2D,QFX,TSK,QSFC,   
     &              GRDFLX,LH,GSW,GLW,CAPG2D,THC,SNOWC,EMISS,    
     &              MAVAIL,DELTSM,ROVCP,XLV,DT,IFSNOW,        
     &              STBOLT,P2SI,rhogrnd,co2ice_threshold,        
     &              co2_lheat,co2ice,co2sublimate,h2oice,        
     &              h2oice_threshold,iplanet,ice_ti_feedback,
     &              XLON,XLAT,perm_co2_cap,
     &              TSLB2D,ZS,DZS,num_soil_layers,porosity,      
     &              soil_den,ss_ice,kdirt,radiation,                   
     &              P1000mb,gkz,adelz,bdelz,cdelz,               
     &              ids,ide, jds,jde, kds,kde,                   
     &              ims,ime, jms,jme, kms,kme,                 
     &              its,ite, jts,jte, kts,kte ,j            )
!----------------------------------------------------------------
      IMPLICIT NONE
!----------------------------------------------------------------
!                                                                        
!     SUBROUTINE TGGCM CALCULATES THE GROUND TEMPERATURE TENDENCY 
!     ACCORDING TO THE GFDL MARS GCM CODE
!                                                                      
!----------------------------------------------------------------          

      INTEGER ids,ide, jds,jde, kds,kde, 
     &        ims,ime, jms,jme, kms,kme, 
     &        its,ite, jts,jte, kts,kte, j

      INTEGER num_soil_layers, iplanet
      LOGICAL radiation, ice_ti_feedback

      INTEGER IFSNOW
!
      _RL  DT,XLV,ROVCP,DELTSM

      _RL  STBOLT
      _RL  P1000mb,P2SI,rhogrnd,kdirt 
      _RL  co2ice_threshold,co2_lheat
      _RL  h2oice_threshold

      _RL TSLB2D ( ims:ime , 1:num_soil_layers )
      _RL CAPG2D ( ims:ime , 1:num_soil_layers )

      _RL SS_ICE  ( ims:ime , 1:num_soil_layers )
      _RL porosity( ims:ime , 1:num_soil_layers )
      _RL soil_den( ims:ime , 1:num_soil_layers )

      _RL ZS  (1:num_soil_layers)
      _RL DZS (1:num_soil_layers)   

      _RL gkz  (1:num_soil_layers)
      _RL adelz(1:num_soil_layers)
      _RL bdelz(1:num_soil_layers)
      _RL cdelz(1:num_soil_layers)
!
      _RL HFX    ( ims:ime )
      _RL QFX    ( ims:ime )
      _RL LH     ( ims:ime )
      _RL GRDFLX ( ims:ime )
      _RL TSK    ( ims:ime )
      _RL QSFC   ( ims:ime )
! A 2-D variable to propagate the net radiation at the surface.
      _RL RNET_2D( ims:ime ) 
!
      _RL SNOWC  ( ims:ime )
      _RL XLAND  ( ims:ime )
      _RL EMISS  ( ims:ime )
      _RL MAVAIL ( ims:ime )
      _RL TMN    ( ims:ime )
      _RL GSW    ( ims:ime )
      _RL GLW    ( ims:ime )
      _RL THC    ( ims:ime )
!
      _RL QV1D   ( ims:ime ) 
      _RL P1D    ( ims:ime )
      _RL T1D    ( ims:ime )
!
      _RL PSFCPA ( ims:ime )
!
      _RL FLHC   ( ims:ime )
      _RL FLQC   ( ims:ime ) 

      _RL co2ice      ( ims:ime )
      _RL co2sublimate( ims:ime )
      _RL h2oice      ( ims:ime ) 

      _RL xlon        ( ims:ime )
      _RL xlat        ( ims:ime )

      _RL tfrost_co2, rsat

      LOGICAL perm_co2_cap

! Infrared emissivity of CO2 ice
!REAL, PARAMETER :: emiss_ice= 0.8
      _RL emiss_co2ice_north
      _RL emiss_co2ice_south
      PARAMETER ( emiss_co2ice_north = 0.58  )
      PARAMETER ( emiss_co2ice_south = 0.90  )

! Infrared emissivity of H2O ice
! I have no idea what this value should be, and so it's not used below
! However, I really should look it up, shouldn't I?  Anyone else want to
! volunteer?
      _RL emiss_h2oice
      PARAMETER ( emiss_h2oice= 1.0 )

      _RL mw_air, mw_vap
      PARAMETER ( mw_air = 43.49   )
      PARAMETER ( mw_vap = 18.0152 )

      _RL a0, a1, a2, a3, a4, a5, a6
      PARAMETER ( a0 = 6.107799961     )
      PARAMETER ( a1 = 4.436518521e-01 )
      PARAMETER ( a2 = 1.428945805e-02 )
      PARAMETER ( a3 = 2.650648471e-04 )
      PARAMETER ( a4 = 3.031240396e-06 )
      PARAMETER ( a5 = 2.034080948e-08 )
      PARAMETER ( a6 = 6.136820929e-11 )

      _RL c1, c2, c3, eis
      PARAMETER ( c1  = 9.09718 )
      PARAMETER ( c2  = 3.56654 )
      PARAMETER ( c3  = 0.876793)
      PARAMETER ( eis = 6.1071  )

!c-mm  GK is a diffusivity
      _RL ADIAG( num_soil_layers )
      _RL BDIAG( num_soil_layers )
      _RL CDIAG( num_soil_layers )
      _RL RHS  ( num_soil_layers )
!c-mm Move GK to here from 0-D
      _RL GK   ( num_soil_layers )
      _RL cfl  ( num_soil_layers )

      INTEGER I,K,L
      INTEGER num_small_t_steps, dstp

      _RL TG0, CRD
      _RL kio, ki, kice, fa
      _RL PS1,THG,QSG,HFXT,QFXT,THCON
      _RL RNET, UPFLUX
      _RL t1,ew,q1,ewrhs,emissv
      _RL dtemp, rtlatent, tcrit
      _RL dt_ss

! Change the following variable to .TRUE. to turn off the hardwired
! minimum temperature for the ground (i.e., the CO2 condensation temp.)
      LOGICAL no_co2_cycle 
   
      no_co2_cycle    = .FALSE.

      IF (iplanet == 61) THEN 
         no_co2_cycle    = .TRUE.
         ICE_TI_FEEDBACK = .FALSE.
         perm_co2_cap = .FALSE.
      ENDIF

      DO i=its,ite
         ! TSK is Temperature at ground sfc
         TG0=TSK(I)

         ! Mars GCM sub/surface temperature code
         
         LH(I) = 0.

         DO l=1,num_soil_layers
            IF (iplanet == 61) THEN
               ! Tokano (1999) has cp=1400 J/K/kg for porous icy regolith
               ! and rock-ice mixture
               CAPG2D(I,l)=1400.0*soil_den(i,l)    ! Heat capacity of the soil, J/(m^3 K)
            ELSE
               ! 837.2 comes from MGCM value of 0.2 cal/g/K for soil heat capacity
               ! 837.2 = 0.2 cal/g/K * 4.186 J/cal * 1000.g/kg
               CAPG2D(i,l)=837.2*soil_den(i,l)
            ENDIF
         ENDDO

         num_small_t_steps=1

         ! c-yl, conceptually using small time step for the subsurface only is not correct. 
         !       This is because the heat exchange between surface and the atmosphere is to minimize 
         !       the temperature difference between the two. For each small time step, the sensible 
         !       heat flux needs to be passed to PBL mixing, which changes the downward IR flux GLW. 
         !       Accumulating the sensible heat flux for all small soil time step without considering 
         !       the changes of GLW is not correct. That is, we are excessively oversetimating the 
         !       sensible heat flux during one model time step if soil time step is significantly smaller. 
         ! c-yl, What's more physical is to iterate within the subsurface without accumulating sensible 
         !       heat flux and assuming the sensible heat flux from previous timestep is constant. 
         !       After running all the small timesteps during one large timestep, the end state of 
         !       surface temperature can be used to update the sensible heat flux passing to the PBL mixing.
         
         ! c-yl, for now, we iterate all small timesteps before calculating the sensible heat flux for next time 
         !       step within the subsurface, till we find a better solution
 101     dt_ss=dt/dble(num_small_t_steps)
         ! c-yl, start loop over small soil timesteps
         !       forwarding in time for this loop is implicit (not implicit scheme!) 
         DO dstp=1,num_small_t_steps 
            DO l=1,num_soil_layers
               IF (ICE_TI_FEEDBACK) THEN
               !c-mm The next line (gk=) is calculating a conductivity *temporarily*, even though gk is a diffusivity variable
               !c-mm    The difference is that conductivity = thc**2/capg2d while diffusivity = thc**2/capg2d**2.  I am
               !c-mm    intentionally doing this since it gets overwritten correctly later.
               !c-mm As of 5-2-11, I've triple-checked this code and it is derived correctly.
                 gk(l)=(thc(i)**2.)/capg2d(i,l)
                 kio=porosity(i,l)*gk(l)
     &                          *kdirt/(kdirt-(1-porosity(i,l))*gk(l))
                 IF (porosity(i,l) == 0.0) THEN   !c-mm  This equation, from Mellon et al., 1997, falls apart
                    fa = 0.0                      !c-mm     when porosity is zero, but in this case, set fa=0.0
                 ELSE
                    fa=MAX(SQRT(ss_ice(i,l)/(917.*porosity(i,l))),0.0) !c-mm  If negative, just set to zero
                 ENDIF
                 kice=(0.4685+488.19/tslb2d(i,l))
                 ki=(1-fa)*kio+fa*kice
                 !c-mm This is a correctly calculated diffusivity (note the extra 1/capg2d term)
                 gk(l)=(1./capg2d(i,l))*kdirt*ki
     &                /((1.-porosity(i,l))*ki+porosity(i,l)*kdirt)
               ELSE
                 GK(l)=(THC(I)/CAPG2D(I,l))**2.   ! CAPG is Cp*Rho and gk is a diffusivity
               ENDIF
            ENDDO
            cfl=gk*dt_ss/(dzs*dzs)      !c-mm  Calculate CFL criteria at all depths.
            IF ((MAXVAL(cfl)) > 1.0 ) THEN
               !c-mm  I've added an extra '+1' to the statement below to ensure that when we calculate
               !c-mm     the cfl value above, we will always be below 1.0
               num_small_t_steps=INT(CEILING(dt*MAXVAL(gk/(dzs*dzs))))+1
               GO TO 101
            ENDIF

            ! Calculate diffusive heating rate and put in GRDFLX
            CALL CALCD2TDZ2(ZS,DZS,TSLB2D(I,1:num_soil_layers),
     &                         GK(1),GRDFLX(I),num_soil_layers)
            !TCRIT = 149.2 + 6.48*LOG( PSFCPA(I)*1.35E-3 )
            !c-yl, use MCS team's formula
            TCRIT = tfrost_co2 (PSFCPA(I))

            dtemp = 100.
            ! Check to make sure that the difference in temperatures used by the
            ! radiation calculation (UPFLUX) matches that returned by the diffusion
            ! updater.  Tolerance is 1 K.
            !t_consistency_check: 
            DO WHILE (dtemp > 1.)
            ! ADIAG, BDIAG, and CDIAG are modified by TRDSLV, so they must
            ! be regenerated before each call to TRDSLV
               DO L= 1, num_soil_layers-1
                  ADIAG(L)=     - GK(l)*0.5*DT_SS*ADELZ(L)
                  BDIAG(L)= 1.0 + GK(l)*0.5*DT_SS*BDELZ(L)
                  CDIAG(L)=     - GK(l)*0.5*DT_SS*CDELZ(L)
               ENDDO
               CDIAG(num_soil_layers-1)= - GK(num_soil_layers-1)
     &                           *DT_SS*CDELZ(num_soil_layers-1)

               IF (iplanet == 61) THEN
                  emissv = emiss(i)
               ELSE IF ( co2ice(i) > CO2ICE_THRESHOLD )  THEN
                  IF (XLAT(i) > 0.) THEN
                     emissv=emiss_co2ice_north
                  ELSE
                     emissv=emiss_co2ice_south
                  END IF
               ! If we are modifying surface emissivity for ice, here is where
               ! we would do it.  But since emiss(i) is set to 1. everywhere now
               ! we don't need to do this because emiss_h2oice would also be 1. (?)
      !         ELSE IF (h2oice(i) > H2OICE_THRESHOLD) THEN
      !            emissv=emiss_h2oice
               ELSE
                  emissv=emiss(i)
               ENDIF
               !
               !-----COMPUTE THE SURFACE ENERGY BUDGET:
               !

               UPFLUX = STBOLT*TG0**4
               RNET   = GSW(I)+EMISSV*(GLW(I)-UPFLUX)
      ! c-asoto RNET_2D is added to propagate the RNET variable all the way to the output files. 
      ! c-asoto RNET is needed for polar heat balance calculations.
               RNET_2D(I) = RNET  

               CRD    = 1.0/( CAPG2D(I,1)*DZS(1) )
               RHS(1) = DT_SS*CRD*( RNET - HFX(I) ) 
     &                     + (2.0-BDIAG(1))*TSLB2D(I,1)
     &                     - CDIAG(1)*TSLB2D(I,2)
               DO L= 2, num_soil_layers-1
                  RHS(L)=  -ADIAG(L)* TSLB2D(I,L-1) + 
     &                    (2.0-BDIAG(L))*TSLB2D(I,L)     
     &                       - CDIAG(L) *TSLB2D(I,L+1)
               ENDDO
            
               CALL TRDSLV1D(num_soil_layers-1,ADIAG,BDIAG,CDIAG,RHS)
               dtemp = ABS(RHS(1)-TG0)
               IF (dtemp > 1.) THEN
                  TG0=0.5*(RHS(1)+TG0)
               ELSE
                  TG0= RHS(1)
                  DO L= 1, num_soil_layers-1
                     TSLB2D(I,L)=RHS(L)
                  ENDDO
               ENDIF

            ENDDO !t_consistency_check
            ! c-yl, loop over small timesteps within subsurface, see comments above
            TSLB2D(I,1)=TG0
            TSK(I)=TG0
         ENDDO  !c-yl, end loop over small soil timesteps

         !co2_check: (c-yl, for model time step) 
         IF (.not.no_co2_cycle) THEN
         !  If we have turned off the co2_cycle, we must make sure that
         !  we don't adjust the surface temperatures to the condensation
         !  temperature of CO2.

         !  Surface CO2 microphysics/snow code
            co2sublimate(i)= 0.0

            rtlatent= 1.0/( CRD*co2_lheat*dt )
            ! co2sublimate is positive (+) if loss from surface
            co2sublimate(i) = ( tg0 - tcrit )*rtlatent ! kg/(m^2 s)
            ! Save this useful diagnostic in the LH variable for output
            lh(i) = co2sublimate(i)*co2_lheat
            ! Here we will only consider setting of the surface temperature, tg0
            ! We will modify the atmospheric CO2 in the microphysics section
            ! And we will remember how much to change atmospheric CO2 with
            ! the co2sublimate variable

            ! Consider condensation/sublimation at the surface

            ! Surface wanted to be colder than critical temperature
            ! Steal heat from atmosphere by condensing CO2 on surface, and
            ! adjust column mass later (in microphysics)
            IF (co2sublimate(i) < 0.) tg0 = tcrit

               ! Surface is warm enough to sublime any surface ice, if present
            IF (co2sublimate(i) > 0.) THEN
               ! Is there any ice present on the surface?
               IF (co2ice(i) > 0.) THEN
                  ! 2 cases (partial and complete) for sublimation of surface ice
                  IF (co2sublimate(i)*dt < co2ice(i)) THEN
                     ! Case 1: More than enough ice to accomodate mass flux of CO2
                     !         but ice remains on surface, so temperature remains
                     !         at freezing/critical point.
                     tg0 = tcrit
                  ELSE
                     ! Case 2: Sublime remaining snow and adjust temperature.
                     !         Temperature change is less than previously
                     !         calculated because some of the heating is used
                     !         to sublime CO2.
                     !  c-yl, when using small soil time steps, 
                     !         [d(tg0)/d(t)]_small*DT=co2ice(i)*co2_lheat*crd
                     !         For simplicity, we should correct the ground temperature 
                     !         after all small soil time steps at location i
                        tg0 = tg0 - co2ice(i)*co2_lheat*crd
                     !  c-yl, CO2 sublimation only occurs once during DT, could 
                     !         do this near the end of i loop
                        co2sublimate(i) = co2ice(i)/dt
                  ENDIF
               ELSE
                     ! There's no ice, so we can't sublimate anything
                     co2sublimate(i) = 0.
               ENDIF
            ENDIF
         ENDIF !co2_check
c-yl  we make sure the surface temperature at permanent southern CO2 ice cap 
c-yl  is no larger than the sublimation temperature in this region (Bibring et al., 2004)
         IF (perm_co2_cap) THEN
c-yl      In MITgcm longtitude is from -180 to 180 deg 
c-yl      and latitude is from -90 to 90 deg
          IF ( (xlon(i).le.0.0).and.(xlon(i).ge.-90.0)
     &          .and.(xlat(i).le.-82.5) ) THEN
              tg0=tcrit
          ENDIF ! set tsk to be the critical value 
         ENDIF ! permanent co2 ice cap
         !  Update TSLA/B(,,1) with surface temperature modifications
         !  c-yl, the update is for temperature modified by CO2 sublimation
         TSLB2D(I,1)=TG0
         TSK(I)=TG0
 
         IF ((MAVAIL(I).LT.1.e-3) .OR. (iplanet == 61)) THEN
            QSG=0.
         ELSE
             IF ( TSLB2D(I,1) .GT. 273.16 ) THEN
                T1 = TSLB2D(I,1) - 273.16
                EW = A0 + T1 * (A1 + T1 * (A2 + T1 * (A3 + T1 * 
     &                         (A4 + T1 * (A5 + T1 *  A6)))))
             ELSE
                EWRHS = - C1 * (273.16 / TSLB2D(I,1) - 1.) 
     &                  - C2 * LOG10(273.16 / TSLB2D(I,1)) 
     &                  + C3 * (1. - TSLB2D(I,1) / 273.16) 
     &                  + LOG10(EIS)
                EW = 10.**EWRHS
             ENDIF
             ! EW is in mbar -- need to convert to Pa
             EW=100.*MAX(EW,0.)
             Q1 = MW_VAP*EW / (MW_VAP*EW + MW_AIR*(PSFCPA(I)-EW))
             ! Q will refer to mass of water per mass of total air (not just
             ! dry air) -- real amount of water mass is extremely small, so the
             ! difference is negligible EXCEPT when calculating fluxes near the
             ! saturation point ...
             ! Q1 = MW_VAP*EW / (MW_VAP*EW + MW_AIR*PSFCPA(I))
             ! QSG=Q1/(1.-Q1)
             QSG = EW/PSFCPA(I)
         ENDIF

         THCON=(P1000mb/PSFCPA(I))**ROVCP
         THG=TSLB2D(I,1)*THCON

         THCON=(P1000mb/P1D(I))**ROVCP
         ! UPDATE FLUXES FOR NEW GROUND TEMPERATURE
         ! c-yl, the new ground temperature is the end state of loop over 
         !       small soil timesteps
         HFXT=FLHC(I)*(THG-T1D(I)*THCON)
         QFXT=FLQC(I)*(QSG-QV1D(I))
         ! SUM HFX AND QFX OVER SOIL TIMESTEP
         HFX(I)=HFX(I)+HFXT
         QFX(I)=QFX(I)+QFXT

         ! The following are as seen in module_sf_slab.F
         ! QSFC and CHKLOWQ needed by Eta PBL
         IF (FLQC(I) > 0.) THEN
            QSFC(I)=QV1D(I)+QFX(I)/FLQC(I)
         ELSE
            ! If FLQC=0, then QFXT=0, but does QFX=0???
            ! I think we are assuming that an FLQC=0 means QFX=0...
            QSFC(I)=QV1D(I)+QFX(I)
         ENDIF
         ! We didn't pass in CHKLOWQ, so we'll do this in module_surface_driver
         ! Other subroutines use CHKLOWQ=1 or 0, but who the heck knows...
         ! The Registry is, as usual, extremely undescriptive of these variables
         !CHKLOWQ(I)=MAVAIL(I)
      ENDDO   !c-mm  i=its,ite
    
      RETURN
      END

C!================================================================
      SUBROUTINE trdslv1d(n,a,b,c,x)
C!----------------------------------------------------------------
      IMPLICIT NONE
C!----------------------------------------------------------------
      INTEGER n
      _RL a(1:n),b(1:n),c(1:n),x(1:n)
      INTEGER i

      b(1) = 1.0/b(1)
      c(1) = c(1)*b(1)
      DO i= 2, n
         b(i) = 1.0/( b(i)-a(i)*c(i-1) )
         c(i)= c(i)*b(i)
      END DO

      x(1) = x(1)*b(1)
      DO i= 2, n
         x(i) = ( x(i)-a(i)*x(i-1) )*b(i)
      END DO

      DO i= n-1, 1, -1
         x(i) = x(i)-c(i)*x(i+1)
      END DO

      RETURN
      END

!================================================================
      SUBROUTINE trdslv(n,a,b,c,x,iflg,mlx,          
     &                ids,ide, jds,jde, kds,kde,    
     &                ims,ime, jms,jme, kms,kme,   
     &                its,ite, jts,jte, kts,kte     )
!----------------------------------------------------------------
!
!     if  iflg = 0  this subroutine computes the lu-decomposition
!     of the input matrix and then solves for the solution vector  x.
!
!     if  iflg = 1  the calculation of the lu-decomposition is skipped
!     and the solution vector  x  is calculated.
!
!----------------------------------------------------------------
      IMPLICIT NONE
!----------------------------------------------------------------
      INTEGER ids,ide, jds,jde, kds,kde, 
     &        ims,ime, jms,jme, kms,kme,
     &        its,ite, jts,jte, kts,kte

      INTEGER n, mlx, iflg

      _RL a(ims:ime,mlx)
      _RL b(ims:ime,mlx) 
      _RL c(ims:ime,mlx)
      _RL x(ims:ime,mlx)

      INTEGER i,j

! obtain the lu-decomposition
      IF( IFLG.EQ.0 ) THEN
         DO i=its,ite
            b(i,1) = 1.0/b(i,1)
            c(i,1) = c(i,1)*b(i,1)
            DO j= 2, n
               b(i,j) = 1.0/( b(i,j)-a(i,j)*c(i,j-1) )
               c(i,j)= c(i,j)*b(i,j)
            ENDDO
         ENDDO
      ENDIF

! ----come here for back-solving----

      DO i=its,ite
         x(i,1) = x(i,1)*b(i,1)
         DO j= 2, n
            x(i,j) = ( x(i,j)-a(i,j)*x(i,j-1) )*b(i,j)
         ENDDO

         DO j= n-1, 1, -1
            x(i,j) = x(i,j)-c(i,j)*x(i,j+1)
         ENDDO
      ENDDO

      RETURN
      END 

!================================================================
      SUBROUTINE CALCD2TDZ2(ZS,DZS,TSLB,GK,GRDFLX,n)
      IMPLICIT NONE
      INTEGER n
      _RL zs  (n)
      _RL dzs (n)
      _RL tslb(n)
      _RL gk, grdflx

      _RL z12, z23, z34, z13, z24
      _RL dtdz_1, dtdz_2, dtdz_3
      _RL d2tdz2_0, d2tdz2_1, d2tdz2_2

      z12 = 0.5*(zs(1)+zs(2))
      z23 = 0.5*(zs(2)+zs(3))
      z34 = 0.5*(zs(3)+zs(4))
      z13 = 0.5*(z12+z23)
      z24 = 0.5*(z23+z34)

! DTSLB/Dz, true at the midpoint of the two locations
      dtdz_1 = (tslb(2)-tslb(1))/(zs(2)-zs(1))
      dtdz_2 = (tslb(3)-tslb(2))/(zs(3)-zs(2))
      dtdz_3 = (tslb(4)-tslb(3))/(zs(4)-zs(3))
      d2tdz2_1 = (dtdz_2 - dtdz_1)/(z23-z12) ! true at z13
      d2tdz2_2 = (dtdz_3 - dtdz_2)/(z34-z23) ! true at z24
! We have two 2nd derivatives at two points.
! Use linear interpolation to get the value at z=0
! y = y1*(x-x2)/(x1-x2) + y2*(x-x1)/(x2-x1)
      d2tdz2_0 = -d2tdz2_1*z24/(z13-z24) - d2tdz2_2*z13/(z24-z13)
      grdflx = gk*d2tdz2_0

      RETURN
      END 
!================================================================
      SUBROUTINE tggcminit(TSK,TMN,                       
     &                  TSLB,ZS,DZS,num_soil_layers,           
     &                  restart, allowed_to_read,             
     &                  ids,ide, jds,jde, kds,kde,          
     &                  ims,ime, jms,jme, kms,kme,           
     &                  its,ite, jts,jte, kts,kte           )
!----------------------------------------------------------------
      IMPLICIT NONE
!----------------------------------------------------------------
      LOGICAL restart, allowed_to_read
      INTEGER ids,ide, jds,jde, kds,kde, 
     &        ims,ime, jms,jme, kms,kme, 
     &        its,ite, jts,jte, kts,kte

      INTEGER num_soil_layers
!   
      _RL TSLB ( ims:ime, jms:jme, num_soil_layers )

!c-mm   REAL,     DIMENSION(1:num_soil_layers), INTENT(IN)  ::  ZS,DZS
      _RL ZS  (num_soil_layers)
      _RL DZS (num_soil_layers)

      _RL TSK ( ims:ime, jms:jme )
      _RL TMN ( ims:ime, jms:jme )
!  LOCAR VAR

      INTEGER L,J,I,itf,jtf
!----------------------------------------------------------------
 
      itf=min0(ite,ide-1)
      jtf=min0(jte,jde-1)

      RETURN
      END

!-------------------------------------------------------------------          

CEND MODULE module_sf_tggcm
