C     define WRF configurations

      COMMON /WRF_CONFIG/
     &       cAdjFreqWRF, radFreq, 
     &       prejulian,
     &       du_physics,
     &       ISFTCFLX,
     &       ISFFLX, IFSNOW,
     &       nlte_physics,
     &       radiation, 
     &       remove_extra_vels, 
     &       diurnavg, 
     &       isothermal_top, 
     &       tau_damp,
     &       l_double, uv_physics,
     &       sw_correct,
     &       ice_ti_feedback,
     &       perm_co2_cap,
     &       useShap_damping,
     &       mars_co2_cycle,
     &       wrf_rad_scheme,
     &       wrf_surface_physics,
     &       wrf_sf_scheme,
     &       wrf_pbl_physics,
     &       disable_water_regolith,
     &       wrf_tracer_cycle

C === Mars physics configuration ===
      INTEGER du_physics
      INTEGER ISFTCFLX
      INTEGER ISFFLX
      INTEGER IFSNOW

      LOGICAL radiation   
      LOGICAL remove_extra_vels
C mars KDM stuff, including single or double gaussian quadrature
      LOGICAL l_double
      LOGICAL sw_correct
C enable non-LTE physics for Mars
      INTEGER nlte_physics
C enabel UV phsyics for Mars
      INTEGER uv_physics

C enable or disable Mars CO2 cycle
      LOGICAL mars_co2_cycle

C enable or disable permanent CO2 cap
      LOGICAL perm_co2_cap

C enable strong shapiro damping for to three layers
      LOGICAL useShap_damping

C disable regolith if TRUE
      LOGICAL disable_water_regolith

C enable ice-thermal inertia feedback
      LOGICAL ice_ti_feedback

C === End of Mars configuration


C === Universal configuration ===
      REAL*8 cAdjFreqWRF
      REAL*8 radFreq
      REAL*8 tau_damp(kte)
      INTEGER wrf_rad_scheme
      LOGICAL wrf_tracer_cycle
      LOGICAL wrf_surface_physics
      INTEGER wrf_sf_scheme 
      LOGICAL wrf_pbl_physics
c     wrf_rad_scheme = 0 : no radiative transfer scheme
c                    = 1 : mars WBM scheme
c                    = 2 : mars KDM or Titan KDM scheme
c     wrf_sf_scheme = 1 :  TGGCM
c                   = 2 :  sf_simple (depreciated)
c prejulian is the Julian data before restart
      REAL*8 prejulian

      LOGICAL diurnavg
      LOGICAL isothermal_top

      COMMON /WRF_RESTART/
     &       wrf_mdsio_read_pickup 

      LOGICAL wrf_mdsio_read_pickup

      COMMON /WRF_SURFDATA/
     &       albFile, inertiaFile, 
     &       roughnessFile, sstFile

      CHARACTER*(MAX_LEN_FNAM) albFile
      CHARACTER*(MAX_LEN_FNAM) inertiaFile
      CHARACTER*(MAX_LEN_FNAM) roughnessFile
      CHARACTER*(MAX_LEN_FNAM) sstFile
C === 
