C  read in the configuration files (moved from do_planetWRF.F)
#include "PACKAGES_CONFIG.h"
#include "CPP_OPTIONS.h"
#include "planetWRF_OPTIONS.h"

C---+----1----+----2----+----3----+----4----+----5----+----6----+----7-|--+----|
CBOP 0
C     !ROUTINE: WRF_MNC_INIT

C     !INTERFACE:
      SUBROUTINE WRF_READPARMS( myThid )

C     !DESCRIPTION:
C     Read WRF Namelist and Get the Model Date and Time from File

C     !USES:
      implicit none

#include "SIZE.h"
#include "EEPARAMS.h"
#include "PARAMS.h"
#include "WRF_SIZE.h"
#include "WRF_CONFIG.h"

C     !INPUT PARAMETERS:
      integer myThid
CEOP

C     !LOCAL VARIABLES:
      character*(MAX_LEN_MBUF) msgBuf
      integer ku, ku2

      namelist / wrf_list / 
     .     Path2Data,
     .     radiation,
     .     remove_extra_vels, 
     .     ISFTCFLX, iz0tlnd,
     .     ISFFLX, du_physics,
     .     mp_physics, no_min_ust,
     .     grid_factor,
     .     diurnavg, 
     .     nlte_physics, uv_physics,
     .     optical_depth_multiplier,
     .     ra_mars_kdm_double_gauss, 
     .     ra_mars_kdm_simpledust,
     .     simpledust_omega,
     .     simpledust_asymmetry,
     .     number_of_aerosols,
     .     limb_data_rec,
     .     sw_correct, 
     .     perm_co2_cap,
     .     tau_damp,
     .     bldt,
     .     useShap_damping, 
     .     correct_pbl_tracer_tend,
     .     co2_convection,
     .     cAdjFreqWRF, radFreq,
     .     prejulian,
     .     isothermal_top, IFSNOW, 
     .     mars_co2_cycle,
     .     wrf_rad_scheme,
     .     wrf_surface_physics,
     .     wrf_pbl_physics,
     .     mars_water_cycle,
     .     mars_dust_cycle,
     .     titan_methane_cycle,
     .     disable_water_regolith,
     .     prescribed_ice, do_porosity,
     .     ice_ti_feedback,
     .     ddpp_co2_eff, ccn,
     .     rho_dust, ipcp, sspp,
     .     mumars, ddpp, ddpp2,
     .     nswalpha1, nswalpha2,
     .     dalpha1, dalpha2,
     .     nsws, ddev,
     .     reset_surfdust1, reset_surfdust2,
     .     initsurfdust1, initsurfdust2,
     .     albFile, inertiaFile, 
     .     roughnessFile, sstFile,
     .     k_coeff_file, dust_params_file,
     .     ice_params_file,
     .     do_deep_dust_mixing,         
     .     k_overshoot, k_pbl_offset,      
     .     detrain_dust,             
     .     dust_detrain_frac, inject_layer,
     .     nh_h2oice_albedo,
     .     nh_h2oice_emiss ,
     .     sh_h2oice_albedo,
     .     sh_h2oice_emiss ,
     .     nh_co2ice_albedo,
     .     nh_co2ice_emiss ,
     .     sh_co2ice_albedo,
     .     sh_co2ice_emiss ,
     .     co2ice_threshold,
     .     h2oice_threshold,
     .     ground_ice_cond_north,
     .     ground_ice_cond_south,
     .     ground_ice_den_constant,
     .     ground_ice_cap_constant

C     Set defaults
      IF (startTime.EQ.baseTime .AND. nIter0.EQ.0 ) THEN
       wrf_mdsio_read_pickup = .FALSE.
      ELSE
       wrf_mdsio_read_pickup = .TRUE.
      ENDIF
      print *, 'wrf_mdsio_read_pickup',wrf_mdsio_read_pickup
C     Read WRF Namelist
      WRITE(msgBuf,'(A)') ' WRF_READPARMS: opening data.'
      CALL PRINT_MESSAGE( msgBuf, standardMessageUnit,SQUEEZE_RIGHT,1)

      CALL OPEN_COPY_DATA_FILE('data.wrf', 'WRF_READPARMS',
     &     ku,myThid )
      read  (ku,NML=wrf_list)
      close (ku)
 
C     Make sure there isn't overlaped definition between 
C     mars and titan tracer cycles
#ifdef WRF_MARS
      IF (titan_methane_cycle) titan_methane_cycle = .FALSE.
#elif defined WRF_TITAN
      IF (mars_water_cycle) mars_water_cycle = .FALSE.
      IF (mars_dust_cycle)  mars_dust_cycle  = .FALSE.
#endif /* WRF_MARS */

      return
      end

C---+----1----+----2----+----3----+----4----+----5----+----6----+----7-|--+----|
