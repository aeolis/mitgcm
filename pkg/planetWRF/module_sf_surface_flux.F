C!WRF:MEDIATION_LAYER:PHYSICS

CMODULE module_sf_surface_flux
C
CUSE module_model_constants
CUSE module_planet_utilities

CCONTAINS
#include "CPP_OPTIONS.h"
#include "planetWRF_OPTIONS.h"

!---------------------------------------------------------------------
      SUBROUTINE surface_flux(                                        
     &                      tslb,ss_ice,porosity,dzs,dt,p8w,t,qflux,  
     &                      qst_v,RQVMPTEN,tsk,h2oice,
     &                      num_soil_layers,ss_vapor,
     &                      g,r_d,h2oice_threshold,
     &                      disable_water_regolith,
     &                      ids,ide,jds,jde,kds,kde,                  
     &                      ims,ime,jms,jme,kms,kme,                  
     &                      its,ite,jts,jte,kts,kte                   )
!---------------------------------------------------------------------
      IMPLICIT NONE
!---------------------------------------------------------------------
!
!  Subroutine surface_flux balances the flux of vapor across the 
!     surface and calculates the amount of surface ice present
!
!---------------------------------------------------------------------
!-- tslb             Soil temperature (K)
!-- ss_ice           Ice density in pore space (kg/m3)
!-- porosity         Dry porosity of the soil layers
!-- dzs              Thicknesses of soil layers (m)
!-- dt               Model timestep (s)
!-- p8w              Pressure at full levels, p8w(kts)=psfc (Pa)
!-- qflux            Vapor flux through surface (kg/m2/s)
!-- t                Atmospheric temperature at half levels (K)
!-- qst_v            Atmospheric vapor mixing ratio
!-- ss_vapor         Vapor density (kg/m3 gas) in subsurface
!-- tsk              Surface temperature (K)
!-- h2oice           Surface ice abundance (kg/m2)
!-- dzs              Subsurface layer thickness (m)
!---------------------------------------------------------------------

C====INPUT/OUTPUT====

      INTEGER ids,ide, jds,jde, kds,kde,          
     &        ims,ime, jms,jme, kms,kme,          
     &        its,ite, jts,jte, kts,kte

      INTEGER num_soil_layers

      _RL p8w(ims:ime, kms:kme, jms:jme)
      _RL t  (ims:ime, kms:kme, jms:jme) 

      _RL tsk(ims:ime, jms:jme)

      _RL dzs(1:num_soil_layers)

      _RL ss_ice  (ims:ime, 1:num_soil_layers, jms:jme)
      _RL tslb    (ims:ime, 1:num_soil_layers, jms:jme)
      _RL porosity(ims:ime, 1:num_soil_layers, jms:jme)

      _RL qst_v   (ims:ime, kms:kme, jms:jme) 
      _RL RQVMPTEN(ims:ime, kms:kme, jms:jme)

      _RL ss_vapor(ims:ime, 1:num_soil_layers, jms:jme)

      _RL h2oice  (ims:ime, jms:jme) 

      _RL qflux   (ims:ime, jms:jme) 

      _RL dt
   
      _RL g, r_d, h2oice_threshold

      LOGICAL disable_water_regolith

C====Local Variables====

      INTEGER j

      _RL tslb1d        (ims:ime)
      _RL ss_ice1d      (ims:ime)
      _RL ss_vapor1d    (ims:ime)
      _RL psfc          (ims:ime)
      _RL delta_p       (ims:ime)
      _RL qflux1d       (ims:ime)
      _RL qst_v1d       (ims:ime)
      _RL tsk1d         (ims:ime)
      _RL t1d           (ims:ime)
      _RL h2oice1d      (ims:ime)
      _RL porosity1d_mod(ims:ime)
      _RL z_atm1d       (ims:ime)

!c-mm------------------------------------------------------------------

      DO j=jts,jte
         !c-mm Prep all the variables by  making 1-D versions of them to pass
         tslb1d=tslb(:,1,j)
         ss_ice1d=ss_ice(:,1,j)
         porosity1d_mod=MAX(0.0,porosity(:,1,j)-ss_ice1d/917.)
         ss_vapor1d=ss_vapor(:,1,j)
         psfc=p8w(:,kds,j)
         delta_p=p8w(:,kds,j)-p8w(:,kds+1,j)      !c-mm Pressure change (Pa) across lowest atm. layer
         qflux1d=qflux(:,j)
         qst_v1d=qst_v(:,kds,j)
         tsk1d=tsk(:,j)
         t1d=t(:,kds,j)
         h2oice1d=h2oice(:,j)
         z_atm1d=r_d*t1d/g*LOG(p8w(:,kds,j)/p8w(:,kds+1,j))  !c-mm Thickness (m) of lowest atmospheric layer
         CALL surface_flux_1d(                                     
     &                      tslb1d,porosity1d_mod,dzs(1),dt,         
     &                      psfc,t1d,delta_p,
     &                      qflux1d,qst_v1d,tsk1d,  
     &                      h2oice1d,z_atm1d,ss_vapor1d,g, 
     &                      h2oice_threshold,
     &                      disable_water_regolith,
     &                      ids,ide,jds,jde,kds,kde,              
     &                      ims,ime,jms,jme,kms,kme,             
     &                      its,ite,jts,jte,kts,kte,j             )
c-yl     update tracer tendencies due to surface flux (kg/kg/s) 
         RQVMPTEN(:,kds,j)=RQVMPTEN(:,kds,j)+(qst_v1d-qst_v(:,kds,j))/dt
         qst_v(:,kds,j) = qst_v1d
         qflux(:,j) = qflux1d
         ss_vapor(:,1,j) = ss_vapor1d
         h2oice(:,j) = h2oice1d

      ENDDO

      RETURN
      END 

!-----------------------------------------------------------------------------------------

      SUBROUTINE surface_flux_1d(                                   
     &                      tslb1d,porosity1d_mod,dzs,dt,psfc,t1d,  
     &                      delta_p,qflux1d,qst_v1d,tsk1d,h2oice1d, 
     &                      z_atm,ss_vapor1d,g, 
     &                      h2oice_threshold,
     &                      disable_water_regolith,
     &                      ids,ide,jds,jde,kds,kde,               
     &                      ims,ime,jms,jme,kms,kme,              
     &                      its,ite,jts,jte,kts,kte,j             )

      IMPLICIT NONE

#include "WRF_PARAMS_WATER.h"

      INTEGER ids,ide, jds,jde, kds,kde,         
     &        ims,ime, jms,jme, kms,kme,        
     &        its,ite, jts,jte, kts,kte,j

      _RL dzs, dt

      _RL tslb1d        (ims:ime)
      _RL psfc          (ims:ime)
      _RL delta_p       (ims:ime)
      _RL tsk1d         (ims:ime)
      _RL t1d           (ims:ime)
      _RL z_atm         (ims:ime)
      _RL porosity1d_mod(ims:ime)

      _RL ss_vapor1d    (ims:ime)
      _RL qflux1d       (ims:ime)
      _RL qst_v1d       (ims:ime)
      _RL h2oice1d      (ims:ime)

      _RL esat

      _RL g, h2oice_threshold

      LOGICAL disable_water_regolith

! Local variables

      INTEGER i

      _RL vap_den_atm   (ims:ime)
      _RL vap_den_sfc   (ims:ime)
      _RL vap_den_reg   (ims:ime)
      _RL old_vap_den_reg(ims:ime)
      _RL total_z       (ims:ime)

      _RL delta_atm_sfc
      _RL delta_sfc_reg
      _RL allowable_amount
      _RL eq_den
      _RL sat_vap_den_atm
      _RL total_for_eq
      _RL total_water

      vap_den_atm=(delta_p*qst_v1d)/g                      !c-mm Calculate vapor density (kg/m2) in lowest atmospheric layer
      vap_den_reg = porosity1d_mod*ss_vapor1d*dzs          !c-mm This converts from kg/m3(gas) to kg/m3(volume) in regolith
      old_vap_den_reg=vap_den_reg                          !c-mm Used to preserve old value to calculate qflux at end
c-yl  the calculation of saturation density in lowest atmosphere layer should be identical to that for surface level.
c-yl  also see comments in esat function
c-yl  vap_den_sfc=6.11e3*EXP(22.5*(1.0-273.16/tsk1d))/g    !c-mm Calculate saturation vapor density (kg/m2) at tsk

      DO i=its,ite

         total_z = z_atm(i) + dzs                          !c-mm Total thickness of lowest atm. layer and topmost subsfc. layer
         sat_vap_den_atm = esat(t1d(i))/g                  !c-mm Saturation vapor pressure (kg/m2) in lowest atm. layer
         vap_den_sfc(i)  = esat(tsk1d(i))/g                !c-yl use esat to calculate saturation vapor density (kg/m^2) at tsk 

         delta_atm_sfc=vap_den_atm(i)-vap_den_sfc(i)             
         delta_sfc_reg=vap_den_sfc(i)-vap_den_reg(i)

!c-mm This IF-THEN discriminates between locations with surface ice and without.  Different transport physics apply in each case
C#ifndef DISABLE_WATER_REG
C=========//  USE WATER REGOLITH OR NOT //==============
C         //      ENABLE REGOLITH       //
         IF (.not. disable_water_regolith) THEN 
c-yl  we run all conditions for condensation, sublimation and regolith exchange if DISABLE_WATER_REG is not set
         IF (h2oice1d(i) <= h2oice_threshold) THEN
!c-mm Case #1:  Vapor pressure in atm. and reg. both greater than esat at tsk.
            IF ((vap_den_atm(i) >= vap_den_sfc(i)).AND.  
     &                 (vap_den_reg(i) >= vap_den_sfc(i))) THEN
               vap_den_atm(i) = vap_den_sfc(i)             !c-mm  Bring atm. level down to surface level
               vap_den_reg(i) = vap_den_sfc(i)             !c-mm  Bring reg. level down to surface level
!c-mm  Add these changes to surface ice amount. Porosity accounted for in vap_den_reg
               h2oice1d(i) = h2oice1d(i) + delta_atm_sfc 
     &                                   - delta_sfc_reg  
            ENDIF 

!c-mm Case #2:  Vapor pressure highest in atmosphere, lowest in regolith.
!c-mm           Assumes that over one timestep, we won't vastly oversaturate if we push vapor into a nearly saturated regolith.
            IF ((vap_den_atm(i) >= vap_den_sfc(i)).AND.         
     &                   (vap_den_reg(i) < vap_den_sfc(i))) THEN
               vap_den_atm(i) = vap_den_sfc(i)               !c-mm  Bring atm. level down to surface level
               h2oice1d(i) = h2oice1d(i) + delta_atm_sfc     !c-mm  Add this change to surface 'ice' amount ('ice' means placeholder for water)
               IF (porosity1d_mod(i)==0.0) THEN              !c-mm  If regolith is ice-choked, do nothing...
                  vap_den_reg(i) = vap_den_reg(i)
                  h2oice1d(i) = h2oice1d(i)
               ELSE                                          !c-mm  ...else move this water into regolith...
                  vap_den_reg(i) = vap_den_sfc(i)    
                  h2oice1d(i) = h2oice1d(i) - delta_sfc_reg  !c-mm  ...and remove from sfc. Porosity accounted for in definition of vap_den_reg at top
               ENDIF
            ENDIF
!c-mm Case #3:  Vapor pressure lowest in atmosphere, highest in regolith.
            IF ((vap_den_atm(i) < vap_den_sfc(i)).AND.        
     &             (vap_den_reg(i) >= vap_den_sfc(i))) THEN
               vap_den_reg(i) = vap_den_sfc(i)               !c-mm  Bring reg. level down to surface level
               h2oice1d(i) = h2oice1d(i) - delta_sfc_reg     !c-mm  Add this change to surface 'ice' amount.  Porosity accounted for in vap_den_reg
               allowable_amount = sat_vap_den_atm-vap_den_atm(i)  !c-mm  How much water can we put into atm. until saturated?  
               IF (allowable_amount > 0.0) THEN                   !c-mm  Allowable_amount > 0.0 means we are undersaturated
                  vap_den_atm(i) = vap_den_atm(i) +               
     &                     MIN(allowable_amount,h2oice1d(i))   !c-mm  Put in exactly enough to saturate or exhaust regolith contrib.
                  h2oice1d(i) = h2oice1d(i) -                         
     &                     MIN(allowable_amount,h2oice1d(i))   !c-mm  Remove a like amount from surface reservoir
               ENDIF
            ENDIF

         !c-mm Case #4: Vapor pressure in atm. and reg. both less than esat at tsk.
            IF ((vap_den_atm(i) < vap_den_sfc(i)).AND.        
     &            (vap_den_reg(i) < vap_den_sfc(i))) THEN
               eq_den = (vap_den_atm(i)+vap_den_reg(i))/total_z(i) !c-mm  What is the equilibrium density [kg/m3] of vapor across atm and regolith?
               vap_den_atm(i) = eq_den*z_atm(i)                    !c-mm  Set atm. density to this value
               IF (vap_den_atm(i) > sat_vap_den_atm) THEN          !c-mm  However, if we're saturated...
                  h2oice1d(i) = h2oice1d(i) + vap_den_atm(i) -    
     &                                          sat_vap_den_atm !c-mm  ...return the excess back to the surface...
                  vap_den_atm(i) = sat_vap_den_atm                 !c-mm  ...and peg the atm. level at saturation
               ENDIF
               IF (porosity1d_mod(i)==0.0) THEN                    !c-mm  Separately, if the regolith IS ice-choked...
                  h2oice1d(i) = h2oice1d(i) + eq_den*dzs 
     &                                      - vap_den_reg(i) !c-mm  ...no more vapor can go into the regolith, so put on surface.
               ELSE
                  vap_den_reg(i) = eq_den*dzs                      !c-mm  IF regolith is NOT ice-choked, set reg. density to eqilibrium value.
               ENDIF
            ENDIF
         ELSE !c-mm In this situation, there is ice already on the surface.
!c-mm Case #1:  Vapor pressure in atm. and reg. both greater than esat at tsk.
            IF ((vap_den_atm(i) >= vap_den_sfc(i)).AND.      
     &           (vap_den_reg(i) >= vap_den_sfc(i))) THEN
               vap_den_atm(i) = vap_den_sfc(i)
               vap_den_reg(i) = vap_den_sfc(i)
               h2oice1d(i) = h2oice1d(i) + delta_atm_sfc 
     &                                   - delta_sfc_reg  !c-mm Porosity accounted for in def. of vap_den_reg at top
            ENDIF 

!c-mm Case #2:  Vapor pressure highest in atmosphere, lowest in regolith.
            IF ((vap_den_atm(i) >= vap_den_sfc(i)).AND.      
     &           (vap_den_reg(i) < vap_den_sfc(i))) THEN
               vap_den_atm(i) = vap_den_sfc(i)
               h2oice1d(i) = h2oice1d(i) + delta_atm_sfc
               IF (porosity1d_mod(i)==0.0) THEN
                  vap_den_reg(i) = vap_den_reg(i)
                  h2oice1d(i) = h2oice1d(i)
               ELSE
                  vap_den_reg(i) = vap_den_sfc(i)
                  h2oice1d(i) = h2oice1d(i) - delta_sfc_reg   !c-mm  Porosity accounted for in def. of vap_den_reg at top
               ENDIF
            ENDIF

!c-mm Case #3:  Vapor pressure lowest in atmosphere, highest in regolith.
            IF ((vap_den_atm(i) < vap_den_sfc(i)).AND.    
     &           (vap_den_reg(i) >= vap_den_sfc(i))) THEN
               allowable_amount = sat_vap_den_atm - vap_den_atm(i) !c-mm  How much more water can fit in atm.?
               IF (allowable_amount > 0.0) THEN                    !c-mm  If undersaturated, then...
                   vap_den_atm(i) = vap_den_atm(i) +              
     &                        MIN(allowable_amount,h2oice1d(i)) !c-mm  ...fill to saturation or sfc. exhaustion.
                   h2oice1d(i) = h2oice1d(i) -                      
     &                        MIN(allowable_amount,h2oice1d(i)) !c-mm  Remove corresponding amount of sfc. water
                ENDIF
                IF (h2oice1d(i) == 0.0) THEN                        !c-mm  If ice still on sfc. then do nothing, since atm. is saturated, else...
                  h2oice1d(i) = h2oice1d(i) - delta_sfc_reg        !c-mm  Add excess reg. water to sfc.  Porosity accounted for at top
                  vap_den_reg(i) = vap_den_sfc(i)                  !c-mm  Set reg. density to that at sfc.
                  allowable_amount = sat_vap_den_atm - vap_den_atm(i) !c-mm  Calculate remaining amount that can fit in atm.
                  IF (allowable_amount > 0.0) THEN                    !c-mm  If still undersaturated...(should always be, since we were sfc. limited)
                     vap_den_atm(i) = vap_den_atm(i) +    
     &                        MIN(allowable_amount,h2oice1d(i)) !c-mm  ...fill to saturation or sfc. exhaustion...
                     h2oice1d(i) = h2oice1d(i) -                  
     &                        MIN(allowable_amount,h2oice1d(i)) !c-mm  ...and remove corresponding amount of sfc. water
                  ENDIF
               ENDIF
            ENDIF

         !c-mm Case #4: Vapor pressure in atm. and reg. both less than esat at tsk.
            IF ((vap_den_atm(i) < vap_den_sfc(i)).AND.     
     &           (vap_den_reg(i) < vap_den_sfc(i))) THEN
               total_for_eq = delta_sfc_reg - delta_atm_sfc      !c-mm  What is the total amount we are less than eq. at sfc. temp?
               IF (h2oice1d(i) >= total_for_eq) THEN             !c-mm  If we have enough sfc. water to get there, set everything to sfc. value...
                  h2oice1d(i) = h2oice1d(i) + delta_atm_sfc      !c-mm  First equilibrate atmosphere and surface...
                  vap_den_atm(i) = vap_den_sfc(i)                !c-mm  
                  IF (vap_den_atm(i) > sat_vap_den_atm) THEN     !c-mm  ...but if atm. is now supersaturated...
                     h2oice1d(i) = h2oice1d(i) + vap_den_atm(i) -   
     &                                        sat_vap_den_atm !c-mm  Put excess back on sfc...
                     vap_den_atm(i) = sat_vap_den_atm            !c-mm  ...and peg atm. at saturation value
                  ENDIF
                  IF (porosity1d_mod(i) == 0.0) THEN             !c-mm  If regolith is ice-choked, do nothing, else...
                     vap_den_reg(i) = vap_den_reg(i)
                     h2oice1d(i) = h2oice1d(i)
                  ELSE
                     vap_den_reg(i) = vap_den_sfc(i)             !c-mm  ...put water into regolith from surface
                     h2oice1d(i) = h2oice1d(i) - delta_sfc_reg
                  ENDIF
               ELSE                                              !c-mm  If insufficient water to equilibrate at sfc. temp...
                  total_water = vap_den_atm(i) + vap_den_reg(i) + 
     &                                            h2oice1d(i) !c-mm  ...calculate how much water we do have available
                  h2oice1d(i) = 0.0                              !c-mm  We are going to recalculate sfc. water depending on saturation of atm and reg.
                  eq_den = total_water/total_z(i)                !c-mm  Caluclate the equilibrium density possible with water we have
                  vap_den_atm(i) = eq_den*z_atm(i)               !c-mm  Set atm. equal to this value
                  IF (vap_den_atm(i) > sat_vap_den_atm) THEN     !c-mm  But if atm. is now supersaturated...
                     h2oice1d(i) = h2oice1d(i) + vap_den_atm(i) -  
     &                                       sat_vap_den_atm  !c-mm  Put excess back on sfc...
                     vap_den_atm(i) = sat_vap_den_atm            !c-mm ...and peg atm. at saturation value
                  ENDIF
                  IF (porosity1d_mod(i) == 0.0) THEN             !c-mm Separately, see if regolith is ice-choked.  If it is...
                     h2oice1d(i) = h2oice1d(i) + eq_den*dzs      !c-mm  ...nothing goes into reg. and add this to sfc.
                  ELSE                                           !c-mm If not ice-choked...
                     vap_den_reg(i) = eq_den*dzs                 !c-mm ...set reg. density equal to the equilibrium value.
                  ENDIF
               ENDIF
            ENDIF
         ENDIF
         qst_v1d(i) = vap_den_atm(i)*g/delta_p(i)                !c-mm Extract qst_v1d from the new value of vap_den_atm
         IF (porosity1d_mod(i) == 0.0) THEN
            qflux1d(i) = 0.0                                     !c-mm Forces qflux to be zero if regolith is ice-choked
         ELSE
            qflux1d(i) = (vap_den_reg(i)-old_vap_den_reg(i))/dt  !c-mm Flux based on old and new vapor density in regolith  Porosity accounted for at top
         ENDIF

C#else /* DISABLE_WATER_REG */
C         //      DISABLE REGOLITH       //
         ELSE

c-yl     When we disable the regolith for water, then we don't care the surface ice limit (h2oice_threshold)

c-yl     OK, we are dealing a two layer model, one soil layer (one surface level) and one atmosphere layer.
c        Condensation to the surface:   layer deeper than the surface level, colder surface than atm
c        Sublimation to the atmosphere: layer higher than the surface level, warmer surface than atm
c        So during condensation, even if vap_den_sfc > sat_vap_den_atm, the atmospheric vapor 
c        only condenses to the point where vap_den_atm = vap_den_sfc, not sat_vap_den_atm.
c        During sublimation, the surface ice sublimate to the point where vap_den_atm = sat_vap_den_atm

c-yl     If the atmospheric vapor pressure in lowest layer is greater than the surface 
c        vapor pressure, then we have H2O condensation on to the surface.
         IF ((vap_den_atm(i) >= vap_den_sfc(i))) THEN
            vap_den_atm(i) = vap_den_sfc(i)
            h2oice1d(i) = h2oice1d(i) + delta_atm_sfc
         ENDIF
c-yl     If the atmospheric vapor pressure in lowest layer is smaller than the surface 
c        vapor pressure, we COULD have H2O sublimation from the surface
         IF ((vap_den_atm(i) < vap_den_sfc(i))) THEN
c-yl        If the atmospheric vapor pressure is larger than saturated, we still have H2O condensation to the surface
            IF (vap_den_atm(i) > sat_vap_den_atm) THEN
               h2oice1d(i) = h2oice1d(i) + vap_den_atm(i) -
     &                                       sat_vap_den_atm
               vap_den_atm(i) = sat_vap_den_atm
c-yl        If the atmospheric vapor pressure is smaller than saturated, we have H2O sublimation from the surface
            ELSE
               allowable_amount = sat_vap_den_atm-vap_den_atm(i)
               vap_den_atm(i) = vap_den_atm(i) +
     &                  MIN(allowable_amount,h2oice1d(i))
               h2oice1d(i) = h2oice1d(i) -
     &                  MIN(allowable_amount,h2oice1d(i))
            ENDIF
         ENDIF
         qst_v1d(i) = vap_den_atm(i)*g/delta_p(i)
         qflux1d(i) = 0.0
C#endif /* DISABLE_WATER_REG */
         ENDIF

      ENDDO

      RETURN
      END 

C!----------------------------------------------------------------
      REAL*8 FUNCTION esat(temperature)
C!----------------------------------------------------------------
C!    straight copy from function rsat, use esat instead of rsat
C!
C!----------------------------------------------------------------
      IMPLICIT NONE
C!----------------------------------------------------------------

C!  Input/Ouptut variables
      REAL*8 temperature

!  Parameter variables

      REAL*8 MW_AIR
      REAL*8 MW_WAT
      REAL*8 AA0
      REAL*8 AA1
      REAL*8 AA2
      REAL*8 AA3
      REAL*8 AA4
      REAL*8 AA5
      REAL*8 AA6
      REAL*8 C1
      REAL*8 C2
      REAL*8 C3
      REAL*8 EIS

      PARAMETER (MW_AIR = 43.49  )
      PARAMETER (MW_WAT = 18.0152)
      PARAMETER (AA0    = 6.107799961    )
      PARAMETER (AA1    = 4.436518521e-01)
      PARAMETER (AA2    = 1.428945805e-02)
      PARAMETER (AA3    = 2.650648471e-04)
      PARAMETER (AA4    = 3.031240396e-06)
      PARAMETER (AA5    = 2.034080948e-08)
      PARAMETER (AA6    = 6.136820929e-11)
      PARAMETER (C1     = 9.09718)
      PARAMETER (C2     = 3.56654)
      PARAMETER (C3     = 0.876793)
      PARAMETER (EIS    = 6.1071)

!  Local variables

      REAL*8 t1, rhs

      IF ( temperature > 273.16 ) THEN
         t1 = temperature - 273.16
         esat =  AA0 + T1 * (AA1 + T1 * (AA2 + T1 * (AA3 + T1 *
     &       (AA4 + T1 * (AA5 + T1 *  AA6)))))
      ELSE
         rhs =  -C1 * (273.16 / temperature - 1.)
     &        - C2 * LOG10(273.16 / temperature)
     &        + C3 * (1. - temperature / 273.16) + LOG10(EIS)
         esat = 10.**rhs
      END IF
C    !  esat is in mbar -- need to convert to Pa
      esat = 100.*MAX(esat,0.)

      RETURN
      END

CEND MODULE module_sf_surface_flux

