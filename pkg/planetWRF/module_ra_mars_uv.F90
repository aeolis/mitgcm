!WRF:MEDIATION_LAYER:PHYSICS

#define _RL REAL*8

!-----------------------------------------------------------------------
! This module contains parameters needed to deal with the UV section   !
! of the Hadley radiation scheme.  The method is derived from a paper  !
! by Gonzalez-Galindo et al. (2005)                                    !
! Species number for purposes of this code:                            !
!            1:   CO2                                                  !
!            2:   O2                                                   !
!            3:   O(3P)                                                !
!            4:   H2O                                                  !
!            5:   H2                                                   !
!            6:   H2O2                                                 !
!            7:   O3                                                   !
!-----------------------------------------------------------------------
   MODULE uv_parameters

      USE module_model_constants

      IMPLICIT NONE

      REAL(KIND(0.d0)), PARAMETER :: hc           = dble(h_planck)*dble(light_speed) ! 1.986D-25 ! Planck constant * speed of light
      REAL(KIND(0.d0)), PARAMETER :: mw_co2       = dble(N_avogadro)*dble(mass_co2)  ! 0.044     ! [kg/mol]
      _RL,             PARAMETER :: solar_date   = 1993.4     ! Year of solar medium conditions.  Solar min=1996.4, Solar max=1990.6
      REAL(KIND(0.d0)), PARAMETER :: pi           = dble(pi2)/2.d0      ! Pi
      REAL(KIND(0.d0)), PARAMETER :: scale_height = dble(r_d)*dble(t0)/dble(g) ! this gives about 15 km, which may be too big
      REAL(KIND(0.d0)), PARAMETER :: radius       = 1.d0/dble(reradius) ! radius

         INTEGER, PARAMETER ::                                         &
                               n_colden_levs =                    253, &
                                n_uv_species =                      7, &
                              n_uv_intervals =                     36

         REAL(KIND(0.d0)), PARAMETER ::                                &
                                 uv_heat_eff =                   0.22, & ! UV+EUV heating efficiency (from Fox, 1988)
                               mol_constants =   dble(N_avogadro)*     &
                                                 co2_mixing_ratio/     & 
                                                 mw_co2/g           

         REAL(KIND(0.d0)), DIMENSION(n_uv_intervals) ::                &
                                                        uv_bandwidths= &
                           (/ 4.9, 5.3, 6.1, 12.8, 2.0, 6.9, 11.8,     & ! Width of each of the 36 intervals [nm]
                              0.4, 1.9, 4.9, 0.9, 4.0, 6.9, 4.9, 4.9,  &
                              10.2, 5.6, 1.9, 0.9, 0.9, 0.9, 0.9, 3.7, &
                              13.1, 3.9, 6.0, 4.8, 23.9, 8.4, 8.5,     &
                              26.8, 7.5, 20.8, 9.0, 97.6, 462.2 /)

         REAL(KIND(0.d0)), DIMENSION(n_uv_intervals+1) ::              &
                                                          lambda_edge= &
                        (/ 0.1, 5.0, 10.4, 16.5, 29.4, 31.5, 38.5,     &
                            50.4, 51.4, 53.4, 58.4, 59.4, 63.5, 70.5,  &
                            75.5, 80.5, 90.8, 96.5, 98.5, 99.5, 100.5, &
                            101.5, 102.5, 106.3, 119.5, 123.5, 129.6,  &
                            134.5, 158.5, 167.0, 175.6, 202.5, 210.0,  &
                            230.9, 240.0, 337.7, 800.0 /)

         REAL(KIND(0.d0)), DIMENSION(n_uv_intervals) ::                &
                                                           lambda_avg= &
                        (/ 2.55, 7.65, 13.45, 22.9, 30.4, 34.95, 44.4, & ! Median wavelength of each of the 36
                           50.6, 52.35, 55.85, 58.85, 61.4, 66.95,     & !    intervals [nm]
                           72.95, 77.95, 85.6, 93.6, 97.45, 98.95,     &
                           99.95, 100.95, 101.95, 104.35, 112.85,      &
                           121.45, 126.5, 132.0, 146.45, 162.8,        &
                           171.25, 189.0, 206.25, 220.4, 235.4, 288.8, &
                           568.8 /)

         REAL(KIND(0.d0)), DIMENSION(n_uv_intervals) ::                &
                                                         uv_flux_band 

         INTEGER, DIMENSION(n_uv_species) ::                           &
                                                            int_start= &
                       (/ 2, 1, 1, 25, 1, 25, 34 /)                      ! This only works because there are no 'gaps' in the
                                                                         !    absorption bands of these 7 gases in the UV
         INTEGER, DIMENSION(n_uv_species) ::                           &
                                                              int_end= &
                       (/ 32, 34, 16, 31, 15, 35, 36 /)

   END MODULE uv_parameters

!-----------------------------------------------------------------------
! This module contains all the UV-specific routines, following the     !
!    work of Gonzalez-Galindo et al. (2005).  One thing to note is     !
!    that the order of the vertical dimension follows WRF (i.e. k=1 is !
!    the surface-most layer, and k=n_layer is at TOA.                  !
!-----------------------------------------------------------------------
   MODULE module_ra_mars_uv

      USE uv_parameters
      USE module_ra_chapman

      IMPLICIT NONE

      REAL(KIND(0.d0)), DIMENSION(n_colden_levs,n_uv_intervals) ::     &
                                                               colden    ! Column density array from Gonzalez-Galindo et al. (2005)

      REAL(KIND(0.d0)), DIMENSION(n_colden_levs,n_uv_intervals,n_uv_species) ::  &
                                                                j_Xn     ! Photoabsorption coefficients for all gases from Gonzalez-Galindo et al. (2005)

      REAL(KIND(0.d0)), DIMENSION(24,4) ::                             &
                                                          flux_coeffs    ! Flux coefficients c1, p1, c2, p2 from Gonzalez-Galindo et al., (2005) Table 1.
   CONTAINS
!-----------------------------------------------------------------------
!+ Subroutine to determine the UV heating rate as per method described !
!  in Gonzalez-Galindo et al. (2005).                                  !
!                                                                      !
! Method:                                                              !
!       Loop over the individual 'intervals', summing up contributions !
!       from each gas in each interval.  Right now it's only for CO2   !
!-----------------------------------------------------------------------
      SUBROUTINE uv_heating(t,p,pf,hr_g_uv2d,mubar,dz,                 &
                            ims,ime,kms,kme,its,ite,kts,kte)

      IMPLICIT NONE

      INTEGER, INTENT(IN   ) ::                                        &
                                                              ims,ime, &
                                                              kms,kme, &
                                                              its,ite, &
                                                              kts,kte

      _RL, INTENT(IN   ) ::                                           &
                                                                mubar

      _RL, DIMENSION(kms:kme), INTENT(IN   ) ::                       &
                                                                    t, & ! Temperature of each layer
                                                                    p, & ! Pressure of each layer
                                                                   pf, & ! Pressure at each level (layer edges)
                                                                   dz

      _RL,                                                            &
          DIMENSION(kms:kme), INTENT(  OUT) ::                         &
                                                            hr_g_uv2d

!c-mm Local variables

      INTEGER                                                 i,j,k,l

      INTEGER                                               n_profile, &
                                                              n_layer

      REAL(KIND(0.d0)) ::                                     airmass

      REAL(KIND(0.d0)), DIMENSION(kte-kts+1) ::                        &
                                                           int_d_mass, &
                                                             molperm2, &
                                                             test

      REAL(KIND(0.d0)),                                                &
       DIMENSION(kte-kts+1, n_uv_intervals, n_uv_species) ::           &
                                                              j_array

      REAL(KIND(0.d0)), DIMENSION(24:32) ::                            &
                                                            sigma_195= & ! Average cross-section for intervals 24-32 @ 195 K
                    (/ 2.05864D-17, 5.90557D-20, 3.10270D-19,          &
                       6.70653D-19, 4.55132D-19, 8.87122D-20,          &
                       1.32138D-20, 7.22244D-23, 2.88002D-26 /)

      REAL(KIND(0.d0)), DIMENSION(24:32) ::                            &
                                                            sigma_295= & ! Average cross-section for intervals 24-32 @ 295 K
                    (/ 2.05897D-17, 6.71104D-20, 3.45509D-19,          &
                       7.45711D-19, 4.82752D-19, 1.11594D-19,          &
                       1.98308D-20, 1.38530D-22, 2.14140D-25 /)

      REAL(KIND(0.d0)), DIMENSION(24:32) ::                            &
                                                                alpha

      REAL(KIND(0.d0)), PARAMETER ::                                   &
                        t_min =                                  195., &
                        t_max =                                  295.

      n_profile=ite-its+1
      n_layer=kte-kts+1

      j_array=0.d0                                                        ! Initialize j_array to zero everywhere

      alpha=((sigma_295/sigma_195)-1.)/(295.-195.)

      airmass=SQRT((radius/scale_height*mubar)**2+ 2.*radius/          &  ! Modeling the atmosphere as a simple spherical shell
              scale_height+1)-radius/scale_height*mubar                   !    to improve accuracy near horizon (vs. sec approx.)
                                                                          ! From Schoenberg (1929) (via Wikipedia for 'Air mass')
                                                                          ! Yeah, that's right, we went there.  That's a Wikipedia
                                                                          ! reference. Wanna make something of it? huh?

      DO k=1,n_layer
!c-mm--------------------------
!c-mm Next two statements are to be used if we employ a Chapman function
!c-mm    instead of simple SZA.
!         molperm2(k) = (pf(k)-pf(k+1))*mol_constants*                  &
!                       atm_chapman((1./reradius/1000.+SUM(dz(1:k)))/   &
!                       11000.,SNGL(acos(mubar)*180./pi))
!         int_d_mass(k) = pf(k)*mol_constants*                          &
!                         atm_chapman((1./reradius/1000.+SUM(dz(1:k)))/ &
!                         11000.,SNGL(acos(mubar)*180./pi))
!c-mm--------------------------
!c-mm In conversation with Francisco Gonzalez-Galindo, he indicated that we
!c-mm    needed to divide by cos(SZA), or mubar.  I'm finding that we need
!c-mm    to multiply instead, which makes more intuitive sense.  Results look
!c-mm    better, too, especially around the twilight regions.
!c-mm Here's my argument...incoming flux as W/m2 is multiplied by cos(SZA)
!c-mm    to account for angle, thus it =0 at SZA=90.  When calculating the
!c-mm    heating rate at the end, we are calculating W/m3, or, another way,
!c-mm    W/m2/m.  The W/m2 term portion is obtained from hc/lambda*j*molperm2
!c-mm    Notice that we're just incorporating mubar (=cos(SZA)) into molperm2
!c-mm    but that it's equivalent to multiplying the W/m2 portion by cos(SZA)
!c-mm    at the very end.  In a sense, we're just 'hiding' the angular
!c-mm    dependence inside molperm2.  We could just as easily pull it out
!c-mm    and explicitly multiply the heating rate term by cos(SZA).  The way
!c-mm    it's done is, indeed, correct (mm 10-12-10)
         molperm2(k) = (pf(k)-pf(k+1))*mol_constants*mubar                   ! Convert from pressure to number of molecules per m2. Equivalent to n(z)dz at level z
         int_d_mass(k) = pf(k)*mol_constants*mubar                           ! Number of molecules above and including layer k. Both these are units of m-2
      ENDDO                                                           
!      FORALL (i=1:n_profile, k=1:n_layer, l=1:n_uv_species,            & ! For each of the 7 UV-active gases in each of the 36
!      The following is invalid coding, and will cause some 
!      compilers to fail, i.e., l can't loop from a constant
!      of 1 to a constant of 1.  If either were a variable
!      or either had a different value from the other, then
!      the statement would qualify as valid.
      FORALL (k=1:n_layer,                                             & ! For just CO2 in the appropriate
              j=int_start(1):int_end(1))                               & !    spectral intervals for each point in the GCM grid,
              j_array(k,j,1)=get_j(int_d_mass(k)/1.D4,j,1)               !    j_array holds the partial photoabsorption coeff. @ 195 K. 1.D4 goes from m-2 to cm-2

      FORALL (k=1:n_layer, j=1:24)                                     & ! This does the scaling for solar flux.  Currently, solar date is hardwired
              j_array(k,j,1)=j_array(k,j,1)*scale_flux(j,solar_date)              !    to solar medium conditions.

      DO k=1,n_layer
!c-mm            DO l=1,n_uv_gases
         DO l=1,1                                                     ! Only for CO2 right now
            DO j=24,32                                                ! Only bands 24-32 have CO2 temperature dependence.
               IF (l==1) THEN
                  j_array(k,j,l)=j_array(k,j,l)*                    &
                                   EXP(-sigma_195(j)*alpha(j)*      &
                                   SUM((MAX(MIN(DBLE(t(k:n_layer)), & ! <---This MAX(MIN(...)) block limits the temperature
                                   t_max),t_min)-t_min)*            & !    dependence to only between 195 and 295 K.  Above/
                                   molperm2(k:n_layer)))*        & !    below this range, value is pegged at 295/195 K
                                   (1+alpha(j)*(MAX(MIN(DBLE(t(k)), & !    respectively.
                                   t_max),t_min)-t_min))                   
               ELSE
                  j_array(k,j,l)=j_array(k,j,l)*                    & ! The purpose of these two j_array blocks is to adjust
                                   EXP(-sigma_195(j)*alpha(j)*      & !    the photoabsorption coefficients based on the varying
                                   SUM((MAX(MIN(DBLE(t(k:n_layer)), & !    CO2 cross-section with temperature.  Slightly
                                   t_max),t_min)-t_min)*            & !    different adjustment for CO2 vs. other gases.
                                   molperm2(k:n_layer)))
               ENDIF
            ENDDO
         ENDDO

!c-mm The t(k)/p(k)/4. term comes about because we want to convert the heating rate [W/m3] into [K/s]
!c-mm    this requires dividing the former by density*cp.  Density is p/RT, and R=cp/4. This
!c-mm    cancels out the cp values, leaving us with t/4p
         hr_g_uv2d(k)=1.d9*uv_heat_eff*hc*molperm2(k)/dz(k)*(t(k)/  &
                      p(k)/4.)*sum(j_array(k,1:n_uv_intervals,1)/   &
                      lambda_avg(1:n_uv_intervals))
      ENDDO

      END SUBROUTINE uv_heating

!-----------------------------------------------------------------------
!+ Function to interpolate the photoabsorption coefficient from data   !
!  tables as described in Gonzalez-Galindo et al. (2005).              !
!                                                                      !
! Method:                                                              !
!       Searches down the corresponding 'interval' column in coln.dat  !
!       and interpolates column number density.  Takes the appropriate !
!       interval and interpolates the proper photoabsorption           !
!       coefficient for each gas and each of the 36 spectral intervals !
!       in the UV (only for those in which the gas is active)          !
!-----------------------------------------------------------------------
      PURE FUNCTION get_j(int_d_mass, j, l)                              ! Pure function has no side effects
                                                                         !   (i.e. doesn't open files or change
      IMPLICIT NONE                                                      !   global variable values)

      INTEGER, INTENT(IN   ) ::                                        &
                                                                 j, l

      REAL(KIND(0.d0)), INTENT(IN   ) ::                               &
                                                           int_d_mass

!c-mm Local variables

      INTEGER                                                       i

      REAL(KIND(0.d0))                                                 &
                                                                get_j, &
                                                                 frac, &
                                                       log_int_d_mass


      log_int_d_mass=DLOG10(int_d_mass)

      DO i=1,n_colden_levs,1
         IF (colden(i,j) > int_d_mass) EXIT                              ! Find the index for point greater than int_d_mass
      ENDDO
      ! If int_d_mass > colden(n_colden_levs,j), then the loop
      ! will finish, and i will end up with a value of n_colden_levs+1
      ! and the equations for frac and get_j below will (or should...)
      ! fail with an illegal array address
      IF (i > n_colden_levs) i=n_colden_levs

      frac=(DLOG10(colden(i,j))-log_int_d_mass)/(DLOG10(colden(i,j))-  &
            DLOG10(colden(i-1,j)))                                       ! Fractional difference

      get_j=(1-frac)*j_Xn(i,j,l)+frac*j_Xn(i-1,j,l)                      ! Interpolate within j_Xn for the appropriate

      END FUNCTION get_j

!-----------------------------------------------------------------------
!+ Function to calculate the scale factor for the solar flux depending !
!  on season                                                           !
!                                                                      !
! Method:                                                              !
!       Table in solar_flux.dat contains 4 parameters (taken from      !
!       Gonzalez-Galindo et al., (2005) for each wavelength bin from   !
!       1-24.  A parameterized function is used to calculate a scaling !
!       value (which should be ~between 0.5-1.5) which is multiplied   !
!       to the value of j_array                                        !
!-----------------------------------------------------------------------
      PURE FUNCTION scale_flux(j,solar_date)

      IMPLICIT NONE

      INTEGER, INTENT(IN   ) ::                                        &
                                                                    j

      _RL, INTENT(IN   ) ::                                           &
                                                           solar_date

!c-mm Local variables

      REAL(KIND(0.d0))                                                 &
                                                           scale_flux

      scale_flux=(flux_coeffs(j,1)+flux_coeffs(j,2)*solar_date)*       &
                 (SIN(2*pi/11*(solar_date-1985.-pi)))+(flux_coeffs(j,3)+        &
                 flux_coeffs(j,4)*solar_date)

      END FUNCTION scale_flux

!-----------------------------------------------------------------------
!+ Subroutine calculating the fraction of blackbody radiation          !
!  between two wavelengths, assuming this is for solar wavelengths.    ! 
!                                                                      !
! Method:                                                              !
!        Uses series formulae from Houghton 'Physics of Atmospheres'   !
!-----------------------------------------------------------------------
      SUBROUTINE solar_fraction(wl1, wl2, denom, fst4)

      IMPLICIT NONE

      REAL(KIND(0.d0)), PARAMETER ::    			       &
                                          c  =         1.53989733D-01, &
                                         c2  =            1.43883D+04, &
                                       temp  =             5777.0D+00    ! Equivalent blackbody temperature of the Sun

      REAL(KIND(0.d0)), INTENT(IN   ) ::			       &
                                                                  wl1, &
                                                                  wl2, &
                                                                denom    ! Term to calculate *fraction* of total flux rather
                                                                         !    than flux itself.
      REAL(KIND(0.d0)), INTENT(  OUT) ::                         fst4

!c-mm  Local variables

      REAL(KIND(0.d0)), DIMENSION(2) ::   		               &
                                                       	           wl, &
                                                                    f

      REAL(KIND(0.d0)) ::                                              &
                                                                 facc, &
                                                                   ff, &
                                                                    w, &
                                                                    v, &
                                                                   wv

      INTEGER         					               &
                                                                 i, m


      wl(1)=wl1
      wl(2)=wl2
      facc=0.00001
      DO i=1,2
         f(i)=0.
         IF (wl(i) <= 0.0) CYCLE
         v=c2/(wl(i)*temp)
         DO m=1,1000
            w=FLOAT(m)
            wv=w*v
            ff=(w**(-4))*(EXP(-wv))*(((wv+3.)*wv+6.)*wv+6.)
            f(i)=f(i)+ff
            IF (f(i) == 0.0) CYCLE
            IF (ff/f(i) <= facc) EXIT
         ENDDO
      ENDDO
      fst4=(c*(f(2)-f(1))*5.67D-08*temp*temp*temp*temp)/denom
      END SUBROUTINE

!-----------------------------------------------------------------------
!+ Function generates specific heat values (cp) to be used in flux     !
!    calculations.  The gas mixture assumes a modern-day composition   !
!    of CO2 and N2.  Coefficients are derived from cp/R vs T data in   !
!    Hilsenrath (1955).                                                !
!-----------------------------------------------------------------------
      FUNCTION cp_mars(t)

      USE module_model_constants, ONLY: co2_mixing_ratio

      IMPLICIT NONE

      REAL(KIND(0.d0)), INTENT(IN   ) ::       t
      
      real(kind(0.d0)) :: cp_mars

      cp_mars=(((3.47D-07*t*t*t)-(1.269D-03*t*t)+(1.688*t)+443.1)* &  ! First term is for CO2, second is
              co2_mixing_ratio)+(1059.*(1.-co2_mixing_ratio))         !    for N2.

      END FUNCTION



!-----------------------------------------------------------------------
!+ Subroutine to read in the column densities and UV photoabsorption   !
!  cross-sections.                                                     !
!                                                                      !
! Method:                                                              !
!	Straightforward.                                               !
!       For now, hardwire these data filenames.                        !
!-----------------------------------------------------------------------
      SUBROUTINE uvinit(Path2Data)

      IMPLICIT NONE

!c-mm Local variables

      INTEGER                                                    i, j
  
      CHARACTER*256, INTENT(IN) :: Path2Data

      CHARACTER (LEN=80) ::                                      junk


      OPEN(UNIT=2010,FILE=trim(Path2Data)//'/Data/radiation/coln_full.dat',STATUS='old')
      READ(2010,'(A80)') junk
      READ(2010,'(A80)') junk
      READ(2010,*) ((colden(i,j), j=1,n_uv_intervals), i=1,n_colden_levs)
      CLOSE(2010)

      OPEN(UNIT=2011,FILE=trim(Path2Data)//'/Data/radiation/j1_Xn.dat',STATUS='old')
      READ(2011,'(A80)') junk
      READ(2011,'(A80)') junk
      READ(2011,*) ((j_Xn(i,j,1), j=1,n_uv_intervals), i=1,n_colden_levs)
      CLOSE(2011)

      OPEN(UNIT=2012,FILE=trim(Path2Data)//'/Data/radiation/j2_Xn.dat',STATUS='old')
      READ(2012,'(A80)') junk
      READ(2012,'(A80)') junk
      READ(2012,*) ((j_Xn(i,j,2), j=1,n_uv_intervals), i=1,n_colden_levs)
      CLOSE(2012)

      OPEN(UNIT=2013,FILE=trim(Path2Data)//'/Data/radiation/j3_Xn.dat',STATUS='old')
      READ(2013,'(A80)') junk
      READ(2013,'(A80)') junk
      READ(2013,*) ((j_Xn(i,j,3), j=1,n_uv_intervals), i=1,n_colden_levs)
      CLOSE(2013)

      OPEN(UNIT=2014,FILE=trim(Path2Data)//'/Data/radiation/j4_Xn.dat',STATUS='old')
      READ(2014,'(A80)') junk
      READ(2014,'(A80)') junk
      READ(2014,*) ((j_Xn(i,j,4), j=1,n_uv_intervals), i=1,n_colden_levs)
      CLOSE(2014)

      OPEN(UNIT=2015,FILE=trim(Path2Data)//'/Data/radiation/j5_Xn.dat',STATUS='old')
      READ(2015,'(A80)') junk
      READ(2015,'(A80)') junk
      READ(2015,*) ((j_Xn(i,j,5), j=1,n_uv_intervals), i=1,n_colden_levs)
      CLOSE(2015)

      OPEN(UNIT=2016,FILE=trim(Path2Data)//'/Data/radiation/j6_Xn.dat',STATUS='old')
      READ(2016,'(A80)') junk
      READ(2016,'(A80)') junk
      READ(2016,*) ((j_Xn(i,j,6), j=1,n_uv_intervals), i=1,n_colden_levs)
      CLOSE(2016)

      OPEN(UNIT=2017,FILE=trim(Path2Data)//'/Data/radiation/j7_Xn.dat',STATUS='old')
      READ(2017,'(A80)') junk
      READ(2017,'(A80)') junk
      READ(2017,*) ((j_Xn(i,j,7), j=1,n_uv_intervals), i=1,n_colden_levs)
      CLOSE(2017)

      OPEN(UNIT=2018, FILE=trim(Path2Data)//'/Data/radiation/solar_flux.dat',STATUS='old')
      READ(2018,'(A80)') junk
      READ(2018,'(A80)') junk
      READ(2018,*) ((flux_coeffs(i,j), j=1,4), i=1,24)
      CLOSE(2018)

      END SUBROUTINE 

    END MODULE module_ra_mars_uv

