module module_mars24

implicit none

real(kind(0.d0)) :: j2000_epoch = 2451545.0d0
real(kind(0.d0)) :: dtr = 0.017453292519943295d0

public ::&
     mars24_utc_to_tt_offset, mars24_julian_tt, mars24_j2000_offset_tt, &
     mars24_Mars_Mean_Anomaly, mars24_FMS_Angle, mars24_alpha_perturbs, &
     mars24_Equation_Of_Center, mars24_Mars_Ls, mars24_Equation_Of_Time, &
     mars24_j2000_ott_from_Mars_Solar_Date, mars24_Mars_Solar_Date, &
     mars24_Coordinated_Mars_Time, mars24_Local_Mean_Solar_Time, &
     mars24_Local_True_Solar_Time, mars24_subsolar_longitude, &
     mars24_solar_declination, mars24_heliocentric_distance, &
     mars24_heliocentric_longitude, mars24_heliocentric_latitude, &
     mars24_hourangle, mars24_solar_zenith, mars24_solar_elevation, &
     mars24_solar_azimuth


contains

!====================================================================

  subroutine mars24_utc_to_tt_offset(jday, utc_to_tt_offset)
    !Returns the offset in seconds from a julian date in Terrestrial Time (TT)
    !to a Julian day in Coordinated Universal Time (UTC)
    real(kind(0.d0)) :: jday
    real(kind(0.d0)) :: off, utc_to_tt_offset
    off = 32.184 !!<1972
    
    if (jday .gt. 2441317.5) off=off+10. !1972 1 1
    if (jday .gt. 2441499.5) off=off+1.0 !1972 6 1
    if (jday .gt. 2441683.5) off=off+1.0 !1973 1 1
    if (jday .gt. 2442048.5) off=off+1.0 !1974 1 1
    if (jday .gt. 2442413.5) off=off+1.0 !1975 1 1
    if (jday .gt. 2442778.5) off=off+1.0 !1976 1 1
    if (jday .gt. 2443144.5) off=off+1.0 !1977 1 1
    if (jday .gt. 2443509.5) off=off+1.0 !1978 1 1
    if (jday .gt. 2443874.5) off=off+1.0 !1979 1 1
    if (jday .gt. 2444239.5) off=off+1.0 !1980 1 1
    if (jday .gt. 2444786.5) off=off+1.0 !1981 6 1
    if (jday .gt. 2445151.5) off=off+1.0 !1982 6 1
    if (jday .gt. 2445516.5) off=off+1.0 !1983 6 1
    if (jday .gt. 2446247.5) off=off+1.0 !1985 6 1
    if (jday .gt. 2447161.5) off=off+1.0 !1988 1 1
    if (jday .gt. 2447892.5) off=off+1.0 !1990 1 1
    if (jday .gt. 2448257.5) off=off+1.0 !1991 1 1
    if (jday .gt. 2448804.5) off=off+1.0 !1992 6 1
    if (jday .gt. 2449169.5) off=off+1.0 !1993 6 1
    if (jday .gt. 2449534.5) off=off+1.0 !1994 6 1
    if (jday .gt. 2450083.5) off=off+1.0 !1996 1 1
    if (jday .gt. 2450630.5) off=off+1.0 !1997 6 1
    if (jday .gt. 2451179.5) off=off+1.0 !1999 1 1
    if (jday .gt. 2453736.5) off=off+1.0 !2006 1 1
    if (jday .gt. 2454832.5) off=off+1.0 !2009 1 1
    if (jday .gt. 2456109.5) off=off+1.0 !2009 1 1

    !off= 65.184
    utc_to_tt_offset = off

  end subroutine mars24_utc_to_tt_offset

  subroutine mars24_julian_tt(jday_utc, julian_tt)
    !Returns the TT Julian day given a UTC Julian day
    real(kind(0.d0)) :: jday_utc
    real(kind(0.d0)) :: julian_tt, utc
    call mars24_utc_to_tt_offset(jday_utc, julian_tt)
    julian_tt = jday_utc + julian_tt/86400.
  end subroutine mars24_julian_tt

  subroutine mars24_j2000_offset_tt(jday_tt, j2000_offset_tt)
    !Returns the julian day offset since the J2000 epoch
    real(kind(0.d0)) :: jday_tt
    real(kind(0.d0)) :: j2000_offset_tt
    j2000_offset_tt = jday_tt - j2000_epoch
  end subroutine mars24_j2000_offset_tt

  subroutine mars24_Mars_Mean_Anomaly(j2000_ott, Mars_Mean_Anomaly)
    !Calculates the Mars Mean Anomaly given a j2000 julian day offset
    real(kind(0.d0)) :: j2000_ott
    real(kind(0.d0)) :: Mars_Mean_Anomaly
    !  real(kind(0.d0)) :: Mars_Mean_Anomaly

    Mars_Mean_Anomaly = 19.3870 + 0.52402075 * j2000_ott
    Mars_Mean_Anomaly = modulo(Mars_Mean_Anomaly+360.,360.)
    
  end subroutine mars24_Mars_Mean_Anomaly

  subroutine mars24_FMS_Angle(j2000_ott, FMS_Angle)
    !Returns the Fictional Mean Sun angle
    real(kind(0.d0)) :: j2000_ott
    real(kind(0.d0)) :: FMS_Angle
    !  real(kind(0.d0)) :: FMS_Angle

    FMS_Angle = 270.3863 +  0.52403840 * j2000_ott
    FMS_Angle = modulo(FMS_Angle+360.,360.)

  end subroutine mars24_FMS_Angle

  subroutine mars24_alpha_perturbs(j2000_ott, alpha_perturbs)
    !Returns the perturbations to apply to the FMS Angle from orbital perturbations
    real(kind(0.d0)) :: j2000_ott
    real(kind(0.d0)) :: alpha_perturbs

    real(kind(0.d0)), dimension(7) :: A, tau, phi

    integer :: i

    data A/0.0071, 0.0057, 0.0039, 0.0037, 0.0021, 0.0020, 0.0018/
    data tau/2.2353, 2.7543, 1.1177, 15.7866, 2.1354, 2.4694, 32.8493/
    data phi/49.409, 168.173, 191.837, 21.736, 15.704, 95.528, 49.095/

    alpha_perturbs =0.0

    do i=1,7 
       alpha_perturbs = alpha_perturbs + A(i) * cos(((0.985626*j2000_ott/tau(i)) + phi(i))*dtr)
    end do

  end subroutine mars24_alpha_perturbs

  subroutine mars24_Equation_Of_Center(j2000_ott, Equation_Of_Center)
    !The true anomaly (v) - the Mean anomaly (M)
    real(kind(0.d0)) :: j2000_ott
    real(kind(0.d0)) :: M, pbs
    real(kind(0.d0)) :: Equation_Of_Center
    call mars24_Mars_Mean_Anomaly(j2000_ott, M)
    M=M*dtr
    call mars24_alpha_perturbs(j2000_ott, pbs)

    Equation_Of_Center = (10.691 + 3.0e-7 * j2000_ott)*sin(M) &
         + 0.6230 * sin(2*M) &
         + 0.0500 * sin(3*M) & 
         + 0.0050 * sin(4*M) & 
         + 0.0005 * sin(5*M) &
         + pbs

  end subroutine mars24_Equation_Of_Center

  subroutine mars24_Mars_Ls(j2000_ott, Mars_Ls)
    !Returns the Areocentric solar longitude
    real(kind(0.d0)) :: j2000_ott
    real(kind(0.d0)) :: alpha, v_m
    real(kind(0.d0)) :: Mars_Ls
    
    call mars24_FMS_Angle(j2000_ott, alpha)
    call mars24_Equation_Of_Center(j2000_ott, v_m)

    Mars_Ls = (alpha+v_m)
    Mars_Ls = modulo(Mars_Ls+360., 360.)

  end subroutine mars24_Mars_Ls

  subroutine mars24_Equation_Of_Time(j2000_ott, Equation_Of_Time)
    !Equation of Time
    real(kind(0.d0)) :: j2000_ott
    real(kind(0.d0)) :: Ls
    real(kind(0.d0)) :: Equation_Of_Time, eoc

    call mars24_Mars_ls(j2000_ott, Ls)
    Ls = Ls*dtr
    call mars24_Equation_Of_Center(j2000_ott, eoc)

    Equation_Of_Time = 2.861*sin(2*Ls) &
         - 0.071 * sin(4*ls) &
         + 0.002 * sin(6*ls) - eoc

  end subroutine mars24_Equation_Of_Time

  subroutine mars24_j2000_ott_from_Mars_Solar_Date(msd, j2_ott)
    real(kind(0.d0)) :: msd
    real(kind(0.d0)) :: j2, j2_ott
    j2 = ((msd*1.0d0 - 44796.0d0 + 0.00096d0)*1.027491252d0 + 4.5)
    call mars24_julian_tt(j2+j2000_epoch, j2_ott)
    j2_ott = j2_ott - j2000_epoch

  end subroutine mars24_j2000_ott_from_Mars_Solar_Date

  subroutine mars24_Mars_Solar_Date(j2000_ott, msd)
    real(kind(0.d0)) :: j2000_ott, msd
    msd = (((j2000_ott - 4.5)/1.027491252d0) + 44796.0d0 - 0.00096d0)
  end subroutine mars24_Mars_Solar_Date

  subroutine mars24_Coordinated_Mars_Time(j2000_ott, Coordinated_Mars_Time)
    !The Mean Solar Time at the Prime Meridian
    real(kind(0.d0)) :: j2000_ott, Coordinated_Mars_Time
    
    Coordinated_Mars_Time = 24.0 * (((j2000_ott - 4.5)/1.027491252d0) + 44796.0 - 0.00096d0)
    Coordinated_Mars_Time= modulo(24.+Coordinated_Mars_Time, 24.0d0)

  end subroutine mars24_Coordinated_Mars_Time

  subroutine mars24_Local_Mean_Solar_Time(longitude, j2000_ott, Local_Mean_Solar_Time)
    !The Local Mean Solar Time given a planetographic longitude
    real(kind(0.d0)) :: longitude
    real(kind(0.d0)) :: j2000_ott, west_longitude
    real(kind(0.d0)) :: mtc, Local_Mean_Solar_Time
    call mars24_Coordinated_Mars_Time(j2000_ott, mtc)
    west_longitude = 360.0 - longitude + 0.271
    Local_Mean_Solar_Time = mtc - longitude * (24/360.)
    Local_Mean_Solar_Time = modulo(Local_Mean_Solar_Time + 24.0, 24.d0)

  end subroutine mars24_Local_Mean_Solar_Time


  subroutine mars24_Local_True_Solar_Time(longitude, j2000_ott, Local_True_Solar_Time)
    !Local true solar time is the Mean solar time + equation of time perturbation
    real(kind(0.d0)) :: j2000_ott, longitude
    real(kind(0.d0)) :: eot, lmst, Local_True_Solar_Time

    call mars24_Equation_Of_Time(j2000_ott, eot)
    call mars24_Local_Mean_Solar_Time(longitude, j2000_ott, lmst)

    Local_True_Solar_Time = lmst + eot*(24/360.)
    Local_True_Solar_Time = modulo(Local_True_Solar_Time + 24., 24.0d0)

  end subroutine mars24_Local_True_Solar_Time

  subroutine mars24_subsolar_longitude(j2000_ott, subsolar_longitude)
    !    """returns the longitude of the subsolar point for a given julian day."""
    real(kind(0.d0)) :: j2000_ott, subsolar_longitude
    real(kind(0.d0)) :: MTC, EOT

    call mars24_Coordinated_Mars_Time(j2000_ott, mtc)
    call mars24_equation_of_time(j2000_ott, eot)
    eot = eot*24/360.
    subsolar_longitude = (MTC + EOT)*(360/24.) + 180.
    subsolar_longitude = modulo(subsolar_longitude+360.,360.0)
  end subroutine mars24_subsolar_longitude

  subroutine mars24_solar_declination(ls, solar_declination)
    !    """Returns the solar declination"""
    real(kind(0.d0)) :: ls, solar_declination
    solar_declination = (asin(0.42565d0 * sin(ls*dtr)) + 0.25d0*dtr * sin(ls*dtr)) / dtr
  end subroutine mars24_solar_declination

  subroutine mars24_heliocentric_distance(j2000_ott, heliocentric_distance)
    !    """Instantaneous orbital radius"""
    real(kind(0.d0)) :: j2000_ott, heliocentric_distance
    real(kind(0.d0)) :: M
    call mars24_Mars_Mean_Anomaly(j2000_ott, M)
    M=M*dtr

    heliocentric_distance = 1.523679 * &
         (1.00436 - 0.09309*cos(M) &
         - 0.004336*cos(2*M) &
         - 0.00031*cos(3*M) &
         - 0.00003*cos(4*M))

  end subroutine mars24_heliocentric_distance

  subroutine mars24_heliocentric_longitude(j2000_ott, heliocentric_longitude)
    !    """Heliocentric longitude"""
    real(kind(0.d0)) :: j2000_ott, heliocentric_longitude
    real(kind(0.d0)) :: ls

    call mars24_Mars_Ls(j2000_ott, ls)

    heliocentric_longitude = ls + 85.061 - &
         0.015 * sin((71+2*ls)*dtr) - &
         5.5e-6*j2000_ott

    heliocentric_longitude = modulo(heliocentric_longitude + 360., 360.)

  end subroutine mars24_heliocentric_longitude


  subroutine mars24_heliocentric_latitude(j2000_ott, heliocentric_latitude)
    !    """Heliocentric Latitude"""
    real(kind(0.d0)) :: j2000_ott, heliocentric_latitude
    real(kind(0.d0)) :: ls

    call mars24_Mars_Ls(j2000_ott, ls)


    heliocentric_latitude = -(1.8497 - 2.23e-5*j2000_ott) &
         * sin((ls - 144.50 + 2.57e-6*j2000_ott)*dtr)

  end subroutine mars24_heliocentric_latitude

  subroutine mars24_hourangle(longitude, j2000_ott, hourangle)
    !    """Hourangle is the longitude - subsolar longitude"""
    real(kind(0.d0)) :: j2000_ott, longitude, hourangle
    real(kind(0.d0)) :: subsolar_longitude

    call mars24_subsolar_longitude(j2000_ott, subsolar_longitude)
    hourangle = longitude*dtr - subsolar_longitude*dtr

  end subroutine mars24_hourangle

  subroutine mars24_solar_zenith(longitude,latitude, j2000_ott, solar_zenith)
    !    """Zenith Angle"""

    real(kind(0.d0)) :: j2000_ott, longitude, latitude
    real(kind(0.d0)) :: ha, ls, dec, cosZ, solar_zenith

    call mars24_hourangle(longitude, j2000_ott, ha)
    call mars24_Mars_Ls(j2000_ott, ls)
    call mars24_solar_declination(ls,dec)
    dec=dec*dtr

    cosZ = sin(dec) * sin(latitude*dtr) + &
         cos(dec)*cos(latitude*dtr)*cos(ha)

    solar_zenith = acos(cosZ)/dtr
  end subroutine mars24_solar_zenith

  subroutine mars24_solar_elevation(longitude, latitude, j2000_ott, solar_elevation)
    !    """Elevation = 90-Zenith"""
    real(kind(0.d0)) :: longitude, latitude, j2000_ott, solar_elevation, solar_zenith
    call mars24_solar_zenith(longitude, latitude, j2000_ott, solar_zenith)
    solar_elevation = 90 - solar_zenith

    
  end subroutine mars24_solar_elevation

  subroutine mars24_solar_azimuth(longitude, latitude, j2000_ott, solar_azimuth)
    !    """Azimuth Angle"""
    real(kind(0.d0)) :: longitude, latitude, j2000_ott
    real(kind(0.d0)) :: ha, ls, dec, denom, num, solar_azimuth

    call mars24_hourangle(longitude, j2000_ott, ha)
    call mars24_Mars_Ls(j2000_ott, ls)
    call mars24_solar_declination(ls, dec)
    dec=dec*dtr
    denom = (cos(latitude*dtr)*tan(dec) &
         - sin(latitude)*cos(ha))

    num = sin(ha) 
    solar_azimuth = modulo(360.+atan2(num,denom)/dtr,360.)

  end subroutine mars24_solar_azimuth

end module module_mars24