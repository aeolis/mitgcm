C-- no common blocks yet, may set options to WRF physics later and 
C-- enable common blocks 
C-- add tendencies of u, v, w, T and q 
C-- define phydics variables in radtran, surface scheme and PBL scheme

C==// Physics tendencies 

C-- RTHRATEN: radiative heating rate
C-- RUBLTEN:  U tendency due to PBL mixing on A grid (m/s^2)
C-- RVBLTEN:  V tendency due to PBL mixing on A grid (m/s^2)
C-- RTHBLTEN: theta tendency due to PBL mixing (K/s)
C-- RTRBLTEN: Passive tracer tendency due to PBL mixing (only need ONE) (kg/kg/s)
C--           This is corresponding to the tracer field PTR_DIAG defined below

C-- The following are tracer tendencies from active tracer cycle
C-- RQVBLTEN: Vapor phase tendency due to PBL mixing  (kg/kg/s)
C-- RQCBLTEN: Liquid phase tendency due to PBL mixing (kg/kg/s)
C-- RQIBLTEN: Solid phase tendency due to PBL mixing  (kg/kg/s)

C-- RCO2MPTEN: co2 ice tendency due to microphysical processes     (kg/kg/s)
C-- RTRMPTEN: passive tracer tendency due to a simple sink-source  (kg/kg/s)
C-- RQVMPTEN: Vapor phase tendency due to microphysical processes  (kg/kg/s)
C-- RQCMPTEN: Liquid phase tendency due to microphysical processes (kg/kg/s)
C-- RQIMPTEN: Solid phase tendency due to microphysical processes  (kg/kg/s)

C-- RQST01BLTEN: dust tendency for psize 1 (kg/kg/s)
C-- RQST02BLTEN: dust tendency for psize 2 (kg/kg/s)


      COMMON /physics_tendency/
     &                RTHRATEN, RTHRATENSW, RTHRATENLW,
     &                RQVBLTEN, RQCBLTEN, RQIBLTEN,
     &                RUBLTEN,  RVBLTEN,  RTHBLTEN,
     &                RQVMPTEN, RQCMPTEN, RQIMPTEN,
     &                RTRBLTEN, RTRMPTEN, RMPDTTEN,
     &                RQST01BLTEN, RQST02BLTEN, 
     &                RQST01MPTEN, RQST02MPTEN,
     &                RCO2MPTEN,   RCO2BLTEN

      _RL RTHRATEN    (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RTHRATENSW  (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RTHRATENLW  (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RUBLTEN     (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RVBLTEN     (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RTHBLTEN    (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RTRBLTEN    (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RQVBLTEN    (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RQCBLTEN    (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RQIBLTEN    (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RCO2BLTEN   (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RTRMPTEN    (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RQVMPTEN    (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RQCMPTEN    (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RQIMPTEN    (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RMPDTTEN    (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RCO2MPTEN   (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RQST01BLTEN (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RQST02BLTEN (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RQST01MPTEN (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL RQST02MPTEN (ims:ime,jms:jme,kms:kme,nSx,nSy )

C==// State variables 

C-- WRF State Common - State variables
C-- gtrphy are the tracer tendencies (due to physics)
C-- gTracer(:,:,:,nSx,nSy,1) is reserved for one passive tracer PTR_DIAG

C-- uphy:      zonal wind on A grid (m/s)
C-- vphy:      meridional wind on A grid (m/s)
C-- thphy:     potential temperature, same as theta but with one more vertical layer
C-- pephy:     pressure at the interfaces of the vertical grid (Pa)
C-- pcphy:     pressure at the center of the vertical grid (Pa)
C-- RNET_2D:   output to calculate surface heat balance (K/s)
C-- HFX:       upward heat flux (sensible heat flux) at the surface (W/m^2) 
C-- QFX:       upward moisture flux at the surface (kg/m^2/s)
C-- HOL:       PBL height over Monin-Obukhov length
C-- UST:       u* in similarity theory (m/s)
C-- PBL:       PBL height (m)
C-- ZNT:       roughness length (m)
C-- GSW:       downward short wave flux at ground surface (W/m^2)
C-- GLW:       downward long wave flux at ground surface (W/m^2)
C-- ALBEDO:    surface albedo
C-- FLHC:      exchange coefficient for heat (m/s)
C-- FLQC:      exchange coefficient for moisture (m/s
C-- LH:        latent heat flux at the surface (W/m^2)
C-- GRDFLX:    diffusive heating rate at ground (W/m^2)
C-- EMISS:     surface emissivity (0 to 1)
C-- MAVAIL:    surface moisture availability (between 0 and 1)
C-- TMN:       soil temperature at lower boundary (K)
C-- THC:       thermal inertia (Cal/cm/K/s^0.5)
C-- QSFC:      ground saturated mixing ratio
C-- gTrphy:    tracer (Ar, N2, H2O etc) tendencies in due to physics (kg/kg/s)
C-- gublphy:   PBL tendency of zonal wind on C-grid  
C-- gvblphy:   PBL tendency of meridional wind on C-grid  
 
      COMMON /physics_state_3D/
     &                uphy,     vphy,     thphy,
     &                pephy,    pcphy 

C--  For dynamic variables
      _RL uphy (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL vphy (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL thphy(ims:ime,jms:jme,kms:kme,nSx,nSy )

C-- For pressure with r* correction
      _RL pephy     (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL pcphy     (ims:ime,jms:jme,kms:kme,nSx,nSy )

      COMMON /physics_tend_3D/
     &                gublphy,  gvblphy,  gtrphy

C-- For PBL mixing tendencies
      _RL gublphy (ims:ime,jms:jme,kts:kte,nSx,nSy)
      _RL gvblphy (ims:ime,jms:jme,kts:kte,nSx,nSy)
#ifdef WRF_ENABLE_TRACERS
      _RL gtrphy  (ims:ime,jms:jme,kts:kte,nSx,nSy,PTRACERS_num)
#else
      _RL gtrphy  (ims:ime,jms:jme,kts:kte,nSx,nSy)
#endif

      COMMON /physics_state_2D/
     &                RNET_2D,
     &                HFX,      QFX,      HOL,
     &                UST,      PBL,      ZNT,
     &                GSW,      GLW,      ALBEDO,
     &                FLHC,     FLQC,     LH,
     &                GRDFLX,   EMISS,    MAVAIL,
     &                TMN,      THC,      QSFC,
     &                mup,          
     &                ALBBCK,   EMBCK,    THCBCK 

C--  For Radtran
      _RL RNET_2D (ims:ime,jms:jme, nSx,nSy )
      _RL GSW     (ims:ime,jms:jme, nSx,nSy )
      _RL GLW     (ims:ime,jms:jme, nSx,nSy )
      _RL ALBEDO  (ims:ime,jms:jme, nSx,nSy )

C--  For PBL, surface scheme 
      _RL HFX     (ims:ime,jms:jme, nSx,nSy )
      _RL QFX     (ims:ime,jms:jme, nSx,nSy )
      _RL HOL     (ims:ime,jms:jme, nSx,nSy )
      _RL UST     (ims:ime,jms:jme, nSx,nSy )
      _RL PBL     (ims:ime,jms:jme, nSx,nSy )
      _RL ZNT     (ims:ime,jms:jme, nSx,nSy )

C--  For surface scheme 
      _RL FLHC    (ims:ime,jms:jme, nSx,nSy )
      _RL FLQC    (ims:ime,jms:jme, nSx,nSy )
      _RL LH      (ims:ime,jms:jme, nSx,nSy )
      _RL GRDFLX  (ims:ime,jms:jme, nSx,nSy )
      _RL EMISS   (ims:ime,jms:jme, nSx,nSy )
      _RL MAVAIL  (ims:ime,jms:jme, nSx,nSy )
      _RL TMN     (ims:ime,jms:jme, nSx,nSy )
      _RL THC     (ims:ime,jms:jme, nSx,nSy )
      _RL QSFC    (ims:ime,jms:jme, nSx,nSy )

C-- For surface pressure perturbation due to CO2 cycle
      _RL mup(ims:ime,jms:jme,nSx,nSy)

C-- For background surface properties (loaded from files)
      _RL ALBBCK   (ims:ime,jms:jme, nSx,nSy )
      _RL EMBCK    (ims:ime,jms:jme, nSx,nSy )
      _RL THCBCK   (ims:ime,jms:jme, nSx,nSy )

C==// Diagnostics variables for radiation

C-- hr_ir:     Heating rate in IR wavelengths  (K/s)
C-- hr_a_ir:   Heating rate due to aerosol in IR wavelengths (K/s)
C-- hr_vis:    Heating rate in Solar wavelengths  (K/s)
C-- hr_a_vis:  Heating rate due to aerosol in Solar wavelengths (K/s)

      COMMON /physics_diag/
     &               hr_ir,   hr_a_ir,  hr_g_ir,  
     &               hr_vis,  hr_a_vis, hr_g_vis, 
     &               hr_g_uv

      _RL hr_ir     (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL hr_a_ir   (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL hr_g_ir   (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL hr_vis    (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL hr_a_vis  (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL hr_g_vis  (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL hr_g_uv   (ims:ime,jms:jme,kms:kme,nSx,nSy )

C==// Subsurface/surface properties

C-- Mars subsurface properties for water cycle
C-- porosity:       porosity of soil layers (between 0 to 1)
C-- soil_den:       bulk density of soil layers (accounting for porosity) (kg/m3)
C-- ss_vapor:       Vapor density in pore space (kg/m3 gas)
C-- ss_ice:         density of ice in pore space (kg/m^3)
C-- ss_adsorbate:   Adsorbate density in pore space (kg/m3)
C-- porosity_mod:   Modified (wet) porosity including ice blocking
C-- qflux:          Vapor flux through surface (kg/m2/s)

      COMMON /subsurface_properties/
     &       qflux, soil_ice_dp, soil_ice_pc,
     &       porosity, soil_den, 
     &       soil_cap, soil_cond,
     &       ss_vapor, ss_ice, ss_adsorbate,
     &       porosity_mod

      _RL qflux       (ims:ime,jms:jme, nSx,nSy)
      _RL soil_ice_dp (ims:ime,jms:jme, nSx,nSy)
      _RL soil_ice_pc (ims:ime,jms:jme, nSx,nSy)
      _RL porosity    (ims:ime,jms:jme, 1:num_soil_layers, nSx,nSy)
      _RL soil_den    (ims:ime,jms:jme, 1:num_soil_layers, nSx,nSy)
      _RL soil_cap    (ims:ime,jms:jme, 1:num_soil_layers, nSx,nSy)
      _RL soil_cond   (ims:ime,jms:jme, 1:num_soil_layers, nSx,nSy)
      _RL ss_vapor    (ims:ime,jms:jme, 1:num_soil_layers, nSx,nSy)
      _RL ss_ice      (ims:ime,jms:jme, 1:num_soil_layers, nSx,nSy)
      _RL ss_adsorbate(ims:ime,jms:jme, 1:num_soil_layers, nSx,nSy)
      _RL porosity_mod(ims:ime,jms:jme, 1:num_soil_layers, nSx,nSy)

C==// physics parameters and tracers

C-- DUST_ARRAY:       aerosol species in atmosphere (kg/kg or opacity)
C-- CLOUD_ARRAY:      clouds in atmosphere (kg/kg or opacity)
C-- qv_column:        vertically integrated water vapor mass density in air (g/m^2)
C-- qi_column:        vertically integrated water ice mass density in air (g/m^2)
C-- co2ice:           surface CO2 ice density (kg/m^2)
C-- h2oice:           surface water ice density (kg/m^2)
C-- co2sublimate:     total amount of CO2 ice sublimated from surface (kg/m^2)
C-- SNOWC:            flag indicating snow coverage (1 for snow cover)
C-- ground_ice_depth: ground ice depth (m) -- not used
C-- ground_ice_percentage: ground ice percentage -- not used
C-- co2dep_air:       CO2 condensates in the air (kg/m^2)
C-- co2sub_air:       CO2 ice sublimated in the air (kg/m^2)
C-- co2sub_grd:       CO2 condensates fallen to the surface from air (kg/m^2)

#ifdef WRF_MARS
      COMMON /WRF_MARS_TRACERS_3D/
     &       DUST_ARRAY, CLOUD_ARRAY, co2dep_air, kappa_conv

      _RL DUST_ARRAY (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL CLOUD_ARRAY(ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL co2dep_air (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL co2sub_air (ims:ime,jms:jme,kms:kme,nSx,nSy )
      _RL kappa_conv (ims:ime,jms:jme,kms:kme,nSx,nSy )

      COMMON /WRF_MARS_TRACERS_2D/
     &       qv_column, qi_column,
     &       co2ice, h2oice,
     &       co2sublimate, SNOWC,
     &       ground_ice_depth,
     &       ground_ice_percentage,
     &       co2sub_grd,
     &       co2cape

      _RL qv_column (ims:ime,jms:jme, nSx,nSy )
      _RL qi_column (ims:ime,jms:jme, nSx,nSy )

      _RL co2ice                (ims:ime,jms:jme, nSx,nSy )
      _RL h2oice                (ims:ime,jms:jme, nSx,nSy )
      _RL co2sublimate          (ims:ime,jms:jme, nSx,nSy )
      _RL SNOWC                 (ims:ime,jms:jme, nSx,nSy )
      _RL ground_ice_depth      (ims:ime,jms:jme, nSx,nSy )
      _RL ground_ice_percentage (ims:ime,jms:jme, nSx,nSy )
      _RL co2sub_grd            (ims:ime,jms:jme, nSx,nSy )

      _RL co2cape                  (ims:ime,jms:jme, nSx,nSy )

      COMMON /WRF_MARS_DUST/
     &       nlif1, nlif2,
     &       dlif1, dlif2,
     &       dsed1, dsed2,
     &       surftracer1, surftracer2,
     &       dust_mix_depth

C nlif1: NSWS (near surface wind stress lifting) dust lifted since last output for psize 1 (kg/m^2)
C nlif2: NSWS (near surface wind stress lifting) dust lifted since last output for psize 2 (kg/m^2)
C dlif1: DDEV (dust devil lifting) dust lifted since last output for psize 1 (kg/m^2)
C dlif2: DDEV (dust devil lifting) dust lifted since last output for psize 2 (kg/m^2)
C dsed1: Dust amount deposited since last output for psize 1 (kg/m^2)
C dsed2: Dust amount deposited since last output for psize 2 (kg/m^2)
C surftracer1: Tracer (dust) on the surface for psize 1 (kg/m^2)
C surftracer2: Tracer (dust) on the surface for psize 2 (kg/m^2)
C dust_mix_depth: height to dust mixing level (m)

      _RL nlif1   (ims:ime,jms:jme, nSx,nSy )
      _RL nlif2   (ims:ime,jms:jme, nSx,nSy )
      _RL dlif1   (ims:ime,jms:jme, nSx,nSy )
      _RL dlif2   (ims:ime,jms:jme, nSx,nSy )
      _RL dsed1   (ims:ime,jms:jme, nSx,nSy )
      _RL dsed2   (ims:ime,jms:jme, nSx,nSy )
      _RL surftracer1   (ims:ime,jms:jme, nSx,nSy )
      _RL surftracer2   (ims:ime,jms:jme, nSx,nSy )
      _RL dust_mix_depth(ims:ime,jms:jme, nSx,nSy )
#endif

#ifdef WRF_TITAN
C CH4SURF:    surface methane (kg/m^2), now use surftracer1 instead
C TSCH4EVAP:  Cooling due to surface methane evaporation (K)
C CH4EVP:     amount surface methane being evaporated (kg/m^2)
      COMMON /WRF_TITAN_TRACERS/
     &        surftracer1, TSCH4EVAP, CH4EVP 

      _RL surftracer1   (ims:ime,jms:jme, nSx,nSy )
      _RL TSCH4EVAP     (ims:ime,jms:jme, nSx,nSy )
      _RL CH4EVP        (ims:ime,jms:jme, nSx,nSy )
#endif

C==// soil depth configuration, can be initialized in wrf_init_vars 
C-- DZS:    soil layer thickness (m)
C-- ZS:     soil layer hight     (m)

      COMMON /WRF_SOIL/
     &        ZS, DZS

      _RL ZS (1:num_soil_layers)
      _RL DZS(1:num_soil_layers)

C==// Shared physics properties

C== Solar properties
C-- HA:            Hour angle of sun light
C-- ANGSLOPE:      surface slope angle (radians)
C-- AZMSLOPE:      surface slope azimuth (radians)
C-- sza0:          mean cosine zenith angle(do not divide by r**2)
C-- sunfrac:       fraction of daylight coverage
C== Surface and PBL properties
C-- XLAND:         land mask (1 for land, 2 for water)
C-- WSPD:          wind speed at lowest model level (m/s)
C-- GZ1OZ0:        log(z/z0) where z0 is roughness length
C-- BR:            Bulk Richardson number in surface layer
C-- TSK:           Surface temperature (K)
C-- ZOL:           z/L height over Monin-Obukhov length
C-- KPBL2D:        PBL layer number 
C-- PSIM:          similarity stability function for momentum
C-- PSIH:          similarity stability function for heat
C-- PSFC:          pressure at the surface (Pa)
C-- XKZMBL, XKZHBL ?? -- not used
C-- REGIME:        flag indicating PBL regime (stable, unstable, etc.)
C-- CAPG:          heat capacity for soil (J/K/m^3)
C-- TSLB:          soil temperature in 12-layer model
C-- PBLH:          PBL height from previous time (m)
C-- MOL:           T* (similarity theory) (K)
C-- RMOL:          1.0 over Monin-Obukhov length
C-- QGH:           saturation water vapor mixing ration at ground level (kg/kg)
C-- dz8w:          layer thickness between edges of the vertical grid (m)
C-- dzc:           layer thickness between centers of the vertical grid (m)
C-- ze:            altitude at the edges of the vertical grid (m)
C-- zc:            altitude at the centers of the vertical grid (m)

      COMMON /WRF_PHYS_COMS_3D/
     &       CAPG, XKZMBL, XKZHBL, TSLB,
     &       dz8w, dzc, ze, zc
     
      _RL CAPG    (ims:ime,jms:jme, 1:num_soil_layers, nSx,nSy)
      _RL TSLB    (ims:ime,jms:jme, 1:num_soil_layers, nSx,nSy)
      _RL XKZMBL  (ims:ime,jms:jme, kms:kme, nSx,nSy )
      _RL XKZHBL  (ims:ime,jms:jme, kms:kme, nSx,nSy )
      _RL dz8w    (ims:ime,jms:jme, kms:kme, nSx,nSy )
      _RL dzc     (ims:ime,jms:jme, kms:kme, nSx,nSy )
      _RL ze      (ims:ime,jms:jme, kms:kme, nSx,nSy )
      _RL zc      (ims:ime,jms:jme, kms:kme, nSx,nSy )

      COMMON /WRF_PHYS_COMS_2D/
     &       HA,     ANGSLOPE, AZMSLOPE, 
     &       sza0,   sunfrac,  XLAND,
     &       WSPD,   GZ1OZ0,   BR,
     &       TSK,    ZOL,      KPBL2D,
     &       PSIM,   PSIH,     PSFC,
     &       REGIME, PBLH,     MOL, 
     &       RMOL,   QGH
         
      _RL HA      (ims:ime,jms:jme, nSx,nSy )
      _RL ANGSLOPE(ims:ime,jms:jme, nSx,nSy )
      _RL AZMSLOPE(ims:ime,jms:jme, nSx,nSy )
      _RL sza0    (ims:ime,jms:jme, nSx,nSy )
      _RL sunfrac (ims:ime,jms:jme, nSx,nSy )
      _RL XLAND   (ims:ime,jms:jme, nSx,nSy )
      _RL WSPD    (ims:ime,jms:jme, nSx,nSy )
      _RL GZ1OZ0  (ims:ime,jms:jme, nSx,nSy )
      _RL BR      (ims:ime,jms:jme, nSx,nSy )
      _RL TSK     (ims:ime,jms:jme, nSx,nSy )
      _RL ZOL     (ims:ime,jms:jme, nSx,nSy )
      _RL PSIM    (ims:ime,jms:jme, nSx,nSy )
      _RL PSIH    (ims:ime,jms:jme, nSx,nSy )
      _RL PSFC    (ims:ime,jms:jme, nSx,nSy )
      _RL REGIME  (ims:ime,jms:jme, nSx,nSy )
      _RL PBLH    (ims:ime,jms:jme, nSx,nSy )
      _RL MOL     (ims:ime,jms:jme, nSx,nSy )
      _RL RMOL    (ims:ime,jms:jme, nSx,nSy )
      _RL QGH     (ims:ime,jms:jme, nSx,nSy )
      INTEGER KPBL2D  (ims:ime,jms:jme, nSx,nSy )     
      


