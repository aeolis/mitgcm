#include "CPP_OPTIONS.h"
#include "planetWRF_OPTIONS.h"

      SUBROUTINE radconst(XTIME,DECLIN,SOLCON,JULIAN,                   
     &                  DEGRAD,DPD, planet_year,               
     &                  obliquity,eccentricity,semimajoraxis,         
     &                  equinox_fraction,zero_date,                   
     &                  xlat,ha,P2SI,sza0,sunfrac,l_s,sunbody,        
     &                  ids,ide, jds,jde, kds,kde,                    
     &                  ims,ime, jms,jme, kms,kme,                    
     &                  its,ite, jts,jte, kts,kte                     )
C!---------------------------------------------------------------------
C   USE module_wrf_error
C   USE module_model_constants, ONLY : planet_year
      IMPLICIT NONE
C!---------------------------------------------------------------------

C! !ARGUMENTS:
      INTEGER planet_year 
      _RL DEGRAD,DPD,XTIME,JULIAN
      _RL DECLIN,SOLCON
      _RL OBECL,SINOB,SXLONG,ARG, 
     &    DECDEG,DJUL,RJUL,ECCFAC

      INTEGER ids,ide, jds,jde, kds,kde, 
     &        ims,ime, jms,jme, kms,kme, 
     &        its,ite, jts,jte, kts,kte
      _RL l_s,sunbody
      _RL P2SI, obliquity, eccentricity
      _RL semimajoraxis,equinox_fraction,zero_date
      _RL XLAT( ims:ime, jms:jme )
      _RL HA     ( ims:ime, jms:jme ), 
     &    sza0   ( ims:ime, jms:jme ), 
     &    sunfrac( ims:ime, jms:jme )


C   !
C   ! !DESCRIPTION:
C   ! Compute terms used in radiation physics 
C   ! This subroutine computes radius vector, declination and right ascension of
C   ! sun, equation of time, and hour angle of sun at sunset, the mean
C   ! cosine of the sun's zenith angle and daylight fraction given the julian
C   ! day and fraction of a julian day
C   !
C   ! definition of the arguments used by this subroutine
C   !
C   ! julday =         julian day
C   ! radius =         radius vector (distance to sun in a. u.)
C   ! declin =         declination of sun
C   ! alp    =         right ascension of sun
C   ! slag   =         apparent sun lag angle
C   ! sza0   =         mean cosine zenith angle(do not divide by r**2).
C   ! sunfrac=         fraction of daylight
C   ! als    =         solar longitude
C   !
C   !     note - all angles are expressed in radians
C   !
C   !EOP
   
C   ! Local Variables
      _RL PI
      _RL DELEQN, DP_DATE

      _RL EM, W, ALS, RADIUS, SINDEC, PH, HA1, SS, CC
      _RL AR, AC, CD0, AP, EPS
      INTEGER I, J

      _RL EP, E, EQ, QQ, EN, ER, CR, SMALL_VALUE
   
C   !DBLE(KIND (0d0)) :: alp, slag, tini, tst

      PI=ACOS(-1.d0)
C   !small_value = 1.d-7      ! caused problems on certain single precision
C                             ! machines when it got stuck at: 4.7683716E-07
      SMALL_VALUE = 1.D-6
      DELEQN = EQUINOX_FRACTION * DBLE(PLANET_YEAR)


   
C   ! for short wave radiation
   
      DECLIN=0.
      SOLCON=0.
      l_s = 0.
   
C   !-----OBECL : OBLIQUITY = 23.5 DEGREE.
        
      OBECL=OBLIQUITY*DEGRAD
      SINOB=SIN(OBECL)
        
C   !-----CALCULATE LONGITUDE OF THE SUN FROM VERNAL EQUINOX:
        
C   ! DP_DATE = DAYS SINCE LAST PERIHELION PASSAGE
      DP_DATE = JULIAN - ZERO_DATE
      DO WHILE (DP_DATE < 0.)
        DP_DATE=DP_DATE+DBLE(PLANET_YEAR)
      END DO
      DO WHILE (DP_DATE > DBLE(PLANET_YEAR))
        DP_DATE=DP_DATE-DBLE(PLANET_YEAR)
      END DO
   
      ER = SQRT( (1.0+ECCENTRICITY)/(1.0-ECCENTRICITY) )
   
C   ! qq is the mean anomaly
      QQ = 2.0 * (PI * DELEQN / DBLE(PLANET_YEAR))
   
C   ! Determine true anomaly at equinox: eq
C   ! Iteration for eq
      E = 1.0
      CD0 = 1.
      DO WHILE (CD0 > SMALL_VALUE)
        EP = E - (E-ECCENTRICITY*SIN(E)-QQ) / (1.0-ECCENTRICITY*COS(E))
        CD0 = ABS(E-EP)
        E = EP
      END DO
      EQ = 2. * ATAN( ER * TAN(0.5*E) )

C   ! Determine true anomaly at current date:  w
C   ! Iteration for w
      EM = 2.0 * PI * DP_DATE / DBLE(PLANET_YEAR)
      E = 1.0
      CD0 = 1.
      DO WHILE (CD0 > SMALL_VALUE)
        EP = E-(E-ECCENTRICITY*SIN(E)-EM) / (1.0-ECCENTRICITY*COS(E))
        CD0 = ABS(E-EP)
        E = EP
      END DO
      W = 2.0 * ATAN( ER * TAN(0.5*E) )

C   ! Radius vector ( astronomical units:  AU )
      ALS= (W - EQ)/DEGRAD      !Aerocentric Longitude
      IF (ALS.LT.0.) ALS=ALS+360.
      l_s = DBLE(ALS)
      RADIUS = SEMIMAJORAXIS * (1.0 - ECCENTRICITY * COS(E))
      sunbody= radius
C   ! Declination: declin
C   ! sindec = sine of angle of inclination of planet's orbit
      SINDEC = SINOB * SIN(w-eq)
      DECLIN = ASIN(SINDEC)
      DECDEG=DECLIN/DEGRAD

C   ! Right Ascension: alp
C   ! Right ascension is only used for sun lag
C   !tini = 0.d0
C   !IF (obliquity > 0.) tini = 1.d0/TAN(obecl)
C   !alp = ASIN(TAN(declin)*tini)
C   !tst = COS(w-eq)
C   !IF (tst < 0.d0) alp = pi  - alp
C   !IF (alp < 0.d0) alp = alp + 2.d0*pi


      DO j = jts,jte
        DO i = its,ite
           PH = PI*XLAT(I,J)/180.

           IF (DECLIN.NE.0.0) THEN
              AP = ABS(PH)
C            ! EPS is absolute angular distance from pole (radians)
              EPS = ABS(AP - 0.5 * PI)

              IF(EPS.LE.SMALL_VALUE) THEN
C               ! i.e., if we are at a pole point
C               ! Hour angle of sunset at the pole is either
C               ! 0 or pi, depending on declination
                 HA1 = 0.5 * PI * ABS(AP / PH + ABS(DECLIN) / DECLIN)
                 SS = SIN(PH) * SIN(DECLIN)
                 CC = 0.0
               ELSE
C               ! we are not a pole point
                 SS = SIN(PH) * SIN(DECLIN)
                 CC = COS(PH) * COS(DECLIN)
                 AR = - SS / CC
                 AC = ABS(AR)

                 IF((AC - 1. + SMALL_VALUE) .LT. 0.0) THEN
C                  ! ABS(-SS/CC) was < 1. (can do ACOS)
                    HA1 = ACOS(AR)
                 ELSE IF((AC - 1. + SMALL_VALUE) .EQ. 0.0) THEN
C                  ! ABS(-SS/CC) is 0., result is +/- PI/2
                    HA1 = (AC - AR) * 0.5 * PI
                 ELSE
C                  ! ABS(-SS/CC) was > 1. (can't do ACOS)
                   IF(AR .LT. 0.) THEN
                     HA1 = PI
                   ELSE
                     HA1 = 0.0
                   END IF
                 END IF
              END IF
           ELSE
              HA1 = 0.5 * PI
              SS = 0.0
              CC= COS(PH)
           END IF


           HA(I,J)=DBLE(HA1)
           sunfrac(i,j) = DBLE(HA1/PI)
C         ! Integrating from -h to +h yields the average zenith angle
C         ! sza is zenith angle averaged over daylight hours
C         ! SIN(ha1)/ha1 is {integral of cos(theta) dtheta} / 
C         !                 {integral of dtheta} ... ????
           if (ha1 == 0.d0) then
              sza0(i,j)=0.
           else
              sza0(i,j)=MAX(DBLE(ss+cc*SIN(ha1)/ha1),0.)
           end if
        END DO
      END DO

      SOLCON=1367.6/(radius*radius)

      RETURN
      END
