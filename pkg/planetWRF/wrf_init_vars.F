C $Header: /u/gcmpack/MITgcm/pkg/wrf/wrf_init_vars.F,v 1.22 2009/05/12 19:56:35 jmc Exp $
C $Name:  $
#include "PACKAGES_CONFIG.h"
#include "CPP_OPTIONS.h"
#include "planetWRF_OPTIONS.h"
       subroutine wrf_init_vars (myThid)
c-----------------------------------------------------------------------
c  Routine to initialise the WRF state.
c
c  Input: myThid       - Process number calling this routine
c
c  Notes:
c   1) For a Cold Start -
c      This routine takes the initial condition on the dynamics grid
c      and interpolates to the physics grid to initialize the state
c      variables that are on both grids. It initializes the variables
c      of the turbulence scheme to 0., and the land state from a model
c      climatology.
c   2) For a Restart, read the WRF pickup file
c   3) The velocity component physics fields are on an A-Grid
c
c-----------------------------------------------------------------------
       implicit none
#include "SIZE.h"
#include "WRF_SIZE.h"
#include "EEPARAMS.h"
#include "SURFACE.h"
#include "PARAMS.h"
#include "GRID.h"
#include "DYNVARS.h"
#ifdef WRF_ENABLE_TRACERS
#include "PTRACERS_SIZE.h"
#include "PTRACERS_PARAMS.h"
#include "PTRACERS_FIELDS.h"
#endif /* WRF_ENABLE_TRACERS */
#include "WRF_PHYS.h"
#include "WRF_CONFIG.h"
#ifdef ALLOW_EXCH2
#include "W2_EXCH2_SIZE.h"
#include "W2_EXCH2_TOPOLOGY.h"
#endif /* ALLOW_EXCH2 */

       integer myThid

       INTEGER xySize
#if defined(ALLOW_EXCH2)
       PARAMETER ( xySize = W2_ioBufferSize )
#else
       PARAMETER ( xySize = Nx*Ny )
#endif
       Real*8 globalArr( xySize*8 )

       integer i, j, L, bi, bj, iTracer
       integer xsize, ysize
       logical alarm
       external alarm

#if defined(ALLOW_EXCH2)
       xsize = exch2_global_Nx
       ysize = exch2_global_Ny
#else
       xsize = Nx
       ysize = Ny
#endif
 
C Before we start using planetary WRF packages, check the configuration first
C Mars simulation requires sigma coordinate
#ifdef WRF_MARS
      IF ((topoFile.NE.' ') .AND. (selectSigmaCoord.NE.1)) THEN
          print *,'MARS GCM ENABLED: '
          print *,'MUST USE SIGMA COORDINATE' 
          print *,'SET selectSigmaCoord=1 in data'
          stop
      ENDIF
#endif
       
C Deal Here with Variables that are on a Fizhi Pickup or need Initialization

      IF ( startTime.EQ.baseTime .AND. nIter0.EQ.0 ) THEN
      print *,' In wrf_init_vars: Beginning of New Experiment '

        DO bj = myByLo(myThid), myByHi(myThid)
        DO bi = myBxLo(myThid), myBxHi(myThid)

C initialize pressure array
           DO j = jms,jme
           DO i = ims,ime
           DO L = kms, kme
             pephy(i,j,L,bi,bj)=0.
             pcphy(i,j,L,bi,bj)=0.
           ENDDO
           ENDDO
           ENDDO
         
        ENDDO
        ENDDO

       CALL BUILD_PRESSURE(pephy, pcphy, myThid)

c Create initial fields on phys. grid - Move Dynamics u and v to A-Grid
       CALL rotate_uv2en_rl(
     .          uvel, vvel,
     .          uphy(ims:ime,jms:jme,kts:kte,1:nSx,1:nSy),
     .          vphy(ims:ime,jms:jme,kts:kte,1:nSx,1:nSy),
     .          .TRUE., .TRUE., .TRUE., Nr, mythid
     .                     )

        DO bj = myByLo(myThid), myByHi(myThid)
        DO bi = myBxLo(myThid), myBxHi(myThid)

           DO L = kts,kte
           DO j = jts,jte
           DO i = its,ite
              thphy(i,j,L,bi,bj) = theta(i,j,L,bi,bj)
           ENDDO
           ENDDO
           ENDDO

C--    the following can be place holder for tracers such as H2O, Ar or N2
#ifdef WRF_ENABLE_TRACERS
C initialize tracer and tracer tendencies on physics grid

C A safety check for tracer definition
C PTRACERS_num >= PTRACERS_numInUse in general but the size of 
C Ptracers_ref needs to be consistent with PTRACERS_num
           IF (PTRACERS_num .ne. PTRACERS_numInUse) THEN
             write (0,*) "Warning: PTRACERS_num != PTRACERS_numInUse"
           ENDIF
          
           DO iTracer = 1, PTRACERS_num
           DO L = kts,kte
           DO j = jms,jme
           DO i = ims,ime
              gTrphy(i,j,L,bi,bj,iTracer) = 0.
           ENDDO
           ENDDO
           ENDDO
           ENDDO  
#else
           DO L = kts,kte
           DO j = jms,jme
           DO i = ims,ime
              gTrphy(i,j,L,bi,bj) = 0.
           ENDDO
           ENDDO
           ENDDO
#endif /* WRF_ENABLE_TRACERS */
       
C Initialize the varibles and 
c zero out WRF tendency arrays on the WRF grid
           DO L = kms,kme
           DO j = jms,jme
           DO i = ims,ime
#ifdef WRF_MARS
C--     initialize dust (could use variable dust) --
             DUST_ARRAY (I,J,L,bi,bj) = 0.0
             CLOUD_ARRAY(I,J,L,bi,bj) = 0.0
#endif
C--     initialize radiative heating rates and PBL tendencies
             hr_ir     (I,J,L,bi,bj) = 0.0
             hr_a_ir   (I,J,L,bi,bj) = 0.0
             hr_vis    (I,J,L,bi,bj) = 0.0
             hr_a_vis  (I,J,L,bi,bj) = 0.0
             RTHRATEN  (i,j,L,bi,bj) = 0.0
             RTHRATENSW(i,j,L,bi,bj) = 0.0 
             RTHRATENLW(i,j,L,bi,bj) = 0.0

             RUBLTEN   (i,j,L,bi,bj) = 0.0
             RVBLTEN   (i,j,L,bi,bj) = 0.0
             RTHBLTEN  (i,j,L,bi,bj) = 0.0
             RTRBLTEN  (i,j,L,bi,bj) = 0.0
             RQVBLTEN  (i,j,L,bi,bj) = 0.0
             RQCBLTEN  (i,j,L,bi,bj) = 0.0
             RQIBLTEN  (i,j,L,bi,bj) = 0.0
             RCO2BLTEN (i,j,L,bi,bj) = 0.0

             RTRMPTEN   (i,j,L,bi,bj) = 0.0
             RQVMPTEN   (i,j,L,bi,bj) = 0.0
             RQCMPTEN   (i,j,L,bi,bj) = 0.0
             RQIMPTEN   (i,j,L,bi,bj) = 0.0
             RMPDTTEN   (i,j,L,bi,bj) = 0.0
             RQST01BLTEN(i,j,L,bi,bj) = 0.0
             RQST02BLTEN(i,j,L,bi,bj) = 0.0
             RQST01MPTEN(i,j,L,bi,bj) = 0.0
             RQST02MPTEN(i,j,L,bi,bj) = 0.0
             RCO2MPTEN  (i,j,L,bi,bj) = 0.0   

             XKZMBL(I,J,L,bi,bj) = 0.0
             XKZHBL(I,J,L,bi,bj) = 0.0

#ifdef WRF_MARS
             co2dep_air(I,J,L,bi,bj) = 0.0
             co2sub_air(I,J,L,bi,bj) = 0.0
             kappa_conv(I,J,L,bi,bj) = 0.0
#endif
           ENDDO
           ENDDO
           ENDDO
c Zero out WRF tendency arrays on the dynamics grid
           DO L = kts, kte
           DO j = jms, jme
           DO i = ims, ime
              gublphy (i,j,L,bi,bj) = 0.
              gvblphy (i,j,L,bi,bj) = 0.
           ENDDO
           ENDDO
           ENDDO

        ENDDO
        ENDDO

C--   Initialize other WRF variables including topography etc.
        CALL init_WRFphys( myThid )

      ELSE
        print *,' In wrf_init_vars: Read from restart '

C--   Read WRF package state variables from pickup file
C--   First initialize all WRF variables
        CALL init_WRFphys( myThid )
C--   Overwrites some WRF variables from pickup files
        CALL wrf_read_pickup( nIter0, myThid )
C--   Build pressure
        CALL BUILD_PRESSURE(pephy, pcphy, myThid)

      ENDIF

      RETURN
      END
