C $Header: /u/gcmpack/MITgcm/model/src/external_forcing.F,v 1.58 2011/05/14 20:01:33 jmc Exp $
C $Name:  $

#include "PACKAGES_CONFIG.h"
#include "CPP_OPTIONS.h"

CBOP
C     !ROUTINE: EXTERNAL_FORCING_U
C     !INTERFACE:
      SUBROUTINE EXTERNAL_FORCING_U(
     I           iMin,iMax, jMin,jMax, bi,bj, kLev,
     I           myTime, myThid )
C     !DESCRIPTION: \bv
C     *==========================================================*
C     | S/R EXTERNAL_FORCING_U
C     | o Contains problem specific forcing for zonal velocity.
C     *==========================================================*
C     | Adds terms to gU for forcing by external sources
C     | e.g. wind stress, bottom friction etc ...
C     *==========================================================*
C     \ev

C     !USES:
      IMPLICIT NONE
C     == Global data ==
#include "SIZE.h"
#include "EEPARAMS.h"
#include "PARAMS.h"
#include "GRID.h"
#include "DYNVARS.h"
#include "FFIELDS.h"
#include "MC_REF.h"

C     !INPUT/OUTPUT PARAMETERS:
C     == Routine arguments ==
C     iMin,iMax :: Working range of x-index for applying forcing.
C     jMin,jMax :: Working range of y-index for applying forcing.
C     bi,bj     :: Current tile indices
C     kLev      :: Current vertical level index
C     myTime    :: Current time in simulation
C     myThid    :: Thread Id number
      INTEGER iMin, iMax, jMin, jMax, kLev, bi, bj
      _RL myTime
      INTEGER myThid

C     !LOCAL VARIABLES:
C     == Local variables ==
C     i,j       :: Loop counters
C     kSurface  :: index of surface layer
      INTEGER i, j
      INTEGER kSurface

      _RL tauc
     
      tauc = 2. _d 0 * 3600. _d 0
    
      DO j=1,sNy
      DO i=1,sNx
         gU(i,j,kLev,bi,bj) = gU(i,j,kLev,bi,bj) 
     &    - 1.0 _d 0/tauc*(uVel(i,j,kLev,bi,bj)-uRef(i,j,kLev,bi,bj))
      ENDDO
      ENDDO
CEOP

C--   Forcing term

      RETURN
      END

C---+----1----+----2----+----3----+----4----+----5----+----6----+----7-|--+----|
CBOP
C     !ROUTINE: EXTERNAL_FORCING_V
C     !INTERFACE:
      SUBROUTINE EXTERNAL_FORCING_V(
     I           iMin,iMax, jMin,jMax, bi,bj, kLev,
     I           myTime, myThid )
C     !DESCRIPTION: \bv
C     *==========================================================*
C     | S/R EXTERNAL_FORCING_V
C     | o Contains problem specific forcing for merid velocity.
C     *==========================================================*
C     | Adds terms to gV for forcing by external sources
C     | e.g. wind stress, bottom friction etc ...
C     *==========================================================*
C     \ev

C     !USES:
      IMPLICIT NONE
C     == Global data ==
#include "SIZE.h"
#include "EEPARAMS.h"
#include "PARAMS.h"
#include "GRID.h"
#include "DYNVARS.h"
#include "FFIELDS.h"
#include "MC_REF.h"

C     !INPUT/OUTPUT PARAMETERS:
C     == Routine arguments ==
C     iMin,iMax :: Working range of x-index for applying forcing.
C     jMin,jMax :: Working range of y-index for applying forcing.
C     bi,bj     :: Current tile indices
C     kLev      :: Current vertical level index
C     myTime    :: Current time in simulation
C     myThid    :: Thread Id number
      INTEGER iMin, iMax, jMin, jMax, kLev, bi, bj
      _RL myTime
      INTEGER myThid

C     !LOCAL VARIABLES:
C     == Local variables ==
C     i,j       :: Loop counters
C     kSurface  :: index of surface layer
      INTEGER i, j
      INTEGER kSurface

      _RL tauc
CEOP

      tauc=2.0 _d 0 * 3600. _d 0

C--   Forcing term

      DO j=1,sNy
      DO i=1,sNx
         gV(i,j,kLev,bi,bj) = gV(i,j,kLev,bi,bj) 
     &    - 1.0 _d 0/tauc*(vVel(i,j,kLev,bi,bj)-vRef(i,j,kLev,bi,bj))
      ENDDO
      ENDDO

      RETURN
      END

C---+----1----+----2----+----3----+----4----+----5----+----6----+----7-|--+----|
CBOP
C     !ROUTINE: EXTERNAL_FORCING_T
C     !INTERFACE:
      SUBROUTINE EXTERNAL_FORCING_T(
     I           iMin,iMax, jMin,jMax, bi,bj, kLev,
     I           myTime, myThid )
C     !DESCRIPTION: \bv
C     *==========================================================*
C     | S/R EXTERNAL_FORCING_T
C     | o Contains problem specific forcing for temperature.
C     *==========================================================*
C     | Adds terms to gT for forcing by external sources
C     | e.g. heat flux, climatalogical relaxation, etc ...
C     *==========================================================*
C     \ev

C     !USES:
      IMPLICIT NONE
C     == Global data ==
#include "SIZE.h"
#include "EEPARAMS.h"
#include "PARAMS.h"
#include "GRID.h"
#include "DYNVARS.h"
#include "FFIELDS.h"
#include "SURFACE.h"
#include "MC_REF.h"

C     !INPUT/OUTPUT PARAMETERS:
C     == Routine arguments ==
C     iMin,iMax :: Working range of x-index for applying forcing.
C     jMin,jMax :: Working range of y-index for applying forcing.
C     bi,bj     :: Current tile indices
C     kLev      :: Current vertical level index
C     myTime    :: Current time in simulation
C     myThid    :: Thread Id number
      INTEGER iMin, iMax, jMin, jMax, kLev, bi, bj
      _RL myTime
      INTEGER myThid

C     !LOCAL VARIABLES:
C     == Local variables ==
C     i,j       :: Loop counters
C     kSurface  :: index of surface layer
      INTEGER i, j
      INTEGER kSurface

      _RL pii, dTs
C     Storm relaxation time
      _RL tauc
C     Math PI
      _RL MaPI
      _RL myTimeLocal

      _RL dtheta(1-OLx:sNx+OLx,1-OLy:sNy+OLy)

      MaPI = acos(-1.0) 

      dTs  = 1.0 _d 0
      pii  = (atm_po/rC(kLev))**atm_kappa

      tauc = 1800.0 _d 0
CEOP

C--   Forcing term

      DO j=1,sNy
      DO i=1,sNx
          dtheta(i,j)=dTs* exp( - (  xC(I,J,bi,bj) - xGWS
C               Downwind source, moving at constant U
     &        - uGWS*myTime  * 180. _d 0/MaPI/( rsphere
     &        *cos(yGWS*MaPI/180. _d 0) )  )**2.0/4. _d 0)
C               latitudinal structure
     &        * exp( -(yC(I,J,bi,bj) - yGWS)**2.0/4. _d 0 )
C               lifetime of storm      
C     &        * (1.0 - cos(omgc*myTimeLocal))/2.0*omgc 
C               vertical structure, centered at 6.5bar or 2.5bar
C     &        * (1.-tanh( (log(6.5 _d 5)-log(rC(kLev)))/0.2 ))/2.0
C               T to theta conversion
     &        * pii
      ENDDO
      ENDDO

      DO j=1,sNy
      DO i=1,sNx
         IF ( (rC(kLev) .ge. 0.5 _d 5)   
     &       .AND. (rC(kLev) .le. 10. _d 5 ) )  THEN
              gT(i,j,kLev,bi,bj) = gT(i,j,kLev,bi,bj)
     &        -1. d 0/tauc*( theta(i,j,kLev,bi,bj) - 
     &        (tRef(kLev)+dtheta(i,j)) )
C            Top damping
         ELSEIF (rC(kLev) .LT. 0.08 _d 5) THEN
                gT(i,j,kLev,bi,bj) = gT(i,j,kLev,bi,bj)
     &         -1. d 0/86400.*(theta(i,j,kLev,bi,bj)-tRef(kLev))
         ELSE
                gT(i,j,kLev,bi,bj) = gT(i,j,kLev,bi,bj)
         ENDIF
      ENDDO
      ENDDO

      RETURN
      END

C---+----1----+----2----+----3----+----4----+----5----+----6----+----7-|--+----|
CBOP
C     !ROUTINE: EXTERNAL_FORCING_S
C     !INTERFACE:
      SUBROUTINE EXTERNAL_FORCING_S(
     I           iMin,iMax, jMin,jMax, bi,bj, kLev,
     I           myTime, myThid )

C     !DESCRIPTION: \bv
C     *==========================================================*
C     | S/R EXTERNAL_FORCING_S
C     | o Contains problem specific forcing for merid velocity.
C     *==========================================================*
C     | Adds terms to gS for forcing by external sources
C     | e.g. fresh-water flux, climatalogical relaxation, etc ...
C     *==========================================================*
C     \ev

C     !USES:
      IMPLICIT NONE
C     == Global data ==
#include "SIZE.h"
#include "EEPARAMS.h"
#include "PARAMS.h"
#include "GRID.h"
#include "DYNVARS.h"
#include "FFIELDS.h"
#include "SURFACE.h"

C     !INPUT/OUTPUT PARAMETERS:
C     == Routine arguments ==
C     iMin,iMax :: Working range of x-index for applying forcing.
C     jMin,jMax :: Working range of y-index for applying forcing.
C     bi,bj     :: Current tile indices
C     kLev      :: Current vertical level index
C     myTime    :: Current time in simulation
C     myThid    :: Thread Id number
      INTEGER iMin, iMax, jMin, jMax, kLev, bi, bj
      _RL myTime
      INTEGER myThid

C     !LOCAL VARIABLES:
C     == Local variables ==
C     i,j       :: Loop counters
C     kSurface  :: index of surface layer
      INTEGER i, j
      INTEGER kSurface
CEOP

C--   Forcing term

      RETURN
      END
