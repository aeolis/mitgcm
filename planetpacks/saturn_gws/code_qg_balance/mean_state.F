#include "CPP_OPTIONS.h"

      SUBROUTINE DTH_PROFILE(nlay,pc,pe,theta,dth)

      IMPLICIT NONE
#include "SIZE.h"
#include "EEPARAMS.h"
#include "PARAMS.h"

C==== INPUT/OUTPUT ====
      INTEGER nlay
      _RL pc   (1:nlay  )
      _RL pe   (1:nlay+1)
      _RL theta(1:nlay  )
      _RL dth  (1:nlay  )

      _RL esat

C==== local variables ====
      _RL Tm  (1:nlay) 
      _RL Tc  (1:nlay)
      _RL Tw  (1:nlay)
      _RL rho (1:nlay)
      _RL esT (1:nlay)
      _RL qsT (1:nlay)
      _RL qH2O(1:nlay)
      _RL dT  (1:nlay)
  
      _RL q_deep, qsV, qmmrH2O

C     Universal constants
c     Runiv: universal gas constant
c     AN:    Avogadro's number
c     PI0:   Pi
      _RL Runiv, AN, PI0
      PARAMETER ( Runiv= 8314.0 _d 0    )
      PARAMETER ( AN   = 6.022 _d 23    )
      PARAMETER ( PI0  =3.14159265 _d 0 )

C     Atmospheric compositions
c     mH2O: molecular mass of water vapor
c     mH2:  molecular mass of H2
c     mHe:  molecluar mass of He
c     mCH4: molecular mass of methane
c     mNH3: molecular mass of Amonia
c     Runiv: universal gas constant

      _RL mH2O, mH2, mHe, mCH4, mNH3
      PARAMETER ( mH2O = 18.0 _d 0    )
      PARAMETER ( mH2  = 2. _d 0      )
      PARAMETER ( mHe  = 2. _d 0      )
      PARAMETER ( mCH4 = 16. _d 0     )
      PARAMETER ( mNH3 = 17. _d 0     )

C==== water properties ====
C     Water vapor section
c     LH_H2O: latent heat of water vapor (to solid)
c     R_H2O:  gas constant for water vapor
c     CP_H2OV: heat capacity of water vapor
c     CP_H2OS: heat capacity of water particle
c     Dcoll_H2O: coagulation coefficient for water?
c     rp_H2O:   radius of water droplets
c     rho_H2O:  density of liquid water

      _RL LH_H2O, R_H2O, CP_H2OV
      _RL Dcoll_H2O, rp_H2O,  rho_H2O, CP_H2OS
      _RL lnC_H2O, alpha_H2O, beta_H2O
      PARAMETER ( LH_H2O    = 3.1482 _d 6 )
      PARAMETER ( R_H2O     = Runiv/mH2O  )
      PARAMETER ( CP_H2OV   = 2.08 _d 3   )
      PARAMETER ( CP_H2OS   = 4.18 _d 3   )
      PARAMETER ( Dcoll_H2O = 3.11 _d -10 )
      PARAMETER ( rp_H2O    = 10.0 _d -6  )
      PARAMETER ( rho_H2O   = 1000.0 _d 0 )
      PARAMETER ( lnC_H2O   = 25.096      )
      PARAMETER ( alpha_H2O = 0.0 _d 0    )
      PARAMETER ( beta_H2O  = -17.4 _d -3 )

      INTEGER i, j, k, n_LCL, n_LNB

      _RL dz
 
      qmmrH2O = 3.0 _d -2
      q_deep  = 3.0 _d -2

C   ! #########################################################################
C   ! #                                                                       #
C   ! #         SECTION 1: TEMPERATURE PROFILE, PRESSURE, Z, H, RHO           #
C   ! #                                                                       #
C   ! #########################################################################


C! find layer thickness in pressure [Pa]
C! make them positive
      DO k=1, nlay
C        ! reference temperature prfile (T)
         Tc (k) = theta(k)*(pc(k)/atm_po)**atm_kappa
         Tm (k) = Tc(k)
         ! initialize dT, dth
         dT (k) = 0.0 _d 0
         dth(k) = 0.0 _d 0
      ENDDO

c     the reference T, P given in the model has constant dz
      dz=-log(pe(2)/pe(1))*atm_Rd*Tc(1)/gravity

      DO k=1, nlay
         rho (k) = pc(k)/(atm_Rd*Tc(k))
      ENDDO

C   ! #########################################################################
C   ! #                                                                       #
C   ! #         SECTION 2: WATER AND AMMONIA SATURATION LAPSE RATE            #
C   ! #                                                                       #
C   ! #########################################################################

C! assuming moist convection occurs deeper than 0.6bars

      DO k=1, nlay
C        ! calculate saturation vapor pressure
         esT(k)=esat(Tc(k),LH_H2O,R_H2O,lnC_H2O,alpha_H2O,beta_H2O)
C        ! get volume mixing ratio
         qsV=esT(k)/pc(k) 
C        ! get mass mixing ratio = eps*qv/(1+(eps-1)*qv)
         qsT(K)=mH2O/mH2*qsV/(1.0+(mH2O/mH2-1.0)*qsV)

c         IF (qsT(k) .GT. q_deep) qsT(k) = q_deep
c        non-leaky duct: use 0.45bars, 0.35bar is fine too
c         qH2O(k)=(1.0-tanh((0.45 _d 5-pc(k))/6. _d 4))/2.0*qmmrH2O

         Tw(k) = gravity*(1.0+LH_H2O*qsT(k)/(atm_Rd*Tc(k)))  
     &         / (atm_Cp + LH_H2O**2.0*qsT(k) 
     &         * mH2O/mH2/(atm_Rd*Tc(k)**2.0))

c         dT (k) = LH_H2O*(qH2O(k)-qsT(k))/atm_cp
c         dth(k) = dT(k)*(atm_po/pc(k))**atm_kappa
      ENDDO
      ! Find the cloud base n_LCL
      n_LCL = 1
      DO WHILE (qsT(n_LCL) .GT. qmmrH2O )
         n_LCL=n_LCL + 1
      ENDDO

      ! integrate to get the new temperature, from center to center
      DO k=n_LCL, nlay-1
         Tm(k+1) = Tm(k) - Tw(k+1)*dz
      ENDDO

      ! Find cloud top n_LNB
      DO k=n_LCL, nlay-3
         IF (  ( Tm(k-1) .gt. Tc(k-1) ) .AND. 
     &         ( Tm(k  ) .ge. Tc(k  ) ) .AND. 
     &         ( Tm(k+1) .lt. Tc(k+1) ) .AND. 
     &         ( Tm(k+2) .lt. Tc(k+2) ) )     
     &         n_LNB = k
      ENDDO

      DO k=n_LCL, n_LNB
         dT(k) = Tm(k) - Tc(k)
         dth(k) = dT(k)*(atm_po/pc(k))**atm_kappa
      ENDDO

      RETURN
      END


C   ! #########################################################################
C   ! #                                                                       #
C   ! #         SECTION: REFERNECE WIND PROFILE, GRADIENT AND CURVATURE       #
C   ! #                                                                       #
C   ! #########################################################################

      SUBROUTINE UZ_PROFILE(uz,vz,uGWS, myThid)

#include "SIZE.h"
#include "EEPARAMS.h"
#include "PARAMS.h"
#include "GRID.h"

      INTEGER myThid

C!==== input/output ====
      _RL uz(1-OLx:sNx+OLx,1-OLy:sNy+OLy,Nr,nSx,nSy)
      _RL vz(1-OLx:sNx+OLx,1-OLy:sNy+OLy,Nr,nSx,nSy)
      _RL uGWS
C!==== local variables ====
      _RL zc (1:Nr  )
      _RL ze (1:Nr+1)
      _RL dze(1:Nr  )
      _RL dzc(1:Nr  )

      _RL ca, cb, czg, czt, uz_scalar,dz,MaPI

      INTEGER bi,bj, n_tropo, i,j,k

      MaPI = acos(-1.)

      CALL calc_z_pressure(ze,zc,dze,dzc,rF,rC,tRef,Nr,
     &                     atm_cp,atm_kappa,atm_po,gravity)


      ca = MaPI/15. _d 0
      cb = 40. _d 0/15. _d 0 * MaPI  
      czg = -100. _d 3  ! altitude of peak wind speed (in meters)

      n_tropo = 0
C     find tropopause p_tropo=0.1bar
      DO k=1, Nr
         IF (rC(k) .gt. 1. _d 4) n_tropo=n_tropo+1
      ENDDO

      DO bj = myByLo(myThid), myByHi(myThid)
      DO bi = myBxLo(myThid), myBxHi(myThid)

      DO i=1, sNx
      DO j=1, sNy
      DO k=1, Nr
         uz(i,j,k,bi,bj) = 
C          Latitudinal variation, gives peak zonal wind speed of 25m/s at 40 deg
     &     (-cos(ca*yC(i,j,bi,bj) - cb)**2.*150. _d 0 + 120. _d 0)
     &     * (1.- ( tanh( (zc(k)-zc(n_tropo)-czg)/1.e5 ) )/5.)
         vz(i,j,k,bi,bj) = 0.0 _d 0
      ENDDO
      ENDDO
      ENDDO

      ENDDO
      ENDDO

C     assuming GWS moving with velocity at the wind speed of the lowest model 
C     layer at 40 deg north
      uGWS = (-cos(ca*40. _d 0 - cb)**2.*150. _d 0 + 120. _d 0)
     &       * (1.- ( tanh( (zc(1)-zc(n_tropo)-czg)/1.e5 ) )/5.)

      RETURN
      END 

      FUNCTION esat(T,LH,RV,lnC,alpha,beta)

      IMPLICIT NONE

      _RL esat
C!== input ==
      _RL T
      _RL LH,RV,lnC,alpha,beta

C!== local variables ==
      _RL LHg, RVg

C!    convert J/kg to J/g
      LHg=LH/1. _d 3
      RVg=RV/1. _d 3

      esat=exp(lnC+1.0/RVg*(-LHg/T+alpha*log(T)+beta/2.0*T))
C!    convert bar to Pa
      esat=esat*1. _d 5

      RETURN
      END 
