C define the reference U, V profile
      COMMON /Vel_Ref/ uRef, vRef, uGWS

      _RL uRef(1-OLx:sNx+OLx,1-OLy:sNy+OLy,Nr,nSx,nSy)
      _RL vRef(1-OLx:sNx+OLx,1-OLy:sNy+OLy,Nr,nSx,nSy)

      _RL xGWS, yGWS, uGWS

      PARAMETER (xGWS = 90.0)
      PARAMETER (yGWS = 40.0)
