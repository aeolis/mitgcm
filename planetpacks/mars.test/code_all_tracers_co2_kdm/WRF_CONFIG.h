C     define WRF configurations

      COMMON /WRF_CONFIG/
     &       Path2Data,
     &       cAdjFreqWRF, radFreq, 
     &       prejulian,
     &       du_physics,
     &       ISFTCFLX,
     &       ISFFLX, IFSNOW,
     &       iz0tlnd,
     &       nlte_physics,
     &       radiation, 
     &       optical_depth_multiplier,
     &       remove_extra_vels, 
     &       diurnavg, 
     &       isothermal_top, 
     &       tau_damp,
     &       bldt,
     &       ra_mars_kdm_double_gauss, 
     &       ra_mars_kdm_simpledust, 
     &       simpledust_omega,
     &       simpledust_asymmetry,
     &       number_of_aerosols,
     &       limb_data_rec,
     &       uv_physics,
     &       sw_correct,
     &       ice_ti_feedback,
     &       perm_co2_cap,
     &       useShap_damping,
     &       correct_pbl_tracer_tend,
     &       co2_convection,
     &       co2_physics,
     &       rco2_eff,
     &       qext_co2_eff,
     &       mars_co2_cycle,
     &       wrf_rad_scheme,
     &       wrf_surface_physics,
     &       wrf_pbl_physics,
     &       mp_physics,
     &       grid_factor,
     &       no_min_ust,
     &       disable_water_regolith,
     &       do_porosity, prescribed_ice,
     &       mars_water_cycle,
     &       mars_dust_cycle,
     &       titan_methane_cycle,
     &       ddpp_co2_eff, ccn,
     &       rho_dust, ipcp, sspp,
     &       ddpp, ddpp2,
     &       mumars, 
     &       nswalpha1, nswalpha2,
     &       dalpha1, dalpha2, 
     &       nsws, ddev, 
     &       reset_surfdust1, reset_surfdust2,
     &       initsurfdust1, initsurfdust2,
     &       do_deep_dust_mixing,         
     &       k_overshoot, k_pbl_offset,      
     &       detrain_dust,             
     &       dust_detrain_frac, inject_layer,
     &       nh_h2oice_albedo,
     &       nh_h2oice_emiss ,
     &       sh_h2oice_albedo,
     &       sh_h2oice_emiss ,
     &       nh_co2ice_albedo,
     &       nh_co2ice_emiss ,
     &       sh_co2ice_albedo,
     &       sh_co2ice_emiss ,
     &       co2ice_threshold,
     &       h2oice_threshold,
     &       ground_ice_cond_north,
     &       ground_ice_cond_south,
     &       ground_ice_den_constant,
     &       ground_ice_cap_constant

C === Mars physics configuration ===
     
      CHARACTER*256 Path2Data   

C Mars dust physics selection
C du_physics  = 41 : conrath
c               42 : twoparticle
c               43 : mcdmgs 
c               44 : mcdviking
c               45 : damcdmgs
c               46 : oxforddust
c               47 : mcsdust
c               48 : mcdmgsx2
c               49 : tes_limb_avg
      INTEGER du_physics
      INTEGER ISFTCFLX
      INTEGER ISFFLX
      INTEGER IFSNOW

      INTEGER iz0tlnd

      LOGICAL radiation   
      LOGICAL remove_extra_vels
C mars KDM stuff, including single or double gaussian quadrature
      LOGICAL ra_mars_kdm_double_gauss
      LOGICAL ra_mars_kdm_simpledust
      LOGICAL sw_correct
      INTEGER number_of_aerosols
c     limb data record ID: 1=TES climatology 
c                          2=MCS 
c                          3=MY24 
c                          4=MY25 
c                          5=Y26 
c                          6=MY27
      INTEGER limb_data_rec
      REAL*8 simpledust_omega
      REAL*8 simpledust_asymmetry

C enable non-LTE physics for Mars
      INTEGER nlte_physics
C enabel UV phsyics for Mars
      INTEGER uv_physics
C multiplayer for dust opacity (temp fix)
      REAL*8 optical_depth_multiplier

C enable or disable Mars CO2 cycle
c mars_co2_cycle = 0 : original WRF simple co2 cycle
c                = 1 : single moment co2 physics
c                = 2 : two moment co2 physics
      INTEGER mars_co2_cycle

C enable or disable permanent CO2 cap
      LOGICAL perm_co2_cap

C enable strong shapiro damping for to three layers
      LOGICAL useShap_damping

C Correct passive tracer tendencies in PBL mixing
      LOGICAL correct_pbl_tracer_tend

C use convective diffusion due to CO2 moist convection
      LOGICAL co2_convection 

C use radiative transfer of CO2 clouds

      LOGICAL co2_physics

C co2 ice effective radius (one case only)
C r_eff=10um is Hayne et al., (2012)
C in mesosphere r_eff=1.5um from MGS-TES and Mini-TES data
      REAL*8 rco2_eff
C co2 ice extinction efficiency at rco2_eff
C qext_co2_eff is obtained at rco2_eff=10um and reference wavelenght=1um
      REAL*8 qext_co2_eff

C disable regolith if TRUE
      LOGICAL disable_water_regolith

C use prescribed surface H2O ice or not
      LOGICAL prescribed_ice

C use surface porosity or not
      LOGICAL do_porosity

C enable ice-thermal inertia feedback
      LOGICAL ice_ti_feedback

C Mars CO2 configuration
c     diameter of ice particle
      REAL*8 ddpp_co2_eff
c     Assumed cloud condensation nucleation (kg^-1)
      REAL*8 ccn
C Mars dust configuration
c     density of dust (kg/m^3)
      REAL*8 rho_dust
c     diameter of first dust particles (m)
      REAL*8 ddpp
c     diameter of second dust particles (m)
      REAL*8 ddpp2
c     Interparticle cohesion
      REAL*8 ipcp
c     Saltating particle diameter (m)
      REAL*8 sspp
c     dynamic viscosity of Martian atmosphere?
      REAL*8 mumars
c     Near-surface wind stress lifting rate parameter for psize 1
      REAL*8 nswalpha1
c     Near-surface wind stress lifting rate parameter for psize 2
      REAL*8 nswalpha2
c     Dust devil lifting rate parameter for psize 1
      REAL*8 dalpha1
c     Dust devil lifting rate parameter for psize 2
      REAL*8 dalpha2
c     Near-surface wind stress lifting
      LOGICAL nsws
c     Dust devil lifting
      LOGICAL ddev
c     If true, reset surfdust1 at start
      LOGICAL reset_surfdust1
c     If true, reset surfdust2 at start
      LOGICAL reset_surfdust2
c     Amount to reset surfdust1 to (kg/m^2)
      REAL*8 initsurfdust1
c     Amount to reset surfdust2 to (kg/m^2)
      REAL*8 initsurfdust2
c     Flag to tell the PBL scheme to do deep mixing at injection
      LOGICAL do_deep_dust_mixing
c     Number of levels by which dust deep mixing overshoots PBL top
      INTEGER k_overshoot          
c     Number of levels above diagnosed PBL top to place PBL top for dust inj
      INTEGER k_pbl_offset      
c     Flag that switches on sub-PBL injection layer detrainment of dust
      LOGICAL detrain_dust           
c     Fraction of dust lifted in PBL plumes that detrains below PBL top layer
      REAL*8 dust_detrain_frac
c     Flag to tell PBL to inject dust in layer at PBL top +/- k_overshoot
      LOGICAL inject_layer
c     The following parameters are surface properties relating to surface ice
      REAL*8 nh_h2oice_albedo
      REAL*8 nh_h2oice_emiss 
      REAL*8 sh_h2oice_albedo
      REAL*8 sh_h2oice_emiss 
      REAL*8 nh_co2ice_albedo
      REAL*8 nh_co2ice_emiss 
      REAL*8 sh_co2ice_albedo
      REAL*8 sh_co2ice_emiss 
      REAL*8 co2ice_threshold
      REAL*8 h2oice_threshold

      REAL*8 ground_ice_cond_north
      REAL*8 ground_ice_cond_south
      REAL*8 ground_ice_den_constant
      REAL*8 ground_ice_cap_constant

C === End of Mars configuration


C === Universal configuration ===
      REAL*8 cAdjFreqWRF
      REAL*8 radFreq
      REAL*8 tau_damp(kte)
c     wrf_rad_scheme = 0 : no radiative transfer scheme
c                    = 1 : mars WBM scheme
c                    = 2 : mars KDM or Titan KDM scheme
      INTEGER wrf_rad_scheme
      LOGICAL mars_water_cycle
      LOGICAL mars_dust_cycle
      LOGICAL titan_methane_cycle
      LOGICAL wrf_surface_physics
      LOGICAL wrf_pbl_physics
c     if sfclay=1: FALSE=minimum ust=0.1, TRUE=no minimum ust imposed
      LOGICAL no_min_ust
c     select microphysics
c     mp_physics==0:  Passive tracer
c                 1:  kesslerscheme (not avail)
c                 10: morr_two_moment (active tracers: qv,qc,qr,qi,qs,qg)
c                 41: marsco2 (active tracers: qv;scalar:qst01,qst02,qst_a,qst_b,qst_c,qst_d)
c                 42: marssimpwater (active tracers: qv,qi;scalar:qst01,qst02,qst_a,qst_b,qst_c,qst_d,qst_v,qst_i)
c                 43: marsdustwater (active tracers: qv,qi;scalar:qst01,qst02,qst_a,qst_b,qst_c,qst_d,qst_v,qst_i)
c                 44: marschem (active tracers: qv,qi;scalar:qst01,qst02,qst_a,qst_b,qst_c,qst_d,qst_v,qst_i)
c                 45: md1_vartaut (active tracers: qst01)
c                 46: md1_prescinj (active tracers: qst01)
c                 47: mars dust with 1 particle size (active tracers: qst01)
c                 48: mars dust with 2 particle sizes (active tracers: qst01, qst02)
c                 66: titan active methane cycle
c                 60: titan passive methane cycle
      INTEGER mp_physics
c     grid factor to define soil layers
      REAL*8 grid_factor
c prejulian is the Julian data before restart
      REAL*8 prejulian
c boundary layer time step
      REAL*8 bldt

      LOGICAL diurnavg
      LOGICAL isothermal_top

      COMMON /WRF_RESTART/
     &       wrf_mdsio_read_pickup 

      LOGICAL wrf_mdsio_read_pickup

      COMMON /WRF_SURFDATA/
     &       albFile, inertiaFile, 
     &       roughnessFile, sstFile

      CHARACTER*(MAX_LEN_FNAM) albFile
      CHARACTER*(MAX_LEN_FNAM) inertiaFile
      CHARACTER*(MAX_LEN_FNAM) roughnessFile
      CHARACTER*(MAX_LEN_FNAM) sstFile

C === 

      COMMON /MARS_KDMDATA/
     &       k_coeff_file,
     &       dust_params_file,
     &       ice_params_file

      CHARACTER*(256) k_coeff_file
      CHARACTER*(256) dust_params_file
      CHARACTER*(256) ice_params_file

