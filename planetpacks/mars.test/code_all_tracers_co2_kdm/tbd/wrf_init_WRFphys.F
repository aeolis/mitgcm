C this module is to set the input arrays in radiative transfer
C modules, surface scheme and planet boundary layer. The real 
C inputs will be replaced during th productive run
C Use lower case of Module to identify the home-brewd code

C definition  of some variables, 
C Registry.EM_PLANET: RTHRATEN GSW 
C pi3D = (p/po)^kappa
C dz8w        dz between full levels (m)
#include "CPP_OPTIONS.h"
#include "planetWRF_OPTIONS.h"
   
      SUBROUTINE init_WRFphys( myThid )

      USE module_sf_sfclay,    ONLY : sfclayinit
      USE module_ra_valverde,  ONLY : valverdeinit
      USE module_ra_mars_kdm,  ONLY : marskdminit
      USE module_ra_mars_uv,   ONLY : uvinit
      USE module_ra_mars_common, ONLY :  dust_tes_limb_init
      USE module_state_description

      IMPLICIT NONE

C  === Global Variables ====
#include "SIZE.h"
#include "WRF_SIZE.h"
#include "EEPARAMS.h"
#include "PARAMS.h"
#include "WRF_PARAMS.h"
#include "GRID.h"
#include "SURFACE.h"
#include "DYNVARS.h"
#ifdef WRF_ENABLE_TRACERS
#include "PTRACERS_SIZE.h"
#include "PTRACERS_PARAMS.h"
#include "PTRACERS_FIELDS.h"
#endif /* WRF_ENABLE_TRACERS */
#include "WRF_PHYS.h"
#include "WRF_CONFIG.h"

C     !INPUT/OUTPUT PARAMETERS:
C     == Routine arguments ==
C     myThid :: Number of this instance of INI_THETA
      INTEGER myThid

C     !LOCAL VARIABLES:
C     == Local variables ==
C     bi,bj  :: Tile indices
C     I,J,K  :: Loop counters
      INTEGER bi, bj
      INTEGER I, J, K
      _RL tempphys(ims:ime,jms:jme,Nsx,Nsy)
      _RL topoH   (ims:ime,jms:jme,Nsx,Nsy)

C     Initialize soil layer thickness
#ifdef WRF_MARS
      DATA DZS /0.30e-2, 0.50e-2,  0.75e-2,  1.10e-2,  1.50e-2,
     &          2.00e-2, 4.00e-2,  8.00e-2,  15.00e-2, 25.00e-2,
     &          40.00e-2, 80.00e-2/
#else /* WRF_TITAN */
      DATA DZS /1.20e-2, 2.00e-2,  3.00e-2,  4.40e-2,  6.00e-2,
     &          8.00e-2, 16.00e-2, 32.00e-2, 60.00e-2, 100.00e-2,
     &          160.00e-2, 320.00e-2/
#endif
C-- set initial variables for WRF_CONFIG
      Path2Data = './'
      radiation = .FALSE.
      remove_extra_vels = .FALSE.
      ISFTCFLX = 0
      ISFFLX = 1
      iz0tlnd = 0
c === mars radtran configuration ===
c du_physics=43 for mcd_mgs, 0 for no dust
      du_physics = 0
c mars radiation config 
      sw_correct = .FALSE.
      ra_mars_kdm_double_gauss = .FALSE.
      ra_mars_kdm_simpledust = .FALSE.
      k_coeff_file = 
     &      trim(Path2Data)//'/Data/radiation/co2_h2o.dat'
      dust_params_file = 
     &      trim(Path2Data)//'/Data/radiation/mischna_qext.dat'
      nlte_physics = 41
      uv_physics   = 41
      optical_depth_multiplier = 1.
      mars_co2_cycle = .TRUE.
      perm_co2_cap = .TRUE.
      disable_water_regolith=.TRUE.
      ice_ti_feedback = .FALSE.
c mars dust config
      rho_dust = 2500.0
      ddpp = 2.e-6
      ddpp2 = 2.e-6
      ipcp = 5.e-7
      sspp = 9.e-5
      mumars = 1.e-5
      nswalpha1 = 1.e-7
      nswalpha2 = 1.e-7
      dalpha1 = 1.e-8
      dalpha2 = 1.e-8
      nsws = .FALSE.
      ddev = .FALSE.
      reset_surfdust1 = .FALSE.
      reset_surfdust2 = .FALSE.
      initsurfdust1 = 1.0
      initsurfdust2 = 1.0
c === universal configurations ===
      diurnavg = .FALSE.
      DO K=1,kte
c        tau_damp is a damping time scale
c        in planet days.
         tau_damp(K)=0.0
      ENDDO
      bldt = 0.
      useShap_damping = .FALSE.
      cAdjFreqWRF = 0.0
      radFreq = deltaT
      prejulian = 0.0
      isothermal_top = .FALSE.
      mars_water_cycle= .FALSE.
      mars_dust_cycle = .FALSE.
c     radiative transfer scheme selector
#ifdef WRF_MARS
      wrf_rad_scheme = 1
#elif defined WRF_TITAN
      wrf_rad_scheme = 2
#endif
c     pbl mixing
      wrf_pbl_physics = .TRUE.
c     surface scheme selector
      wrf_surface_physics = .TRUE.
      wrf_sf_scheme = 1
c     microphysics selector (choose number of tracers)
c     default is 1 passive tracer
      mp_physics = 0
c     impose minimum u* or not 
      no_min_ust = .FALSE.      
c     surface properties
      albFile = ' '
      inertiaFile = ' '
      roughnessFile = ' '
      sstFile = ' '

c Initialize the WRF configurations
      CALL WRF_READPARMS( myThid )
      if (wrf_surface_physics)
     &   CALL sfclayinit(myThid)

#ifdef WRF_TITAN
      mars_co2_cycle = .FALSE.
      perm_co2_cap=.FALSE.
      du_physics = 0
      nlte_physics = 0
#endif

C-- Overide the number of tracers for consistency
#ifdef WRF_ENABLE_TRACERS_DIS
      IF (.not. (mars_water_cycle 
     &     .or.  mars_dust_cycle
     &     .or.  titan_methane_cycle) )
     &   PTRACERS_numInUse=1
#else
      mars_water_cycle = .FALSE.
      mars_dust_cycle  = .FALSE.
      titan_methane_cycle = .FALSE.
#endif /* WRF_ENABLE_TRACERS */

#ifdef WRF_MARS
C-- First check if the vertical coordinate allows 
C-- CO2 condensation (must use nonlinfreesurf)
      IF (mars_co2_cycle) THEN 
        IF (nonlinFreeSurf.le.0) THEN
         print *,'CO2 CYCLE ENABLED: '
         print *,'MUST USE NONLIN_FRSURF, DEF IN CPP_OPTIONS.h'
         stop
        ENDIF
      ENDIF
c initialize the non-LTE cooling (Valverde)
      IF (uv_physics > 0) CALL uvinit(Path2Data)
      IF (du_physics == tes_limb_avg) 
     &   CALL dust_tes_limb_init(Path2Data)
      CALL VALVERDEINIT(Path2Data,myThid)
c Initialize KDM radtran for Mars
      IF (wrf_rad_scheme .EQ. 2)
     &   CALL marskdminit(.false., 
     &        k_coeff_file, dust_params_file, 
     &        ra_mars_kdm_double_gauss, 
     &        ra_mars_kdm_simpledust, myThid) 
#endif /* WRF_MARS */

c Initialize PIA terms for Titan radtran
#ifdef WRF_TITAN
      IF (wrf_rad_scheme .EQ. 2) 
     &   CALL mckay_kdm_init(myThid)
#endif /* WRF_TITAN */
      
      IF (albFile .EQ. ' ') THEN
       DO bj = myByLo(myThid), myByHi(myThid)
        DO bi = myBxLo(myThid), myBxHi(myThid)
         DO J=jts,jte
          DO I=its,ite
#if defined WRF_MARS
           ALBEDO(I,J,bi,bj)   = 0.32 _d 0
#elif defined WRF_TITAN
           ALBEDO(I,J,bi,bj)   = 0.32 _d 0
#else
           ALBEDO(I,J,bi,bj)   = 0.32 _d 0
#endif
          ENDDO
         ENDDO
        ENDDO
       ENDDO
      ELSE
         CALL READ_REC_XY_RS(albFile,tempphys,1,0,myThid )
         DO bj = myByLo(myThid), myByHi(myThid)
          DO bi = myBxLo(myThid), myBxHi(myThid)
           DO J=jts,jte
            DO I=its,ite
             ALBEDO(I,J,bi,bj) = tempphys(I,J,bi,bj)
            ENDDO
           ENDDO
          ENDDO
         ENDDO

      ENDIF
      
      IF (inertiaFile .EQ. ' ') THEN
       DO bj = myByLo(myThid), myByHi(myThid)
        DO bi = myBxLo(myThid), myBxHi(myThid)
         DO J=jts,jte
          DO I=its,ite
#if defined WRF_MARS
           THC(I,J,bi,bj)   = 215.0 _d 0
#elif defined WRF_TITAN
           THC(I,J,bi,bj)   = 335.0 _d 0
#else
           THC(I,J,bi,bj)   = 215.0 _d 0
#endif
          ENDDO
         ENDDO
        ENDDO
       ENDDO
      ELSE
         CALL READ_REC_XY_RS(inertiaFile,tempphys,1,0,myThid )
         DO bj = myByLo(myThid), myByHi(myThid)
          DO bi = myBxLo(myThid), myBxHi(myThid)
           DO J=jts,jte
            DO I=its,ite
             THC(I,J,bi,bj)  = tempphys(I,J,bi,bj)
            ENDDO
           ENDDO
          ENDDO
         ENDDO

      ENDIF

      IF (roughnessFile .EQ. ' ') THEN
       DO bj = myByLo(myThid), myByHi(myThid)
        DO bi = myBxLo(myThid), myBxHi(myThid)
         DO J=jts,jte
          DO I=its,ite
#if defined WRF_MARS
           ZNT(I,J,bi,bj)   = 0.01 
#elif defined WRF_TITAN
           ZNT(I,J,bi,bj)   = 0.001 
#else
           ZNT(I,J,bi,bj)   = 0.01
#endif
          ENDDO
         ENDDO
        ENDDO
       ENDDO
      ELSE
         CALL READ_REC_XY_RS(roughnessFile,tempphys,1,0,myThid )
         DO bj = myByLo(myThid), myByHi(myThid)
          DO bi = myBxLo(myThid), myBxHi(myThid)
           DO J=jts,jte
            DO I=its,ite
             ZNT(I,J,bi,bj) = tempphys(I,J,bi,bj)
            ENDDO
           ENDDO
          ENDDO
         ENDDO

      ENDIF

      IF (sstFile .EQ. ' ') THEN
       DO bj = myByLo(myThid), myByHi(myThid)
        DO bi = myBxLo(myThid), myBxHi(myThid)
         DO J=jts,jte
          DO I=its,ite
#if defined WRF_MARS
           TSK(I,J,bi,bj)   = thphy(i,j,1,bi,bj)*
     &        ((Ro_surf(i,j,bi,bj)+etaN(i,j,bi,bj))/atm_Po)**atm_kappa
#elif defined WRF_TITAN
           TSK(I,J,bi,bj) = 93.0 _d 0
#else
           TSK(I,J,bi,bj)   = thphy(i,j,1,bi,bj)*
     &        ((Ro_surf(i,j,bi,bj)+etaN(i,j,bi,bj))/atm_Po)**atm_kappa
#endif
          ENDDO
         ENDDO
        ENDDO
       ENDDO
      ELSE
         CALL READ_REC_XY_RS(sstFile,tempphys,1,0,myThid )
         DO bj = myByLo(myThid), myByHi(myThid)
          DO bi = myBxLo(myThid), myBxHi(myThid)
           DO J=jts,jte
            DO I=its,ite
             TSK(I,J,bi,bj) = tempphys(I,J,bi,bj)
            ENDDO
           ENDDO
          ENDDO
         ENDDO

      ENDIF

      DO bj = myByLo(myThid), myByHi(myThid)
       DO bi = myBxLo(myThid), myBxHi(myThid)
        DO j=1-Oly,sNy+Oly
         DO i=1-Olx,sNx+Olx
          topoH(i,j,bi,bj) = topoZ(i,j,bi,bj)
         ENDDO
        ENDDO
       ENDDO
      ENDDO

      _EXCH_XY_RL(topoH, myThid )   
 
      DO bj = myByLo(myThid), myByHi(myThid)
       DO bi = myBxLo(myThid), myBxHi(myThid)

C--   Initialize the topographic slopes
       CALL WRF_CALC_SLOPES (
     &          topoH    (ims:ime, jms:jme, bi,bj),
     &          ANGSLOPE (ims:ime, jms:jme, bi,bj),
     &          AZMSLOPE (ims:ime, jms:jme, bi,bj),
     &          yC       (ims:ime, jms:jme, bi,bj),
     &          xC       (ims:ime, jms:jme, bi,bj),
     &          dxC      (ims:ime, jms:jme, bi,bj),
     &          dyC      (ims:ime, jms:jme, bi,bj),
     &          angleCosC(ims:ime, jms:jme, bi,bj),
     &          angleSinC(ims:ime, jms:jme, bi,bj),
     &          ims,ime,jms,jme,kms,kme,
     &          its,ite,jts,jte,kts,kte )

        DO J=jts,jte
         DO I=its,ite
          GLW(I,J,bi,bj)      = 0.0
          GSW(I,J,bi,bj)      = 0.0
          HA(I,J,bi,bj)       = 0.0
c          AZMSLOPE(I,J,bi,bj) = 0.0
          sza0(I,J,bi,bj)     = 0.0
          sunfrac(I,J,bi,bj)  = 0.0
#ifdef WRF_MARS
C--       other variales related to tracers
          co2ice(I,J,bi,bj)   = 0.0
C--       initialize polar water ice cap that has total 
C--       mass of 1.4x10^16g
          if (yC(I,J,bi,bj).ge.80.) then
              h2oice(I,J,bi,bj)   = 5500. _d 8
          else
              h2oice(I,J,bi,bj)   = 0. _d 0
          endif
          co2sublimate(I,J,bi,bj) = 0.0
          SNOWC(I,J,bi,bj)    = 0.0
          ground_ice_depth(I,J,bi,bj) = 0.0
          ground_ice_percentage(I,J,bi,bj) = 0.0
#endif /* WRF_MARS */
C--       
          XLAND(I,J,bi,bj)= 1.    ! replace this with masks later 
          RNET_2D(I,J,bi,bj)  = 0.0
          HFX(I,J,bi,bj)  = 0.0 
          QFX(I,J,bi,bj)  = 0.0
          HOL(I,J,bi,bj)  = 0.0
          UST(I,J,bi,bj)  = 0.0
          PBL(I,J,bi,bj)  = 0.0
          PSIM(I,J,bi,bj) = 0.0
          PSIH(I,J,bi,bj) = 0.0
          WSPD(I,J,bi,bj) = 1.0 _d -8
          GZ1OZ0(I,J,bi,bj) = 0.0
          BR(I,J,bi,bj)   = 0.5 
          PSFC(I,J,bi,bj) = Ro_surf(i,j,bi,bj)+etaN(i,j,bi,bj) !Ro_SeaLevel
          ZOL(I,J,bi,bj)  = 0.0
          KPBL2D(I,J,bi,bj) = 1
          REGIME(I,J,bi,bj) = 1.0  ! assuming stable everywhere
C--       
          FLHC  (I,J,bi,bj) = 0.0
          FLQC  (I,J,bi,bj) = 0.0
          LH    (I,J,bi,bj) = 0.0
          GRDFLX(I,J,bi,bj) = 0.0

          MOL   (I,J,bi,bj) = 0.0
          RMOL  (I,J,bi,bj) = 0.0
#if defined WRF_MARS
          EMISS (I,J,bi,bj) = 1.0
#elif defined  WRF_TITAN
          EMISS (I,J,bi,bj) = 0.86
#else
          EMISS (I,J,bi,bj) = 1.0
#endif
          MAVAIL(I,J,bi,bj) = 0.0
          TMN   (I,J,bi,bj) = 230.0 ! dummy variable
          QSFC  (I,J,bi,bj) = 0.0
          mup   (I,J,bi,bj) = 0.0
   
         ENDDO
        ENDDO
       ENDDO
      ENDDO            

C-- initialize subsurface temperature and skin temperature  
C-- adopt the method from older way (WRF module_setup_subsurface.F)
      DO bj = myByLo(myThid), myByHi(myThid)
       DO bi = myBxLo(myThid), myBxHi(myThid)
        DO J=jts,jte
         DO I=its,ite
          DO K=1,num_soil_layers
             TSLB(I,J,K,bi,bj) = TSK(I,J,bi,bj)
#if defined WRF_MARS
             CAPG(I,J,K,bi,bj) = 837.2*RHO_GROUND
#elif defined WRF_TITAN
             CAPG(I,J,K,bi,bj) = 1400.0*RHO_GROUND
#else
             CAPG(I,J,K,bi,bj) = 837.2*RHO_GROUND
#endif
          ENDDO
         ENDDO
        ENDDO
       ENDDO
      ENDDO

C--   initialize soil layers
      ZS(1) = 0.5*DZS(1)
      DO K = 2, num_soil_layers
         ZS(K) = ZS(K-1) + (DZS(K-1)+DZS(K))/2.0d0
      ENDDO

C--   set/initialize soil properties
      DO bj = myByLo(myThid), myByHi(myThid)
       DO bi = myBxLo(myThid), myBxHi(myThid)
        DO J=jts,jte
         DO I=its,ite
          qflux(I,J,bi,bj) = 0.0
          DO K=1,num_soil_layers
             porosity(I,J,K,bi,bj) = 0.5*EXP(-zs(K)/20.)
#ifdef WRF_MARS
             soil_den(I,J,K,bi,bj) = 3.0E3 ! use 3.0E3 instead of 2.6E3 to match default
     &                          *(1.0-porosity(I,J,K,bi,bj))
             ss_vapor(I,J,K,bi,bj) = 0.0
             ss_ice  (I,J,K,bi,bj) = MIN(10.,0.9
     &                          *porosity(I,J,K,bi,bj)*917.) !c-mm Put 100 kg/m3 in layer or 90% of sat. value
             ss_adsorbate(I,J,K,bi,bj) = 0.0
#else
             soil_den(I,J,K,bi,bj) = RHO_GROUND
#endif /* WRF_MARS */
          ENDDO
         ENDDO
        ENDDO
       ENDDO
      ENDDO 

      _EXCH_XY_RL(ANGSLOPE, myThid )
      _EXCH_XY_RL(AZMSLOPE, myThid )

      RETURN
      END

   
