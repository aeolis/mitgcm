C $Header: wrf_dyn_tendency_apply.F
C $Name:  $
#include "CPP_OPTIONS.h"
#include "planetWRF_OPTIONS.h"

CBOP
C     !ROUTINE: WRF_TENDENCY_APPLY_U
C     !INTERFACE:
      SUBROUTINE WRF_TENDENCY_APPLY_U(
     I           iMin,iMax, jMin,jMax, bi,bj, kLev,
     I           myTime, myThid )
C     !DESCRIPTION: \bv
C     *==========================================================*
C     | S/R WRF_TENDENCY_APPLY_U
C     | o Contains problem specific forcing for zonal velocity.
C     *==========================================================*
C     | Adds terms to gU for forcing by external sources
C     | e.g. wind stress, bottom friction etc ...
C     *==========================================================*
C     \ev

C     !USES:
      IMPLICIT NONE
C     == Global data ==
#include "SIZE.h"
#include "WRF_SIZE.h"
#include "EEPARAMS.h"
#include "PARAMS.h"
#include "WRF_PARAMS.h"
#include "GRID.h"
#include "DYNVARS.h"
#include "FFIELDS.h"
#include "SURFACE.h"
#ifdef WRF_ENABLE_TRACERS
#include "PTRACERS_SIZE.h"
#endif /* WRF_ENABLE_TRACERS */
#include "WRF_PHYS.h"
#include "WRF_CONFIG.h"

C     !INPUT/OUTPUT PARAMETERS:
C     == Routine arguments ==
C     iMin,iMax :: Working range of x-index for applying forcing.
C     jMin,jMax :: Working range of y-index for applying forcing.
C     bi,bj     :: Current tile indices
C     kLev      :: Current vertical level index
C     myTime    :: Current time in simulation
C     myThid    :: Thread Id number
      INTEGER iMin, iMax, jMin, jMax, kLev, bi, bj
      _RL myTime
      INTEGER myThid

C     !LOCAL VARIABLES:
C     == Local variables ==
C     i,j       :: Loop counters
C     kSurface  :: index of surface layer
      INTEGER i, j
      INTEGER kSurface
      _RL uzonmean(1-OLy:sNy+OLy)
      _RL tauc_recip

C--   Forcing term

      IF (tau_damp(kLev) .gt. 0.) THEN
        tauc_recip = 1.0 _d 0/(tau_damp(kLev)*86400.0 _d 0*P2SI)
      ELSE
        tauc_recip = 0.0 _d 0
      ENDIF

C--   calculate zonal mean zonal wind
      IF (tau_damp(kLev) .gt. 0. and. usingSphericalPolarGrid) THEN
         DO j=jMin,jMax
          uzonmean(j)=0.0 _d 0
          DO i=0,sNx+1
             uzonmean(j)=uzonmean(j)+uVel(i,j,kLev,bi,bj)
          ENDDO
          uzonmean(j)=uzonmean(j)/dble(iMax-iMin+1)
         ENDDO
      ENDIF

      DO j=jMin,jMax
       DO i=iMin,iMax
        gU(i,j,kLev,bi,bj) = gU(i,j,kLev,bi,bj) 
c       Add PBL tendency for U
     & + gublphy(i,j,kLev,bi,bj) * maskW(i,j,kLev,bi,bj) 
c       Add Rayleigh drag for U
     & - tauc_recip * maskW(i,j,kLev,bi,bj)
     & * uVel(i,j,kLev,bi,bj)
C     & * (uVel(i,j,kLev,bi,bj) - uzonmean(j) )
       ENDDO
      ENDDO

      RETURN
      END

C---+----1----+----2----+----3----+----4----+----5----+----6----+----7-|--+----|
CBOP
C     !ROUTINE: WRF_TENDENCY_APPLY_V
C     !INTERFACE:
      SUBROUTINE WRF_TENDENCY_APPLY_V(
     I           iMin,iMax, jMin,jMax, bi,bj, kLev,
     I           myTime, myThid )
C     !DESCRIPTION: \bv
C     *==========================================================*
C     | S/R WRF_TENDENCY_APPLY_V
C     | o Contains problem specific forcing for merid velocity.
C     *==========================================================*
C     | Adds terms to gV for forcing by external sources
C     | e.g. wind stress, bottom friction etc ...
C     *==========================================================*
C     \ev

C     !USES:
      IMPLICIT NONE
C     == Global data ==
#include "SIZE.h"
#include "WRF_SIZE.h"
#include "EEPARAMS.h"
#include "PARAMS.h"
#include "WRF_PARAMS.h"
#include "GRID.h"
#include "DYNVARS.h"
#include "FFIELDS.h"
#include "SURFACE.h"
#ifdef WRF_ENABLE_TRACERS
#include "PTRACERS_SIZE.h"
#endif /* WRF_ENABLE_TRACERS */
#include "WRF_PHYS.h"
#include "WRF_CONFIG.h"

C     !INPUT/OUTPUT PARAMETERS:
C     == Routine arguments ==
C     iMin,iMax :: Working range of x-index for applying forcing.
C     jMin,jMax :: Working range of y-index for applying forcing.
C     bi,bj     :: Current tile indices
C     kLev      :: Current vertical level index
C     myTime    :: Current time in simulation
C     myThid    :: Thread Id number
      INTEGER iMin, iMax, jMin, jMax, kLev, bi, bj
      _RL myTime
      INTEGER myThid

C     !LOCAL VARIABLES:
C     == Local variables ==
C     i,j       :: Loop counters
C     kSurface  :: index of surface layer
      INTEGER i, j
      INTEGER kSurface
      _RL tauc_recip
CEOP

C--   Forcing term
 
      IF (tau_damp(kLev) .gt. 0.) THEN
        tauc_recip = 1.0 _d 0/(tau_damp(kLev)*86400.0 _d 0*P2SI)
      ELSE
        tauc_recip = 0.0 _d 0
      ENDIF

      DO j=jMin,jMax
       DO i=iMin,iMax
        gV(i,j,kLev,bi,bj) = gV(i,j,kLev,bi,bj) 
c        Add PBL tendency for V
     & + gvblphy(i,j,kLev,bi,bj) * maskS(i,j,kLev,bi,bj)
c        Add Rayleigh drag for V
     & - tauc_recip * maskS(i,j,kLev,bi,bj)
     & * vVel(i,j,kLev,bi,bj)
       ENDDO
      ENDDO

      RETURN
      END

C---+----1----+----2----+----3----+----4----+----5----+----6----+----7-|--+----|
CBOP
C     !ROUTINE: WRF_TENDENCY_APPLY_T
C     !INTERFACE:
      SUBROUTINE WRF_TENDENCY_APPLY_T(
     I           iMin,iMax, jMin,jMax, bi,bj, kLev,
     I           myTime, myThid )
C     !DESCRIPTION: \bv
C     *==========================================================*
C     | S/R WRF_TENDENCY_APPLY_T
C     | o Contains problem specific forcing for temperature.
C     *==========================================================*
C     | Adds terms to gT for forcing by external sources
C     | e.g. heat flux, climatalogical relaxation, etc ...
C     *==========================================================*
C     \ev

C     !USES:
      IMPLICIT NONE
C     == Global data ==
#include "SIZE.h"
#include "WRF_SIZE.h"
#include "EEPARAMS.h"
#include "PARAMS.h"
#include "WRF_PARAMS.h"
#include "GRID.h"
#include "DYNVARS.h"
#include "FFIELDS.h"
#include "SURFACE.h"
#ifdef WRF_ENABLE_TRACERS
#include "PTRACERS_SIZE.h"
#endif /* WRF_ENABLE_TRACERS */
#include "WRF_PHYS.h"
#include "WRF_CONFIG.h"

C     !INPUT/OUTPUT PARAMETERS:
C     == Routine arguments ==
C     iMin,iMax :: Working range of x-index for applying forcing.
C     jMin,jMax :: Working range of y-index for applying forcing.
C     bi,bj     :: Current tile indices
C     kLev      :: Current vertical level index
C     myTime    :: Current time in simulation
C     myThid    :: Thread Id number
      INTEGER iMin, iMax, jMin, jMax, kLev, bi, bj
      _RL myTime
      INTEGER myThid

C     !LOCAL VARIABLES:
C     == Local variables ==
C     i,j       :: Loop counters
C     kSurface  :: index of surface layer
      INTEGER i, j, k
      INTEGER kSurface
CEOP
      _RL tauc_recip
      _RL pNrkappa
      _RL meanT,dampT,totmass

C--   Forcing term

      IF (tau_damp(kLev) .gt. 0.) THEN
        tauc_recip = 1.0 _d 0/(tau_damp(kLev)*86400.0 _d 0*P2SI)
      ELSE
        tauc_recip = 0.0 _d 0
      ENDIF

      totmass = 0.0 _d 0
#ifdef WRF_TITAN
      meanT = 180. _d 0 
#elif defined WF_MARS
      meanT = 140. _d 0
#endif /* WRF PLANETS */

      DO j=jMin,jMax
       DO i=iMin,iMax
C--  assuming the very top layer is not affected by PBL
        if (kLev .ge. ksurfC(i,j,bi,bj)) then
           pNrkappa = atm_cp*(pcphy(i,j,kLev,bi,bj)/atm_po)**atm_kappa
        else 
c   avoid devided by zero, so set pNrkappa to some constant != 0
c   below the topography
          pNrkappa = 1.0 _d 0
        endif
       dampT=meanT*(atm_po/pcphy(i,j,kLev,bi,bj))**atm_kappa
c       dampT=185.0*(atm_po/pcphy(i,j,kLev,bi,bj))**atm_kappa    

          gT(i,j,kLev,bi,bj) = maskC(i,j,kLev,bi,bj)*
     &      ( gT(i,j,kLev,bi,bj) 
c            temperature tendency from radiation
     &        + RTHRATEN(i,j,kLev,bi,bj)
c            temperature tendency from PBL mixing
     &        + RTHBLTEN(i,j,kLev,bi,bj) 
     &      ) 
C      the energy conserving method does not work for Titan model
#ifdef WRF_MARS
     .      + tauc_recip/pNrkappa * 0.5 *(maskW(i,j,kLev,bi,bj)
     .  *uVel(i,j,kLev,bi,bj)*uVel(i,j,kLev,bi,bj)+ 
     .  maskW(i+1,j,kLev,bi,bj)*
     .                uVel(i+1,j,kLev,bi,bj)*uVel(i+1,j,kLev,bi,bj)+
     .  maskS(i,j,kLev,bi,bj)*vVel(i,j,kLev,bi,bj)*vVel(i,j,kLev,bi,bj)+
     .  maskS(i,j+1,kLev,bi,bj)*
     .                vVel(i,j+1,kLev,bi,bj)*vVel(i,j+1,kLev,bi,bj))
#endif /* WRF_MARS */
C      no need to damp temperature for Mars
#ifdef WRF_TITAN
C     &        - (theta(i,j,kLev,bi,bj)-dampT)
C     &        * tauc_recip*maskC(i,j,kLev,bi,bj)
#endif /* WRF_TITAN */
       ENDDO
      ENDDO

      RETURN
      END

C---+----1----+----2----+----3----+----4----+----5----+----6----+----7-|--+----|
CBOP
C     !ROUTINE: WRF_TENDENCY_APPLY_S
C     !INTERFACE:
      SUBROUTINE WRF_TENDENCY_APPLY_S(
     I           iMin,iMax, jMin,jMax, bi,bj, kLev,
     I           myTime, myThid )

C     !DESCRIPTION: \bv
C     *==========================================================*
C     | S/R WRF_TENDENCY_APPLY_S
C     | o Contains problem specific forcing for merid velocity.
C     *==========================================================*
C     | Adds terms to gS for forcing by external sources
C     | e.g. fresh-water flux, climatalogical relaxation, etc ...
C     *==========================================================*
C     \ev

C     !USES:
      IMPLICIT NONE
C     == Global data ==
#include "SIZE.h"
#include "EEPARAMS.h"
#include "PARAMS.h"
#include "GRID.h"
#include "DYNVARS.h"
#include "FFIELDS.h"
#include "SURFACE.h"

C     !INPUT/OUTPUT PARAMETERS:
C     == Routine arguments ==
C     iMin,iMax :: Working range of x-index for applying forcing.
C     jMin,jMax :: Working range of y-index for applying forcing.
C     bi,bj     :: Current tile indices
C     kLev      :: Current vertical level index
C     myTime    :: Current time in simulation
C     myThid    :: Thread Id number
      INTEGER iMin, iMax, jMin, jMax, kLev, bi, bj
      _RL myTime
      INTEGER myThid

C     !LOCAL VARIABLES:
C     == Local variables ==
C     i,j       :: Loop counters
C     kSurface  :: index of surface layer
      INTEGER i, j

C--   Forcing term
      DO j=jMin,jMax
       DO i=iMin,iMax
           gS(i,j,kLev,bi,bj)=gS(i,j,kLev,bi,bj)
       ENDDO
      ENDDO

      RETURN
      END

C---+----1----+----2----+----3----+----4----+----5----+----6----+----7-|--+----|
CBOP
C     !ROUTINE: WRF_TENDENCY_APPLY_PTR
C     !INTERFACE:
      SUBROUTINE WRF_TENDENCY_APPLY_PTR(gPtracer,
     I           iMin,iMax, jMin,jMax, bi,bj, kLev, iTr,
     I           myTime, myThid )

C     !DESCRIPTION: \bv
C     *==========================================================*
C     | S/R WRF_TENDENCY_APPLY_PTR
C     | o Add passive tracer tendencies due to physical processes.
C     *==========================================================*
C     | Adds terms to gPtr for forcing by external sources
C     | e.g. surface flux, climatalogical relaxation, etc ...
C     *==========================================================*
C     \ev

C     !USES:
      IMPLICIT NONE
C     == Global data ==
#include "SIZE.h"
#include "WRF_SIZE.h"
#include "EEPARAMS.h"
#include "PARAMS.h"
#include "WRF_PARAMS.h"
#include "GRID.h"
#include "DYNVARS.h"
#include "FFIELDS.h"
#include "SURFACE.h"
#ifdef WRF_ENABLE_TRACERS
#include "PTRACERS_SIZE.h"
#endif /* WRF_ENABLE_TRACERS */
#include "WRF_PHYS.h"
#include "WRF_CONFIG.h"
C     !INPUT/OUTPUT PARAMETERS:
C     == Routine arguments ==
C     iMin,iMax :: Working range of x-index for applying forcing.
C     jMin,jMax :: Working range of y-index for applying forcing.
C     bi,bj     :: Current tile indices
C     kLev      :: Current vertical level index
C     iTr       :: Tracer number index
C     myTime    :: Current time in simulation
C     myThid    :: Thread Id number
      INTEGER iMin, iMax, jMin, jMax, kLev, iTr, bi, bj
      _RL myTime
      INTEGER myThid
      _RL gPtracer(1-Olx:sNx+Olx,1-Oly:sNy+Oly,Nr,nSx,nSy)

C     !LOCAL VARIABLES:
C     == Local variables ==
C     i,j       :: Loop counters
C     kSurface  :: index of surface layer
      INTEGER i, j

C--   Forcing term
#ifdef WRF_ENABLE_TRACERS_DIS
      DO j=jMin,jMax
       DO i=iMin,iMax
           gPtracer(i,j,kLev,bi,bj)=gPtracer(i,j,kLev,bi,bj)
     &                          + gtrphy(i,j,kLev,bi,bj,iTr)
       ENDDO
      ENDDO
#endif /* WRF_ENABLE_TRACERS */

      RETURN
      END
