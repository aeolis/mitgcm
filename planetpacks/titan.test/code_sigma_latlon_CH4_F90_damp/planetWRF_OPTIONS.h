#ifndef WRF_PLANET
#define WRF_PLANET
#endif

!--//++++++++++++++++ Section 1 ++++++++++++++++++
!==/// Model Selection:  only change the definition of Mars or Titan ///==
#ifndef WRF_MARS
#undef WRF_MARS
#endif /* WRF_MARS */

#ifndef WRF_TITAN
#define WRF_TITAN
#endif /* WRF_TITAN */
!==/// End of Model Selection ///==

#undef MARS24_TIMING
!--//++++++++++++++++ Section 2 ++++++++++++++++++
!==/// Physics Selection (shared by all models) ///==
!-- // the following should not be changed, unless it 
!-- // is very necessary (such as debugging !!!)

#ifndef EM_CORE
#define EM_CORE 1
#endif /* EM_CORE */

!==/// End of Physics Selection ///==



!--//++++++++++++++++ Section 3 ++++++++++++++++++
!=== /// Start of configurable section for physics /// ===


!--//++++++++++++++++ Section 4 ++++++++++++++++++
!=== /// Fixed definition, not user changeable!!! /// ====


!--// this part can not be modified !!! This overides the user defined 
!--// option if the dependance package does not present. Turning off 
!--// WRF_ENABLE_TRACERS intentionally will turn off the physics processes 
!--// that modify the tracer field (in physics grid), but it won't affect 
!--// the tracer advection as long as ALLOW_PTRACERS is defined (in dynamics grid).
#ifdef ALLOW_PTRACERS

#define WRF_ENABLE_TRACERS 

#else /* ALLOW_PTRACERS */

#undef WRF_ENABLE_TRACERS

#endif /* ALLOW_PTRACERS */

