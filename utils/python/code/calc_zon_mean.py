import numpy as np
import load_grid
from process_datafiles import metadata 
import rdmds as rd
import calc_planet_ls as calc_ls

def zon_mean(dir, vgrid='pressure'):
	class grid:
		pass

	class x2d:
		pass

	mdda=metadata(dir)
	mdda.gen_alldata()

	# get all records
	alldata=np.zeros(mdda.nrecords)

	for i in range(mdda.nrecords):
		alldata[i] = mdda.nIter0 + i*mdda.dumpFreq/mdda.deltaT

	# load grids
	xy=load_grid.load_xy(dir)
	zp=load_grid.load_zp(dir)

	grid.lat = np.mean(xy.yc,axis=0)
	if vgrid == 'pressure':
		grid.p   = zp.rc
	elif vgrid == 'sigma':
		topoP = rd.rdmds(dir,'topo_P')
		rFullDepth = zp.rf[0]-zp.rf[-1]

	# get variable dimensions
	nx = np.size(xy.xc,0)
	ny = np.size(xy.xc,1)
	nz = np.size(zp.rc,0)
	# initialize arrays
	pedyn = np.zeros( [nx, ny, nz+1] )
	pcdyn = np.zeros( [nx, ny, nz  ] )
	x2d.ch4d_zon = np.zeros([mdda.nrecords, ny])
	x2d.ch4e_zon = np.zeros([mdda.nrecords, ny])
	grid.ls = np.zeros([mdda.nrecords])
	grid.year = np.zeros([mdda.nrecords])

	# get all records, u, v, ch4 etc
	for niter in range(mdda.nrecords):
		ch4d=rd.rdmds(dir,'ch4dep',niter+1)
		ch4e=rd.rdmds(dir,'ch4evp',niter+1)
		x2d.ch4d_zon[niter, :] = np.sum(ch4d,axis=0)
		x2d.ch4e_zon[niter, :] = np.sum(ch4e,axis=0)
		grid.ls[niter], grid.year[niter] = calc_ls.calc_planet_ls(mdda.nIter0, alldata[niter], mdda.deltaT, 'titan')
		#Eta=rd.rdmds(dir,'Eta',niter+1)
		#for k in range(nz):
		#	pedyn[:,:,k]=zp.aHybF[k]*rFullDepth+zp.sigF[k]*(topoP+Eta)

	return grid, x2d


