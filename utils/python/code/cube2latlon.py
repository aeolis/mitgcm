# This module contains the following functions
# main functions:
#      xi,yi,z   = cube2latlon_scalar(x,y,c,xi=[],yi=[]) ==> need TriInterp
#      xi,yi,U,V = uvcube2latlon(u,v,xc,yc,xg,yg,rac,dxg,dyg,xi=[],yi=[]) 
#      uE,vN     = rotate_uv2uvEN(u,v,AngleCS,AngleSN,Grid='C')
#      AngleCS,AngleSN = cubeCalcAngle(YG,RAC,dxG,dyG)
#      z6t       = split_Z_cub(z3d)
#      u6t,v6t   = split_UV_cub(u3d,v3d,ksign,kad)
#****************************************************#
# 						     #
#      Part 1. Interpolation of scalars		     #
#						     #
#****************************************************#
# Simple linear-triangulation interpolation of scalars. 
# see: 
#    https://github.com/ianthomas23/matplotlib/commit/413e18e371bfd910899d9269e8d8ed22b728ef6f
#    for examples in section lib/matplotlib/tests/test_triangulation.py
# z=cube2latlon_scalar(x,y,c,xi,yi);
#
# Re-grids model output on expanded spherical cube to lat-lon grid.
#  x,y   are 2-D arrays of the cell-centered coordinates 
#  c     is a 2-D or 3-D scalar field
#  xi,yi are optional vectors of the new regular lat-lon grid to interpolate to.
#        The default value maps the equivalent cube-sphere grid resolution 
#  z     is the interpolated data with dimensions of size(xi) by size(yi).
#
# e.g.
# >> x=rdmds('XC');
# >> y=rdmds('YC');
# >> xi=-179:2:180;yi=-89:2:90;
# >> ti=cube2latlon_scalar(x,y,t,xi,yi);

# need to deal with some out of range data (NaNs). will fix it
def cube2latlon_scalar(x,y,c,xi=[],yi=[]):
    import numpy as np
    import matplotlib.tri as mtri
    from matplotlib import delaunay
    NN=np.asarray(np.shape(c))
    if np.ndim(c)==2:
       nx,ny=np.shape(c)
    elif np.ndim(c)==3: 
       nx,ny,nz=np.shape(c)

    # define the longtiude-latitude grid if not provided
    if np.size(xi)==0:
       print('Longitude in spherical-polar grid is not provided')
       print('Use default resolution dx = '+'%3.3f'%(180./(2.*ny))+'[deg]')
       dx=180./(2.*ny)
       xi=np.linspace(-180.+dx/2.,180.-dx/2.,4*ny)
    if np.size(yi)==0:
       print('Latitude in spherical-polar grid is not provided')
       print('Use default resolution dy = '+'%3.3f'%(180./(2.*ny))+'[deg]')
       dy=180./(2.*ny)
       yi=np.linspace(-90.+dy/2.,90.-dy/2.,2*ny)

    lx,ly=np.meshgrid(xi,yi)

    X=np.ravel(x)
    Y=np.ravel(y)
    # extend the coverage of longitude (no points are outside of domain
    # after interpolation)
    ig=np.where(X> 90)
    il=np.where(X<-90)
    XX=np.append(np.append(X,X[il]+360.),X[ig]-360.)
    YY=np.append(np.append(Y,Y[il]),Y[ig])

    circumcenters, edges, tri_points, tri_neighbors = delaunay.delaunay(XX,YY)

    if np.ndim(c)==2:
       C=np.ravel(c)
       zs = TriInterp(x,y,c,C,il,ig,XX,YY,tri_points,lx,ly)
       z  = zs.reshape(len(xi),len(yi), order='F')
    elif np.ndim(c)==3:
       z1d=[]
       for k in range (nz):
           C=np.ravel(c[:,:,k])
           zs=TriInterp(x,y,c[:,:,k],C,il,ig,XX,YY,tri_points,lx,ly)
           z1d=np.append(z1d,zs)

       # mask the "nan"s to make pcolor plot (different from matlab 
       # where "nan"s are ignored). When summing over masks, the values 
       # are ignored. This is more like a sanity check because the masks 
       # are filled with polar values already
       z1d = np.ma.array(z1d, mask=np.isnan(z1d))

       z=z1d.reshape(len(xi),len(yi),nz,order='F')

    return xi,yi,z

def TriInterp(x,y,c,C,il,ig,XX,YY,tri_points,lx,ly):
    import numpy as np
    import matplotlib.tri as mtri
   
    CC=np.append(np.append(C,C[il]),C[ig])
       
    triang = mtri.Triangulation(XX,YY, tri_points)
    linear_interp = mtri.LinearTriInterpolator(triang, CC)
    zs = linear_interp(lx.ravel(), ly.ravel()) 
    # fill the mask with values obtained from cube-sphere grid at pole
    # because all masks locate at the south or pole
    for i, item in enumerate(zs.mask):
        if zs.mask[i] and (ly.ravel()[i]<0.):
           zs[i]=np.mean(c[np.where(y==np.min(y))])
        elif zs.mask[i] and (ly.ravel()[i]>0.):
           zs[i]=np.mean(c[np.where(y==np.max(y))])

    return zs

#****************************************************#
# 						     #
#      Part 2. Interpolation of vectors		     #
#						     #
#****************************************************#


# ui,vi=uvcube2latlon(u,v,xc,yc,xg,yg,rac,dxg,dyg,xi,yi)
#
# Re-grids model output on expanded spherical cube to lat-lon grid.
#  u,v   is a 2-D or 3-D horizontal components of model flow fields.
#  xc,yc   are 2-D arrays of the cell-centered coordinates 
#  xg,yg   are 2-D arrays of the cell-edge coordinates
#  dxg,dyg are 2-D arrays of the edge-edge length in x and y
#  rac     is 2-D array of cell-centered area
#  xi,yi are vectors of the new regular lat-lon grid to interpolate to.
#  ui,vi are the flow fields with dimensions of size(xi) x size(yi) size(u,3).
#       they are on 'A' grid as tracers
#
# modified using matlab script provided by adcroft@.mit.edu, 2001.

def uvcube2latlon(u,v,xc,yc,xg,yg,rac,dxg,dyg,xi=[],yi=[]):
    import numpy as np

    if np.ndim(u)==2:
       nx,ny=np.shape(u)
    elif np.ndim(u)==3: 
       nx,ny,nz=np.shape(u)

    # define the longtiude-latitude grid if not provided
    if np.size(xi)==0:
       print('Longitude in spherical-polar grid is not provided')
       print('Use default resolution dx = '+'%3.3f'%(180./(2.*ny))+'[deg]')
       dx=180./(2.*ny)
       xi=np.linspace(-180.+dx/2.,180.-dx/2.,4*ny)
    if np.size(yi)==0:
       print('Latitude in spherical-polar grid is not provided')
       print('Use default resolution dy = '+'%3.3f'%(180./(2.*ny))+'[deg]')
       dy=180./(2.*ny)
       yi=np.linspace(-90.+dy/2.,90.-dy/2.,2*ny)

    AngleCS,AngleSN = cubeCalcAngle(yg,rac,dxg,dyg)

    U,V=rotate_uv2uvEN(u,v,AngleCS,AngleSN,'A')

    xi,yi,U=cube2latlon_scalar(xc,yc,U,xi,yi)
    xi,yi,V=cube2latlon_scalar(xc,yc,V,xi,yi)

    return xi, yi, U, V

#function [uE,vN] = rotate_uv2uvEN(u,v,AngleCS,AngleSN,Grid)
# [uE,vN] = rotate_uv2uvEN(u,v,AngleCS,AngleSN,Grid)
#
# Rotate cube sphere U and V vector components to east-west (uE) and
# north-south (vN) components located on cube sphere grid centers.
#
# Incoming u and v matricies are assumed to be cube sphere A-grid or C-grid
# vector fields (defaut is C-grid) where the first two dimensions are (6*nc
# nc), where nc is the cube face resolution.  There may up to 4 additional
# dimensions (likely z and time, trials, etc.) beyond this.
#
# e.g.
#
# >> [uE,vN] = rotate_uv2uvEN(uC,vC,AngleCS,AngleSN,'C');
#
# >> [uE,vN] = rotate_uv2uvEN(uA,vA,AngleCS,AngleSN,'A');

# $Header: /u/gcmpack/MITgcm/utils/matlab/cs_grid/rotate_uv2uvEN.m,v 1.2 2006/02/23 14:04:12 jmc Exp $
# $Name:  $

# Default is a C-grid configuration.

def rotate_uv2uvEN(u,v,AngleCS,AngleSN,Grid='C'):
   
    import numpy as np
    # Verify dimensions are that of cube-sphere
    if np.shape(u)[0] != 6.*np.shape(u)[1] or np.shape(v)[0] != 6.*np.shape(v)[1]:
       raise ValueError('Error in CS-dimensions: '+'u = '+str(np.shape(u)) \
                                            + ', '+'v = '+str(np.shape(v)))

    # Parse dimension information, flatten extra dimensions. 
    if np.ndim(u)==2:
       # exends 2D u and v to 3D
       dim2=np.asarray(np.shape(u))
       u=np.reshape(u,(np.append(dim2,1)),order='F')
       v=np.reshape(v,(np.append(dim2,1)),order='F')
    dim=np.asarray(np.shape(u)) 
    nc=dim[1]
    nz=dim[2]
    u=np.reshape(u,(6*nc,nc,nz),order='F')
    v=np.reshape(v,(6*nc,nc,nz),order='F')

    # Do simple average to put u,v at the cell center (A-grid) as needed.
    if Grid=='A':
       u=np.reshape(u,(6*nc*nc,nz),order='F')
       v=np.reshape(v,(6*nc*nc,nz),order='F')
    elif Grid=='C':
       uu,vv = split_UV_cub(u,v)
       uu=np.reshape(uu,(nc+1,nc,nz,6),order='F')
       vv=np.reshape(vv,(nc,nc+1,nz,6),order='F')
       u=(uu[:nc,:,:,:]+uu[1:nc+1,:,:,:])/2.
       v=(vv[:,:nc,:,:]+vv[:,1:nc+1,:,:])/2.
       u=u.transpose(0,3,1,2).reshape(6*nc*nc,nz,order='F')
       v=v.transpose(0,3,1,2).reshape(6*nc*nc,nz,order='F')
    else:
       raise ValueError('Unrecognized grid type: '+Grid)

    # Make rotation to find uE, vN.
    uE=np.nan*np.zeros((6*nc*nc,nz))
    vN=np.nan*np.zeros((6*nc*nc,nz))
    AngleCS=AngleCS.reshape(6*nc*nc,order='F')
    AngleSN=AngleSN.reshape(6*nc*nc,order='F')
    for k in range(nz):
        uE[:,k]=AngleCS[:]*u[:,k]-AngleSN[:]*v[:,k]
        vN[:,k]=AngleSN[:]*u[:,k]+AngleCS[:]*v[:,k]
    uE = uE.reshape(dim,order='F')
    vN = vN.reshape(dim,order='F')

    return uE, vN

#********************************************************************#
# 						    		     #
#      Part 3. Calculate rotation angles (for vectors)		     #
#						     		     #
#********************************************************************#


#function [AngleCS,AngleSN] = cubeCalcAngle(YG,RAC,dxG,dyG);
# [AngleCS,AngleSN] = cubeCalcAngle(YG,RAC,dxG,dyG);
#
# Determine rotation angle, alpha (returned as cos(alpha) and sin(alpha)),
# to convert cube sphere C-grid vectors to east-west and north-south
# components.
#
# e.g.
#
# >> XG=rdmds('XG');
# >> YG=rdmds('YG');
# >> RAC=rdmds('RAC');
# >> dxG=rdmds('dxG');
# >> dyG=rdmds('dyG');
# >> [AngleCS,AngleSN] = cubeCalcAngle(YG,RAC,dxG,dyG);
#

def cubeCalcAngle(YG,RAC,dxG,dyG):
    import numpy as np
    nc=np.shape(RAC)[1]
    deg2rad=np.pi/180
    r_earth=np.sqrt(np.sum(RAC)/(4.*np.pi))

    yZ6=split_Z_cub(YG)

    # Build purely zonal flow from a stream function, psi = r_earth * latitude.
    psi=-r_earth*yZ6*deg2rad
    uZ=psi[:nc,:nc,:]-psi[:nc,1:nc+1,:]
    vZ=psi[1:nc+1,:nc,:]-psi[:nc,:nc,:]
    uZ=uZ.transpose(0,2,1).reshape(6.*nc,nc,order='F')/dyG
    vZ=vZ.transpose(0,2,1).reshape(6.*nc,nc,order='F')/dxG

    # Put constructed zonal wind at cell center.
    uu,vv=split_UV_cub(uZ,vZ)
    u=(uu[:nc,:,:]+uu[1:nc+1,:,:])/2.
    v=(vv[:,:nc,:]+vv[:,1:nc+1,:])/2.
    uZc=u.transpose(0,2,1).reshape(6.*nc,nc,order='F')
    vZc=v.transpose(0,2,1).reshape(6.*nc,nc,order='F')

    # Calculate angles.
    norm=np.sqrt(uZc*uZc+vZc*vZc)
    AngleCS =  uZc/norm
    AngleSN = -vZc/norm

    return AngleCS, AngleSN

#********************************************************************#
# 						    		     #
#      Part 4. Splitting tiles of scalars and vectors		     #
#						     		     #
#********************************************************************#

#function [z6t] = split_Z_cub(z3d)
# [z6t] = split_Z_cub(z3d)
#---------------------------------------------
# split 2d/3d arrays z3d to 2d/3d x 6 faces
# and add 1 column + 1 row
#  => output is z6t(nc+1,nc+1,[nr],6)
# either input is z3d(nc*6*nc+2,*): includes the 2 missing corners
#   or   input is z3d(nc*6,nc,*): => use the average value (from the 3
#                                 neighbours) for the 2 missing corners
#----------------------------------------------
# Written by jmc@ocean.mit.edu, 2005.
# $Header: /u/gcmpack/MITgcm/utils/matlab/cs_grid/split_Z_cub.m,v 1.1 2005/09/15 16:22:24 jmc Exp $
# $Name:  $

def split_Z_cub(z3d):
    import numpy as np
    dims=np.asarray(np.shape(z3d)) 
    ND=np.ndim(z3d)

    nc=np.fix((dims[0]-2)/6) 
    nc=np.fix(np.sqrt(abs(nc)))
    if dims[0] == nc*nc*6+2:
       nr=dims[1] 
       if ND > 2: 
          nr=dims[1]*dims[2]
          nx=6*nc 
          nPg=nx*nc 
          nPts=nPg+2 
          dims=np.append([nPts,1],dims[1:])
          z3d=np.reshape(z3d,(nPts,nr),order='F')
          zzC=z3d[nPg:nPg+2,:] 
          z3d=z3d[:nPg,:]
    elif dims[0] == 6*dims[1]:
       if ND == 2: 
          nr=1 
       else:
          nr=dims[2]
       nc=dims[1] 
       nx=6*nc 
       nPg=nx*nc 
       nPts=nPg+2
       zzC=np.zeros((2,nr))
    else:
       raise ValueError(' Error in split_Z_cub: bad input dimensions :'+ str(dims))

    #=================================================================

    z3d=np.reshape(z3d,(nc,6,nc,nr),order='F').transpose(0,2,3,1)
    ncp=nc+1 
    z6t=np.zeros((ncp,ncp,nr,6))

    #-- split on to 6 faces:
    z6t[:nc,:nc,:,:]=z3d.copy()

    #-- add overlap in i+1 & j+1 :
    z6t[nc,:nc,  :,0]=z3d[0,:nc,:,1].copy()
    z6t[nc,1:ncp,:,1]=z3d[nc::-1,0,:,3].copy()
    z6t[nc,:nc,  :,2]=z3d[0,:nc,:,3].copy()
    z6t[nc,1:ncp,:,3]=z3d[nc::-1,0,:,5].copy()
    z6t[nc,:nc,  :,4]=z3d[0,:nc,:,5].copy()
    z6t[nc,1:ncp,:,5]=z3d[nc::-1,0,:,1].copy()
 
    z6t[1:ncp,nc,:,0]=z3d[0,nc::-1,:,2].copy()
    z6t[:nc,  nc,:,1]=z3d[:nc,0,:,2].copy()
    z6t[1:ncp,nc,:,2]=z3d[0,nc::-1,:,4].copy()
    z6t[0:nc, nc,:,3]=z3d[:nc,0,:,4].copy()
    z6t[1:ncp,nc,:,4]=z3d[0,nc::-1,:,0].copy()
    z6t[0:nc, nc,:,5]=z3d[:nc,0,:,0].copy()   

    #----------------------------------------------------

    #-- missing corners :
    if dims[0] != nPts:
    #- use the average value (from the 3 neighbours)
       zzC[0,:]=z3d[0,nc-1,:,0]+z3d[0,nc-1,:,2]+z3d[0,nc-1,:,4]
       zzC[1,:]=z3d[nc-1,0,:,1]+z3d[nc-1,0,:,3]+z3d[nc-1,0,:,5]
       zzC=zzC/3
       print('split_Z_cub: fills 2 missing corners with local mean value')
    #- 1rst (nPg+1) = N.W corner of face 1
    z6t[0,nc,:,0]=zzC[0,:]
    z6t[0,nc,:,2]=zzC[0,:]
    z6t[0,nc,:,4]=zzC[0,:]
    #- 2nd  (nPg+2) = S.E corner of face 2
    z6t[nc,0,:,1]=zzC[1,:]
    z6t[nc,0,:,3]=zzC[1,:]
    z6t[nc,0,:,5]=zzC[1,:]

    if ND > 2:
       z6t=np.reshape(z6t,(ncp,ncp,dims[2],6),order='F')
    else:
       z6t=np.squeeze(z6t)

    return z6t


#function [u6t,v6t] = split_UV_cub(u3d,v3d,ksign,kad)
# [u6t,v6t] = split_UV_cub(u3d,v3d,[ksign,kad])
#---------------------------------------------
# split 2d/3d arrays u,v to 3d x 6 faces, and:
# (kad=1): add 1 column to U and one row to V
#  => output is u6t(nc+1,nc,[nr],6) & v6t(nc,nc+1,[nr],6)
# (kad=2): add 1 column + 2 rows to U and 2 columns + 1 row to V
#  => output is u6t(nc+1,nc+2,[nr],6) & v6t(nc+2,nc+1,[nr],6)
#----------------------------------------------
# Written by jmc@ocean.mit.edu, 2005.
# $Header: /u/gcmpack/MITgcm/utils/matlab/cs_grid/split_UV_cub.m,v 1.1 2005/09/15 16:22:24 jmc Exp $
# $Name:  $

def split_UV_cub(u3d,v3d,ksign=0,kad=1):

    import numpy as np

    # ksign = 0 ==> no sign ; ksign = 1 ==> change the sign where needed
    # ==> in fact, used only if kad=2
    plmn = 1 - 2*ksign

    dims=np.asarray(np.shape(u3d))
    if dims[0] != 6*dims[1]:
       print(' dimensions: '+str(dims)+' ; => STOP')
       raise ValueError(' ERROR in split_UV_cub: 1rst array has the wrong shape !')
    dims=np.asarray(np.shape(v3d)) 
    ND=np.ndim(v3d)
    if dims[0] != 6*dims[1]:
       print(' dimensions: '+str(dims)+' ; => STOP')
       raise ValueError(' ERROR in split_UV_cub:  2nd array has the wrong shape !')
    nx=dims[0]; nc=dims[1]; ncp=nc+1; n2p=nc+2;
    if ND == 2: 
       nr=1 
    else: 
       nr=dims[2]
    #=================================================================

    u3d=np.reshape(u3d,(nc,6,nc,nr),order='F').transpose(0,2,3,1)
    v3d=np.reshape(v3d,(nc,6,nc,nr),order='F').transpose(0,2,3,1)

    if kad == 0:
       #- split on to 6 faces with no overlap:
       dims[0]=nc 
       u6t=u3d.copy()
       v6t=v3d.copy()

    elif kad == 1:
       dims[0]=ncp
       #- split on to 6 faces with overlap in i+1 for u and j+1 for v :
       u6t=np.zeros((ncp,nc,nr,6))
       v6t=np.zeros((nc,ncp,nr,6))
       u6t[:nc,:,:,:]=u3d[:,:,:,:].copy()
       v6t[:,:nc,:,:]=v3d[:,:,:,:].copy()

       u6t[nc,:nc,:,0]=u3d[0,:nc,:,1].copy()
       u6t[nc,:nc,:,1]=v3d[nc::-1,0,:,3].copy()
       u6t[nc,:nc,:,2]=u3d[0,:nc,:,3].copy()
       u6t[nc,:nc,:,3]=v3d[nc::-1,0,:,5].copy()
       u6t[nc,:nc,:,4]=u3d[0,:nc,:,5].copy()
       u6t[nc,:nc,:,5]=v3d[nc::-1,0,:,1].copy()

       v6t[:nc,nc,:,0]=u3d[0,nc::-1,:,2].copy()
       v6t[:nc,nc,:,1]=v3d[:nc,0,:,2].copy()
       v6t[:nc,nc,:,2]=u3d[0,nc::-1,:,4].copy()
       v6t[:nc,nc,:,3]=v3d[:nc,0,:,4].copy()
       v6t[:nc,nc,:,4]=u3d[0,nc::-1,:,0].copy()
       v6t[:nc,nc,:,5]=v3d[:nc,0,:,0].copy()   

    elif kad == 2:
         dims[0]=ncp
         dims[1]=n2p
         #- split on to 6 faces:
         u6t=np.zeros((ncp,n2p,nr,6))
         v6t=np.zeros((n2p,ncp,nr,6))
         u6t[:nc,1:ncp,:,:]=u3d[:,:,:,:].copy()
         v6t[1:ncp,:nc,:,:]=v3d[:,:,:,:].copy()

         #- add overlap in i=nc+1 for u and j=nc+1 for v :
         u6t[nc,1:ncp,:,0]=u3d[0,:nc,:,1].copy()
         u6t[nc,1:ncp,:,1]=v3d[nc::-1,0,:,3].copy()
         u6t[nc,1:ncp,:,2]=u3d[0,:nc,:,3].copy()
         u6t[nc,1:ncp,:,3]=v3d[nc::-1,0,:,5].copy()
         u6t[nc,1:ncp,:,4]=u3d[0,:nc,:,5].copy()
         u6t[nc,1:ncp,:,5]=v3d[nc::-1,0,:,1].copy()

         v6t[1:ncp,nc,:,0]=u3d[0,nc::-1,:,2].copy()
         v6t[1:ncp,nc,:,1]=v3d[:nc,0,:,2].copy()
         v6t[1:ncp,nc,:,2]=u3d[0,nc::-1,:,4].copy()
         v6t[1:ncp,nc,:,3]=v3d[:nc,0,:,4].copy()
         v6t[1:ncp,nc,:,4]=u3d[0,nc::-1,:,0].copy()
         v6t[1:ncp,nc,:,5]=v3d[:nc,0,:,0].copy()   

         #- add overlap in j=0,nc+1 for u and i=0,nc+1 for v :
         u6t[:ncp, 0 ,  :,0]=u6t[:ncp,nc,:,5].copy()
         u6t[:ncp,n2p-1,:,0]=v6t[ 1 ,ncp::-1,:,2]*plmn
         u6t[:ncp, 0 ,  :,2]=u6t[:ncp,nc,:,1].copy()
         u6t[:ncp,n2p-1,:,2]=v6t[ 1 ,ncp::-1,:,4]*plmn
         u6t[:ncp, 0 ,  :,4]=u6t[:ncp,nc,:,3].copy()
         u6t[:ncp,n2p-1,:,4]=v6t[ 1 ,ncp::-1,:,0]*plmn

         u6t[:ncp, 0 ,  :,1]=v6t[nc,ncp::-1,:,5]*plmn
         u6t[:ncp,n2p-1,:,1]=u6t[:ncp, 1 ,:,2].copy()
         u6t[:ncp, 0 ,  :,3]=v6t[nc,ncp::-1,:,1]*plmn
         u6t[:ncp,n2p-1,:,3]=u6t[:ncp, 1 ,:,4].copy()
         u6t[:ncp, 0 ,  :,5]=v6t[nc,ncp::-1,:,3]*plmn
         u6t[:ncp,n2p-1,:,5]=u6t[:ncp, 1 ,:,0].copy()

         v6t[ 0 ,  :ncp,:,0]=u6t[ncp::-1,nc,:,4]*plmn
         v6t[n2p-1,:ncp,:,0]=v6t[ 1 ,:ncp,:,1].copy()
         v6t[ 0 ,  :ncp,:,2]=u6t[ncp::-1,nc,:,0]*plmn
         v6t[n2p-1,:ncp,:,2]=v6t[ 1 ,:ncp,:,3].copy()
         v6t[ 0 ,  :ncp,:,4]=u6t[ncp::-1,nc,:,2]*plmn
         v6t[n2p-1,:ncp,:,4]=v6t[ 1 ,:ncp,:,5].copy()

         v6t[ 0 ,  :ncp,:,1]=v6t[nc,:ncp,:,0].copy()
         v6t[n2p-1,:ncp,:,1]=u6t[ncp::-1, 1 ,:,3]*plmn
         v6t[ 0 ,  :ncp,:,3]=v6t[nc,:ncp,:,2].copy()
         v6t[n2p-1,:ncp,:,3]=u6t[ncp::-1, 1 ,:,5]*plmn
         v6t[ 0 ,  :ncp,:,5]=v6t[nc,:ncp,:,4].copy()
         v6t[n2p-1,:ncp,:,5]=u6t[ncp::-1, 1 ,:,1]*plmn


    #- restaure the right shape:
    if ND == 2:
      u6t=np.squeeze(u6t)
      v6t=np.squeeze(v6t)
    else:
      u6t=np.reshape(u6t,(dims[0],dims[1],dims[2],6))
      v6t=np.reshape(v6t,(dims[1],dims[0],dims[2],6))

    return u6t, v6t



