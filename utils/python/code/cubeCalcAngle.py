#function [AngleCS,AngleSN] = cubeCalcAngle(YG,RAC,dxG,dyG);
# [AngleCS,AngleSN] = cubeCalcAngle(YG,RAC,dxG,dyG);
#
# Determine rotation angle, alpha (returned as cos(alpha) and sin(alpha)),
# to convert cube sphere C-grid vectors to east-west and north-south
# components.
#
# e.g.
#
# >> XG=rdmds('XG');
# >> YG=rdmds('YG');
# >> RAC=rdmds('RAC');
# >> dxG=rdmds('dxG');
# >> dyG=rdmds('dyG');
# >> [AngleCS,AngleSN] = cubeCalcAngle(YG,RAC,dxG,dyG);
#

def cubeCalcAngle(YG,RAC,dxG,dyG):
    import numpy as np
    nc=np.shape(RAC)[1]
    deg2rad=np.pi/180
    r_earth=np.sqrt(np.sum(RAC)/(4.*np.pi))

    yZ6=split_Z_cub(YG)

    # Build purely zonal flow from a stream function, psi = r_earth * latitude.
    psi=-r_earth*yZ6*deg2rad
    uZ=psi[:nc,:nc,:]-psi[:nc,1:nc+1,:]
    vZ=psi[1:nc+1,:nc,:]-psi[:nc,:nc,:]
    uZ=uZ.transpose(0,2,1).reshape(6.*nc,nc,order='F')/dyG
    vZ=vZ.transpose(0,2,1).reshape(6.*nc,nc,order='F')/dxG

    # Put constructed zonal wind at cell center.
    uu,vv=split_UV_cub(uZ,vZ)
    u=(uu[:nc,:,:]+uu[1:nc+1,:,:])/2.
    v=(vv[:,:nc,:]+vv[:,1:nc+1,:])/2.
    uZc=u.transpose(0,2,1).reshape(6.*nc,nc,order='F')
    vZc=v.transpose(0,2,1).reshape(6.*nc,nc,order='F')

    # Calculate angles.
    norm=np.sqrt(uZc*uZc+vZc*vZc)
    AngleCS =  uZc/norm
    AngleSN = -vZc/norm

    return AngleCS, AngleSN

#function [z6t] = split_Z_cub(z3d)
# [z6t] = split_Z_cub(z3d)
#---------------------------------------------
# split 2d/3d arrays z3d to 2d/3d x 6 faces
# and add 1 column + 1 row
#  => output is z6t(nc+1,nc+1,[nr],6)
# either input is z3d(nc*6*nc+2,*): includes the 2 missing corners
#   or   input is z3d(nc*6,nc,*): => use the average value (from the 3
#                                 neighbours) for the 2 missing corners
#----------------------------------------------
# Written by jmc@ocean.mit.edu, 2005.
# $Header: /u/gcmpack/MITgcm/utils/matlab/cs_grid/split_Z_cub.m,v 1.1 2005/09/15 16:22:24 jmc Exp $
# $Name:  $

def split_Z_cub(z3d):
    import numpy as np
    dims=np.asarray(np.shape(z3d)) 
    ND=np.ndim(z3d)

    nc=np.fix((dims[0]-2)/6) 
    nc=np.fix(np.sqrt(abs(nc)))
    if dims[0] == nc*nc*6+2:
       nr=dims[1] 
       if ND > 2: 
          nr=dims[1]*dims[2]
          nx=6*nc 
          nPg=nx*nc 
          nPts=nPg+2 
          dims=np.append([nPts,1],dims[1:])
          z3d=np.reshape(z3d,(nPts,nr),order='F')
          zzC=z3d[nPg:nPg+2,:] 
          z3d=z3d[:nPg,:]
    elif dims[0] == 6*dims[1]:
       if ND == 2: 
          nr=1 
       else:
          nr=dims[2]
       nc=dims[1] 
       nx=6*nc 
       nPg=nx*nc 
       nPts=nPg+2
       zzC=np.zeros((2,nr))
    else:
       raise ValueError(' Error in split_Z_cub: bad input dimensions :'+ str(dims))

    #=================================================================

    z3d=np.reshape(z3d,(nc,6,nc,nr),order='F').transpose(0,2,3,1)
    ncp=nc+1 
    z6t=np.zeros((ncp,ncp,nr,6))

    #-- split on to 6 faces:
    z6t[:nc,:nc,:,:]=z3d.copy()

    #-- add overlap in i+1 & j+1 :
    z6t[nc,:nc,  :,0]=z3d[0,:nc,:,1].copy()
    z6t[nc,1:ncp,:,1]=z3d[nc::-1,0,:,3].copy()
    z6t[nc,:nc,  :,2]=z3d[0,:nc,:,3].copy()
    z6t[nc,1:ncp,:,3]=z3d[nc::-1,0,:,5].copy()
    z6t[nc,:nc,  :,4]=z3d[0,:nc,:,5].copy()
    z6t[nc,1:ncp,:,5]=z3d[nc::-1,0,:,1].copy()
 
    z6t[1:ncp,nc,:,0]=z3d[0,nc::-1,:,2].copy()
    z6t[:nc,  nc,:,1]=z3d[:nc,0,:,2].copy()
    z6t[1:ncp,nc,:,2]=z3d[0,nc::-1,:,4].copy()
    z6t[0:nc, nc,:,3]=z3d[:nc,0,:,4].copy()
    z6t[1:ncp,nc,:,4]=z3d[0,nc::-1,:,0].copy()
    z6t[0:nc, nc,:,5]=z3d[:nc,0,:,0].copy()   

    #----------------------------------------------------

    #-- missing corners :
    if dims[0] != nPts:
    #- use the average value (from the 3 neighbours)
       zzC[0,:]=z3d[0,nc-1,:,0]+z3d[0,nc-1,:,2]+z3d[0,nc-1,:,4]
       zzC[1,:]=z3d[nc-1,0,:,1]+z3d[nc-1,0,:,3]+z3d[nc-1,0,:,5]
       zzC=zzC/3
       print('split_Z_cub: fills 2 missing corners with local mean value')
    #- 1rst (nPg+1) = N.W corner of face 1
    z6t[0,nc,:,0]=zzC[0,:]
    z6t[0,nc,:,2]=zzC[0,:]
    z6t[0,nc,:,4]=zzC[0,:]
    #- 2nd  (nPg+2) = S.E corner of face 2
    z6t[nc,0,:,1]=zzC[1,:]
    z6t[nc,0,:,3]=zzC[1,:]
    z6t[nc,0,:,5]=zzC[1,:]

    if ND > 2:
       z6t=np.reshape(z6t,(ncp,ncp,dims[2],6),order='F')
    else:
       z6t=np.squeeze(z6t)

    return z6t


#function [u6t,v6t] = split_UV_cub(u3d,v3d,ksign,kad)
# [u6t,v6t] = split_UV_cub(u3d,v3d,[ksign,kad])
#---------------------------------------------
# split 2d/3d arrays u,v to 3d x 6 faces, and:
# (kad=1): add 1 column to U and one row to V
#  => output is u6t(nc+1,nc,[nr],6) & v6t(nc,nc+1,[nr],6)
# (kad=2): add 1 column + 2 rows to U and 2 columns + 1 row to V
#  => output is u6t(nc+1,nc+2,[nr],6) & v6t(nc+2,nc+1,[nr],6)
#----------------------------------------------
# Written by jmc@ocean.mit.edu, 2005.
# $Header: /u/gcmpack/MITgcm/utils/matlab/cs_grid/split_UV_cub.m,v 1.1 2005/09/15 16:22:24 jmc Exp $
# $Name:  $

def split_UV_cub(u3d,v3d,ksign=0,kad=1):

    import numpy as np

    # ksign = 0 ==> no sign ; ksign = 1 ==> change the sign where needed
    # ==> in fact, used only if kad=2
    plmn = 1 - 2*ksign

    dims=np.asarray(np.shape(u3d))
    if dims[0] != 6*dims[1]:
       print(' dimensions: '+str(dims)+' ; => STOP')
       raise ValueError(' ERROR in split_UV_cub: 1rst array has the wrong shape !')
    dims=np.asarray(np.shape(v3d)) 
    ND=np.ndim(v3d)
    if dims[0] != 6*dims[1]:
       print(' dimensions: '+str(dims)+' ; => STOP')
       raise ValueError(' ERROR in split_UV_cub:  2nd array has the wrong shape !')
    nx=dims[0]; nc=dims[1]; ncp=nc+1; n2p=nc+2;
    if ND == 2: 
       nr=1 
    else: 
       nr=dims[2]
    #=================================================================

    u3d=np.reshape(u3d,(nc,6,nc,nr),order='F').transpose(0,2,3,1)
    v3d=np.reshape(v3d,(nc,6,nc,nr),order='F').transpose(0,2,3,1)

    if kad == 0:
       #- split on to 6 faces with no overlap:
       dims[0]=nc 
       u6t=u3d.copy()
       v6t=v3d.copy()

    elif kad == 1:
       dims[0]=ncp
       #- split on to 6 faces with overlap in i+1 for u and j+1 for v :
       u6t=np.zeros((ncp,nc,nr,6))
       v6t=np.zeros((nc,ncp,nr,6))
       u6t[:nc,:,:,:]=u3d[:,:,:,:].copy()
       v6t[:,:nc,:,:]=v3d[:,:,:,:].copy()

       u6t[nc,:nc,:,0]=u3d[0,:nc,:,1].copy()
       u6t[nc,:nc,:,1]=v3d[nc::-1,0,:,3].copy()
       u6t[nc,:nc,:,2]=u3d[0,:nc,:,3].copy()
       u6t[nc,:nc,:,3]=v3d[nc::-1,0,:,5].copy()
       u6t[nc,:nc,:,4]=u3d[0,:nc,:,5].copy()
       u6t[nc,:nc,:,5]=v3d[nc::-1,0,:,1].copy()

       v6t[:nc,nc,:,0]=u3d[0,nc::-1,:,2].copy()
       v6t[:nc,nc,:,1]=v3d[:nc,0,:,2].copy()
       v6t[:nc,nc,:,2]=u3d[0,nc::-1,:,4].copy()
       v6t[:nc,nc,:,3]=v3d[:nc,0,:,4].copy()
       v6t[:nc,nc,:,4]=u3d[0,nc::-1,:,0].copy()
       v6t[:nc,nc,:,5]=v3d[:nc,0,:,0].copy()   

    elif kad == 2:
         dims[0]=ncp
         dims[1]=n2p
         #- split on to 6 faces:
         u6t=np.zeros((ncp,n2p,nr,6))
         v6t=np.zeros((n2p,ncp,nr,6))
         u6t[:nc,1:ncp,:,:]=u3d[:,:,:,:].copy()
         v6t[1:ncp,:nc,:,:]=v3d[:,:,:,:].copy()

         #- add overlap in i=nc+1 for u and j=nc+1 for v :
         u6t[nc,1:ncp,:,0]=u3d[0,:nc,:,1].copy()
         u6t[nc,1:ncp,:,1]=v3d[nc::-1,0,:,3].copy()
         u6t[nc,1:ncp,:,2]=u3d[0,:nc,:,3].copy()
         u6t[nc,1:ncp,:,3]=v3d[nc::-1,0,:,5].copy()
         u6t[nc,1:ncp,:,4]=u3d[0,:nc,:,5].copy()
         u6t[nc,1:ncp,:,5]=v3d[nc::-1,0,:,1].copy()

         v6t[1:ncp,nc,:,0]=u3d[0,nc::-1,:,2].copy()
         v6t[1:ncp,nc,:,1]=v3d[:nc,0,:,2].copy()
         v6t[1:ncp,nc,:,2]=u3d[0,nc::-1,:,4].copy()
         v6t[1:ncp,nc,:,3]=v3d[:nc,0,:,4].copy()
         v6t[1:ncp,nc,:,4]=u3d[0,nc::-1,:,0].copy()
         v6t[1:ncp,nc,:,5]=v3d[:nc,0,:,0].copy()   

         #- add overlap in j=0,nc+1 for u and i=0,nc+1 for v :
         u6t[:ncp, 0 ,  :,0]=u6t[:ncp,nc,:,5].copy()
         u6t[:ncp,n2p-1,:,0]=v6t[ 1 ,ncp::-1,:,2]*plmn
         u6t[:ncp, 0 ,  :,2]=u6t[:ncp,nc,:,1].copy()
         u6t[:ncp,n2p-1,:,2]=v6t[ 1 ,ncp::-1,:,4]*plmn
         u6t[:ncp, 0 ,  :,4]=u6t[:ncp,nc,:,3].copy()
         u6t[:ncp,n2p-1,:,4]=v6t[ 1 ,ncp::-1,:,0]*plmn

         u6t[:ncp, 0 ,  :,1]=v6t[nc,ncp::-1,:,5]*plmn
         u6t[:ncp,n2p-1,:,1]=u6t[:ncp, 1 ,:,2].copy()
         u6t[:ncp, 0 ,  :,3]=v6t[nc,ncp::-1,:,1]*plmn
         u6t[:ncp,n2p-1,:,3]=u6t[:ncp, 1 ,:,4].copy()
         u6t[:ncp, 0 ,  :,5]=v6t[nc,ncp::-1,:,3]*plmn
         u6t[:ncp,n2p-1,:,5]=u6t[:ncp, 1 ,:,0].copy()

         v6t[ 0 ,  :ncp,:,0]=u6t[ncp::-1,nc,:,4]*plmn
         v6t[n2p-1,:ncp,:,0]=v6t[ 1 ,:ncp,:,1].copy()
         v6t[ 0 ,  :ncp,:,2]=u6t[ncp::-1,nc,:,0]*plmn
         v6t[n2p-1,:ncp,:,2]=v6t[ 1 ,:ncp,:,3].copy()
         v6t[ 0 ,  :ncp,:,4]=u6t[ncp::-1,nc,:,2]*plmn
         v6t[n2p-1,:ncp,:,4]=v6t[ 1 ,:ncp,:,5].copy()

         v6t[ 0 ,  :ncp,:,1]=v6t[nc,:ncp,:,0].copy()
         v6t[n2p-1,:ncp,:,1]=u6t[ncp::-1, 1 ,:,3]*plmn
         v6t[ 0 ,  :ncp,:,3]=v6t[nc,:ncp,:,2].copy()
         v6t[n2p-1,:ncp,:,3]=u6t[ncp::-1, 1 ,:,5]*plmn
         v6t[ 0 ,  :ncp,:,5]=v6t[nc,:ncp,:,4].copy()
         v6t[n2p-1,:ncp,:,5]=u6t[ncp::-1, 1 ,:,1]*plmn


    #- restaure the right shape:
    if ND == 2:
      u6t=np.squeeze(u6t)
      v6t=np.squeeze(v6t)
    else:
      u6t=np.reshape(u6t,(dims[0],dims[1],dims[2],6))
      v6t=np.reshape(v6t,(dims[1],dims[0],dims[2],6))

    return u6t, v6t

