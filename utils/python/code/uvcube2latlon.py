# ui,vi=cube2latlon(u,v,xc,yc,xg,yg,rac,dxg,dyg,xi,yi)
#
# Re-grids model output on expanded spherical cube to lat-lon grid.
#  u,v   is a 2-D or 3-D horizontal components of model flow fields.
#  xc,yc   are 2-D arrays of the cell-centered coordinates 
#  xg,yg   are 2-D arrays of the cell-edge coordinates
#  dxg,dyg are 2-D arrays of the edge-edge length in x and y
#  rac     is 2-D array of cell-centered area
#  xi,yi are vectors of the new regular lat-lon grid to interpolate to.
#  ui,vi are the flow fields with dimensions of size(xi) x size(yi) size(u,3).
#       they are on 'A' grid as tracers
#
# modified using matlab script provided by adcroft@.mit.edu, 2001.

def uvcube2latlon(u,v,xc,yc,xg,yg,rac,dxg,dyg,xi=[],yi=[]):
    import numpy as np

    AngleCS,AngleSN = cubeCalcAngle(yg,rac,dxg,dyg)

    U,V=rotate_uv2uvEN(u,v,AngleCS,AngleSN,'A')

    U=cube2latlon_scalar(xc,yc,U,xi,yi)
    V=cube2latlon_scalar(xc,yc,V,xi,yi)

    return U, V, xi, yi

#function [uE,vN] = rotate_uv2uvEN(u,v,AngleCS,AngleSN,Grid)
# [uE,vN] = rotate_uv2uvEN(u,v,AngleCS,AngleSN,Grid)
#
# Rotate cube sphere U and V vector components to east-west (uE) and
# north-south (vN) components located on cube sphere grid centers.
#
# Incoming u and v matricies are assumed to be cube sphere A-grid or C-grid
# vector fields (defaut is C-grid) where the first two dimensions are (6*nc
# nc), where nc is the cube face resolution.  There may up to 4 additional
# dimensions (likely z and time, trials, etc.) beyond this.
#
# e.g.
#
# >> [uE,vN] = rotate_uv2uvEN(uC,vC,AngleCS,AngleSN,'C');
#
# >> [uE,vN] = rotate_uv2uvEN(uA,vA,AngleCS,AngleSN,'A');

# $Header: /u/gcmpack/MITgcm/utils/matlab/cs_grid/rotate_uv2uvEN.m,v 1.2 2006/02/23 14:04:12 jmc Exp $
# $Name:  $

# Default is a C-grid configuration.

def rotate_uv2uvEN(u,v,AngleCS,AngleSN,Grid='C'):
   
    import numpy as np
    # Verify dimensions are that of cube-sphere
    if np.shape(u)[0] != 6.*np.shape(u)[1] or np.shape(v)[0] != 6.*np.shape(v)[1]:
       raise ValueError('Error in CS-dimensions: '+'u = '+str(np.shape(u)) \
                                            + ', '+'v = '+str(np.shape(v)))

    # Parse dimension information, flatten extra dimensions. 
    if np.ndim(u)==2:
       # exends 2D u and v to 3D
       dim2=np.asarray(np.shape(u))
       u=np.reshape(u,(np.append(dim2,1)),order='F')
       v=np.reshape(v,(np.append(dim2,1)),order='F')
    dim=np.asarray(np.shape(u)) 
    nc=dim[1]
    nz=dim[2]
    u=np.reshape(u,(6*nc,nc,nz),order='F')
    v=np.reshape(v,(6*nc,nc,nz),order='F')

    # Do simple average to put u,v at the cell center (A-grid) as needed.
    if Grid=='A':
       u=np.reshape(u,(6*nc*nc,nz),order='F')
       v=np.reshape(v,(6*nc*nc,nz),order='F')
    elif Grid=='C':
       uu,vv = split_UV_cub(u,v)
       uu=np.reshape(uu,(nc+1,nc,nz,6),order='F')
       vv=np.reshape(vv,(nc,nc+1,nz,6),order='F')
       u=(uu[:nc,:,:,:]+uu[1:nc+1,:,:,:])/2.
       v=(vv[:,:nc,:,:]+vv[:,1:nc+1,:,:])/2.
       u=u.transpose(0,3,1,2).reshape(6*nc*nc,nz,order='F')
       v=v.transpose(0,3,1,2).reshape(6*nc*nc,nz,order='F')
    else:
       raise ValueError('Unrecognized grid type: '+Grid)

    # Make rotation to find uE, vN.
    uE=np.nan*np.zeros((6*nc*nc,nz))
    vN=np.nan*np.zeros((6*nc*nc,nz))
    AngleCS=AngleCS.reshape(6*nc*nc,order='F')
    AngleSN=AngleSN.reshape(6*nc*nc,order='F')
    for k in range(nz):
        uE[:,k]=AngleCS[:]*u[:,k]-AngleSN[:]*v[:,k]
        vN[:,k]=AngleSN[:]*u[:,k]+AngleCS[:]*v[:,k]
    uE = uE.reshape(dim,order='F')
    vN = vN.reshape(dim,order='F')

    return uE, vN

