#function [z] = cube2latlon(x,y,c,xi,yi,varargin)
# z=cube2latlon(x,y,c,xi,yi);
#
# Re-grids model output on expanded spherical cube to lat-lon grid.
#  x,y   are 2-D arrays of the cell-centered coordinates 
#  c     is a 2-D or 3-D scalar field
#  xi,yi are vectors of the new regular lat-lon grid to interpolate to.
#  z     is the interpolated data with dimensions of size(xi) by size(yi).
#
# e.g.
# >> x=rdmds('XC');
# >> y=rdmds('YC');
# >> t=rdmds('Ttave.0000513360');
# >> xi=-179:2:180;yi=-89:2:90;
# >> ti=cube2latlon(x,y,t,xi,yi);
#
# Written by adcroft@.mit.edu, 2001.
# $Header: /u/gcmpack/MITgcm/utils/matlab/cs_grid/cube2latlon.m,v 1.1 2005/09/15 20:04:56 jmc Exp $
# $Name:  $

def cube2latlon(x,y,c,xi,yi):
    import numpy as np
    NN=c.shape
    if np.ndim(c)==2:
       nx,ny=c.shape
    elif np.ndim(c)==3: 
       nx,ny,nz=c.shape

    X=np.ravel(x)
    Y=np.ravel(y)
    ig=np.where(X> 90)
    il=np.where(X<-90)
    delx=griddata_preprocess(np.append(np.append(Y,Y[il]),Y[ig]), \
                            np.append(np.append(X,X[il]+360.),X[ig]-360.), \
                            yi,xi.transpose())

    if np.ndim(c)==2:
       C=np.reshape(c,(1,nx*ny),order='F')
       z=griddata_fast(delx,np.append(np.append(C,C[il]),C[ig]))
    elif np.ndim(c)==3:
       z1d=[]
       for k in range (nz):
           C=np.reshape(c[:,:,k],(1,nx*ny),order='F')
           z2d=griddata_fast(delx,np.append(np.append(C,C[il]),C[ig]))
           z1d=np.append(z1d,z2d.reshape(z2d.size,order='F'))
       # len(z1d)=z2d.size*nz
       z=z1d.reshape(z2d.size,nz,order='F').reshape(z2d.shape[0],z2d.shape[1],nz,order='F')

    return z

# function [del] = griddata_preprocess(x,y,xi,yi,method)
# GRIDDATA_PREPROCESS Pre-calculate Delaunay triangulation for use
#   with GRIDDATA_FAST.
#
#   DEL = GRIDDATA_PREPROCESS(X,Y,XI,YI)

#   Clay M. Thompson 8-21-95
#   Copyright 1984-2001 The MathWorks, Inc. 
#   $Revision: 1.1.1.1 $  $Date: 2003/11/11 18:08:08 $

def griddata_preprocess(x,y,xi,yi):
    import numpy as np
    lx, ly =np.meshgrid(x, y )
    lxi,lyi=np.meshgrid(xi,yi)

    delx = linear4(lx,ly,lxi,lyi)
   
    return delx

# function delau = linear(x,y,xi,yi)
# LINEAR Triangle-based linear interpolation

#   Reference: David F. Watson, "Contouring: A guide
#   to the analysis and display of spacial data", Pergamon, 1994.

def linear4(x,y,xi,yi):
    import numpy as np
    from matplotlib import delaunay
    import matplotlib.tri as mtri
    siz = np.shape(xi)
    # Treat these as columns
    xi = np.ravel(xi)
    yi = np.ravel(yi)
    x  = np.ravel(x )
    y  = np.ravel(y )

    # Triangularize the data
    circumcenters, edges, tri_points, tri_neighbors = delaunay(x, y)
    if not tri_points:
       exit('Data cannot be triangulated.')


    # Only keep the relevant triangles.
    out = np.where(np.isnan(t))
    if not not out: 
       # t is the index of array 
       t[out] = np.zeros(np.shape(out))
    tri = tri[t,:]

    # Compute Barycentric coordinates (w).  P. 78 in Watson.
    delx = (x[tri[:,1]]-x[tri[:,0]]) * (y[tri[:,2]]-y[tri[:,0]]) - \
           (x[tri[:,2]]-x[tri[:,0]]) * (y[tri[:,1]]-y[tri[:,0]])
    w=np.zeros( (np.size(delx),3) )
    w[:,2] = ((x[tri[:,0]]-xi) * (y[tri[:,1]]-yi) - \
              (x[tri[:,1]]-xi) * (y[tri[:,0]]-yi)) / delx
    w[:,1] = ((x[tri[:,2]]-xi) * (y[tri[:,0]]-yi) - \
              (x[tri[:,0]]-xi) * (y[tri[:,2]]-yi)) / delx
    w[:,0] = ((x[tri[:,1]]-xi) * (y[tri[:,2]]-yi) - \
              (x[tri[:,2]]-xi) * (y[tri[:,1]]-yi)) / delx
    w[out,:] = np.zeros((len(out),3))

    class delau:
          tri=tri
          w=w
          siz=siz
          out=out

    return delau

#function zi = griddata_fast(delau,z,method)
#GRIDDATA_FAST Data gridding and surface fitting.
#   ZI = GRIDDATA_FAST(DEL,Z)
#
#   See also GRIDDATA_PREPROCESS

#   Clay M. Thompson 8-21-95
#   Copyright 1984-2001 The MathWorks, Inc. 
#   $Revision: 1.1.1.1 $  $Date: 2003/11/11 18:08:08 $

def griddata_fast(delau,z):
    import numpy as np

    zi = linear2(delau,z)

    return zi

# function zi = linear(delx,z)
# LINEAR Triangle-based linear interpolation

#   Reference: David F. Watson, "Contouring: A guide
#   to the analysis and display of spacial data", Pergamon, 1994.

def linear2(delau,z):
    import numpy as np
    z = np.reshape(z, np.size(z), order='F')
    # Treat z as a row so that code below involving
    # z(tri) works even when tri is 1-by-3.
    zi = np.sum(z[delau.tri] * delau.w,1)

    zi = np.reshape(zi,delau.siz,order='F')

    if not not delau.out 
       zi[delau.out] = np.nan

    return zi
