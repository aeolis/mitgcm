# This is the routine to calculate Ls based on planet orbital properties
# Usage:  ls, year = calc_planet_ls(basetime, niter, dt, planet)
def calc_planet_ls(basetime, niter, dt, planet):

    # bastime: base time to start the new simulation in seconds
    # niter:   Current iteration number
    # dt:      Time Step in seconds

    import numpy as np

    # define some constants for calculating the solar angle l_s
    if planet.lower() == 'Titan'.lower():
       p2si=15.95
       equinox_fraction= 0.2098
       planet_year=686.0
       zero_date=542.06
       eccentricity=0.056
       semimajoraxis= 9.58256

    if planet.lower() == 'Mars'.lower():
       p2si=1.0274912
       equinox_fraction= 0.2695
       planet_year=669.0
       zero_date=488.7045
       eccentricity=0.09341233
       semimajoraxis= 1.52366231

    SMALL_VALUE=1.0e-6
    deleqn = equinox_fraction * planet_year
    qq = 2.0 * (np.pi * deleqn / planet_year)
    er = np.sqrt( (1.0+eccentricity)/(1.0-eccentricity) )
    #  determine true anomaly at equinox:  eq
    #  Iteration for eq
    e = 1.0
    cd0 = 1.0
    while (cd0 > SMALL_VALUE):
       ep = e - (e-eccentricity*np.sin(e)-qq) / (1.0-eccentricity*np.cos(e))
       cd0 = abs(e-ep)
       e = ep

    eqx = 2.0 * np.arctan( er * np.tan(0.5*e) )
    
    
    # calculate ls -------
    julian=(basetime+niter*dt)/(86400*p2si)
    date_dbl = julian - zero_date

    while (date_dbl < 0.):
          date_dbl=date_dbl+planet_year

    while (date_dbl > planet_year):
          date_dbl=date_dbl-planet_year

    #  determine true anomaly at current date:  w
    #  Iteration for w
    em = 2.0 * np.pi * date_dbl/planet_year
    e  = 1.0
    cd0 = 1.0
    while (cd0 > SMALL_VALUE):
      ep = e-(e-eccentricity*np.sin(e)-em) / (1.0-eccentricity*np.cos(e))
      cd0 = abs(e-ep)
      e = ep

    w = 2.0 * np.arctan( er * np.tan(0.5*e) )
     
    #  Radius vector ( astronomical units:  AU )
    als= (w - eqx)*180.0/np.pi      #Aerocentric Longitude
    if (als < 0.0): 
        als=als+360.0

    ls = als
    year=julian/planet_year

    print 'Planet is : ' + planet
    print 'Solar longitude Ls = ' + '%3.3f'%(ls)
    print 'Planet year is = ' + '%3.3f'%(year)

    return ls, year

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(prog='python calc_plant_ls.py', \
             formatter_class=argparse.RawDescriptionHelpFormatter)
    # use nargs='?' for optional variables
    # This returns a single variable, not a list obtained fomr e.g., nargs=1
    # Using nargs=1 then n, m are lists, so args.n[0] is needed
    parser.add_argument('-basetime','--basetime', nargs='?',type=float, default=0., \
                        help='Base time to calculate Ls. It may be non-zero if time \
                        step is changed during simulation. Default is 0.')
    parser.add_argument('-niter','--niter', nargs='?',type=float, default=0., \
                        help='Number of iteration at time step DT. Default is 0')
    parser.add_argument('-dt','--dt', nargs='?',type=float, default=0., \
                        help='Time step in seconds. Default is 0.')
    parser.add_argument('-planet','--planet', nargs='?',type=str, default='Mar', \
                        help='Choose a planet.  Default is Mars.')

    args = parser.parse_args()

    calc_planet_ls(args.basetime, args.niter, args.dt, args.planet)
