#function [] = merccube(XX,YY,C,opt_arg)
# merccube(x,y,c,'k') to display cube-sphere colormap with black lines
#
# Plots cubed-sphere data in mercator projection. (x,y) are
# coordinates, c is cell-centered scalar to be plotted.
# All arrays (x,y,c) should have dimensions of NxNx6 or 6NxN.
#
# The default plotting mode is shading faceted. Using this or
# shading flat, (x,y) should be the coordinates of grid-corners
# and can legitimately have dimension (N+1)x(N+1)x6.
#
# If using shading interp, then (x,y) must be the coordinates of
# the cell centers.
#
# e.g.
#
# xg=rdmds('XG');
# yg=rdmds('YG');
# ps=rdmds('Eta.0000000000');
# mercube(xg,yg,ps);
# colorbar;xlabel('Longitude');ylabel('Latitude');
#
# xc=rdmds('XC');
# yc=rdmds('YC');
# mercube(xc,yc,ps);shading interp
#
# Written by adcroft@.mit.edu, 2004.
# $Header: /u/gcmpack/MITgcm/utils/matlab/cs_grid/merccube.m,v 1.2 2005/11/21 21:57:31 enderton Exp $
# $Name:  $

def merccube(XX,YY,C,lc='None'):

    import numpy as np
    import matplotlib.pyplot as plt

    if np.nanmax(YY)-np.nanmin(YY) < 3.*np.pi:
       X=XX*180/np.pi
       Y=YY*180/np.pi
    else:
       X=XX.copy()
       Y=YY.copy()

    Q=C.copy()

    if np.ndim(Q)==2 and Q.shape[0]==6*Q.shape[1]:
       nx,ny=X.shape
       X=X.reshape(nx/6,6,ny,order='F').transpose(0,2,1)
       Y=Y.reshape(nx/6,6,ny,order='F').transpose(0,2,1)
       Q=Q.reshape(nx/6,6,ny,order='F').transpose(0,2,1)
    elif np.ndim(Q)==3 and Q.shape[1]==6:
       X=X.transpose(0,2,1)
       Y=Y.transpose(0,2,1) 
       Q=Q.transpose(0,2,1) 
    elif np.ndim(Q)==3 and Q.shape[2]==6:
       nx,ny,nt=X.shape
    else:
       print(XX.shape,'\n',YY.shape,'\n',C.shape)       
       exit('Dimensions should be 2 or 3 dimensions: NxNx6, 6NxN or Nx6xN')

    if X.shape[0]==Q.shape[0]:
       X=np.concatenate((X,X[[-1],:,:]),axis=0); X[-1,:,:]=np.nan
       X=np.concatenate((X,X[:,[-1],:]),axis=1); X[:,-1,:]=np.nan
       X[-1,:,[0,2,4]]=X[0,:,[1,3,5]]
       X[:,-1,[1,3,5]]=X[:,0,[2,4,0]]
       X[:,-1,[0,2,4]]=X[0,::-1,[2,4,0]].transpose()
       X[-1,:,[1,3,5]]=X[::-1,0,[3,5,1]].transpose()
       X[0,-1,[0,2,4]]=X[0,0,0]
       X[-1,0,[1,3,5]]=X[-1,-1,1]
       Y=np.concatenate((Y,Y[[-1],:,:]),axis=0); Y[-1,:,:]=np.nan
       Y=np.concatenate((Y,Y[:,[-1],:]),axis=1); Y[:,-1,:]=np.nan
       Y[-1,:,[0,2,4]]=Y[0,:,[1,3,5]]
       Y[:,-1,[1,3,5]]=Y[:,0,[2,4,0]]
       Y[:,-1,[0,2,4]]=Y[0,::-1,[2,4,0]].transpose()
       Y[-1,:,[1,3,5]]=Y[::-1,0,[3,5,1]].transpose()
       Y[0,-1,[0,2,4]]=Y[-1,-1,0]
       Y[-1,0,[1,3,5]]=Y[0,0,1]

    nx,ny,nt=X.shape

    Q=np.concatenate((Q,Q[[-1],:,:]),axis=0); Q[-1,:,:]=np.nan
    Q=np.concatenate((Q,Q[:,[-1],:]),axis=1); Q[:,-1,:]=np.nan
    Q[-1,:,[0,2,4]]=Q[0,:,[1,3,5]]
    Q[:,-1,[1,3,5]]=Q[:,0,[2,4,0]]
    Q[:,-1,[0,2,4]]=Q[0,::-1,[2,4,0]].transpose()
    Q[-1,:,[1,3,5]]=Q[::-1,0,[3,5,1]].transpose()

    hnx=int(np.ceil(nx/2))
    hny=int(np.ceil(ny/2))

    plt.axis([-180,180,-90,90])
    plt.hold(True)
    cmax=np.nanmax(Q)
    cmin=np.nanmin(Q)

    for k in range (6):
        i=np.arange(hnx+1) 
        x=longitude(X[i,:,k])
        plt.pcolor(x,Y[i,:,k],Q[i,:,k],vmin=cmin,vmax=cmax,edgecolors=lc)
        if np.nanmax(x)>180.:
           plt.pcolor(x-360.,Y[i,:,k],Q[i,:,k],vmin=cmin,vmax=cmax,edgecolors=lc)
        i=np.arange(hnx,nx)
        x=longitude(X[i,:,k])
        plt.pcolor(x,Y[i,:,k],Q[i,:,k],vmin=cmin,vmax=cmax,edgecolors=lc)
        if np.nanmax(x)>180.:
           plt.pcolor(x-360.,Y[i,:,k],Q[i,:,k],vmin=cmin,vmax=cmax,edgecolors=lc)
    plt.hold(False)

    return

#function [X] = longitude(x)
# X=longitude(x);
#
# tries to determine best range of longitude (e.g. -180<180 or 0<360)
# so that coordinate (x) doesn't span a discontinuity.
#

# also works for radians which are assumed if range of x<=2*pi

def longitude(x):
    import numpy as np
    #minx=np.nanmin(x)
    #maxx=np.nanmax(x)
    units=1.
    #minx=np.nanmin(x*units)
    #maxx=np.nanmax(x*units)

    X=np.mod(720.+x*units,360.)
    maxP=np.nanmax(X)
    minP=np.nanmin(X)

    XX=np.mod(X+180.,360.)-180.
    maxM=np.nanmax(XX)
    minM=np.nanmin(XX)

    if maxP-minP > maxM-minM:
       X=XX.copy()

    return X
