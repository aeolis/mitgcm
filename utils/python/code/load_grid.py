import rdmds as rd
import numpy as np

def load_xy(dir):
	class xy:
		pass
	# load x and y coordinate
	xy.xc=rd.rdmds(dir,'XC')
	xy.yc=rd.rdmds(dir,'YC')
	xy.xg=rd.rdmds(dir,'XG')
	xy.yg=rd.rdmds(dir,'YG')
	# load area
	xy.rac=rd.rdmds(dir,'RAC')

	return xy

def load_zp(dir, vgrid='pressure'):
	class zgrid:
		pass
	# load pressure 
	zgrid.rc=rd.rdmds(dir,'RC')
	zgrid.rf=rd.rdmds(dir,'RF') 
	zgrid.rc=np.squeeze(zgrid.rc)
	zgrid.rf=np.squeeze(zgrid.rf)
	
	if (vgrid == 'sigma'):
		zgrid.sigC=rd.rdmds(dir,'BHybSigC')
		zgrid.sigF=rd.rdmds(dir,'BHybSigF');
		zgrid.aHybC=rd.rdmds(dir,'AHybSigC'); 
		zgrid.aHybF=rd.rdmds(dir,'AHybSigF'); 

		zgrid.sigC=np.squeeze(zgrid.sigC)
		zgrid.sigF=np.squeeze(zgrid.sigF)
		zgrid.aHybC=np.squeeze(zgrid.aHybC)
		zgrid.aHybF=np.squeeze(zgrid.aHybF)
	return zgrid


