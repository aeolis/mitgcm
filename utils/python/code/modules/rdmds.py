# this is a simple version of rdmds to read 2-D or 
# 3-D or 4-D data from MITgcm outputs. This only works for 
# outputs from useSingleCPUIO=.TRUE. ( including single
# and multiple-record files)
def rdmds(direc, fname, rec=[]):

    from sys import byteorder

    import numpy as np
    import re
    import struct

    fn_meta=direc+'/'+fname+'.meta'

    f=open(fn_meta)
    lines=f.readlines()
    f.close()

    #numbers=[int(s) for s in strs1.split() if s.isdigit()]

    #nend=len(numbers)-1

    # find dimension of the data

    for i, item in enumerate(lines):
        if lines[i].lower().find('ndims')==1:
           nDims=[int(s) for s in lines[i].replace(",","").replace("\n","").split() if s.isdigit()]
        elif lines[i].lower().find('dimlist')==1:
           kstart=i
        elif lines[i].lower().find('dataprec')==1:
           print(lines[i])
           kend=i
        elif lines[i].lower().find('nrecords')==1:
           nrecords=[int(s) for s in lines[i].replace(",","").replace("\n","").split() if s.isdigit()]
       
    dimlist=''.join(lines[kstart:kend])
    dims=[int(s) for s in dimlist.replace(",","").replace("\n","").split() if s.isdigit()]
    dims=np.asarray(dims)

    nDims=nDims[0]
    nrecords=nrecords[0]
    
    # 2-D data
    if np.size(dims)==6:
       dims=dims.reshape(2,3)
       nx=dims[0,0]; ny=dims[1,0]
    if np.size(dims)==9:
       dims=dims.reshape(3,3)
       nx=dims[0,0]; ny=dims[1,0]; nz=dims[2,0]

    if nrecords > 1:
       if not rec:
          print('data=rdmds(direc, fname, rec=[])')
          raise ValueError('Accessing multiple-record data: A record number is required')
 
    # if rec is not empty, check if rec > nrecords
    if rec:
       if rec > nrecords:
          print('Data record length: nrecords = '+str(nrecords))
          raise ValueError('requested record number is larger than the record length')

# read binary file
    fn_data=direc+'/'+fname+'.data'

#    with open("myfile", "rb") as f:
#    byte = f.read(1)
#    while byte != "":
#        # Do stuff with byte.
#        byte = f.read(1)
#    with open("myfile", "rb") as f:
#         fileContent = f.read()

#    finally:
#        f.close()

    with open(fn_data, "rb") as f:
         # Can use data=np.fromfile(f,'>f8') to 
         # force big endian ('>')
         # >f8 big endian, double precision
         # <f8 little endian, double precision
         # ~f8 native, double precision
         #data=np.fromfile(f,'d')
         # 2-D
         if nDims == 2 and nrecords==1:
            data=np.fromfile(f,'d')
            # a naive way to check data (not system) endianness
            if np.isnan(np.max(data.byteswap())) or np.max(data.byteswap())>1.e99:
               # no need to swap endianness
               data=data.reshape(nx,ny, order='F')
            else:
               # swap endianness
               data=data.byteswap().reshape(nx,ny, order='F')
         # 3-D
         elif nDims == 3 and nrecords==1:
            data=np.fromfile(f,'d')
            if np.isnan(np.max(data.byteswap())) or np.max(data.byteswap())>1.e99:
               data=data.reshape(nx,ny,nz, order='F')
            else:
               data=data.byteswap().reshape(nx,ny,nz, order='F')
         # 4-D
         elif rec and nrecords > 1:
            if nDims==2:
               # skip the data before requested record (8 bytes for double precision data)
               f.seek(int(nx*ny*(rec-1)*8))
               rawdata=f.read(int(nx*ny*8))
               # MITgcm data is big endian (by default, except 2-D data)
               data=struct.unpack(">%dd"%(int(nx*ny)),rawdata)
               data=np.reshape(data,(nx,ny),order='F')
            elif nDims==3:
               # skip the data before requested record (8 bytes for double precision data)
               f.seek(int(nx*ny*nz*(rec-1)*8))
               rawdata=f.read(int(nx*ny*nz*8))
               # MITgcm data is big endian (by default, except 2-D data)
               data=struct.unpack(">%dd"%(int(nx*ny*nz)),rawdata)
               data=np.reshape(data,(nx,ny,nz),order='F')
         
    return data
