# this is the python script to load all single-record files in a directory
# following the pattern "var.0000000000.data"
# var = name of dynamical variables (T etc.)
# ext = file extensions ('data')
# lev = vertical level to be imported

def load_timeserie(dir,var,ext,lev):
    import os
    import numpy as np
    
    # enter the data directory
    os.chdir(dir)
    
    all_files=[]
    for files in os.listdir("."):
        if files.startswith(var) and files.endswith('.'+ext):
            all_files.append(files)
            
    niter = []
    
    for line in x:
        niter.append(line.replace("."," ").split()[1])
        
    var3d=rdmds('.',var+'.'+niter[0])
    dyn3d=np.squeeze(var3d[:,:,lev-1])
    nx=dyn3d.shape[0]; ny=dyn3d.shape[1];
    for line in niter[1:]:
        var3d=rdmds.rdmds('.',var+'.'+line)
        dyn3d=np.append(dyn3d,np.squeeze(var3d[:,:,lev-1]),axis=1)
        
    dyn3d=dyn3d.reshape(nx,ny,len(niter))

    return dyn3d
