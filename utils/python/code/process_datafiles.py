class metadata:

	def __init__(self, dir):
	    self.dir = dir
	    self.nIter0 = ''
	    self.nTimeSteps = ''
	    self.deltaT = ''
	    self.dumpFreq = ''
	    self.nDims = ''
	    self.nrecords = ''
	    self.timeStepNumber = ''

	def readdata(self):
	    # open data file to process
	    f=open(self.dir+'/'+'data','r')
	    allparams=f.readlines()
	    f.close()

	    for line in allparams:
		# find parameters of interest
		if (line[0] != '#'):
			# niter0
			if ( 'niter0' in line.lower() ):
			   self.nIter0=int(line.replace(',','').replace('\n','').split('=')[1])
			# nTimeSteps
			if ( 'ntimesteps' in line.lower() ):
                           self.nTimeSteps=int(line.replace(',','').replace('\n','').split('=')[1])
			# deltaT
			if ( 'deltat' in line.lower() ):
                           self.deltaT=float(line.replace(',','').replace('\n','').split('=')[1])
			# dumpFreq
			if ( 'dumpfreq' in line.lower() ):
                           self.dumpFreq=float(line.replace(',','').replace('\n','').split('=')[1])

	def readmeta(self):
	    # open meta file to process
	    f=open(self.dir+'/'+'U.meta','r')
	    allparams=f.readlines()
	    f.close()

	    for line in allparams:
		if (line[0] != '#'):
			# nDims
			if ( 'ndims' in line.lower() ):
			   self.nDims=[int(s) for s in line.split() if s.isdigit()][0]
			# nrecords
			if ( 'nrecords' in line.lower() ):
                           self.nrecords=[int(s) for s in line.split() if s.isdigit()][0]
			# timeStepNumber
			if ( 'timestepnumber' in line.lower() ):
			   self.timeStepNumber=[int(s) for s in line.split() if s.isdigit()][0]

	def gen_alldata(self):
	     # call function to get meta data
	    self.readdata()
	    self.readmeta()
	    

	
	
	
