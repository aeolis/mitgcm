#nlay=input('layer number: ');
#pb=input('bottom pressure [pa] :  ');

#planet=input('Jupiter (1), Saturn (2) : ');

# some gas constant in SI units
import numpy as np

def jup_z_profile(planet):
    
    cp=13000.
    kappa=0.29
    Rd=cp*kappa
    # reference pressure in pascal
    Po=1.E5 

    if (planet==1): 
        gravity=22.88
        # Known P-T points in bars 
        p_tropo=0.1 
        p_1mbar=0.02
        p_1bar=0.5  
        T_1bar=165.13*(0.5e5/Po)**kappa # T at 1bar is 165.13
        T_tropo=110
    else:
        gravity=10.44
        p_tropo=0.1
        p_1mbar=0.02
        p_1bar=0.5
        T_1bar=134.0*(0.5e5/Po)^kappa;
        T_tropo=85

    # find the p_top in range of the following two limits
    p_top_lo =3000.  # in Pa
    p_top_hi =2000.  # in Pa

    # properties of condensible gases
    Runiv=8314

    H2O_params = np.zeros(8)
    H2O_params[0]=18.0;       # molecular mass
    H2O_params[1]=Runiv/18.0; # gas constant 
    H2O_params[2]=609.140;    # reference pressure
    H2O_params[3]=273.0;      # reference temperature
    H2O_params[4]=3148.0e3;   # latent heat
    H2O_params[5]=25.096;     # ln(C)
    H2O_params[6]=0.0;        # delta(alpha)
    H2O_params[7]=-8.7e-3*2;  # delta(beta)

    NH3_params = np.zeros(8)
    NH3_params[0]=17.0;       # molecular mass
    NH3_params[1]=Runiv/17.0; # gas constant 
    NH3_params[2]=4.29e5;     # reference pressure
    NH3_params[3]=273.0;      # reference temperature
    NH3_params[4]=2016.0e3;   # latent heat 
    NH3_params[5]=27.863;     # ln(C)
    NH3_params[6]=-0.888;     # delta(alpha)
    NH3_params[7]=0.0;        # delta(beta)

    mH2=2;

    # first find the T-P profile in high resolution between 0.001 to 100bars
    # T=a( log(p)-log(p_tropo) )^2 + b, sysmetric with respective to 0.1 bar
    # T=110K at p=p_tropo=0.1bar, so b=110K
    # and dT/d(log(p)) = RT/cp at p=1bar <== dp/dz=-rho*g and dT/dz=-g/cp
    # ==> a = -kappa*T_1bar/(2*log(p_tropo)) where T_1bar=165K
    pmin=0.001; pmax=200.; n_hires=2500;
    dlogp=(np.log(pmax)-np.log(pmin))/n_hires

    pa=np.zeros(n_hires+1)
    pa[0]=0.001 # pressure at the top of the atmosphere (in bar)
    # integrated downwards to find all pressures (linearly spaced in log(p))
    for k in range (n_hires):  
        pa[k+1]=pa[k]*np.exp(dlogp) 

    # find the indicies at 1mbar and 1bar
    n_1mbar=np.max(np.where(pa < p_1mbar))
    n_1bar =np.max(np.where(pa < p_1bar))

    parabolic_shap=1.
    Tp_1bar=T_1bar+1.

    Tp=np.zeros(n_hires+1)
    while (abs(Tp_1bar-T_1bar) > 0.1):
        parabolic_shap=parabolic_shap+0.001
        ac=kappa*T_1bar/(parabolic_shap*(np.log(p_1bar)-np.log(p_tropo)))

        # 0.001bar to 0.01bar
        Tp[:n_1mbar+1]= (  ( np.tanh( ((np.log(p_1mbar))-np.log(pa[:n_1mbar+1]))/0.3 )  \
                     - np.tanh( ((np.log(p_1mbar))-np.log(pa[n_1mbar-1]))/0.3 ) ) \
                     / 2.0 + 1. )*ac*(np.log(pa[n_1mbar-1])-np.log(p_tropo))**2+T_tropo 

        # 0.01bar to 1bar
        ac=kappa*T_1bar/(parabolic_shap*(np.log(p_1bar)-np.log(p_tropo)))
        Tp[n_1mbar+1:n_1bar+1]=ac*(np.log(pa[n_1mbar+1:n_1bar+1])-np.log(p_tropo))**2+T_tropo
        Tp_1bar=Tp[n_1bar-1]

    # 1bar to 100bar
    Tp[n_1bar+1:n_hires+1]=T_1bar*(pa[n_1bar+1:n_hires+1]/p_1bar)**kappa

    thetap=(Po/pa)**kappa**Tp

    # sort order of p, T and theta (from bottom up)
    # also convert pressure to pascal
    T=np.flipud(Tp)
    P=np.flipud(pa)*1.e5
    theta=np.flipud(theta)


    # first find the base of the atmosphere from top down-- pb

    n_current=np.max(np.where(P >= pb)) 
    n_base=n_current

    # pressure at the bottom of the domain
    pe=np.zeros(nlay+1)
    pc=np.zeros(nlay  )
    Tc=np.zeros(nlay  )
    Te=np.zeros(nlay+1)
    pe[0]=pb 
    pe[nlay]=0.001
    # give an initial trial dz
    dz=1000. # dz in meters
    niter=0

    # integrate vertically to get desiired pressures (nlay)
    while ( (pe[nlay] >= p_top_lo) and (pe[nlay] <= p_top_hi) ):
        for k in range (nlay):
           pe[k+1]=pe[k]*np.exp(-gravity*dz/(Rd*T[n_current-1]))
           n_current=np.max(np.where(P >= pe[k+1])) 
        pc=(pe[:-1]+pe[1:])/2.
        if (pc[nlay-1] <= p_top_hi):
            dz=dz/1.01 
            pe[0]=pb 
            n_current=n_base
        if (pc[nlay-1] >= p_top_lo): 
            dz=dz*1.01 
            pe[0]=pb 
            n_current=n_base
        niter=niter+1 
        if (niter>1000): 
            raise ValueError("Can't integrate pressure" )

    pc=(pe[:-1]+pe[1:])/2.
    dpe=pe[:-1]-pe[1:]

    n_1bar =np.max(np.where(pc >= p_1bar*1.e5))
    n_1mbar=np.max(np.where(pc >= p_1mbar*1.e5));
    # above 1bar and below 0.01bar
    Tc[n_1bar:n_1mbar-1]=ac*(np.log(pc[n_1bar:n_1mbar-1]*1.e-5) \
                            -np.log(p_tropo))**2+T_tropo
    if (planet==1):
        # deeper than 1bar, note p_1bar and p_tropo are in bar, not pascal
        Tc[:n_1bar+1]=T_1bar*(pc[:n_1bar+1]*1.e-5/p_1bar)**kappa
    else:
        # deeper than 1bar, note p_1bar and p_tropo are in bar, not pascal
        Tc[:n_1bar+1]=T_1bar*(pc[:n_1bar+1]*1.e-5/p_1bar)**kappa

    # above 0.01bar
    Tc[n_1mbar+1:nlay] = (  ( np.tanh( ((np.log(p_1mbar))-np.log(pc[n_1mbar+1:nlay]*1.e-5))/0.3 )   \
                          -   np.tanh( ((np.log(p_1mbar))-np.log(pc[n_1mbar       ]*1.e-5))/0.3 ) ) \
                          /   2.0 + 1.  )*ac*(np.log(pc[n_1mbar]*1.e-5)-np.log(p_tropo))**2+T_tropo

    n_1bar =np.max(np.where(pe >= p_1bar *1.e5))
    n_1mbar=np.max(np.where(pe >= p_1mbar*1.e5))
    if (planet==1):
        # deeper than 1bar, note p_1bar and p_tropo are in bar, not pascal
        Te[:n_1bar+1]=T_1bar*(pe[:n_1bar+1]*1.e-5/p_1bar)**kappa
    else:
        # deeper than 1bar, note p_1bar and p_tropo are in bar, not pascal
        Te[:n_1bar+1]=T_1bar*(pe[:n_1bar+1]*1.e-5/p_1bar)**kappa

    # above 1bar and below 0.01bar
    Te[n_1bar+1:n_1mbar-1]=ac*(np.log(pe[n_1bar+1:n_1mbar-1]*1.e-5)-np.log(p_tropo))**2+T_tropo
    # above 0.01bar
    Te[n_1mbar:nlay+1] = (  ( np.tanh( ((np.log(p_1mbar))-np.log(pe[n_1mbar:nlay+1]*1.e-5))/0.3 )   \
                            - np.tanh( ((np.log(p_1mbar))-np.log(pe[n_1mbar       ]*1.e-5))/0.3 ) ) \
                            / 2.0 + 1.  )*ac*(np.log(pe[n_1mbar]*1.e-5)-np.log(p_tropo))**2+T_tropo
    # smooth temperature profile
    n_smooth=np.min(np.where(pe<p_1bar*1.e5))
    dTc_1bar=2.*Tc[n_smooth-1]-Tc[n_smooth-2]-Tc[n_smooth]
    Tc[n_smooth:]=Tc[n_smooth:]+dTc_1bar

    dTe_1bar=2.*Te[n_smooth-1]-Te[n_smooth-2]-Te[n_smooth]
    Te[n_smooth:]=Te[n_smooth:]+dTe_1bar

    Tc[n_smooth:]=smoothListGaussian(Tc[n_smooth:])
    Te[n_smooth:]=smoothListGaussian(Te[n_smooth:])
 
    # Tc=interp1(log(P),T,log(pc));
    thetac=Tc*(Po./pc)**kappa
    thetae=Te*(Po./pe)**kappa

    # need to update dz here.

    n_tropo=np.max(np.where(p_tropo*1.e5<pc))
    # approximate zc in km
    ze=dz*np.arange(nlay+1, dtype='d') 
    zc=(ze[:-1]+ze[1:])/2.0
    ze=ze/1.e3; 
    zc=zc/1.e3; # convert z from meters to km
    z_tropo=zc[n_tropo]


    # moisture part
    qsH2O,esatH2O=jup_saturation(Tc,pc,H2O_params,mH2,3)
    qsNH3,esatNH3=jup_saturation(Tc,pc,NH3_params,mH2,3)

    for k in range(nlay): 
        if (qsH2O[k] > 3.e-2):
            qsH2O[k]=3.e-2

    qH2O=(1.0-np.tanh((0.45e5-pc)/6.e4))/2.0*3.e-2
    delT=H2O_params[4]*(qH2O-qsH2O)/cp

    dp0=np.reshape(dpe,5,nlay/5)
    th0=np.reshape(thetac,5,nlay/5)

    dp0s=num2str(dp0,'%6.5e, %6.5e, %6.5e, %6.5e, %6.5e, \n');
th0s=num2str(th0,'%6.5e, %6.5e, %6.5e, %6.5e, %6.5e, \n');

qsH2O0=reshape(qsH2O,[5 nlay/5]); qsH2O0=qsH2O0';
qsH2Os=num2str(qsH2O0,'%6.5e, %6.5e, %6.5e, %6.5e, %6.5e, \n');

nextra=round(pe(nlay+1)/dpe(nlay)); 
if (nextra>1); dpe(nlay+1:nlay+nextra-1)=dpe(nlay); 
dpe(nlay+nextra)=pe(nlay+1)-sum(dpe(nlay+1:nlay+nextra-1)); 
else; dpe(nlay+nextra)=pe(nlay); end; 

for k=nlay+1:nlay+nextra; pe(k+1)=pe(k)-dpe(k); end;
pc(nlay+1:nlay+nextra)=(pe(nlay+1:nlay+nextra)+pe(nlay+2:nlay+nextra+1))/2;

Tc(n_1mbar:nlay+nextra)= ( ( tanh( ((log(p_1mbar))-log(pc(n_1mbar:nlay+nextra)*1e-5))/0.3 )   ...
                            -tanh( ((log(p_1mbar))-log(pc(n_1mbar     )*1e-5))/0.3 ) ) ...
                           /2.0 + 1  )*ac*(log(pc(n_1mbar)*1e-5)-log(p_tropo)).^2+T_tropo;
thetac(n_1mbar:nlay+nextra)=Tc(n_1mbar:nlay+nextra) ... 
                           .*(Po./pc(n_1mbar:nlay+nextra)).^kappa;

nlay+nextra


% save to .dat file for future use

%aa(1:nlay+1,1)=dz;
%aa(1:nlay  ,2)=(zc(1:nlay  )-z_tropo)*1e3; 
%aa(nlay+1  ,2)=(ze(nlay+1)-z_tropo)*1e3;
%aa(1:nlay+1,3)=ze(1:nlay+1);
aa(1:nlay+1,1)=pc(1:nlay+1);
aa(1:nlay+1,2)=pe(1:nlay+1);
aa(1:nlay+1,3)=Tc(1:nlay+1);
aa(1:nlay+1,4)=Te(1:nlay+1);

save('Ref_PT.dat','aa','-ascii');


def smoothList(list,strippedXs=False,degree=10):  
    if strippedXs==True: return Xs[0:-(len(list)-(len(list)-degree+1))]  
    smoothed=[0]*(len(list)-degree+1)  
    for i in range(len(smoothed)):  
        smoothed[i]=sum(list[i:i+degree])/float(degree)  
    return smoothed  
   

def smoothListTriangle(list,strippedXs=False,degree=10):  

    weight=[]  
    window=degree*2-1  
    smoothed=[0.0]*(len(list)-window)  
    for x in range(1,2*degree):weight.append(degree-abs(degree-x))  
    w=np.array(weight)  

    for i in range(len(smoothed)):  
        smoothed[i]=sum(np.array(list[i:i+window])*w)/float(sum(w))  
    return smoothed  


def smoothListGaussian(list,strippedXs=False,degree=10):  

    window=degree*2-1  
    weight=np.array([1.0]*window)  
    weightGauss=[]  

    for i in range(window):  
        i=i-degree+1  
        frac=i/float(window)  
        gauss=1/(np.exp((4*(frac))**2))  

        weightGauss.append(gauss)  

    weight=np.array(weightGauss)*weight  

    smoothed=[0.0]*(len(list)-window)  

    for i in range(len(smoothed)):  
        smoothed[i]=sum(np.array(list[i:i+window])*weight)/sum(weight)  
    return smoothed 

