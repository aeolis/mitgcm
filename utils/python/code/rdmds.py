# this is the python script to load all single-record files in a directory
# following the pattern "var.0000000000.data"
# var = name of dynamical variables (T etc.)
# ext = file extensions ('data')
# lev = vertical level to be imported

def load_timeserie(dir,var,ext,lev):
    import os
    import numpy as np

    # enter the data directory
    os.chdir(dir)

    all_files=[]
    for files in os.listdir("."):
        if files.startswith(var) and files.endswith('.'+ext):
            all_files.append(files)

    niter = []

    for line in x:
        niter.append(line.replace("."," ").split()[1])

    var3d=rdmds('.',var+'.'+niter[0])
    dyn3d=np.squeeze(var3d[:,:,lev-1])
    nx=dyn3d.shape[0]; ny=dyn3d.shape[1];
    for line in niter[1:]:
        var3d=rdmds.rdmds('.',var+'.'+line)
        dyn3d=np.append(dyn3d,np.squeeze(var3d[:,:,lev-1]),axis=1)

    dyn3d=dyn3d.reshape(nx,ny,len(niter),order='F')

    return dyn3d

# this is a simple version of rdmds to read 2-D or 
# 3-D or 4-D data from MITgcm outputs. This only works for 
# outputs from useSingleCPUIO=.TRUE. ( including single
# and multiple-record files)
# Usage:
# Single record file:     var3d=rdmds(dir,'var.0000000000')
# Multiple record fiiel:  var3d=rdmds(dir,'var',recnum)
def rdmds(direc, fname, rec=[]):

    from sys import byteorder

    import numpy as np
    import re
    import struct

    fn_meta=direc+'/'+fname+'.meta'

    f=open(fn_meta)
    lines=f.readlines()
    f.close()

    #numbers=[int(s) for s in strs1.split() if s.isdigit()]

    #nend=len(numbers)-1

    # find dimension of the data

    # these two variables are for diagnostics output only
    nFlds=[]
    fldList=[]

    for i, item in enumerate(lines):
        if lines[i].lower().find('ndims')==1:
           nDims=[int(s) for s in lines[i].replace(",","").replace("\n","").split() if s.isdigit()]
        elif lines[i].lower().find('dimlist')==1:
           kstart=i
        elif lines[i].lower().find('dataprec')==1:
           print(lines[i])
           kend=i
        elif lines[i].lower().find('nrecords')==1:
           nrecords=[int(s) for s in lines[i].replace(",","").replace("\n","").split() if s.isdigit()]
        elif lines[i].lower().find('nflds')==1:
           nFlds=[int(s) for s in lines[i].replace("\n","").split() if s.isdigit()]
        elif lines[i].lower().find('fldlist')==1:
           fldList=''.join(lines[i:i+3]).replace("{}","").replace("\n","")
       
    dimlist=''.join(lines[kstart:kend])
    dims=[int(s) for s in dimlist.replace(",","").replace("\n","").split() if s.isdigit()]
    dims=np.asarray(dims)

    nDims=nDims[0]
    nrecords=nrecords[0]
    if (nFlds):
       nFlds=nFlds[0]
    
    # 2-D data
    if np.size(dims)==6:
       dims=dims.reshape(2,3)
       nx=dims[0,0]; ny=dims[1,0]
    if np.size(dims)==9:
       dims=dims.reshape(3,3)
       nx=dims[0,0]; ny=dims[1,0]; nz=dims[2,0]

    if (nrecords > 1) and not(nFlds):
       if not rec:
          print('data=rdmds(direc, fname, rec=[])')
          raise ValueError('Accessing multiple-record data: A record number is required')
 
    # if rec is not empty, check if rec > nrecords
    if rec:
       if rec > nrecords:
          print('Data record length: nrecords = '+str(nrecords))
          raise ValueError('requested record number is larger than the record length')

# read binary file
    fn_data=direc+'/'+fname+'.data'

#    with open("myfile", "rb") as f:
#    byte = f.read(1)
#    while byte != "":
#        # Do stuff with byte.
#        byte = f.read(1)
#    with open("myfile", "rb") as f:
#         fileContent = f.read()

#    finally:
#        f.close()

    with open(fn_data, "rb") as f:
         # Can use data=np.fromfile(f,'>f8') to 
         # force big endian ('>')
         # >f8 big endian, double precision
         # <f8 little endian, double precision
         # ~f8 native, double precision
         #data=np.fromfile(f,'d')
         # 2-D
         if not(fldList):
             if nDims == 2 and nrecords==1:
                 data=np.fromfile(f,'d')
                 # a naive way to check data (not system) endianness
                 if np.isnan(np.max(data.byteswap())) or np.max(data.byteswap())>1.e99:
                     # no need to swap endianness
                     data=data.reshape(nx,ny, order='F')
                 else:
                     # swap endianness
                     data=data.byteswap().reshape(nx,ny, order='F')
             # 3-D
             elif nDims == 3 and nrecords==1:
                 data=np.fromfile(f,'d')
                 if np.isnan(np.max(data.byteswap())) or np.max(data.byteswap())>1.e99:
                     data=data.reshape(nx,ny,nz, order='F')
                 else:
                     data=data.byteswap().reshape(nx,ny,nz, order='F')
             # 4-D
             elif rec and nrecords > 1:
                 if nDims==2:
                     # skip the data before requested record (8 bytes for double precision data)
                     f.seek(int(nx*ny*(rec-1)*8))
                     rawdata=f.read(int(nx*ny*8))
                     # MITgcm data is big endian (by default, except 2-D data)
                     data=struct.unpack(">%dd"%(int(nx*ny)),rawdata)
                     data=np.reshape(data,(nx,ny),order='F')
                 elif nDims==3:
                     # skip the data before requested record (8 bytes for double precision data)
                     f.seek(int(nx*ny*nz*(rec-1)*8))
                     rawdata=f.read(int(nx*ny*nz*8))
                     # MITgcm data is big endian (by default, except 2-D data)
                     data=struct.unpack(">%dd"%(int(nx*ny*nz)),rawdata)
                     data=np.reshape(data,(nx,ny,nz),order='F')
         elif not(rec) and (nFlds>=1) and (fldList):
               print('Diagnostics output enabled')
               print(fldList)
               data=np.fromfile(f,'d')
               if not (np.isnan(np.max(data.byteswap())) or np.max(data.byteswap())>1.e99 ):
                  data=data.byteswap()
               if nDims == 2:
                  data=data.reshape(nx,ny,nFlds,order='F')
               elif nDims == 3:
                  data=data.reshape(nx,ny,nz,nFlds,order='F')
         
    return data
