load flux.out;

dims= size(flux);

nlay = dims(1);

alt = flux(:,2);
rho = flux(:,3);
fw = flux(:,4);
fs = flux(:,5);

fwfs = fw + fs;

dfw(1:nlay)=0.0;
dfs(1:nlay)=0.0;
altd(1:nlay)=0.0;

dfw(1:nlay-1) = -(fw(2:nlay) - fw(1:nlay-1))./(alt(2:nlay)-alt(1:nlay-1))/1.e5;
dfs(1:nlay-1) = -(fs(2:nlay) - fs(1:nlay-1))./(alt(2:nlay)-alt(1:nlay-1))/1.e5;

altd(1:nlay-1) = (alt(2:nlay)+alt(1:nlay-1))/2.0;

dfwfs=dfw+dfs;
