% first ntry=12 layers are in physics grid, max(ksurf) and above are in dynamics grid
% The hybrid grid is for zonal mean of temperature, winds and stream function only!!!
 

clear all;

% section 1: load grid files and data configurations
chdatapath=input('full path to load data /~/ : ','s');
ny_latlon=input('grid points in y in latlon grid: ' );
deltaT=input('time step: ');
multi_dataset=input('multiple data sets (y/n)? ','s');
diffDT=input('Using different time step (y/n)? ','s');
if (diffDT=='y') 
   prejulian=input('last planet day before change time step: ');
   niter0=input('time step at the pickup point: ');
else
   prejulian=0.0;
   niter0=0.0;
end;
tstart=input('starting time step: ');
if (multi_dataset == 'y')
   tend=input('ending time steps: ');
   dnout=input('Output interval in number of time steps: ');
else
   tend=tstart;
   dnout=1;
end;

multi_steps=input('time average over multiple dataset (y/n): ', 's');
plotstream=input('plot meridional stream functions (y/n): ', 's');

rac=rdmds([chdatapath,'RAC']);
dxg=rdmds([chdatapath,'DXG']);
dyg=rdmds([chdatapath,'DYG']);
xg=rdmds([chdatapath,'XG']);
yg=rdmds([chdatapath,'YG']);
xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);
hfacc=rdmds([chdatapath,'hFacC']);
drF=rdmds([chdatapath,'DRF']); drF=squeeze(drF);
topoP=rdmds([chdatapath,'topo_P']);
topoZ=rdmds([chdatapath,'topo_H']);

g2d(:,:,1)=xc; g2d(:,:,2)=yc; g2d(:,:,3)=xg; g2d(:,:,4)=yg;
g2d(:,:,5)=dxg; 
g2d(:,:,6)=dyg; 
g2d(:,:,7)=rac;

% section 2: preprocessing for grid structure and grid points required for 
% viking lander sites as well as the Mars physics parameters

if (plotstream == 'y')
   [namf_cs]=make_psi_lines(g2d, ny_latlon);
else
   'no options'
end;

[AngleCS,AngleSN] = cubeCalcAngle(yg,rac,dxg,dyg);

for k=1:(tend-tstart)/dnout+1
    nstep(k)=tstart+dnout*(k-1);
end;
alldata=nstep;

grv=22.88;
atm_Po=1.E5;
atm_cp=13000.0;
atm_kappa=0.29;
zlow=0;
ztop=250.0e3; % set maximum altitude to 250km
rsphere=71492.0E3;

drsphere=rsphere*(1.0/ny_latlon)*pi;

phys_para(1)=atm_Po;
phys_para(2)=atm_cp;
phys_para(3)=atm_kappa;
phys_para(4)=grv;

% load all data need to be processed, including winds, ice, tracers and temperature

% dynamics grid
ustr='U';
vstr='V';
thetastr='T';       % potential temperature
pedstr='pedyn';     % pressure field (co2+N2)
ptr1str='PTRACER01'; 
ptr2str='PTRACER02';

Etastr='Eta';       % surface pressure anomaly
dims=size(topoP);
nx=dims(1); ny=dims(2);

% before start calculation, find ksurf
% find array size -------------------------------

rC=rdmds([chdatapath,'RC']);
rC=squeeze(rC); 

nr=length(rC); 

topoZ=zeros(nx,ny);

% dynamics grid part


%------------------------------------------------

% section 3: main loop to process data, including zonal mean 
%            and time evolution

for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter),'%d');
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        time=allzero
        timed=alldata(niter);
        day(niter)=(timed-niter0)*deltaT/86400+prejulian; % this is earth day, nomalized to sol later
% dynamics grid variables
        ufile=sprintf('%s%s.%s',chdatapath,ustr,time);
        vfile=sprintf('%s%s.%s',chdatapath,vstr,time);
        pedynfile=sprintf('%s%s.%s',chdatapath,pedstr,time);
        Etafile=sprintf('%s%s.%s',chdatapath,Etastr,time);
        thetafile=sprintf('%s%s.%s',chdatapath,thetastr,time);
        ptr1file=sprintf('%s%s.%s',chdatapath,ptr1str,time);
        ptr2file=sprintf('%s%s.%s',chdatapath,ptr2str,time);

        pedyn=rdmds(pedynfile);
        u3d=rdmds(ufile);
        v3d=rdmds(vfile);
        Eta=rdmds(Etafile);
        theta=rdmds(thetafile);
        ptr1=rdmds(ptr1file);
        ptr2=rdmds(ptr2file);
% construct dynamics grid
        dpdyn(:,:,1:nr)=pedyn(:,:,1:nr)-pedyn(:,:,2:nr+1);
        pcdyn(:,:,1:nr)=(pedyn(:,:,1:nr)+pedyn(:,:,2:nr+1))/2.0;

        for i=1:nx, for j=1:ny, for k=1:nr
           if (dpdyn(i,j,k)<0), dpdyn(i,j,k)=0; end;
        end; end; end;

% calculate zonal mean temperature, winds and streamfunction-----------------------------

% prepare hybrid grid for further diagnostics

        for k=1:nr
            hFacC(1:nx,1:ny,k)= 1; %(Ro+Eta)./Ro.*hfacc(:,:,k);
        end;

        hFacF_fake_dyn(1:nx,1:ny,1:nr+1)=1.0;

% rotate U and V and calculate the zonal mean zonal wind
        [uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C');

        for k=1:nr
            Tdyn(:,:,k)=theta(:,:,k).*(pcdyn(:,:,k)/atm_Po).^atm_kappa;
        end;

        [Tdyn2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(Tdyn,ny_latlon,yc,rac,hFacC); 
        [U2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(uE,ny_latlon,yc,rac,hFacC);
        [V2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(vN,ny_latlon,yc,rac,hFacC);
        [pedyn2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(pedyn,ny_latlon,yc,rac,hFacF_fake_dyn);
        [dp2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(dpdyn,ny_latlon,yc,rac,hFacC);
        [pc2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(pcdyn,ny_latlon,yc,rac,hFacC);
        [ptr12d,mskzon,ylat,areazon] = calcZonalAvgCubeC(ptr1,ny_latlon,yc,rac,hFacC);
        [ptr22d,mskzon,ylat,areazon] = calcZonalAvgCubeC(ptr2,ny_latlon,yc,rac,hFacC);        

        [zc,dz]=cal_height(theta, pedyn, topoZ, phys_para);
        hFacz(1:nx,1:ny,1:nr)=1.0;
        [z2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(zc,ny_latlon,yc,rac,hFacz);

%        pc2d=(pedyn2dtmp(:,1:nr)+pedyn2dtmp(:,2:nr+1))/2.0;
%        dp2d=(pedyn2dtmp(:,1:nr)-pedyn2dtmp(:,2:nr+1));

        if (plotstream == 'y')
           [psi]=use_bk_line_mars(uE,vN,dxg,dyg,rac,dp2d,namf_cs,grv);
        else
           'no options';
        end;
   
% move to permanent storage
        T2dt(:,:,niter)=Tdyn2dtmp;   % Temperature [K], (p,y,ls)
        U2dt(:,:,niter)=U2dtmp;   % U, (p,y,ls) 
        V2dt(:,:,niter)=V2dtmp;   % V, (p,y,ls)
        p2dt(:,:,niter)=pc2d;
        pe2dt(:,:,niter)=pedyn2dtmp;
        z2dt(:,:,niter)=z2d/1e3; % height in km

        if (plotstream == 'y')
           psi2dt(:,:,niter)=-psi(2:ny_latlon+1,2:nr+1); % psi< 0 for clockwise flow
        else
           'no options';
        end;
%        Etat(:,:,niter)=Eta;

end;

% finally expand latitude to 2d        
for k=1:nr;  y2d(1:ny_latlon,k)=ylat; end;

 
fprintf('Zonal mean Temperature vs Ls, T2dt [ %d x %d x %d ]:  \n',ny_latlon,nr,niter);
fprintf('Zonal mean zonal wind vs Ls, U2dt [ %d x %d x %d ]:  \n',ny_latlon,nr,niter);
fprintf('Zonal mean streamfunction vs Ls, psi2dt [ %d x %d x %d ]:  \n',ny_latlon,nr,niter);

if (multi_steps == 'y')
   T2dm=sum(T2dt,3)/length(alldata);
   U2dm=sum(U2dt,3)/length(alldata);
   V2dm=sum(V2dt,3)/length(alldata);
   z2dm=sum(z2dt,3)/length(alldata);
   p2dm=sum(p2dt,3)/length(alldata);
   if (plotstream == 'y')
       psi2dm=sum(psi2dt,3)/length(alldata);
   end;
   fprintf('Time averaged dataset requested : T2dm, U2dm, V2dm, z2dm, p2dm, psi2dm \n');
end;
