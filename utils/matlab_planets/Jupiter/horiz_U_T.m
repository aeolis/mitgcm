% first ntry=12 layers are in physics grid, max(ksurf) and above are in dynamics grid
% The hybrid grid is for zonal mean of temperature, winds and stream function only!!!
 

clear all;

% section 1: load grid files and data configurations
chdatapath=input('full path to load data /~/ : ','s');
ny_latlon=input('grid points in y in latlon grid: ' );
deltaT=input('time step: ');
multi_dataset=input('multiple data sets (y/n)? ','s');
diffDT=input('Using different time step (y/n)? ','s');
if (diffDT=='y') 
   prejulian=input('last planet day before change time step: ');
   niter0=input('time step at the pickup point: ');
else
   prejulian=0.0;
   niter0=0.0;
end;
tstart=input('starting time step: ');
if (multi_dataset == 'y')
   tend=input('ending time steps: ');
   dnout=input('Output interval in number of time steps: ');
else
   tend=tstart;
   dnout=1;
end;

multi_steps=input('time average over multiple dataset (y/n): ', 's');

rac=rdmds([chdatapath,'RAC']);
dxg=rdmds([chdatapath,'DXG']);
dyg=rdmds([chdatapath,'DYG']);
xg=rdmds([chdatapath,'XG']);
yg=rdmds([chdatapath,'YG']);
xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);

% section 2: preprocessing for grid structure and grid points required for 
% viking lander sites as well as the Mars physics parameters

[AngleCS,AngleSN] = cubeCalcAngle(yg,rac,dxg,dyg);

for k=1:(tend-tstart)/dnout+1
    nstep(k)=tstart+dnout*(k-1);
end;
alldata=nstep;

grv=22.88;
atm_Po=1.E5;
atm_cp=13000.0;
atm_kappa=0.29;
rsphere=71492.0E3;

ustr='U';
vstr='V';
thetastr='T';       % potential temperature
pedstr='pedyn';     % pressure field (co2+N2)
ptr1str='PTRACER01';
ptr2str='PTRACER02';

rC=rdmds([chdatapath,'RC']);
rC=squeeze(rC); 

nr=length(rC); 

% section 3: main loop to process data, including zonal mean 
%            and time evolution

for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter),'%d');
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        time=allzero
        timed=alldata(niter);
        day(niter)=(timed-niter0)*deltaT/86400;
% dynamics grid variables
        ufile=sprintf('%s%s.%s',chdatapath,ustr,time);
        vfile=sprintf('%s%s.%s',chdatapath,vstr,time);
        pedynfile=sprintf('%s%s.%s',chdatapath,pedstr,time);
        thetafile=sprintf('%s%s.%s',chdatapath,thetastr,time);
        ptr1file=sprintf('%s%s.%s',chdatapath,ptr1str,time);
        ptr2file=sprintf('%s%s.%s',chdatapath,ptr2str,time);

        pedyn=rdmds(pedynfile);
        u3d=rdmds(ufile);
        v3d=rdmds(vfile);
        theta=rdmds(thetafile);
        ptr1=rdmds(ptr1file);
        ptr2=rdmds(ptr2file);

        dpdyn=pedyn(:,:,1:end-1)-pedyn(:,:,2:end);
        pcdyn(:,:,1:nr)=(pedyn(:,:,1:nr)+pedyn(:,:,2:nr+1))/2.0;

% calculate zonal mean temperature, winds and streamfunction-----------------------------

% rotate U and V and calculate the zonal mean zonal wind
        [uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C');

        for k=1:nr
            Tdyn(:,:,k)=theta(:,:,k).*(pcdyn(:,:,k)/atm_Po).^atm_kappa;
        end;

% move to permanent storage
        Tdt(:,:,:,niter)=Tdyn;   % Temperature [K]
        Udt(:,:,:,niter)=uE;   % U
        Vdt(:,:,:,niter)=vN;   % V
        ptr1dt(:,:,:,niter)=ptr1;
        ptr2dt(:,:,:,niter)=ptr2;
        ptr1mass7bar(:,:,niter)=sum(ptr1(:,:,38:end).*dpdyn(:,:,38:end)/grv, 3).*rac;
        ptr2mass7bar(:,:,niter)=sum(ptr2(:,:,38:end).*dpdyn(:,:,38:end)/grv, 3).*rac;
        T3lev(:,:,1,niter)=Tdyn(:,:,55);
        T3lev(:,:,2,niter)=Tdyn(:,:,45);
        T3lev(:,:,3,niter)=Tdyn(:,:, 1);
        U3lev(:,:,1,niter)=Tdyn(:,:,55);
        U3lev(:,:,2,niter)=Tdyn(:,:,45);
        U3lev(:,:,3,niter)=Tdyn(:,:, 1);
        V3lev(:,:,1,niter)=Tdyn(:,:,55);
        V3lev(:,:,2,niter)=Tdyn(:,:,45);
        V3lev(:,:,3,niter)=Tdyn(:,:, 1);
end;
