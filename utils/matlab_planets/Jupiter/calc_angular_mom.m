%function calc_angular_mom

clear all;

% section 1: load grid files and data configurations
chdatapath=input('full path to load data /~/ : ','s');

% get the time intervals etc
deltaT=input('time step: ');
tstart=input('starting time step: ');

tend=input('ending time steps: ');
DTout=input('Output interval in seconds: ');
rstar=input('Use r* coordinate (y/n)?  ','s');
rigidlid=input('Use rigid Lid (y/n)?  ','s');
multi_rec=input('multiple record files (y/n)?  ','s');
gridtype=input('cube-sphere grid (y/n)?  ','s');
%has_diag=input('Use mass weighted U from diagnostics (y/n)?  ','s');

% convert output interval from seconds to number of time step
dnout=floor(DTout/deltaT);

% get all data needs to be processed
for k=1:(tend-tstart)/dnout+1
    nstep(k)=tstart+dnout*(k-1);
end;
alldata=nstep;

% get grid data
rac=rdmds([chdatapath,'RAC']); raw=rdmds([chdatapath,'RAW']);
raz=rdmds([chdatapath,'RAZ']); ras=rdmds([chdatapath,'RAS']);
dxc=rdmds([chdatapath,'DXC']); dyc=rdmds([chdatapath,'DYC']);
dxg=rdmds([chdatapath,'DXG']); dyg=rdmds([chdatapath,'DYG']);
xc=rdmds([chdatapath,'XC']);   yc=rdmds([chdatapath,'YC']);
xg=rdmds([chdatapath,'XG']);   yg=rdmds([chdatapath,'YG']);
rc=rdmds([chdatapath,'RC']);   rc=squeeze(rc);
rF=rdmds([chdatapath,'RF']);   rF=squeeze(rF);
drF=rdmds([chdatapath,'DRF']); drF=squeeze(drF);

if (gridtype=='y') [AngleCS,AngleSN] = cubeCalcAngle(yg,rac,dxg,dyg); end;

ndims=size(rac); nx=ndims(1); ny=ndims(2); nz=length(rc);

grv=9.36;  atm_Po=1.5E5; atm_cp=13000; atm_Rd=3770.0; atm_kappa=atm_Rd/atm_cp;
rotationPeriod=3.024E5; omega=2*pi/rotationPeriod; rsphere=94370.0E3;
deg2rad=pi/180;

%For finding angular mom of atm at rest u_p=omega*r and r=rsphere*cos(yc)
mmrest=zeros(nx,ny,nz);
for k=1:nz
   mmrest(:,:,k)=omega*rsphere*rsphere*cos(yc*deg2rad).^2*drF(k)/grv.*rac;
end;

rest_mom_tot=sum(sum(sum(mmrest)));

rot_mom_tot (1)=rest_mom_tot;
spin_mom_tot(1)=0.0;
shap_mom_tot(1)=0.0;

for niter=2:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter),'%d');
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        time=allzero
        timed=alldata(niter);
        day(niter)=timed*deltaT/rotationPeriod; % use planet rotation instead;

        if (multi_rec=='y')
           etafile=sprintf('%s%s',chdatapath,'Eta'); eta=rdmds(etafile,'rec',niter);
        else 
           etafile=sprintf('%s%s.%s',chdatapath,'Eta', time);  eta=rdmds(etafile);
        end;
        
%        if (has_diag=='y') 
%           hFacWfile=sprintf('%s%s.%s',chdatapath,'hFacW', time); hFacW=rdmds('hFacWfile');
%           dynDiagfile=sprintf('%s%s.%s',chdatapath,'dynDiagMass', time); dynDiagMass=rdmds(dynDiagfile);
%           u3d=dynDiagMass(:,:,:,1); v3d=dynDiagMass(:,:,:,2);
%           [uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C');

           % angular momentum of planetary rotation
%           for k=1:nz
%               mmrot(:,:,k)=omega*rsphere*rsphere*cos(yc*deg2rad).^2*drF(k)/grv.*rac.*hFacW(:,:,k);
%           end;
%           rot_mom_tot(niter)=sum(sum(sum(mmrot)));

           % angular momentum for wind field
%        else
           if (multi_rec=='y')
              ufile=sprintf('%s%s',chdatapath,'U');  u3d=rdmds(ufile,'rec',niter);
              vfile=sprintf('%s%s',chdatapath,'V');  v3d=rdmds(vfile,'rec',niter);
              tfile=sprintf('%s%s',chdatapath,'T');  t3d=rdmds(tfile,'rec',niter);
              shapufile=sprintf('%s%s',chdatapath,'shap_dU');  shapu3d=rdmds(shapufile,'rec',niter);
              shapvfile=sprintf('%s%s',chdatapath,'shap_dV');  shapv3d=rdmds(shapvfile,'rec',niter);
           else
              ufile=sprintf('%s%s.%s',chdatapath,'U', time);  u3d=rdmds(ufile);
              vfile=sprintf('%s%s.%s',chdatapath,'V', time);  v3d=rdmds(vfile);
              tfile=sprintf('%s%s.%s',chdatapath,'T', time);  t3d=rdmds(tfile);
              shapufile=sprintf('%s%s.%s',chdatapath,'shap_dU', time);  shapu3d=rdmds(shapufile);
              shapvfile=sprintf('%s%s.%s',chdatapath,'shap_dV', time);  shapv3d=rdmds(shapvfile);
           end;

           if (gridtype=='y')
              [uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C');
              [shapuE,shapvN] = rotate_uv2uvEN(shapu3d,shapv3d,AngleCS,AngleSN,'C');
           else
              uE=u3d; vN=v3d;
              shapuE=shapu3d; shapvN=shapv3d;
           end;

           if (rstar=='y')
              % angular momentum of planetary rotation
              if (multi_rec=='y')
                 hFacWfile=sprintf('%s%s',chdatapath,'hFacW.'); hFacW=rdmds(hFacWfile,'rec',niter);
                 hFacSfile=sprintf('%s%s',chdatapath,'hFacS.'); hFacS=rdmds(hFacSfile,'rec',niter);
              else
                 hFacWfile=sprintf('%s%s.%s',chdatapath,'hFacW', time); hFacW=rdmds(hFacWfile);
                 hFacSfile=sprintf('%s%s.%s',chdatapath,'hFacS', time); hFacS=rdmds(hFacSfile);
              end;
              for k=1:nz
                  mmrot(:,:,k)=omega*rsphere*rsphere*cos(yc*deg2rad).^2*drF(k)/grv.*rac.*hFacW(:,:,k);
              end;
              % angular momentum and KE for wind field
              for k=1:nz
                mmspin(:,:,k)=uE(:,:,k).*rsphere.*cos(yc*deg2rad)*drF(k)/grv.*raw.*hFacW(:,:,k);
                mmshap(:,:,k)=shapuE(:,:,k).*rsphere.*cos(yc*deg2rad)*drF(k)/grv.*raw.*hFacW(:,:,k)*deltaT;
                keUV(:,:,k) = 0.5*drF(k)*hFacW(:,:,k)/grv.*raw.*u3d(:,:,k).^2 ...
                            + 0.5*drF(k)*hFacS(:,:,k)/grv.*ras.*v3d(:,:,k).^2;
              end;
           else
              if (rigidlid=='y')
                 mmrot(:,:,1)=omega*rsphere*rsphere*cos(yc*deg2rad).^2.*(drF(1))/grv.*rac;
              else
                 mmrot(:,:,1)=omega*rsphere*rsphere*cos(yc*deg2rad).^2.*(drF(1)+eta)/grv.*rac;
              end;
              for k=2:nz
                mmrot(:,:,k)=omega*rsphere*rsphere*cos(yc*deg2rad).^2*drF(k)/grv.*rac;
              end;
              for k=1:nz
                mmspin(:,:,k)=uE(:,:,k).*rsphere.*cos(yc*deg2rad)*drF(k)/grv.*raw;
                mmshap(:,:,k)=shapuE(:,:,k).*rsphere.*cos(yc*deg2rad)*drF(k)/grv.*raw*deltaT;
                keUV(:,:,k) = 0.5*drF(k)/grv.*raw.*u3d(:,:,k).^2 ...
                            + 0.5*drF(k)/grv.*ras.*v3d(:,:,k).^2;
                % add temperature here
                if (niter==length(alldata)) Tdyn(:,:,k)=t3d(:,:,k)*(rc(k)/atm_Po)^(atm_Rd/atm_cp); end;
              end;
           end;
           rot_mom_tot (niter)=sum(sum(sum(mmrot )));
           spin_mom_tot(niter)=sum(sum(sum(mmspin)));
           shap_mom_tot(niter)=sum(sum(sum(mmshap)));
           spin_ke_tot(niter)=sum(sum(sum(keUV)));

%        end;
end;

% for rigid-lid
dMdt=(spin_mom_tot(2:end)-spin_mom_tot(1:end-1))/DTout;
daym=(day(1:end-1)+day(2:end))/2;
dShap_mom=shap_mom_tot/deltaT;
        

