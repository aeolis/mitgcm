nrecord=length(day);

marsday=day/p2si; 
current_hr_at_0=(marsday(1)-floor(marsday(1)))*24;
hrdiff=24-(137.5/360*24+(marsday(1)-floor(marsday(1)))*24);
[minhrdiff,ndiff]=min( abs( (marsday(1:24)-floor(marsday(1:24)))*24-current_hr_at_0-hrdiff ) );

ndiff=ndiff+1;

dayfrac(1:nrecord-ndiff+1)=marsday(ndiff:nrecord)-floor(marsday(ndiff:nrecord));
dayfrac(nrecord-ndiff+2:nrecord)=marsday(1:ndiff-1)-floor(marsday(1:ndiff-1));

hrs=dayfrac*24+137.5/360*24; % dayfrac*24 is the time at xlon=0, +137.5/360*24 gives current time at MSL
for i=1:length(hrs); if (hrs(i) >= 24) hrs(i)=hrs(i)-24; end; end;

qvnew(1:nrecord-ndiff+1)=qvatmsl(ndiff:nrecord);
qvnew(nrecord-ndiff+2:nrecord)=qvatmsl(1:ndiff-1);

qsnew(1:nrecord-ndiff+1)=qsatmsl(ndiff:nrecord);
qsnew(nrecord-ndiff+2:nrecord)=qsatmsl(1:ndiff-1); 

dpnew(1:nrecord-ndiff+1)=dpmsl(ndiff:nrecord);  
dpnew(nrecord-ndiff+2:nrecord)=dpmsl(1:ndiff-1);

Tnew(1:nrecord-ndiff+1)=Tatmsl(ndiff:nrecord);
Tnew(nrecord-ndiff+2:nrecord)=Tatmsl(1:ndiff-1);

pcnew(1:nrecord-ndiff+1)=pcmsl(ndiff:nrecord);
pcnew(nrecord-ndiff+2:nrecord)=pcmsl(1:ndiff-1);

psnew(1:nrecord-ndiff+1)=psmsl(ndiff:nrecord);
psnew(nrecord-ndiff+2:nrecord)=psmsl(1:ndiff-1);

Tsnew(1:nrecord-ndiff+1)=Tsatmsl(ndiff:nrecord);
Tsnew(nrecord-ndiff+2:nrecord)=Tsatmsl(1:ndiff-1);

hrs24=reshape(hrs(1:720),[24 30]);
dp24=reshape(dpnew(1:720),[24 30]);
pc24=reshape(pcnew(1:720),[24 30]);
ps24=reshape(psnew(1:720),[24 30]);
T24=reshape(Tnew(1:720),[24 30]);  
qv24=reshape(qvnew(1:720),[24 30]);
qs24=reshape(qsnew(1:720),[24 30]);
Ts24=reshape(Tsnew(1:720),[24 30]);

hrsmean=sum(hrs24,2)/30;
dpmean=sum(dp24,2)/30; 
qvmean=sum(qv24.*dp24,2)/30./dpmean;  
qsmean=sum(qs24.*dp24,2)/30./dpmean;  
Tmean=sum(T24.*dp24,2)/30./dpmean;
pcmean=sum(pc24,2)/30;
psmean=sum(ps24,2)/30;
Tsmean=sum(Ts24,2)/30;

