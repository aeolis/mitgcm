clear all;

% section 1: load grid files and data configurations
chdatapath=input('full path to load data /~/ : ','s');
ny_latlon=input('grid points in y in latlon grid: ' );
deltaT=input('time step: ');
tstart=input('starting time step: ');
tend=input('ending time steps: ');
dnout=input('Output interval in number of time steps: ');

rac=rdmds([chdatapath,'RAC']);
dxg=rdmds([chdatapath,'DXG']);
dyg=rdmds([chdatapath,'DYG']);
xg=rdmds([chdatapath,'XG']);
yg=rdmds([chdatapath,'YG']);
xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);
hfacc=rdmds([chdatapath,'hFacC']);
drF=rdmds([chdatapath,'DRF']); drF=squeeze(drF);
topoP=rdmds([chdatapath,'topo_P']);
topoZ=rdmds([chdatapath,'topo_H']);

g2d(:,:,1)=xc; g2d(:,:,2)=yc; g2d(:,:,3)=xg; g2d(:,:,4)=yg;
g2d(:,:,5)=dxg;
g2d(:,:,6)=dyg;
g2d(:,:,7)=rac;

[AngleCS,AngleSN] = cubeCalcAngle(yg,rac,dxg,dyg);

for k=1:(tend-tstart)/dnout+1
    nstep(k)=tstart+dnout*(k-1);
end;

alldata=nstep;

% find the viking lander 1 and 2 sites on the cubed-spherical grid.
for i=1:size(xc,1)
 for j=1:size(xc,2)
     if (abs(xc(i,j)-(-48.222))<180/ny_latlon/2 && abs(yc(i,j)-22.697)<180/ny_latlon/2 )
        fprintf('\n VL1 m=%d n=%d \n',i,j)
        nxcs_vl1=i; nycs_vl1=j;
%     elseif (abs(xc(i,j)-(-48.222))<180/ny_latlon && abs(yc(i,j)-22.697)<180/ny_latlon )
%        fprintf('\n VL1 m=%d n=%d \n',i,j)
%        nxcs_vl1=i; nycs_vl1=j;
     end;
     if (abs(xc(i,j)-134.2630)<180/ny_latlon/2 && abs(yc(i,j)-47.967)<180/ny_latlon/2 )
        fprintf('\n VL2 m=%d n=%d \n',i,j)
        nxcs_vl2=i; nycs_vl2=j;
     elseif (abs(xc(i,j)-134.2630)<180/ny_latlon && abs(yc(i,j)-47.967)<180/ny_latlon )
        fprintf('\n VL2 m=%d n=%d \n',i,j)
        nxcs_vl2=i; nycs_vl2=j;
     end;

     if (abs(xc(i,j))<180/ny_latlon/2 && abs(yc(i,j)+2)<180/ny_latlon/2 )
        fprintf('\n OP m=%d n=%d \n',i,j)
        nxcs_op=i; nycs_op=j;
     end;

     if (abs(xc(i,j)-137.5)<180/ny_latlon/2 && abs(yc(i,j)+4.5)<180/ny_latlon/2 )
        fprintf('\n MSL m=%d n=%d \n',i,j)
        nxcs_msl=i; nycs_msl=j;
     end;
 end;
end;


% Mars physics parameters, including gravity, atmospheric properties and topography
grv=3.727;
atm_Po=610.0;
atm_cp=767.35;
atm_kappa=0.25;
zlow=-6.5470e3; % 2.5 degree res only
ztop=80.0e3; % set maximum altitude to 80km
rsphere=3389.9E3;
drsphere=rsphere*(1.0/ny_latlon)*pi;

phys_para(1)=atm_Po;
phys_para(2)=atm_cp;
phys_para(3)=atm_kappa;
phys_para(4)=grv;

% define some constants for calculating the solar angle l_s
p2si=1.0274912;
equinox_fraction= 0.2695;
planet_year=669.0;
zero_date=488.7045;
eccentricity=0.09341233;
semimajoraxis= 1.52366231;
SMALL_VALUE=1.0e-6;
deleqn = equinox_fraction * planet_year;
qq = 2.0 * (pi * deleqn / planet_year);
er = sqrt( (1.0+eccentricity)/(1.0-eccentricity) );
%  determine true anomaly at equinox:  eq
%  Iteration for eq
   e = 1.0;
   cd0 = 1.0;
   while (cd0 > SMALL_VALUE)
      ep = e - (e-eccentricity*sin(e)-qq) / (1.0-eccentricity*cos(e));
      cd0 = abs(e-ep);
      e = ep;
   end;
   eq = 2.0 * atan( er * tan(0.5*e) );

Etastr='Eta';


current_year=1;

for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter));
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        time=allzero
        timed=alldata(niter);
        day(niter)=timed*deltaT/86400; 
    
        Etafile=sprintf('%s%s.%s',chdatapath,Etastr,time);
        Eta=rdmds(Etafile);

        patvl1(niter)=topoP(nxcs_vl1,nycs_vl1)+Eta(nxcs_vl1,nycs_vl1);
        patvl2(niter)=topoP(nxcs_vl2,nycs_vl2)+Eta(nxcs_vl2,nycs_vl2);
        patop(niter)=topoP(nxcs_op,nycs_op)+Eta(nxcs_op,nycs_op);
        pamsl(niter)=topoP(nxcs_msl,nycs_msl)+Eta(nxcs_msl,nycs_msl);

% calculate ls  (solar longitude) -------
        julian=day(niter)/p2si;
        date_dbl = julian - zero_date;
        while (date_dbl < 0.)
           date_dbl=date_dbl+planet_year;
        end;
        while (date_dbl > planet_year)
           date_dbl=date_dbl-planet_year;
        end;


%  determine true anomaly at current date:  w
%  Iteration for w
       em = 2.0 * pi * date_dbl/planet_year;
       e  = 1.0;
       cd0 = 1.0;
       while (cd0 > SMALL_VALUE)
       ep = e-(e-eccentricity*sin(e)-em) / (1.0-eccentricity*cos(e));
       cd0 = abs(e-ep);
       e = ep;
       end;
       w = 2.0 * atan( er * tan(0.5*e) );

%  Radius vector ( astronomical units:  AU )
       als= (w - eq)*180.0/pi;      %Aerocentric Longitude
       if (als < 0.0)
          als=als+360.0;
       end;
% if als is reset to some value close to zero, then we want to 
% start a new year
       if (niter>1); if (ls(niter-1)-360.*(current_year-1)-als>180.);
          current_year=current_year+1;
       end; end;
       ls(niter) = als+360.*(current_year-1);
       yr(niter) = ls(niter)/360.;

end;
