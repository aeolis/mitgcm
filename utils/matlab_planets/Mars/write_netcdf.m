function write_netcdf (u4d, v4d, p4d, t4d, z4d, xc, yc, ls, chdatapath )

%*****************************************************************************80
%
%% PRES_TEMP_4D_WR writes a 4 dimensional array to a NETCDF file.
%
%  Discussion:
%
%    We are writing 4D data, a 2 x 6 x 12 lvl-lat-lon grid,
%    with 2 timesteps of data.
%
%  Licensing:
%
%    This code is distributed under the GNU LGPL license.
%
%  Modified:
%
%    03 December 2009
%
%  Author:
%
%    John Burkardt
%
%  Reference:
%
%    Russ Rew, Glenn Davis, Steve Emmerson, Harvey Davies, Ed Hartne,
%    The NetCDF User's Guide,
%    Unidata Program Center, March 2009.
%
  fprintf ( 1, '\n' );
  fprintf ( 1, 'UVPTZ_write_netcdf:\n' );
  fprintf ( 1, '  MATLAB version.\n' );
  fprintf ( 1, '\n' );
  fprintf ( 1, '  Create two 4D arrays of U, V, P, T, Z,\n' );
  fprintf ( 1, '  and write them to a NETCDF file.\n' );
%
%  Create the data.
%
  lat_num = length(yc);
  lat_start = yc(1) ;

%  for lat = 1 : lat_num
%    lats(lat) = lat_start + ( lat - 1 ) * 5.0;
%  end

  lats = yc;

  lon_num = length(xc);
  lon_start = xc(1);

%  for lon = 1 : lon_num
%    lons(lon) = lon_start + ( lon - 1 ) * 5.0;
%  end

  lons = xc;
  
  lvl_num = size(u4d,3);
%  i = 0;
%  for lvl = 1 : lvl_num
%    for lat = 1 : lat_num
%      for lon = 1 : lon_num
%        press_out(lon,lat,lvl) = sample_pressure + i;
%        temp_out(lon,lat,lvl) = sample_temp + i;
%        i = i + 1;
%      end
%    end
%  end

  rec_num = length(ls);

%
%  Create the NETCDF file.
%
  nc_filename = sprintf('%s%s',chdatapath,'UVPTZ_4D.nc');

  ncid = netcdf.create ( nc_filename, 'NC_CLOBBER' );
%
%  Define the dimensions.  The record dimension is defined to have
%  unlimited length - it can grow as needed. In this example it is
%  the time dimension.
%
  lvl_name = 'level';
  lvl_dimid = netcdf.defDim ( ncid, lvl_name, lvl_num );

  lat_name = 'latitude';
  lat_dimid = netcdf.defDim ( ncid, lat_name, lat_num );

  lon_name = 'longitude';
  lon_dimid = netcdf.defDim ( ncid, lon_name, lon_num );

  rec_name = 'time';
%  rec_dimid = netcdf.defDim ( ncid, rec_name, netcdf.getConstant ( 'NC_UNLIMITED' ) );
  rec_dimid = netcdf.defDim ( ncid, rec_name, rec_num );
%
%  Define the coordinate variables.  We will only define coordinate
%  variables for lat and lon.  Ordinarily we would need to provide
%  an array of dimension IDs for each variable's dimensions, but
%  since coordinate variables only have one dimension, we can
%  simply provide the address of that dimension ID (lat_dimid) and
%  similarly for (lon_dimid).
%
  lat_varid = netcdf.defVar ( ncid, lat_name, 'double', lat_dimid );
  lon_varid = netcdf.defVar ( ncid, lon_name, 'double', lon_dimid );
  rec_varid = netcdf.defVar ( ncid, rec_name, 'double', rec_dimid );
%
%  Assign units attributes to coordinate variables.
%
  units = 'units';
  lat_units = 'degrees north';
  netcdf.putAtt ( ncid, lat_varid, units, lat_units );
  lon_units = 'degrees east';
  netcdf.putAtt ( ncid, lon_varid, units, lon_units );
  rec_units = 'Solar Longitude';
  netcdf.putAtt ( ncid, rec_varid, units, rec_units );
%
%  The dimids array is used to pass the dimids of the dimensions of
%  the NETCDF variables. Both of the NETCDF variables we are creating
%  share the same four dimensions.
%
  dimids = [ lon_dimid, lat_dimid, lvl_dimid, rec_dimid ];
%
%  Define the NETCDF variables for the pressure and temperature data.
%
  u_name = 'Zonal velocity U';
  u_varid = netcdf.defVar ( ncid, u_name, 'double', dimids );
  v_name = 'Meridional velocity V';
  v_varid = netcdf.defVar ( ncid, v_name, 'double', dimids ); 
  press_name = 'pressure';
  press_varid = netcdf.defVar ( ncid, press_name, 'double', dimids );
  temp_name = 'temperature';
  temp_varid = netcdf.defVar ( ncid, temp_name, 'double', dimids );
  z_name = 'Altitude z';
  z_varid = netcdf.defVar ( ncid, z_name, 'double', dimids );
%
%  Assign units attributes to the variables.
%
  u_units = 'm/s';
  netcdf.putAtt ( ncid, u_varid, units, u_units );
  v_units = 'm/s';
  netcdf.putAtt ( ncid, v_varid, units, v_units );
  press_units = 'Pascal';
  netcdf.putAtt ( ncid, press_varid, units, press_units );
  temp_units = 'Kelvin';
  netcdf.putAtt ( ncid, temp_varid, units, temp_units );
  z_units = 'kilometers';
  netcdf.putAtt ( ncid, z_varid, units, z_units );
%
%  End define mode.
%
  netcdf.endDef ( ncid );
%
%  Write the coordinate variable data.  This will put the latitudes
%  and longitudes of our data grid into the NETCDF file.
%
  netcdf.putVar ( ncid, lat_varid, lats );
  netcdf.putVar ( ncid, lon_varid, lons );
  netcdf.putVar ( ncid, rec_varid, ls   );
%
%  These settings tell NETCDF to write one timestep of data.  The
%  setting of start(4) inside the loop below tells NETCDF which
%  timestep to write.
%
%  Apparently, START must be set using the C convention, that is,
%  it seems to be zero-based!
%
  count = [ lon_num, lat_num, lvl_num, 1 ];
  start = [ 0, 0, 0, 0 ];
%
%  Write the data.  This will write our surface pressure and
%  surface temperature data.  The arrays only hold one timestep worth
%  of data.  We will just rewrite the same data for each timestep.  In
%  a real application, the data would change between timesteps.
%
%  rec_num = length(ls);

  for rec = 1 : rec_num

    start(4) = rec - 1;
    netcdf.putVar ( ncid, u_varid, start, count, u4d(:,:,:,rec) );
    netcdf.putVar ( ncid, v_varid, start, count, v4d(:,:,:,rec) );
    netcdf.putVar ( ncid, press_varid, start, count, p4d(:,:,:,rec) );
    netcdf.putVar ( ncid, temp_varid, start, count, t4d(:,:,:,rec) );
    netcdf.putVar ( ncid, z_varid, start, count, z4d(:,:,:,rec) );

  end
%
%  Close the file.
%
  netcdf.close ( ncid );

  fprintf ( 1, '\n' );
  fprintf ( 1, '  The data was written to the file "%s".\n', nc_filename );
  fprintf ( 1, '\n' );
  fprintf ( 1, 'PRES_TEMP_4D_WR:\n' );
  fprintf ( 1, '  Normal end of execution.\n' );

  return
end
