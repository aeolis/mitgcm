% first ntry=12 layers are in physics grid, max(ksurf) and above are in dynamics grid
% The hybrid grid is for zonal mean of temperature, winds and stream function only!!!
 

clear all;

% section 1: load grid files and data configurations
chdatapath=input('full path to load data /~/ : ','s');
ny_latlon=input('grid points in y in latlon grid: ' );
deltaT=input('time step: ');
multi_dataset=input('multiple data sets (y/n)? ','s');
tstart=input('starting time step: ');
if (multi_dataset == 'y')
   tend=input('ending time steps: ');
   dnout=input('Output interval in number of time steps: ');
else
   tend=tstart;
   dnout=1;
end;

multi_steps=input('time average over multiple dataset (y/n): ', 's');
plotstream=input('plot meridional stream functions (y/n): ', 's');
planet=input('select planet Mars 1, Titan 2 :  ' );
tracer=input('select 1 for tracer :  ' );

rac=rdmds([chdatapath,'RAC']);
dxg=rdmds([chdatapath,'DXG']);
dyg=rdmds([chdatapath,'DYG']);
xg=rdmds([chdatapath,'XG']);
yg=rdmds([chdatapath,'YG']);
xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);
hfacc=rdmds([chdatapath,'hFacC']);
drF=rdmds([chdatapath,'DRF']); drF=squeeze(drF);
sigC=rdmds([chdatapath,'BHybSigC']); sigC=squeeze(sigC);
sigF=rdmds([chdatapath,'BHybSigF']); sigF=squeeze(sigF);
topoP=rdmds([chdatapath,'topo_P']);
topoZ=rdmds([chdatapath,'topo_H']);

g2d(:,:,1)=xc; g2d(:,:,2)=yc; g2d(:,:,3)=xg; g2d(:,:,4)=yg;
g2d(:,:,5)=dxg; 
g2d(:,:,6)=dyg; 
g2d(:,:,7)=rac;

% section 2: preprocessing for grid structure and grid points required for 
% viking lander sites as well as the Mars physics parameters

if (plotstream == 'y')
   [namf_cs]=make_psi_lines(g2d, ny_latlon);
else
   'no options'
end;

[AngleCS,AngleSN] = cubeCalcAngle(yg,rac,dxg,dyg);

for k=1:(tend-tstart)/dnout+1
    nstep(k)=tstart+dnout*(k-1);
end;
alldata=nstep;

% Mars physics parameters, including gravity, atmospheric properties and topography
grv=3.727;
atm_Po=610.0;
atm_cp=767.35;
atm_kappa=0.25;
zlow=-6.5470e3; % 2.5 degree res only
ztop=80.0e3; % set maximum altitude to 80km
rsphere=3389.9E3;

% define some constants for calculating the solar angle l_s
p2si=1.0274912;
equinox_fraction= 0.2695;
planet_year=669.0;
zero_date=488.7045;
eccentricity=0.09341233;
semimajoraxis= 1.52366231;

drsphere=rsphere*(1.0/ny_latlon)*pi;

phys_para(1)=atm_Po;
phys_para(2)=atm_cp;
phys_para(3)=atm_kappa;
phys_para(4)=grv;

% calculate the solar longitude.
SMALL_VALUE=1.0e-6;
deleqn = equinox_fraction * planet_year;
qq = 2.0 * (pi * deleqn / planet_year);
er = sqrt( (1.0+eccentricity)/(1.0-eccentricity) );
%  determine true anomaly at equinox:  eq
%  Iteration for eq
   e = 1.0;
   cd0 = 1.0;
   while (cd0 > SMALL_VALUE)
      ep = e - (e-eccentricity*sin(e)-qq) / (1.0-eccentricity*cos(e));
      cd0 = abs(e-ep);
      e = ep;
   end;
   eq = 2.0 * atan( er * tan(0.5*e) );

% load all data need to be processed, including winds, ice, tracers and temperature

% dynamics grid
ustr='U';
vstr='V';
thetastr='T';       % potential temperature
if (planet==1) co2icestr='co2ice'; end;
if (tracer==1) ptrstr='PTRACER01'; end;

Etastr='Eta';       % surface pressure anomaly


dims=size(topoP);
nx=dims(1); ny=dims(2);
ksurf(1:nx,1:ny)=1;

% before start calculation, find ksurf
% find array size -------------------------------

rC=rdmds([chdatapath,'RC']);
rC=squeeze(rC); 

nr=length(rC); 

ksurf(:,:)=1; kmax=1;


% number of surrounding points at MSL
if (planet==1)
num_vl1=0;
num_vl2=0;
num_op =0;
num_msl=0;
num_pat=0;
% find the viking lander 1 and 2 sites on the cubed-spherical grid.
for i=1:size(xc,1)
 for j=1:size(xc,2)
     if (abs(xc(i,j)-(-48.222))<180/ny_latlon && abs(yc(i,j)-22.697)<180/ny_latlon )
        fprintf('\n VL1 m=%d n=%d \n',i,j)
        num_vl1=num_vl1+1;
        nxcs_vl1(num_vl1)=i; nycs_vl1(num_vl1)=j;
     end;
     if (abs(xc(i,j)-134.2630)<180/ny_latlon && abs(yc(i,j)-47.967)<180/ny_latlon )
        fprintf('\n VL2 m=%d n=%d \n',i,j)
        num_vl2=num_vl2+1;
        nxcs_vl2(num_vl2)=i; nycs_vl2(num_vl2)=j;
     end;

     if (abs(xc(i,j))<180/ny_latlon && abs(yc(i,j)+2)<180/ny_latlon )
        fprintf('\n OP m=%d n=%d \n',i,j)
        num_op=num_op+1;
        nxcs_op(num_op)=i; nycs_op(num_op)=j;
     end;

     if (abs(xc(i,j)-137.4417)<180/ny_latlon && abs(yc(i,j)+4.5895)<180/ny_latlon )
        fprintf('\n MSL m=%d n=%d \n',i,j)
        num_msl=num_msl+1;
        nxcs_msl(num_msl)=i; nycs_msl(num_msl)=j;
     end;

     if (abs(xc(i,j)+33)<180/ny_latlon && abs(yc(i,j)-19)<180/ny_latlon )
        fprintf('\n PAT m=%d n=%d \n',i,j)
        num_pat=num_pat+1;
        nxcs_pat(num_pat)=i; nycs_pat(num_pat)=j;
     end;
 end;
end;
end;

%------------------------------------------------

% section 3: main loop to process data, including zonal mean 
%            and time evolution

for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter),'%d');
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        time=allzero
        timed=alldata(niter);
        day(niter)=timed*deltaT/86400; % this is earth day, nomalized to sol later
% dynamics grid variables
        ufile=sprintf('%s%s.%s',chdatapath,ustr,time);
        vfile=sprintf('%s%s.%s',chdatapath,vstr,time);
        Etafile=sprintf('%s%s.%s',chdatapath,Etastr,time);
        thetafile=sprintf('%s%s.%s',chdatapath,thetastr,time);
        if (planet == 1)
          co2icefile=sprintf('%s%s.%s',chdatapath,co2icestr,time);
          if (tracer==1) ptrfile=sprintf('%s%s.%s',chdatapath,ptrstr,time); end;
        end;

        u3d=rdmds(ufile);
        v3d=rdmds(vfile);
        Eta=rdmds(Etafile);
        theta=rdmds(thetafile);
        if (planet == 1)
           co2ice=rdmds(co2icefile);
           if (tracer==1) ptr=rdmds(ptrfile); end;
        end;

% construct dynamics grid
        for k=1:nr
            pedyn(:,:,k)=sigF(k)*(topoP+Eta);
            pcdyn(:,:,k)=sigC(k)*(topoP+Eta);
        end;
        pedyn(:,:,nr+1)=sigF(nr+1)*(topoP+Eta);
        dpdyn(:,:,1:nr)=pedyn(:,:,1:nr)-pedyn(:,:,2:nr+1);

        for i=1:nx
         for j=1:ny
          for k=1:nr
              if (dpdyn(i,j,k)<0)
                  dpdyn(i,j,k)=0;
              end;
          end;
         end;
        end;

        % calculate the total air mass and surface co2 ice mass in kilograms--
        if (planet==1)
            airmass=(topoP+Eta).*rac/grv;
            icemass=co2ice.*rac;
            totair(niter)=sum(sum(airmass));
            totice(niter)=sum(sum(icemass));
            if (tracer==1)
               Armassmean(niter)=sum(sum(sum(ptr.*dpdyn/grv,3).*rac))/sum(sum(rac));
            end;
        end;
% calculate zonal mean temperature, winds and streamfunction-----------------------------

% prepare hybrid grid for further diagnostics

        for k=1:nr
            hFacC(1:nx,1:ny,k)= 1; %(Ro+Eta)./Ro.*hfacc(:,:,k);
        end;

        hFacF_fake_dyn(1:nx,1:ny,1:nr+1)=1.0;

% rotate U and V and calculate the zonal mean zonal wind
        [uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C');

        for k=1:nr
            Tdyn(:,:,k)=theta(:,:,k).*(pcdyn(:,:,k)/atm_Po).^atm_kappa;
        end;

        [Tdyn2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(Tdyn,ny_latlon,yc,rac,hFacC); 
        [U2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(uE,ny_latlon,yc,rac,hFacC);
        [V2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(vN,ny_latlon,yc,rac,hFacC);
        [pedyn2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(pedyn,ny_latlon,yc,rac,hFacF_fake_dyn);
        if (planet == 1)
        [ci2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(co2ice,ny_latlon,yc,rac,squeeze(hFacC(:,:,1)));
        end;

        [zc,dz]=cal_height(theta, pedyn, topoZ, phys_para);
        hFacz(1:nx,1:ny,1:nr)=1.0;
        [z2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(zc,ny_latlon,yc,rac,hFacz);

        pc2d=(pedyn2dtmp(:,1:nr)+pedyn2dtmp(:,2:nr+1))/2.0;
        dp2d=(pedyn2dtmp(:,1:nr)-pedyn2dtmp(:,2:nr+1));

        if (plotstream == 'y')
           [psi]=use_bk_line_mars(uE,vN,dxg,dyg,rac,dp2d,namf_cs,grv);
        else
           'no options';
        end;
   
% move to permanent storage
        T2dt(:,:,niter)=Tdyn2dtmp;   % Temperature [K], (p,y,ls)
        U2dt(:,:,niter)=U2dtmp;   % U, (p,y,ls) 
        V2dt(:,:,niter)=V2dtmp;   % V, (p,y,ls)
        p2dt(:,:,niter)=pc2d;
        pe2dt(:,:,niter)=pedyn2dtmp;
        z2dt(:,:,niter)=z2d/1e3; % height in km
        if (planet == 1)
         ci2dt(:,:,niter)=ci2dtmp;
        end;

        if (plotstream == 'y')
           psi2dt(:,:,niter)=-psi(2:ny_latlon+1,2:nr+1); % psi< 0 for clockwise flow
        else
           'no options';
        end;
%        Etat(:,:,niter)=Eta;

% calculate ls  (solar longitude) -------
        julian=day(niter)/p2si;
        date_dbl = julian - zero_date;
        while (date_dbl < 0.)
           date_dbl=date_dbl+planet_year;
        end;
        while (date_dbl > planet_year)
           date_dbl=date_dbl-planet_year;
        end;


%  determine true anomaly at current date:  w
%  Iteration for w
       em = 2.0 * pi * date_dbl/planet_year;
       e  = 1.0;
       cd0 = 1.0;
       while (cd0 > SMALL_VALUE)
       ep = e-(e-eccentricity*sin(e)-em) / (1.0-eccentricity*cos(e));
       cd0 = abs(e-ep);
       e = ep;
       end;
       w = 2.0 * atan( er * tan(0.5*e) );
 
%  Radius vector ( astronomical units:  AU )
       als= (w - eq)*180.0/pi;      %Aerocentric Longitude
       if (als < 0.0) 
          als=als+360.0;
       end;
       ls(niter) = als;

end;

% finally expand latitude to 2d        
for k=1:nr;  y2d(1:ny_latlon,k)=ylat; end;

 
fprintf('Zonal mean Temperature vs Ls, T2dt [ %d x %d x %d ]:  \n',ny_latlon,nr,niter);
fprintf('Zonal mean zonal wind vs Ls, U2dt [ %d x %d x %d ]:  \n',ny_latlon,nr,niter);
fprintf('Zonal mean streamfunction vs Ls, psi2dt [ %d x %d x %d ]:  \n',ny_latlon,nr,niter);

if (multi_steps == 'y')
   T2dm=sum(T2dt,3)/length(alldata);
   U2dm=sum(U2dt,3)/length(alldata);
   V2dm=sum(V2dt,3)/length(alldata);
   z2dm=sum(z2dt,3)/length(alldata);
   p2dm=sum(p2dt,3)/length(alldata);
   ci2dm=sum(ci2dt,3)/length(alldata);
   if (plotstream == 'y')
       psi2dm=sum(psi2dt,3)/length(alldata);
   end;
   fprintf('Time averaged dataset requested : T2dm, U2dm, V2dm, z2dm, p2dm, psi2dm \n');
end;
