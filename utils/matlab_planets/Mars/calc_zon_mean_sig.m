% first ntry=12 layers are in physics grid, max(ksurf) and above are in dynamics grid
% The hybrid grid is for zonal mean of temperature, winds and stream function only!!!
 

clear all;

% section 1: load grid files and data configurations
chdatapath=input('full path to load data /~/ : ','s');
ny_latlon=input('grid points in y in latlon grid: ' );
deltaT=input('time step: ');
multi_dataset=input('multiple data sets (y/n)? ','s');
useMultiRec=input('Use multiple-record data(y/n)? ','s');

if(isempty(multi_dataset)||multi_dataset~='y') multi_dataset='n'; end;
if(isempty(useMultiRec)||useMultiRec~='y') useMultiRec='n'; end;

% find dump frequency
fid=fopen(sprintf('%s%s',chdatapath,'data'));
while ~feof(fid)
     aa=fgetl(fid); aa=strtrim(aa); xx=strfind(aa,'dumpFreq');
     if(~isempty(xx) && aa(1)~='#') bb=textscan(aa,'%s');
       cc=(strrep(bb{1},'dumpFreq=','')); dumpfreq=str2num(cc{1});
     end;
end;
fclose(fid); clear aa bb cc xx;

if (multi_dataset~='y') recnum=input('the record number to be accessed: '); 
else recnum=[]; end;

if (useMultiRec=='y')
   fid=fopen(sprintf('%s%s',chdatapath,'U.meta')); 
else
   tstart=input('starting time step: ');
   str0='0000000000'; str1=num2str(tstart);
   str0(length(str0)-length(str1)+1:end)=str1;
   fid=fopen(sprintf('%s%s%s%s',chdatapath,'U.',str0,'.meta')); 
end;

% read record info from meta file
while ~feof(fid)
     aa=fgetl(fid); xx1=strfind(aa,'nrecords'); xx2=strfind(aa,'timeStepNumber');
     if(~isempty(xx1)) bb1=textscan(aa,'%s%s%s%d%s'); nrecords=double(bb1{4}); end;
     if(~isempty(xx2)) bb2=textscan(aa,'%s%s%s%d%s'); tend=double(bb2{4}); end;
end;
fclose(fid); clear aa xx1 xx2 bb1 bb2;

if (useMultiRec=='y')
   tstart=tend-(nrecords-1)*floor(dumpfreq/deltaT);
   myiter(1)=tstart;
   for i=2:nrecords myiter(i)=tstart+(i-1)*dumpfreq/deltaT; end;
   fprintf('%s%d\n','nrecords(end)=',nrecords);
   fprintf('%s%d\n','myIter(1)=',tstart);
   fprintf('%s%d\n','myIter(end)=',tend);
   dnout=floor((tend-tstart)/max(1,nrecords-1)); % or dumpfreq/deltaT;
else
   dnout=max(1,floor(dumpfreq/deltaT));
end;

multi_steps=input('time average over multiple dataset (y/n): ', 's');
plotstream=input('plot meridional stream functions (y/n): ', 's');
planet=input('select planet Mars 1, Titan 2 :  ' );
tracer=input('select: 1 Ar+dust, 2 Ar+co2 ice, 3 Ar+co2 ice+dust, 4 Ar :  ' );

rac=rdmds([chdatapath,'RAC']);
dxg=rdmds([chdatapath,'DXG']);
dyg=rdmds([chdatapath,'DYG']);
xg=rdmds([chdatapath,'XG']);
yg=rdmds([chdatapath,'YG']);
xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);
hfacc=rdmds([chdatapath,'hFacC']);
drF=rdmds([chdatapath,'DRF']); drF=squeeze(drF);
sigC=rdmds([chdatapath,'BHybSigC']); sigC=squeeze(sigC);
sigF=rdmds([chdatapath,'BHybSigF']); sigF=squeeze(sigF);
aHybC=rdmds([chdatapath,'AHybSigC']); aHybC=squeeze(aHybC);
aHybF=rdmds([chdatapath,'AHybSigF']); aHybF=squeeze(aHybF);
topoP=rdmds([chdatapath,'topo_P']);
topoZ=rdmds([chdatapath,'topo_H']);

g2d(:,:,1)=xc;   g2d(:,:,2)=yc;   g2d(:,:,3)=xg; g2d(:,:,4)=yg;
g2d(:,:,5)=dxg;  g2d(:,:,6)=dyg;  g2d(:,:,7)=rac;

rFullDepth=sum(drF);

% section 2: preprocessing for grid structure and grid points required for 
% viking lander sites as well as the Mars physics parameters

if (plotstream == 'y')
   [namf_cs]=make_psi_lines(g2d, ny_latlon);
else
   'no options'
end;

[AngleCS,AngleSN] = cubeCalcAngle(yg,rac,dxg,dyg);

for k=1:(tend-tstart)/dnout+1, nstep(k)=tstart+dnout*(k-1); end;

if (multi_dataset=='y') 
   alldata=nstep;
else 
   if isempty(recnum), recnum=nrecords; end;
   alldata=myiter(recnum); 
end;

% Mars physics parameters, including gravity, atmospheric properties and topography
if (isempty(planet) || planet==1)

   grv=3.727; atm_Po=610.0; atm_cp=767.35; atm_kappa=0.25;
   zlow=-6.5470e3; ztop=80.0e3; rsphere=3389.9E3;

   % define some constants for calculating the solar angle l_s
   p2si=1.0274912;     equinox_fraction= 0.2695; planet_year=669.0;
   zero_date=488.7045; eccentricity=0.09341233;  semimajoraxis= 1.52366231;
 
   % polar average, some 'deg_pole' poleward.
   deg_pole=75;

elseif (planet == 2)

   grv=1.358; atm_Po=1.5E5; atm_cp=1044.0; atm_kappa=0.27778;
   zlow=0; ztop=250.0e3;  rsphere=2575.0E3;

   p2si=15.95;       equinox_fraction= 0.2098; planet_year=686.0;
   zero_date=542.06; eccentricity=0.056;       semimajoraxis= 9.58256;

end;

drsphere=rsphere*(1.0/ny_latlon)*pi;

phys_para(1)=atm_Po;    phys_para(2)=atm_cp;
phys_para(3)=atm_kappa; phys_para(4)=grv;

% calculate the solar longitude.
SMALL_VALUE=1.0e-6;
deleqn = equinox_fraction * planet_year;
qq = 2.0 * (pi * deleqn / planet_year);
er = sqrt( (1.0+eccentricity)/(1.0-eccentricity) );
%  determine true anomaly at equinox:  eq
%  Iteration for eq
e = 1.0;  cd0 = 1.0;
while (cd0 > SMALL_VALUE)
      ep = e - (e-eccentricity*sin(e)-qq) / (1.0-eccentricity*cos(e));
      cd0 = abs(e-ep);  e = ep;
end;
eq = 2.0 * atan( er * tan(0.5*e) );

% load all data need to be processed, including winds, ice, tracers and temperature

% dynamics grid
ustr='U'; vstr='V'; thetastr='T';       % potential temperature
Etastr='Eta'; tskstr='TSK';
if (planet==1) co2icestr='co2ice'; end;
if (tracer==1) ptrstr='PTRACER01'; duststr ='PTRACER02'; dustsedstr='dustsed01'; end;
if (tracer==2) ptrstr='PTRACER01'; co2istr='PTRACER02'; end;
if (tracer==3) ptrstr='PTRACER01'; co2istr='PTRACER02'; duststr ='PTRACER03'; dustsedstr='dustsed01'; end; 
if (tracer==4) ptrstr='PTRACER01'; end;


% find array size -------------------------------
dims=size(topoP); nx=dims(1); ny=dims(2);
rC=rdmds([chdatapath,'RC']); rC=squeeze(rC);  nr=length(rC); 

if (isempty(topoZ)) topoZ=zeros(nx,ny); end;

% number of surrounding points at MSL
if (planet==1)
   num_vl1=0; num_vl2=0; num_op =0;
   num_msl=0; num_pat=0;
   % find the viking lander 1 and 2 sites on the cubed-spherical grid.
   for i=1:size(xc,1), for j=1:size(xc,2)
     if (abs(xc(i,j)-(-48.222))<180/ny_latlon && abs(yc(i,j)-22.697)<180/ny_latlon )
        fprintf('\n VL1 m=%d n=%d \n',i,j)
        num_vl1=num_vl1+1;
        nxcs_vl1(num_vl1)=i; nycs_vl1(num_vl1)=j;
     end;
     if (abs(xc(i,j)-134.2630)<180/ny_latlon && abs(yc(i,j)-47.967)<180/ny_latlon )
        fprintf('\n VL2 m=%d n=%d \n',i,j)
        num_vl2=num_vl2+1;
        nxcs_vl2(num_vl2)=i; nycs_vl2(num_vl2)=j;
     end;

     if (abs(xc(i,j))<180/ny_latlon && abs(yc(i,j)+2)<180/ny_latlon )
        fprintf('\n OP m=%d n=%d \n',i,j)
        num_op=num_op+1;
        nxcs_op(num_op)=i; nycs_op(num_op)=j;
     end;

     if (abs(xc(i,j)-137.4417)<180/ny_latlon && abs(yc(i,j)+4.5895)<180/ny_latlon )
        fprintf('\n MSL m=%d n=%d \n',i,j)
        num_msl=num_msl+1;
        nxcs_msl(num_msl)=i; nycs_msl(num_msl)=j;
     end;

     if (abs(xc(i,j)+33)<180/ny_latlon && abs(yc(i,j)-19)<180/ny_latlon )
        fprintf('\n PAT m=%d n=%d \n',i,j)
        num_pat=num_pat+1;
        nxcs_pat(num_pat)=i; nycs_pat(num_pat)=j;
     end;
   end; end;
end;

%------------------------------------------------
% initialize the surface arrays
dpdynNM1=zeros(nx,ny,length(alldata));
pcdynNM1=zeros(nx,ny,length(alldata));
psNM1   =zeros(nx,ny,length(alldata));
uENM1   =zeros(nx,ny,length(alldata));
vNNM1   =zeros(nx,ny,length(alldata));
tsk_all =zeros(nx,ny,length(alldata));
TdynNM1 =zeros(nx,ny,length(alldata));
TphyNM1 =zeros(nx,ny,length(alldata));


% section 3: main loop to process data, including zonal mean 
%            and time evolution

for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter),'%d');
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        % dynamics variables
        if (useMultiRec~='y') % single record
            recnum=0;
            time=allzero, timed=alldata(niter);  day(niter)=timed*deltaT/86400; 
            % this is earth day, nomalized to sol later
        else % multi-record
            time=0;
            if (multi_dataset=='y'), timed=alldata(niter), else, timed=myiter(recnum), end;
            day(niter)=timed*deltaT/86400;
            if (multi_dataset=='y'), recnum=niter; end;
        end;

        if (planet == 1)
           co2ice=load_bin_data(chdatapath,co2icestr,useMultiRec,time,recnum);
           if (tracer==1) 
              ptr = load_bin_data(chdatapath,ptrstr,useMultiRec,time,recnum);
              dust3d = load_bin_data(chdatapath,duststr,useMultiRec,time,recnum); 
              dustsed = load_bin_data(chdatapath,dustsedstr,useMultiRec,time,recnum);
           elseif (tracer==2)
              ptr = load_bin_data(chdatapath,ptrstr,useMultiRec,time,recnum);
              co2i3d = load_bin_data(chdatapath,co2istr,useMultiRec,time,recnum);
           elseif (tracer==3)
              ptr = load_bin_data(chdatapath,ptrstr,useMultiRec,time,recnum);
              co2i3d = load_bin_data(chdatapath,co2istr,useMultiRec,time,recnum);
              dust3d = load_bin_data(chdatapath,duststr,useMultiRec,time,recnum);
              dustsed = load_bin_data(chdatapath,dustsedstr,useMultiRec,time,recnum);
           elseif (tracer==4)
              ptr = load_bin_data(chdatapath,ptrstr,useMultiRec,time,recnum);
           end;
        end;
        u3d  = load_bin_data(chdatapath,ustr,useMultiRec,time,recnum);
        v3d  = load_bin_data(chdatapath,vstr,useMultiRec,time,recnum);
        Eta  = load_bin_data(chdatapath,Etastr,useMultiRec,time,recnum);
        tsk  = load_bin_data(chdatapath,tskstr,useMultiRec,time,recnum);
        theta= load_bin_data(chdatapath,thetastr,useMultiRec,time,recnum);

        % construct dynamics grid
        for k=1:nr
            pedyn(:,:,k)=aHybF(k)*rFullDepth+sigF(k)*(topoP+Eta);
            pcdyn(:,:,k)=aHybC(k)*rFullDepth+sigC(k)*(topoP+Eta);
        end;
        pedyn(:,:,nr+1)=sigF(nr+1)*(topoP+Eta);
        dpdyn(:,:,1:nr)=pedyn(:,:,1:nr)-pedyn(:,:,2:nr+1);

        for i=1:nx, for j=1:ny, for k=1:nr,
            if (dpdyn(i,j,k)<0) dpdyn(i,j,k)=0; end;
        end; end; end;

        % calculate the total air mass and surface co2 ice mass in kilograms--
        if (planet==1)
            totair (niter)=sum(sum((topoP+Eta).*rac/grv));  
            surfice(niter)=sum(sum(co2ice.*rac));
            if (tracer==1)
               Armassmean(niter)=sum(sum(sum(ptr.*dpdyn/grv,3).*rac))/sum(sum(rac));
               airdust   (niter)=sum(sum( squeeze(sum(dust3d.*dpdyn/grv,3)).*rac ));
               surfdust  (niter)=sum(sum(dustsed.*rac));
            elseif (tracer==2)
               Armassmean(niter)=sum(sum(sum(ptr.*dpdyn/grv,3).*rac))/sum(sum(rac));
               airice    (niter)=sum(sum( squeeze(sum(co2i3d.*dpdyn/grv,3)).*rac ));
            elseif (tracer==3)
               Armassmean(niter)=sum(sum(sum(ptr.*dpdyn/grv,3).*rac))/sum(sum(rac));
               airice    (niter)=sum(sum( squeeze(sum(co2i3d.*dpdyn/grv,3)).*rac ));
               airdust   (niter)=sum(sum( squeeze(sum(dust3d.*dpdyn/grv,3)).*rac ));
               surfdust  (niter)=sum(sum(dustsed.*rac));
            elseif (tracer==4)
               Armassmean(niter)=sum(sum(sum(ptr.*dpdyn/grv,3).*rac))/sum(sum(rac));
            end;
        end;

        % calculate the grid-point and polar-cap Ar/N2 mass density (kg/m^2)
        if ((planet==1) & (tracer>=1))
           Arlocal=ptr.*dpdyn/grv;

           ArpolarN=zeros(nx,ny);    ArpolarS=zeros(nx,ny);
           polarareaN=zeros(nx,ny);  polarareaS=zeros(nx,ny);
           areaN=0.0;

           % calculate the grid-point Ar and N2 mass in the polar caps (in kg).
           for i=1:nx, for j=1:ny, 
             for k=1:nr
                if (yc(i,j)>deg_pole) % center of the grid includes 'deg_pole'
                   ArpolarN(i,j)=ArpolarN(i,j)+Arlocal(i,j,k).*rac(i,j);
                   polarareaN(i,j)=rac(i,j);
                end;
                if (yc(i,j)<-deg_pole)
                   ArpolarS(i,j)=ArpolarS(i,j)+Arlocal(i,j,k).*rac(i,j);
                   polarareaS(i,j)=rac(i,j);
                end;
             end;
             if (yc(i,j)>80.0),  areaN=areaN+rac(i,j); end;
           end; end;

           % calculate the grid-point and area-weighted Ar and N2 mass mixing ratio in polar caps
           % note that ArpolarN, ArpolarS, N2polarN and N2polarS have dummy "zeros"
           % values outside of the polar caps. They have unit of kg m^2 at each grid point.
           % The global operation is only for convenience!!!
           %        ArpolarN=ArpolarN*grv./(topoP+Eta);
           %        ArpolarS=ArpolarS*grv./(topoP+Eta);
           % area weighted Ar and N2 mass mixing ratio, sum(d(mass_mixing_ratio)*d(rac))/sum(rac_polar)
           ArN(niter)=sum(sum(ArpolarN))/sum(sum(polarareaN.*(topoP+Eta)/grv));
           ArS(niter)=sum(sum(ArpolarS))/sum(sum(polarareaS.*(topoP+Eta)/grv));
           ArNmass(niter)=sum(sum(ArpolarN));
           ArSmass(niter)=sum(sum(ArpolarS));
           % (globally) column integrated grid-point mass mixing ratio and mass density for Ar and N2    
           Arq=sum(Arlocal,3)*grv./(topoP+Eta);
           Arm=sum(Arlocal,3);
        end;


        % calculate zonal mean temperature, winds and streamfunction-----------------------------

        % prepare hybrid grid for further diagnostics

        for k=1:nr
            hFacC(1:nx,1:ny,k)= 1; %(Ro+Eta)./Ro.*hfacc(:,:,k);
        end;

        hFacF_fake_dyn(1:nx,1:ny,1:nr+1)=1.0;

        % rotate U and V and calculate the zonal mean zonal wind
        [uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C');

        for k=1:nr
            Tdyn(:,:,k)=theta(:,:,k).*(pcdyn(:,:,k)/atm_Po).^atm_kappa;
        end;

        % Surface dynamics variables
        dpdynNM1(:,:,niter)=dpdyn(:,:,1);
        pcdynNM1(:,:,niter)=pcdyn(:,:,1);
        TdynNM1 (:,:,niter)=Tdyn (:,:,1);
        uENM1   (:,:,niter)=uE   (:,:,1);
        vNNM1   (:,:,niter)=vN   (:,:,1);
        psNM1   (:,:,niter)=pedyn(:,:,1);
        tsk_all (:,:,niter)=tsk;
        % Zonal means
        [Tdyn2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(Tdyn,ny_latlon,yc,rac,hFacC); 
        [U2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(uE,ny_latlon,yc,rac,hFacC);
        [V2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(vN,ny_latlon,yc,rac,hFacC);
        [pedyn2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(pedyn,ny_latlon,yc,rac,hFacF_fake_dyn);
        if (planet == 1)
           [ci2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(co2ice,ny_latlon,yc,rac,squeeze(hFacC(:,:,1)));
           if (tracer==1)
              [Arq2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(Arq,ny_latlon,yc,rac,hFacC);
              [Arm2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(Arm,ny_latlon,yc,rac,hFacC);
              [sed2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(dustsed,ny_latlon,yc,rac,squeeze(hFacC(:,:,1)));
              [dustcol2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(squeeze(sum(dust3d.*dpdyn/grv,3)), ... 
                                                   ny_latlon,yc,rac,squeeze(hFacC(:,:,1)));
              [dust2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(dust3d,ny_latlon,yc,rac,hFacC);
           elseif (tracer==2)
              [Arq2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(Arq,ny_latlon,yc,rac,hFacC);
              [Arm2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(Arm,ny_latlon,yc,rac,hFacC);
              [co2icol2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(squeeze(sum(max(co2i3d,0).*dpdyn/grv,3)), ...
                                                   ny_latlon,yc,rac,squeeze(hFacC(:,:,1)));
              [co2i2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(max(co2i3d,0),ny_latlon,yc,rac,hFacC);
           elseif (tracer==3)
              [Arq2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(Arq,ny_latlon,yc,rac,hFacC);
              [Arm2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(Arm,ny_latlon,yc,rac,hFacC);
              [dustcol2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(squeeze(sum(dust3d.*dpdyn/grv,3)), ...
                                                   ny_latlon,yc,rac,squeeze(hFacC(:,:,1)));
              [co2icol2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(squeeze(sum(max(co2i3d,0).*dpdyn/grv,3)), ...
                                                   ny_latlon,yc,rac,squeeze(hFacC(:,:,1)));
              [co2i2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(max(co2i3d,0),ny_latlon,yc,rac,hFacC);
              [sed2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(dustsed,ny_latlon,yc,rac,squeeze(hFacC(:,:,1)));
              [dust2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(dust3d,ny_latlon,yc,rac,hFacC);
           elseif (tracer==4)
              [Arq2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(Arq,ny_latlon,yc,rac,hFacC);
              [Arm2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(Arm,ny_latlon,yc,rac,hFacC);
           end;
        end;

        [zc,dz]=cal_height(theta, pedyn, topoZ, phys_para);
        hFacz(1:nx,1:ny,1:nr)=1.0;
        [z2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(zc,ny_latlon,yc,rac,hFacz);

        pc2d=(pedyn2dtmp(:,1:nr)+pedyn2dtmp(:,2:nr+1))/2.0;
        dp2d=(pedyn2dtmp(:,1:nr)-pedyn2dtmp(:,2:nr+1));

        if (plotstream == 'y')
           [psi]=use_bk_line_mars(uE,vN,dxg,dyg,rac,dp2d,namf_cs,grv);
        else
           'no options';
        end;
   
        % move to permanent storage
        T2dt(:,:,niter)=Tdyn2dtmp;   % Temperature [K], (p,y,ls)
        U2dt(:,:,niter)=U2dtmp;   % U, (p,y,ls) 
        V2dt(:,:,niter)=V2dtmp;   % V, (p,y,ls)
        p2dt(:,:,niter)=pc2d;
        pe2dt(:,:,niter)=pedyn2dtmp;
        z2dt(:,:,niter)=z2d/1e3; % height in km
        if (planet == 1) 
           ArVL2(niter)=Arq(nxcs_vl2(end),nycs_vl2(end));
           Ar2d(:,niter)=Arq2d;  % Ar vertically integrated mixing ratio
           Ar2m(:,niter)=Arm2d;  % Ar zonal mean vertically integrated total mass
           ci2dt(:,niter)=squeeze(ci2dtmp); 
           if    (tracer==1), ArVL2(niter)=Arq(nxcs_vl2(end),nycs_vl2(end));
                              Ar2d(:,niter)=Arq2d;  % Ar vertically integrated mixing ratio
                              Ar2m(:,niter)=Arm2d;  % Ar zonal mean vertically integrated total mass
                              dustsed2dt(:,niter) = squeeze(sed2dtmp);
                              dust2dt(:,:,niter) = squeeze(dust2dtmp);
                              dustcol2dt(:,niter) = squeeze(dustcol2dtmp);
           elseif(tracer==2), ArVL2(niter)=Arq(nxcs_vl2(end),nycs_vl2(end));
                              Ar2d(:,niter)=Arq2d;  % Ar vertically integrated mixing ratio
                              Ar2m(:,niter)=Arm2d;  % Ar zonal mean vertically integrated total mass
                              co2i2dt(:,:,niter)=squeeze(co2i2dtmp); 
                              co2icol2dt(:,niter) = squeeze(co2icol2dtmp);
           elseif(tracer==3), ArVL2(niter)=Arq(nxcs_vl2(end),nycs_vl2(end));
                              Ar2d(:,niter)=Arq2d;  % Ar vertically integrated mixing ratio
                              Ar2m(:,niter)=Arm2d;  % Ar zonal mean vertically integrated total mass
                              co2i2dt(:,:,niter)=squeeze(co2i2dtmp);
                              dustsed2dt(:,niter) = squeeze(sed2dtmp);
                              dust2dt(:,:,niter) = squeeze(dust2dtmp);
                              dustcol2dt(:,niter) = squeeze(dustcol2dtmp);
                              co2icol2dt(:,niter) = squeeze(co2icol2dtmp);
           elseif(tracer==4), ArVL2(niter)=Arq(nxcs_vl2(end),nycs_vl2(end));
                              Ar2d(:,niter)=Arq2d;  % Ar vertically integrated mixing ratio
                              Ar2m(:,niter)=Arm2d;  % Ar zonal mean vertically integrated total mass
           end;
        end;

        if (plotstream == 'y')
           psi2dt(:,:,niter)=-psi(2:ny_latlon+1,2:nr+1); % psi< 0 for clockwise flow
        else
           'no options';
        end;

        % calculate ls  (solar longitude) -------
        julian=day(niter)/p2si;
        date_dbl = julian - zero_date;
        while (date_dbl < 0.)
           date_dbl=date_dbl+planet_year;
        end;
        while (date_dbl > planet_year)
           date_dbl=date_dbl-planet_year;
        end;


       %  determine true anomaly at current date:  w
       %  Iteration for w
       em = 2.0 * pi * date_dbl/planet_year;
       e  = 1.0;
       cd0 = 1.0;
       while (cd0 > SMALL_VALUE)
       ep = e-(e-eccentricity*sin(e)-em) / (1.0-eccentricity*cos(e));
       cd0 = abs(e-ep);
       e = ep;
       end;
       w = 2.0 * atan( er * tan(0.5*e) );
 
       %  Radius vector ( astronomical units:  AU )
       als= (w - eq)*180.0/pi;      %Aerocentric Longitude
       if (als < 0.0) 
          als=als+360.0;
       end;
       ls(niter) = als;

end;

% finally expand latitude to 2d        
for k=1:nr;  y2d(1:ny_latlon,k)=ylat; end;

 
fprintf('Zonal mean Temperature vs Ls, T2dt [ %d x %d x %d ]:  \n',ny_latlon,nr,niter);
fprintf('Zonal mean zonal wind vs Ls, U2dt [ %d x %d x %d ]:  \n',ny_latlon,nr,niter);
fprintf('Zonal mean streamfunction vs Ls, psi2dt [ %d x %d x %d ]:  \n',ny_latlon,nr,niter);

if (multi_steps == 'y')
   T2dm=sum(T2dt,3)/length(alldata);
   U2dm=sum(U2dt,3)/length(alldata);
   V2dm=sum(V2dt,3)/length(alldata);
   z2dm=sum(z2dt,3)/length(alldata);
   p2dm=sum(p2dt,3)/length(alldata);
   ci2dm=sum(ci2dt,3)/length(alldata);
   if (plotstream == 'y')
       psi2dm=sum(psi2dt,3)/length(alldata);
   end;
   fprintf('Time averaged dataset requested : T2dm, U2dm, V2dm, z2dm, p2dm, psi2dm \n');
end;

