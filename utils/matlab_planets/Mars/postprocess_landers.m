
% VL1/VL2 pressure
alt_vl1=-1.5e3; % VL1 is 1.5km below datum
alt_vl2=-3.0e3; % VL2 is 3.0km below datum
alt_op =-1.38e3; 
alt_msl=-4.5e3;
alt_pat=0.0;

patvl1(1:length(alldata))=0;
patvl2(1:length(alldata))=0;
patop (1:length(alldata))=0;
patmsl(1:length(alldata))=0;
patpat(1:length(alldata))=0;

for i=1:num_vl1
  topo_vl1(i)=topoZ(nxcs_vl1(i), nycs_vl1(i));
  area_vl1(i)=rac  (nxcs_vl1(i), nycs_vl1(i));
end;
for i=1:num_vl2
  topo_vl2(i)=topoZ(nxcs_vl2(i), nycs_vl2(i));
  area_vl2(i)=rac  (nxcs_vl2(i), nycs_vl2(i));
end;
for i=1:num_op
  topo_op(i)=topoZ(nxcs_op(i), nycs_op(i));
  area_op(i)=rac  (nxcs_op(i), nycs_op(i));
end;
for i=1:num_msl
  topo_msl(i)=topoZ(nxcs_msl(i), nycs_msl(i));
  area_msl(i)=rac  (nxcs_msl(i), nycs_msl(i));
end;
for i=1:num_pat
  topo_pat(i)=topoZ(nxcs_pat(i), nycs_pat(i));
  area_pat(i)=rac  (nxcs_pat(i), nycs_pat(i));
end;

for niter=1:length(alldata)
 for i=1:num_vl1
  patvl1(niter)=patvl1(niter)+psNM1(nxcs_vl1(i), nycs_vl1(i),niter);
%                patvl1(niter)+psNM1(nxcs_vl1(i), nycs_vl1(i),niter) ...
%               *exp(-(alt_vl1-topo_vl1(i))*grv ...
%               /(TphyNM1(nxcs_vl1(i), nycs_vl1(i),niter)*atm_cp*atm_kappa)) ... 
%               *area_vl1(i);

 end;
 for i=1:num_vl2
  patvl2(niter)=patvl2(niter)+psNM1(nxcs_vl2(i), nycs_vl2(i),niter);
%              patvl2(niter)+psNM1(nxcs_vl2(i), nycs_vl2(i),niter) ...
%               *exp(-(alt_vl2-topo_vl2(i))*grv ...
%               /(TphyNM1(nxcs_vl2(i), nycs_vl2(i),niter)*atm_cp*atm_kappa)) ...
%               *area_vl2(i);

 end; 
 for i=1:num_op
  patop(niter)=patop(niter)+psNM1(nxcs_op(i), nycs_op(i),niter) ...
               *exp(-(alt_op-topo_op(i))*grv ...
               /(TphyNM1(nxcs_op(i), nycs_op(i),niter)*atm_cp*atm_kappa)) ...
               *area_op(i);
 end;
 for i=1:num_msl
  patmsl(niter)=patmsl(niter)+psNM1(nxcs_msl(i), nycs_msl(i),niter) ...
               *exp(-(alt_msl-topo_msl(i))*grv ...
               /(TphyNM1(nxcs_msl(i), nycs_msl(i),niter)*atm_cp*atm_kappa)) ...
               *area_msl(i);
 end;
 for i=1:num_pat
  patpat(niter)=patpat(niter)+psNM1(nxcs_pat(i), nycs_pat(i),niter) ...
               *exp(-(alt_pat-topo_pat(i))*grv ...
               /(TphyNM1(nxcs_pat(i), nycs_pat(i),niter)*atm_cp*atm_kappa)) ...
               *area_pat(i);
 end;
end;

patvl1=patvl1/num_vl1; %/sum(area_vl1);
patvl2=patvl2/num_vl2; %/sum(area_vl2);
patop =patop /sum(area_op);
patmsl=patmsl/sum(area_msl);
patpat=patpat/sum(area_pat);

