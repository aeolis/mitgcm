% first ntry=12 layers are in physics grid, max(ksurf) and above are in dynamics grid
% The hybrid grid is for zonal mean of temperature, winds and stream function only!!!
 

clear all;

% section 1: load grid files and data configurations
chdatapath=input('full path to load data /~/ : ','s');
ny_latlon=input('grid points in y in latlon grid: ' );
deltaT=input('time step: ');
multi_dataset=input('multiple data sets (y/n)? ','s');
useMultiRec=input('Use multiple-record data(y/n)? ','s');

if(isempty(multi_dataset)||multi_dataset~='y') multi_dataset='n'; end;
if(isempty(useMultiRec)||useMultiRec~='y') useMultiRec='n'; end;

% find dump frequency
fid=fopen(sprintf('%s%s',chdatapath,'data'));
while ~feof(fid)
     aa=fgetl(fid); aa=strtrim(aa); xx=strfind(aa,'dumpFreq');
     if(~isempty(xx) && aa(1)~='#') bb=textscan(aa,'%s');
       cc=(strrep(bb{1},'dumpFreq=','')); dumpfreq=str2num(cc{1});
     end;
end;
fclose(fid); clear aa bb cc xx;

if (multi_dataset~='y') recnum=input('the record number to be accessed: '); 
else recnum=[]; end;

if (useMultiRec=='y')
   fid=fopen(sprintf('%s%s',chdatapath,'U.meta')); 
else
   tstart=input('starting time step: ');
   str0='0000000000'; str1=num2str(tstart);
   str0(length(str0)-length(str1)+1:end)=str1;
   fid=fopen(sprintf('%s%s%s%s',chdatapath,'U.',str0,'.meta')); 
end;

% read record info from meta file
while ~feof(fid)
     aa=fgetl(fid); xx1=strfind(aa,'nrecords'); xx2=strfind(aa,'timeStepNumber');
     if(~isempty(xx1)) bb1=textscan(aa,'%s%s%s%d%s'); nrecords=double(bb1{4}); end;
     if(~isempty(xx2)) bb2=textscan(aa,'%s%s%s%d%s'); tend=double(bb2{4}); end;
end;
fclose(fid); clear aa xx1 xx2 bb1 bb2;

if (useMultiRec=='y')
   tstart=tend-(nrecords-1)*floor(dumpfreq/deltaT);
   myiter(1)=tstart;
   for i=2:nrecords myiter(i)=tstart+(i-1)*dumpfreq/deltaT; end;
   fprintf('%s%d\n','nrecords(end)=',nrecords);
   fprintf('%s%d\n','myIter(1)=',tstart);
   fprintf('%s%d\n','myIter(end)=',tend);
   dnout=floor((tend-tstart)/max(1,nrecords-1)); % or dumpfreq/deltaT;
else
   dnout=max(1,floor(dumpfreq/deltaT));
end;

multi_steps=input('time average over multiple dataset (y/n): ', 's');
plotstream=input('plot meridional stream functions (y/n): ', 's');
planet=input('select planet Mars 1, Titan 2 :  ' );
tracer=input('select: 1 Ar+dust, 2 Ar+co2 ice, 3 Ar+co2 ice+dust, 4 Ar :  ' );

rac=rdmds([chdatapath,'RAC']);
dxg=rdmds([chdatapath,'DXG']);
dyg=rdmds([chdatapath,'DYG']);
xg=rdmds([chdatapath,'XG']);
yg=rdmds([chdatapath,'YG']);
xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);
hfacc=rdmds([chdatapath,'hFacC']);
drF=rdmds([chdatapath,'DRF']); drF=squeeze(drF);
sigC=rdmds([chdatapath,'BHybSigC']); sigC=squeeze(sigC);
sigF=rdmds([chdatapath,'BHybSigF']); sigF=squeeze(sigF);
aHybC=rdmds([chdatapath,'AHybSigC']); aHybC=squeeze(aHybC);
aHybF=rdmds([chdatapath,'AHybSigF']); aHybF=squeeze(aHybF);
topoP=rdmds([chdatapath,'topo_P']);
topoZ=rdmds([chdatapath,'topo_H']);

dxr=180.0/ny_latlon; %0.703125;
dyr=180.0/ny_latlon; %0.703125;
xi=-180+dxr/2:dxr:180-dxr/2;
yi=-90+dxr/2:dxr:90-dxr/2;
lx=length(xi);
ly=length(yi);

[del] = cube2latlon_preprocess(xc,yc,xi,yi);


g2d(:,:,1)=xc;   g2d(:,:,2)=yc;   g2d(:,:,3)=xg; g2d(:,:,4)=yg;
g2d(:,:,5)=dxg;  g2d(:,:,6)=dyg;  g2d(:,:,7)=rac;

rFullDepth=sum(drF);

% section 2: preprocessing for grid structure and grid points required for 
% viking lander sites as well as the Mars physics parameters

if (plotstream == 'y')
   [namf_cs]=make_psi_lines(g2d, ny_latlon);
else
   'no options'
end;

[AngleCS,AngleSN] = cubeCalcAngle(yg,rac,dxg,dyg);

for k=1:(tend-tstart)/dnout+1, nstep(k)=tstart+dnout*(k-1); end;

if (multi_dataset=='y') 
   alldata=nstep;
else 
   if isempty(recnum), recnum=nrecords; end;
   alldata=myiter(recnum); 
end;

% Mars physics parameters, including gravity, atmospheric properties and topography
if (isempty(planet) || planet==1)

   grv=3.727; atm_Po=610.0; atm_cp=767.35; atm_kappa=0.25;
   zlow=-6.5470e3; ztop=80.0e3; rsphere=3389.9E3;

   % define some constants for calculating the solar angle l_s
   p2si=1.0274912;     equinox_fraction= 0.2695; planet_year=669.0;
   zero_date=488.7045; eccentricity=0.09341233;  semimajoraxis= 1.52366231;
 
   % polar average, some 'deg_pole' poleward.
   deg_pole=75;

elseif (planet == 2)

   grv=1.358; atm_Po=1.5E5; atm_cp=1044.0; atm_kappa=0.27778;
   zlow=0; ztop=250.0e3;  rsphere=2575.0E3;

   p2si=15.95;       equinox_fraction= 0.2098; planet_year=686.0;
   zero_date=542.06; eccentricity=0.056;       semimajoraxis= 9.58256;

end;

drsphere=rsphere*(1.0/ny_latlon)*pi;

phys_para(1)=atm_Po;    phys_para(2)=atm_cp;
phys_para(3)=atm_kappa; phys_para(4)=grv;

% calculate the solar longitude.
SMALL_VALUE=1.0e-6;
deleqn = equinox_fraction * planet_year;
qq = 2.0 * (pi * deleqn / planet_year);
er = sqrt( (1.0+eccentricity)/(1.0-eccentricity) );
%  determine true anomaly at equinox:  eq
%  Iteration for eq
e = 1.0;  cd0 = 1.0;
while (cd0 > SMALL_VALUE)
      ep = e - (e-eccentricity*sin(e)-qq) / (1.0-eccentricity*cos(e));
      cd0 = abs(e-ep);  e = ep;
end;
eq = 2.0 * atan( er * tan(0.5*e) );

% load all data need to be processed, including winds, ice, tracers and temperature

% dynamics grid
ustr='U'; vstr='V'; thetastr='T';       % potential temperature
Etastr='Eta'; tskstr='TSK';
if (planet==1) co2icestr='co2ice'; end;
if (tracer==1) ptrstr='PTRACER01'; duststr ='PTRACER02'; dustsedstr='dustsed01'; end;
if (tracer==2) ptrstr='PTRACER01'; co2istr='PTRACER02'; end;
if (tracer==3) ptrstr='PTRACER01'; co2istr='PTRACER02'; duststr ='PTRACER03'; dustsedstr='dustsed01'; end; 
if (tracer==4) ptrstr='PTRACER01'; end;


% find array size -------------------------------
dims=size(topoP); nx=dims(1); ny=dims(2);
rC=rdmds([chdatapath,'RC']); rC=squeeze(rC);  nr=length(rC); 

if (isempty(topoZ)) topoZ=zeros(nx,ny); end;

% number of surrounding points at MSL
if (planet==1)
   num_vl1=0; num_vl2=0; num_op =0;
   num_msl=0; num_pat=0;
   % find the viking lander 1 and 2 sites on the cubed-spherical grid.
   for i=1:size(xc,1), for j=1:size(xc,2)
     if (abs(xc(i,j)-(-48.222))<180/ny_latlon && abs(yc(i,j)-22.697)<180/ny_latlon )
        fprintf('\n VL1 m=%d n=%d \n',i,j)
        num_vl1=num_vl1+1;
        nxcs_vl1(num_vl1)=i; nycs_vl1(num_vl1)=j;
     end;
     if (abs(xc(i,j)-134.2630)<180/ny_latlon && abs(yc(i,j)-47.967)<180/ny_latlon )
        fprintf('\n VL2 m=%d n=%d \n',i,j)
        num_vl2=num_vl2+1;
        nxcs_vl2(num_vl2)=i; nycs_vl2(num_vl2)=j;
     end;

     if (abs(xc(i,j))<180/ny_latlon && abs(yc(i,j)+2)<180/ny_latlon )
        fprintf('\n OP m=%d n=%d \n',i,j)
        num_op=num_op+1;
        nxcs_op(num_op)=i; nycs_op(num_op)=j;
     end;

     if (abs(xc(i,j)-137.4417)<180/ny_latlon && abs(yc(i,j)+4.5895)<180/ny_latlon )
        fprintf('\n MSL m=%d n=%d \n',i,j)
        num_msl=num_msl+1;
        nxcs_msl(num_msl)=i; nycs_msl(num_msl)=j;
     end;

     if (abs(xc(i,j)+33)<180/ny_latlon && abs(yc(i,j)-19)<180/ny_latlon )
        fprintf('\n PAT m=%d n=%d \n',i,j)
        num_pat=num_pat+1;
        nxcs_pat(num_pat)=i; nycs_pat(num_pat)=j;
     end;
   end; end;
end;


% section 3: main loop to process data, including zonal mean 
%            and time evolution

% create NETCDF file


for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter),'%d');
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        % dynamics variables
        if (useMultiRec~='y') % single record
            recnum=0;
            time=allzero, timed=alldata(niter);  day(niter)=timed*deltaT/86400; 
            % this is earth day, nomalized to sol later
        else % multi-record
            time=0;
            if (multi_dataset=='y'), timed=alldata(niter), else, timed=myiter(recnum), end;
            day(niter)=timed*deltaT/86400;
            if (multi_dataset=='y'), recnum=niter; end;
        end;

        if (planet == 1)
           co2ice=load_bin_data(chdatapath,co2icestr,useMultiRec,time,recnum);
           if (tracer==1) 
              ptr = load_bin_data(chdatapath,ptrstr,useMultiRec,time,recnum);
              dust3d = load_bin_data(chdatapath,duststr,useMultiRec,time,recnum); 
              dustsed = load_bin_data(chdatapath,dustsedstr,useMultiRec,time,recnum);
           elseif (tracer==2)
              ptr = load_bin_data(chdatapath,ptrstr,useMultiRec,time,recnum);
              co2i3d = load_bin_data(chdatapath,co2istr,useMultiRec,time,recnum);
           elseif (tracer==3)
              ptr = load_bin_data(chdatapath,ptrstr,useMultiRec,time,recnum);
              co2i3d = load_bin_data(chdatapath,co2istr,useMultiRec,time,recnum);
              dust3d = load_bin_data(chdatapath,duststr,useMultiRec,time,recnum);
              dustsed = load_bin_data(chdatapath,dustsedstr,useMultiRec,time,recnum);
           elseif (tracer==4)
              ptr = load_bin_data(chdatapath,ptrstr,useMultiRec,time,recnum);
           end;
        end;
        u3d  = load_bin_data(chdatapath,ustr,useMultiRec,time,recnum);
        v3d  = load_bin_data(chdatapath,vstr,useMultiRec,time,recnum);
        Eta  = load_bin_data(chdatapath,Etastr,useMultiRec,time,recnum);
        tsk  = load_bin_data(chdatapath,tskstr,useMultiRec,time,recnum);
        theta= load_bin_data(chdatapath,thetastr,useMultiRec,time,recnum);

        % construct dynamics grid
        for k=1:nr
            pedyn(:,:,k)=aHybF(k)*rFullDepth+sigF(k)*(topoP+Eta);
            pcdyn(:,:,k)=aHybC(k)*rFullDepth+sigC(k)*(topoP+Eta);
        end;
        pedyn(:,:,nr+1)=sigF(nr+1)*(topoP+Eta);
        dpdyn(:,:,1:nr)=pedyn(:,:,1:nr)-pedyn(:,:,2:nr+1);

        for i=1:nx, for j=1:ny, for k=1:nr,
            if (dpdyn(i,j,k)<0) dpdyn(i,j,k)=0; end;
        end; end; end;

        [uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C');

        for k=1:nr
            Tdyn(:,:,k)=theta(:,:,k).*(pcdyn(:,:,k)/atm_Po).^atm_kappa;
        end;

        [zc,dz]=cal_height(theta, pedyn, topoZ, phys_para);
        hFacz(1:nx,1:ny,1:nr)=1.0;


        [u3d_latlon,v3d_latlon]=uvcube2latlongrid(del,u3d,v3d,xg,yg,rac,dxg,dyg);
        T3d_latlon=cube2latlon_fast(del,Tdyn);
        p3d_latlon=cube2latlon_fast(del,pcdyn);
        z3d_latlon=cube2latlon_fast(del,zc);

        % move to permanent storage
        u4d(:,:,:,niter)=u3d_latlon; 
        v4d(:,:,:,niter)=v3d_latlon;   
        p4d(:,:,:,niter)=p3d_latlon;
        t4d(:,:,:,niter)=T3d_latlon;
        z4d(:,:,:,niter)=z3d_latlon/1e3; % height in km

        % calculate ls  (solar longitude) -------
        julian=day(niter)/p2si;
        date_dbl = julian - zero_date;
        while (date_dbl < 0.)
           date_dbl=date_dbl+planet_year;
        end;
        while (date_dbl > planet_year)
           date_dbl=date_dbl-planet_year;
        end;


       %  determine true anomaly at current date:  w
       %  Iteration for w
       em = 2.0 * pi * date_dbl/planet_year;
       e  = 1.0;
       cd0 = 1.0;
       while (cd0 > SMALL_VALUE)
       ep = e-(e-eccentricity*sin(e)-em) / (1.0-eccentricity*cos(e));
       cd0 = abs(e-ep);
       e = ep;
       end;
       w = 2.0 * atan( er * tan(0.5*e) );
 
       %  Radius vector ( astronomical units:  AU )
       als= (w - eq)*180.0/pi;      %Aerocentric Longitude
       if (als < 0.0) 
          als=als+360.0;
       end;
       ls(niter) = als;

end;

write_netcdf (u4d, v4d, p4d, t4d, z4d, xi, yi, ls, chdatapath );

