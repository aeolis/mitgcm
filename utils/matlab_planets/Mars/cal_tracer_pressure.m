% first ntry=12 layers are in physics grid, max(ksurf) and above are in dynamics grid
% The hybrid grid is for zonal mean of temperature, winds and stream function only!!!
 

clear all;

% section 1: load grid files and data configurations
chdatapath=input('full path to load data /~/ : ','s');
ny_latlon=input('grid points in y in latlon grid: ' );
deltaT=input('time step: ');
fracstep=input('fractional dumping frequency (y/n): ','s');
if (fracstep ~='y')
    tstart=input('starting time step: ');
    tend=input('ending time steps: ');
    dnout=input('Output interval in number of time steps: ');
end;
h2o=input('With or without water cycle (y/n): ', 's');
dyn_phys=input('physics grid ? (y/n): ','s'); 
multi_steps=input('time average over multiple dataset (y/n): ', 's');

rac=rdmds([chdatapath,'RAC']);
dxg=rdmds([chdatapath,'DXG']);
dyg=rdmds([chdatapath,'DYG']);
xg=rdmds([chdatapath,'XG']);
yg=rdmds([chdatapath,'YG']);
xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);
hfacc=rdmds([chdatapath,'hFacC']);
drF=rdmds([chdatapath,'DRF']); drF=squeeze(drF);
topoP=rdmds([chdatapath,'topo_P']);
topoZ=rdmds([chdatapath,'topo_H']);

g2d(:,:,1)=xc; g2d(:,:,2)=yc; g2d(:,:,3)=xg; g2d(:,:,4)=yg;
g2d(:,:,5)=dxg; 
g2d(:,:,6)=dyg; 
g2d(:,:,7)=rac;

% section 2: preprocessing for grid structure and grid points required for 
% viking lander sites as well as the Mars physics parameters


% polar average, some 'deg_pole' poleward.
deg_pole=75;

if (fracstep == 'y')

   load alldata;

%{
   starttime=(tstart-tstart)*deltaT;
   endtime=(tend-tstart)*deltaT;

   mytime=starttime;
   nTimeSteps=0;
   nrecord=0;
   nstep(1)=tstart;
   while (mytime <= endtime)
%         o This case is more complex because of round-off error
          v1 = mytime;
          v2 = mytime - deltaT;
          v3 = mytime + deltaT;

%         Test v1 to see if its a "closest multiple"
          v4 = round(v1/dnout)*dnout;
          d1 = v1-v4;
          d2 = v2-v4;
          d3 = v3-v4;
          if ( (abs(d1) < abs(d2)) && (abs(d1) <= abs(d3)) )
             nrecord = nrecord + 1;
             nstep(nrecord) = nstep(1)+nTimeSteps;
          end;
          nTimeSteps = nTimeSteps + 1;
          mytime = nTimeSteps * deltaT;  
   end;

%}

else

   for k=1:(tend-tstart)/dnout+1
       nstep(k)=tstart+dnout*(k-1);
   end;
   alldata=nstep;
end;


% find the viking lander 1 and 2 sites on the cubed-spherical grid.
for i=1:size(xc,1)
 for j=1:size(xc,2)
     if (abs(xc(i,j)-(-48.222))<180/ny_latlon/2 && abs(yc(i,j)-22.697)<180/ny_latlon/2 )
        fprintf('\n VL1 m=%d n=%d \n',i,j)
        nxcs_vl1=i; nycs_vl1=j;
%     elseif (abs(xc(i,j)-(-48.222))<180/ny_latlon && abs(yc(i,j)-22.697)<180/ny_latlon )
%        fprintf('\n VL1 m=%d n=%d \n',i,j)
%        nxcs_vl1=i; nycs_vl1=j;
     end;
     if (abs(xc(i,j)-134.2630)<180/ny_latlon/2 && abs(yc(i,j)-47.967)<180/ny_latlon/2 )
        fprintf('\n VL2 m=%d n=%d \n',i,j)
        nxcs_vl2=i; nycs_vl2=j;
     elseif (abs(xc(i,j)-134.2630)<180/ny_latlon && abs(yc(i,j)-47.967)<180/ny_latlon )
        fprintf('\n VL2 m=%d n=%d \n',i,j)
        nxcs_vl2=i; nycs_vl2=j;
     end;

     if (abs(xc(i,j))<180/ny_latlon/2 && abs(yc(i,j)+2)<180/ny_latlon/2 )
        fprintf('\n OP m=%d n=%d \n',i,j)
        nxcs_op=i; nycs_op=j;
     end;
    
     if (abs(xc(i,j)-137.5)<180/ny_latlon/2 && abs(yc(i,j)+4.5)<180/ny_latlon/2 )
        fprintf('\n MSL m=%d n=%d \n',i,j)
        nxcs_msl=i; nycs_msl=j;
     end;
 end;
end;

% Mars physics parameters, including gravity, atmospheric properties and topography
grv=3.727;
atm_Po=610.0;
atm_cp=767.35;
atm_kappa=0.25;
zlow=-6.5470e3; % 2.5 degree res only
ztop=80.0e3; % set maximum altitude to 80km
rsphere=3389.9E3;
drsphere=rsphere*(1.0/ny_latlon)*pi;

phys_para(1)=atm_Po;
phys_para(2)=atm_cp;
phys_para(3)=atm_kappa;
phys_para(4)=grv;

% define some constants for calculating the solar angle l_s
p2si=1.0274912;
equinox_fraction= 0.2695;
planet_year=669.0;
zero_date=488.7045;
eccentricity=0.09341233;
semimajoraxis= 1.52366231;
SMALL_VALUE=1.0e-6;
deleqn = equinox_fraction * planet_year;
qq = 2.0 * (pi * deleqn / planet_year);
er = sqrt( (1.0+eccentricity)/(1.0-eccentricity) );
%  determine true anomaly at equinox:  eq
%  Iteration for eq
   e = 1.0;
   cd0 = 1.0;
   while (cd0 > SMALL_VALUE)
      ep = e - (e-eccentricity*sin(e)-qq) / (1.0-eccentricity*cos(e));
      cd0 = abs(e-ep);
      e = ep;
   end;
   eq = 2.0 * atan( er * tan(0.5*e) );

% area of zonal band on a sphere
dylat=180/ny_latlon;
ylat=[-90+dylat/2:dylat:90-dylat/2];
for i=1:ny_latlon
 das(i)=2*pi*rsphere^2*sin(ylat(i)*pi/180+pi/2)*(pi/ny_latlon);
end;

% load all data need to be processed, including winds, ice, tracers and temperature

% dynamics grid
ustr='U';
vstr='V';
cistr='co2ice';     % surface ice
if (h2o=='y')
   histr='h2oice';
end;
%castr='co2sub_air'; % ice in the atmosphere
ptrstr='PTRACER01'; % Ar 
thetastr='T';       % potential temperature
pedstr='pedyn';     % pressure field (co2+N2)
Etastr='Eta';       % surface pressure anomaly
tskstr='TSK';

% physics grid
uphystr='uphy';
vphystr='vphy';
thphystr='thphy';
pephystr='pephy';

if (h2o=='y')
qvcstr='qv_column';
qicstr='qi_column';

qvdynstr='PTRACER02';
qidynstr='PTRACER04';

qvphystr='trphy';
end;
% reconstruct hybrid grid: lower physics grid + higher dynamics grid
% so the pressure field is in consistent with level numbers.
hFac(1:size(topoZ,1),1:size(topoZ,2))=1; 
% find levels for new grid ----------------------

% physics grid part
nphys=12;

% before start calculation, find ksurf
% find array size -------------------------------

rC=rdmds([chdatapath,'RC']);
rC=squeeze(rC); 

dims=size(topoP);
nx=dims(1); ny=dims(2); nr=length(rC); 

% dynamics grid part
ksurf(1:nx,1:ny)=1;

for i=1:nx
  for j=1:ny
    kindex(1:nr)=nr+1;
    for k=1:nr-1
       if (hfacc(i,j,k)>0)                                    
          kindex(k)=k;
       end;
    end;
    ksurf(i,j)=min(kindex);
  end;
end;

for i=1:nx
  for j=1:ny
    for k=1:nr
       if (k<ksurf(i,j))
          mask(i,j,k)=NaN;
       else
          mask(i,j,k)=1;
       end;
    end;
  end;
end;

kmax=max(max(ksurf));

%------------------------------------------------

% section 3: main loop to process data, including zonal mean 
%            and time evolution

current_year=1;

for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter));
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        time=allzero
        timed=alldata(niter);
        day(niter)=timed*deltaT/86400; % this is earth day, nomalized to sol later
% dynamics grid variables
        cifile=sprintf('%s%s.%s',chdatapath,cistr,time);
        if (h2o=='y')
           hifile=sprintf('%s%s.%s',chdatapath,histr,time);
        end;
        %cafile=sprintf('%s%s.%s',chdatapath,castr,time);
        ptrfile=sprintf('%s%s.%s',chdatapath,ptrstr,time);
        pedynfile=sprintf('%s%s.%s',chdatapath,pedstr,time);
        Etafile=sprintf('%s%s.%s',chdatapath,Etastr,time);
        thetafile=sprintf('%s%s.%s',chdatapath,thetastr,time);
        tskfile=sprintf('%s%s.%s',chdatapath,tskstr,time);

        ci=rdmds(cifile);
        if (h2o=='y')
           hi=rdmds(hifile);
        end;
        %ca=rdmds(cafile);
        ptr=rdmds(ptrfile);
        pedyn=rdmds(pedynfile);
        Eta=rdmds(Etafile);
        theta=rdmds(thetafile);
        tsk=rdmds(tskfile);

        if(h2o=='y')
          qvcfile=sprintf('%s%s.%s',chdatapath,qvcstr,time);
          qicfile=sprintf('%s%s.%s',chdatapath,qicstr,time);
          qvdynfile=sprintf('%s%s.%s',chdatapath,qvdynstr,time);
          qidynfile=sprintf('%s%s.%s',chdatapath,qidynstr,time);
          qvphyfile=sprintf('%s%s.%s',chdatapath,qvphystr,time);
          qvc=rdmds(qvcfile);
          qic=rdmds(qicfile);
          qvdyn=rdmds(qvdynfile); qidyn=rdmds(qidynfile);
          if (dyn_phys =='y') qvphy=rdmds(qvphyfile); end;


% convert pr um to kg/m^2: 1 pr um = 10^-4 g/cm^2 = 10^-3 kg/m^2
          qvc=1.e-3*qvc;
          qic=1.e-3*qic;
        end;

% easy one first, calculate the VL1 and VL2 pressure curve
        patvl1(niter)=topoP(nxcs_vl1,nycs_vl1)+Eta(nxcs_vl1,nycs_vl1);
        patvl2(niter)=topoP(nxcs_vl2,nycs_vl2)+Eta(nxcs_vl2,nycs_vl2);
        patop(niter)=topoP(nxcs_op,nycs_op)+Eta(nxcs_op,nycs_op);

% physics grid variables
        pephyfile=sprintf('%s%s.%s',chdatapath,pephystr,time);
        thphyfile=sprintf('%s%s.%s',chdatapath,thphystr,time);

        pephy=rdmds(pephyfile);
        thphy=rdmds(thphyfile);

% calculate the total air mass and surface co2 ice mass in kilograms--
        airmass=(topoP+Eta).*rac/grv;
        icemass=ci.*rac;
        totair(niter)=sum(sum(airmass));
        totice(niter)=sum(sum(icemass));
    
        if (h2o=='y')
        h2oicesfcmass=hi.*rac;
        h2oiceairmass=qic.*rac;
        h2ovapairmass=qvc.*rac;
        tot_h2oice_sfc(niter)=sum(sum(h2oicesfcmass)); 
        tot_h2oice_air(niter)=sum(sum(h2oiceairmass));
        tot_h2ovap_air(niter)=sum(sum(h2ovapairmass));
        end;
 
% calculate the total ice in north and south poles
        toticeN(niter)=0.0;
        toticeS(niter)=0.0;
        totairN(niter)=0.0;
        totairS(niter)=0.0;
        for i=1:nx
         for j=1:ny
% north polar ice and air
             if(yg(i,j)>0.0)
                toticeN(niter)=toticeN(niter)+icemass(i,j);
                totairN(niter)=totairN(niter)+airmass(i,j);
% south polar ice and air
             elseif (yg(i,j)<-0.0)
                toticeS(niter)=toticeS(niter)+icemass(i,j);
                totairS(niter)=totairS(niter)+airmass(i,j);
             end;
         end;
        end;

% construct dynamics grid
        dpdyn(:,:,1:nr)=pedyn(:,:,1:nr)-pedyn(:,:,2:nr+1);
        pcdyn(:,:,1:nr)=(pedyn(:,:,1:nr)+pedyn(:,:,2:nr+1))/2.0;

        for i=1:nx
         for j=1:ny
          for k=1:nr
              if (dpdyn(i,j,k)<0)
                  dpdyn(i,j,k)=0;
              end;
          end;
         end;
        end;

% calculate the global mean Ar/N2 mass density (kg/m^2)
        Armassmean(niter)=sum(sum(sum(ptr.*dpdyn/grv,3).*rac))/sum(sum(rac));
% calculate the grid-point and polar-cap Ar/N2 mass density (kg/m^2)
        Arlocal=ptr.*dpdyn/grv;

        ArpolarN=zeros(nx,ny);
        ArpolarS=zeros(nx,ny);

        polarareaN=zeros(nx,ny);
        polarareaS=zeros(nx,ny);

        tskN=0.0;
        areaN=0.0;

% calculate the grid-point Ar and N2 mass in the polar caps (in kg).
        for i=1:nx
         for j=1:ny
          for k=1:nr

          if (yc(i,j)>deg_pole) % center of the grid includes 'deg_pole'
             ArpolarN(i,j)=ArpolarN(i,j)+Arlocal(i,j,k).*rac(i,j);
             polarareaN(i,j)=rac(i,j);
          end;
          if (yc(i,j)<-deg_pole)
             ArpolarS(i,j)=ArpolarS(i,j)+Arlocal(i,j,k).*rac(i,j);
             polarareaS(i,j)=rac(i,j);
          end;

          end;
          if (yc(i,j)>80.0)
             tskN=tskN+tsk(i,j).*rac(i,j);
             areaN=areaN+rac(i,j);
          end;
         end;
        end;

% calculate the grid-point and area-weighted Ar and N2 mass mixing ratio in polar caps
% note that ArpolarN, ArpolarS, N2polarN and N2polarS have dummy "zeros"
% values outside of the polar caps. They have unit of kg m^2 at each grid point.
% The global operation is only for convenience!!!
%        ArpolarN=ArpolarN*grv./(topoP+Eta);
%        ArpolarS=ArpolarS*grv./(topoP+Eta);
% area weighted Ar and N2 mass mixing ratio, sum(d(mass_mixing_ratio)*d(rac))/sum(rac_polar)
        ArN(niter)=sum(sum(ArpolarN))/sum(sum(polarareaN.*(topoP+Eta)/grv));
        ArS(niter)=sum(sum(ArpolarS))/sum(sum(polarareaS.*(topoP+Eta)/grv));    
        ArNmass(niter)=sum(sum(ArpolarN));
        ArSmass(niter)=sum(sum(ArpolarS));
% (globally) column integrated grid-point mass mixing ratio and mass density for Ar and N2    
        Arq=sum(Arlocal,3)*grv./(topoP+Eta); 
        Arm=sum(Arlocal,3);

% calculate the zonal mean Ar and N2 mass mixing ratio and mass density as a function of latitude
        for k=1:nr
            hFacC(1:nx,1:ny,k)= 1; %(Ro+Eta)./Ro.*hfacc(:,:,k);
        end;
        [Arq2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(Arq,ny_latlon,yc,rac,hFac);
        [Arm2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(Arm,ny_latlon,yc,rac,hFac);
        
% calculate the zonally integrated total water vapor and ice (cloud) mass
        if (h2o=='y')
          [qvc2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(qvc,ny_latlon,yc,rac,hFac);
          [qic2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(qic,ny_latlon,yc,rac,hFac);
          qvc2d=qvc2d.*das';
          qic2d=qic2d.*das';
        end;

% zonal mean Ar (mass mixing ratio) and N2 (specific humidity) for quick diagnostics 
        [Ar2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(ptr.*mask,ny_latlon,yc,rac,hFacC);

% calculate zonal mean temperature, winds and streamfunction-----------------------------

        pcphy=(pephy(:,:,2:size(pephy,3))+pephy(:,:,1:size(pephy,3)-1))/2.0;

% interpolate the temperature and Ar to reference pressure profile

        for k=1:nr
            Tdyn(:,:,k)=theta(:,:,k).*(pcdyn(:,:,k)/atm_Po).^atm_kappa;
        end;

        for k=1:size(thphy,3)-1
            Tphy(:,:,k)=thphy(:,:,k).*(pcphy(:,:,k)/atm_Po).^atm_kappa;
        end;
        Tphy(:,:,size(thphy,3))=Tphy(:,:,size(thphy,3)-1);

% remove zeros in pcdyn
        for i=1:nx; for j=1:ny; 
            pcdyn(i,j,1:ksurf(i,j)-1)=NaN;
        end; end;
        %[ca2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(ca,ny_latlon,yc,rac,hFacF_fake_phy); 
   
% move to permanent storage
        ArVL2(niter)=Arq(nxcs_vl2,nycs_vl2); 
        ArOP(niter)=Arq(nxcs_op,nycs_op);
        Ar2d(:,niter)=Arq2d;  % Ar vertically integrated mixing ratio
        Ar2m(:,niter)=Arm2d;  % Ar zonal mean vertically integrated total mass

        if (h2o=='y')
          qvcm(:,niter)=qvc2d;
          qicm(:,niter)=qic2d;

         if ( (dyn_phys ~= 'y') )

          qvatmsl(niter) = qvdyn(nxcs_msl  ,nycs_msl,ksurf(nxcs_msl  ,nycs_msl));
          qiatmsl(niter) = qidyn(nxcs_msl  ,nycs_msl,ksurf(nxcs_msl  ,nycs_msl));
          dpmsl(niter)   = pephy(nxcs_msl  ,nycs_msl,ksurf(nxcs_msl  ,nycs_msl)) - ...
                           pedyn(nxcs_msl  ,nycs_msl,ksurf(nxcs_msl  ,nycs_msl)+1);
          pcmsl(niter) = pcdyn(nxcs_msl  , nycs_msl, ksurf(nxcs_msl  , nycs_msl));

          rsat0=mars_h2o_rsat(  Tdyn (nxcs_msl  , nycs_msl, ksurf(nxcs_msl  , nycs_msl)), ...
                                pcdyn(nxcs_msl  , nycs_msl, ksurf(nxcs_msl  , nycs_msl)) ); 

          qsatmsl(niter)=rsat0;
          Tatmsl(niter)= Tdyn(nxcs_msl  , nycs_msl, ksurf(nxcs_msl  , nycs_msl));

         else

          qvatmsl(niter)= qvphy(nxcs_msl,nycs_msl,1) ;

          dpmsl(niter)  = pephy(nxcs_msl,nycs_msl,1)-pephy(nxcs_msl,nycs_msl,2);
          pcmsl(niter)  =( pephy(nxcs_msl,nycs_msl,1)+pephy(nxcs_msl,nycs_msl,2) )/2;

          rsat=mars_h2o_rsat( Tphy(nxcs_msl, nycs_msl, 1), pcphy(nxcs_msl, nycs_msl,1) );
          qsatmsl(niter)=rsat;
          Tatmsl(niter)= Tphy(nxcs_msl, nycs_msl, 1);

         end;

          Tsatmsl(niter)= tsk(nxcs_msl, nycs_msl);
          psmsl(niter) = topoP(nxcs_msl  ,nycs_msl)+Eta(nxcs_msl  ,nycs_msl);

        end;      

        tskNm(:,niter)=tskN/areaN;

        Ar2dt(:,:,niter)=Ar2dtmp; % Ar mixing ratio, (P,y,ls)
        co2icemass(:,:,niter)=icemass(:,:);
        %ca2dt(:,:,niter)=ca2dtmp;

        
% calculate ls  (solar longitude) -------
        julian=day(niter)/p2si;
        date_dbl = julian - zero_date;
        while (date_dbl < 0.)
           date_dbl=date_dbl+planet_year;
        end;
        while (date_dbl > planet_year)
           date_dbl=date_dbl-planet_year;
        end;


%  determine true anomaly at current date:  w
%  Iteration for w
       em = 2.0 * pi * date_dbl/planet_year;
       e  = 1.0;
       cd0 = 1.0;
       while (cd0 > SMALL_VALUE)
       ep = e-(e-eccentricity*sin(e)-em) / (1.0-eccentricity*cos(e));
       cd0 = abs(e-ep);
       e = ep;
       end;
       w = 2.0 * atan( er * tan(0.5*e) );
 
%  Radius vector ( astronomical units:  AU )
       als= (w - eq)*180.0/pi;      %Aerocentric Longitude
       if (als < 0.0) 
          als=als+360.0;
       end;
% if als is reset to some value close to zero, then we want to 
% start a new year
       if (niter>1); if (ls(niter-1)-360.*(current_year-1)-als>180.);
          current_year=current_year+1;
       end; end;
       ls(niter) = als+360.*(current_year-1);
       yr(niter) = ls(niter)/360.;
       

end;

 
fprintf('Surface co2 ice vs Ls, co2icemass [ %d x %d x %d ]:  \n',nx,ny,niter);
fprintf('Zonal mean Ar mixing ration vs Ls, Ar2dt [ %d x %d x %d ]:  \n',ny_latlon,nr,niter);
fprintf('Vertically integrated Ar mixing ration vs Ls, Ar2d [ %d x %d ]:  \n',ny_latlon,niter);
fprintf('Vertically integrated Ar mass density vs Ls, Ar2m [ %d x %d ]:  \n',ny_latlon,niter);
fprintf('Ar mixing ratio at south pole vs Ls, ArS [ %d ]:  \n',niter);
fprintf('Ar mixing ratio at north pole vs Ls, ArN [ %d ]:  \n',niter);
fprintf('Vertically and zonally integrated water vapor mass, qvcm (kg) [ %d x %d ]:  \n',ny_latlon,niter);
fprintf('Vertically and zonally integrated water cloud mass, qicm (kg) [ %d x %d ]:  \n',ny_latlon,niter);

