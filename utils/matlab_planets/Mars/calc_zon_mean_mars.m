% first ntry=12 layers are in physics grid, max(ksurf) and above are in dynamics grid
% The hybrid grid is for zonal mean of temperature, winds and stream function only!!!
 

clear all;

% section 1: load grid files and data configurations
chdatapath=input('full path to load data /~/ : ','s');
deltaT=input('time step: ');
multi_dataset=input('multiple data sets (y/n)? ','s');
diffDT=input('Using different time step (y/n)? ','s');
if (diffDT=='y') 
   prejulian=input('last planet day before change time step: ');
   niter0=input('time step at the pickup point: ');
else
   prejulian=0.0;
   niter0=0.0;
end;
tstart=input('starting time step: ');
if (multi_dataset == 'y')
   tend=input('ending time steps: ');
   dnout=input('Output interval in number of time steps: ');
else
   tend=tstart;
   dnout=1;
end;

multi_steps=input('time average over multiple dataset (y/n): ', 's');
plotstream=input('plot meridional stream functions (y/n): ', 's');
h2o=input('With or without water cycle (y/n): ', 's');

rac=rdmds([chdatapath,'RAC']);
dxg=rdmds([chdatapath,'DXG']);
dyg=rdmds([chdatapath,'DYG']);
xg=rdmds([chdatapath,'XG']);
yg=rdmds([chdatapath,'YG']);
xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);
hfacc=rdmds([chdatapath,'hFacC']);
drF=rdmds([chdatapath,'DRF']); drF=squeeze(drF);
topoP=rdmds([chdatapath,'topo_P']);
topoZ=rdmds([chdatapath,'topo_H']);

g2d(:,:,1)=xc; g2d(:,:,2)=yc; g2d(:,:,3)=xg; g2d(:,:,4)=yg;
g2d(:,:,5)=dxg; 
g2d(:,:,6)=dyg; 
g2d(:,:,7)=rac;

dims=size(rac); nx=dims(1); ny=dims(2);
ny_latlon=2*ny;

% section 2: preprocessing for grid structure and grid points required for 
% viking lander sites as well as the Mars physics parameters

if (plotstream == 'y')
   [namf_cs]=make_psi_lines(g2d, ny_latlon);
else
   'no options'
end;

[AngleCS,AngleSN] = cubeCalcAngle(yg,rac,dxg,dyg);

for k=1:(tend-tstart)/dnout+1
    nstep(k)=tstart+dnout*(k-1);
end;
alldata=nstep;

% Mars physics parameters, including gravity, atmospheric properties and topography
grv=3.727;
atm_Po=610.0;
atm_cp=767.35;
atm_kappa=0.25;
zlow=-6.5470e3; % 2.5 degree res only
ztop=80.0e3; % set maximum altitude to 80km
rsphere=3389.9E3;

% define some constants for calculating the solar angle l_s
p2si=1.0274912;
equinox_fraction= 0.2695;
planet_year=669.0;
zero_date=488.7045;
eccentricity=0.09341233;
semimajoraxis= 1.52366231;


drsphere=rsphere*(1.0/ny_latlon)*pi;

phys_para(1)=atm_Po;
phys_para(2)=atm_cp;
phys_para(3)=atm_kappa;
phys_para(4)=grv;

% calculate the solar longitude.
SMALL_VALUE=1.0e-6;
deleqn = equinox_fraction * planet_year;
qq = 2.0 * (pi * deleqn / planet_year);
er = sqrt( (1.0+eccentricity)/(1.0-eccentricity) );
%  determine true anomaly at equinox:  eq
%  Iteration for eq
   e = 1.0;
   cd0 = 1.0;
   while (cd0 > SMALL_VALUE)
      ep = e - (e-eccentricity*sin(e)-qq) / (1.0-eccentricity*cos(e));
      cd0 = abs(e-ep);
      e = ep;
   end;
   eq = 2.0 * atan( er * tan(0.5*e) );

% load all data need to be processed, including winds, ice, tracers and temperature

% dynamics grid
ustr='U';
vstr='V';
thetastr='T';       % potential temperature
pedstr='pedyn';     % pressure field (co2+N2)
cistr='co2ice';     % surface ice

Etastr='Eta';       % surface pressure anomaly

% water ice 
ptrstr='PTRACER01'; % Ar

if (h2o=='y')
   qvcstr='qv_column';
   qicstr='qi_column';
   qvdynstr='PTRACER02';
   qidynstr='PTRACER04';
   histr='h2oice';
end;

% before start calculation, find ksurf
% find array size -------------------------------

rC=rdmds([chdatapath,'RC']);
rC=squeeze(rC); 

nr=length(rC); 
ksurf(1:nx,1:ny)=1;
mask(1:nx,1:ny,1:nr)=1;

% dynamics grid part

for i=1:nx
  for j=1:ny
    kindex(1:nr)=nr+1;
    for k=1:nr-1
       if (hfacc(i,j,k)>0)                                    
          kindex(k)=k;
       end;
    end;
    ksurf(i,j)=min(kindex);
  end;
end;

for i=1:nx
  for j=1:ny
    for k=1:nr
       if (k<ksurf(i,j))
          mask(i,j,k)=NaN;
       else
          mask(i,j,k)=1;
       end;
    end;
  end;
end;

kmax=max(max(ksurf));

%------------------------------------------------

% section 3: main loop to process data, including zonal mean 
%            and time evolution

for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter),'%d');
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        time=allzero
        timed=alldata(niter);
        day(niter)=(timed-niter0)*deltaT/86400+prejulian*p2si; % this is earth day, nomalized to sol later
% dynamics grid variables
        ufile=sprintf('%s%s.%s',chdatapath,ustr,time);
        vfile=sprintf('%s%s.%s',chdatapath,vstr,time);
        pedynfile=sprintf('%s%s.%s',chdatapath,pedstr,time);
        Etafile=sprintf('%s%s.%s',chdatapath,Etastr,time);
        thetafile=sprintf('%s%s.%s',chdatapath,thetastr,time);
        cifile=sprintf('%s%s.%s',chdatapath,cistr,time);

        pedyn=rdmds(pedynfile);
        u3d=rdmds(ufile);
        v3d=rdmds(vfile);
        Eta=rdmds(Etafile);
        theta=rdmds(thetafile);
        ci=rdmds(cifile);

        ptrfile=sprintf('%s%s.%s',chdatapath,ptrstr,time);
        ptr=rdmds(ptrfile);

        if(h2o=='y')
 
            hifile=sprintf('%s%s.%s',chdatapath,histr,time);
            hi=rdmds(hifile);

            qvcfile=sprintf('%s%s.%s',chdatapath,qvcstr,time);
            qicfile=sprintf('%s%s.%s',chdatapath,qicstr,time);
            qvdynfile=sprintf('%s%s.%s',chdatapath,qvdynstr,time);
            qidynfile=sprintf('%s%s.%s',chdatapath,qidynstr,time);
            qvc=rdmds(qvcfile);
            qic=rdmds(qicfile);
            qvdyn=rdmds(qvdynfile); qidyn=rdmds(qidynfile);

% convert pr um to kg/m^2: 1 pr um = 10^-4 g/cm^2 = 10^-3 kg/m^2
            qvc=1.e-3*qvc;
            qic=1.e-3*qic;
            h2oicesfcmass=hi.*rac;
            h2oiceairmass=qic.*rac;
            h2ovapairmass=qvc.*rac;
            tot_h2oice_sfc(niter)=sum(sum(h2oicesfcmass));
            tot_h2oice_air(niter)=sum(sum(h2oiceairmass));
            tot_h2ovap_air(niter)=sum(sum(h2ovapairmass));
        end;

% construct dynamics grid
        dpdyn(:,:,1:nr)=pedyn(:,:,1:nr)-pedyn(:,:,2:nr+1);
        pcdyn(:,:,1:nr)=(pedyn(:,:,1:nr)+pedyn(:,:,2:nr+1))/2.0;

        for i=1:nx, for j=1:ny, for k=1:nr
            if (dpdyn(i,j,k)<0), dpdyn(i,j,k)=0; end;
        end; end; end;
     
% calculate the global mean Ar/N2 mass density (kg/m^2)
        Armassmean(niter)=sum(sum(sum(ptr.*dpdyn/grv,3).*rac))/sum(sum(rac));

        airmass=(topoP+Eta).*rac/grv;
        icemass=ci.*rac;
        totair(niter)=sum(sum(airmass));
        totice(niter)=sum(sum(icemass));

% calculate zonal mean temperature, winds and streamfunction-----------------------------

% prepare hybrid grid for further diagnostics

        for k=1:nr
            hFacC(1:nx,1:ny,k)= 1; %(Ro+Eta)./Ro.*hfacc(:,:,k);
        end;

        hFacF_fake_dyn(1:nx,1:ny,1:nr+1)=1.0;

% rotate U and V and calculate the zonal mean zonal wind
        [uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C');

        for k=1:nr
            Tdyn(:,:,k)=theta(:,:,k).*(pcdyn(:,:,k)/atm_Po).^atm_kappa;
        end;

        [Tdyn2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(Tdyn.*mask,ny_latlon,yc,rac,hFacC); 
        [U2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(uE.*mask,ny_latlon,yc,rac,hFacC);
        [V2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(vN.*mask,ny_latlon,yc,rac,hFacC);
        [pedyn2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(pedyn,ny_latlon,yc,rac,hFacF_fake_dyn);
        [dp2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(dpdyn,ny_latlon,yc,rac,hFacC);
        [pc2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(pcdyn,ny_latlon,yc,rac,hFacC);
        if (h2o=='y')
        [qv2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(qvdyn.*mask,ny_latlon,yc,rac,hFacC);
        [qi2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(qidyn.*mask,ny_latlon,yc,rac,hFacC);
        end;
        
        [zc,dz]=cal_height(theta, pedyn, topoZ, phys_para);
        hFacz(1:nx,1:ny,1:nr)=1.0;
        [z2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(zc,ny_latlon,yc,rac,hFacz);

%        pc2d=(pedyn2dtmp(:,1:nr)+pedyn2dtmp(:,2:nr+1))/2.0;
%        dp2d=(pedyn2dtmp(:,1:nr)-pedyn2dtmp(:,2:nr+1));

        if (plotstream == 'y')
           [psi]=use_bk_line_mars(uE.*mask,vN.*mask,dxg,dyg,rac,dp2d,namf_cs,grv);
        else
           'no options';
        end;
   
% move to permanent storage
        T2dt(:,:,niter)=Tdyn2dtmp;   % Temperature [K], (p,y,ls)
        U2dt(:,:,niter)=U2dtmp;   % U, (p,y,ls) 
        V2dt(:,:,niter)=V2dtmp;   % V, (p,y,ls)
        p2dt(:,:,niter)=pc2d;
        pe2dt(:,:,niter)=pedyn2dtmp;
        z2dt(:,:,niter)=z2d/1e3; % height in km
    
        if (h2o=='y')
          qv2dt(:,:,niter)=qv2dtmp;
          qi2dt(:,:,niter)=qi2dtmp;
        end;
        

        if (plotstream == 'y')
           psi2dt(:,:,niter)=-psi(2:ny_latlon+1,2:nr+1); % psi< 0 for clockwise flow
        else
           'no options';
        end;
%        Etat(:,:,niter)=Eta;

% calculate ls  (solar longitude) -------
        julian=day(niter)/p2si;
        date_dbl = julian - zero_date;
        while (date_dbl < 0.)
           date_dbl=date_dbl+planet_year;
        end;
        while (date_dbl > planet_year)
           date_dbl=date_dbl-planet_year;
        end;


%  determine true anomaly at current date:  w
%  Iteration for w
       em = 2.0 * pi * date_dbl/planet_year;
       e  = 1.0;
       cd0 = 1.0;
       while (cd0 > SMALL_VALUE)
       ep = e-(e-eccentricity*sin(e)-em) / (1.0-eccentricity*cos(e));
       cd0 = abs(e-ep);
       e = ep;
       end;
       w = 2.0 * atan( er * tan(0.5*e) );
 
%  Radius vector ( astronomical units:  AU )
       als= (w - eq)*180.0/pi;      %Aerocentric Longitude
       if (als < 0.0) 
          als=als+360.0;
       end;
       ls(niter) = als;

end;

% finally expand latitude to 2d        
for k=1:nr;  y2d(1:ny_latlon,k)=ylat; end;

 
fprintf('Zonal mean Temperature vs Ls, T2dt [ %d x %d x %d ]:  \n',ny_latlon,nr,niter);
fprintf('Zonal mean zonal wind vs Ls, U2dt [ %d x %d x %d ]:  \n',ny_latlon,nr,niter);
fprintf('Zonal mean streamfunction vs Ls, psi2dt [ %d x %d x %d ]:  \n',ny_latlon,nr,niter);

if (multi_steps == 'y')
   T2dm=sum(T2dt,3)/length(alldata);
   U2dm=sum(U2dt,3)/length(alldata);
   V2dm=sum(V2dt,3)/length(alldata);
   z2dm=sum(z2dt,3)/length(alldata);
   p2dm=sum(p2dt,3)/length(alldata);
   ci2dm=sum(ci2dt,3)/length(alldata);
   if (plotstream == 'y')
       psi2dm=sum(psi2dt,3)/length(alldata);
   end;
   fprintf('Time averaged dataset requested : T2dm, U2dm, V2dm, z2dm, p2dm, psi2dm \n');
end;
