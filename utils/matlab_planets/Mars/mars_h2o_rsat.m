function [rsat]=mars_h2o_rsat(temperature,pressure);

MW_AIR = 43.49; 
MW_WAT = 18.0152;
AA0    = 6.107799961;
AA1    = 4.436518521e-01;
AA2    = 1.428945805e-02;
AA3    = 2.650648471e-04;
AA4    = 3.031240396e-06;
AA5    = 2.034080948e-08;
AA6    = 6.136820929e-11;
C1     = 9.09718;
C2     = 3.56654;
C3     = 0.876793;
EIS    = 6.1071;


if ( temperature > 273.16 ) 
   T1 = temperature - 273.16;
   esat =  AA0 + T1 * (AA1 + T1 * (AA2 + T1 * (AA3 + T1 * ...
   (AA4 + T1 * (AA5 + T1 *  AA6)))));
else
   rhs =  - C1 * (273.16 / temperature - 1.)  ...
          - C2 * log10(273.16 / temperature)  ...
          + C3 * (1. - temperature / 273.16) + log10(EIS);
   esat = 10.^rhs;
end;
% esat is in mbar -- need to convert to Pa
esat = 100.*max(esat,0.);

rsat = MW_WAT *esat / (MW_AIR * pressure);
