%without co2 cycle, the model works 
%with box_size=40 and 60 for fspecial

clear all;

cs_path=input('enter the path to cs grid files /~/ : ','s');

rsphere=3389.9e3;

filter_opt=2;

ny_orig=720; %was 360
nx_orig=ny_orig*2;

deg_size=input('resolution in latlon 5, 2.5 or 1.25 : ');

nc=180/deg_size/2;

[lonC0, latC0, lonG, latG,rac]=MITgrid(nc,cs_path);
lonC=lonC0(1:nc,1:nc,:);
latC=latC0(1:nc,1:nc,:);
xg=reshape(permute(lonG(1:nc,1:nc,:),[1 3 2]), [nc*6 nc]);
yg=reshape(permute(latG(1:nc,1:nc,:),[1 3 2]), [nc*6 nc]);

dble_tin=1;

load '/Users/lian/matlab/Mars/Data/albedo.tbl';
load '/Users/lian/matlab/Mars/Data/inertia.tbl';

fid=fopen('/Users/lian/matlab/Mars/Data/roughness.dat');
znt=fread(fid,[720 360],'int8','b');
fclose(fid);

[cm,cn,cv]=find(znt<0);
for l=1:length(cm) znt(cm(l),cn(l))=znt(cm(l),cn(l))+256; end;
znt=znt*0.15/198; %the conversion is referred to WRF

adims=size(albedo);
idims=size(inertia);

abd=albedo(:,3);   % albedo  
tin=inertia(:,3); % thermal inertia

abd=reshape(abd,[360 180]);
tin=reshape(tin,[180 90 ]);

sst0=dlmread('/Users/lian/matlab/Mars/Data/ssts.n18'); % skin temperature
sst0=reshape(sst0',[2520 1]);
jj=1;
for ii=1:252*10
  if (sst0(ii)~=0) sst(jj)=sst0(ii); jj=jj+1; end;
end;
sst=reshape(sst,[62 36]);


clear albedo inertia sst0 sst1;

ndim_abd=size(abd); nx_abd=ndim_abd(1); ny_abd=ndim_abd(2);
ndim_tin=size(tin); nx_tin=ndim_tin(1); ny_tin=ndim_tin(2);
ndim_znt=size(znt); nx_znt=ndim_znt(1); ny_znt=ndim_znt(2);
ndim_sst=size(sst); nx_sst=ndim_sst(1); ny_sst=ndim_sst(2); %nx_sst=62


%%%%    albedo    %%%%
dx=180/ny_abd;
xabd=[-180-dx/2:dx:180+dx/2];
yabd(2:ny_abd+1)=[-90+dx/2:dx:90-dx/2];
yabd(1)=-90; yabd(ny_abd+2)=90;

for j=1:ny_abd+2
dA_abd(1:nx_abd+2,j)=rsphere^2*cos(yabd(j)/180*pi)*dx*pi/180*dx*pi/180;
end;


% periodical in x
abd_sp(2:nx_abd+1,2:ny_abd+1)=abd;
abd_sp(1,2:ny_abd+1)=abd(nx_abd,:);
abd_sp(nx_abd+2,2:ny_abd+1)=abd(2,:);

% patch polar points
abd_sp(:,1)=sum(abd_sp(:,2))/(nx_abd+2);
abd_sp(:,ny_abd+2)=sum(abd_sp(:,ny_abd+1))/(nx_abd+2);


%%%%  thermal inertia  %%%%

dx=180/ny_tin;
xtin=[-180-dx/2:dx:180+dx/2];
ytin(2:ny_tin+1)=[-90+dx/2:dx:90-dx/2];
ytin(1)=-90; ytin(ny_tin+2)=90;

for j=1:ny_tin+2
dA_tin(1:nx_tin+2,j)=rsphere^2*cos(ytin(j)/180*pi)*dx*pi/180*dx*pi/180;
end;

% periodical in x
tin_sp(2:nx_tin+1,2:ny_tin+1)=tin;
tin_sp(1,2:ny_tin+1)=tin(nx_tin,:);
tin_sp(nx_tin+2,2:ny_tin+1)=tin(2,:);

% patch polar points
tin_sp(:,1)=sum(tin_sp(:,2))/(nx_tin+2);
tin_sp(:,ny_tin+2)=sum(tin_sp(:,ny_tin+1))/(nx_tin+2);


%%%%    roughness   %%%%
dx=180/ny_znt;
xznt=[-180-dx/2:dx:180+dx/2];
yznt(2:ny_znt+1)=[-90+dx/2:dx:90-dx/2];
yznt(1)=-90; yznt(ny_znt+2)=90;

for j=1:ny_znt+2
dA_znt(1:nx_znt+2,j)=rsphere^2*cos(yznt(j)/180*pi)*dx*pi/180*dx*pi/180;
end;

% periodical in x
znt_sp(2:nx_znt+1,2:ny_znt+1)=znt;
znt_sp(1,2:ny_znt+1)=znt(nx_znt,:);
znt_sp(nx_znt+2,2:ny_znt+1)=znt(2,:);

% patch polar points
znt_sp(:,1)=sum(znt_sp(:,2))/(nx_znt+2);
znt_sp(:,ny_znt+2)=sum(znt_sp(:,ny_znt+1))/(nx_znt+2);



%%%%    skin temperature  %%%%
dx=360/(nx_sst-2);
dy=180/ny_sst;
xsst=[-180-dx/2:dx:180+dx/2];
ysst(2:ny_sst+1)=[-90+dy/2:dy:90-dy/2];
ysst(1)=-90; ysst(ny_sst+2)=90;

for j=1:ny_sst+2
dA_sst(1:nx_sst,j)=rsphere^2*cos(ysst(j)/180*pi)*dy*pi/180*dx*pi/180;
end;

sst_sp(:,2:ny_sst+1)=sst;
sst_sp(:,1) =sum(sst_sp(:,2))/(nx_sst); %don't use nx_sst+2, nx_sst=60+2 already
sst_sp(:,ny_sst+2)=sum(sst_sp(:,37))/(nx_sst);

if (filter_opt==2)
%use image filter to make smooth topography first
 h_abd=fspecial('disk',nx_abd/36); % was 20
 h_tin=fspecial('disk',nx_tin/36);
 h_znt=fspecial('disk',nx_znt/36);
 h_sst=fspecial('disk',nx_sst/36);
 
 abd_sp = imfilter(abd_sp,h_abd,'replicate');
 tin_sp = imfilter(tin_sp,h_tin,'replicate');
 znt_sp = imfilter(znt_sp,h_znt,'replicate');
 sst_sp = imfilter(sst_sp,h_sst,'replicate');
end;

x6s=lonC; %x6s=permute(reshape(xcs,[nc 6 nc]),[1 3 2]);
y6s=latC; %y6s=permute(reshape(ycs,[nc 6 nc]),[1 3 2]);

abd_cs=zeros(nc,nc,6);
tin_cs=zeros(nc,nc,6);
znt_cs=zeros(nc,nc,6);
sst_cs=zeros(nc,nc,6);

for ntiles=1:6
 abd_cs(:,:,ntiles)=interp2(yabd,xabd,abd_sp,y6s(:,:,ntiles),x6s(:,:,ntiles));
 tin_cs(:,:,ntiles)=interp2(ytin,xtin,tin_sp,y6s(:,:,ntiles),x6s(:,:,ntiles));
 znt_cs(:,:,ntiles)=interp2(yznt,xznt,znt_sp,y6s(:,:,ntiles),x6s(:,:,ntiles));
 sst_cs(:,:,ntiles)=interp2(ysst,xsst,sst_sp,y6s(:,:,ntiles),x6s(:,:,ntiles));
end;

abd_cs=permute(abd_cs,[1 3 2]);
abd_cs=reshape(abd_cs,[nc*6 nc]);

tin_cs=permute(tin_cs,[1 3 2]);
tin_cs=reshape(tin_cs,[nc*6 nc]);

znt_cs=permute(znt_cs,[1 3 2]);
znt_cs=reshape(znt_cs,[nc*6 nc]);

sst_cs=permute(sst_cs,[1 3 2]);
sst_cs=reshape(sst_cs,[nc*6 nc]);


fid=fopen('albedo.bin','w');
fwrite(fid,abd_cs,'real*8','b');
fclose(fid);

fid=fopen('inertia.bin','w');
fwrite(fid,tin_cs,'real*8','b');
fclose(fid);

fid=fopen('roughness.bin','w');
fwrite(fid,znt_cs,'real*8','b');
fclose(fid);

fid=fopen('sst.bin','w');
fwrite(fid,sst_cs,'real*8','b');
fclose(fid);




