% first generate a list of file name, sorted by modification time
%ls -rt wrfout_d01*00 | xargs -n1 basename > ~/matlab/workdir/wrf_netcdf
clear all;

datadir=input('full path to the wrf netcdf data /~/ : ','s');

wrf_files=textread('wrf_netcdf','%s'); % wrf_netcdf is a cell array, don't use load

nfiles=size(wrf_files,1);

% some constants

nindex=ncread(sprintf('%s%s','/nobackup/mrichar5/water/2m/',char(wrf_files(1))),'LU_INDEX');
ndims=size(nindex); nx=ndims(1); ny=ndims(2);

gravity=3.71;
rsphere=3.39e+06;
dx=360/nx; dy=180/ny; 
xi=-180+dx/2:dx:180-dx/2;
yi=-90+dy/2:dy:90-dy/2;

% get fractional area of a sphere
dA=zeros(nx,ny);
for j=1:ny
    % dA = r^2*cos(yi)*dy*dx (dx and dy in radians)
    dA(:,j)=rsphere^2*cos(yi(j)*pi/180)*dx*dy*(pi/180)^2;
end;

% read sigma levels (at interfaces)
znw=ncread(sprintf('%s%s','/nobackup/mrichar5/water/2m/',char(wrf_files(1))),'ZNW');
znw=double(znw); nz=size(znw,1); nz=nz-1;

% get y2d
for k=1:nz, y2d(:,k)=yi; end;


% now load data in loop %

nrecords=0;

for n=1:nfiles

    sprintf('%s%s',datadir,char(wrf_files(n)))

    PS3D=ncread(sprintf('%s%s',datadir,char(wrf_files(n))),'PSFC');
    V4D =ncread(sprintf('%s%s',datadir,char(wrf_files(n))),'V'   );
    QI4D=ncread(sprintf('%s%s',datadir,char(wrf_files(n))),'TRC_I');
    QV4D=ncread(sprintf('%s%s',datadir,char(wrf_files(n))),'TRC_V');
    QI_COLUMN=ncread(sprintf('%s%s',datadir,char(wrf_files(n))),'QI_COLUMN');
    QV_COLUMN=ncread(sprintf('%s%s',datadir,char(wrf_files(n))),'QV_COLUMN');
    L_S=ncread(sprintf('%s%s',datadir,char(wrf_files(n))),'L_S');
    nt=length(L_S);

    if (n==1), PS3D(:,:,1)=PS3D(:,:,2); end;
    % loop over time
    for niter=1:nt

      nrecords=nrecords+1;

      % === calculate mass streamfunction ===
      V3d=double(V4D(:,:,:,niter)); V2d=squeeze(sum(V3d,1)/nx);
      for k=1:nz
         vz(:,k)=2*pi*rsphere*(V2d(2:end,k)+V2d(1:end-1,k))/2.*cos(yi(:)*pi/180);
      end;

      for k=1:nz+1
          PF3D(:,:,k)=double(PS3D(:,:,niter))*znw(k);
      end;
      % zonal mean pressure
      PC3D=(PF3D(:,:,1:end-1)+PF3D(:,:,2:end))/2;; 
      % make dp positive for calculating mass
      delR3D=PF3D(:,:,1:end-1)-PF3D(:,:,2:end); 
      dp2d=squeeze(sum(delR3D,1)/nx);

      psi=zeros(ny+2,nz+1); 
      for k=nz:-1:1,
          psi(2:ny+1,k)=psi(2:ny+1,k+1) + dp2d(:,k).*vz(:,k)/gravity ;  
      end;

      % === calcuate zonal mean water ice cloud
      QI3D=double(QI4D(:,:,:,niter));
      QI2D=squeeze(sum(QI3D,1))/nx;

      QV3D=double(QV4D(:,:,:,niter));
      QV2D=squeeze(sum(QV3D,1))/nx;

      % move variables to permanent storage
      psi2dt(:,:,nrecords)=-(psi(2:ny+1,1:end-1)+psi(2:ny+1,2:end))/2;
      pc2dt (:,:,nrecords)=squeeze(sum(PC3D,1))/nx;
      qi2dt (:,:,nrecords)=QI2D;
      qv2dt (:,:,nrecords)=QV2D;
      % total water ice and vapor in the air
      tot_h2oice_air(nrecords)=double ( sum(sum( squeeze(QI_COLUMN(:,:,niter)).*dA)) );
      tot_h2ovap_air(nrecords)=double ( sum(sum( squeeze(QV_COLUMN(:,:,niter)).*dA)) ); 
      Ls(nrecords)=double(L_S(niter));
    % end time loop
    end;
% end data looop
end;

clear QI4D QV4D V4D;
