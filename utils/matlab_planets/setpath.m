cs_grid=input('perform diagnostics in cube-sphere grid:  y/n? ', 's');

path('~/matlab/make_profile',path);
path('~/matlab/workdir',path);
path('~/matlab/tools',path);
path('~/matlab/diagnostics_packs',path);
path('~/matlab/gwave',path);
path('~/matlab/Titan',path);
path('~/matlab/Mars',path);
path('~/matlab/Jupiter',path);

if cs_grid == 'y'
   path('~/matlab/cs_routines',path);
   path('~/matlab/cs_routines/bk_line',path);
end;

clear all;
