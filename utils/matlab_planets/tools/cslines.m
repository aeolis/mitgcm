function cslines(h)

    hc = get(h,'Children');
    cdata = cell2mat(get(hc,'Cdata'));
    i = find(isnan(cdata));
    tcdata = cdata(i-1);
    set(hc(tcdata<0),'LineStyle','--')
    set(hc(tcdata==0),'LineStyle',':')
    set(hc(tcdata>0),'LineStyle','-')
 

