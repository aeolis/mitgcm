clear all;

deltaT=input('Time Step (120 seconds typical), dT = ');
nfreq=3*3600/deltaT;
timed=[494640:nfreq:494640*2];
nmax=length(timed);

% define some constants for calculating the solar angle l_s
p2si=1.0274912;
equinox_fraction= 0.2695;
planet_year=669.0; % martian year
zero_date=488.7045;
eccentricity=0.09341233;
semimajoraxis= 1.52366231;
SMALL_VALUE=1.0e-6;
deleqn = equinox_fraction * planet_year;
qq = 2.0 * (pi * deleqn / planet_year);
er = sqrt( (1.0+eccentricity)/(1.0-eccentricity) );
%  determine true anomaly at equinox:  eq
%  Iteration for eq
   e = 1.0;
   cd0 = 1.0;
   while (cd0 > SMALL_VALUE)
      ep = e - (e-eccentricity*sin(e)-qq) / (1.0-eccentricity*cos(e));
      cd0 = abs(e-ep);
      e = ep;
   end;
   eqx = 2.0 * atan( er * tan(0.5*e) );

for niter=1:nmax

        day(niter)=timed(niter)*deltaT/86400;

% calculate ls -------
        julian=day(niter)/p2si;
        date_dbl = julian - zero_date;
        while (date_dbl < 0.)
           date_dbl=date_dbl+planet_year;
        end;
        while (date_dbl > planet_year)
           date_dbl=date_dbl-planet_year;
        end;


%  determine true anomaly at current date:  w
%  Iteration for w
       em = 2.0 * pi * date_dbl/planet_year;
       e  = 1.0;
       cd0 = 1.0;
       while (cd0 > SMALL_VALUE)
       ep = e-(e-eccentricity*sin(e)-em) / (1.0-eccentricity*cos(e));
       cd0 = abs(e-ep);
       e = ep;
       end;
       w = 2.0 * atan( er * tan(0.5*e) );
 
%  Radius vector ( astronomical units:  AU )
       als= (w - eqx)*180.0/pi;      %Aerocentric Longitude
       if (als < 0.0) 
          als=als+360.0;
       end;
       ls(niter) = als;

end;

n_0=find(abs(ls-0.0)<0.1);
n_90=find(abs(ls-90.0)<0.1);
n_180=find(abs(ls-180.0)<0.1);
n_270=find(abs(ls-270.0)<0.1);

% assume Ls=, ntimestep=494640 (Full martian year in 687 Earth days)
ndiff=timed(n_0(1))-494640; % differred by half of a day
n3day=3*86400/deltaT; % in Earth days

nstart(1)=timed(n_0(1))-ndiff;    nend(1)=nstart(1)+n3day;
nstart(2)=timed(n_90(1))-ndiff;   nend(2)=nstart(2)+n3day;
nstart(3)=timed(n_180(1))-ndiff;  nend(3)=nstart(3)+n3day;
nstart(4)=timed(n_270(1))-ndiff;  nend(4)=nstart(4)+n3day;

fprintf('Output interval for 3 hours: ntimesteps = %d \n', nfreq);
fprintf('Ls =   0, nstart = %d, nend = %d \n', nstart(1), nend(1));
fprintf('Ls =  90, nstart = %d, nend = %d \n', nstart(2), nend(2));
fprintf('Ls = 180, nstart = %d, nend = %d \n', nstart(3), nend(3)); 
fprintf('Ls = 270, nstart = %d, nend = %d \n', nstart(4), nend(4));
