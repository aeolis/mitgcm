clear all;

chdatapath=input('full path to load data /~/ : ','s');
ny_latlon=input('grid points in y in latlon grid: ' );
has_pedyn=input('pedyn is output? Y(es) or N(o) : ', 's');
deltaT=input('time step: ');
tstart=input('starting time step: ');
tend=input('ending time steps: ');
dnout=input('Output interval in number of time steps: ');
rac=rdmds([chdatapath,'RAC']);
dxg=rdmds([chdatapath,'DXG']);
dyg=rdmds([chdatapath,'DYG']);
xg=rdmds([chdatapath,'XG']);
yg=rdmds([chdatapath,'YG']);
xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);
hfacc=rdmds([chdatapath,'hFacC']);
drF=rdmds([chdatapath,'DRF']);
rc=rdmds([chdatapath,'RC']);
rf=rdmds([chdatapath,'RF']);

rc=squeeze(rc);
rf=squeeze(rf);
drF=squeeze(drF);

for k=1:tend/dnout+1
    nstep(k)=tstart+dnout*(k-1);
end;
alldata=nstep;


[AngleCS,AngleSN] = cubeCalcAngle(yg,rac,dxg,dyg);

%ny_latlon=72;

if (ny_latlon==36)
    nxcs_vl1=76;  nycs_vl1=18;
    nxcs_vl2=54;  nycs_vl2=10;
elseif (ny_latlon==72)
    nxcs_vl1=151; nycs_vl1=35;
    nxcs_vl2=107; nycs_vl2=19;
elseif (ny_latlon==144)
    nxcs_vl1=302; nycs_vl1=71;
    nxcs_vl2=214; nycs_vl2=37;
end;

deg_pole=75;

%Ro=rdmds([chdatapath,'Ro_surf']);
%load alldata;

if (has_pedyn == 'Y')
   co2cycle=1;
else
   co2cycle=0;
end;

ustr='uVeltave';
vstr='vVeltave';
pstr='pephy';
pedstr='pedyn'
cistr='co2ice';
Rostr='topo_P';
ptrstr='PTRACER01';
Etastr='Eta';
thetastr='Ttave';
grv=3.727;

% define some constants for calculating the solar angle l_s
p2si=1.0274912;
equinox_fraction= 0.2695;
planet_year=669.0;
zero_date=488.7045;
eccentricity=0.09341233;
semimajoraxis= 1.52366231;
SMALL_VALUE=1.0e-6;
deleqn = equinox_fraction * planet_year;
qq = 2.0 * (pi * deleqn / planet_year);
er = sqrt( (1.0+eccentricity)/(1.0-eccentricity) );
%  determine true anomaly at equinox:  eq
%  Iteration for eq
   e = 1.0;
   cd0 = 1.0;
   while (cd0 > SMALL_VALUE)
      ep = e - (e-eccentricity*sin(e)-qq) / (1.0-eccentricity*cos(e));
      cd0 = abs(e-ep);
      e = ep;
   end;
   eq = 2.0 * atan( er * tan(0.5*e) );


Rofile=sprintf('%s%s',chdatapath,Rostr);
Ro=rdmds(Rofile);

rC=rdmds([chdatapath,'RC']);
rC=squeeze(rC); 

dims=size(Ro);
nx=dims(1); ny=dims(2); nr=length(rC);

pedyn=zeros(nx,ny,nr+1);
dppe=zeros(nx,ny,nr);

ksurf(1:nx,1:ny)=1;

% before start calculation, set where hfac=0 to be NaN
  hfac_update=hfacc;
  for i=1:nx
   for j=1:ny
    for k=1:nr
        if (hfac_update(i,j,k)==0)
           hfac_update(i,j,k)=0/0;
        end;
    end;
    for k=2:nr-1
       if (hfacc(i,j,k)-hfacc(i,j,k-1)>0.5)                                    
          ksurf(i,j)=k;
       end;
    end;
   end;
  end;

for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter));
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        time=allzero
        timed=alldata(niter);
        ufile=sprintf('%s%s.%s',chdatapath,ustr,time);
        vfile=sprintf('%s%s.%s',chdatapath,vstr,time);
%        pfile=sprintf('%s%s.%s',chdatapath,pstr,time);
        if (co2cycle==1)
        cifile=sprintf('%s%s.%s',chdatapath,cistr,time);
        end;
        ptrfile=sprintf('%s%s.%s',chdatapath,ptrstr,time);
        pedynfile=sprintf('%s%s.%s',chdatapath,pedstr,time);
%        Rofile=sprintf('%s%s.%s',chdatapath,Rostr,time);
        Etafile=sprintf('%s%s.%s',chdatapath,Etastr,time);
        thetafile=sprintf('%s%s.%s',chdatapath,thetastr,time);

%        pe=rdmds(pfile);
        if (co2cycle==1)
        ci=rdmds(cifile);
        end;
        ptr=rdmds(ptrfile);

        if (has_pedyn == 'Y')
           pedyn=rdmds(pedynfile);
        else
           'no option';
        end;
%        Ro=rdmds(Rofile);
        u3d=rdmds(ufile);
        v3d=rdmds(vfile);
        Eta=rdmds(Etafile);
        theta=rdmds(thetafile);

        [uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C');
        [u2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(uE,ny_latlon,yc,rac,hfacc);
        

        airmass=(Ro+Eta).*rac/grv;
        if (co2cycle==1)
        icemass=ci.*rac;
        end;

        totair(niter)=sum(sum(airmass));
        if (co2cycle==1)
        totice(niter)=sum(sum(icemass));
        end;

        day(niter)=timed*deltaT/86400;
        patvl1(niter)=Ro(nxcs_vl1,nycs_vl1)+Eta(nxcs_vl1,nycs_vl1);
        patvl2(niter)=Ro(nxcs_vl2,nycs_vl2)+Eta(nxcs_vl2,nycs_vl2);

        if (has_pedyn == 'Y')
           dppe(:,:,1:nr)=pedyn(:,:,1:nr)-pedyn(:,:,2:nr+1);
        else
           hfac=(Ro+Eta)./Ro;
           for k=1:nr
               dppe(:,:,k)=drF(k).*hfac(:,:).*hfacc(:,:,k);
           end;
        end;

        for i=1:nx
         for j=1:ny
          for k=1:nr
              if (dppe(i,j,k)<0)
                  dppe(i,j,k)=0;
              end;
          end;
         end;
        end;
        Armassmean(niter)=sum(sum(sum(ptr.*dppe/grv,3).*rac))/sum(sum(rac));
        Armass(niter)=sum(sum(sum(ptr.*dppe/grv,3).*rac*grv./(Ro+Eta)))/sum(sum(rac));

        Arlocal=ptr.*dppe/grv;

        ArpolarN=zeros(nx,ny);
        polarareaN=zeros(nx,ny);
        ArpolarS=zeros(nx,ny);
        polarareaS=zeros(nx,ny);

        for i=1:nx
         for j=1:ny
          for k=1:nr
          if (yg(i,j)>deg_pole)
             ArpolarN(i,j)=ArpolarN(i,j)+Arlocal(i,j,k).*rac(i,j);
             polarareaN(i,j)=rac(i,j);
          end;
          if (yg(i,j)<-deg_pole)
             ArpolarS(i,j)=ArpolarS(i,j)+Arlocal(i,j,k).*rac(i,j);
             polarareaS(i,j)=rac(i,j);
          end;
          end;
         end;
        end;

        ArpolarN=ArpolarN*grv./(Ro+Eta);
        ArpolarS=ArpolarS*grv./(Ro+Eta);

        ArN(niter)=sum(sum(ArpolarN))/sum(sum(polarareaN));
        ArS(niter)=sum(sum(ArpolarS))/sum(sum(polarareaS));    
     
        Arq=sum(Arlocal,3)*grv./(Ro+Eta); 
        Arm=sum(Arlocal,3).*rac;

        for k=1:nr
            hFacC(:,:,k)=(Ro+Eta)./Ro;
        end;


% interpolate the temperature and Ar to reference pressure profile
        ptr_0=ptr.*hfac_update;
        theta_0=theta.*hfac_update; 
        for i=1:nx
         for j=1:ny
             pcdyn(i,j,ksurf(i,j):nr)=(pedyn(i,j,ksurf(i,j)+1:nr+1)+pedyn(i,j,ksurf(i,j):nr))/2.0;
             dpedyn(i,j,ksurf(i,j):nr)=(pedyn(i,j,ksurf(i,j):nr)-pedyn(i,j,ksurf(i,j)+1:nr+1));
%             ptr_pre(i,j,ksurf(i,j):nr)= squeeze(ptr_0(i,j,ksurf(i,j):nr)) ...
%                                            .*squeeze(dpedyn(i,j,ksurf(i,j):nr));  %./squeeze(drF(ksurf(i,j):nr)); 
%             theta_pre(i,j,ksurf(i,j):nr)=squeeze(theta_0(i,j,ksurf(i,j):nr)) ...
%                                            .*squeeze(dpedyn(i,j,ksurf(i,j):nr));  %./squeeze(drF(ksurf(i,j):nr)); 
%             ptr_interp(i,j,1:max(ksurf(i,j)-1,1))=0/0;
%             theta_interp(i,j,1:max(ksurf(i,j)-1,1))=0/0;
%             theta_0(i,j,1:max(ksurf(i,j)-1,1))=0/0;

             ksurfkm1(i,j)=min(find(rc<pedyn(i,j,ksurf(i,j))));
             ptr_interp(i,j,ksurfkm1(i,j):nr)=interp1(squeeze(pcdyn(i,j,ksurf(i,j):nr)), ... 
                                                      squeeze(ptr_0(i,j,ksurf(i,j):nr)),rc(ksurfkm1(i,j):nr),'spline');
             theta_interp(i,j,ksurfkm1(i,j):nr)=interp1(squeeze(pcdyn(i,j,ksurf(i,j):nr)), ... 
                                                      squeeze(theta_0(i,j,ksurf(i,j):nr)),rc(ksurfkm1(i,j):nr),'spline'); 
             ptr_interp(i,j,1:max(ksurfkm1(i,j)-1,1))=0/0;
             theta_interp(i,j,1:max(ksurfkm1(i,j)-1,1))=0/0;
                 
         end; 
        end;
        for k=1:nr
            T_interp(:,:,k)=theta_interp(:,:,k)*(rc(k)/610).^0.25;
%            T_interp(:,:,k)=theta_0(:,:,k)*(rc(k)/610).^0.25;
        end;

        [Arq2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(Arq,ny_latlon,yc,rac,hFacC);
        [Arm2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(Arm,ny_latlon,yc,rac,hFacC);
        [hFacC2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(hFacC(:,:,1),ny_latlon,yc,rac,hFacC);

% calculate the zonal mean of Ar and temperature as a function of lat-pressure and time
        [Ar2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(ptr_interp,ny_latlon,yc,rac,hFacC);
        [T2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(T_interp,ny_latlon,yc,rac,hFacC); 

% rotate U and V and calculate the zonal mean zonal wind
        [uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C');
        for i=1:nx
         for j=1:ny
             uE(i,j,1:max(ksurf(i,j)-1,1))=0/0;
             vN(i,j,1:max(ksurf(i,j)-1,1))=0/0;
         end; 
        end;
        [U2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(uE,ny_latlon,yc,rac,hfacc);
        [V2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(vN,ny_latlon,yc,rac,hfacc);

        

% move to permanent storage        
        Ar2d(:,niter)=Arq2d;  % Ar vertically integrated mixing ratio
        Ar2m(:,niter)=Arm2d;  % Ar zonal mean vertically integrated total mass
        hfac2d(:,niter)=hFacC2d; 
      
        Ar2dt(:,:,niter)=Ar2dtmp; % Ar mixing ratio, (P,y,ls)
        T2dt(:,:,niter)=T2dtmp;   % Temperature [K], (p,y,ls)
        U2dt(:,:,niter)=U2dtmp;   % U, (p,y,ls) 
        V2dt(:,:,niter)=V2dtmp;   % V, (p,y,ls)
        co2icemass(:,:,niter)=icemass(:,:);

% calculate ls -------
        julian=day(niter)/p2si;
        date_dbl = julian - zero_date;
        while (date_dbl < 0.)
           date_dbl=date_dbl+planet_year;
        end;
        while (date_dbl > planet_year)
           date_dbl=date_dbl-planet_year;
        end;


%  determine true anomaly at current date:  w
%  Iteration for w
       em = 2.0 * pi * date_dbl/planet_year;
       e  = 1.0;
       cd0 = 1.0;
       while (cd0 > SMALL_VALUE)
       ep = e-(e-eccentricity*sin(e)-em) / (1.0-eccentricity*cos(e));
       cd0 = abs(e-ep);
       e = ep;
       end;
       w = 2.0 * atan( er * tan(0.5*e) );
 
%  Radius vector ( astronomical units:  AU )
       als= (w - eq)*180.0/pi;      %Aerocentric Longitude
       if (als < 0.0) 
          als=als+360.0;
       end;
       ls(niter) = als;

end;
        
fprintf('Surface co2 ice vs Ls, co2icemass [ %d x %d x %d ]:  \n',nx,ny,niter);
fprintf('Zonal mean Ar mixing ration vs Ls, Ar2dt [ %d x %d x %d ]:  \n',ny_latlon,nr,niter);
fprintf('Zonal mean Temperature vs Ls, T2dt [ %d x %d x %d ]:  \n',ny_latlon,nr,niter);
fprintf('Zonal mean zonal wind vs Ls, U2dt [ %d x %d x %d ]:  \n',ny_latlon,nr,niter);
fprintf('Vertically integrated Ar mixing ration vs Ls, Ar2d [ %d x %d ]:  \n',ny_latlon,niter);
fprintf('Vertically integrated Ar mass vs Ls, Ar2m [ %d x %d ]:  \n',ny_latlon,niter);
fprintf('Ar mixing ratio at south pole vs Ls, ArS [ %d ]:  \n',niter);
fprintf('Ar mixing ratio at north pole vs Ls, ArN [ %d ]:  \n',niter);


