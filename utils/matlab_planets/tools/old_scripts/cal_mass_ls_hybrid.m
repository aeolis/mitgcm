% first ntry=12 layers are in physics grid, max(ksurf) and above are in dynamics grid
% The hybrid grid is for zonal mean of temperature, winds and stream function only!!!
 

clear all;

chdatapath=input('full path to load data /~/ : ','s');
ny_latlon=input('grid points in y in latlon grid: ' );
has_pedyn=input('pedyn is output? y(es) or n(o) : ', 's');
deltaT=input('time step: ');
tstart=input('starting time step: ');
tend=input('ending time steps: ');
dnout=input('Output interval in number of time steps: ');
multi_steps=input('time average over multiple dataset (y/n): ', 's');
rac=rdmds([chdatapath,'RAC']);
dxg=rdmds([chdatapath,'DXG']);
dyg=rdmds([chdatapath,'DYG']);
xg=rdmds([chdatapath,'XG']);
yg=rdmds([chdatapath,'YG']);
xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);
hfacc=rdmds([chdatapath,'hFacC']);
drF=rdmds([chdatapath,'DRF']); drF=squeeze(drF);
topoP=rdmds([chdatapath,'topo_P']);
topoZ=rdmds([chdatapath,'topo_H']);

g2d(:,:,1)=xc; g2d(:,:,2)=yc; g2d(:,:,3)=xg; g2d(:,:,4)=yg;
g2d(:,:,5)=dxg; 
g2d(:,:,6)=dyg; 
g2d(:,:,7)=rac;

[namf_cs]=make_psi_lines(g2d, ny_latlon); close;

[AngleCS,AngleSN] = cubeCalcAngle(yg,rac,dxg,dyg);

for k=1:(tend-tstart)/dnout+1
    nstep(k)=tstart+dnout*(k-1);
end;
alldata=nstep;

for i=1:size(xc,1)
 for j=1:size(xc,2)
     if (abs(xc(i,j)-(-48.222))<180/ny_latlon/2 && abs(yc(i,j)-22.697)<180/ny_latlon/2 )
        fprintf('\n VL1 m=%d n=%d \n',i,j)
        nxcs_vl1=i; nycs_vl1=j;
     end;
%     if (abs(xc(i,j)-134.2630)<180/ny_latlon/2 && abs(yc(i,j)-47.967)<180/ny_latlon/2 )
     if (abs(xc(i,j)-134.2630)<180/ny_latlon && abs(yc(i,j)-47.967)<180/ny_latlon )
        fprintf('\n VL2 m=%d n=%d \n',i,j)
        nxcs_vl2=i; nycs_vl2=j;
     end;
 end;
end;

%if (ny_latlon==36)
%    nxcs_vl1=76;  nycs_vl1=18;
%    nxcs_vl2=54;  nycs_vl2=10;
%elseif (ny_latlon==72)
%    nxcs_vl1=151; nycs_vl1=35;
%   nxcs_vl2=107; nycs_vl2=19;
%elseif (ny_latlon==144)
%    nxcs_vl1=302; nycs_vl1=71;
%    nxcs_vl2=214; nycs_vl2=37;
%end;

deg_pole=75;

if (has_pedyn == 'y')
   co2cycle=1;
else
   co2cycle=0;
end;

grv=3.727;
atm_Po=610.0;
atm_cp=767.35;
atm_kappa=0.25;
zlow=-6.5470e3; % 2.5 degree res only
ztop=80.0e3; % set maximum altitude to 80km
rsphere=3389.9E3;
drsphere=rsphere*(1.0/ny_latlon)*pi;

phys_para(1)=atm_Po;
phys_para(2)=atm_cp;
phys_para(3)=atm_kappa;
phys_para(4)=grv;

topoZ=topoZ+zlow;

hFac(1:size(topoZ,1),1:size(topoZ,2))=1;
z1d=calcZonalAvgCubeC(topoZ,ny_latlon,yc,rac,squeeze(hFac(:,:)));
p1d=calcZonalAvgCubeC(topoP,ny_latlon,yc,rac,squeeze(hFac(:,:)));

z1d=z1d/1e3; % in km

ustr='U';
vstr='V';
pedstr='pedyn';
cistr='co2ice';
ptrstr='PTRACER01';
Etastr='Eta';
thetastr='T';

uphystr='uphy';
vphystr='vphy';
thphystr='thphy';
pephystr='pephy';

% find levels for new grid ----------------------

% physics grid part
nphys=12;

% before start calculation, find ksurf
% find array size -------------------------------

rC=rdmds([chdatapath,'RC']);
rC=squeeze(rC); 

dims=size(topoP);
nx=dims(1); ny=dims(2); nr=length(rC);  clear rC;

% dynamics grid part
ksurf(1:nx,1:ny)=1;

for i=1:nx
  for j=1:ny
    kindex(1:nr)=nr+1;
    for k=1:nr-1
       if (hfacc(i,j,k)>0)                                    
          kindex(k)=k;
       end;
    end;
    ksurf(i,j)=min(kindex);
  end;
end;

kmax=max(max(ksurf));

nhybrid=nphys+nr-kmax+1;

%------------------------------------------------

% define some constants for calculating the solar angle l_s
p2si=1.0274912;
equinox_fraction= 0.2695;
planet_year=669.0;
zero_date=488.7045;
eccentricity=0.09341233;
semimajoraxis= 1.52366231;
SMALL_VALUE=1.0e-6;
deleqn = equinox_fraction * planet_year;
qq = 2.0 * (pi * deleqn / planet_year);
er = sqrt( (1.0+eccentricity)/(1.0-eccentricity) );
%  determine true anomaly at equinox:  eq
%  Iteration for eq
   e = 1.0;
   cd0 = 1.0;
   while (cd0 > SMALL_VALUE)
      ep = e - (e-eccentricity*sin(e)-qq) / (1.0-eccentricity*cos(e));
      cd0 = abs(e-ep);
      e = ep;
   end;
   eq = 2.0 * atan( er * tan(0.5*e) );


for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter));
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        time=allzero
        timed=alldata(niter);
% dynamics grid variables
        ufile=sprintf('%s%s.%s',chdatapath,ustr,time);
        vfile=sprintf('%s%s.%s',chdatapath,vstr,time);
        if (co2cycle==1)
           cifile=sprintf('%s%s.%s',chdatapath,cistr,time);
        end;
        ptrfile=sprintf('%s%s.%s',chdatapath,ptrstr,time);
        pedynfile=sprintf('%s%s.%s',chdatapath,pedstr,time);
        Etafile=sprintf('%s%s.%s',chdatapath,Etastr,time);
        thetafile=sprintf('%s%s.%s',chdatapath,thetastr,time);

        if (co2cycle==1)
           ci=rdmds(cifile);
        end;
        ptr=rdmds(ptrfile);

        if (has_pedyn == 'y')
           pedyn=rdmds(pedynfile);
        else
           'no option';
        end;
        u3d=rdmds(ufile);
        v3d=rdmds(vfile);
        Eta=rdmds(Etafile);
        theta=rdmds(thetafile);
% physics grid variables
        uphyfile=sprintf('%s%s.%s',chdatapath,uphystr,time);
        vphyfile=sprintf('%s%s.%s',chdatapath,vphystr,time); 
        pephyfile=sprintf('%s%s.%s',chdatapath,pephystr,time);
        thphyfile=sprintf('%s%s.%s',chdatapath,thphystr,time);

        uphy=rdmds(uphyfile);
        vphy=rdmds(vphyfile);
        pephy=rdmds(pephyfile);
        thphy=rdmds(thphyfile);

% calculate the total air mass and co2 ice mass--
        airmass=(topoP+Eta).*rac/grv;
        if (co2cycle==1)
        icemass=ci.*rac;
        end;

        totair(niter)=sum(sum(airmass));
        if (co2cycle==1)
        totice(niter)=sum(sum(icemass));
        end;

        day(niter)=timed*deltaT/86400;

        if (has_pedyn == 'y')
           dpdyn(:,:,1:nr)=pedyn(:,:,1:nr)-pedyn(:,:,2:nr+1);
           pcdyn(:,:,1:nr)=(pedyn(:,:,1:nr)+pedyn(:,:,2:nr+1))/2.0;
        else
           hfac=(topoP+Eta)./topoP;
           for k=1:nr
               dpdyn(:,:,k)=drF(k).*hfac(:,:).*hfacc(:,:,k);
           end;
        end;

        for i=1:nx
         for j=1:ny
          for k=1:nr
              if (dpdyn(i,j,k)<0)
                  dpdyn(i,j,k)=0;
              end;
          end;
         end;
        end;

        if (has_pedyn ~= 'y')
           pedyn(1:nx,1:ny,1)=sum(dpdyn,3);
           for k=1:nr
               pedyn(1:nx,1:ny,k+1)=pedyn(1:nx,1:ny,k)-dpdyn(1:nx,1:ny,k);
           end;
           pcdyn(:,:,1:nr)=(pedyn(:,:,1:nr)+pedyn(:,:,2:nr+1))/2.0;
        end;
            
        
        Armassmean(niter)=sum(sum(sum(ptr.*dpdyn/grv,3).*rac))/sum(sum(rac));
        Armass(niter)=sum(sum(sum(ptr.*dpdyn/grv,3).*rac*grv./(topoP+Eta)))/sum(sum(rac));

        Arlocal=ptr.*dpdyn/grv;

        ArpolarN=zeros(nx,ny);
        polarareaN=zeros(nx,ny);
        ArpolarS=zeros(nx,ny);
        polarareaS=zeros(nx,ny);

        for i=1:nx
         for j=1:ny
          for k=1:nr
          if (yg(i,j)>deg_pole)
             ArpolarN(i,j)=ArpolarN(i,j)+Arlocal(i,j,k).*rac(i,j);
             polarareaN(i,j)=rac(i,j);
          end;
          if (yg(i,j)<-deg_pole)
             ArpolarS(i,j)=ArpolarS(i,j)+Arlocal(i,j,k).*rac(i,j);
             polarareaS(i,j)=rac(i,j);
          end;
          end;
         end;
        end;

        ArpolarN=ArpolarN*grv./(topoP+Eta);
        ArpolarS=ArpolarS*grv./(topoP+Eta);

        ArN(niter)=sum(sum(ArpolarN))/sum(sum(polarareaN));
        ArS(niter)=sum(sum(ArpolarS))/sum(sum(polarareaS));    
     
        Arq=sum(Arlocal,3)*grv./(topoP+Eta); 
        Arm=sum(Arlocal,3); %.*rac;

% calculate the VL1 and VL2 pressure curve
        patvl1(niter)=topoP(nxcs_vl1,nycs_vl1)+Eta(nxcs_vl1,nycs_vl1);
        patvl2(niter)=topoP(nxcs_vl2,nycs_vl2)+Eta(nxcs_vl2,nycs_vl2);

        for k=1:nr
            hFacC(1:nx,1:ny,k)= 1; %(Ro+Eta)./Ro.*hfacc(:,:,k);
        end;
        [Arq2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(Arq,ny_latlon,yc,rac,hFacC);
        [Arm2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(Arm,ny_latlon,yc,rac,hFacC);
        [Ar2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(ptr,ny_latlon,yc,rac,hFacC);



% calculate zonal mean temperature, winds and streamfunction
        for k=1:size(uphy,3)
            hFacC_phy(1:nx,1:ny,k)= 1; %(Ro+Eta)./Ro.*hfacc(:,:,ksurf(i,j));
        end;

        hFacF_fake_dyn(1:nx,1:ny,1:nr+1)=1.0;
        hFacF_fake_phy(1:nx,1:ny,1:size(uphy,3))=1.0;

        pcphy=(pephy(:,:,2:size(pephy,3))+pephy(:,:,1:size(pephy,3)-1))/2.0;

% rotate U and V and calculate the zonal mean zonal wind
        [uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C');
        [uphyE,vphyN] = rotate_uv2uvEN(uphy,vphy,AngleCS,AngleSN,'C');

% interpolate the temperature and Ar to reference pressure profile

        for k=1:nr
            Tdyn(:,:,k)=theta(:,:,k).*(pcdyn(:,:,k)/atm_Po).^atm_kappa;
        end;

        for k=1:size(thphy,3)-1
            Tphy(:,:,k)=thphy(:,:,k).*(pcphy(:,:,k)/atm_Po).^atm_kappa;
        end;
        Tphy(:,:,size(thphy,3))=Tphy(:,:,size(thphy,3)-1);

        [Tdyn2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(Tdyn,ny_latlon,yc,rac,hFacC); 
        [U2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(uE,ny_latlon,yc,rac,hFacC);
        [V2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(vN,ny_latlon,yc,rac,hFacC);
        [pedyn2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(pedyn,ny_latlon,yc,rac,hFacF_fake_dyn);

        [Tphy2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(Tphy,ny_latlon,yc,rac,hFacC_phy); 
        [uphy2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(uphyE,ny_latlon,yc,rac,hFacC_phy);
        [vphy2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(vphyN,ny_latlon,yc,rac,hFacC_phy);
        [pephy2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(pephy,ny_latlon,yc,rac,hFacF_fake_phy); 
        
        uhyb2d(:,1:nphys)=uphy2dtmp(:,1:nphys); uhyb2d(:,nphys+1:nhybrid)=U2dtmp(:,kmax:nr);
        vhyb2d(:,1:nphys)=vphy2dtmp(:,1:nphys); vhyb2d(:,nphys+1:nhybrid)=V2dtmp(:,kmax:nr);
        Thyb2d(:,1:nphys)=Tphy2dtmp(:,1:nphys); Thyb2d(:,nphys+1:nhybrid)=Tdyn2dtmp(:,kmax:nr);
%        uhyb2d(:,nphys+1)=(uhyb2d(:,nphys)+uhyb2d(:,nphys+2))/2.0;
%        vhyb2d(:,nphys+1)=(vhyb2d(:,nphys)+vhyb2d(:,nphys+2))/2.0;
%        Thyb2d(:,nphys+1)=(Thyb2d(:,nphys)+Thyb2d(:,nphys+2))/2.0;
        uhyb2d(:,nphys+1)=(uphy2dtmp(:,nphys+1)+U2dtmp(:,kmax))/2.0;
        vhyb2d(:,nphys+1)=(vphy2dtmp(:,nphys+1)+V2dtmp(:,kmax))/2.0;
        Thyb2d(:,nphys+1)=(Tphy2dtmp(:,nphys+1)+Tdyn2dtmp(:,kmax))/2.0;


        pehyb2d(:,1:nphys+1)=pephy2dtmp(:,1:nphys+1); 
        pehyb2d(:,nphys+2:nhybrid+1)=pedyn2dtmp(:,kmax+1:nr+1);
        pchyb2d=(pehyb2d(:,1:nhybrid)+pehyb2d(:,2:nhybrid+1))/2.0;
        dphyb2d=(pehyb2d(:,1:nhybrid)-pehyb2d(:,2:nhybrid+1));

        % calculate streamfunction
        u3dhyb(:,:,1:nphys)=uphy(:,:,1:nphys); u3dhyb(:,:,nphys+1:nhybrid)=u3d(:,:,kmax:nr); 
        v3dhyb(:,:,1:nphys)=vphy(:,:,1:nphys); v3dhyb(:,:,nphys+1:nhybrid)=v3d(:,:,kmax:nr); 
        u3dhyb(:,:,nphys)=(u3dhyb(:,:,nphys-1)+u3dhyb(:,:,nphys+1))/2.0;
        v3dhyb(:,:,nphys)=(v3dhyb(:,:,nphys-1)+v3dhyb(:,:,nphys+1))/2.0;

        thhyb(:,:,1:nphys)=thphy(:,:,1:nphys); thhyb(:,:,nphys+1:nhybrid)=theta(:,:,kmax:nr);
        pehyb(:,:,1:nphys+1)=pephy(:,:,1:nphys+1); pehyb(:,:,nphys+2:nhybrid+1)=pedyn(:,:,kmax+1:nr+1);
        thhyb(:,:,nphys+1)=(thphy(:,:,nphys+1)+theta(:,:,kmax))/2.0;

        [zc,dz]=cal_height(thhyb, pehyb, topoZ, phys_para);
        hFacz(1:nx,1:ny,1:nhybrid)=1.0;
        [z2dhyb,mskzon,ylat,areazon] = calcZonalAvgCubeC(zc,ny_latlon,yc,rac,hFacz);

        [psi]=use_bk_line_mars(u3dhyb,v3dhyb,dxg,dyg,rac,dphyb2d,namf_cs,grv);

% move to permanent storage        
        Ar2d(:,niter)=Arq2d;  % Ar vertically integrated mixing ratio
        Ar2m(:,niter)=Arm2d;  % Ar zonal mean vertically integrated total mass
%        hfac2d(:,niter)=hFacC2d; 
      
        Ar2dt(:,:,niter)=Ar2dtmp; % Ar mixing ratio, (P,y,ls)
        T2dt(:,:,niter)=Thyb2d;   % Temperature [K], (p,y,ls)
        U2dt(:,:,niter)=uhyb2d;   % U, (p,y,ls) 
        V2dt(:,:,niter)=vhyb2d;   % V, (p,y,ls)
        p2dt(:,:,niter)=pchyb2d;
        z2dt(:,:,niter)=z2dhyb/1e3; % height in km
        psi2dt(:,:,niter)=psi(2:ny_latlon+1,2:nhybrid+1);
        co2icemass(:,:,niter)=icemass(:,:);

% calculate ls -------
        julian=day(niter)/p2si;
        date_dbl = julian - zero_date;
        while (date_dbl < 0.)
           date_dbl=date_dbl+planet_year;
        end;
        while (date_dbl > planet_year)
           date_dbl=date_dbl-planet_year;
        end;


%  determine true anomaly at current date:  w
%  Iteration for w
       em = 2.0 * pi * date_dbl/planet_year;
       e  = 1.0;
       cd0 = 1.0;
       while (cd0 > SMALL_VALUE)
       ep = e-(e-eccentricity*sin(e)-em) / (1.0-eccentricity*cos(e));
       cd0 = abs(e-ep);
       e = ep;
       end;
       w = 2.0 * atan( er * tan(0.5*e) );
 
%  Radius vector ( astronomical units:  AU )
       als= (w - eq)*180.0/pi;      %Aerocentric Longitude
       if (als < 0.0) 
          als=als+360.0;
       end;
       ls(niter) = als;

end;
       
for k=1:nhybrid
    y2d(1:ny_latlon,k)=ylat;
end;

 
fprintf('Surface co2 ice vs Ls, co2icemass [ %d x %d x %d ]:  \n',nx,ny,niter);
fprintf('Zonal mean Ar mixing ration vs Ls, Ar2dt [ %d x %d x %d ]:  \n',ny_latlon,nr,niter);
fprintf('Zonal mean Temperature vs Ls, T2dt [ %d x %d x %d ]:  \n',ny_latlon,nhybrid,niter);
fprintf('Zonal mean zonal wind vs Ls, U2dt [ %d x %d x %d ]:  \n',ny_latlon,nhybrid,niter);
fprintf('Zonal mean streamfunction vs Ls, psi2dt [ %d x %d x %d ]:  \n',ny_latlon,nhybrid,niter);
fprintf('Vertically integrated Ar mixing ration vs Ls, Ar2d [ %d x %d ]:  \n',ny_latlon,niter);
fprintf('Vertically integrated Ar mass vs Ls, Ar2m [ %d x %d ]:  \n',ny_latlon,niter);
fprintf('Ar mixing ratio at south pole vs Ls, ArS [ %d ]:  \n',niter);
fprintf('Ar mixing ratio at north pole vs Ls, ArN [ %d ]:  \n',niter);

if (multi_steps == 'y')
   T2dm=sum(T2dt,3)/length(alldata);
   U2dm=sum(U2dt,3)/length(alldata);
   V2dm=sum(V2dt,3)/length(alldata);
   z2dm=sum(z2dt,3)/length(alldata);
   p2dm=sum(p2dt,3)/length(alldata);
   psi2dm=sum(psi2dt,3)/length(alldata);
   fprintf('Time averaged dataset requested : T2dm, U2dm, V2dm, z2dm, p2dm, psi2dm \n');
end;
