function [namf_cs]=make_psi_lines(g2d, ny);

% ny is the number of grid points in latlon grid

xc=squeeze(g2d(:,:,1));
yc=squeeze(g2d(:,:,2));
xg=squeeze(g2d(:,:,3));
yg=squeeze(g2d(:,:,4));
dxg=squeeze(g2d(:,:,5));
dyg=squeeze(g2d(:,:,6));
rAc=squeeze(g2d(:,:,7));

dx_latlon=180.0/ny;

try_and_real=1;
gener_bk_line; % generate the broke lines, first try

try_and_real=0;
gener_bk_line;

dims=size(xc); nx=ndims(1); nc=dims(2);

ijprt=nc+1;

gen_bk_Zon;


