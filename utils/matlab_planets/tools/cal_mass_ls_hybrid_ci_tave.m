% first ntry=12 layers are in physics grid, max(ksurf) and above are in dynamics grid
% The hybrid grid is for zonal mean of temperature, winds and stream function only!!!
 

clear all;

% section 1: load grid files and data configurations
chdatapath=input('full path to load data /~/ : ','s');
ny_latlon=input('grid points in y in latlon grid: ' );
deltaT=input('time step: ');
tstart=input('starting time step: ');
tend=input('ending time steps: ');
dnout=input('Output interval in number of time steps: ');
multi_steps=input('time average over multiple dataset (y/n): ', 's');
plotstream=input('plot meridional stream functions (y/n): ', 's');

rac=rdmds([chdatapath,'RAC']);
dxg=rdmds([chdatapath,'DXG']);
dyg=rdmds([chdatapath,'DYG']);
xg=rdmds([chdatapath,'XG']);
yg=rdmds([chdatapath,'YG']);
xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);
hfacc=rdmds([chdatapath,'hFacC']);
drF=rdmds([chdatapath,'DRF']); drF=squeeze(drF);
topoP=rdmds([chdatapath,'topo_P']);
topoZ=rdmds([chdatapath,'topo_H']);

g2d(:,:,1)=xc; g2d(:,:,2)=yc; g2d(:,:,3)=xg; g2d(:,:,4)=yg;
g2d(:,:,5)=dxg; 
g2d(:,:,6)=dyg; 
g2d(:,:,7)=rac;

% section 2: preprocessing for grid structure and grid points required for 
% viking lander sites as well as the Mars physics parameters

if (plotstream == 'y')
   [namf_cs]=make_psi_lines(g2d, ny_latlon);
else
   'no options'
end;

[AngleCS,AngleSN] = cubeCalcAngle(yg,rac,dxg,dyg);

% polar average, some 'deg_pole' poleward.
deg_pole=75;

for k=1:(tend-tstart)/dnout+1
    nstep(k)=tstart+dnout*(k-1);
end;
alldata=nstep;

% find the viking lander 1 and 2 sites on the cubed-spherical grid.
for i=1:size(xc,1)
 for j=1:size(xc,2)
     if (abs(xc(i,j)-(-48.222))<180/ny_latlon/2 && abs(yc(i,j)-22.697)<180/ny_latlon/2 )
        fprintf('\n VL1 m=%d n=%d \n',i,j)
        nxcs_vl1=i; nycs_vl1=j;
%     elseif (abs(xc(i,j)-(-48.222))<180/ny_latlon && abs(yc(i,j)-22.697)<180/ny_latlon )
%        fprintf('\n VL1 m=%d n=%d \n',i,j)
%        nxcs_vl1=i; nycs_vl1=j;
     end;
     if (abs(xc(i,j)-134.2630)<180/ny_latlon/2 && abs(yc(i,j)-47.967)<180/ny_latlon/2 )
        fprintf('\n VL2 m=%d n=%d \n',i,j)
        nxcs_vl2=i; nycs_vl2=j;
     elseif (abs(xc(i,j)-134.2630)<180/ny_latlon && abs(yc(i,j)-47.967)<180/ny_latlon )
        fprintf('\n VL2 m=%d n=%d \n',i,j)
        nxcs_vl2=i; nycs_vl2=j;
     end;
 end;
end;

% Mars physics parameters, including gravity, atmospheric properties and topography
grv=3.727;
atm_Po=610.0;
atm_cp=767.35;
atm_kappa=0.25;
zlow=-6.5470e3; % 2.5 degree res only
ztop=80.0e3; % set maximum altitude to 80km
rsphere=3389.9E3;
drsphere=rsphere*(1.0/ny_latlon)*pi;

phys_para(1)=atm_Po;
phys_para(2)=atm_cp;
phys_para(3)=atm_kappa;
phys_para(4)=grv;

% define some constants for calculating the solar angle l_s
p2si=1.0274912;
equinox_fraction= 0.2695;
planet_year=669.0;
zero_date=488.7045;
eccentricity=0.09341233;
semimajoraxis= 1.52366231;
SMALL_VALUE=1.0e-6;
deleqn = equinox_fraction * planet_year;
qq = 2.0 * (pi * deleqn / planet_year);
er = sqrt( (1.0+eccentricity)/(1.0-eccentricity) );
%  determine true anomaly at equinox:  eq
%  Iteration for eq
   e = 1.0;
   cd0 = 1.0;
   while (cd0 > SMALL_VALUE)
      ep = e - (e-eccentricity*sin(e)-qq) / (1.0-eccentricity*cos(e));
      cd0 = abs(e-ep);
      e = ep;
   end;
   eq = 2.0 * atan( er * tan(0.5*e) );

% zonal mean topographya
topoZ=topoZ+zlow;
hFac(1:size(topoZ,1),1:size(topoZ,2))=1;
z1d=calcZonalAvgCubeC(topoZ,ny_latlon,yc,rac,squeeze(hFac(:,:)));
p1d=calcZonalAvgCubeC(topoP,ny_latlon,yc,rac,squeeze(hFac(:,:)));
z1d=z1d/1e3; % in km

% load all data need to be processed, including winds, ice, tracers and temperature

% dynamics grid
ustr='U';
vstr='V';
cistr='co2ice';     % surface ice
castr='co2sub_air'; % ice in the atmosphere
ptrstr='PTRtave01'; % Ar 
n2str='S';          % N2
thetastr='T';       % potential temperature
pedstr='pedyn';     % pressure field (co2+N2)
Etastr='ETAtave';       % surface pressure anomaly

% physics grid
uphystr='uphy';
vphystr='vphy';
thphystr='thphy';
pephystr='pephy';

% reconstruct hybrid grid: lower physics grid + higher dynamics grid
% so the pressure field is in consistent with level numbers.
 
% find levels for new grid ----------------------

% physics grid part
nphys=12;

% before start calculation, find ksurf
% find array size -------------------------------

rC=rdmds([chdatapath,'RC']);
rC=squeeze(rC); 

dims=size(topoP);
nx=dims(1); ny=dims(2); nr=length(rC); 

% dynamics grid part
ksurf(1:nx,1:ny)=1;

for i=1:nx
  for j=1:ny
    kindex(1:nr)=nr+1;
    for k=1:nr-1
       if (hfacc(i,j,k)>0)                                    
          kindex(k)=k;
       end;
    end;
    ksurf(i,j)=min(kindex);
  end;
end;

for i=1:nx
  for j=1:ny
    for k=1:nr
       if (k<ksurf(i,j))
          mask(i,j,k)=NaN;
       else
          mask(i,j,k)=1;
       end;
    end;
  end;
end;

kmax=max(max(ksurf));

nhybrid=nphys+nr-kmax+1;

%------------------------------------------------

% section 3: main loop to process data, including zonal mean 
%            and time evolution

for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter));
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        time=allzero
        timed=alldata(niter);
        day(niter)=timed*deltaT/86400; % this is earth day, nomalized to sol later
% dynamics grid variables
        ufile=sprintf('%s%s.%s',chdatapath,ustr,time);
        vfile=sprintf('%s%s.%s',chdatapath,vstr,time);
        cifile=sprintf('%s%s.%s',chdatapath,cistr,time);
        cafile=sprintf('%s%s.%s',chdatapath,castr,time);
        ptrfile=sprintf('%s%s.%s',chdatapath,ptrstr,time);
        n2file=sprintf('%s%s.%s',chdatapath,n2str,time);
        pedynfile=sprintf('%s%s.%s',chdatapath,pedstr,time);
        Etafile=sprintf('%s%s.%s',chdatapath,Etastr,time);
        thetafile=sprintf('%s%s.%s',chdatapath,thetastr,time);

        ci=rdmds(cifile);
        ca=rdmds(cafile);
        ptr=rdmds(ptrfile);
        n2tr=rdmds(n2file); 
        pedyn=rdmds(pedynfile);
        u3d=rdmds(ufile);
        v3d=rdmds(vfile);
        Eta=rdmds(Etafile);
        theta=rdmds(thetafile);

% easy one first, calculate the VL1 and VL2 pressure curve
        patvl1(niter)=topoP(nxcs_vl1,nycs_vl1)+Eta(nxcs_vl1,nycs_vl1);
        patvl2(niter)=topoP(nxcs_vl2,nycs_vl2)+Eta(nxcs_vl2,nycs_vl2);

% physics grid variables
        uphyfile=sprintf('%s%s.%s',chdatapath,uphystr,time);
        vphyfile=sprintf('%s%s.%s',chdatapath,vphystr,time); 
        pephyfile=sprintf('%s%s.%s',chdatapath,pephystr,time);
        thphyfile=sprintf('%s%s.%s',chdatapath,thphystr,time);

        uphy=rdmds(uphyfile);
        vphy=rdmds(vphyfile);
        pephy=rdmds(pephyfile);
        thphy=rdmds(thphyfile);

% calculate the total air mass and surface co2 ice mass in kilograms--
        airmass=(topoP+Eta).*rac/grv;
        icemass=ci.*rac;

        totair(niter)=sum(sum(airmass));
        totice(niter)=sum(sum(icemass));

% calculate the total ice in north and south poles
        toticeN(niter)=0.0;
        toticeS(niter)=0.0;
        totairN(niter)=0.0;
        totairS(niter)=0.0;
        for i=1:nx
         for j=1:ny
% north polar ice and air
             if(yg(i,j)>0.0)
                toticeN(niter)=toticeN(niter)+icemass(i,j);
                totairN(niter)=totairN(niter)+airmass(i,j);
% south polar ice and air
             elseif (yg(i,j)<-0.0)
                toticeS(niter)=toticeS(niter)+icemass(i,j);
                totairS(niter)=totairS(niter)+airmass(i,j);
             end;
         end;
        end;

% construct dynamics grid
        dpdyn(:,:,1:nr)=pedyn(:,:,1:nr)-pedyn(:,:,2:nr+1);
        pcdyn(:,:,1:nr)=(pedyn(:,:,1:nr)+pedyn(:,:,2:nr+1))/2.0;

        for i=1:nx
         for j=1:ny
          for k=1:nr
              if (dpdyn(i,j,k)<0)
                  dpdyn(i,j,k)=0;
              end;
          end;
         end;
        end;

% calculate the global mean Ar/N2 mass density (kg/m^2)
        Armassmean(niter)=sum(sum(sum(ptr.*dpdyn/grv,3).*rac))/sum(sum(rac));
        N2massmean(niter)=sum(sum(sum(n2tr.*dpdyn/grv,3).*rac))/sum(sum(rac));
% calculate the grid-point and polar-cap Ar/N2 mass density (kg/m^2)
        Arlocal=ptr.*dpdyn/grv;
        N2local=n2tr.*dpdyn/grv;

        ArpolarN=zeros(nx,ny);
        ArpolarS=zeros(nx,ny);
        N2polarN=zeros(nx,ny);
        N2polarS=zeros(nx,ny);

        polarareaN=zeros(nx,ny);
        polarareaS=zeros(nx,ny);

% calculate the grid-point Ar and N2 mass in the polar caps (in kg).
        for i=1:nx
         for j=1:ny
          for k=1:nr
          if (yg(i,j)>deg_pole) % center of the grid includes 'deg_pole'
             ArpolarN(i,j)=ArpolarN(i,j)+Arlocal(i,j,k).*rac(i,j);
             N2polarN(i,j)=N2polarN(i,j)+N2local(i,j,k).*rac(i,j);
             polarareaN(i,j)=rac(i,j);
          end;
          if (yg(i,j)<-deg_pole)
             ArpolarS(i,j)=ArpolarS(i,j)+Arlocal(i,j,k).*rac(i,j);
             N2polarS(i,j)=N2polarS(i,j)+N2local(i,j,k).*rac(i,j);
             polarareaS(i,j)=rac(i,j);
          end;
          end;
         end;
        end;

% calculate the grid-point and area-weighted Ar and N2 mass mixing ratio in polar caps
% note that ArpolarN, ArpolarS, N2polarN and N2polarS have dummy "zeros"
% values outside of the polar caps. They have unit of kg m^2 at each grid point.
% The global operation is only for convenience!!!
        ArpolarN=ArpolarN*grv./(topoP+Eta);
        ArpolarS=ArpolarS*grv./(topoP+Eta);
        N2polarN=N2polarN*grv./(topoP+Eta);
        N2polarS=N2polarS*grv./(topoP+Eta);
% area weighted Ar and N2 mass mixing ratio, sum(d(mass_mixing_ratio)*d(rac))/sum(rac_polar)
        ArN(niter)=sum(sum(ArpolarN))/sum(sum(polarareaN));
        ArS(niter)=sum(sum(ArpolarS))/sum(sum(polarareaS));    
        N2N(niter)=sum(sum(N2polarN))/sum(sum(polarareaN));
        N2S(niter)=sum(sum(N2polarS))/sum(sum(polarareaS));
% (globally) column integrated grid-point mass mixing ratio and mass density for Ar and N2    
        Arq=sum(Arlocal,3)*grv./(topoP+Eta); 
        Arm=sum(Arlocal,3);
        N2q=sum(N2local,3)*grv./(topoP+Eta);
        N2m=sum(N2local,3);

% calculate the zonal mean Ar and N2 mass mixing ratio and mass density as a function of latitude
        for k=1:nr
            hFacC(1:nx,1:ny,k)= 1; %(Ro+Eta)./Ro.*hfacc(:,:,k);
        end;
        [Arq2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(Arq,ny_latlon,yc,rac,hFacC);
        [Arm2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(Arm,ny_latlon,yc,rac,hFacC);
        [N2q2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(N2q,ny_latlon,yc,rac,hFacC);
        [N2m2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(N2m,ny_latlon,yc,rac,hFacC);

% zonal mean Ar (mass mixing ratio) and N2 (specific humidity) for quick diagnostics 
        [Ar2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(ptr.*mask,ny_latlon,yc,rac,hFacC);
        [N22dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(n2tr,ny_latlon,yc,rac,hFacC);

% calculate zonal mean temperature, winds and streamfunction-----------------------------

% prepare hybrid grid for further diagnostics
        for k=1:size(uphy,3)
            hFacC_phy(1:nx,1:ny,k)= 1; %(Ro+Eta)./Ro.*hfacc(:,:,ksurf(i,j));
        end;

        hFacF_fake_dyn(1:nx,1:ny,1:nr+1)=1.0;
        hFacF_fake_phy(1:nx,1:ny,1:size(uphy,3))=1.0;

        pcphy=(pephy(:,:,2:size(pephy,3))+pephy(:,:,1:size(pephy,3)-1))/2.0;

% rotate U and V and calculate the zonal mean zonal wind
        [uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C');
        [uphyE,vphyN] = rotate_uv2uvEN(uphy,vphy,AngleCS,AngleSN,'C');

% interpolate the temperature and Ar to reference pressure profile

        for k=1:nr
            Tdyn(:,:,k)=theta(:,:,k).*(pcdyn(:,:,k)/atm_Po).^atm_kappa;
        end;

        for k=1:size(thphy,3)-1
            Tphy(:,:,k)=thphy(:,:,k).*(pcphy(:,:,k)/atm_Po).^atm_kappa;
        end;
        Tphy(:,:,size(thphy,3))=Tphy(:,:,size(thphy,3)-1);

        [Tdyn2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(Tdyn,ny_latlon,yc,rac,hFacC); 
        [U2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(uE,ny_latlon,yc,rac,hFacC);
        [V2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(vN,ny_latlon,yc,rac,hFacC);
        [pedyn2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(pedyn,ny_latlon,yc,rac,hFacF_fake_dyn);

        [Tphy2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(Tphy,ny_latlon,yc,rac,hFacC_phy); 
        [uphy2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(uphyE,ny_latlon,yc,rac,hFacC_phy);
        [vphy2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(vphyN,ny_latlon,yc,rac,hFacC_phy);
        [pephy2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(pephy,ny_latlon,yc,rac,hFacF_fake_phy);
        [ca2dtmp,mskzon,ylat,areazon] = calcZonalAvgCubeC(ca,ny_latlon,yc,rac,hFacF_fake_phy); 
        
        uhyb2d(:,1:nphys)=uphy2dtmp(:,1:nphys); uhyb2d(:,nphys+1:nhybrid)=U2dtmp(:,kmax:nr);
        vhyb2d(:,1:nphys)=vphy2dtmp(:,1:nphys); vhyb2d(:,nphys+1:nhybrid)=V2dtmp(:,kmax:nr);
        Thyb2d(:,1:nphys)=Tphy2dtmp(:,1:nphys); Thyb2d(:,nphys+1:nhybrid)=Tdyn2dtmp(:,kmax:nr);
        uhyb2d(:,nphys+1)=(uphy2dtmp(:,nphys+1)+U2dtmp(:,kmax))/2.0;
        vhyb2d(:,nphys+1)=(vphy2dtmp(:,nphys+1)+V2dtmp(:,kmax))/2.0;
        Thyb2d(:,nphys+1)=(Tphy2dtmp(:,nphys+1)+Tdyn2dtmp(:,kmax))/2.0;


        pehyb2d(:,1:nphys+1)=pephy2dtmp(:,1:nphys+1); 
        pehyb2d(:,nphys+2:nhybrid+1)=pedyn2dtmp(:,kmax+1:nr+1);
        pchyb2d=(pehyb2d(:,1:nhybrid)+pehyb2d(:,2:nhybrid+1))/2.0;
        dphyb2d=(pehyb2d(:,1:nhybrid)-pehyb2d(:,2:nhybrid+1));

        % calculate streamfunction
        u3dhyb(:,:,1:nphys)=uphy(:,:,1:nphys); u3dhyb(:,:,nphys+1:nhybrid)=u3d(:,:,kmax:nr); 
        v3dhyb(:,:,1:nphys)=vphy(:,:,1:nphys); v3dhyb(:,:,nphys+1:nhybrid)=v3d(:,:,kmax:nr); 
        u3dhyb(:,:,nphys)=(u3dhyb(:,:,nphys-1)+u3dhyb(:,:,nphys+1))/2.0;
        v3dhyb(:,:,nphys)=(v3dhyb(:,:,nphys-1)+v3dhyb(:,:,nphys+1))/2.0;

        thhyb(:,:,1:nphys)=thphy(:,:,1:nphys); thhyb(:,:,nphys+1:nhybrid)=theta(:,:,kmax:nr);
        pehyb(:,:,1:nphys+1)=pephy(:,:,1:nphys+1); pehyb(:,:,nphys+2:nhybrid+1)=pedyn(:,:,kmax+1:nr+1);
        thhyb(:,:,nphys+1)=(thphy(:,:,nphys+1)+theta(:,:,kmax))/2.0;

        [zc,dz]=cal_height(thhyb, pehyb, topoZ, phys_para);
        hFacz(1:nx,1:ny,1:nhybrid)=1.0;
        [z2dhyb,mskzon,ylat,areazon] = calcZonalAvgCubeC(zc,ny_latlon,yc,rac,hFacz);

        if (plotstream == 'y')
           [psi]=use_bk_line_mars(u3dhyb,v3dhyb,dxg,dyg,rac,dphyb2d,namf_cs,grv);
        else
           'no options';
        end;
   
% move to permanent storage
        ArVL2(niter)=Arq(nxcs_vl2,nycs_vl2); 
        N2VL2(niter)=N2q(nxcs_vl2,nycs_vl2);       
        Ar2d(:,niter)=Arq2d;  % Ar vertically integrated mixing ratio
        Ar2m(:,niter)=Arm2d;  % Ar zonal mean vertically integrated total mass
        N22d(:,niter)=N2q2d;  % N2 vertically integrated mixing ratio
        N22m(:,niter)=N2m2d;  % N2 zonal mean vertically integrated total mass
      
        Ar2dt(:,:,niter)=Ar2dtmp; % Ar mixing ratio, (P,y,ls)
        N22dt(:,:,niter)=N22dtmp; % N2 mixing ratio, (P,y,ls)
        T2dt(:,:,niter)=Thyb2d;   % Temperature [K], (p,y,ls)
        U2dt(:,:,niter)=uhyb2d;   % U, (p,y,ls) 
        V2dt(:,:,niter)=vhyb2d;   % V, (p,y,ls)
        p2dt(:,:,niter)=pchyb2d;
        pe2dt(:,:,niter)=pephy2dtmp;
        z2dt(:,:,niter)=z2dhyb/1e3; % height in km
        co2icemass(:,:,niter)=icemass(:,:);
        ca2dt(:,:,niter)=ca2dtmp;
        
        if (plotstream == 'y')
           psi2dt(:,:,niter)=-psi(2:ny_latlon+1,2:nhybrid+1); % psi< 0 for clockwise flow
        else
           'no options';
        end;
%        Etat(:,:,niter)=Eta;

% calculate ls  (solar longitude) -------
        julian=day(niter)/p2si;
        date_dbl = julian - zero_date;
        while (date_dbl < 0.)
           date_dbl=date_dbl+planet_year;
        end;
        while (date_dbl > planet_year)
           date_dbl=date_dbl-planet_year;
        end;


%  determine true anomaly at current date:  w
%  Iteration for w
       em = 2.0 * pi * date_dbl/planet_year;
       e  = 1.0;
       cd0 = 1.0;
       while (cd0 > SMALL_VALUE)
       ep = e-(e-eccentricity*sin(e)-em) / (1.0-eccentricity*cos(e));
       cd0 = abs(e-ep);
       e = ep;
       end;
       w = 2.0 * atan( er * tan(0.5*e) );
 
%  Radius vector ( astronomical units:  AU )
       als= (w - eq)*180.0/pi;      %Aerocentric Longitude
       if (als < 0.0) 
          als=als+360.0;
       end;
       ls(niter) = als;

end;

% finally expand latitude to 2d        
for k=1:nhybrid;  y2d(1:ny_latlon,k)=ylat; end;
for k=1:nr-5+nphys+2;  ye2d(1:ny_latlon,k)=ylat; end;

 
fprintf('Surface co2 ice vs Ls, co2icemass [ %d x %d x %d ]:  \n',nx,ny,niter);
fprintf('Zonal mean Ar mixing ration vs Ls, Ar2dt [ %d x %d x %d ]:  \n',ny_latlon,nr,niter);
fprintf('Zonal mean Temperature vs Ls, T2dt [ %d x %d x %d ]:  \n',ny_latlon,nhybrid,niter);
fprintf('Zonal mean zonal wind vs Ls, U2dt [ %d x %d x %d ]:  \n',ny_latlon,nhybrid,niter);
fprintf('Zonal mean streamfunction vs Ls, psi2dt [ %d x %d x %d ]:  \n',ny_latlon,nhybrid,niter);
fprintf('Vertically integrated Ar mixing ration vs Ls, Ar2d [ %d x %d ]:  \n',ny_latlon,niter);
fprintf('Vertically integrated Ar mass density vs Ls, Ar2m [ %d x %d ]:  \n',ny_latlon,niter);
fprintf('Ar mixing ratio at south pole vs Ls, ArS [ %d ]:  \n',niter);
fprintf('Ar mixing ratio at north pole vs Ls, ArN [ %d ]:  \n',niter);
fprintf('N2 absolute mixing ratio at south pole vs Ls, N2S [ %d ]:  \n',niter);
fprintf('N2 absolute mixing ratio at north pole vs Ls, N2N [ %d ]:  \n',niter);
fprintf('Vertical levels: dynamics grid = %d, physics grid = %d, hybrid grid = %d  \n', nr, nr-5+nphys+2, nhybrid);

if (multi_steps == 'y')
   T2dm=sum(T2dt,3)/length(alldata);
   U2dm=sum(U2dt,3)/length(alldata);
   V2dm=sum(V2dt,3)/length(alldata);
   z2dm=sum(z2dt,3)/length(alldata);
   p2dm=sum(p2dt,3)/length(alldata);
   if (plotstream == 'y')
       psi2dm=sum(psi2dt,3)/length(alldata);
   end;
   fprintf('Time averaged dataset requested : T2dm, U2dm, V2dm, z2dm, p2dm, psi2dm \n');
end;
