%without co2 cycle, the model works 
%with box_size=40 and 60 for fspecial

clear all;

cs_path=input('enter the path to cs grid files /~/ : ','s');
%grid_files=sprintf('%s%s',cs_path,'CUBE_HGRID.mat');
%load(grid_files);
%%keep latC lonC;
%latC=180/pi*latC;
%lonC=180/pi*lonC;

%xg=rdmds([cs_path,'XG']);
%yg=rdmds([cs_path,'YG']);

filter_opt=2;

ny_orig=720; %was 360
nx_orig=ny_orig*2;

deg_size=input('resolution in latlon 5, 2.5 or 1.25 : ');

nc=180/deg_size/2;

[lonC0, latC0, lonG, latG,rac]=MITgrid(nc,cs_path);
lonC=lonC0(1:nc,1:nc,:);
latC=latC0(1:nc,1:nc,:);
xg=reshape(permute(lonG(1:nc,1:nc,:),[1 3 2]), [nc*6 nc]);
yg=reshape(permute(latG(1:nc,1:nc,:),[1 3 2]), [nc*6 nc]);

dble_tin=1;

load '/Users/lian/workdir/WRFV3/Data/albedo.tbl';
load '/Users/lian/workdir/WRFV3/Data/inertia.tbl';

fid=fopen('/Users/lian/workdir/WRFV3/Data/roughness.dat');
znt=fread(fid,[720 360],'int8','b');
fclose(fid);

znt=znt*0.15/198; %the conversion is referred to WRF

adims=size(albedo);
idims=size(inertia);

abd=albedo(:,3);   % albedo 
tin=inertia(:,3); % thermal inertia

abd=reshape(abd,[360 180]);
tin=reshape(tin,[180 90 ]);

sst=dlmread('/Users/lian/workdir/WRFV3/Data/ssts.n18'); % skin temperature
sst0=reshape(sst',[2520 1]);
jj=1;
for ii=1:252*10
  if (sst0(ii)~=0) sst1(jj)=sst0(ii); jj=jj+1; end;
end;
sst_sp(:,2:37)=reshape(sst1,[62 36]);

clear albedo inertia sst sst0 sst1;

dx=180/ny_orig;
x=[-180+dx/2:dx:180-dx/2];
y=[-90+dx/2:dx:90-dx/2];

% first exapnd abd and tin to be in size of 1440x720

abd_sp(2:361,2:181)=abd;
abd_sp(1,2:181)=abd(360,:);
abd_sp(362,2:181)=abd(2,:);

abd_sp(:,1:3)=sum(abd_sp(:,2))/362;
abd_sp(:,180:182)=sum(abd_sp(:,181))/362;

tin_sp(2:181,2:91)=tin;
tin_sp(1,2:91)=tin(180,:);
tin_sp(182,2:91)=tin(2,:);

tin_sp(:,1:3)=sum(tin_sp(:,2))/182;
tin_sp(:,90:92)=sum(tin_sp(:,91))/182;

znt_sp(2:721,2:361)=znt;
znt_sp(1,2:361)=znt(720,:);
znt_sp(722,2:361)=znt(2,:);

znt_sp(:,1:3)=sum(znt_sp(:,2))/722;
znt_sp(:,360:362)=sum(znt_sp(:,361))/722;

for isst=1:62
    xs(isst)=-183. + (isst-1)*6;
end;
for jsst=1:38
    ys(jsst)  = -92.5 + (jsst-1)*5.;
end;
sst_sp(:,1) =sum(sst_sp(:,2))/62;
sst_sp(:,38)=sum(sst_sp(:,37))/62;


clear abd tin znt;

dxa=180/180;
xa=[-180-dxa/2:dxa:180+dxa/2];
ya=[-90-dxa/2:dxa:90+dxa/2];

dxt=180/90;
xt=[-180-dxt/2:dxt:180+dxt/2];
yt=[-90-dxt/2:dxt:90+dxt/2];

dxz=180/360;
xz=[-180-dxz/2:dxz:180+dxz/2];
yz=[-90-dxz/2:dxz:90+dxz/2];

[cy,cx]=meshgrid(y,x);
abd=interp2(ya,xa,abd_sp,cy,cx);
tin=interp2(yt,xt,tin_sp,cy,cx);
znt=interp2(yz,xz,znt_sp,cy,cx);
sst=interp2(ys,xs,sst_sp,cy,cx);

%abd(:,ny_orig)=abd(:,ny_orig-1);
%abd(:,1)  =abd(:,2);
%tin(:,ny_orig-1:ny_orig)=tin(:,ny_orig-2:-1:ny_orig-3);
%tin(:,1:2)  =tin(:,4:-1:3);
%znt(:,ny_orig)=znt(:,ny_orig-1);
%znt(:,1)  =znt(:,2);

% make smallest thermal inertia to be 200

tindims=size(tin); tinx=tindims(1); tiny=tindims(2);

% for most up2date runs, such as workdir_hires_ptrAdv77_37L, the following tweak is 
% not applied.
%for i=1:tinx
% for j=1:tiny 
%  if (tin(i,j)<150.0)
%      tin(i,j)=150.0;
%  end;
% end;
%end;

% now, double the thermal inertia near the polar caps
% They were a bit small after the interpolation anyway
% 70S and 70N
if (dble_tin==1)
   nloy=max(find(y<-70)); % south pole
   nhiy=min(find(y> 70)); % north pole
% south pole
%   tin(:,1:nloy)=60.0; % 2.0*tin(:,1:nloy);
% north pole
%   tin(:,nhiy:tiny)=150.0; % 2.0*tin(:,nhiy:tiny);
else
   'no option';
end;

if (filter_opt==1)
%use image filter to make smooth topography first
 h=fspecial('disk',20);
 abd = imfilter(abd,h,'replicate');
 tin = imfilter(tin,h,'replicate');
 znt = imfilter(znt,h,'replicate');
 sst = imfilter(sst,h,'replicate');
end;

clear abd_sp tin_sp znt_sp sst_sp;

% Now use a fancier approach for domain expansion
% based on the box size 5, 10 or 20 degree, only 
% works for 5 degree resolution in the model

%nc=36; %the resolution in cs grid is consistent with latlon

nc=180/deg_size/2;

%deg_size=2.5;
nx_cs=360/deg_size;
ny_cs=180/deg_size;

box_size=deg_size; % 20 degree for a single box

dx_orig=180/ny_orig; %360; % orginal map resolution
nbox_model=box_size/dx_orig; % number of grid points in a box
nbox_avg=box_size/dx_orig; %number of grid points in sweeping box

nlon_w=(nbox_avg-nbox_model)/2; nlon_e=nlon_w;
nlat_s=(nbox_avg-nbox_model)/2; nlat_n=nlat_s;

% expand the orginal arrays
abd_ep(nlon_w+1:nx_orig+nlon_e,nlat_s+1:ny_orig+nlat_n)=abd;
tin_ep(nlon_w+1:nx_orig+nlon_e,nlat_s+1:ny_orig+nlat_n)=tin;
znt_ep(nlon_w+1:nx_orig+nlon_e,nlat_s+1:ny_orig+nlat_n)=znt;
sst_ep(nlon_w+1:nx_orig+nlon_e,nlat_s+1:ny_orig+nlat_n)=sst;

abd_ep(nx_orig+nlon_e+1:nx_orig+2*nlon_e,nlat_s+1:ny_orig+nlat_n)=abd(1:nlon_w,:); 
abd_ep(1:nlon_w,nlat_s+1:ny_orig+nlat_n)=abd(nx_orig-nlon_e+1:nx_orig,:); % periodic in longitude

tin_ep(nx_orig+nlon_e+1:nx_orig+2*nlon_e,nlat_s+1:ny_orig+nlat_n)=tin(1:nlon_w,:); 
tin_ep(1:nlon_w,nlat_s+1:ny_orig+nlat_n)=tin(nx_orig-nlon_e+1:nx_orig,:); % periodic in longitude

znt_ep(nx_orig+nlon_e+1:nx_orig+2*nlon_e,nlat_s+1:ny_orig+nlat_n)=znt(1:nlon_w,:); 
znt_ep(1:nlon_w,nlat_s+1:ny_orig+nlat_n)=znt(nx_orig-nlon_e+1:nx_orig,:); % periodic in longitude

sst_ep(nx_orig+nlon_e+1:nx_orig+2*nlon_e,nlat_s+1:ny_orig+nlat_n)=sst(1:nlon_w,:); 
sst_ep(1:nlon_w,nlat_s+1:ny_orig+nlat_n)=sst(nx_orig-nlon_e+1:nx_orig,:); % periodic in longitude

clear abd tin znt sst; % release topoe in memory

abd_ep(:,1:nlat_s)=abd_ep(:,2*nlat_s:-1:nlat_s+1);
abd_ep(:,ny_orig+nlat_n+1:ny_orig+2*nlat_n)=abd_ep(:,ny_orig+nlat_n:-1:ny_orig+1); % expand in latitude

tin_ep(:,1:nlat_s)=tin_ep(:,2*nlat_s:-1:nlat_s+1);
tin_ep(:,ny_orig+nlat_n+1:ny_orig+2*nlat_n)=tin_ep(:,ny_orig+nlat_n:-1:ny_orig+1); % expand in latitude

znt_ep(:,1:nlat_s)=znt_ep(:,2*nlat_s:-1:nlat_s+1);
znt_ep(:,ny_orig+nlat_n+1:ny_orig+2*nlat_n)=znt_ep(:,ny_orig+nlat_n:-1:ny_orig+1); % expand in latitude

sst_ep(:,1:nlat_s)=sst_ep(:,2*nlat_s:-1:nlat_s+1);
sst_ep(:,ny_orig+nlat_n+1:ny_orig+2*nlat_n)=sst_ep(:,ny_orig+nlat_n:-1:ny_orig+1); % expand in latitude

if (filter_opt==2)
%use image filter to make smooth topography first
 h=fspecial('disk',10); % was 20
 abd_ep = imfilter(abd_ep,h,'replicate');
 tin_ep = imfilter(tin_ep,h,'replicate');
 znt_ep = imfilter(znt_ep,h,'replicate');
 sst_ep = imfilter(sst_ep,h,'replicate');
end;

% box-average the topography to produce 72x36 resolution
for i=1:nx_cs  
for j=1:ny_cs  
box_abd(i,j)=sum(sum(abd_ep((i-1)*nbox_model+1:(i-1)*nbox_model+nbox_avg, ...
            (j-1)*nbox_model+1:(j-1)*nbox_model+nbox_avg)))/(nbox_avg^2);
box_tin(i,j)=sum(sum(tin_ep((i-1)*nbox_model+1:(i-1)*nbox_model+nbox_avg, ...
            (j-1)*nbox_model+1:(j-1)*nbox_model+nbox_avg)))/(nbox_avg^2);
box_znt(i,j)=sum(sum(znt_ep((i-1)*nbox_model+1:(i-1)*nbox_model+nbox_avg, ...
            (j-1)*nbox_model+1:(j-1)*nbox_model+nbox_avg)))/(nbox_avg^2);
box_sst(i,j)=sum(sum(sst_ep((i-1)*nbox_model+1:(i-1)*nbox_model+nbox_avg, ...
            (j-1)*nbox_model+1:(j-1)*nbox_model+nbox_avg)))/(nbox_avg^2);
end;
end;

% cell centered lat-lon coordinate, add ghost points to 
% longitudinal direction for better interpolation

abd_sp(2:nx_cs+1,:)=box_abd;
abd_sp(1,:)=box_abd(nx_cs,:);
abd_sp(nx_cs+2,:)=box_abd(2,:);

tin_sp(2:nx_cs+1,:)=box_tin;
tin_sp(1,:)=box_tin(nx_cs,:);
tin_sp(nx_cs+2,:)=box_tin(2,:);

znt_sp(2:nx_cs+1,:)=box_znt;
znt_sp(1,:)=box_znt(nx_cs,:);
znt_sp(nx_cs+2,:)=box_znt(2,:);

sst_sp(2:nx_cs+1,:)=box_sst;
sst_sp(1,:)=box_sst(nx_cs,:);
sst_sp(nx_cs+2,:)=box_sst(2,:);

if (filter_opt==3) 
  h=fspecial('disk',6);
  abd_sp= imfilter(abd_sp,h,'replicate');
  tin_sp= imfilter(tin_sp,h,'replicate');  
  znt_sp= imfilter(znt_sp,h,'replicate');
  sst_sp= imfilter(sst_sp,h,'replicate'); 
end;

dx=deg_size;
xc=[-180-dx/2:dx:180+dx/2];
yc=[-90+dx/2:dx:90-dx/2];

%xcs=rdmds('XC');
%ycs=rdmds('YC');

x6s=lonC; %x6s=permute(reshape(xcs,[nc 6 nc]),[1 3 2]);
y6s=latC; %y6s=permute(reshape(ycs,[nc 6 nc]),[1 3 2]);

abd_cs=zeros(nc,nc,6);
tin_cs=zeros(nc,nc,6);
znt_cs=zeros(nc,nc,6);
sst_cs=zeros(nc,nc,6);

for ntiles=1:6
 abd_cs(:,:,ntiles)=interp2(yc,xc,abd_sp,y6s(:,:,ntiles),x6s(:,:,ntiles));
 tin_cs(:,:,ntiles)=interp2(yc,xc,tin_sp,y6s(:,:,ntiles),x6s(:,:,ntiles));
 znt_cs(:,:,ntiles)=interp2(yc,xc,znt_sp,y6s(:,:,ntiles),x6s(:,:,ntiles));
 sst_cs(:,:,ntiles)=interp2(yc,xc,sst_sp,y6s(:,:,ntiles),x6s(:,:,ntiles));
end;

abd_cs=permute(abd_cs,[1 3 2]);
abd_cs=reshape(abd_cs,[nc*6 nc]);

tin_cs=permute(tin_cs,[1 3 2]);
tin_cs=reshape(tin_cs,[nc*6 nc]);

znt_cs=permute(znt_cs,[1 3 2]);
znt_cs=reshape(znt_cs,[nc*6 nc]);

sst_cs=permute(sst_cs,[1 3 2]);
sst_cs=reshape(sst_cs,[nc*6 nc]);


fid=fopen('albedo.bin','w');
fwrite(fid,abd_cs,'real*8','b');
fclose(fid);

fid=fopen('inertia.bin','w');
fwrite(fid,tin_cs,'real*8','b');
fclose(fid);

fid=fopen('roughness.bin','w');
fwrite(fid,znt_cs,'real*8','b');
fclose(fid);

fid=fopen('sst.bin','w');
fwrite(fid,sst_cs,'real*8','b');
fclose(fid);




