clear all;
fid=fopen('/Users/lian/workdir/WRFV3/Data/ieg0031t.dat');
topo=fread(fid,[11520 5760],'int16','b');
topoe=topo(1:8:11520,1:8:5760);
clear topo;
fclose(fid);

% cut off anywhere higher than 10km 
% and deeper than -5km
% possible working topo -5km -- 10km
for i=1:1440
for j=1:720 
if topoe(i,j)>100000.0
topoe(i,j)=100000.0;
elseif topoe(i,j)<-8000.0
topoe(i,j)=-8000.0;
else 
'no option';
end;
end;            
end;

% or load topo_mid.dat;

% Now use a fancier approach for domain expansion
% based on the box size 5, 10 or 20 degree, only 
% works for 5 degree resolution in the model

box_size=40; % 20 degree for a single box
dx_orig=180/720; % orginal map resolution
nbox_model=5/dx_orig; % number of grid points in a box
nbox_avg=box_size/dx_orig; %number of grid points in sweeping box

nlon_w=(nbox_avg-nbox_model)/2; nlon_e=nlon_w;
nlat_s=(nbox_avg-nbox_model)/2; nlat_n=nlat_s;

topoe=topoe-min(min(topoe)); % set the lowest point to be zero

topo(nlon_w+1:1440+nlon_e,nlat_s+1:720+nlat_n)=topoe; % expand the topography array

topo(1440+nlon_e+1:1440+2*nlon_e,nlat_s+1:720+nlat_n)=topoe(1:nlon_w,:); 
topo(1:nlon_w,nlat_s+1:720+nlat_n)=topoe(1440-nlon_e+1:1440,:); % periodic in longitude

clear topoe; % release topoe in memory

topo(:,1:nlat_s)=topo(:,2*nlat_s:-1:nlat_s+1);
topo(:,720+nlat_n+1:720+2*nlat_n)=topo(:,720+nlat_n:-1:721); % expand in latitude

% box-average the topography to produce 72x36 resolution
for i=1:72  
for j=1:36  
box(i,j)=sum(sum(topo((i-1)*nbox_model+1:(i-1)*nbox_model+nbox_avg, ...
            (j-1)*nbox_model+1:(j-1)*nbox_model+nbox_avg)))/(nbox_avg^2);
end;
end;

% cell centered lat-lon coordinate, add ghost points to 
% longitudinal direction for better interpolation

box_sp(2:73,:)=box;
box_sp(1,:)=box(72,:);
box_sp(74,:)=box(2,:);

dx=180/36;
xc=[-180-dx/2:dx:180+dx/2];
yc=[-90+dx/2:dx:90-dx/2];

xcs=rdmds('XC');
ycs=rdmds('YC');

nc=18;

x6s=permute(reshape(xcs,[nc 6 nc]),[1 3 2]);
y6s=permute(reshape(ycs,[nc 6 nc]),[1 3 2]);

topo_cs=zeros(nc,nc,6);

for ntiles=1:6
 topo_cs(:,:,ntiles)=interp2(yc,xc,box_sp,y6s(:,:,ntiles),x6s(:,:,ntiles));
end;

topo_cs=permute(topo_cs,[1 3 2]);
topo_cs=reshape(topo_cs,[nc*6 nc]);
topo_cs=(topo_cs-min(min(topo_cs)))/1.0;

fid=fopen('topo.bin','w');
fwrite(fid,topo_cs,'real*8','b');
fclose(fid);



