%without co2 cycle, the model works 
%with box_size=40 and 60 for fspecial

clear all;

cs_path=input('enter the path to cs grid files /~/ : ','s');
%grid_files=sprintf('%s%s',cs_path,'CUBE_HGRID.mat');
%load(grid_files);
%%keep latC lonC;
%latC=180/pi*latC;
%lonC=180/pi*lonC;

%xg=rdmds([cs_path,'XG']);
%yg=rdmds([cs_path,'YG']);

filter_opt=1;

ny_orig=720; %was 360
nx_orig=ny_orig*2;

deg_size=input('resolution in latlon 5, 2.5, 1.25, 0.625 : ');

nc=180/deg_size/2;

[lonC0, latC0, lonG, latG,rac]=MITgrid(nc,cs_path);
lonC=lonC0(1:nc,1:nc,:);
latC=latC0(1:nc,1:nc,:);
xg=reshape(permute(lonG(1:nc,1:nc,:),[1 3 2]), [nc*6 nc]);
yg=reshape(permute(latG(1:nc,1:nc,:),[1 3 2]), [nc*6 nc]);

dble_tin=1;

load '~/matlab/tools/mars_surface_data/albedo.tbl';
load '~/matlab/tools/mars_surface_data/inertia.tbl';

fid=fopen('~/matlab/tools/mars_surface_data/roughness.dat');
znt=fread(fid,[720 360],'int8','b');
fclose(fid);

znt=znt*0.15/198; %the conversion is referred to WRF

adims=size(albedo);
idims=size(inertia);

abd=albedo(:,3);   % albedo 
tin=inertia(:,3); % thermal inertia

abd=reshape(abd,[360 180]);
tin=reshape(tin,[180 90 ]);

sst=dlmread('~/matlab/tools/mars_surface_data/ssts.n18'); % skin temperature
sst0=reshape(sst',[2520 1]);
jj=1;
for ii=1:252*10
  if (sst0(ii)~=0) sst1(jj)=sst0(ii); jj=jj+1; end;
end;
sst_sp(:,2:37)=reshape(sst1,[62 36]);

clear albedo inertia sst sst0 sst1;

dx=180/ny_orig;
x=[-180-dx/2:dx:180+dx/2];
y=[-90-dx/2:dx:90+dx/2];

% first exapnd abd and tin to be in size of 1440x720

abd_sp(2:361,2:181)=abd;
abd_sp(1,2:181)=abd(360,:);
abd_sp(362,2:181)=abd(2,:);

abd_sp(:,1:3)=sum(abd_sp(:,2))/362;
abd_sp(:,180:182)=sum(abd_sp(:,181))/362;

tin_sp(2:181,2:91)=tin;
tin_sp(1,2:91)=tin(180,:);
tin_sp(182,2:91)=tin(2,:);

tin_sp(:,1:3)=sum(tin_sp(:,2))/182;
tin_sp(:,90:92)=sum(tin_sp(:,91))/182;

znt_sp(2:721,2:361)=znt;
znt_sp(1,2:361)=znt(720,:);
znt_sp(722,2:361)=znt(2,:);

znt_sp(:,1:3)=sum(znt_sp(:,2))/722;
znt_sp(:,360:362)=sum(znt_sp(:,361))/722;

for isst=1:62
    xs(isst)=-183. + (isst-1)*6;
end;
for jsst=1:38
    ys(jsst)  = -92.5 + (jsst-1)*5.;
end;
sst_sp(:,1) =sum(sst_sp(:,2))/62;
sst_sp(:,38)=sum(sst_sp(:,37))/62;


clear abd tin znt;

dxa=180/180;
xa=[-180-dxa/2:dxa:180+dxa/2];
ya=[-90-dxa/2:dxa:90+dxa/2];

dxt=180/90;
xt=[-180-dxt/2:dxt:180+dxt/2];
yt=[-90-dxt/2:dxt:90+dxt/2];

dxz=180/360;
xz=[-180-dxz/2:dxz:180+dxz/2];
yz=[-90-dxz/2:dxz:90+dxz/2];

[cy,cx]=meshgrid(y,x);
abd=interp2(ya,xa,abd_sp,cy,cx);
tin=interp2(yt,xt,tin_sp,cy,cx);
znt=interp2(yz,xz,znt_sp,cy,cx);
sst=interp2(ys,xs,sst_sp,cy,cx);

%abd(:,ny_orig)=abd(:,ny_orig-1);
%abd(:,1)  =abd(:,2);
%tin(:,ny_orig-1:ny_orig)=tin(:,ny_orig-2:-1:ny_orig-3);
%tin(:,1:2)  =tin(:,4:-1:3);
%znt(:,ny_orig)=znt(:,ny_orig-1);
%znt(:,1)  =znt(:,2);

% make smallest thermal inertia to be 200

tindims=size(tin); tinx=tindims(1); tiny=tindims(2);

% now, double the thermal inertia near the polar caps
% They were a bit small after the interpolation anyway
% 70S and 70N
if (dble_tin==1)
   nloy=max(find(y<-70)); % south pole
   nhiy=min(find(y> 70)); % north pole
% south pole
%   tin(:,1:nloy)=60.0; % 2.0*tin(:,1:nloy);
% north pole
%   tin(:,nhiy:tiny)=150.0; % 2.0*tin(:,nhiy:tiny);
else
   'no option';
end;

if (filter_opt==1)
%use image filter to make smooth topography first
 h=fspecial('disk',10);
 abd = imfilter(abd,h,'replicate');
 tin = imfilter(tin,h,'replicate');
 znt = imfilter(znt,h,'replicate');
 sst = imfilter(sst,h,'replicate');
end;

clear abd_sp tin_sp znt_sp sst_sp;

dx=180/720;
xc=[-180-dx/2:dx:180+dx/2];
yc=[-90-dx/2:dx:90+dx/2];

x6s=lonC; %x6s=permute(reshape(xcs,[nc 6 nc]),[1 3 2]);
y6s=latC; %y6s=permute(reshape(ycs,[nc 6 nc]),[1 3 2]);

abd_cs=zeros(nc,nc,6);
tin_cs=zeros(nc,nc,6);
znt_cs=zeros(nc,nc,6);
sst_cs=zeros(nc,nc,6);

for ntiles=1:6
 abd_cs(:,:,ntiles)=interp2(yc,xc,abd,y6s(:,:,ntiles),x6s(:,:,ntiles));
 tin_cs(:,:,ntiles)=interp2(yc,xc,tin,y6s(:,:,ntiles),x6s(:,:,ntiles));
 znt_cs(:,:,ntiles)=interp2(yc,xc,znt,y6s(:,:,ntiles),x6s(:,:,ntiles));
 sst_cs(:,:,ntiles)=interp2(yc,xc,sst,y6s(:,:,ntiles),x6s(:,:,ntiles));
end;

abd_cs=permute(abd_cs,[1 3 2]);
abd_cs=reshape(abd_cs,[nc*6 nc]);

tin_cs=permute(tin_cs,[1 3 2]);
tin_cs=reshape(tin_cs,[nc*6 nc]);

znt_cs=permute(znt_cs,[1 3 2]);
znt_cs=reshape(znt_cs,[nc*6 nc]);

sst_cs=permute(sst_cs,[1 3 2]);
sst_cs=reshape(sst_cs,[nc*6 nc]);


fid=fopen('albedo.bin','w');
fwrite(fid,abd_cs,'real*8','b');
fclose(fid);

fid=fopen('inertia.bin','w');
fwrite(fid,tin_cs,'real*8','b');
fclose(fid);

fid=fopen('roughness.bin','w');
fwrite(fid,znt_cs,'real*8','b');
fclose(fid);

fid=fopen('sst.bin','w');
fwrite(fid,sst_cs,'real*8','b');
fclose(fid);




