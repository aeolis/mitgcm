function filter_matrix = bp(order, cutoff1, cutoff2);
% bp --> Creates a band pass filter
%	 The filter has size (order x order)
%	 and cutoffs specified between 0 and 1
%	 cutoff1 is the lowcutoff
%	 cutoff2 is the highcutoff

%Create desired frequency response
[f1,f2] = freqspace(order,'meshgrid');
d1 = find(f1.^2+f2.^2 < cutoff1^2);
d2 = find(f1.^2+f2.^2 < cutoff2^2);
Hd1 = zeros(order);
Hd1(d1) = ones(size(d1));
Hd2 = zeros(order);
Hd2(d2) = ones(size(d2));
Hd = zeros(order);
Hd = Hd2 - Hd1;

% Design the filter's impulse response
filter_matrix = fsamp2(Hd);

