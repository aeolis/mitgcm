function [X] = reorder(A, ndim, norder)

% ordering a matrix by dimension, up to a 4-D array
% usage X = reorder(A, ndim, norder)
% A       input matrix
% ndim    the dimension along which to order (integer)
% norder  new orders along dimension ndim
% 
% Example
% 
% for a 3-D matrix with size(A) = [4 4 3]
% X = reorder(A, 3, [1 3 2]);

dims = size(A); 
nend = length(norder);

if ( (length(size(A)) < ndim) || (dims(ndim) ~= length(norder)) )
   error(' wrong dimension to re-order, check the dimension of the array ');
end;

if ( ndim > 4 )
   error('excceed the maximum size of the array that reorder.m can handle');
end;

if      (ndim == 1)
   for n = 1:nend
       X(n,:,:,:) = A(norder(n),:,:,:);
   end;
elseif (ndim == 2)
   for n = 1:nend
       X(:,n,:,:) = A(:,norder(n),:,:);
   end;
elseif (ndim == 3)
   for n = 1:nend
       X(:,:,n,:) = A(:,:,norder(n),:);
   end;
elseif (ndim == 4) 
   for n = 1:nend
       X(:,:,:,n) = A(:,:,:,norder(n));
   end;
else   'no option';

end;

X = squeeze(X);
 
