function [xdata]=load_bin_data(chdatapath,xstr,multirec, varargin);

         time   = varargin{1};
         recnum = varargin{2};
         if (multirec=='y')
            xfile = sprintf('%s%s%s',chdatapath,xstr);
            xdata = rdmds(xfile,'rec',recnum);
         else
            xfile = sprintf('%s%s%s',chdatapath,xstr,time);
            xdata  =rdmds(xfile);
         end;
