clear all;
chdatapath=input('full path to load data /~/ : ','s');
nit=input('time to process (Press Enter key to use "alldata") : ');
deltaT=input('time step: ');
phys=input('physics grid = 1; dynamics grid = 0; ');

if (phys == 1)
    Tfile='thphy'; pfile='pephy';
else
    Tfile='T';     pfile='pedyn';
end;

grv=1.358;
atm_Po=1.5E5;
atm_cp=1044.0;
atm_kappa=0.27778;
zlow=0;
ztop=250.0e3; % set maximum altitude to 250km
rsphere=2575.0E3;

% define some constants for calculating the solar angle l_s
p2si=15.95;
equinox_fraction= 0.2098;
planet_year=686.0;
zero_date=542.06;
eccentricity=0.056;
semimajoraxis= 9.58256

% calculate the solar longitude.
SMALL_VALUE=1.0e-6;
deleqn = equinox_fraction * planet_year;
qq = 2.0 * (pi * deleqn / planet_year);
er = sqrt( (1.0+eccentricity)/(1.0-eccentricity) );
%  determine true anomaly at equinox:  eq
%  Iteration for eq
   e = 1.0;
   cd0 = 1.0;
   while (cd0 > SMALL_VALUE)
      ep = e - (e-eccentricity*sin(e)-qq) / (1.0-eccentricity*cos(e));
      cd0 = abs(e-ep);
      e = ep;
   end;
   eq = 2.0 * atan( er * tan(0.5*e) );


allzero='0000000000';
n2s=num2str(nit);
%replace string with different data
for i=1:length(n2s)
    allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
end;

time=allzero

theta=rdmds(sprintf('%s%s.%s',chdatapath,Tfile,time));
pe=rdmds(sprintf('%s%s.%s',chdatapath,pfile,time));
rac=rdmds([chdatapath,'RAC']);
pc=(pe(:,:,1:end-1)+pe(:,:,2:end))/2;
if (phys == 1)
 T=theta(:,:,1:end-1).*(pc/1.5e5).^0.27778;
else
 T=theta.*(pc/1.5e5).^0.27778;
end;
 dim=size(T); nr=dim(3);
 for k=1:nr
dm(:,:,k)=rac.*(pe(:,:,k)-pe(:,:,k+1));
Tm(:,:,k)=T(:,:,k).*dm(:,:,k);
pm(:,:,k)=pc(:,:,k).*rac;
end;
 T1d=sum(squeeze(sum(Tm,1)),1)./sum(squeeze(sum(dm,1)),1);
 p1d=sum(squeeze(sum(pm,1)),1)./sum(squeeze(sum(rac)));
 
if (phys == 1)
 plot(T1d,p1d,'-+k')
else
 plot(T1d,p1d,'-or')
end;
 
 set(gca,'ydir','reverse'); set(gca,'yscale','log');

day=nit*deltaT/86400;

% calculate ls  (solar longitude) -------
        julian=day/p2si;
        date_dbl = julian - zero_date;
        while (date_dbl < 0.)
           date_dbl=date_dbl+planet_year;
        end;
        while (date_dbl > planet_year)
           date_dbl=date_dbl-planet_year;
        end;


%  determine true anomaly at current date:  w
%  Iteration for w
       em = 2.0 * pi * date_dbl/planet_year;
       e  = 1.0;
       cd0 = 1.0;
       while (cd0 > SMALL_VALUE)
       ep = e-(e-eccentricity*sin(e)-em) / (1.0-eccentricity*cos(e));
       cd0 = abs(e-ep);
       e = ep;
       end;
       w = 2.0 * atan( er * tan(0.5*e) );

%  Radius vector ( astronomical units:  AU )
       als= (w - eq)*180.0/pi;      %Aerocentric Longitude
       if (als < 0.0)
          als=als+360.0;
       end;
       ls = als;


