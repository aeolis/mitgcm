% calculate the height above the topography in the physics grid
% z height at the cnters of the layer (on top of the topography, absolute height)
% dz layer thickness between interfaces of layers (in atmosphere only)

function [zc,dz]=cal_height(theta, pe, topoZ, phys_para)

  Po=phys_para(1); 
  cp=phys_para(2);
  kappa=phys_para(3);
  gravity=phys_para(4); 

  ndims=size(pe); nx=ndims(1); ny=ndims(2); nz0=ndims(3); nz=nz0-1;

% calculate the pressure in the middle of layers
  pc=(pe(:,:,1:nz)+pe(:,:,2:nz+1))/2.0;

  for L=1:nz
      dz(:,:,L) = -cp/gravity* ...
                   (pe(:,:,L+1).^kappa - pe(:,:,L).^kappa) ...
                   /Po^kappa .* theta(:,:,L);
  end;

% calculate the altitudes on faces of the vertical grid
  for L=1:nz
      if (L >= 2)
         zc(:,:,L) = zc(:,:,L-1)-cp/gravity*  ...
                   (pe(:,:,L).^kappa - pc(:,:,L-1).^kappa) ...
                   /Po^kappa .* theta(:,:,L-1) ...
                   -cp/gravity*(pc(:,:,L).^kappa - pe(:,:,L).^kappa) ...
                   /Po^kappa .* theta(:,:,L);
      else
         zc(:,:,L) = topoZ(:,:)-cp/gravity*  ...
                   (pc(:,:,L).^kappa - pe(:,:,L).^kappa) ...
                   /Po^kappa .* theta(:,:,L);
      end;
  end;


% dummy altitude
% zc(:,:,nz+1) = zc(:,:,nz)-cp/gravity*  ...
%               (pe(:,:,nz+1).^kappa - pc(:,:,nz).^kappa) ...
%               /Po.^kappa .* theta(:,:,nz);


% calculate the altitudes on faces of the vertical grid
%  zf(:,:,1)=topoZ;
%  for L=1:nz
%      zf(:,:,L+1)=zf(:,:,L)+dz(:,:,L);
%  end;


