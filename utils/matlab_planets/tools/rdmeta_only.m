function [N] = rdmeta_only(fname,varargin)
%
% Read MITgcmUV Meta/Data files
%
% A = RDMDS(FNAME) reads data described by meta/data file format.
% FNAME is a string containing the "head" of the file names.
%
% eg. To load the meta-data files
%     T.0000002880.000.000.meta, T.0000002880.000.000.data
%     T.0000002880.001.000.meta, T.0000002880.001.000.data
%     T.0000002880.002.000.meta, T.0000002880.002.000.data
%     T.0000002880.003.000.meta, T.0000002880.003.000.data
% use
%    >> A=rdmds('T.0000002880');
%

% Match name of all meta-files
allfiles=strcat(fname,'.meta');

% Beginning and end of strings
Iend=findstr(allfiles,'.meta')+4;
Ibeg=[1 Iend(1:end-1)+2];

% Loop through allfiles
for j=1:prod(size(Ibeg)),

% Read meta-file
[N] = localrdmeta(allfiles(Ibeg(j):Iend(j)));

bdims=N(1,:);
r0=N(2,:);
rN=N(3,:);
ndims=prod(size(bdims));

end

%-------------------------------------------------------------------------------

function [N] = localrdmeta(fname)

mname=strrep(fname,' ','');

% Read and interpret Meta file
fid = fopen(mname,'r');
if (fid == -1)
 error(['File' mname ' could not be opened'])
end

% Scan each line of the Meta file
allstr=' ';
keepgoing = 1;
while keepgoing > 0,
 line = fgetl(fid);
 if (line == -1)
  keepgoing=-1;
 else
% Strip out "(PID.TID *.*)" by finding first ")"
%old  ind=findstr([line ')'],')'); line=line(ind(1)+1:end);
  ind=findstr(line,')');
  if size(ind) ~= 0
    line=line(ind(1)+1:end);
  end
% Remove comments of form //
  line=[line ' //']; ind=findstr(line,'//'); line=line(1:ind(1)-1);
% Add to total string
  allstr=[allstr line];
 end
end

% Close meta file
fclose(fid);

% Strip out comments of form /* ... */
ind1=findstr(allstr,'/*'); ind2=findstr(allstr,'*/');
if size(ind1) ~= size(ind2)
 error('The /* ... */ comments are not properly paired')
end
while size(ind1,2) > 0
 allstr=[allstr(1:ind1(1)-1) allstr(ind2(1)+3:end)];
 ind1=findstr(allstr,'/*'); ind2=findstr(allstr,'*/');
end

% This is a kludge to catch whether the meta-file is of the
% old or new type. nrecords does not exist in the old type.
nrecords = -987;

% Everything in lower case
allstr=lower(allstr);

% Fix the unfortunate choice of 'format'
allstr=strrep(allstr,'format','dataprec');

% Evaluate meta information
eval(allstr);

N=reshape( dimlist , 3 , prod(size(dimlist))/3 );

