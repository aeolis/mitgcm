press=input('set three pressure levels [n1 n2 n3]: ');
field1=input('set the field to plot in first column: ');
field2=input('set the field to plot in second column: ');
htitle1=input('set colorbar title for first column: ','s');
htitle2=input('set colorbar title for second column: ','s');

n1=press(1); n2=press(2); n3=press(3);

figure;

g1=subplot(3,2,1); set(g1,'fontsize',12); 
merccube(xg,yg,field1(:,:,n1)); shading interp; c1=colorbar; set(gca,'tickdir','out');
set(g1,'position',[0.09    0.7    0.36    0.23]);
set(c1,'position',[0.47    0.7    0.015    0.23]); set(get(c1,'title'),'string',htitle1);

g2=subplot(3,2,2); set(g2,'fontsize',12); 
merccube(xg,yg,field2(:,:,n1)); shading interp; c2=colorbar; set(gca,'tickdir','out');
set(g2,'position',[0.55    0.7    0.36    0.23]);
set(c2,'position',[0.93    0.7    0.015    0.23]); set(get(c2,'title'),'string',htitle2);

g3=subplot(3,2,3); set(g3,'fontsize',12); 
merccube(xg,yg,field1(:,:,n2)); shading interp; c3=colorbar; set(gca,'tickdir','out');
set(g3,'position',[0.09    0.4    0.36    0.23]);
set(c3,'position',[0.47    0.4    0.015    0.23]); set(get(c3,'title'),'string',htitle1);

g4=subplot(3,2,4); set(g4,'fontsize',12);
merccube(xg,yg,field2(:,:,n2)); shading interp; c4=colorbar; set(gca,'tickdir','out');
set(g4,'position',[0.55    0.4    0.36    0.23]);
set(c4,'position',[0.93    0.4    0.015    0.23]); set(get(c4,'title'),'string',htitle2);

g5=subplot(3,2,5); set(g5,'fontsize',12); 
merccube(xg,yg,field1(:,:,n3)); shading interp; c5=colorbar; set(gca,'tickdir','out');
set(g5,'position',[0.09    0.1    0.36    0.23]);
set(c5,'position',[0.47    0.1    0.015    0.23]); set(get(c5,'title'),'string',htitle1);

g6=subplot(3,2,6); set(g6,'fontsize',12);
merccube(xg,yg,field2(:,:,n3)); shading interp; c6=colorbar; set(gca,'tickdir','out');
set(g6,'position',[0.55    0.1    0.36    0.23]);
set(c6,'position',[0.93    0.1    0.015    0.23]); set(get(c6,'title'),'string',htitle2);
