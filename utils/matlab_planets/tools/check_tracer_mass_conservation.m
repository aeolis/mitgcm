
clear all;

% section 1: load grid files and data configurations
chdatapath=input('full path to load data /~/ : ','s');
ny_latlon=input('grid points in y in latlon grid: ' );
deltaT=input('time step: ');
tstart=input('starting time step: ');
tend=input('ending time steps: ');
dnout=input('Output interval in number of time steps: ');

rac=rdmds([chdatapath,'RAC']);
dxg=rdmds([chdatapath,'DXG']);
dyg=rdmds([chdatapath,'DYG']);
xg=rdmds([chdatapath,'XG']);
yg=rdmds([chdatapath,'YG']);
xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);
hfacc=rdmds([chdatapath,'hFacC']);
drF=rdmds([chdatapath,'DRF']); drF=squeeze(drF);
topoP=rdmds([chdatapath,'topo_P']);

g2d(:,:,1)=xc; g2d(:,:,2)=yc; g2d(:,:,3)=xg; g2d(:,:,4)=yg;
g2d(:,:,5)=dxg; 
g2d(:,:,6)=dyg; 
g2d(:,:,7)=rac;

[AngleCS,AngleSN] = cubeCalcAngle(yg,rac,dxg,dyg);


for k=1:(tend-tstart)/dnout+1
    nstep(k)=tstart+dnout*(k-1);
end;
alldata=nstep;

% Mars physics parameters, including gravity, atmospheric properties and topography
grv=22.88;
atm_Po=1.0E5;
atm_cp=13000.;
atm_kappa=0.29;
rsphere=71492.E3;
drsphere=rsphere*(1.0/ny_latlon)*pi;

phys_para(1)=atm_Po;
phys_para(2)=atm_cp;
phys_para(3)=atm_kappa;
phys_para(4)=grv;

% dynamics grid
ustr='U';
vstr='V';
ptr1str='PTRACER01'; % Vapor
ptr2str='PTRACER02'; % liquid
thetastr='T';       % potential temperature
pedstr='pedyn';     % pressure field (co2+N2)
Etastr='Eta';       % surface pressure anomaly

rC=rdmds([chdatapath,'RC']);
rC=squeeze(rC); 

dims=size(topoP);
nx=dims(1); ny=dims(2); nr=length(rC); 


% section 2: main loop to process data, including zonal mean 
%            and time evolution

for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter));
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        time=allzero
        timed=alldata(niter);
        day(niter)=timed*deltaT/86400; % this is earth day, nomalized to sol later
% dynamics grid variables
        ufile=sprintf('%s%s.%s',chdatapath,ustr,time);
        vfile=sprintf('%s%s.%s',chdatapath,vstr,time);
        ptr1file=sprintf('%s%s.%s',chdatapath,ptr1str,time);
        ptr2file=sprintf('%s%s.%s',chdatapath,ptr2str,time);
        pedynfile=sprintf('%s%s.%s',chdatapath,pedstr,time);
        Etafile=sprintf('%s%s.%s',chdatapath,Etastr,time);
        thetafile=sprintf('%s%s.%s',chdatapath,thetastr,time);

        ptr1=rdmds(ptr1file);
        ptr2=rdmds(ptr2file); 
        pedyn=rdmds(pedynfile);
        u3d=rdmds(ufile);
        v3d=rdmds(vfile);
        Eta=rdmds(Etafile);
        theta=rdmds(thetafile);

% calculate the total air mass in kilograms--
        airmass=(topoP+Eta).*rac/grv;

        totair(niter)=sum(sum(airmass));

% construct dynamics grid
        dpdyn(:,:,1:nr)=pedyn(:,:,1:nr)-pedyn(:,:,2:nr+1);
        pcdyn(:,:,1:nr)=(pedyn(:,:,1:nr)+pedyn(:,:,2:nr+1))/2.0;

% calculate the global mean Ar/N2 mass density (kg/m^2)
        ptr1mass(niter)=sum(sum(sum(ptr1.*dpdyn/grv,3).*rac));
        ptr2mass(niter)=sum(sum(sum(ptr2.*dpdyn/grv,3).*rac));

        totmass(niter)=sum(sum(sum(dpdyn/grv,3).*rac));
end;
