clear all;

% section 1: load grid files and data configurations
chdatapath=input('full path to load data /~/ : ','s');
ny_latlon=input('grid points in y in latlon grid: ' );
deltaT=input('time step: ');
tstart=input('starting time step: ');
tend=input('ending time steps: ');
dnout=input('Output interval in number of time steps: ');

rac=rdmds([chdatapath,'RAC']);
dxg=rdmds([chdatapath,'DXG']);
dyg=rdmds([chdatapath,'DYG']);
xg=rdmds([chdatapath,'XG']);
yg=rdmds([chdatapath,'YG']);
xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);
hFacC=rdmds([chdatapath,'hFacC']);
rc =rdmds([chdatapath,'RC']);  rc =squeeze(rc);
drF=rdmds([chdatapath,'DRF']); drF=squeeze(drF);

[AngleCS,AngleSN] = cubeCalcAngle(yg,rac,dxg,dyg);

% define diagnostics files
dyn3d_str ='dynDiag';
winds_str ='dynDiagMass';
tra3d_str ='tracerDiagMass';
pedyn_str ='pedyn'; 

for k=1:(tend-tstart)/dnout+1
    nstep(k)=tstart+dnout*(k-1);
end;
alldata=nstep;

dims=size(hFacC);
nx=dims(1); ny=dims(2); nr=dims(3);

% dynamics grid part
ksurf(1:nx,1:ny)=1;

for i=1:nx; for j=1:ny
    kindex(1:nr)=nr+1;
    for k=1:nr-1
       if (hFacC(i,j,k)>0); kindex(k)=k; end;
    end;
    ksurf(i,j)=min(kindex);
end; end;

for i=1:nx; for j=1:ny; for k=1:nr
       if (k<ksurf(i,j))
          mask(i,j,k)=NaN;
       else
          mask(i,j,k)=1;
       end;
end; end; end;

grv=3.727;
atm_Po=610.0;
atm_cp=767.35;
atm_kappa=0.25;
zlow=-6.5470e3; % 2.5 degree res only
ztop=80.0e3; % set maximum altitude to 80km
rsphere=3389.9E3;
drsphere=rsphere*(1.0/ny_latlon)*pi;

% define some constants for calculating the solar angle l_s
p2si=1.0274912;
equinox_fraction= 0.2695;
planet_year=669.0;
zero_date=488.7045;
eccentricity=0.09341233;
semimajoraxis= 1.52366231;
SMALL_VALUE=1.0e-6;
deleqn = equinox_fraction * planet_year;
qq = 2.0 * (pi * deleqn / planet_year);
er = sqrt( (1.0+eccentricity)/(1.0-eccentricity) );
%  determine true anomaly at equinox:  eq
%  Iteration for eq
   e = 1.0;
   cd0 = 1.0;
   while (cd0 > SMALL_VALUE)
      ep = e - (e-eccentricity*sin(e)-qq) / (1.0-eccentricity*cos(e));
      cd0 = abs(e-ep);
      e = ep;
   end;
   eq = 2.0 * atan( er * tan(0.5*e) );

for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter));
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        time=allzero
        timed=alldata(niter);
        day(niter)=timed*deltaT/86400; % this is earth day, nomalized to sol later
% dynamics grid variables
        dyn3d_file=sprintf('%s%s.%s',chdatapath,dyn3d_str,time);
        winds_file=sprintf('%s%s.%s',chdatapath,winds_str,time);
        tra3d_file=sprintf('%s%s.%s',chdatapath,tra3d_str,time);
        pedyn_file=sprintf('%s%s.%s',chdatapath,pedyn_str,time);

        dyn3d = rdmds(dyn3d_file);
        winds = rdmds(winds_file);
        tra3d = rdmds(tra3d_file);
        pedyn = rdmds(pedyn_file);

        dpdyn = pedyn(:,:,1:end-1)-pedyn(:,:,2:end);
        pcdyn = (pedyn(:,:,1:end-1)+pedyn(:,:,2:end))/2.0;
        for i=1:nx; for j=1:ny
          for k=1:nr
              if (dpdyn(i,j,k)<0)
                  dpdyn(i,j,k)=0;
              end;
          end;
%          pcdyn(i,j,1:ksurf(i,j))=0.0; %NaN;
        end; end;

%        u3d=dyn3d(:,:,:,1);
%        v3d=dyn3d(:,:,:,2);
        u3d=winds(:,:,:,1);
        v3d=winds(:,:,:,2);
        w3d=winds(:,:,:,3);
        t3d=dyn3d(:,:,:,4);
        s3d=dyn3d(:,:,:,5);

        ut3d=tra3d(:,:,:,1);
        vt3d=tra3d(:,:,:,2);
        wt3d=tra3d(:,:,:,3);
        us3d=tra3d(:,:,:,4);
        vs3d=tra3d(:,:,:,5);
        ws3d=tra3d(:,:,:,6);

        [uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C');
        [usE,vsN] = rotate_uv2uvEN(us3d,vs3d,AngleCS,AngleSN,'C');
        [utE,vtN] = rotate_uv2uvEN(ut3d,vt3d,AngleCS,AngleSN,'C');

        [u2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(uE, ny_latlon,yc,rac,hFacC);
        [v2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(vN, ny_latlon,yc,rac,hFacC);
        [w2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(w3d, ny_latlon,yc,rac,hFacC);
        [s2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(s3d,ny_latlon,yc,rac,hFacC);
        [t2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(t3d,ny_latlon,yc,rac,hFacC);
        [dp2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(dpdyn,ny_latlon,yc,rac,hFacC);
        [pc2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(pcdyn,ny_latlon,yc,rac,hFacC);

        [vs2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(vsN,ny_latlon,yc,rac,hFacC);
        [vt2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(vtN,ny_latlon,yc,rac,hFacC);
        [ws2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(ws3d,ny_latlon,yc,rac,hFacC);
        [wt2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(wt3d,ny_latlon,yc,rac,hFacC);

        vtedd=(vt2d-v2d.*t2d);
        vsedd=(vs2d-v2d.*s2d);

        wtedd=(wt2d-w2d.*t2d);
        wsedd=(ws2d-w2d.*s2d);

        for k=1:length(rc) 
            % eddy transport (transient+stationary)
            vtedd(:,k)=vtedd(:,k)*(rc(k)/atm_Po)^atm_kappa;
            vsedd(:,k)=vsedd(:,k).*drF(k)/grv; %dp2d(:,k)/grv;
            wtedd(:,k)=wtedd(:,k)*(rc(k)/atm_Po)^atm_kappa;
            wsedd(:,k)=wsedd(:,k).*drF(k)/grv;
            % total transport
            vttot(:,k)=vt2d (:,k)*(rc(k)/atm_Po)^atm_kappa;
            vstot(:,k)=vs2d (:,k).*drF(k)/grv; %dp2d(:,k)/grv;
            wttot(:,k)=wt2d (:,k)*(rc(k)/atm_Po)^atm_kappa;
            wstot(:,k)=ws2d (:,k).*drF(k)/grv; 
            % advective transport
            vtadv(:,k)=v2d(:,k).*t2d(:,k)*(rc(k)/atm_Po)^atm_kappa;
            vsadv(:,k)=v2d(:,k).*s2d(:,k).*drF(k)/grv; %dp2d(:,k)/grv;
            wtadv(:,k)=w2d(:,k).*t2d(:,k)*(rc(k)/atm_Po)^atm_kappa;
            wsadv(:,k)=w2d(:,k).*s2d(:,k).*drF(k)/grv;
        end;

        vsedd0=vsedd;
        vstot0=vstot;
        vsadv0=vsadv;
        for j=1:ny_latlon; for k=1:length(rc);
            if(isnan(vsedd0(j,k))); vsedd0(j,k)=0; end;
            if(isnan(vstot0(j,k))); vstot0(j,k)=0; end;
            if(isnan(vsadv0(j,k))); vsadv0(j,k)=0; end;
        end; end;

        % move to permanent storage
        vsavge(:,niter)=sum(vsedd0,2);
        vsavgt(:,niter)=sum(vstot0,2);
        vsavga(:,niter)=sum(vsadv0,2);

        vtmavgt(:,:,niter)=vttot;
        vtmavga(:,:,niter)=vtadv;
        vtmavge(:,:,niter)=vtedd;
        
        vsmavgt(:,:,niter)=vstot;
        vsmavga(:,:,niter)=vsadv;
        vsmavge(:,:,niter)=vsedd;

        wtmavgt(:,:,niter)=wttot;
        wtmavga(:,:,niter)=wtadv;
        wtmavge(:,:,niter)=wtedd;

        wsmavgt(:,:,niter)=wstot;
        wsmavga(:,:,niter)=wsadv;
        wsmavge(:,:,niter)=wsedd;
        
        pc2dt(:,:,niter)=pc2d;  
 
        % calculate ls  (solar longitude) -------
        julian=day(niter)/p2si;
        date_dbl = julian - zero_date;
        while (date_dbl < 0.)
           date_dbl=date_dbl+planet_year;
        end;
        while (date_dbl > planet_year)
           date_dbl=date_dbl-planet_year;
        end;

        %  determine true anomaly at current date:  w
        %  Iteration for w
        em = 2.0 * pi * date_dbl/planet_year;
        e  = 1.0;
        cd0 = 1.0;
        while (cd0 > SMALL_VALUE)
        ep = e-(e-eccentricity*sin(e)-em) / (1.0-eccentricity*cos(e));
        cd0 = abs(e-ep);
        e = ep;
        end;
        w = 2.0 * atan( er * tan(0.5*e) );

        %  Radius vector ( astronomical units:  AU )
        als= (w - eq)*180.0/pi;      %Aerocentric Longitude
        if (als < 0.0)
           als=als+360.0;
        end;
        ls(niter) = als;

end;

for k=1:nr; yc2d(:,k)=ylat; end;
