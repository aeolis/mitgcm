function [xc,yc,xg,yg,rac]=MITgrid(nx,chdatapath);

sz=[nx nx 6];
prm=[1 2 3];

path(chdatapath,path);

xc=permute( rdda('LONC.bin',sz,1,'real*8','b') ,prm);
yc=permute( rdda('LATC.bin',sz,1,'real*8','b') ,prm);
dxf=permute( rdda('DXF.bin',sz,1,'real*8','b') ,prm);
dyf=permute( rdda('DYF.bin',sz,1,'real*8','b') ,prm);
rac=permute( rdda('RA.bin',sz,1,'real*8','b') ,prm);

xg=permute( rdda('LONG.bin',sz,1,'real*8','b') ,prm); xg(end+1,end+1,:)=NaN;
yg=permute( rdda('LATG.bin',sz,1,'real*8','b') ,prm); yg(end+1,end+1,:)=NaN;
dxv=permute( rdda('DXV.bin',sz,1,'real*8','b') ,prm); dxv(end+1,end+1,:)=NaN;
dyu=permute( rdda('DYU.bin',sz,1,'real*8','b') ,prm); dyu(end+1,end+1,:)=NaN;
raz=permute( rdda('RAZ.bin',sz,1,'real*8','b') ,prm); raz(end+1,end+1,:)=NaN;

xg(1,end,[1 3 5])=xg(1,1,1);
xg(end,1,[2 4 6])=xg(1,1,4);
xg(end,:,[1 3 5])=xg(1,:,[2 4 6]);
xg(:,end,[1 3 5])=xg(1,end:-1:1,[3 5 1]);
xg(end,:,[2 4 6])=xg(end:-1:1,1,[4 6 2]);
xg(:,end,[2 4 6])=xg(:,1,[3 5 1]);

yg(1,end,[1 3 5])=yg(1,1,3);
yg(end,1,[2 4 6])=yg(1,1,6);
yg(end,:,[1 3 5])=yg(1,:,[2 4 6]);
yg(:,end,[1 3 5])=yg(1,end:-1:1,[3 5 1]);
yg(end,:,[2 4 6])=yg(end:-1:1,1,[4 6 2]);
yg(:,end,[2 4 6])=yg(:,1,[3 5 1]);

raz(1,end,[1 3 5])=raz(1,1,1);
raz(end,1,[2 4 6])=raz(1,1,4);
raz(end,:,[1 3 5])=raz(1,:,[2 4 6]);
raz(:,end,[1 3 5])=raz(1,end:-1:1,[3 5 1]);
raz(end,:,[2 4 6])=raz(end:-1:1,1,[4 6 2]);
raz(:,end,[2 4 6])=raz(:,1,[3 5 1]);

dxv(1,end,[1 3 5])=dxv(1,1,1);
dxv(end,1,[2 4 6])=dxv(1,1,4);
dyu(1,end,[1 3 5])=dxv(1,1,1);
dyu(end,1,[2 4 6])=dxv(1,1,4);
dxv(end,:,[1 3 5])=dxv(1,:,[2 4 6]);
dxv(:,end,[1 3 5])=dyu(1,end:-1:1,[3 5 1]);
dxv(end,:,[2 4 6])=dyu(end:-1:1,1,[4 6 2]);
dxv(:,end,[2 4 6])=dxv(:,1,[3 5 1]);
dyu(end,:,[1 3 5])=dyu(1,:,[2 4 6]);
dyu(:,end,[1 3 5])=dxv(1,end:-1:1,[3 5 1]);
dyu(end,:,[2 4 6])=dxv(end:-1:1,1,[4 6 2]);
dyu(:,end,[2 4 6])=dyu(:,1,[3 5 1]);

dxc=permute( rdda('DXC.bin',sz,1,'real*8','b') ,prm); dxc(end+1,:,:)=NaN;
dyc=permute( rdda('DYC.bin',sz,1,'real*8','b') ,prm); dyc(:,end+1,:,:)=NaN;
raw=permute( rdda('RAW.bin',sz,1,'real*8','b') ,prm); raw(end+1,:,:)=NaN;
ras=permute( rdda('RAS.bin',sz,1,'real*8','b') ,prm); ras(:,end+1,:,:)=NaN;
dxg=permute( rdda('DXG.bin',sz,1,'real*8','b') ,prm); dxg(:,end+1,:,:)=NaN;
dyg=permute( rdda('DYG.bin',sz,1,'real*8','b') ,prm); dyg(end+1,:,:)=NaN;

dxc(end,:,[1 3 5])=dxc(1,:,[2 4 6]);
dxc(end,:,[2 4 6])=dyc(end:-1:1,1,[4 6 2]);
dyc(:,end,[2 4 6])=dyc(:,1,[3 5 1]);
dyc(:,end,[1 3 5])=dxc(1,end:-1:1,[3 5 1]);

raw(end,:,[1 3 5])=raw(1,:,[2 4 6]);
raw(end,:,[2 4 6])=ras(end:-1:1,1,[4 6 2]);
ras(:,end,[2 4 6])=ras(:,1,[3 5 1]);
ras(:,end,[1 3 5])=raw(1,end:-1:1,[3 5 1]);

dyg(end,:,[1 3 5])=dyg(1,:,[2 4 6]);
dyg(end,:,[2 4 6])=dxg(end:-1:1,1,[4 6 2]);
dxg(:,end,[2 4 6])=dxg(:,1,[3 5 1]);
dxg(:,end,[1 3 5])=dyg(1,end:-1:1,[3 5 1]);

% Pad arrays to be same size
xc(end+1,end+1,:)=0;
yc(end+1,end+1,:)=0;
dxf(end+1,end+1,:)=0;
dyf(end+1,end+1,:)=0;
rac(end+1,end+1,:)=0;
dxc(end,end+1,:)=0;
dyc(end+1,end,:)=0;
raw(end,end+1,:)=0;
ras(end+1,end,:)=0;
dyg(end,end+1,:)=0;
dxg(end+1,end,:)=0;

