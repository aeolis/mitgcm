%without co2 cycle, the model works 

clear all;

cs_path=input('enter the path to cs grid files /~/ : ','s');

ny_orig=720; %was 360
nx_orig=ny_orig*2;

deg_size=input('resolution in latlon 5, 2.5, 1.25 or 0.625 : ');

nc=180/deg_size/2;
[lonC0, latC0, lonG, latG,rac]=MITgrid(nc,cs_path);
lonC=lonC0(1:nc,1:nc,:);
latC=latC0(1:nc,1:nc,:);
xg=reshape(permute(lonG(1:nc,1:nc,:),[1 3 2]), [nc*6 nc]);
yg=reshape(permute(latG(1:nc,1:nc,:),[1 3 2]), [nc*6 nc]);

fid=fopen('~/matlab/tools/mars_surface_data/ieg0031t.dat');
topo=fread(fid,[11520 5760],'int16','b');
topoe=topo(1:8:11520,1:8:5760);
clear topo;
fclose(fid);

if (size(topoe,2)~=ny_orig) 
   error('check original resolution of data');
end;

filter_opt=1;

topo_sp(2:1441,2:721)=topoe;
topo_sp(1,2:721)=topoe(1440,:);
topo_sp(1442,2:721)=topoe(2,:);

topo_sp(:,1:3)=sum(topo_sp(:,2))/1442;
topo_sp(:,720:722)=sum(topo_sp(:,721))/1442;

% or load topo_mid.dat;

nc=180/deg_size/2;

if (filter_opt==1)
%use image filter to make smooth topography first
 h=fspecial('disk',40);
 topo_sp = imfilter(topo_sp,h,'replicate');
end;

dx=180/720;
x=[-180-dx/2:dx:180+dx/2];
y=[-90-dx/2:dx:90+dx/2];

[cy,cx]=meshgrid(y,x);
topoe=interp2(y,x,topo_sp,cy,cx);


% original map is 1440x720. dx=180/720
dx=180/720;
xc=[-180-dx/2:dx:180+dx/2];
yc=[-90-dx/2:dx:90+dx/2];


x6s=lonC; %permute(reshape(xcs,[nc 6 nc]),[1 3 2]);
y6s=latC; %permute(reshape(ycs,[nc 6 nc]),[1 3 2]);

topo_cs=zeros(nc,nc,6);

for ntiles=1:6
 topo_cs(:,:,ntiles)=interp2(yc,xc,topoe,y6s(:,:,ntiles),x6s(:,:,ntiles));
end;

topo_cs=permute(topo_cs,[1 3 2]);
topo_cs=reshape(topo_cs,[nc*6 nc]);
topo_low=min(min(topo_cs));
topo_cs=(topo_cs-min(min(topo_cs)));

fid=fopen('topo.bin','w');
fwrite(fid,topo_cs,'real*8','b');
fclose(fid);

%viking lander 1 site: 48.222 west and 22.697 north
xc_cs=reshape(permute(lonC,[1 3 2]), [nc*6 nc]); %rdmds('XC');
yc_cs=reshape(permute(latC,[1 3 2]), [nc*6 nc]); %rdmds('YC');
xy_vl1=abs(xc_cs+48.222)+abs(yc_cs-22.697);
[nxcs_vl1,nycs_vl1]=find(xy_vl1==min(min(xy_vl1)));

xy_vl2=abs(xc_cs+225.99)+abs(yc_cs-48.269);
[nxcs_vl2,nycs_vl2]=find(xy_vl2==min(min(xy_vl2)));

%use hydrostatic atmosphere for calculation of 
%reference bottom pressure (T=const)

rgas=767.35*0.25;
T1=185;  % at 26.68km, the temperature is actually 183.8 (just for simplicity)
grv=3.727;
hh=rgas*T1/grv;
p_vl1=900; %48.3*exp(26.68e3/hh);

% MOLA to Model scalar
zb_mola=mean(mean(topoe));
zb_model=mean(mean(topo_cs+topo_low));
deltaz=topo_cs(nxcs_vl1,nycs_vl1)+topo_low+(zb_mola-zb_model)-topoe(528,450);
model2mola=exp(deltaz/hh);

%pressure at VL1

%p_vl1=p_vl1/model2mola;
T_vl1=250.0; %250 %make temperature higher to prevent CO2 from depositing at spin-up
hh=rgas*T_vl1/grv;
ps=p_vl1*exp(topo_cs(nxcs_vl1,nycs_vl1)/hh); % lowest point for topo_cs is zero

% initialize reference temperature (theta_ref)

nlay=round(ps/40)-1+5;
% construct pressure

pf(1)=round(ps/40)*40;
dp(1:round(ps/40)-1)=40;
dp(round(ps/40)-1+1)=20;
dp(round(ps/40)-1+2)=10;
dp(round(ps/40)-1+3)=5;
dp(round(ps/40)-1+4)=2.5;
dp(round(ps/40)-1+5)=2.5;

for k=1:nlay
pf(k+1)=pf(k)-dp(k);
end;

pc=(pf(1:nlay)+pf(2:nlay+1))/2;

%test temperature profile and find potential temperature near 300k
theta_test=T_vl1*(610./pc).^0.25;
[C,I]=min(abs(theta_test-300));

if (theta_test(I)<=300)
   nlow=I;
else 
   nlow=I-1;
end

T_test=300*(pc/610).^0.25;
[C,I]=min(abs(T_test-140));

nmid=I;

theta(1:nlow)=T_vl1*(610./pc(1:nlow)).^0.25;
theta(nlow+1:nmid)=300;  
theta(nmid+1:nlay)=140*(610./pc(nmid+1:nlay)).^0.25;

clear topo;
