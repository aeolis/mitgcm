function [hr,ylatc]=calc_eddy_heat_cs(dyn3d,tr3d,g2d,drC,drF,rC,rF,kappa,hFacC,rsphere,varargin);

% the heating equation follows Karoly (1998)

% usage 1, without condensation of water vapor
%          [hr,ylatc] = calc_eddy_heat_cs(dyn3d,tr3d,g2d,drC,drF,rC,rF,kappa,hFacC,rsphere,varargin);
%          Example:
%                  [hr,ylatc] = calc_eddy_heat_cs(dyn3d,tr3d,g2d,drC,drF,rC,rF,kappa,hFacC,rsphere);

% usage 2, with condensation of water vapor
%          [hr,ylatc] = calc_eddy_heat_cs(dyn3d,tr3d,g2d,drC,drF,rC,rF,kappa,hFacC,rsphere,varargin);
%          Example:
%                  [hr,ylatc] = calc_eddy_heat_cs(dyn3d,tr3d,g2d,drC,drF,rC,rF,kappa,hFacC,rsphere,'w',tau,tauc,tRef);


% the following terms are time-averaged and zonal-mean quantities
% dhdt -- diabatic heating
% gamw -- vertical advection of heat
% vdtdy -- horizontal advection of heat
% dvtdy -- horizontal convergence of eddy heat heat flux
% dwtdp -- vertical convergence of eddy heat flux


% cwt -- vertical eddy heat flux (extra terms associated with dwtdp)
% tau -- rediative cooling time scale, in seconds
% tauc -- condensation time scale, in seconds

% ny --- number of grid points (lines) in latitude, required for zonal mean 

optargin=size(varargin,2);

if (optargin > 0)
   if (varargin{1} ~='w')
      error('!!!   Stop, check the number of arguments. See usage in calc_eddy_heat_cs.m ');
   end;
   tau=varargin{2};
   tauc=varargin{3};
   tRef=varargin{4};
end;

ny=256;

pref=1.0e5; %reference pressure is 1 bar.

u3d=squeeze(dyn3d(:,:,:,1));
v3d=squeeze(dyn3d(:,:,:,2));
w3d=squeeze(dyn3d(:,:,:,3));
t3d=squeeze(dyn3d(:,:,:,4));
s3d=squeeze(dyn3d(:,:,:,5));

ut3d=squeeze(tr3d(:,:,:,1));
vt3d=squeeze(tr3d(:,:,:,2));
wt3d=squeeze(tr3d(:,:,:,3));

xc=squeeze(g2d(:,:,1));
yc=squeeze(g2d(:,:,2));
xg=squeeze(g2d(:,:,3));
yg=squeeze(g2d(:,:,4));
dxc=squeeze(g2d(:,:,5));
dyc=squeeze(g2d(:,:,6));
dxg=squeeze(g2d(:,:,7));
dyg=squeeze(g2d(:,:,8));
rAc=squeeze(g2d(:,:,9));
rAz=squeeze(g2d(:,:,10));
rAs=squeeze(g2d(:,:,11));
rAw=squeeze(g2d(:,:,12));

[AngleCS,AngleSN] = cubeCalcAngle(yg,rAc,dxg,dyg);

check_var=input('caution: must use time average variables !!!, press 1 to contiue  ');

if (check_var ~= 1)
   error('Stop, use time averaged variables');
end;

dims=size(u3d); nx=dims(1); nc=dims(2); ncp=nc+1; n2p=nc+2; nPg=nx*nc ;
if nx == 6*nc, flag=1; else flag=0; end
if size(v3d) ~= dims, flag=0; end
if size(dxc) ~= dims(1:2), flag=0; end
if size(dyc) ~= dims(1:2), flag=0; end
if flag == 0,
 fprintf(' Error in size of input arrays\n');
 vort=0; return
end
if length(dims) == 2, nr=1; else nr=prod(dims(3:end)); end


rF(nr+1)=0.0;

u3d=reshape(u3d,[nx nc nr]);
v3d=reshape(v3d,[nx nc nr]);
w3d=reshape(w3d,[nx nc nr]);
t3d=reshape(t3d,[nx,nc,nr]);

ut3d=reshape(ut3d,[nx nc nr]);
vt3d=reshape(vt3d,[nx nc nr]);
wt3d=reshape(wt3d,[nx nc nr]);

% filling t3d for missing corners, split_Z_cub will make
% t3d in form of [ncp,ncp,nr,6]

t6t=split_Z_cub(t3d);
s6t=split_Z_cub(s3d);

if nr > 1,
  t6t=permute(t6t,[1 2 4 3]);
  s6t=permute(s6t,[1 2 4 3]);
end

if (optargin > 0)
 if (varargin{1}=='w')
   %---calculate diabatic heating (cell centerd)---d[h]/dt-----optional------------------------------------1

   %---use virtual potential temperature for diagnostics---optional--------------

   Rq=-0.8778;
   t6t=t6t.*(Rq*s6t+1.0);

   RWV = 461.0;
   es0 = 609.140;
   T0ref = 273.0;
   LHWV = 2.5e6;
   Cp   = 13000.0;

   realT=zeros(ncp,ncp,6,nr);
  
   for k=1:nr,
      realT(:,:,:,k)=(rC(k)/pref)^kappa*t6t(:,:,:,k);
   end;

   esT =es0*exp(-LHWV/RWV*(1.0./realT-1.0/T0ref));

   qs       = zeros(ncp,ncp,6,nr);
   delta    = zeros(ncp,ncp,6,nr);
   latentWV = zeros(ncp,ncp,6,nr);

   for k=1:nr, qs(:,:,:,k)=9*esT(:,:,:,k)/rC(k); end;

   for i=1:ncp, for j=1:ncp, for tn=1:6, for k=1:nr,
      if s6t(i,j,tn,k)>=qs(i,j,tn,k) delta(i,j,tn,k)=1; end;
   end; end; end; end;

   for k=1:nr,
      latentWV(:,:,:,k)=LHWV/Cp*delta(:,:,:,k).*(s6t(:,:,:,k)-qs(:,:,:,k))/tauc*(pref/rC(k))^kappa;
   end;

   dhdt=zeros(ncp,ncp,6,nr);

   for k=1:nr,
       dhdt(:,:,:,k)=( -(t6t(:,:,:,k)-tRef(k))/tau+latentWV(:,:,:,k) )*(rC(k)/pref)^kappa;
   end;

   dhdt=permute(dhdt([1:nc],[1:nc],[1:6],[1:nr]),[1 3 2 4]);

   dhdt=reshape(dhdt,6*nc,nc,nr);

   [dhdt,mskzon,ylat,areazon] = calcZonalAvgCubeC(dhdt,ny,yc,rAc,hFacC);

 end;
end;
%---calculate vertical advection of heat (cell centered)---gamma*[w]-------------------------------2

Gamma=zeros(ny,nr);
gamma_w_F=zeros(ny,nr);

t3d_dp=zeros(6*nc,nc,nr);

for k=1:nr,
 km1 = max(k-1,1);
 t3d_dp(:,:,k)=(t3d(:,:,k)-t3d(:,:,km1))/drC(k);
end;

% -- t2d at the center of the cell
[t2d_dp,mskzon,ylat,areazon] = calcZonalAvgCubeC(t3d_dp,ny,yc,rAc,hFacC);

ylatc=ylat; % ylatc is to be returned as a result.

for k=1:nr,
 km1 = max(k-1,1);
 Gamma(:,k)=-(rF(k)/pref)^kappa*t2d_dp(:,k);
end;

[w2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(w3d,ny,yc,rAc,hFacC);
gamma_w_F=Gamma.* w2d;
gamma_w_F(:,nr+1)=0.0;

% move to cell center (in vertical)

gammaw=zeros(ny,nr);
gammaw(:,[1:nr])=( gamma_w_F(:,[1:nr])+gamma_w_F(:,[2:nr+1]) )/2.0;

%---calculate horizontal advection of eddy heat (cell centered)---[v]*d[T]/dy------------------------3

vdtdy=zeros(ny,nr);

[uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C');
% -- v2d at the center of the cell
[v2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(vN,ny,yc,rAc,hFacC);

% -- t2d at the north/south edges of the cell
[t2d,mskzon,ylat,areazon] = calcZonalAvgCubeG(t3d,ny,yc,rAc,hFacC);

dyc_latlon(1:ny)=pi/ny*rsphere;

for k=1:nr,
 vdtdy([2:ny],k)=v2d([2:ny],k).* (t2d([2:ny],k)-t2d([1:ny-1],k))./dyc_latlon(2:ny)'*(rC(k)/pref)^kappa;
end;

% filling vdtdy(1,:) and vdtdy(ny,:) with NaNs

vdtdy(1,:)=NaN; vdtdy(ny,:)=NaN;

%---calculate horizontal eddy heat flux d/dy([v'T']+[v*T*])---use Physics of climate ---[v'T']+[v*T*]=[vT]-[v][T]----------4

[utE,vtN] = rotate_uv2uvEN(ut3d,vt3d,AngleCS,AngleSN,'C');


% -- vt2d at the north/south edges of the cell
[vt2d,mskzon,ylat,areazon] = calcZonalAvgCubeG(vtN,ny,yc,rAc,hFacC);

% -- v2d at the north/south edges of the cell
[v2d,mskzon,ylat,areazon] = calcZonalAvgCubeG(vN,ny,yc,rAc,hFacC);

vt_star=vt2d-v2d.*t2d;

% d/dy([v'T']+[v*T*]) (cell centered)

dvtdy=zeros(ny,nr);

for k=1:nr,
 dvtdy([2:ny],k)=(vt_star([2:ny],k)-vt_star([1:ny-1],k))./dyc_latlon(2:ny)'*(rC(k)/pref)^kappa;;
end;

% filling dvtdy(1,:) and dvtdy(ny,:) with NaNs

dvtdy(1,:)=NaN; dvtdy(ny,:)=NaN;

%---calculate horizontal eddy heat flux d/dp([w'T']+[w*T*])---use Physics of climate ---[w'T']+[w*T*]=[wT]-[w][T]----------5

% -- wt2d w2d at the center of the cell (face of the vertical grid)
[wt2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(wt3d,ny,yc,rAc,hFacC);
[w2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(w3d,ny,yc,rAc,hFacC);

% -- t2d at the center of the cell (center of the vertical grid)

t3d_w=zeros(6*nc,nc,nr);

for k=1:nr,
 km1 = max(k-1,1);
 t3d_w(:,:,k)=(t3d(:,:,k)+t3d(:,:,km1))/2.0;
end;

[t2d_w,mskzon,ylat,areazon] = calcZonalAvgCubeC(t3d_w,ny,yc,rAc,hFacC);

wt_star=wt2d-w2d.*t2d_w;

% d/dp([w'T']+[w*T*]) (cell centered)

wt_star(:,nr+1)=0.0;
dwtdp=zeros(ny,nr);

for k=1:nr,
 dwtdp(:,k)=( wt_star(:,k+1)*(rF(k+1)/pref)^kappa-wt_star(:,k)*(rF(k)/pref)^kappa )/drF(k);
end;

%---calculate kappa/p*([w'T']+[w*T*])-----------------------------------------------------------------------------6

cwt=zeros(ny,nr);

for k=1:nr,
 cwt(:,k)=kappa/rC(k)*( wt_star(:,k+1)*(rF(k+1)/pref)^kappa+wt_star(:,k)*(rF(k)/pref)^kappa )/2.0;
end;

%---calculate the total heating rate

if (optargin > 0)
   hr=zeros(ny,nr,6);
   hr(:,:,1)=dhdt;
   hr(:,:,2)=gammaw;
   hr(:,:,3)=-vdtdy;
   hr(:,:,4)=-dvtdy;
   hr(:,:,5)=-dwtdp;
   hr(:,:,6)=cwt;

   fprintf('++++++++++++++++++++ \n');

   fprintf('terms in hr [ %d x %d x (%d terms) ]:  \n',ny,nr,6);
   fprintf('term 1 : diabatic heating \n');
   fprintf('term 2 : vertical heat advection \n');
   fprintf('term 3 : horizontal heat advection \n');
   fprintf('term 4 : meridional convergence of horizontal eddy heat flux \n');
   fprintf('term 5 : vertical convergence of vertical eddy heat flux \n');
   fprintf('term 6 : extra vertical eddy heat flux \n');
   

else
   hr=zeros(ny,nr,5);
   hr(:,:,1)=gammaw;
   hr(:,:,2)=-vdtdy;
   hr(:,:,3)=-dvtdy;
   hr(:,:,4)=-dwtdp;
   hr(:,:,5)=cwt;

   fprintf('++++++++++++++++++++ \n');

   fprintf('terms in hr [ %d x %d x (%d terms) ]:  \n',ny,nr,5);
   fprintf('term 1 : vertical heat advection \n');
   fprintf('term 2 : horizontal heat advection \n');
   fprintf('term 3 : meridional convergence of horizontal eddy heat flux \n');
   fprintf('term 4 : vertical convergence of vertical eddy heat flux \n');
   fprintf('term 5 : extra vertical eddy heat flux \n');

end;


