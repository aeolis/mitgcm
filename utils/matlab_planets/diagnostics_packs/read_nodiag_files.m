clear all;

tave=input('load time averaged data? 1-->true, 0-->false : ');
diag_type=input('choose types of diagnostics: (1) Basic dynamic Field, (2) Momentum, (3) Heat, (4) or water vapor : '); 
cas=input('choose planet, E 1, U 2, N 3, J 4: ');
kappa=input('Atmosphere kappa = R/C_p :  '); 
dt=input('size of time step: ');
nit=input('time to process (Press Enter key to use "alldata") : ');
chdatapath=input('full path to load data /~/ : ','s');

if (isempty(nit) == 1)
   load alldata; % alldata contains the data to be processed
else 
   alldata=nit;
end;

if ((tave ~=1) && (diag_type > 1))
   error('ERROR: diagnostics of eddy fluxes require input of time averaged quantities');
end; 

  ustr='U';
  vstr='V';
  wstr='W';
  tstr='T';
  sstr='S';
  ustr_tave='uVeltave';
  vstr_tave='vVeltave';
  wstr_tave='wVeltave';
  tstr_tave='Ttave';
  sstr_tave='Stave';
  uvstr='UVtave';
  wustr='UWtave';
  wvstr='VWtave';
  utstr='UTtave';
  vtstr='VTtave';
  wtstr='WTtave';
  usstr='UStave';
  vsstr='VStave';
  wsstr='WStave';

  trial_file=sprintf('%s%s.%s',chdatapath,ustr,'0000000000');
  getsize=rdmds(trial_file);
  dims=size(getsize); nx=dims(1); ny=dims(2); nr=dims(3);
  clear trial_file getsize dims;

  u3d=zeros(nx,ny,nr);
  v3d=zeros(nx,ny,nr);
  w3d=zeros(nx,ny,nr);
  t3d=zeros(nx,ny,nr);
  s3d=zeros(nx,ny,nr);

  uv3d=zeros(nx,ny,nr);
  wu3d=zeros(nx,ny,nr);
  wv3d=zeros(nx,ny,nr);

  ut3d=zeros(nx,ny,nr);
  vt3d=zeros(nx,ny,nr);
  wt3d=zeros(nx,ny,nr);

  us3d=zeros(nx,ny,nr);
  vs3d=zeros(nx,ny,nr);
  ws3d=zeros(nx,ny,nr);


for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter));
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        time=allzero
        timed=alldata(niter);

        if (tave == 1)
        ufile=sprintf('%s%s.%s',chdatapath,ustr_tave,time);
        vfile=sprintf('%s%s.%s',chdatapath,vstr_tave,time);
        wfile=sprintf('%s%s.%s',chdatapath,wstr_tave,time);
        tfile=sprintf('%s%s.%s',chdatapath,tstr_tave,time);
        sfile=sprintf('%s%s.%s',chdatapath,sstr_tave,time);
        else
        ufile=sprintf('%s%s.%s',chdatapath,ustr,time);
        vfile=sprintf('%s%s.%s',chdatapath,vstr,time);
        wfile=sprintf('%s%s.%s',chdatapath,wstr,time);
        tfile=sprintf('%s%s.%s',chdatapath,tstr,time);
        sfile=sprintf('%s%s.%s',chdatapath,sstr,time);
        end;

        uics1=rdmds(ufile);
        vics1=rdmds(vfile);
        wics1=rdmds(wfile);
        tics1=rdmds(tfile);
        sics1=rdmds(sfile);

        u3d=u3d+uics1;
        v3d=v3d+vics1;
        w3d=w3d+wics1;
        t3d=t3d+tics1;
        s3d=s3d+sics1;



        if (diag_type == 2)
        uvfile=sprintf('%s%s.%s',chdatapath,uvstr,time);
        wufile=sprintf('%s%s.%s',chdatapath,wustr,time);
        wvfile=sprintf('%s%s.%s',chdatapath,wvstr,time);

        uvics1=rdmds(uvfile);
        wuics1=rdmds(wufile);
        wvics1=rdmds(wvfile);

        uv3d=uv3d+uvics1;
        wu3d=uv3d+wuics1;
        wv3d=uv3d+wvics1;
        end;
      
        if (diag_type == 3)
        utfile=sprintf('%s%s.%s',chdatapath,utstr,time);
        vtfile=sprintf('%s%s.%s',chdatapath,vtstr,time);
        wtfile=sprintf('%s%s.%s',chdatapath,wtstr,time);
 
        utics1=rdmds(utfile);
        vtics1=rdmds(vtfile);
        wtics1=rdmds(wtfile);

        ut3d=ut3d+utics1;
        vt3d=vt3d+vtics1;
        wt3d=wt3d+wtics1;
        end;

        if (diag_type == 4)
        usfile=sprintf('%s%s.%s',chdatapath,usstr,time);
        vsfile=sprintf('%s%s.%s',chdatapath,vsstr,time);
        wsfile=sprintf('%s%s.%s',chdatapath,wsstr,time);
 
        usics1=rdmds(usfile);
        vsics1=rdmds(vsfile);
        wsics1=rdmds(wsfile);

        us3d=us3d+usics1;
        vs3d=vs3d+vsics1;
        ws3d=ws3d+wsics1;
        end;

clear uics1 vics1 wics1 tics1 sics1 uvics1 wuics1 wvics1 utics1 vtics1 wtics1 usics1 vsics1 wsics1;

end;

u3d=u3d/length(alldata);
v3d=v3d/length(alldata);
w3d=w3d/length(alldata);
t3d=t3d/length(alldata);

if (diag_type == 2)
uv3d=uv3d/length(alldata);
wu3d=wu3d/length(alldata);
wv3d=wv3d/length(alldata);
end;

if (diag_type == 3)
ut3d=ut3d/length(alldata);
vt3d=vt3d/length(alldata);
wt3d=wt3d/length(alldata);
end;

if (diag_type == 4)
us3d=us3d/length(alldata);
vs3d=vs3d/length(alldata);
ws3d=ws3d/length(alldata);
end;

% read grid files, use network or local directories, defined by chdatapath

dxc=rdmds([chdatapath,'DXC']);
dyc=rdmds([chdatapath,'DYC']);
dxg=rdmds([chdatapath,'DXG']);
dyg=rdmds([chdatapath,'DYG']);


xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);
xg=rdmds([chdatapath,'XG']);
yg=rdmds([chdatapath,'YG']);

rAc=rdmds([chdatapath,'RAC']);
rAz=rdmds([chdatapath,'RAZ']);
rAs=rdmds([chdatapath,'RAS']);
rAw=rdmds([chdatapath,'RAW']);

rC=rdmds([chdatapath,'RC']);
rF=rdmds([chdatapath,'RF']);
drC=rdmds([chdatapath,'DRC']);
drF=rdmds([chdatapath,'DRF']);

hFacC=rdmds([chdatapath,'hFacC']);
hFacW=rdmds([chdatapath,'hFacW']);
hFacS=rdmds([chdatapath,'hFacS']);

t_Ref_file=sprintf('%s%s.%s',chdatapath,tstr,'0000000000');
t_Ref=rdmds(t_Ref_file);
tRef=squeeze(t_Ref(1,1,:));
clear t_Ref;

% load physical parameters for the model

if cas==1
   rsphere=6378.0e3;
   gravity=9.81;
   omega=2*pi/86400;
   day=timed*dt/86400;

elseif cas==2
   rsphere=25559.0e3;
   gravity=7.77;
   omega=2*pi/62064;
   day=timed*dt/86400;

elseif cas==3
   rsphere=24746.0e3;
   gravity=11.7;
   omega=2*pi/62064;
   day=timed*dt/86400;

elseif cas==4
   rsphere=71492.0e3;
   gravity=22.88;
   omega=2*pi/35730;
   day=timed*dt/86400;

else
   'no options';
end;

% define vertical grids and reference temperature

rC = squeeze(rC);  % pc is at the center of the vertical grids.
drF = -squeeze(drF); % drF is the steps of the vertical grids, starts from lower boundary dp<0.
drC = -squeeze(drC);
tr = tRef;  % tr is the reference potential temperature profile.
rF = squeeze(rF); % pf is the pressure at faces of vertical grid.

% check if TPprofile matches datasets
dims = size(u3d); 
if length(dims) == 2, nr=1; else nr=prod(dims(3:end)); end;


% packup dynamic fields and grid files to single arrays


dyn3d(:,:,:,1) = u3d;
dyn3d(:,:,:,2) = v3d;
dyn3d(:,:,:,3) = w3d;
dyn3d(:,:,:,4) = t3d;
dyn3d(:,:,:,5) = s3d;

if (diag_type == 3)
tr3d(:,:,:,1) = ut3d; % heat transport
tr3d(:,:,:,2) = vt3d;
tr3d(:,:,:,3) = wt3d;
end;

if (diag_type == 2)
mom3d(:,:,:,1) = uv3d; % momentum transport
mom3d(:,:,:,2) = wu3d;
mom3d(:,:,:,3) = wv3d;
end;

g2d(:,:,1) = xc;
g2d(:,:,2) = yc;
g2d(:,:,3) = xg;
g2d(:,:,4) = yg;
g2d(:,:,5) = dxc;
g2d(:,:,6) = dyc;
g2d(:,:,7) = dxg;
g2d(:,:,8) = dyg;
g2d(:,:,9) = rAc;
g2d(:,:,10) = rAz;
g2d(:,:,11) = rAs;
g2d(:,:,12) = rAw;


clear u3d v3d w3d t3d s3d ut3d vt3d wt3d uv3d wu3d wv3d us3d vs3d ws3d;
