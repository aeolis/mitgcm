function [conti6t,conti_2d]=check_conti_cs(dyn3d,g2d,drF);

% check_conti_cs calculates the continuity euqation du/dx+dv/dy+dw/dp
% on CS-grid.
%  assume u3d(nc*6,nc,*), v3d(nc*6,nc,*), dxC(nc*6,nc), dyC(nc*6,nc)
%    and deal with: rAc(nc*6,nc)
%  output is provided with 2 shapes:
%   conti_2d(nc*6*nc+2,*) = compressed form;
%   conti6t(nc+1,nc+1,*,6) = face splitted.

u3d=squeeze(dyn3d(:,:,:,1));
v3d=squeeze(dyn3d(:,:,:,2));
w3d=squeeze(dyn3d(:,:,:,3));

dxg=squeeze(g2d(:,:,7));
dyg=squeeze(g2d(:,:,8));
rAc=squeeze(g2d(:,:,9));

dims=size(u3d); nx=dims(1); nc=dims(2); ncp=nc+1; n2p=nc+1; nPg=nx*nc ;
if nx == 6*nc, flag=1; else flag=0; end
if size(v3d) ~= dims, flag=0; end
if size(dxg) ~= dims(1:2), flag=0; end
if size(dyg) ~= dims(1:2), flag=0; end
if flag == 0,
 fprintf(' Error in size of input arrays\n');
 vort=0; return
end
if length(dims) == 2, nr=1; else nr=prod(dims(3:end)); end

%siC=prod(size(rAc));
%if siC == nPg+2,
% rAc=reshape(rAc,[nPg+2 1]);
% aC=split_Z_cub(rAc);
%elseif siC == nPg,
% rAc=reshape(rAc,[nPg 1]);
% aCc=zeros(nPg+2,1); aCc(1:nPg,:)=rAc; aCc(nPg+1)=rAc(1); aCc(nPg+2)=rAc(1);
% aC=split_Z_cub(aCc);
%else
% fprintf(' Error in size of rAc input array\n');
% vort=0; return
%end

aC=split_Z_cub(rAc);

u3d=reshape(u3d,[nx nc nr]);
v3d=reshape(v3d,[nx nc nr]);
w3d=reshape(w3d,[nx nc nr]);

[u6t,v6t]=split_UV_cub(u3d,v3d,1,1);
[d6x,d6y]=split_UV_cub(dxg,dyg,0,2);
w6t=split_Z_cub(w3d);

if nr > 1,
  u6t=permute(u6t,[1 2 4 3]);
  v6t=permute(v6t,[1 2 4 3]);
  w6t=permute(w6t,[1 2 4 3]);
end
z6t=zeros(nc,nc,6,nr);

% here z6t is du/dx+dv/y on six tiles, see integrate_for_w.F for algorithm.

for k=1:nr,
 vv1=d6y([1:ncp],[1:nc],:).*u6t(:,:,:,k);
 vv2=d6x([1:nc],[1:ncp],:).*v6t(:,:,:,k);
 z6t(:,:,:,k)=vv1([2:ncp],[1:nc],:)-vv1([1:nc],[1:nc],:);
 z6t(:,:,:,k)=z6t(:,:,:,k)+(vv2([1:nc],[2:ncp],:)-vv2([1:nc],[1:nc],:));
 z6t(:,:,:,k)=z6t(:,:,:,k)./aC([1:nc],[1:nc],:);
end;

for k=1:nr,
 for n=1:3,
   f=2*n-1; %- odd face number
   %- S-W corner
   z6t(1,1,f,k)   = NaN;
   %- S-E corner
   z6t(nc,1,f,k) = NaN;
   %- N-E corner
   z6t(nc,nc,f,k)=NaN;
   %- N-W corner
   z6t(1,nc,f,k) = NaN;
   f=2*n; %- even face number
   %- S-W corner
   z6t(1,1,f,k)   = NaN;
   %- S-E corner
   z6t(nc,1,f,k) = NaN;
   %- N-E corner
   z6t(nc,nc,f,k)=NaN;
   %- N-W corner
   z6t(1,nc,f,k) = NaN;
 end
end;

% calculate vertical component dw/dp

dwp6t=zeros(nc,nc,6,nr);

for k=1:nr-1
 dwp6t(:,:,:,k)=(w6t([1:nc],[1:nc],:,k+1)-w6t([1:nc],[1:nc],:,k))/drF(k); 
 % drF is actually should be calculated by pstar (add it here later)
end;

% check total of du/dx+dv/dy+dw/dp, ignore the very top layer k=nr
% assume at the top layer, du/dx+dv/dy+dw/dp=0

conti6t=z6t+dwp6t;

% compressed form
conti_2d=zeros(nPg,nr);

%  extract the interior
conti_2d([1:nPg],:)=reshape(permute(conti6t(1:nc,1:nc,:,:),[1 3 2 4]),[nPg nr]);

%- back into final shape:
if length(dims) == 2,
 conti_2d=squeeze(conti_2d);
else
 conti_2d=reshape(conti_2d,[nPg dims(3:end)]);
end;


