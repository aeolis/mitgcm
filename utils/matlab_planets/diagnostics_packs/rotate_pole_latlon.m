function [rln,rlt]=rotate_pole_latlon(gln,glt,phir,thetar,psir);

% rotate_pole_latlon is a function that rotates the polar-spherical 
% coordinate to a arbitrary spherical coordinate

% phir, thetar and psir are the angles that determine the rotated 
% grid. These angles are standard Euler angles (see "Classic mechanics" 
% by Goldstein). Also see mathworld.wolfram.com for reference.

% phir and thetar define the poles of rotated grid. psir defines 
% where the reference longitude (prime meridian) is in rotated grid. 
% It can be set to 0 in most cases.

% the rotation is done in Cartisian coordinates
% x, y and z define the orginal grid (z defines the poles)
% x', y' and z' are the rotated grid.

% convert gln and glt from degree to radian

deg2rad=pi/180;

thetas=gln*deg2rad;
phis=(90.0-glt)*deg2rad; % phis is the angle from axis z

phir=phir*deg2rad;
thetar=thetar*deg2rad;
psir=psir*deg2rad;

% convert the geographic spherical coordinates to Cartisian coordinates

gx=sin(phis)*cos(thetas);
gy=sin(phis)*sin(thetas);
gz=cos(phis);

% define the rotation matrix A (see Euler angle for reference)

A(1,1)=cos(psir)*cos(phir)-cos(thetar)*sin(phir)*sin(psir);
A(1,2)=cos(psir)*sin(phir)+cos(thetar)*cos(phir)*sin(psir);
A(1,3)=sin(psir)*sin(thetar);

A(2,1)=-sin(psir)*cos(phir)-cos(thetar)*sin(phir)*cos(psir);
A(2,2)=-sin(psir)*sin(phir)+cos(thetar)*cos(phir)*cos(psir);
A(2,3)=cos(psir)*sin(thetar);

A(3,1)=sin(thetar)*sin(phir);
A(3,2)=-sin(thetar)*cos(phir);
A(3,3)=cos(thetar);

% transform from x, y and z to x', y' and z'

gc=[gx gy gz]';

rc=A*gc;

% convert x' y' and z' to latlon in rotated grid.
% check rounding errors

rc(3)=min(rc(3),1);
rc(3)=max(rc(3),-1);

% rx=rc(1), ry=rc(2), rz=rc(3)

% calculate latitude in rotated grid
rlt=acos(rc(3));

% calculate longitude in rotated grid

%  if the point in original grid is at one pole of rotated grid
%  then rotated longitude is equal to initial.

if (rlt <= 0 || rlt >= 180.0*deg2rad)
   rln=thetas;
   else  % -135 < rln <-45 or 45 < rln < 135
   if (abs(rc(1)/sin(rlt)) < cos(45.0*deg2rad))
      rln=acos(max(min(rc(1)/sin(rlt), 1.0), -1.0));
      if (rc(2) < 0) 
         rln=-rln;
      end;
   else
      rln=asin(max(min(rc(2)/sin(rlt), 1.0), -1.0));
      if (rc(1) < 0.0)
         rln=180.0*deg2rad-rln;
      end;
   end;
end;

% convert rln and rlt from radian to degree

rlt=90.0-rlt/deg2rad;
rln=rln/deg2rad;

if (rln > 180.0)
   rln=rln-360.0;
end;

if (rln <= -180.0)
   rln=rln+360.0;
end;






