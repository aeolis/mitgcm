function [rt3d,lx_temp,ly_temp]=rotate_general_spGrid(init3d,xi,yi,phir,thetar,psir);

% this function rotate the physical variables in polar-spherical coordinate to 
% an arbitrary spherical coordinate. The new rotation axis (or poles) is 
% defined by phir,thetar and psir.

% xi and yi are original longitude and latitude. 
% lx and ly are new longitude and latitude in rotated grid.
% init3d is the 3-D dynamic variables in original grid.
% rt3d is the 3-D dynamic variables in rotated grid.

% example:  
% this will produce the same rotation as the one in native cube-sphere grid:

% [rt3d,lx,ly]=rotate_general_spGrid(init3d,xi,yi,90,90,-90);

dims=size(init3d);
nx=dims(1); ny=dims(2); nz=dims(3);

if (length(xi) ~= nx) || (length(yi) ~= ny)
   fprintf(' Error in size of input arrays, check xi, yi and input 3D field. \n');
   return;
end;   

lx_temp=zeros(nx,ny);
ly_temp=zeros(nx,ny);

% calculate the longitudes and latitudes in rotated grids 
% from longitudes and latitudes in riginal grids. 
% lx_temp and ly_temp are not in sorted order.
for i=1:nx
 for j=1:ny
 [rln,rlt]=rotate_pole_latlon(xi(i),yi(j),phir,thetar,psir);
 lx_temp(i,j)=rln;
 ly_temp(i,j)=rlt;
 end;
end;

% interpolate 3-D field in rotated grid to standard 
% polar-spherical coordinate. Linear interpolation is 
% implemented here.

rt3d=zeros(nx,ny,nz);

[cx,cy]=meshgrid(xi,yi);
cx=cx'; cy=cy';

% filling the missing points at two original poles (two spots at equator of 
% rotated grid, (-90,0) and (90,0)). This is necessary for CS grid but not 
% for latlon grid. Use linear interpolation.

% Use linear interpolation to make a "smooth" surface in rotated grid.
% the interpolation method "linear" produces NaNs at convex hull. These 
% NaNs need to be filled. Use method "nearest" to fill the missing points.
 
rt3d_corr=zeros(nx,ny,nz);

init3d(:,1,:)=2.0*init3d(:,2,:)-init3d(:,3,:);
init3d(:,ny,:)=2.0*init3d(:,ny-1,:)-init3d(:,ny-2,:);

for k=1:nz
    rt3d(:,:,k)=griddata(lx_temp,ly_temp,squeeze(init3d(:,:,k)),cx,cy);
    rt3d_corr(:,:,k)=griddata(lx_temp,ly_temp,squeeze(init3d(:,:,k)),cx,cy,'nearest');
end;

% fill the missing points by linear interpolation

for i=1:nx
 for j=1:ny
     if isnan(rt3d(i,j,1))
        rt3d(i,j,:)=rt3d_corr(i,j,:);
     end;
 end;
end;

lx=xi;
ly=yi;

% the final output is rt3d(lx,ly,p), where p is pressure level (layer number).
