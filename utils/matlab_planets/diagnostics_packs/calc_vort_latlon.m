function [vort_r,vort_p,vort_ipv]=calc_vort_latlon(dyn3d,omega,gravity,g2d,drF);

% check_conti_latlon calculates the continuity euqation du/dx+dv/dy+dw/dp
% on spherical-polar grid.
% it calculates the continuity equation for both a whole sphere or a channel.

% vort_r : relative vorticity (cell centered)
% vort_p : planetary vorticity (cell centered)
% vort_ipv : isobaric potential vorticity (following Nakamura and Wallace (1993))

% add one extra grid point in x direction for v (native) 
% add one extra grid point in y direction for u (extrapolate), currently 
% ignore the grid point at north boundary
% add one extra grid point in x and one in y for theta 

u3d=squeeze(dyn3d(:,:,:,1));
v3d=squeeze(dyn3d(:,:,:,2));
w3d=squeeze(dyn3d(:,:,:,3));
t3d=squeeze(dyn3d(:,:,:,4));

xc=squeeze(g2d(:,:,1));
yc=squeeze(g2d(:,:,2));
xg=squeeze(g2d(:,:,3));
yg=squeeze(g2d(:,:,4));
dxc=squeeze(g2d(:,:,5));
dyc=squeeze(g2d(:,:,6));
dxg=squeeze(g2d(:,:,7));
dyg=squeeze(g2d(:,:,8));
rAc=squeeze(g2d(:,:,9));
rAz=squeeze(g2d(:,:,10));
rAs=squeeze(g2d(:,:,11));
rAw=squeeze(g2d(:,:,12));
AngleCS=squeeze(g2d(:,:,13));
AngleSN=squeeze(g2d(:,:,14));

knorm=0;

dims=size(u3d); nx=dims(1); ny=dims(2); 
if length(dims) == 2, nr=1; else nr=prod(dims(3:end)); end;

v3d(nx+1,:,:)=v3d(1,:,:);
u3d(:,ny+1,:)=2.0*u3d(:,ny,:)-u3d(:,ny-1,:);

utrans=zeros(nx,ny+1,nr);
vtrans=zeros(nx+1,ny,nr);

for k=1:nr,
 utrans(:,[1:ny],k)=u3d(:,[1:ny],k).*dxc;
 vtrans([1:nx],:,k)=v3d([1:nx],:,k).*dyc;

 vtrans(nx+1,:,k)=reshape(  ...
                 (squeeze(v3d(nx+1,:,k)).*squeeze(dyc(1,:))), [1 ny 1]);
end;

% calculate relative vorticity
vort_r=zeros(nx,ny,nr);

for k=1:nr,
 vort_r(:,:,k)= ( (vtrans([2:nx+1],:,k)-vtrans([1:nx],:,k))- ... 
                  (utrans(:,[2:ny+1],k)-utrans(:,[1:ny],k)) )./rAz;
end;


% filling the north and east boundaries with NaNs
vort_r(nx,:,:)=NaN;
vort_r(:,ny,:)=NaN;

% calculate quasigeostrophic potential vorticity (cell centered)

vort_p=2.0*omega*sin(yc*pi/180.0);



t3d_f=zeros(nx,ny,nr+1);

for k=1:nr
 km1 = max(k-1,1);
 t3d_f(:,:,k)=(t3d(:,:,km1)+t3d(:,:,k))/2.0;
end;

%--- upper boundaries---
t3d_f(:,:,nr+1)=2.0*t3d(:,:,nr)-t3d_f(:,:,nr);

%----calculate stability factor s(p)----
%----1--<ln(theta)>---
lnT(1:nr) = 0.0;
lnT_f(1:nr+1) = 0.0;

for k=1:nr
 lnT(k) = sum( sum(log(t3d(:,:,k)).*rAc,2) );
 lnT(k) = lnT(k)/(sum(sum(rAc)));
end;

for k=1:nr+1
 lnT_f(k) = sum( sum(log(t3d_f(:,:,k)).*rAc,2) );
 lnT_f(k) = lnT_f(k)/(sum(sum(rAc)));
end;

%---2-- s(p) = -d(lnT)/dp) ---

sp_temp_1 = -(lnT_f(2)-lnT_f(1))/ (log(rF(2))-log(rF(1))) ;
sp_temp_nr = -(lnT_f(nr+1)-lnT_f(nr))/(log(rF(nr+1))-log(rF(nr)));

sp(1:nr+1) = 0.0;

sp(2:nr) = -(lnT(2:nr)-lnT(1:nr-1))./reshape( (log(rC(2:nr))-log(rC(1:nr-1))),[1 nr-1]) ;

sp(1) = 2.0*sp_temp_1-sp(2);

sp(nr+1) = 2.0*sp_temp_nr - sp(nr);

%----calculate <T>--------

rT3d_f = zeros(nx,ny,nr+1);

for k=1:nr
 rT3d_f(:,:,k)=t3d_f(:,:,k)*(rF(k)/pref)^kappa;
end;

rT3d_f(:,:,nr+1)=rT3d_f(:,:,nr);

aT(1:nr+1) = 0.0;

for k=1:nr+1
 aT(k) = sum( sum(rT3d_f(:,:,k).*rAc,2) );
 aT(k) = aT(k)/(sum(sum(rAc)));
end;

term3_r = zeros(nx,ny,nr+1);

for k=1:nr+1
 term3_r(:,:,k) = rF(k)/sp(k)/aT(k)*(rT3d_f(:,:,k)-aT(k));
end;

%----calculate zonal mean of terms in term 3 on RHS

term3_2d=squeeze(sum(term3_r,1)/nx);

%---calculate term 3 on RHS---

term3 = zeros(nx,ny,nr);

%---expand term3_2d to 3d
term3_3d = zeros(nx,ny,nr+1);

for i=1:nx
 term3_3d(i,:,:)=reshape(term3_2d(:,:), [1 ny nr+1]);
end;

for k=1:nr
 term3(:,:,k) = vort_p(:,:).*( term3_3d(:,:,k+1)-term3_3d(:,:,k) )/drF(k);
end;

%----calculate qgpv-------

qgpv = zeros(nx,ny,nr);

for k=1:nr
 qgpv(:,:,k) = vort_p(:,:) + vort_r(:,:,k) - term3(:,:,k);
end;


