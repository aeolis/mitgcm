function [hr]=calc_eddy_heat_latlon(dyn3d,tr3d,g2d,drC,drF,rC,rF,kappa,varargin);

% the heating equation follows Karoly (1998)

% usage 1, without condensation of water vapor
%          [hr] = calc_eddy_heat_latlon(dyn3d,tr3d,g2d,drC,drF,rC,rF,kappa,varargin);
%          Example:
%                  [hr] = calc_eddy_heat_latlon(dyn3d,tr3d,g2d,drC,drF,rC,rF,kappa);

% usage 2, with condensation of water vapor
%          [hr] = calc_eddy_heat_latlon(dyn3d,tr3d,g2d,drC,drF,rC,rF,kappa,varargin);
%          Example:
%                  [hr] = calc_eddy_heat_latlon(dyn3d,tr3d,g2d,drC,drF,rC,rF,kappa,'w',tau,tauc,tRef);


% the following terms are time-averaged and zonal-mean quantities
% hr -- total heating rate
% dhdt -- diabatic heating
% gamw -- vertical advection of heat
% vdtdy -- horizontal advection of heat
% dvtdy -- horizontal convergence of eddy heat heat flux
% dwtdp -- vertical convergence of eddy heat flux


% cwt -- vertical eddy heat flux (extra terms associated with dwtdp)
% tau -- rediative cooling time scale, in seconds
% tauc -- condensation time scale, in seconds

optargin=size(varargin,2);

if (optargin > 0)
   if (varargin{1} ~='w')
      error('!!!   Stop, check the number of arguments. See usage in calc_eddy_heat_cs.m ');
   end;
   tau=varargin{2};
   tauc=varargin{3};
   tRef=varargin{4};
end;

pref=1.0e5; %reference pressure is 1 bar.

u3d=squeeze(dyn3d(:,:,:,1));
v3d=squeeze(dyn3d(:,:,:,2));
w3d=squeeze(dyn3d(:,:,:,3));
t3d=squeeze(dyn3d(:,:,:,4));
s3d=squeeze(dyn3d(:,:,:,5));

ut3d=squeeze(tr3d(:,:,:,1));
vt3d=squeeze(tr3d(:,:,:,2));
wt3d=squeeze(tr3d(:,:,:,3));

xc=squeeze(g2d(:,:,1));
yc=squeeze(g2d(:,:,2));
xg=squeeze(g2d(:,:,3));
yg=squeeze(g2d(:,:,4));
dxc=squeeze(g2d(:,:,5));
dyc=squeeze(g2d(:,:,6));
dxg=squeeze(g2d(:,:,7));
dyg=squeeze(g2d(:,:,8));
rAc=squeeze(g2d(:,:,9));
rAz=squeeze(g2d(:,:,10));
rAs=squeeze(g2d(:,:,11));
rAw=squeeze(g2d(:,:,12));

check_var=input('caution: must use time average variables !!!, press 1 to contiue  ');

if (check_var ~= 1)
   error('Stop, use time averaged variables');
end;

dims=size(u3d); nx=dims(1); ny=dims(2);
if length(dims) == 2, nr=1; else nr=prod(dims(3:end)); end;

rF(nr+1)=0.0;

if (optargin > 0)
 if (varargin{1}=='w')
   %---calculate diabatic heating (cell centerd)---d[h]/dt------optional-----------------------------------1

   %---use virtual potential temperature for diagnostics---optional-----

   Rq=-0.8778;
   t3d=t3d.*(Rq*s3d+1.0);

   RWV = 461.0;
   es0 = 609.140;
   T0ref = 273.0;
   LHWV = 2.5e6;
   Cp   = 13000.0;

   realT=zeros(nx,ny,nr);
  
   for k=1:nr,
      realT(:,:,k)=(rC(k)/pref)^kappa*t3d(:,:,k);
   end;

   esT =es0*exp(-LHWV/RWV*(1.0./realT-1.0/T0ref));

   qs       = zeros(nx,ny,nr);
   delta    = zeros(nx,ny,nr);
   latentWV = zeros(nx,ny,nr);

   for k=1:nr, qs(:,:,k)=9*esT(:,:,k)/rC(k); end;

   for i=1:nx, for j=1:ny, for k=1:nr,
      if s3d(i,j,k)>=qs(i,j,k) delta(i,j,k)=1; end;
   end; end; end;

   for k=1:nr,
      latentWV(:,:,k)=LHWV/Cp*delta(:,:,k).*(s3d(:,:,k)-qs(:,:,k))/tauc*(pref/rC(k))^kappa;
   end;

   dhdt=zeros(nx,ny,nr);

   for k=1:nr,
       dhdt(:,:,k)=( -(t3d(:,:,k)-tRef(k))/tau+latentWV(:,:,k) )*(rC(k)/pref)^kappa;
   end;

   dhdt=squeeze(sum(dhdt,1))/nx;

 end;
end;

%---calculate vertical advection of heat (cell centered)---gamma*[w]-------------------------------2

Gamma=zeros(ny,nr);
gamma_w_F=zeros(ny,nr);

t2d=squeeze(sum(t3d,1))/nx;

for k=1:nr,
 km1 = max(k-1,1);
 Gamma(:,k)=-(rF(k)/pref)^kappa*(t2d(:,k)-t2d(:,km1))/drC(k);
end;

gamma_w_F=Gamma.* squeeze(sum(w3d,1))/nx;
gamma_w_F(:,nr+1)=0.0;

% move to cell center (in vertical)

gammaw=zeros(ny,nr);
gammaw(:,[1:nr])=( gamma_w_F(:,[1:nr])+gamma_w_F(:,[2:nr+1]) )/2.0;

%---calculate horizontal advection of eddy heat (cell centered)---[v]*d[T]/dy------------------------3

vdtdy_v=zeros(ny,nr);

v2d=squeeze(sum(v3d,1))/nx;

for k=1:nr,
 vdtdy_v([2:ny],k)=v2d([2:ny],k).* (t2d([2:ny],k)-t2d([1:ny-1],k))./(squeeze(dyc(1,[2:ny])))'*(rC(k)/pref)^kappa;
end;

% move to cell center

vdtdy=zeros(ny,nr);
vdtdy([1:ny-1],:)=( vdtdy_v([1:ny-1],:)+vdtdy_v([2:ny],:) )/2.0;

% filling vdtdy(1,:) and vdtdy(ny,:) with NaNs

vdtdy(1,:)=NaN; vdtdy(ny,:)=NaN;

%---calculate horizontal eddy heat flux d/dy([v'T']+[v*T*])---use Physics of climate ---[v'T']+[v*T*]=[vT]-[v][T]----------4

vt2d=squeeze(sum(vt3d,1))/nx;

t2d_v=zeros(ny,nr);
t2d_v([2:ny],:)=(t2d([1:ny-1],:)+t2d([2:ny],:))/2.0;
vt_star=vt2d-v2d.*t2d_v;

% d/dy([v'T']+[v*T*]) (cell centered)

dvtdy=zeros(ny,nr);

for k=1:nr,
 dvtdy([1:ny-1],k)=(vt_star([2:ny],k)-vt_star([1:ny-1],k))./(squeeze(dyg(1,[1:ny-1])))'*(rC(k)/pref)^kappa;
end;

% filling dvtdy(1,:) and dvtdy(ny,:) with NaNs

dvtdy(1,:)=NaN; dvtdy(ny,:)=NaN;

%---calculate horizontal eddy heat flux d/dp([w'T']+[w*T*])---use Physics of climate ---[w'T']+[w*T*]=[wT]-[w][T]----------5

wt2d=squeeze(sum(wt3d,1))/nx;
w2d =squeeze(sum(w3d,1))/nx;

t2d_w=zeros(ny,nr);

for k=1:nr,
 km1 = max(k-1,1);
 t2d_w(:,k)=(t2d(:,k)+t2d(:,km1))/2.0;
end;

wt_star=wt2d-w2d.*t2d_w;

% d/dp([w'T']+[w*T*]) (cell centered)

wt_star(:,nr+1)=0.0;
dwtdp=zeros(ny,nr);

for k=1:nr,
 dwtdp(:,k)=(wt_star(:,k+1)*(rF(k+1)/pref)^kappa-wt_star(:,k)*(rF(k)/pref)^kappa)/drF(k);
end;

%---calculate kappa/p*([w'T']+[w*T*])-----------------------------------------------------------------------------6

cwt=zeros(ny,nr);

for k=1:nr,
 cwt(:,k)=kappa/rC(k)*(wt_star(:,k+1)*(rF(k+1)/pref)^kappa+wt_star(:,k)*(rF(k)/pref)^kappa)/2.0;
end;

%---calculate the total heating rate

if (optargin > 0)
   hr=zeros(ny,nr,6);
   hr(:,:,1)=dhdt;
   hr(:,:,2)=gammaw;
   hr(:,:,3)=-vdtdy;
   hr(:,:,4)=-dvtdy;
   hr(:,:,5)=-dwtdp;
   hr(:,:,6)=cwt;

   fprintf('++++++++++++++++++++ \n');

   fprintf('terms in hr [ %d x %d x (%d terms) ]:  \n',ny,nr,6);
   fprintf('term 1 : diabatic heating \n');
   fprintf('term 2 : vertical heat advection \n');
   fprintf('term 3 : horizontal heat advection \n');
   fprintf('term 4 : meridional convergence of horizontal eddy heat flux \n');
   fprintf('term 5 : vertical convergence of vertical eddy heat flux \n');
   fprintf('term 6 : extra vertical eddy heat flux \n');


else
   hr=zeros(ny,nr,5);
   hr(:,:,1)=gammaw;
   hr(:,:,2)=-vdtdy;
   hr(:,:,3)=-dvtdy;
   hr(:,:,4)=-dwtdp;
   hr(:,:,5)=cwt;

   fprintf('++++++++++++++++++++ \n');

   fprintf('terms in hr [ %d x %d x (%d terms) ]:  \n',ny,nr,5);
   fprintf('term 1 : vertical heat advection \n');
   fprintf('term 2 : horizontal heat advection \n');
   fprintf('term 3 : meridional convergence of horizontal eddy heat flux \n');
   fprintf('term 4 : vertical convergence of vertical eddy heat flux \n');
   fprintf('term 5 : extra vertical eddy heat flux \n');

end;

