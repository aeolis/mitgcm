function [t3drt_cs]=rotate_ssaxis(t3d,nx,nc,nr);

t1=tiles(t3d,1);
t2=tiles(t3d,2);
t3=tiles(t3d,3);
t4=tiles(t3d,4);
t5=tiles(t3d,5);
t6=tiles(t3d,6);


for i=1:nc
for j=1:nc
tnew(j,nc-i+1,:,3)=t1(i,j,:);
tnew(j,nc-i+1,:,2)=t2(i,j,:);      
end;
end;

for i=1:nc
for j=1:nc
tnew(i,j,:,5)=t5(j,nc-i+1,:);
tnew(i,j,:,6)=t4(j,nc-i+1,:);
end;
end;

tnew(:,:,:,4)=t3;
tnew(:,:,:,1)=t6;

t3drt_cs=permute(tnew,[1 4 2 3]);    
t3drt_cs=reshape(t3drt_cs,[nx nc nr]);

