function [KE,wz]=calc_KE_cs(dyn3d,g2d,rC,drF,rAc,kappa,atm_cp,gravity);

% calculate total kinetic energy in J
% vertical velocity wz (m/s) is calculated by:

% wz(k)-wz(k+1)=-RT(k)/g*(w(k)-w(k+1))

% total KE is
% KE(i,j)=dp/g*rA(i,j)*1/2*(u^2+v^2+wz^2)

pref=1.0e5; % reference pressure at 1 bar.

atm_R=atm_cp*kappa;

u3d=squeeze(dyn3d(:,:,:,1));
v3d=squeeze(dyn3d(:,:,:,2));
w3d=squeeze(dyn3d(:,:,:,3));
t3d=squeeze(dyn3d(:,:,:,4));
s3d=squeeze(dyn3d(:,:,:,5));

xc=squeeze(g2d(:,:,1));
yc=squeeze(g2d(:,:,2));
xg=squeeze(g2d(:,:,3));
yg=squeeze(g2d(:,:,4));
dxc=squeeze(g2d(:,:,5));
dyc=squeeze(g2d(:,:,6));
dxg=squeeze(g2d(:,:,7));
dyg=squeeze(g2d(:,:,8));
rAc=squeeze(g2d(:,:,9));
rAz=squeeze(g2d(:,:,10));
rAs=squeeze(g2d(:,:,11));
rAw=squeeze(g2d(:,:,12));

[AngleCS,AngleSN] = cubeCalcAngle(yg,rAc,dxg,dyg);

dims=size(u3d); nx=dims(1); nc=dims(2); ncp=nc+1; n2p=nc+2; nPg=nx*nc ;
if nx == 6*nc, flag=1; else flag=0; end
if size(v3d) ~= dims, flag=0; end
if size(dxc) ~= dims(1:2), flag=0; end
if size(dyc) ~= dims(1:2), flag=0; end
if flag == 0,
 fprintf(' Error in size of input arrays\n');
 vort=0; return
end
if length(dims) == 2, nr=1; else nr=prod(dims(3:end)); end


[uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C');

u6t=split_Z_cub(uE);
v6t=split_Z_cub(vN);
w6t=split_Z_cub(w3d);
t6t=split_Z_cub(t3d);

if nr > 1,
  u6t=permute(u6t,[1 2 4 3]);
  v6t=permute(v6t,[1 2 4 3]);
  t6t=permute(t6t,[1 2 4 3]);
  w6t=permute(w6t,[1 2 4 3]);
end;

aC=split_Z_cub(rAc);

wz6t=zeros(nc,nc,6,nr+1);
w6t(:,:,:,nr+1)=0.0;

realT6t=zeros(nc,nc,6,nr);

for k=1:nr,
 realT6t([1:nc],[1:nc],:,k)=t6t([1:nc],[1:nc],:,k)*(rC(k)/pref)^kappa;
end;

for k=nr:-1:1,
 wz6t([1:nc],[1:nc],:,k)=wz6t([1:nc],[1:nc],:,k+1)-atm_R/(rC(k)*gravity)* ... 
                         realT6t([1:nc],[1:nc],:,k).*(w6t([1:nc],[1:nc],:,k)-w6t([1:nc],[1:nc],:,k+1));
end;

wz6t_c=zeros(nc,nc,6,nr);

for k=1:nr,
 wz6t_c([1:nc],[1:nc],:,k)=0.5*( wz6t([1:nc],[1:nc],:,k)+wz6t([1:nc],[1:nc],:,k+1) );
end;

KE_tmp=zeros(nc,nc,6,nr);

for k=1:nr,
 KE_tmp([1:nc],[1:nc],:,k)=0.5*abs(drF(k))/gravity*aC([1:nc],[1:nc],:) ... 
                           .*(u6t([1:nc],[1:nc],:,k).^2+v6t([1:nc],[1:nc],:,k).^2+wz6t_c([1:nc],[1:nc],:,k).^2);
end;

KE_tmp=permute(KE_tmp,[1 3 2 4]);
KE_tmp=reshape(KE_tmp,6*nc,nc,nr);

KE=squeeze(sum(KE_tmp,1));
KE=squeeze(sum(KE,1));
KE=sum(KE);

wz=permute(wz6t,[1 3 2 4]);
wz=reshape(wz,6*nc,nc,nr+1);
