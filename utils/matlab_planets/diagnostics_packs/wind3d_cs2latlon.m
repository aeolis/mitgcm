function [dyn3d_latlon,xi,yi]=wind3d_cs2latlon(dyn3d,g2d,hFacC,ny);

u3d=squeeze(dyn3d(:,:,:,1));
v3d=squeeze(dyn3d(:,:,:,2));
w3d=squeeze(dyn3d(:,:,:,3));
t3d=squeeze(dyn3d(:,:,:,4));
s3d=squeeze(dyn3d(:,:,:,5));

xc=squeeze(g2d(:,:,1));
yc=squeeze(g2d(:,:,2));
xg=squeeze(g2d(:,:,3));
yg=squeeze(g2d(:,:,4));
dxc=squeeze(g2d(:,:,5));
dyc=squeeze(g2d(:,:,6));
dxg=squeeze(g2d(:,:,7));
dyg=squeeze(g2d(:,:,8));
rAc=squeeze(g2d(:,:,9));
rAz=squeeze(g2d(:,:,10));
rAs=squeeze(g2d(:,:,11));
rAw=squeeze(g2d(:,:,12));


dims=size(u3d); nx=dims(1); nc=dims(2); nr=dims(3);

dxr=180.0/ny; %0.703125;
dyr=180.0/ny; %0.703125;
xi=-180+dxr/2:dxr:180-dxr/2;
yi=-90+dxr/2:dxr:90-dxr/2;
lx=length(xi);
ly=length(yi);


[del] = cube2latlon_preprocess(xc,yc,xi,yi);

[u3d,v3d]=uvcube2latlongrid(del,u3d,v3d,xg,yg,rAc,dxg,dyg);
w3d=cube2latlon_fast(del,w3d);
t3d=cube2latlon_fast(del,t3d);
s3d=cube2latlon_fast(del,s3d);

dyn3d_latlon = zeros(lx,ly,nr,5);

dyn3d_latlon(:,:,:,1) = u3d;
dyn3d_latlon(:,:,:,2) = v3d;
dyn3d_latlon(:,:,:,3) = w3d;
dyn3d_latlon(:,:,:,4) = t3d;
dyn3d_latlon(:,:,:,5) = s3d;

fprintf('terms in dyn3d_latlon [ %d x %d x %d x (%d terms) ]:  \n',xi,yi,nr,5);
fprintf('term 1 : zonal winds \n');
fprintf('term 2 : meridional winds \n');
fprintf('term 3 : vertical flow \n');
fprintf('term 4 : potential temperature \n');
fprintf('term 5 : specific humidity \n');


