% output of the matlab script (stored to memory)
% surf2d          stores eta, eta^2 and dt^2
% dyn3d           stores u, v, w, t, s
% mom3d           stores uvc, wu, wv, usq, vsq and uvz
% tr3d            stores ut, vt, wt, us, vs, ws
% shap3d          stores shap_du, shap_dv, shap_dt, shap_ds and shap_dKE
% g2d             stores 2-D grid information.
% note: vertical grids are not part of g2d.

clear all;

usesurfdata=0;  % use surface data to diagnose when "usesurfdata=1"
massweighted=1; % mass weighted velocity when "massweighted=1'
%avragalldata='true'; % average over all the datasets in "alldata"

diag_type=input('choose types of diagnostics: (1) Basic dynamic Field, (2) Momentum, (3) Tracers, (4) Shapiro : ');
cas=input('choose planet, E 1, U 2, N 3, J 4, S 5: ');
kappa=input('Atmosphere kappa = R/C_p :  ');
dt=input('size of time step: ');
nit=input('time to process (Press Enter key to use "alldata") : ');
chdatapath=input('full path to load data /~/ : ','s');

if (isempty(nit) == 1)
   load alldata; % alldata contains the data to be processed
else
   alldata=nit;
end;

allzero='0000000000';
n2s=num2str(nit);
%replace string with different data
for i=1:length(n2s)
    allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
end;

fname=strcat(chdatapath,'U.',allzero);
[ndims] = rdmeta_only(fname)

nx=ndims(1,1); ny=ndims(1,2); nr=ndims(1,3);

nsurf=3; ndyn=5; ndynmass=3; ndiagmoms=6; ndiagtracer=6; ndiagtracermass=6; ndiagshap=5; 

% ----- this section initialize the data to be used for diagnostics.
if (usesurfdata == 1)
   diagSurf_str = 'surfDiag';  % includes all surface data, i.e. Eta (surface pressure anomaly)
   surf2d = zeros(nx,ny,nsurf);
end;

if (massweighted == 1)
   diagDyn_str = 'dynDiagMass';   % includes mass weighted u, v, w
   dyn3d       = zeros(nx,ny,nr,ndyn);
   if      (diag_type == 2)
           diagMom_str = 'momsDiag';  % includes usq, vsq, uvc, uvz, wu and wv 
           mom3d       = zeros(nx,ny,nr,ndiagmoms);
   elseif  (diag_type == 3)
           diagTracer_str  = 'tracerDiagMass'; % includes ut, vt, wt, us, vs, ws, mass weighted
           tr3d        = zeros(nx,ny,nr,ndiagtracermass);
   elseif  (diag_type == 4) 
           diagShap_str = 'shapDiag';  % includes shap_du, shap_dv, shap_dt, shap_ds, shap_dKE
           shap3d      = zeros(nx,ny,nr,ndiagshap);
   else    'no option'
   end;
else
   diagDyn_str  = 'dynDiag';   % includes all dynamic variables: u, v, w, t and s(salinity or specfic humidity)
   dyn3d        = zeros(nx,ny,nr,ndyn);
   if      (diag_type == 2)
           diagMom_str = 'momsDiag';  % includes usq, vsq, uvc, uvz, wu and wv
           mom3d       = zeros(nx,ny,nr,ndiagmoms);
   elseif  (diag_type == 3)
           diagTracer_str  = 'tracerDiag'; % includes ut, vt, wt, us, vs, ws, mass weighted
           tr3d        = zeros(nx,ny,nr,ndiagtracer);
   elseif  (diag_type == 4)
           diagShap_str = 'shapDiag';  % includes shap_du, shap_dv, shap_ds, shap_dt, shap_dKE
           shap3d      = zeros(nx,ny,nr,ndiagshap);
   else    'no option';
   end;
end;


for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter));
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        time=allzero
        timed=alldata(niter);
        if (usesurfdata == 1)
           diagSurf_file = sprintf('%s%s.%s',chdatapath,diagSurf_str,time);
           diagSurf_tmp = rdmds(diagSurf_file);
           surf2d     = surf2d + diagSurf_tmp;
        end;
        
        diagDyn_file    = sprintf('%s%s.%s',chdatapath,diagDyn_str,time);
        diagDyn_file_1  = sprintf('%s%s.%s',chdatapath,'dynDiag',time);
        diagDyn_tmp     = rdmds(diagDyn_file);
        diagDyn_tmp_1   = rdmds(diagDyn_file_1);
        diagDyn_tmp_1(:,:,:,1:ndynmass)  = diagDyn_tmp(:,:,:,1:ndynmass);   clear diagDyn_tmp;
        dyn3d           = dyn3d+diagDyn_tmp_1;                              clear diagDyn_tmp_1;

        if (diag_type == 2) 
           diagMom_file = sprintf('%s%s.%s',chdatapath,diagMom_str,time);
           diagMom_tmp  = rdmds(diagMom_file);                         
           mom3d        = mom3d + diagMom_tmp;                              clear diagMom_tmp;
        end;

        if (diag_type == 3)
           diagTracer_file  = sprintf('%s%s.%s',chdatapath,diagTracer_str,time);
           diagTracer_tmp   = rdmds(diagTracer_file);
           tr3d             = tr3d + diagTracer_tmp;                        clear diagTracer_tmp;
        end;

        if (diag_type == 4)        
           diagShap_file = sprintf('%s%s.%s',chdatapath,diagShap_str,time);
           diagShap_tmp  = rdmds(diagShap_file);
           shap3d        = shap3d + diagShap_tmp;                           clear diagShap_tmp;
        end;
end;

if (usesurfdata == 1)
   surf2d  = surf2d/length(alldata);
end;

dyn3d  = dyn3d/length(alldata);

if (diag_type == 2)
   mom3d  = mom3d/length(alldata);
end;

if (diag_type == 3)
   tr3d   = tr3d/length(alldata);
end;

if (diag_type == 4)
   shap3d = shap3d/length(alldata);
end;

% mom3d, originally it is "usq, vsq, uvc, uvz, wu and wv", reorder this to "uvc, wu, wv, usq, vsq and uvz"

if (diag_type == 2)
   mom3d = reorder(mom3d,4,[3 5 6 1 2 4]);
end;

% read grid files, use network or local directories, defined by chdatapath

dxc=rdmds([chdatapath,'DXC']);
dyc=rdmds([chdatapath,'DYC']);
dxg=rdmds([chdatapath,'DXG']);
dyg=rdmds([chdatapath,'DYG']);


xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);
xg=rdmds([chdatapath,'XG']);
yg=rdmds([chdatapath,'YG']);

rAc=rdmds([chdatapath,'RAC']);
rAz=rdmds([chdatapath,'RAZ']);
rAs=rdmds([chdatapath,'RAS']);
rAw=rdmds([chdatapath,'RAW']); 

rC=rdmds([chdatapath,'RC']);
rF=rdmds([chdatapath,'RF']);
drC=rdmds([chdatapath,'DRC']);
drF=rdmds([chdatapath,'DRF']);

hFacC=rdmds([chdatapath,'hFacC']);
hFacW=rdmds([chdatapath,'hFacW']);
hFacS=rdmds([chdatapath,'hFacS']);

% load physical parameters for the model

if cas==1
   rsphere=6378.0e3;
   atm_cp=1002.0;
   gravity=9.81;
   omega=2*pi/86400;
   day=timed*dt/86400;

elseif cas==2
   rsphere=25559.0e3;
   gravity=7.77;
   atm_cp=13000.0;
   omega=2*pi/62064;
   day=timed*dt/86400;

elseif cas==3
   rsphere=24746.0e3;
   gravity=11.7;
   atm_cp=13000.0;
   omega=2*pi/62064;
   day=timed*dt/86400;

elseif cas==4
   rsphere=71492.0e3;
   gravity=22.88;
   atm_cp=13000.0;
   omega=2*pi/35730;
   day=timed*dt/86400;
elseif cas==5
   rsphere=60268.0E3;
   gravity=8.96;
   atm_cp=13000.0;
   omega=2*pi/37920;
   day=timed*dt/86400;
else
   'no options';
end;

% define vertical grids and reference temperature

rC = squeeze(rC);  % pc is at the center of the vertical grids.
drF = -squeeze(drF); % dp is the steps of the vertical grids, starts from lower boundary dp<0.
drC = -squeeze(drC);
rF = squeeze(rF); % pf is the pressure at faces of vertical grid.

g2d(:,:,1) = xc;
g2d(:,:,2) = yc;
g2d(:,:,3) = xg;
g2d(:,:,4) = yg;
g2d(:,:,5) = dxc;
g2d(:,:,6) = dyc;
g2d(:,:,7) = dxg;
g2d(:,:,8) = dyg;
g2d(:,:,9) = rAc;
g2d(:,:,10) = rAz;
g2d(:,:,11) = rAs;
g2d(:,:,12) = rAw;

