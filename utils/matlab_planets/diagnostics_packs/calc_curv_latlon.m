function [d2udy2,beta]=calc_curv_latlon(dyn3d,g2d,omega)

u3d=squeeze(dyn3d(:,:,:,1));
v3d=squeeze(dyn3d(:,:,:,2));

xc=squeeze(g2d(:,:,1));
yc=squeeze(g2d(:,:,2));
xg=squeeze(g2d(:,:,3));
yg=squeeze(g2d(:,:,4));
dxc=squeeze(g2d(:,:,5));
dyc=squeeze(g2d(:,:,6));

dims=size(u3d); nx=dims(1); ny=dims(2); nr=dims(3);

d2udy2=zeros(nx,ny,nr);

% cell centered in y direction

for k=1:nr
 d2udy2(:,2:ny-1,k)=( u3d(:,1:ny-2,k)+u3d(:,3:ny,k)-2.0*u3d(:,2:ny-1,k) ) ...
                    ./dyc(:,2:ny-1).^2.0;
end;

% calculate beta=df/dy

f=2.0*omega*sin(yg*pi/180.0);

beta(1:ny-1) = reshape( ( f(1,2:ny)-f(1,1:ny-1) )./dyc(1,1:ny-1) ,[ny-1 1]);

beta(ny)=beta(1);

figure;
pcolor(xc,yc,dyc);

