function [dyn_zon]=calc_zon_mean_latlon(dyn3d)

u3d=squeeze(dyn3d(:,:,:,1));
v3d=squeeze(dyn3d(:,:,:,2));
w3d=squeeze(dyn3d(:,:,:,3));
t3d=squeeze(dyn3d(:,:,:,4));
s3d=squeeze(dyn3d(:,:,:,5));

dims=size(u3d); nx=dims(1); ny=dims(2); nr=dims(3);

u2d=squeeze( sum(u3d,1) )/nx;
v2d=squeeze( sum(v3d,1) )/nx;
w2d=squeeze( sum(w3d,1) )/nx;
t2d=squeeze( sum(t3d,1) )/nx;
s2d=squeeze( sum(s3d,1) )/nx;

dyn_zon=zeros(ny,nr,5);

dyn_zon(:,:,1)=u2d;
dyn_zon(:,:,2)=v2d;
dyn_zon(:,:,3)=w2d;
dyn_zon(:,:,4)=t2d;
dyn_zon(:,:,5)=s2d;


