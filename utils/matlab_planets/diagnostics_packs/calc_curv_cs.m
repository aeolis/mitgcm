function [d2udy2,beta,xi,yi]=calc_curv_cs(dyn3d,g2d,omega,rsphere);

ny=256;

u3d=squeeze(dyn3d(:,:,:,1));
v3d=squeeze(dyn3d(:,:,:,2));

xc=squeeze(g2d(:,:,1));
yc=squeeze(g2d(:,:,2));
xg=squeeze(g2d(:,:,3));
yg=squeeze(g2d(:,:,4));
dxg=squeeze(g2d(:,:,7));
dyg=squeeze(g2d(:,:,8));
rAc=squeeze(g2d(:,:,9));


dims=size(u3d); nx=dims(1); nc=dims(2); ncp=nc+1; n2p=nc+2; nPg=nx*nc ;
if nx == 6*nc, flag=1; else flag=0; end
if size(v3d) ~= dims, flag=0; end
if flag == 0,
 fprintf(' Error in size of input arrays\n');
 vort=0; return
end
if length(dims) == 2, nr=1; else nr=prod(dims(3:end)); end

dxr=180.0/ny;

xi=-180+dxr/2:dxr:180-dxr/2;
yi=-90+dxr/2:dxr:90-dxr/2;

lx=length(xi);
ly=length(yi);

[del] = cube2latlon_preprocess(xc,yc,xi,yi);

[u3d,v3d]=uvcube2latlongrid(del,u3d,v3d,xg,yg,rAc,dxg,dyg);

dyc=rsphere*dxr*pi/180.0

d2udy2=zeros(lx,ly,nr);

% cell centered in y direction

for k=1:nr
 d2udy2(:,2:ly-1,k)=( u3d(:,1:ly-2,k)+u3d(:,3:ly,k)-2.0*u3d(:,2:ly-1,k) ) ... 
                    /dyc^2.0;
end;

% calculate beta=df/dy

yf=-90:dxr:90;

f=2.0*omega*sin(yf*pi/180.0);

beta = ( f(2:ly+1)-f(1:ly) )/dyc;

