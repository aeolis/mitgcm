function [dyn_zon,ylatc]=calc_zon_mean_cs(dyn3d,g2d,hFacC);

ny=36;

u3d=squeeze(dyn3d(:,:,:,1));
v3d=squeeze(dyn3d(:,:,:,2));
w3d=squeeze(dyn3d(:,:,:,3));
t3d=squeeze(dyn3d(:,:,:,4));
s3d=squeeze(dyn3d(:,:,:,5));

xc=squeeze(g2d(:,:,1));
yc=squeeze(g2d(:,:,2));
xg=squeeze(g2d(:,:,3));
yg=squeeze(g2d(:,:,4));
dxc=squeeze(g2d(:,:,5));
dyc=squeeze(g2d(:,:,6));
dxg=squeeze(g2d(:,:,7));
dyg=squeeze(g2d(:,:,8));
rAc=squeeze(g2d(:,:,9));
rAz=squeeze(g2d(:,:,10));
rAs=squeeze(g2d(:,:,11));
rAw=squeeze(g2d(:,:,12));

[AngleCS,AngleSN] = cubeCalcAngle(yg,rAc,dxg,dyg);

dims=size(u3d); nx=dims(1); nc=dims(2); ncp=nc+1; n2p=nc+2; nPg=nx*nc ;
if nx == 6*nc, flag=1; else flag=0; end
if size(v3d) ~= dims, flag=0; end
if size(dxc) ~= dims(1:2), flag=0; end
if size(dyc) ~= dims(1:2), flag=0; end
if flag == 0,
 fprintf(' Error in size of input arrays\n');
 vort=0; return
end
if length(dims) == 2, nr=1; else nr=prod(dims(3:end)); end


[uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C');

[u2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(uE,ny,yc,rAc,hFacC);
[v2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(vN,ny,yc,rAc,hFacC);

ylatc=ylat;

[t2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(t3d,ny,yc,rAc,hFacC);
[w2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(w3d,ny,yc,rAc,hFacC);
[s2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(s3d,ny,yc,rAc,hFacC);

dyn_zon=zeros(ny,nr,5);

dyn_zon(:,:,1)=u2d;
dyn_zon(:,:,2)=v2d;
dyn_zon(:,:,3)=w2d;
dyn_zon(:,:,4)=t2d;
dyn_zon(:,:,5)=s2d;


fprintf('terms in dyn_zon [ %d x %d x (%d terms) ]:  \n',ny,nr,5);
fprintf('term 1 : zonal-mean zonal winds \n');
fprintf('term 2 : zonal-mean meridional winds \n');
fprintf('term 3 : zonal-mean vertical flow \n');
fprintf('term 4 : zonal-mean potential temperature \n');
fprintf('term 5 : zonal-mean specific humidity \n');

