function [conti_3d,conti_uv]=check_conti_latlon(dyn3d,g2d,drF);

% check_conti_latlon calculates the continuity euqation du/dx+dv/dy+dw/dp
% on spherical-polar grid.
% it calculates the continuity equation for both a whole sphere or a channel.
% 
% conti_3d du/dx+dv/dy+dwdp
% conti_uv du/dx+dv/dy

% add one extra grid points in x direction for u (native) 
% add one extra grid points in y direction for v (extrapolate), currently 
% ignore the grid point at north boundary
% add one extra grid points in z direction for w (native)

u3d=squeeze(dyn3d(:,:,:,1));
v3d=squeeze(dyn3d(:,:,:,2));
w3d=squeeze(dyn3d(:,:,:,3));

dxg=squeeze(g2d(:,:,7));
dyg=squeeze(g2d(:,:,8));
rAc=squeeze(g2d(:,:,9));

dims=size(u3d); nx=dims(1); ny=dims(2); 
if length(dims) == 2, nr=1; else nr=prod(dims(3:end)); end;

u3d(nx+1,:,:)=u3d(1,:,:);
v3d(:,ny+1,:)=2.0*v3d(:,ny,:)-v3d(:,ny-1,:);
w3d(:,:,nr+1)=0.0;

utrans=zeros(nx+1,ny,nr);
vtrans=zeros(nx,ny+1,nr);

for k=1:nr,
 utrans([1:nx],:,k)=u3d([1:nx],:,k).*dyg;
 vtrans(:,[1:ny],k)=v3d(:,[1:ny],k).*dxg;

 utrans(nx+1,:,k)=reshape(  ...
                 (squeeze(u3d(nx+1,:,k)).*squeeze(dxg(1,:))), [1 ny 1]);
end;

conti_uv=zeros(nx,ny,nr);

for k=1:nr,
 conti_uv(:,:,k)=(utrans([2:nx+1],:,k)-utrans([1:nx],:,k)+ ... 
                  vtrans(:,[2:ny+1],k)-vtrans(:,[1:ny],k))./rAc;
end;

dwdp=zeros(nx,ny,nr);

for k=1:nr,
 dwdp(:,:,k)=(w3d(:,:,k+1)-w3d(:,:,k))/drF(k);
end;
 

conti_3d=conti_uv+dwdp;

% filling the north and east boundaries with NaNs
conti_uv(nx,:,:)=NaN;
conti_uv(:,ny,:)=NaN;

conti_3d(nx,:,:)=NaN;
conti_3d(:,ny,:)=NaN;


