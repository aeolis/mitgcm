function [hr,dhdt,gammaw,vdtdy,dvtdy,dwtdp,cwt,ylatc]=calc_eddy_heat_cs(dyn3d,Ttrans3d,grid2d,drC,drF,tRef,rC,rF,tau,tauc,kappa,hFacC,rsphere);

% the heating equation follows Karoly (1998)

% the following terms are time-averaged and zonal-mean quantities
% hr -- total heating rate
% dhdt -- diabatic heating
% gamw -- vertical advection of heat
% vdtdy -- horizontal advection of heat
% dvtdy -- horizontal convergence of eddy heat heat flux
% dwtdp -- vertical convergence of eddy heat flux


% cwt -- vertical eddy heat flux (extra terms associated with dwtdp)
% tau -- rediative cooling time scale, in seconds
% tauc -- condensation time scale, in seconds

% ny --- number of grid points (lines) in latitude, required for zonal mean 

ny=256;

u3d=squeeze(dyn3d(:,:,:,1));
v3d=squeeze(dyn3d(:,:,:,2));
w3d=squeeze(dyn3d(:,:,:,3));
t3d=squeeze(dyn3d(:,:,:,4));
s3d=squeeze(dyn3d(:,:,:,5));

ut3d=squeeze(Ttrans3d(:,:,:,1));
vt3d=squeeze(Ttrans3d(:,:,:,2));
wt3d=squeeze(Ttrans3d(:,:,:,3));

xc=squeeze(grid2d(:,:,1));
yc=squeeze(grid2d(:,:,2));
xg=squeeze(grid2d(:,:,3));
yg=squeeze(grid2d(:,:,4));
dxc=squeeze(grid2d(:,:,5));
dyc=squeeze(grid2d(:,:,6));
dxg=squeeze(grid2d(:,:,7));
dyg=squeeze(grid2d(:,:,8));
rAc=squeeze(grid2d(:,:,9));
rAz=squeeze(grid2d(:,:,10));
rAs=squeeze(grid2d(:,:,11));
rAw=squeeze(grid2d(:,:,12));

[AngleCS,AngleSN] = cubeCalcAngle(yg,rAc,dxg,dyg);

check_var=input('caution: must use time average variables !!!, press 1 to contiue  ');

if (check_var ~= 1)
   error('Stop, use time averaged variables');
end;

dims=size(u3d); nx=dims(1); nc=dims(2); ncp=nc+1; n2p=nc+2; nPg=nx*nc ;
if nx == 6*nc, flag=1; else flag=0; end
if size(v3d) ~= dims, flag=0; end
if size(dxc) ~= dims(1:2), flag=0; end
if size(dyc) ~= dims(1:2), flag=0; end
if flag == 0,
 fprintf(' Error in size of input arrays\n');
 vort=0; return
end
if length(dims) == 2, nr=1; else nr=prod(dims(3:end)); end


rF(nr+1)=0.0;

u3d=reshape(u3d,[nx nc nr]);
v3d=reshape(v3d,[nx nc nr]);
w3d=reshape(w3d,[nx nc nr]);
t3d=reshape(t3d,[nx,nc,nr]);

ut3d=reshape(ut3d,[nx nc nr]);
vt3d=reshape(vt3d,[nx nc nr]);
wt3d=reshape(wt3d,[nx nc nr]);

% filling t3d for missing corners, split_Z_cub will make
% t3d in form of [ncp,ncp,nr,6]

[u6t,v6t]=split_UV_cub(u3d,v3d,1,2);
[ut6t,vt6t]=split_UV_cub(ut3d,vt3d,1,2);
[d6x_c,d6y_c]=split_UV_cub(dxc,dyc,0,2);
[d6x_g,d6y_g]=split_UV_cub(dxg,dyg,0,2);
[c6x,c6y]=split_UV_cub(xc,yc,0,1);
w6t=split_Z_cub(w3d);
t6t=split_Z_cub(t3d);
s6t=split_Z_cub(s3d);
wt6t=split_Z_cub(wt3d);

if nr > 1,
  u6t=permute(u6t,[1 2 4 3]);
  v6t=permute(v6t,[1 2 4 3]);
  w6t=permute(w6t,[1 2 4 3]);
  t6t=permute(t6t,[1 2 4 3]);
  s6t=permute(s6t,[1 2 4 3]);
  ut6t=permute(ut6t,[1 2 4 3]);
  vt6t=permute(vt6t,[1 2 4 3]);
  wt6t=permute(wt6t,[1 2 4 3]);
end

%---use virtual potential temperature for diagnostics---

Rq=-0.8778;
t6t=t6t.*(Rq*s6t+1.0);

%---calculate diabatic heating (cell centerd)---d[h]/dt-----------------------------------------1

  RWV = 461.0;
  es0 = 609.140;
  T0ref = 273.0;
  LHWV = 2.5e6;
  pref = 1e5; % 1-bar
  Cp   = 13000.0;

realT=zeros(ncp,ncp,6,nr);
  
for k=1:nr,
   realT(:,:,:,k)=(rC(k)/pref)^kappa*t6t(:,:,:,k);
end;

esT =es0*exp(-LHWV/RWV*(1.0./realT-1.0/T0ref));

qs       = zeros(ncp,ncp,6,nr);
delta    = zeros(ncp,ncp,6,nr);
latentWV = zeros(ncp,ncp,6,nr);

for k=1:nr, qs(:,:,:,k)=9*esT(:,:,:,k)/rC(k); end;

for i=1:ncp, for j=1:ncp, for tn=1:6, for k=1:nr,
   if s6t(i,j,tn,k)>=qs(i,j,tn,k) delta(i,j,tn,k)=1; end;
end; end; end; end;

for k=1:nr,
   latentWV(:,:,:,k)=LHWV/Cp*delta(:,:,:,k).*(s6t(:,:,:,k)-qs(:,:,:,k))/tauc*(pref/rC(k))^kappa;
end;

dhdt=zeros(ncp,ncp,6,nr);

for k=1:nr,
    dhdt(:,:,:,k)=( -(t6t(:,:,:,k)-tRef(k))/tau+latentWV(:,:,:,k) )*(rC(k)/pref)^kappa;
end;

dhdt=permute(dhdt([1:nc],[1:nc],[1:6],[1:nr]),[1 3 2 4]);

dhdt=reshape(dhdt,6*nc,nc,nr);

[dhdt,mskzon,ylat,areazon] = calcZonalAvgCubeC(dhdt,ny,yc,rAc,hFacC);

clear realT latentWV delta qs esT s6t;

%---calculate vertical advection of heat (cell centered)---gamma*[w]-------------------------------2

Gamma=zeros(ny,nr);
gamma_w_F=zeros(ny,nr);

% -- t2d at the center of the cell
[t2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(t3d,ny,yc,rAc,hFacC);

ylatc=ylat; % ylatc is to be returned as a result.

for k=1:nr,
 km1 = max(k-1,1);
 Gamma(:,k)=-(rF(k)/pref)^kappa*(t2d(:,k)-t2d(:,km1))/drC(k);
end;

[w2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(w3d,ny,yc,rAc,hFacC);
gamma_w_F=Gamma.* w2d;
gamma_w_F(:,nr+1)=0.0;

% move to cell center (in vertical)

gammaw=zeros(ny,nr);
gammaw(:,[1:nr])=( gamma_w_F(:,[1:nr])+gamma_w_F(:,[2:nr+1]) )/2.0;


%---calculate horizontal advection of eddy heat (cell centered)---[v]*d[T]/dy------------------------3

vdtdy_v=zeros(ny,nr);

[uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C');
% -- v2d at the center of the cell
[v2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(vN,ny,yc,rAc,hFacC);

% -- t2d at the north/south edges of the cell
[t2d,mskzon,ylat,areazon] = calcZonalAvgCubeG(t3d,ny,yc,rAc,hFacC);

dyc_latlon(1:ny)=pi/ny*rsphere;

for k=1:nr,
 vdtdy([2:ny],k)=v2d([2:ny],k).* (t2d([2:ny],k)-t2d([1:ny-1],k))./dyc_latlon(2:ny)'*(rC(k)/pref)^kappa;
end;

% filling vdtdy(1,:) and vdtdy(ny,:) with NaNs

vdtdy(1,:)=NaN; vdtdy(ny,:)=NaN;

%---calculate horizontal eddy heat flux d/dy([v'T']+[v*T*])---use Physics of climate ---[v'T']+[v*T*]=[vT]-[v][T]----------4

vt_prime=zeros(nc,nc,6,nr);
ut_prime=zeros(nc,nc,6,nr);
t6t_v=zeros(nc,nc,6,nr);
t6t_u=zeros(nc,nc,6,nr);

% theta at v, lined up with v(:,[2:ncp],:,:)
t6t_v(:,:,:,:)=(t6t([1:nc],[1:nc],:,:)+t6t([1:nc],[2:ncp],:,:))/2.0;
t6t_u(:,:,:,:)=(t6t([1:nc],[1:nc],:,:)+t6t([2:ncp],[1:nc],:,:))/2.0;

vt_prime=vt6t([1:nc],[2:ncp],:,:)-v6t([1:nc],[2:ncp],:,:).*t6t_v(:,:,:,:);
ut_prime=ut6t([2:ncp],[1:nc],:,:)-u6t([2:ncp],[1:nc],:,:).*t6t_u(:,:,:,:);

ut_prime=permute(ut_prime,[1 3 2 4]);
vt_prime=permute(vt_prime,[1 3 2 4]);
ut_prime=reshape(ut_prime,6*nc,nc,nr);
vt_prime=reshape(vt_prime,6*nc,nc,nr);

[ut_prime,vt_prime]=split_UV_cub(ut_prime,vt_prime,1,2);

vt_prime=permute(vt_prime,[1 2 4 3]);

% d/dy([v'T']+[v*T*]) (cell centered)

dvtdy=zeros(nc,nc,6,nr);

for k=1:nr,
 dvtdy([1:nc],[1:nc],:,k)=(vt_prime([1:nc],[2:ncp],:,k)-vt_prime([1:nc],[1:nc],:,k))./d6y_c([1:nc],[2:ncp],:)*(rC(k)/pref)^kappa;;
end;

%---calculate horizontal eddy heat flux d/dp([w'T']+[w*T*])---use Physics of climate ---[w'T']+[w*T*]=[wT]-[w][T]----------5


% -- t at the center of the cell (center of the vertical grid)
t6t_w=zeros(ncp,ncp,6,nr);

for k=1:nr,
 km1 = max(k-1,1);
 t6t_w(:,:,:,k)=(t6t(:,:,:,k)+t6t(:,:,:,km1))/2.0;
end;

wt_prime=wt6t-w6t.*t6t_w;

% d/dp([w'T']+[w*T*]) (cell centered)

wt_prime(:,:,:,nr+1)=0.0;
dwtdp=zeros(ncp,ncp,6,nr);

for k=1:nr,
 dwtdp(:,:,:,k)=( wt_prime(:,:,:,k+1)*(rF(k+1)/pref)^kappa-wt_prime(:,:,:,k)*(rF(k)/pref)^kappa )/drF(k);
end;


%---calculate kappa/p*([w'T']+[w*T*])-----------------------------------------------------------------------------6

cwt=zeros(ncp,ncp,6,nr);

for k=1:nr,
 cwt(:,:,:,k)=kappa/rC(k)*( wt_prime(:,:,:,k+1)*(rF(k+1)/pref)^kappa+wt_prime(:,:,:,k)*(rF(k)/pref)^kappa )/2.0;
end;

%---calculate the total heating rate

hr=zeros(ny,nr);

%hr=dhdt+gammaw-vdtdy-cwt;

