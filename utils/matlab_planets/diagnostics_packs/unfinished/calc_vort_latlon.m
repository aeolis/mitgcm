function [vort_r,vort_p,vort_ipv]=calc_vort_latlon(dyn3d,omega,gravity,g2d,drF);

% check_conti_latlon calculates the continuity euqation du/dx+dv/dy+dw/dp
% on spherical-polar grid.
% it calculates the continuity equation for both a whole sphere or a channel.

% vort_r : relative vorticity (cell centered)
% vort_p : planetary vorticity (cell centered)
% vort_ipv : isobaric potential vorticity (following Nakamura and Wallace (1993))

% add one extra grid point in x direction for v (native) 
% add one extra grid point in y direction for u (extrapolate), currently 
% ignore the grid point at north boundary
% add one extra grid point in x and one in y for theta 

u3d=squeeze(dyn3d(:,:,:,1));
v3d=squeeze(dyn3d(:,:,:,2));
w3d=squeeze(dyn3d(:,:,:,3));
t3d=squeeze(dyn3d(:,:,:,4));

xc=squeeze(g2d(:,:,1));
yc=squeeze(g2d(:,:,2));
xg=squeeze(g2d(:,:,3));
yg=squeeze(g2d(:,:,4));
dxc=squeeze(g2d(:,:,5));
dyc=squeeze(g2d(:,:,6));
dxg=squeeze(g2d(:,:,7));
dyg=squeeze(g2d(:,:,8));
rAc=squeeze(g2d(:,:,9));
rAz=squeeze(g2d(:,:,10));
rAs=squeeze(g2d(:,:,11));
rAw=squeeze(g2d(:,:,12));
AngleCS=squeeze(g2d(:,:,13));
AngleSN=squeeze(g2d(:,:,14));

knorm=0;

dims=size(u3d); nx=dims(1); ny=dims(2); 
if length(dims) == 2, nr=1; else nr=prod(dims(3:end)); end;

v3d(nx+1,:,:)=v3d(1,:,:);
u3d(:,ny+1,:)=2.0*u3d(:,ny,:)-u3d(:,ny-1,:);

utrans=zeros(nx,ny+1,nr);
vtrans=zeros(nx+1,ny,nr);

for k=1:nr,
 utrans(:,[1:ny],k)=u3d(:,[1:ny],k).*dxc;
 vtrans([1:nx],:,k)=v3d([1:nx],:,k).*dyc;

 vtrans(nx+1,:,k)=reshape(  ...
                 (squeeze(v3d(nx+1,:,k)).*squeeze(dyc(1,:))), [1 ny 1]);
end;

% calculate relative vorticity
vort_r=zeros(nx,ny,nr);

for k=1:nr,
 vort_r(:,:,k)= ( (vtrans([2:nx+1],:,k)-vtrans([1:nx],:,k))- ... 
                  (utrans(:,[2:ny+1],k)-utrans(:,[1:ny],k)) )./rAz;
end;


% filling the north and east boundaries with NaNs
vort_r(nx,:,:)=NaN;
vort_r(:,ny,:)=NaN;

% calculate isobaric potential vorticity (cell centered)

vort_p=2.0*omega*sin(yc*pi/180.0);

t3d(nx+1,:,:)=t3d(1,:,:);
t3d(:,ny+1,:)=2.0*t3d(:,ny,:)-t3d(:,ny-1,:);

dtdx=zeros(nx,ny,nr); % at u
dtdy=zeros(nx,ny,nr); % at v

for k=1:nr,
 dtdx(:,:,k)=(t3d([2:nx+1],[1:ny],k)-t3d([1:nx],[1:ny],k))./dxg;
 dtdy(:,:,k)=(t3d([1:nx],[2:ny+1],k)-t3d([1:nx],[1:ny],k))./dyg;
end;

uvt=zeros(nx,ny,nr,3); % merge u3d v3d and t3d to one 4-D array

uvt(:,:,:,1)=u3d([1:nx],[1:ny],[1:nr]);
uvt(:,:,:,2)=v3d([1:nx],[1:ny],[1:nr]);
uvt(:,:,:,3)=t3d([1:nx],[1:ny],[1:nr]);

% interpolate uvt to faces of the vertical grid, prepare for d/dp

% first interpolate between layers 2:nr

uvt_f=zeros(nx,ny,nr,3); % u 1, v 2, t 3.

for k=1:nr
 km1 = max(k-1,1);
 uvt_f(:,:,k,:)=(uvt(:,:,km1,:)+uvt(:,:,k,:))/2.0;
end;

% extrapolate uvt at k=nr+1 (use k=nr)

uvt_f(:,:,nr+1,:)=2.0*uvt(:,:,nr,:)-uvt_f(:,:,nr,:);

% calculate d/dp of u v t

duvt_dp=zeros(nx,ny,nr,3);

for k=1:nr,
 duvt_dp(:,:,k,:)=(uvt_f(:,:,k+1,:)-uvt_f(:,:,k,:))/drF(k);
end;

% move dtdx, dtdy and duvt_dp(:,:,:,1:2) to cell center

dtdx_c=zeros(nx,ny,nr);
dtdy_c=zeros(nx,ny,nr);

du_dp_c=zeros(nx,ny,nr);
dv_dp_c=zeros(nx,ny,nr);

for k=1:nr,
 dtdx_c([2:nx],:,k)=(dtdx([1:nx-1],:,k)+dtdx([2:nx],:,k))/2.0;
 dtdx_c(1,:,k)=(dtdx(nx,:,k)+dtdx(1,:,k))/2.0;
end;

for k=1:nr,
 dtdy_c(:,[2:ny],k)=(dtdy(:,[1:ny-1],k)+dtdy(:,[2:ny],k))/2.0;
end;

% filling dtdy_c(:,1,:) with NaNs

dtdy_c(:,1,:)=NaN;

for k=1:nr,
 du_dp_c([2:nx],:,k)=(duvt_dp([1:nx-1],:,k,1)+duvt_dp([2:nx],:,k,1))/2.0;
 du_dp_c(1,:,k)=(duvt_dp(nx,:,k,1)+duvt_dp(1,:,k,1))/2.0;
end;

for k=1:nr,
 dv_dp_c(:,[2:ny],k)=(duvt_dp(:,[1:ny-1],k,2)+duvt_dp(:,[2:ny],k,2))/2.0;
end;

% filling dv_dp_c(:,ny,:) with NaNs

dv_dp_c(:,1,:)=NaN;


vt_px=zeros(nx,ny,nr);
ut_py=zeros(nx,ny,nr);

vt_px=dv_dp_c.*dtdx_c;
ut_py=du_dp_c.*dtdy_c;

for k=1:nr,
 vort_ipv(:,:,k)=gravity*(vt_px(:,:,k)-ut_py(:,:,k)+(vort_r(:,:,k)+vort_p).*(-duvt_dp(:,:,k,3)));
end;

if knorm==1
   dt_dp=sum( squeeze(sum(duvt_dp(:,:,:,3),1))/nx,1 )/ny;
   for k=1:nr,
   vort_ipv(:,:,k)=vort_ipv(:,:,k)/dt_dp(k);
   end;
end;





