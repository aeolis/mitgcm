function [vort_r,vort_p,vort_ipv,z6t_r,z6t_p,z6t_ipv]=calc_vort_cs(dyn3d,g2d,omega,gravity,drF,Grid);
% [vort,z6t]=calc_vort_cs(u3d,v3d,dxc,dyc,rAz);
% vort_r and z6t_r are relative vorticity.
% compute vorticity (3rd component) on CS-grid.
%  assume u3d(nc*6,nc,*), v3d(nc*6,nc,*), dxc(nc*6,nc), dyc(nc*6,nc)
%    and deal with: rAz(nc*6,nc) or rAz(nc*6*nc+2)
%  output is provided with 2 shapes:
%   vort(nc*6*nc+2,*) = compressed form;
%   z6t(nc+1,nc+1,*,6) = face splitted.
%
% vort_r relative vorticity
% vort_p planetary vorticity
% vort_ipv isobaric potential vorticity (following Nakamura and Wallace (1993))
%
% Written by jmc@ocean.mit.edu, 2005.
% $Header: /u/gcmpack/MITgcm/utils/matlab/cs_grid/calc_vort_cs.m,v 1.2 2005/10/06 01:16:44 jmc Exp $
% $Name:  $

u3d=squeeze(dyn3d(:,:,:,1));
v3d=squeeze(dyn3d(:,:,:,2));
w3d=squeeze(dyn3d(:,:,:,3));
t3d=squeeze(dyn3d(:,:,:,4));

xc=squeeze(g2d(:,:,1));
yc=squeeze(g2d(:,:,2));
xg=squeeze(g2d(:,:,3));
yg=squeeze(g2d(:,:,4));
dxc=squeeze(g2d(:,:,5));
dyc=squeeze(g2d(:,:,6));
dxg=squeeze(g2d(:,:,7));
dyg=squeeze(g2d(:,:,8));
rAc=squeeze(g2d(:,:,9));
rAz=squeeze(g2d(:,:,10));
rAs=squeeze(g2d(:,:,11));
rAw=squeeze(g2d(:,:,12));

[AngleCS,AngleSN] = cubeCalcAngle(yg,rAc,dxg,dyg);


dims=size(u3d); nx=dims(1); nc=dims(2); ncp=nc+1; n2p=nc+2; nPg=nx*nc ;
if nx == 6*nc, flag=1; else flag=0; end
if size(v3d) ~= dims, flag=0; end
if size(dxc) ~= dims(1:2), flag=0; end
if size(dyc) ~= dims(1:2), flag=0; end
if flag == 0,
 fprintf(' Error in size of input arrays\n');
 vort=0; return
end
if length(dims) == 2, nr=1; else nr=prod(dims(3:end)); end

siZ=prod(size(rAz));
if siZ == nPg+2,
 rAz=reshape(rAz,[nPg+2 1]);
 aZ=split_Z_cub(rAz);
elseif siZ == nPg,
 rAz=reshape(rAz,[nPg 1]); 
 aZc=zeros(nPg+2,1); aZc(1:nPg,:)=rAz; aZc(nPg+1)=rAz(1); aZc(nPg+2)=rAz(1);
 aZ=split_Z_cub(aZc);
else
 fprintf(' Error in size of rAz input array\n');
 vort=0; return
end

%--------------calculate relative vorticity----------------------------

u3d=reshape(u3d,[nx nc nr]);
v3d=reshape(v3d,[nx nc nr]);
t3d=reshape(t3d,[nx,nc,nr]);

% filling t3d for missing corners, split_Z_cub will make 
% t3d in form of [ncp,ncp,nr,6]

[u6t,v6t]=split_UV_cub(u3d,v3d,1,2);
[d6x_c,d6y_c]=split_UV_cub(dxc,dyc,0,2);
[d6x_g,d6y_g]=split_UV_cub(dxg,dyg,0,2);
[c6x,c6y]=split_UV_cub(xc,yc,0,1);
t6t=split_Z_cub(t3d);
if nr > 1, 
  u6t=permute(u6t,[1 2 4 3]);
  v6t=permute(v6t,[1 2 4 3]);
  t6t=permute(t6t,[1 2 4 3]);
end 
z6t_r=zeros(ncp,ncp,6,nr);

for k=1:nr,
 vv1=d6x_c.*u6t(:,:,:,k);
 vv2=d6y_c.*v6t(:,:,:,k);
 z6t_r(:,:,:,k)=vv2([2:n2p],:,:)-vv2([1:ncp],:,:);
 z6t_r(:,:,:,k)=z6t_r(:,:,:,k)-(vv1(:,[2:n2p],:)-vv1(:,[1:ncp],:));
%- corner: the quick way:
% z6t_r(1,1,:,k)   = vv2(2,1,:)  -vv2(1,1,:)  -vv1(1,2,:);
% z6t_r(1,ncp,:,k) = vv2(2,ncp,:)-vv2(1,ncp,:)+vv1(1,ncp,:);
% z6t_r(ncp,1,:,k) = vv2(n2p,1,:)-vv2(ncp,1,:)-vv1(ncp,2,:);
% z6t_r(ncp,ncp,:,k)=vv2(n2p,ncp,:)-vv2(ncp,ncp,:)+vv1(ncp,ncp,:);
%- corner: add the 3 terms always in the same order 
%   to get the same truncation on the 3 faces
 for n=1:3,
   f=2*n-1; %- odd face number
   vc=-vv2(1,1,f); %- S-W corner
   z6t_r(1,1,f,k)   = (vv2(2,1,f)-vv1(1,2,f))+vc;
   vc=+vv2(n2p,1,f); %- S-E corner
   z6t_r(ncp,1,f,k) = (vc-vv1(ncp,2,f))-vv2(ncp,1,f);
   vc=+vv2(n2p,ncp,f); %- N-E corner
   z6t_r(ncp,ncp,f,k)=(vc-vv2(ncp,ncp,f))+vv1(ncp,ncp,f);
   vc=-vv2(1,ncp,f); %- N-W corner
   vc3=[vc vv2(2,ncp,f) vv1(1,ncp,f) vc vv2(2,ncp,f)];
   z6t_r(1,ncp,f,k) = (vc3(n+2)+vc3(n+1))+vc3(n);
   f=2*n; %- even face number
   vc=-vv2(1,1,f); %- S-W corner
   z6t_r(1,1,f,k)   = (vv2(2,1,f)-vv1(1,2,f))+vc;
   vc=+vv2(n2p,1,f); %- S-E corner
   vc3=[-vv1(ncp,2,f) -vv2(ncp,1,f) vc -vv1(ncp,2,f) -vv2(ncp,1,f)];
   z6t_r(ncp,1,f,k) = (vc3(n)+vc3(n+1))+vc3(n+2);
   vc=+vv2(n2p,ncp,f); %- N-E corner
   z6t_r(ncp,ncp,f,k)=(vv1(ncp,ncp,f)+vc)-vv2(ncp,ncp,f);
   vc=-vv2(1,ncp,f); %- N-W corner
   z6t_r(1,ncp,f,k) = (vv2(2,ncp,f)+vc)+vv1(1,ncp,f);
 end
%- divide by rAz:
 z6t_r(:,:,:,k)=z6t_r(:,:,:,k)./aZ;
end

%-----------------------calculate planetary vorticity----------------------

z6t_p=squeeze( 2.0*omega*sin(c6y*pi/180.0) );

%-----------------------calculate isobaric potential vorticity-------------

%----1----calculate values at faces of vertical grid for using of d/dp

u6t_f=zeros(ncp,n2p,6,nr); 
v6t_f=zeros(n2p,ncp,6,nr);
t6t_f=zeros(ncp,ncp,6,nr);

for k=1:nr
 km1 = max(k-1,1);
 u6t_f(:,:,:,k)=(u6t(:,:,:,km1)+u6t(:,:,:,k))/2.0;
 v6t_f(:,:,:,k)=(v6t(:,:,:,km1)+v6t(:,:,:,k))/2.0;
 t6t_f(:,:,:,k)=(t6t(:,:,:,km1)+t6t(:,:,:,k))/2.0;
end;

%--- upper boundaries---
u6t_f(:,:,:,nr+1)=2.0*u6t(:,:,:,nr)-u6t_f(:,:,:,nr);
v6t_f(:,:,:,nr+1)=2.0*v6t(:,:,:,nr)-v6t_f(:,:,:,nr);
t6t_f(:,:,:,nr+1)=2.0*t6t(:,:,:,nr)-t6t_f(:,:,:,nr);

%----calculate d/dp-------

for k=1:nr,
 du6t_dp(:,:,:,k)=(u6t_f(:,:,:,k+1)-u6t_f(:,:,:,k))/drF(k);
 dv6t_dp(:,:,:,k)=(v6t_f(:,:,:,k+1)-v6t_f(:,:,:,k))/drF(k);
 dt6t_dp(:,:,:,k)=(t6t_f(:,:,:,k+1)-t6t_f(:,:,:,k))/drF(k);
end;

%----calculate dt/dx and dt/dy---

dtdx=zeros(nc,nc,6,nr); % at u
dtdy=zeros(nc,nc,6,nr); % at v

for k=1:nr,
 dtdx(:,:,:,k)=(t6t([2:ncp],[1:nc],:,k)-t6t([1:nc],[1:nc],:,k))./d6x_g([1:nc],[1:nc],:);
 dtdy(:,:,:,k)=(t6t([1:nc],[2:ncp],:,k)-t6t([1:nc],[1:nc],:,k))./d6y_g([1:nc],[1:nc],:);
end;

%----convert du/dp, dv/dp, dt/dx and dt/dy to cell centers

du6t_dp=reshape(permute(du6t_dp(1:nc,1:nc,:,:),[1 3 2 4]),[nx nc nr]);
dv6t_dp=reshape(permute(dv6t_dp(1:nc,1:nc,:,:),[1 3 2 4]),[nx nc nr]);
dtdx=reshape(permute(dtdx(1:nc,1:nc,:,:),[1 3 2 4]),[nx nc nr]);
dtdy=reshape(permute(dtdy(1:nc,1:nc,:,:),[1 3 2 4]),[nx nc nr]);

[du6t_dp_c,dv6t_dp_c] = rotate_uv2uvEN(du6t_dp,dv6t_dp,AngleCS,AngleSN,Grid);
[dtdx_c,     dtdy_c ] = rotate_uv2uvEN(dtdx   ,dtdy,   AngleCS,AngleSN,Grid);

% convert them back to 6 tiles format

du6t_dp_c=permute(reshape(du6t_dp_c,[nc 6 nc nr]),[1 3 2 4]);
dv6t_dp_c=permute(reshape(dv6t_dp_c,[nc 6 nc nr]),[1 3 2 4]);
dtdx_c=permute(reshape(dtdx_c,[nc 6 nc nr]),[1 3 2 4]);
dtdy_c=permute(reshape(dtdy_c,[nc 6 nc nr]),[1 3 2 4]);

%---calculate IPV terms--------------

for k=1:nr,
 z6t_ipv(:,:,:,k)=gravity*(dv6t_dp_c(:,:,:,k).*dtdx_c(:,:,:,k)-du6t_dp_c(:,:,:,k).*dtdy_c(:,:,:,k)) ... 
                  +(z6t_r([1:nc],[1:nc],:,k)+z6t_p([1:nc],[1:nc],:)).*(-dt6t_dp([1:nc],[1:nc],:,k));
end;


%- put in compressed form 

vort_p=  reshape(permute(z6t_p([1:nc],[1:nc],:), [1 3 2]), [nPg 1]);
vort_ipv=reshape(permute(z6t_ipv([1:nc],[1:nc],:,:),[1 3 2 4]),[nPg nr]);

z6t_ipv=permute(z6t_ipv,[1 2 4 3]);

%- put in compressed form: 
vort_r=zeros(nPg+2,nr);
%  extract the interior
 vort_r([1:nPg],:)=reshape(permute(z6t_r(1:nc,1:nc,:,:),[1 3 2 4]),[nPg nr]);
%  put back the 2 missing corners (N.W corner of 1rst face & S.E corner of 2nd face)
 vort_r(nPg+1,:)=z6t_r(1,ncp,1,:);
 vort_r(nPg+2,:)=z6t_r(ncp,1,2,:);
 
%- back into final shape:
z6t_r=permute(z6t_r,[1 2 4 3]);
if length(dims) == 2, 
 vort_r=squeeze(vort_r);
 z6t_r=squeeze(z6t_r);
else
 vort_r=reshape(vort_r,[nPg+2 dims(3:end)]);
 z6t_r=reshape(z6t_r,[ncp ncp dims(3:end) 6]);
end

%----------------- 
return
