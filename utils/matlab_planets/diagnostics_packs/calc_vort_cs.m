function [sp,rT3d_f,aT,vort_r,vort_p,qgpv,xi,yi]=calc_vort_cs(dyn3d,g2d,omega,gravity,kappa,rC,rF,drF,drC,Grid,hFacC);
% [vort,z6t]=calc_vort_cs(u3d,v3d,dxc,dyc,rAz);
% vort_r and z6t_r are relative vorticity.
% compute vorticity (3rd component) on CS-grid.
%  assume u3d(nc*6,nc,*), v3d(nc*6,nc,*), dxc(nc*6,nc), dyc(nc*6,nc)
%    and deal with: rAz(nc*6,nc) or rAz(nc*6*nc+2)
%  output is provided with 2 shapes:
%   vort(nc*6*nc+2,*) = compressed form;
%   z6t(nc+1,nc+1,*,6) = face splitted.
%
% vort_r relative vorticity
% vort_p planetary vorticity
% vort_ipv isobaric potential vorticity (following Nakamura and Wallace (1993))
%
% Written by jmc@ocean.mit.edu, 2005.
% $Header: /u/gcmpack/MITgcm/utils/matlab/cs_grid/calc_vort_cs.m,v 1.2 2005/10/06 01:16:44 jmc Exp $
% $Name:  $

pref = 1.0E5; % reference pressure at 1bar

u3d=squeeze(dyn3d(:,:,:,1));
v3d=squeeze(dyn3d(:,:,:,2));
w3d=squeeze(dyn3d(:,:,:,3));
t3d=squeeze(dyn3d(:,:,:,4));

xc=squeeze(g2d(:,:,1));
yc=squeeze(g2d(:,:,2));
xg=squeeze(g2d(:,:,3));
yg=squeeze(g2d(:,:,4));
dxc=squeeze(g2d(:,:,5));
dyc=squeeze(g2d(:,:,6));
dxg=squeeze(g2d(:,:,7));
dyg=squeeze(g2d(:,:,8));
rAc=squeeze(g2d(:,:,9));
rAz=squeeze(g2d(:,:,10));
rAs=squeeze(g2d(:,:,11));
rAw=squeeze(g2d(:,:,12));

[AngleCS,AngleSN] = cubeCalcAngle(yg,rAc,dxg,dyg);


dims=size(u3d); nx=dims(1); nc=dims(2); ncp=nc+1; n2p=nc+2; nPg=nx*nc ;
if nx == 6*nc, flag=1; else flag=0; end
if size(v3d) ~= dims, flag=0; end
if size(dxc) ~= dims(1:2), flag=0; end
if size(dyc) ~= dims(1:2), flag=0; end
if flag == 0,
 fprintf(' Error in size of input arrays\n');
 vort=0; return
end
if length(dims) == 2, nr=1; else nr=prod(dims(3:end)); end

siZ=prod(size(rAz));
if siZ == nPg+2,
 rAz=reshape(rAz,[nPg+2 1]);
 aZ=split_Z_cub(rAz);
elseif siZ == nPg,
 rAz=reshape(rAz,[nPg 1]); 
 aZc=zeros(nPg+2,1); aZc(1:nPg,:)=rAz; aZc(nPg+1)=rAz(1); aZc(nPg+2)=rAz(1);
 aZ=split_Z_cub(aZc);
else
 fprintf(' Error in size of rAz input array\n');
 vort=0; return
end

%--------------calculate relative vorticity----------------------------

u3d=reshape(u3d,[nx nc nr]);
v3d=reshape(v3d,[nx nc nr]);
t3d=reshape(t3d,[nx,nc,nr]);

% filling t3d for missing corners, split_Z_cub will make 
% t3d in form of [ncp,ncp,nr,6]

[u6t,v6t]=split_UV_cub(u3d,v3d,1,2);
[d6x_c,d6y_c]=split_UV_cub(dxc,dyc,0,2);
[d6x_g,d6y_g]=split_UV_cub(dxg,dyg,0,2);
[c6x,c6y]=split_UV_cub(xc,yc,0,1);
t6t=split_Z_cub(t3d);
if nr > 1, 
  u6t=permute(u6t,[1 2 4 3]);
  v6t=permute(v6t,[1 2 4 3]);
  t6t=permute(t6t,[1 2 4 3]);
end 
z6t_r=zeros(ncp,ncp,6,nr);

for k=1:nr,
 vv1=d6x_c.*u6t(:,:,:,k);
 vv2=d6y_c.*v6t(:,:,:,k);
 z6t_r(:,:,:,k)=vv2([2:n2p],:,:)-vv2([1:ncp],:,:);
 z6t_r(:,:,:,k)=z6t_r(:,:,:,k)-(vv1(:,[2:n2p],:)-vv1(:,[1:ncp],:));
%- corner: the quick way:
% z6t_r(1,1,:,k)   = vv2(2,1,:)  -vv2(1,1,:)  -vv1(1,2,:);
% z6t_r(1,ncp,:,k) = vv2(2,ncp,:)-vv2(1,ncp,:)+vv1(1,ncp,:);
% z6t_r(ncp,1,:,k) = vv2(n2p,1,:)-vv2(ncp,1,:)-vv1(ncp,2,:);
% z6t_r(ncp,ncp,:,k)=vv2(n2p,ncp,:)-vv2(ncp,ncp,:)+vv1(ncp,ncp,:);
%- corner: add the 3 terms always in the same order 
%   to get the same truncation on the 3 faces
 for n=1:3,
   f=2*n-1; %- odd face number
   vc=-vv2(1,1,f); %- S-W corner
   z6t_r(1,1,f,k)   = (vv2(2,1,f)-vv1(1,2,f))+vc;
   vc=+vv2(n2p,1,f); %- S-E corner
   z6t_r(ncp,1,f,k) = (vc-vv1(ncp,2,f))-vv2(ncp,1,f);
   vc=+vv2(n2p,ncp,f); %- N-E corner
   z6t_r(ncp,ncp,f,k)=(vc-vv2(ncp,ncp,f))+vv1(ncp,ncp,f);
   vc=-vv2(1,ncp,f); %- N-W corner
   vc3=[vc vv2(2,ncp,f) vv1(1,ncp,f) vc vv2(2,ncp,f)];
   z6t_r(1,ncp,f,k) = (vc3(n+2)+vc3(n+1))+vc3(n);
   f=2*n; %- even face number
   vc=-vv2(1,1,f); %- S-W corner
   z6t_r(1,1,f,k)   = (vv2(2,1,f)-vv1(1,2,f))+vc;
   vc=+vv2(n2p,1,f); %- S-E corner
   vc3=[-vv1(ncp,2,f) -vv2(ncp,1,f) vc -vv1(ncp,2,f) -vv2(ncp,1,f)];
   z6t_r(ncp,1,f,k) = (vc3(n)+vc3(n+1))+vc3(n+2);
   vc=+vv2(n2p,ncp,f); %- N-E corner
   z6t_r(ncp,ncp,f,k)=(vv1(ncp,ncp,f)+vc)-vv2(ncp,ncp,f);
   vc=-vv2(1,ncp,f); %- N-W corner
   z6t_r(1,ncp,f,k) = (vv2(2,ncp,f)+vc)+vv1(1,ncp,f);
 end
%- divide by rAz:
 z6t_r(:,:,:,k)=z6t_r(:,:,:,k)./aZ;
end

%-----------------------calculate planetary vorticity----------------------

z6t_p=squeeze( 2.0*omega*sin(c6y*pi/180.0) );

%-----------------------calculate isobaric potential vorticity-------------

%----1----calculate values at faces of vertical grid for using of d/dp

t3d_f=zeros(nx,nc,nr+1);

for k=1:nr
 km1 = max(k-1,1);
 t3d_f(:,:,k)=(t3d(:,:,km1)+t3d(:,:,k))/2.0;
end;

%--- upper boundaries---
t3d_f(:,:,nr+1)=2.0*t3d(:,:,nr)-t3d_f(:,:,nr);

%----calculate stability factor s(p)----
%----1--<ln(theta)>---
lnT(1:nr) = 0.0;
lnT_f(1:nr+1) = 0.0;

for k=1:nr
 lnT(k) = sum( sum(log(t3d(:,:,k)).*rAc,2) );
 lnT(k) = lnT(k)/(sum(sum(rAc)));
end;

for k=1:nr+1
 lnT_f(k) = sum( sum(log(t3d_f(:,:,k)).*rAc,2) );
 lnT_f(k) = lnT_f(k)/(sum(sum(rAc)));
end;

%---2-- s(p) = -d(lnT)/dp) ---

sp_temp_1 = -(lnT_f(2)-lnT_f(1))/ (log(rF(2))-log(rF(1))) ;
sp_temp_nr = -(lnT_f(nr+1)-lnT_f(nr))/(log(rF(nr+1))-log(rF(nr)));

sp(1:nr+1) = 0.0;

sp(2:nr) = -(lnT(2:nr)-lnT(1:nr-1))./reshape( (log(rC(2:nr))-log(rC(1:nr-1))),[1 nr-1]) ;

sp(1) = 2.0*sp_temp_1-sp(2);

sp(nr+1) = 2.0*sp_temp_nr - sp(nr);

%----calculate <T>--------

rT3d_f = zeros(nx,nc,nr+1);

for k=1:nr
 rT3d_f(:,:,k)=t3d_f(:,:,k)*(rF(k)/pref)^kappa;
end;

rT3d_f(:,:,nr+1)=rT3d_f(:,:,nr);

aT(1:nr+1) = 0.0;

for k=1:nr+1
 aT(k) = sum( sum(rT3d_f(:,:,k).*rAc,2) );
 aT(k) = aT(k)/(sum(sum(rAc)));
end;

term3_r = zeros(nx,nc,nr+1);

for k=1:nr+1
 term3_r(:,:,k) = rF(k)/sp(k)/aT(k)*(rT3d_f(:,:,k)-aT(k));
end;

%----calculate zonal mean of terms in term 3 on RHS
ny=256;

hFacC(:,:,nr+1)=hFacC(:,:,nr);

[term3_2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(term3_r,ny,yc,rAc,hFacC);

%----convert all to latlon grid-----

dxr = 180.0/ny; % 180/ny or 360/nx
xi=-180+dxr/2:dxr:180-dxr/2;
yi=-90+dxr/2:dxr:90-dxr/2;
lx=length(xi);
ly=length(yi);

[del] = cube2latlon_preprocess(xc,yc,xi,yi);

%----1-- f to latlon -- 

vort_p = reshape(permute(z6t_p([1:nc],[1:nc],:), [1 3 2]), [nx nc]);
vort_p = cube2latlon_fast(del,vort_p);

%----2-- zeta to latlon --

vort_r = reshape(permute(z6t_r(1:nc,1:nc,:,:),[1 3 2 4]),[nx nc nr]);
vort_r = cube2latlon_fast(del,vort_r);

%---calculate term 3 on RHS---

term3 = zeros(lx,ly,nr);

%---expand term3_2d to 3d

term3_3d = zeros(lx,ly,nr+1);

for i=1:lx 
 term3_3d(i,:,:)=reshape(term3_2d(:,:), [1 ly nr+1]);
end;

for k=1:nr
 term3(:,:,k) = vort_p(:,:).*( term3_3d(:,:,k+1)-term3_3d(:,:,k) )/drF(k);
end;

%----calculate qgpv-------

qgpv = zeros(lx,ly,nr);

for k=1:nr
 qgpv(:,:,k) = vort_p(:,:) + vort_r(:,:,k) - term3(:,:,k);
end;


return
