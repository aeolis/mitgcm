% output of the matlab script (stored to memory)
% dyn3d           stores u, v, w, t, s
% mom3d           stores uvc, wu, wv
% tr3d            stores ut, vt, wt
% g2d             stores 2-D grid information.
% note: vertical grids are not part of g2d.

clear all;

massweighted=1; % mass weighted velocity when massweighted=1
%avragalldata='true'; % average over all the datasets in "alldata"

diag_type=input('choose types of diagnostics: (1) Basic dynamic Field, (2) Momentum, (3) Heat (4) or water vapor : ');
cas=input('choose planet, E 1, U 2, N 3, J 4: ');
kappa=input('Atmosphere kappa = R/C_p :  ');
dt=input('size of time step: ');
nit=input('time to process (Press Enter key to use "alldata") : ');
chdatapath=input('full path to load data /~/ : ','s');

if (isempty(nit) == 1)
   load alldata; % alldata contains the data to be processed
else
   alldata=nit;
end;


diagSurf_str = 'surfDiag';  % includes all surface data, i.e. Eta (surface pressure anomaly)
diagDyn_str  = 'dynDiag';   % includes all dynamic variables, i.e. u, v, w
diagSecm_str = 'secmoms';   % includes all mass weighted dynamic variables, i.e. u, v, w

ustr='U';
trial_file=sprintf('%s%s.%s',chdatapath,ustr,'0000000000');
getsize=rdmds(trial_file);
dims=size(getsize); nx=dims(1); ny=dims(2); nr=dims(3);
clear trial_file getsize dims;

nsurf=3; ndyn=5; nsecm=10;  %ndyn=12

diagSurf = zeros(nx,ny,nsurf);
dyn3d  = zeros(nx,ny,nr,ndyn);
diagSecm = zeros(nx,ny,nr,nsecm); 


for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter));
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        time=allzero
        timed=alldata(niter);

        diagSurf_file = sprintf('%s%s.%s',chdatapath,diagSurf_str,time);
        diagDyn_file  = sprintf('%s%s.%s',chdatapath,diagDyn_str,time);
        diagSecm_file = sprintf('%s%s.%s',chdatapath,diagSecm_str,time);
  
        diagSurf_tmp = rdmds(diagSurf_file);
        diagDyn_tmp  = rdmds(diagDyn_file);
        diagSecm_tmp = rdmds(diagSecm_file);

        diagSurf     = diagSurf+diagSurf_tmp;
        dyn3d        = dyn3d+diagDyn_tmp(:,:,:,1:5);
        diagSecm     = diagSecm+diagSecm_tmp;
        clear diagSurf_tmp diagDyn_tmp diagSecm_tmp;
end;

diagSurf = diagSurf/length(alldata);
dyn3d    = dyn3d/length(alldata);
diagSecm = diagSecm/length(alldata);

% use or do not use mass weighted variables for diagnostics.
if (massweighted == 1) 
   dyn3d(:,:,:,1) = diagSecm(:,:,:,1); % replace u with mass weigthed
   dyn3d(:,:,:,2) = diagSecm(:,:,:,2); % replace v with mass weighted
   dyn3d(:,:,:,3) = diagSecm(:,:,:,3); % replace w with mass weighted
end;

if (diag_type == 2) 
   mom3d = zeros(nx,ny,nr,3);
   mom3d(:,:,:,1) = diagSecm(:,:,:,7); % cell centered uvc
   mom3d(:,:,:,2) = diagSecm(:,:,:,9); % cell centered wu
   mom3d(:,:,:,3) = diagSecm(:,:,:,10);% cell centered wv
end;

if (diag_type == 3)
   tr3d  = zeros(nx,ny,nr,3);
   if (massweighted == 1)
      tr3d(:,:,:,1) = diagSecm(:,:,:,4); % cell centered ut
      tr3d(:,:,:,2) = diagSecm(:,:,:,5); % cell centered vt
      tr3d(:,:,:,3) = diagSecm(:,:,:,6); % cell centered wt
   else
      tr3d(:,:,:,1) = dyn3d(:,:,:,6); % cell centered ut
      tr3d(:,:,:,2) = dyn3d(:,:,:,7); % cell centered vt
      tr3d(:,:,:,3) = dyn3d(:,:,:,8); % cell centered wt
   end;
end;

eta = diagSurf(:,:,1);

clear diagSecm; 

% read grid files, use network or local directories, defined by chdatapath

dxc=rdmds([chdatapath,'DXC']);
dyc=rdmds([chdatapath,'DYC']);
dxg=rdmds([chdatapath,'DXG']);
dyg=rdmds([chdatapath,'DYG']);


xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);
xg=rdmds([chdatapath,'XG']);
yg=rdmds([chdatapath,'YG']);

rAc=rdmds([chdatapath,'RAC']);
rAz=rdmds([chdatapath,'RAZ']);
rAs=rdmds([chdatapath,'RAS']);
rAw=rdmds([chdatapath,'RAW']); 

rC=rdmds([chdatapath,'RC']);
rF=rdmds([chdatapath,'RF']);
drC=rdmds([chdatapath,'DRC']);
drF=rdmds([chdatapath,'DRF']);

hFacC=rdmds([chdatapath,'hFacC']);
hFacW=rdmds([chdatapath,'hFacW']);
hFacS=rdmds([chdatapath,'hFacS']);

tstr='T';
t_Ref_file=sprintf('%s%s.%s',chdatapath,tstr,'0000000000');
t_Ref=rdmds(t_Ref_file);
tRef=squeeze(t_Ref(1,1,:));
clear t_Ref;

% load physical parameters for the model

if cas==1
   rsphere=6378.0e3;
   atm_cp=1002.0;
   gravity=9.81;
   omega=2*pi/86400;
   day=timed*dt/86400;

elseif cas==2
   rsphere=25559.0e3;
   gravity=7.77;
   atm_cp=13000.0;
   omega=2*pi/62064;
   day=timed*dt/86400;

elseif cas==3
   rsphere=24746.0e3;
   gravity=11.7;
   atm_cp=13000.0;
   omega=2*pi/62064;
   day=timed*dt/86400;

elseif cas==4
   rsphere=71492.0e3;
   gravity=22.88;
   atm_cp=13000.0;
   omega=2*pi/35730;
   day=timed*dt/86400;

else
   'no options';
end;

% define vertical grids and reference temperature

rC = squeeze(rC);  % pc is at the center of the vertical grids.
drF = -squeeze(drF); % dp is the steps of the vertical grids, starts from lower boundary dp<0.
drC = -squeeze(drC);
rF = squeeze(rF); % pf is the pressure at faces of vertical grid.

g2d(:,:,1) = xc;
g2d(:,:,2) = yc;
g2d(:,:,3) = xg;
g2d(:,:,4) = yg;
g2d(:,:,5) = dxc;
g2d(:,:,6) = dyc;
g2d(:,:,7) = dxg;
g2d(:,:,8) = dyg;
g2d(:,:,9) = rAc;
g2d(:,:,10) = rAz;
g2d(:,:,11) = rAs;
g2d(:,:,12) = rAw;

