function [Eta,theta,ylatc]=init_geostrophic_isentropic(u, ymin, ymax, tref, rF, rotperiod, atm_cp, kappa, rsphere);

% this program generates the bottom 
% pressure anomaly that maintains geostrophic winds. 
% the atmosphere is required to be adiabatic at lower boundary.

% u   -- zonal wind field, from data at the center of horizontal grid.
% ymin-- starting latitude
% ymax-- ending latitude
% tref-- reference temperature field
% p   -- pressure levels. 
% rotperiod -- rotation period of planet, in seconds.

% Eta -- surface pressure anomaly
% theta -- temperature field, calculated by thermal wind relation.

% the wind field and bottom pressure anomaly field will be written 
% to binary data as initial condition

dims=size(u); ny=dims(1); nz=dims(2);

rF(nz+1)=0.0;
rC(1:nz)=(rF(1:nz)+rF(2:nz+1))/2.0;
drF(1:nz)=rF(2:nz+1)-rF(1:nz);
drC(1)=rC(1)-rF(1);
drC(2:nz)=rC(2:nz)-rC(1:nz-1);

deg2rad=pi/180;
p0=1.0e5;
atm_R=atm_cp*kappa;
omega=2.0*pi/rotperiod;

dy=(ymax-ymin)/ny;
ylat=ymin:dy:ymax;
ylatc=ymin+dy/2:dy:ymax-dy/2;
xlat=-180+dy/2:dy:180-dy/2;
dyl=dy*deg2rad*rsphere;

nx=length(xlat);

% Coriolis parameter
lx=xlat*deg2rad;
ly=ylat*deg2rad;
lyc=ylatc*deg2rad;

f=2.0*omega*sin(lyc);

% define zonal winds on faces of vertical grid, use linear extrapolation

uF=zeros(ny,nz+1);

for j=1:ny
uF(j,:)=interp1(rC,u(j,:),rF,'linear','extrap');
end;

dudp=zeros(ny,nz);

for j=1:ny
 dudp(j,1:nz)=(uF(j,2:nz+1)-uF(j,1:nz))./drF(1:nz);
end;

theta_y=zeros(ny+1,nz);
theta_y(1,:)=tref;

for k=1:nz
  for j=2:ny+1
      theta_y(j,k)=theta_y(j-1,k)+(p0/rC(k))^kappa*f(j-1)*rC(k)/atm_R*dudp(j-1,k)*dyl;
  end;
end;


theta=(theta_y(1:ny,:)+theta_y(2:ny+1,:))/2.0;

% based on hydrostatic equilibrium, geostrophic balance and 
% ideal gas law:

% p^kappa = -(g*kappa)/(R*theta) * p_0^kappa * z 
%           -int( (f*kappa)/(R*theta) * p_0^kappa * u_g * dy )

% where theta(z) =  theta_0
% u_g is the geostrophic zonal wind. 
% p_0 is the reference pressure.

% when z=0, -int( (f*kappa)/(R*theta) * p_0^kappa * u_g * dy )
% gives pressure anomaly.

Eta=zeros(nx,ny);

% check this later!!!!!!!!!!!!
p_y(1)=rF(1);
pr_tmp(1)=(rF(1)/p0)^kappa; % pr_tmp=(p/p0)^kappa

for j=2:ny+1
    pr_tmp(j)=pr_tmp(j-1) - kappa/(atm_R*tref(1))*(u(j-1)*f(j-1)*dyl);
    p_y(j)=pr_tmp(j)^(1.0/kappa)*p0;  % p_tmp is in unit of pressure (Pa)
end;

p_yc=(p_y(1:ny)+p_y(2:ny+1))/2.0;

for i=1:nx
    Eta(i,:)=p_yc-rF(1);
end;

% expand theta and u to 3d array

theta3d=zeros(nx,ny,nz);
u3d=zeros(nx,ny,nz);

theta3d=permute(theta3d,[2 3 1]);
u3d=permute(u3d,[2 3 1]);

for i=1:nx
  theta3d(:,:,i)=theta;
  u3d(:,:,i)=u;
end;

theta3d=permute(theta3d,[3 1 2]);
u3d=permute(u3d,[3 1 2]);

pcolor(theta3d(:,:,1)'); shading interp;

whos theta3d

% write data to binary files

% Default precision
prec='real*8';
ieee='b';

fid=fopen('Tinit.bin','w',ieee);
if fid<0
  error('Unable to create file')
end
for k=1:nz
    fwrite(fid,theta3d(:,:,k),prec);
end;
fclose(fid);

fid=fopen('ETAinit.bin','w',ieee);
if fid<0
  error('Unable to create file')
end
    fwrite(fid,Eta,prec);
fclose(fid);

fid=fopen('Uinit.bin','w',ieee);
if fid<0
  error('Unable to create file')
end
for k=1:nz
    fwrite(fid,u3d(:,:,k),prec);
end;
fclose(fid);

    
