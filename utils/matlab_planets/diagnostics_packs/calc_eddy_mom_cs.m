function [acc,ylatc,uv_star,wu_star,fv]=calc_eddy_mom_cs(dyn3d,mom3d,g2d,drC,drF,rC,rF,hFacC,rsphere,omega);

% the eddy acceleration equation follows Karoly (1998)

% the following terms are time-averaged and zonal-mean quantities
% hr -- total heating rate
% dhdt -- diabatic heating
% gamw -- vertical advection of heat
% vdtdy -- horizontal advection of heat
% dvtdy -- horizontal convergence of eddy heat heat flux
% dwtdp -- vertical convergence of eddy heat flux


% cwt -- vertical eddy heat flux (extra terms associated with dwtdp)
% tau -- rediative cooling time scale, in seconds
% tauc -- condensation time scale, in seconds

% ny --- number of grid points (lines) in latitude, required for zonal mean 

ny=256;

u3d=squeeze(dyn3d(:,:,:,1));
v3d=squeeze(dyn3d(:,:,:,2));
w3d=squeeze(dyn3d(:,:,:,3));

uvc3d=squeeze(mom3d(:,:,:,1));
wu3d=squeeze(mom3d(:,:,:,2));
wv3d=squeeze(mom3d(:,:,:,3));
usq =squeeze(mom3d(:,:,:,4));
vsq =squeeze(mom3d(:,:,:,5));
uvz3d=squeeze(mom3d(:,:,:,6));

xc=squeeze(g2d(:,:,1));
yc=squeeze(g2d(:,:,2));
xg=squeeze(g2d(:,:,3));
yg=squeeze(g2d(:,:,4));
dxc=squeeze(g2d(:,:,5));
dyc=squeeze(g2d(:,:,6));
dxg=squeeze(g2d(:,:,7));
dyg=squeeze(g2d(:,:,8));
rAc=squeeze(g2d(:,:,9));
rAz=squeeze(g2d(:,:,10));
rAs=squeeze(g2d(:,:,11));
rAw=squeeze(g2d(:,:,12));

[AngleCS,AngleSN] = cubeCalcAngle(yg,rAc,dxg,dyg);

check_var=input('caution: must use time average variables !!!, press Enter to contiue or ctrl+c to terminate  ');

%if (check_var ~= 1)
%   error('Stop, use time averaged variables');
%end;

dims=size(u3d); nx=dims(1); nc=dims(2); ncp=nc+1; n2p=nc+2; nPg=nx*nc ;
if nx == 6*nc, flag=1; else flag=0; end
if size(v3d) ~= dims, flag=0; end
if size(dxc) ~= dims(1:2), flag=0; end
if size(dyc) ~= dims(1:2), flag=0; end
if flag == 0,
 fprintf(' Error in size of input arrays\n');
 vort=0; return
end
if length(dims) == 2, nr=1; else nr=prod(dims(3:end)); end


rF(nr+1)=0.0;

u3d=reshape(u3d,[nx nc nr]);
v3d=reshape(v3d,[nx nc nr]);
w3d=reshape(w3d,[nx nc nr]);
usq=reshape(usq,[nx nc nr]);
vsq=reshape(vsq,[nx nc nr]);

uvc3d=reshape(uvc3d,[nx nc nr]);
wu3d=reshape(wu3d,[nx nc nr]);
wv3d=reshape(wv3d,[nx nc nr]);

cosalpha = reshape(AngleCS, [nPg 1]);
sinalpha = reshape(AngleSN, [nPg 1]);

%---calculate horizontal advection of eddy momentum (cell centered)---[v]*d[u]/dy------------------------1

vdudy=zeros(ny,nr);

[uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C');
% -- v2d at the center of the cell
[v2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(vN,ny,yc,rAc,hFacC);

ylatc=ylat;

% -- t2d at the north/south edges of the cell
[u2d,mskzon,ylat,areazon] = calcZonalAvgCubeG(uE,ny,yc,rAc,hFacC);

dyc_latlon(1:ny)=pi/ny*rsphere;

for k=1:nr,
 vdudy([2:ny],k)=v2d([2:ny],k).* (u2d([2:ny],k)-u2d([1:ny-1],k))./dyc_latlon(2:ny)';
end;

% filling vdtdy(1,:) and vdtdy(ny,:) with NaNs

vdudy(1,:)=NaN; vdudy(ny,:)=NaN;

%---calculate horizontal eddy momentum flux d/dy([u'v']+[u*v*])---use Physics of climate ---[u'v']+[u*v*]=[uv]-[u][v]----------2

[uvc3d,UVtrans,U2tot,U2trans,V2tot,V2trans,errFlag]=calc2ndmom(u3d,v3d,usq,vsq,uvc3d,cosalpha,sinalpha);

%for k=1:nr
%uvc3d(:,:,k)=uvc3d(:,:,k).*(AngleCS.*AngleCS-AngleSN.*AngleSN);
%end;

[uv2d,mskzon,ylat,areazon] = calcZonalAvgCubeG(uvc3d,ny,yc,rAc,hFacC);

[v2d,mskzon,ylat,areazon] = calcZonalAvgCubeG(vN,ny,yc,rAc,hFacC);

%figure;
%subplot(2,1,1); plot(ylat,uv2d(:,25));
%subplot(2,1,2); plot(ylat,u2d(:,25).*v2d(:,25));

uv_star=uv2d-u2d.*v2d;

% d/dy([u'v']+[u*v*]) (cell centered)

duvdy=zeros(ny,nr);

for k=1:nr,
 duvdy([2:ny],k)=(uv_star([2:ny],k)-uv_star([1:ny-1],k))./dyc_latlon(2:ny)';
end;

% filling duvdy(1,:) and duvdy(ny,:) with NaNs

duvdy(1,:)=NaN; duvdy(ny,:)=NaN;

%---calculate horizontal eddy momentum flux d/dp([w'u']+[w*u*])---use Physics of climate ---[w'u']+[w*u*]=[wu]-[w][u]----------3

% -- wu2d w2d at the center of the cell (face of the vertical grid),

% use cube2latlon as a temperory solution---- will change to CS method eventually


%-------don't modify the following part---------------!!!!!!!---

%C aW=split_Z_cub(rAw);

%C w6t=split_Z_cub(w3d);
%C aC=split_Z_cub(rAc);

%C if nr > 1,
%C   w6t=permute(w6t,[1 2 4 3]);
%C end;

%C w6t_u=zeros(nc,nc,6,nr);


%C for k=1:nr,
%C  w6t_u([2:nc],[1:nc],:,k)=( w6t([1:nc-1],[1:nc],:,k).*aC([1:nc-1],[1:nc],:)+ ...
%C                             w6t([2:nc],[1:nc],:,k).*aC([2:nc],[1:nc],:) )./aW([2:nc],[1:nc],:)/2.0;
%C end;

%---test for wu----

%wu_6t_test=zeros(nc,nc,6,nr);

%[wu_6t,wv_6t]=split_UV_cub(wu3d,wv3d);

%wu_6t=permute(wu_6t,[1 2 4 3]);

%u6t=split_UV_cub(u3d,v3d);

%u6t=permute(u6t,[1 2 4 3]);

%for k=1:nr,
% km1 = max(k-1,1);
% wu_6t_test([2:nc],[1:nc],:,k)=(u6t([2:nc],[1:nc],:,km1)+u6t([2:nc],[1:nc],:,k)).* ...
%                               (w6t([1:nc-1],[1:nc],:,k).*aC([1:nc-1],[1:nc],:)+ ...
%                                w6t([2:nc],[1:nc],:,k).*aC([2:nc],[1:nc],:))./aW([2:nc],[1:nc],:)/4.0;
%end;

%--------calculate the missing points wu_6t_test(1,[1:nc],:,:)
%for k=1:nr,
% km1 = max(k-1,1);
% wu_6t_test(1,[1:nc],1,k)=reshape((u6t(1,[1:nc],1,km1)+u6t(1,[1:nc],1,k)),nc,1).* ...
%                         (squeeze(w6t([nc:-1:1],nc-1,5,k).*aC([nc:-1:1],nc-1,5))+ ...
%                          reshape((w6t(1,[1:nc],1,k).*aC(1,[1:nc],1)),nc,1))./reshape(aW(1,[1:nc],1),nc,1)/4.0;
% wu_6t_test(1,[1:nc],2,k)=reshape((u6t(1,[1:nc],1,km1)+u6t(1,[1:nc],1,k)),nc,1).* ...
%                          reshape((w6t(nc-1,[1:nc],1,k).*aC(nc-1,[1:nc],1)+ ...
%                                w6t(1,[1:nc],1,k).*aC(1,[1:nc],1)),nc,1)./reshape(aW(1,[1:nc],1),nc,1)/4.0;
% wu_6t_test(1,[1:nc],3,k)=reshape((u6t(1,[1:nc],1,km1)+u6t(1,[1:nc],1,k)),nc,1).* ...
%                         (squeeze(w6t([nc:-1:1],nc-1,1,k).*aC([nc:-1:1],nc-1,1))+ ...
%                          reshape((w6t(1,[1:nc],1,k).*aC(1,[1:nc],1)),nc,1))./reshape(aW(1,[1:nc],1),nc,1)/4.0;
% wu_6t_test(1,[1:nc],4,k)=reshape((u6t(1,[1:nc],1,km1)+u6t(1,[1:nc],1,k)),nc,1).* ...
%                          reshape((w6t(nc-1,[1:nc],3,k).*aC(nc-1,[1:nc],3)+ ...
%                                w6t(1,[1:nc],1,k).*aC(1,[1:nc],1)),nc,1)./reshape(aW(1,[1:nc],1),nc,1)/4.0;
% wu_6t_test(1,[1:nc],5,k)=reshape((u6t(1,[1:nc],1,km1)+u6t(1,[1:nc],1,k)),nc,1).* ...
%                         (squeeze(w6t([nc:-1:1],nc-1,3,k).*aC([nc:-1:1],nc-1,3))+ ...
%                          reshape((w6t(1,[1:nc],1,k).*aC(1,[1:nc],1)),nc,1))./reshape(aW(1,[1:nc],1),nc,1)/4.0;
% wu_6t_test(1,[1:nc],6,k)=reshape((u6t(1,[1:nc],1,km1)+u6t(1,[1:nc],1,k)),nc,1).* ...
%                          reshape((w6t(nc-1,[1:nc],5,k).*aC(nc-1,[1:nc],5)+ ...
%                                w6t(1,[1:nc],1,k).*aC(1,[1:nc],1)),nc,1)./reshape(aW(1,[1:nc],1),nc,1)/4.0;
 
%end;

%figure;
%subplot(3,2,1); pcolor(wu_6t_test([2:nc],[2:nc],1,25));
%subplot(3,2,2); pcolor(wu_6t_test([2:nc],[2:nc],2,25));
%subplot(3,2,3); pcolor(wu_6t_test([2:nc],[2:nc],3,25));
%subplot(3,2,4); pcolor(wu_6t_test([2:nc],[2:nc],4,25));
%subplot(3,2,5); pcolor(wu_6t_test([2:nc],[2:nc],5,25));
%subplot(3,2,6); pcolor(wu_6t_test([2:nc],[2:nc],6,25));

%figure;
%subplot(3,2,1); pcolor(wu_6t([2:nc],[2:nc],1,25));
%subplot(3,2,2); pcolor(wu_6t([2:nc],[2:nc],2,25));
%subplot(3,2,3); pcolor(wu_6t([2:nc],[2:nc],3,25));
%subplot(3,2,4); pcolor(wu_6t([2:nc],[2:nc],4,25));
%subplot(3,2,5); pcolor(wu_6t([2:nc],[2:nc],5,25));
%subplot(3,2,6); pcolor(wu_6t([2:nc],[2:nc],6,25));

%figure;
%pcolor(wu3d(:,:,25));
%wu_test=reshape(permute(wu_6t_test,[1 3 2 4]),[6*nc nc nr]);
%figure;
%pcolor(wu_test(:,:,25));
%figure
%pcolor(wu3d(:,:,25)-wu_test(:,:,25));

%---end test-----------------------

%--------------the conversion from cubed sphere to latlon grid 
%--------------will produce errors in w3d-

%w6t_u=permute(w6t_u,[1 3 2 4]);

%w3d_u=reshape(w6t_u,6*nc,nc,nr);

%C figure;
%C pcolor(w3d_u(:,:,25)-w3d(:,:,25));
%C merccube(xg,yg,w3d_u(:,:,25));

u3d_w=zeros(6*nc,nc,nr);
v3d_w=zeros(6*nc,nc,nr);

for k=1:nr,
 km1 = max(k-1,1);
 u3d_w(:,:,k)=(u3d(:,:,k)+u3d(:,:,km1))/2.0;
 v3d_w(:,:,k)=(v3d(:,:,k)+v3d(:,:,km1))/2.0;
end;

[uE_w,vN_w] = rotate_uv2uvEN(u3d_w,v3d_w,AngleCS,AngleSN,'C');

[wuE,wvN] = rotate_uv2uvEN(wu3d,wv3d,AngleCS,AngleSN,'C');

[wu2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(wuE,ny,yc,rAc,hFacC);
%[w2d_u,mskzon,ylat,areazon] = calcZonalAvgCubeC(w3d_u,ny,yc,rAc,hFacC);
[w2d_u,mskzon,ylat,areazon] = calcZonalAvgCubeC(w3d,ny,yc,rAc,hFacC);
[u2d_w,mskzon,ylat,areazon] = calcZonalAvgCubeC(uE_w,ny,yc,rAc,hFacC);

wu_star=wu2d-w2d_u.*u2d_w;

% d/dp([w'u']+[w*u*]) (cell centered)

wu_star(:,nr+1)=0.0;
dwudp=zeros(ny,nr);

for k=1:nr,
 dwudp(:,k)=( wu_star(:,k+1)-wu_star(:,k) )/drF(k);
end;

%---calculate vertical advection of momentum---[w]d[u]/dp----------------------------------------------------------------------4

wdudp=zeros(ny,nr);

for k=1:nr-1,
 wdudp(:,k)=(w2d_u(:,k+1)+w2d_u(:,k))/2.0.*(u2d_w(:,k+1)-u2d_w(:,k))/drF(k);
end;

%----calculate Coriolis acceleration------f[v]---------------------------------------------------------5

fv=zeros(ny,nr);

[v2d,mskzon,ylat,areazon] = calcZonalAvgCubeC(vN,ny,yc,rAc,hFacC);

for k=1:nr,
 fv(:,k)=2.0*omega*sin( reshape(ylatc,ny,1)*pi/180.0 ).*v2d(:,k);
end;


acc=zeros(ny,nr,5);
acc(:,:,1)=-duvdy;
acc(:,:,2)=-dwudp;
acc(:,:,3)=-vdudy;
acc(:,:,4)=-wdudp;
acc(:,:,5)=fv;

fprintf('++++++++++++++++++++ \n');

fprintf('terms in accz [ %d x %d x (%d terms) ]:  \n',ny,nr,5);
fprintf('term 1 : meridional convergence of horizontal eddy momentum flux \n');
fprintf('term 2 : vertical convergence of vertical eddy momentum flux \n');
fprintf('term 3 : horizontal momentum advection \n');
fprintf('term 4 : vertical momentum advection\n');
fprintf('term 5 : Coriolis acceleration \n');
