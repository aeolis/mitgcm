function [u3drt_cs,v3drt_cs,u3drt_latlon,v3drt_latlon,lx,ly]=rotate_uv(u3d,v3d,xc,yc,yg,dxg,dyg,rAc,dx);

% rotate cube-sphere grid, the (0,0) point in original 
% grid is rotated to (0,90), here (0,0) and (0,90) means longitude=0 degree,
% latitude=0/90. Also assume (0,0) in original grid is the substellar point.

% u3d and v3d are original wind fields from cube-sphere grid.

% u3drt_cs, v3drt_cs are the fields after rotation in cube-sphere grid.
% u3drt_latlon,v3drt_latlon are the fields interpolated to latlon grid, 
% with longitude "lx' and latitude "ly".

% xc, yc, yg are coordinates in cube-sphere grid.
% xc=rdmds('XC'); yc=rdmds('YC'); yg=rdmds('YG');
% "c" means center, "g" means corner.
% 
% dxg nd dyg are grid resolution in cube-sphere grid. 
% dxg=rdmds('DXG'); dyg=rdmds('DYG');

% rAc is the area of grids.
% rAc=rdmds('RAC');

% dx is the resolution in latlon grid. 
% ie, if use C128, then dx=360/512=0.703125;

% Usage (C128 as an example):  
% [u3drt_cs,v3drt_cs,u3drt_latlon,v3drt_latlon,lx,ly]=rotate_uv(u3d,v3d,xc,yc,yg,dxg,dyg,rAc,0.703125);

dims=size(u3d); nx=dims(1); nc=dims(2); ncp=nc+1; n2p=nc+1; nPg=nx*nc ;
if nx == 6*nc, flag=1; else flag=0; end
if flag == 0,
 fprintf(' Error in size of input arrays\n');
 t3drt_cs=0; t3drt_latlon=0; return
end
if length(dims) == 2, nr=1; else nr=prod(dims(3:end)); end

% rotate u and v to center of the grid

[AngleCS,AngleSN] = cubeCalcAngle(yg,rAc,dxg,dyg);

[uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C');

% rotate u

[u3drt_cs]=rotate_ssaxis(uE,nx,nc,nr);

% rotate v

[v3drt_cs]=rotate_ssaxis(vN,nx,nc,nr);

% define latlon grid

lx=-180+dx/2:dx:180-dx/2;
ly=-90+dx/2:dx:90-dx/2;

% inerpolate the CS grid to latlon

[del] = cube2latlon_preprocess(xc,yc,lx,ly);

u3drt_latlon=cube2latlon_fast(del,u3drt_cs);
v3drt_latlon=cube2latlon_fast(del,v3drt_cs);


