clear all;

tave=input('load time averaged data? 1-->true, 0-->false : ');
cas=input('choose planet, E 1, U 2, N 3, J 4: ');
kappa=input('Atmosphere kappa = R/C_p :  '); 
dt=input('size of time step: ');
nit=input('time to process (Press Enter key to use "alldata" : ');
chdatapath=input('full path to load data /~/ : ','s');

if (isempty(nit) == 1)
   load alldata; % alldata contains the data to be processed
else 
   alldata=nit;
end;

if (tave == 0)
  ustr='U';
  vstr='V';
  wstr='W';
  tstr='T';
  sstr='S';
else
  ustr='uVeltave';
  vstr='vVeltave';
  wstr='wVeltave';
  tstr='Ttave';
  sstr='Stave';
end;

ustr0 = 'U';
tstr0 = 'T';

  trial_file=sprintf('%s%s.%s',chdatapath,ustr0,'0000000000');
  getsize=rdmds(trial_file);
  dims=size(getsize); nx=dims(1); ny=dims(2); nr=dims(3);
  clear trial_file getsize dims;

  u3d=zeros(nx,ny,nr);
  v3d=zeros(nx,ny,nr);
  w3d=zeros(nx,ny,nr);
  t3d=zeros(nx,ny,nr);
  s3d=zeros(nx,ny,nr);


for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter));
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        time=allzero
        timed=alldata(niter);

        ufile=sprintf('%s%s.%s',chdatapath,ustr,time);
        vfile=sprintf('%s%s.%s',chdatapath,vstr,time);
        wfile=sprintf('%s%s.%s',chdatapath,wstr,time);
        tfile=sprintf('%s%s.%s',chdatapath,tstr,time);
        sfile=sprintf('%s%s.%s',chdatapath,sstr,time);

        uics1=rdmds(ufile);
        vics1=rdmds(vfile);
        wics1=rdmds(wfile);
        tics1=rdmds(tfile);
        sics1=rdmds(sfile);

        u3d=u3d+uics1;
        v3d=v3d+vics1;
        w3d=w3d+wics1;
        t3d=t3d+tics1;
        s3d=s3d+sics1;

end;

clear uics1 vics1 wics1 tics1 sics1;


u3d=u3d/length(alldata);
v3d=v3d/length(alldata);
w3d=w3d/length(alldata);
t3d=t3d/length(alldata);

% read grid files, use network or local directories, defined by chdatapath

dxc=rdmds([chdatapath,'DXC']);
dyc=rdmds([chdatapath,'DYC']);
dxg=rdmds([chdatapath,'DXG']);
dyg=rdmds([chdatapath,'DYG']);


xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);
xg=rdmds([chdatapath,'XG']);
yg=rdmds([chdatapath,'YG']);

rAc=rdmds([chdatapath,'RAC']);
rAz=rdmds([chdatapath,'RAZ']);
rAs=rdmds([chdatapath,'RAS']);
rAw=rdmds([chdatapath,'RAW']);

rC=rdmds([chdatapath,'RC']);
rF=rdmds([chdatapath,'RF']);
drC=rdmds([chdatapath,'DRC']);
drF=rdmds([chdatapath,'DRF']);

hFacC=rdmds([chdatapath,'hFacC']);
hFacW=rdmds([chdatapath,'hFacW']);
hFacS=rdmds([chdatapath,'hFacS']);

t_Ref_file=sprintf('%s%s.%s',chdatapath,tstr0,'0000000000');
t_Ref=rdmds(t_Ref_file);
tRef=squeeze(t_Ref(1,1,:));
clear t_Ref;

% load physical parameters for the model

if cas==1
   rsphere=6378.0e3;
   gravity=9.81;
   omega=2*pi/86400;
   day=timed*dt/86400;

elseif cas==2
   rsphere=25559.0e3;
   gravity=7.77;
   omega=2*pi/62064;
   day=timed*dt/86400;

elseif cas==3
   rsphere=24746.0e3;
   gravity=11.7;
   omega=2*pi/62064;
   day=timed*dt/86400;

elseif cas==4
   rsphere=71492.0e3;
   gravity=22.88;
   omega=2*pi/35730;
   day=timed*dt/86400;

else
   'no options';
end;

% define vertical grids and reference temperature

rC = squeeze(rC);  % pc is at the center of the vertical grids.
drF = -squeeze(drF); % drF is the steps of the vertical grids, starts from lower boundary dp<0.
drC = -squeeze(drC);
tr = tRef;  % tr is the reference potential temperature profile.
rF = squeeze(rF); % pf is the pressure at faces of vertical grid.

% check if TPprofile matches datasets
dims = size(u3d); 
if length(dims) == 2, nr=1; else nr=prod(dims(3:end)); end;


% packup dynamic fields and grid files to single arrays

% convert cube-sphere grid to latlon grid

dxr=0.703125;
dyr=0.703125;
xi=-180+dxr/2:dxr:180-dxr/2;
yi=-90+dxr/2:dxr:90-dxr/2;
lx=length(xi);
ly=length(yi);

d2r=pi/180;

[del] = cube2latlon_preprocess(xc,yc,xi,yi);

[u3d,v3d]=uvcube2latlongrid(del,u3d,v3d,xg,yg,rAc,dxg,dyg);
w3d=cube2latlon_fast(del,w3d);
t3d=cube2latlon_fast(del,t3d);
s3d=cube2latlon_fast(del,s3d);

