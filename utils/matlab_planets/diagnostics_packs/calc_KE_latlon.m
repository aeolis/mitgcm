function [KE,wz]=calc_KE_latlon(dyn3d,g2d,rC,drF,rAc,kappa,atm_cp,gravity);

% calculate total kinetic energy in J
% vertical velocity wz (m/s) is calculated by:

% wz(k)-wz(k+1)=-RT(k)/g*(w(k)-w(k+1))

% total KE is
% KE(i,j)=dp/g*rA(i,j)*1/2*(u^2+v^2+wz^2)

pref=1.0e5; % reference pressure at 1 bar.

atm_R=atm_cp*kappa;

u3d=squeeze(dyn3d(:,:,:,1));
v3d=squeeze(dyn3d(:,:,:,2));
w3d=squeeze(dyn3d(:,:,:,3));
t3d=squeeze(dyn3d(:,:,:,4));

dims=size(u3d); nx=dims(1); ny=dims(2); nr=dims(3);

wz=zeros(nx,ny,nr+1);
w3d(:,:,nr+1)=0.0;

u3d(nx+1,:,:)=u3d(1,:,:);
v3d(:,ny+1,:)=2.0*v3d(:,ny,:)-v3d(:,ny-1,:);

realT=zeros(nx,ny,nr);

for k=1:nr,
 realT(:,:,k)=t3d(:,:,k)*(rC(k)/pref)^kappa;
end;

for k=nr:-1:1,
 wz(:,:,k)=wz(:,:,k+1)-atm_R/(rC(k)*gravity)*realT(:,:,k).*(w3d(:,:,k)-w3d(:,:,k+1));
end;

u_c=zeros(nx,ny,nr);
v_c=zeros(nx,ny,nr);
wz_c=zeros(nx,ny,nr);

u_c(:,:,:)=0.5*(u3d([2:nx+1],:,:)+u3d([1:nx],:,:));
v_c(:,:,:)=0.5*(v3d(:,[2:ny+1],:)+v3d(:,[1:ny],:));

for k=1:nr,
 wz_c(:,:,k)=0.5*( wz(:,:,k)+wz(:,:,k+1) );
end;

KE_tmp=zeros(nx,ny,nr);

for k=1:nr,
 KE_tmp(:,:,k)=0.5*abs(drF(k))/gravity*rAc(:,:).*(u_c(:,:,k).^2+v_c(:,:,k).^2+wz_c(:,:,k).^2);
end;

KE=squeeze(sum(KE_tmp,1));
KE=squeeze(sum(KE,1));
KE=sum(KE);

