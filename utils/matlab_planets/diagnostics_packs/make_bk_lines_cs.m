function [namf_cs]=make_psi_lines_cs(dyn3d,g2d,drF,time,gravity,rC);

% psi -- meridional streamfunction
% yax -- latitude
% zax -- pressure (pc)

u3d=squeeze(dyn3d(:,:,:,1));

xc=squeeze(g2d(:,:,1));
yc=squeeze(g2d(:,:,2));
xg=squeeze(g2d(:,:,3));
yg=squeeze(g2d(:,:,4));
dxg=squeeze(g2d(:,:,7));
dyg=squeeze(g2d(:,:,8));
rAc=squeeze(g2d(:,:,9));

try_and_real=1;
gener_bk_line; % generate the broke lines, first try

try_and_real=0;
gener_bk_line;

dims=size(u3d); nx=dims(1); nc=dims(2); ncp=nc+1; n2p=nc+1; nPg=nx*nc ;

ijprt=nc+1;

gen_bk_Zon;

