% Write P-T profile to ASCII file, including a header to 
% describe the data

%function []=rotate_write_PT(runtype,time,dtime);

runtype=input('type of run : ','s');
time=input('number of run steps : ');
dtime=input('time step : ');
ny=input('number of grid points in latitude : ');
kappa=input('atmosphere kappa : ');
sync=input('synchronous rotation? 1-->true : ');

%t3d=dyn3d(:,:,:,4);
%[t3drt_cs,t3drt_latlon,lx,ly]=rotate_T(t3d,xc,yc,ny);
[dyn3d_latlon,lx,ly]=wind3d_cs2latlon(dyn3d,g2d,hFacC,ny);
t3d_orig=dyn3d_latlon(:,:,:,4);
t3d_orig(:,1,:)=2.0*t3d_orig(:,2,:)-t3d_orig(:,3,:);
t3d_orig(:,ny,:)=2.0*t3d_orig(:,ny-1,:)-t3d_orig(:,ny-2,:);

if sync == 1
% syncrhonous rotation
  [t3drt_latlon,lx_temp,ly_temp]=rotate_general_spGrid(t3d_orig,lx,ly,90,-90,-90);

% non-synchronous rotation, no tilt of rotation axis, initially substellar point 
% at x=0, coordinate is locked to the planet
else 
  xlat_rot=omega*dtime*time*180/pi+90; % in degree, omega has + and -, + is counterclockwise rotation
  [t3drt_latlon,lx_temp,ly_temp]=rotate_general_spGrid(t3d_orig,lx,ly,xlat_rot,-90,-90);
end;

% plot for test purpose

[cx,cy]=meshgrid(lx,ly);
x=cos(cx*pi/180).*cos(cy*pi/180);
y=cos(cy*pi/180).*sin(cx*pi/180);
z=sin(cy*pi/180);
figure; subplot(2,1,1); surf(x,y,z,t3d_orig(:,:,30)');
        subplot(2,1,2); surf(x,y,z,t3drt_latlon(:,:,30)');

dims=size(t3drt_latlon); nlon=dims(1); nlat=dims(2); nlay=dims(3);

days=time*dtime/86400;
str1=sprintf('%s %s %s %s:\n','3D temperature for run',runtype,'at step',num2str(time));
str2=sprintf('%s %s %s \n','corresponding to', num2str(days), 'Earth days. File lists longitude, latitude (deg)');
str3=sprintf('%s \n','followed by a list of p (Pa) and T (K) for that column. Blank');
str4=sprintf('%s \n','lines separate different columns.  Number of lon, lat, height points:');
str5=sprintf('%s %s %s \n \n',num2str(nlon), num2str(nlat), num2str(nlay));
str=sprintf('%s %s %s %s %s', str1,str2,str3,str4,str5);

% Convert potential temperature to real temperature

Pref=1.0e5; % reference pressure at 1 bar

t3d_ar=zeros(nlon,nlat,nlay);
t3d_br=zeros(nlon,nlat,nlay);

for k=1:nlay
    t3d_ar(:,:,k)=(rC(k)/Pref)^kappa*t3drt_latlon(:,:,k);
end;

for k=1:nlay
    t3d_br(:,:,k)=(rC(k)/Pref)^kappa*t3d_orig(:,:,k);
end;

fname_ar=sprintf('%s_%s.dat','P_T_rot',num2str(time));
fname_br=sprintf('%s_%s.dat','P_T_orig',num2str(time));

fid1 = fopen(fname_ar, 'a');
fprintf(fid1, '%s',str);

sprintf('Start writing rotated P-T file')

for i=1:nlon
  for j=1:nlat
    fprintf(fid1, '%f   %f\n',lx(i)-180.0/ny/2, ly(j)-180.0/ny/2);
    for k=1:nlay
        fprintf(fid1, '%e   %f\n',rC(k), t3d_ar(i,j,k));
    end;
    fprintf(fid1, '\n');
  end;
end;
  
fclose(fid1);

sprintf('Start writing original P-T file') 

fid2 = fopen(fname_br, 'a');
fprintf(fid2, '%s',str);
 
for i=1:nlon
   for j=1:nlat
    fprintf(fid2, '%f   %f\n',lx(i)-180.0/ny/2, ly(j)-180.0/ny/2);
    for k=1:nlay 
        fprintf(fid2, '%e   %f\n',rC(k), t3d_br(i,j,k));
    end;
    fprintf(fid2, '\n');
  end;
end;
   
fclose(fid2);


