function [t3drt_cs,t3drt_latlon,lx,ly]=rotate_T(t3d,xc,yc,ny);

% rotate cube-sphere grid, the (0,0) point in original 
% grid is rotated to (0,90), here (0,0) and (0,90) means longitude=0 degree,
% latitude=0/90. Also assume (0,0) in original grid is the substellar point.

% t3d is original temperature field or vertical velocity from cube-sphere grid.

% t3drt_cs is the field after rotation in cube-sphere grid.
% t3drt_latlon is the field interpolated to latlon grid, with longitude "lx' and 
% latitude "ly".

% xc and yc are coordinates in cube-sphere grid.
% xc=rdmds('XC'); yc=rdmds('YC');

% dx is the resolution in latlon grid. 
% ie, if use C128, then dx=360/512=0.703125;

% Usage (C128 as an example):  
% [t3drt_cs,t3drt_latlon,lx,ly]=rotate_T(t3d,xc,yc,0.703125);

dims=size(t3d); nx=dims(1); nc=dims(2); ncp=nc+1; n2p=nc+1; nPg=nx*nc ;
if nx == 6*nc, flag=1; else flag=0; end
if flag == 0,
 fprintf(' Error in size of input arrays\n');
 t3drt_cs=0; t3drt_latlon=0; return
end
if length(dims) == 2, nr=1; else nr=prod(dims(3:end)); end

[t3drt_cs]=rotate_ssaxis(t3d,nx,nc,nr);

% define latlon grid

dx=180.0/ny;

lx=-180+dx/2:dx:180-dx/2;
ly=-90+dx/2:dx:90-dx/2;

% inerpolate the CS grid to latlon

[del] = cube2latlon_preprocess(xc,yc,lx,ly);

t3drt_latlon=cube2latlon_fast(del,t3drt_cs);

t3drt_latlon(:,1,:)=2.0*t3drt_latlon(:,2,:)-t3drt_latlon(:,3,:);
t3drt_latlon(:,ny,:)=2.0*t3drt_latlon(:,ny-1,:)-t3drt_latlon(:,ny-2,:);





