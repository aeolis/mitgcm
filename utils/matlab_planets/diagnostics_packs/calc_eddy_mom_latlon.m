function [acc]=calc_eddy_mom_latlon(dyn3d,mom3d,g2d,drC,drF,rC,rF,omega);

% the heating equation follows Karoly (1998)

% the following terms are time-averaged and zonal-mean quantities
% hr -- total heating rate
% dhdt -- diabatic heating
% gamw -- vertical advection of heat
% vdtdy -- horizontal advection of heat
% dvtdy -- horizontal convergence of eddy heat heat flux
% dwtdp -- vertical convergence of eddy heat flux


% cwt -- vertical eddy heat flux (extra terms associated with dwtdp)
% tau -- rediative cooling time scale, in seconds
% tauc -- condensation time scale, in seconds

u3d=squeeze(dyn3d(:,:,:,1));
v3d=squeeze(dyn3d(:,:,:,2));
w3d=squeeze(dyn3d(:,:,:,3));

uv3d=squeeze(mom3d(:,:,:,1));
wu3d=squeeze(mom3d(:,:,:,2));

xc=squeeze(g2d(:,:,1));
yc=squeeze(g2d(:,:,2));
xg=squeeze(g2d(:,:,3));
yg=squeeze(g2d(:,:,4));
dxc=squeeze(g2d(:,:,5));
dyc=squeeze(g2d(:,:,6));
dxg=squeeze(g2d(:,:,7));
dyg=squeeze(g2d(:,:,8));
rAc=squeeze(g2d(:,:,9));
rAz=squeeze(g2d(:,:,10));
rAs=squeeze(g2d(:,:,11));
rAw=squeeze(g2d(:,:,12));

check_var=input('caution: must use time average variables !!!, press 1 to contiue  ');

if (check_var ~= 1)
   error('Stop, use time averaged variables');
end;

dims=size(u3d); nx=dims(1); ny=dims(2);
if length(dims) == 2, nr=1; else nr=prod(dims(3:end)); end;

rF(nr+1)=0.0;

%---calculate horizontal advection of eddy momentum (cell centered)---[v]*d[u]/dy------------------------1

vdudy_v=zeros(ny,nr);

u2d=squeeze(sum(u3d,1))/nx;
v2d=squeeze(sum(v3d,1))/nx;

for k=1:nr,
 vdudy_v([2:ny],k)=v2d([2:ny],k).* (u2d([2:ny],k)-u2d([1:ny-1],k))./(squeeze(dyc(1,[2:ny])))';
end;

% move to cell center

vdudy=zeros(ny,nr);
vdudy([1:ny-1],:)=( vdudy_v([1:ny-1],:)+vdudy_v([2:ny],:) )/2.0;

% filling vdtdy(1,:) and vdtdy(ny,:) with NaNs

vdudy(1,:)=NaN; vdudy(ny,:)=NaN;

%---calculate horizontal eddy momentum flux d/dy([u'v']+[u*v*])---use Physics of climate ---[u'v']+[u*v*]=[uv]-[u][v]----------2

%---test uv-----

%uv_test=zeros(nx,ny,nr);

%for k=1:nr,
% uv_test([2:nx],[2:ny],k)=(u3d([2:nx],[1:ny-1],k)+u3d([2:nx],[2:ny],k)).* ...
%                          (v3d([1:nx-1],[2:ny],k)+v3d([2:nx],[2:ny],k))/4.0;
%end;

%---end test----

uv2d=squeeze(sum(uv3d,1))/nx;

u2d_v=zeros(ny,nr);
u2d_v([2:ny],:)=(u2d([1:ny-1],:)+u2d([2:ny],:))/2.0;
uv_star=uv2d-u2d_v.*v2d;

% d/dy([u'v']+[u*v*]) (cell centered)

duvdy=zeros(ny,nr);

for k=1:nr,
 duvdy([1:ny-1],k)=(uv_star([2:ny],k)-uv_star([1:ny-1],k))./(squeeze(dyg(1,[1:ny-1])))';
end;

% filling dvtdy(1,:) and dvtdy(ny,:) with NaNs

duvdy(1,:)=NaN; duvdy(ny,:)=NaN;

%---calculate vertical eddy momentum flux d/dp([w'u']+[w*u*])---use Physics of climate ---[w'u']+[w*u*]=[wu]-[w][u]----------3

% zonal mean of wu
wu2d=squeeze(sum(wu3d,1))/nx;

% w3d is scaled by area of horizontal grids to location of u, in horizontal.

%w3d_u=zeros(nx,ny,nr);
%for k=1:nr,
%w3d_u([1:nx-1],:,k)=( w3d([1:nx-1],:,k).*rAc([1:nx-1],:)+w3d([2:nx],:,k).*rAc([2:nx],:) )./rAw([1:nx-1],:)/2.0;
%w3d_u(nx,:,k)=( w3d(nx,:,k).*rAc(nx,:)+w3d(1,:,k).*rAc(1,:) )./rAw(1,:)/2.0;
%end;

% zonal mean of w
w2d_u =squeeze(sum(w3d,1))/nx;

% u3d is interpolated to location of w in vertical grid.

u3d_w=zeros(nx,ny,nr);

for k=1:nr,
 km1 = max(k-1,1);
 u3d_w(:,:,k)=(u3d(:,:,k)+u3d(:,:,km1))/2.0;
end;

%---test for wu---
%wu_test=zeros(nx,ny,nr);
%for k=1:nr,
% km1 = max(k-1,1);
% wu_test([2:nx],[1:ny],k)=(u3d([2:nx],[1:ny],k)+u3d([2:nx],[1:ny],km1))/2.0 .* ...
%                          (w3d([1:nx-1],[1:ny],k)+w3d([2:nx],[1:ny],k))/2.0;
%end;

%dwu=wu3d-wu_test;
%figure;
%pcolor(dwu([2:nx],[1:ny],30)');
%---end test-----

% zonal mean of u
u2d_w=squeeze(sum(u3d_w,1))/nx;

wu_star=wu2d-w2d_u.*u2d_w;

% d/dp([w'u']+[w*u*]) (cell centered)

wu_star(:,nr+1)=0.0;

dwudp=zeros(ny,nr);

for k=1:nr,
 dwudp(:,k)=(wu_star(:,k+1)-wu_star(:,k))/drF(k);
end;

%---calculate vertical advection of momentum---[w]d[u]/dp----------------------------------------------------------------------4

wdudp=zeros(ny,nr);

for k=1:nr-1,
 wdudp(:,k)=(w2d_u(:,k+1)+w2d_u(:,k))/2.0.*(u2d_w(:,k+1)-u2d_w(:,k))/drF(k);
end;

%----calculate Coriolis acceleration------f[v]---------------------------------------------------------5

fv=zeros(ny,nr);

v_c=zeros(ny,nr);

v_c([1:ny-1],:)=( v2d([1:ny-1],:)+v2d([2:ny],:) )/2.0;

for k=1:nr,
 fv(:,k)=2.0*omega*sin( reshape(yc(1,:),ny,1)*pi/180.0 ).*v_c(:,k);
end;

acc=zeros(ny,nr,5);
acc(:,:,1)=-duvdy;
acc(:,:,2)=-dwudp;
acc(:,:,3)=-vdudy;
acc(:,:,4)=-wdudp;
acc(:,:,5)=fv;

fprintf('++++++++++++++++++++ \n');

fprintf('terms in accz [ %d x %d x (%d terms) ]:  \n',ny,nr,5);
fprintf('term 1 : meridional convergence of horizontal eddy momentum flux \n');
fprintf('term 2 : vertical convergence of vertical eddy momentum flux \n');
fprintf('term 3 : horizontal momentum advection \n');
fprintf('term 4 : vertical momentum advection\n');
fprintf('term 5 : Coriolis acceleration \n');
