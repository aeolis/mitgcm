% baseline values of diff and vis are equivalent to 
% 24 Titan hours.
% Viscosity operates on horizonatal direction
% Diffusivity operates on vertical direction. Both mimic the 
% wave induced drag and diffusion.

gridtype=input('latlon (0) or cs (1) grid: ');
nlay=input('total number of layers: ');

p2si=16;
rsphere=2575e3;

if gridtype==0
% latlon
   nx=32; ny=48; nz=nlay; nz0=nz-5+1;

   dx=(180/ny)/180*pi*rsphere;
   dphi=180/48;
   yc=[-90+dphi/2:dphi:90-dphi/2];
   %radius=cos(yc/180*pi)*rsphere;

%   diff0=rc(nz0)^2/(24*p2si*3600);
   vis0(1:ny) = dx^2/(24*p2si*3600)./cos(yc/180*pi);
   diff0(1:ny)= rc(nz0)/(2*p2si*3600)./cos(yc/180*pi);
   vis=zeros(nx,ny,nz);
   diff=zeros(nx,ny,nz);
   for k=nz0:nz, for j=1:ny,
     vis(:,j,k)=vis0(j)*(rc(nz0)./rc(k));
     diff(:,j,k)=diff0(j)*(rc(nz0)/rc(k));
   end; end;

elseif gridtype==1
% cube-sphere

%  load yc in cs
%  yc=rdmds('YC');
   ndims=size(yc);
   nx=ndims(1); ny=ndims(2); nz=nlay; nz0=nz-5+1;

   dx=(180/ny/2)/180*pi*rsphere;
   % reference pressure of 1 Pa or rc(nz0)
   rc_ref=rc(nz0);

   % vis0 (108 18) at 1 Pa
   vis0=dx^2/(24*p2si*3600)./cos(yc/180*pi); 
   diff0= rc(nz0)/(2*p2si*3600)./cos(yc/180*pi);
   vis=zeros(nx,ny,nz);
   diff=zeros(nx,ny,nz);
   for k=nz0:nz 
     vis(:,:,k)=vis0*(rc_ref./rc(k));
     diff(:,:,k)=diff0*(rc_ref/rc(k));
   end;

end;

fid=fopen('visch.bin','w');
fwrite(fid,vis,'real*8','b');
fclose(fid);

fid=fopen('diffkr.bin','w');
fwrite(fid,diff,'real*8','b');
fclose(fid);
