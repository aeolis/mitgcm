%without co2 cycle, the model works 
%with box_size=40 and 60 for fspecial

clear all;

cs_path=input('enter the path to cs grid files /~/ : ','s');


ny_orig=180;
nx_orig=ny_orig*2;

deg_size=input('resolution in latlon 5, 2.5, 1.25 or 0.625 : ');

nc=180/deg_size/2;
[lonC0, latC0, lonG, latG, rac]=MITgrid(nc,cs_path);
lonC=lonC0(1:nc,1:nc,:);
latC=latC0(1:nc,1:nc,:);
xg=reshape(permute(lonG(1:nc,1:nc,:),[1 3 2]), [nc*6 nc]);
yg=reshape(permute(latG(1:nc,1:nc,:),[1 3 2]), [nc*6 nc]);

load '~/matlab/Titan/TitanW2S3f.dat';
topo0=reshape(TitanW2S3f',[360 180]);
% shift by 180 grid points so x=-180:180
topo0=circshift(topo0,[180,0]);
% flip N-S to S-N direction
topoe=flipdim(topo0,2);

if (size(topoe,2)~=ny_orig) 
   error('check original resolution of data');
end;

dx_topo=360/(nx_orig-2);
dy_topo=180/ny_orig;
x_topo=[-180-dx_topo/2:dx_topo:180+dx_topo/2];
y_topo(1:ny_orig)=[-90+dy_topo/2:dy_topo:90-dy_topo/2];

rsphere=2575.0E3;

for j=1:ny_orig
dA_topoe(1:nx_orig,j)=rsphere^2*cos(y_topo(j)/180*pi)*dy_topo*pi/180*dx_topo*pi/180;
end;

topoe=topoe.*dA_topoe;

filter_opt=-1; % =1 with smoothing

% Now use a fancier approach for domain expansion
% based on the box size 5, 10 or 20 degree, only 
% works for 5 degree resolution in the model

%nc=36; %the resolution in cs grid is consistent with latlon, c36
nc=180/deg_size/2;

%deg_size=2.5; %resolution of the grids in latlon
nx_cs=360/deg_size;
ny_cs=180/deg_size;

box_size=deg_size; % degree for a single sweeping box
dx_orig=180/ny_orig; % orginal map resolution
nbox_model=deg_size/dx_orig; % number of grid points in a box
nbox_avg=box_size/dx_orig; %number of grid points in sweeping box

nlon_w=(nbox_avg-nbox_model)/2; nlon_e=nlon_w;
nlat_s=(nbox_avg-nbox_model)/2; nlat_n=nlat_s;

topo(nlon_w+1:nx_orig+nlon_e,nlat_s+1:ny_orig+nlat_n)=topoe; % expand the topography array
dA_topo(nlon_w+1:nx_orig+nlon_e,nlat_s+1:ny_orig+nlat_n)=dA_topoe;

topo(nx_orig+nlon_e+1:nx_orig+2*nlon_e,nlat_s+1:ny_orig+nlat_n)=topoe(1:nlon_w,:); 
topo(1:nlon_w,nlat_s+1:ny_orig+nlat_n)=topoe(nx_orig-nlon_e+1:nx_orig,:); % periodic in longitude

dA_topo(nx_orig+nlon_e+1:nx_orig+2*nlon_e,nlat_s+1:ny_orig+nlat_n)=dA_topoe(1:nlon_w,:); 
dA_topo(1:nlon_w,nlat_s+1:ny_orig+nlat_n)=dA_topoe(nx_orig-nlon_e+1:nx_orig,:); % periodic in longitude



%clear topoe; % release topoe in memory

topo(:,1:nlat_s)=topo(:,2*nlat_s:-1:nlat_s+1);
topo(:,ny_orig+nlat_n+1:ny_orig+2*nlat_n)=topo(:,ny_orig+nlat_n:-1:ny_orig+1); % expand in latitude

% box-average the topography to produce 72x36 resolution
for i=1:nx_cs  
for j=1:ny_cs  
box(i,j)=sum(sum(topo((i-1)*nbox_model+1:(i-1)*nbox_model+nbox_avg, ...
            (j-1)*nbox_model+1:(j-1)*nbox_model+nbox_avg)))/ ...
         sum(sum(dA_topo((i-1)*nbox_model+1:(i-1)*nbox_model+nbox_avg, ...
            (j-1)*nbox_model+1:(j-1)*nbox_model+nbox_avg)));
%(nbox_avg^2);
end;
end;

[xm,ym]=find(box==max(max(box)));

if (filter_opt==0)
  box(xm,ym)=(box(xm-1,ym)+box(xm+1,ym)+box(xm,ym-1)+box(xm,ym+1))/4;
end;

% cell centered lat-lon coordinate, add ghost points to 
% longitudinal direction for better interpolation

box_sp(2:nx_cs+1,:)=box;
box_sp(1,:)=box(nx_cs,:);
box_sp(nx_cs+2,:)=box(2,:);

if (filter_opt==1) 
  h=fspecial('disk',5);
  box_sp= imfilter(box_sp,h,'replicate');
end;

dx=deg_size;
xc=[-180-dx/2:dx:180+dx/2];
yc=[-90+dx/2:dx:90-dx/2];


%xcs=rdmds('XC');
%ycs=rdmds('YC');


x6s=lonC; %permute(reshape(xcs,[nc 6 nc]),[1 3 2]);
y6s=latC; %permute(reshape(ycs,[nc 6 nc]),[1 3 2]);

topo_cs=zeros(nc,nc,6);

for ntiles=1:6
 topo_cs(:,:,ntiles)=interp2(yc,xc,box_sp,y6s(:,:,ntiles),x6s(:,:,ntiles));
end;

topo_cs=permute(topo_cs,[1 3 2]);
topo_cs=reshape(topo_cs,[nc*6 nc]);
topo_low=min(min(topo_cs));
topo_cs=(topo_cs-min(min(topo_cs)));

%topo_cs(77,1)=(topo_cs(76,1)+topo_cs(78,1))/2;


if (filter_opt==1)
  h=fspecial('disk',1.5);
  topo_latlon= imfilter(box,h,'replicate');
else
  topo_latlon=box;
end;



topo_latlon=(topo_latlon-min(min(topo_latlon)));

fid=fopen('topo.bin','w');
fwrite(fid,topo_cs,'real*8','b');
fclose(fid);

fid=fopen('topo_latlon.bin','w');
fwrite(fid,topo_latlon,'real*8','b');
fclose(fid);

