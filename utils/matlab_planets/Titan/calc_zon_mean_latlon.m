function [u2d,v2d,T2d,z2d,dz2d,p2d,y2d]=calc_zon_mean_latlon(u3d,v3d,th3d,pe3d,pc3d,yc,phys_para);

atm_po   =phys_para(1);
atm_cp   =phys_para(2);
atm_kappa=phys_para(3);
grv      =phys_para(4);

ndims=size(u3d); nx=ndims(1); ny=ndims(2); nz=ndims(3);
u2d=squeeze(sum(u3d,1)/nx); v2d=squeeze(sum(v3d,1)/nx);
T3d=th3d.*(pc3d/atm_po).^atm_kappa; T2d=squeeze(sum(T3d,1)/nx);

p2d=squeeze(sum(pc3d,1)/nx);
for k=1:nz; y2d(:,k)=squeeze(yc(1,:)); end;

topoZ=zeros(nx,ny);

[zc,dz]=cal_height(th3d, pe3d, topoZ, phys_para);

z2d =squeeze(sum(zc,1)/nx)/1e3;  % km
dz2d=squeeze(sum(dz,1)/nx)/1e3; % km
