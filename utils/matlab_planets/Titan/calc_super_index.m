clear all;

% section 1: load grid files and data configurations
chdatapath=input('full path to load data /~/ : ','s');
cs_latlon=input('cube-sphere grid (c) or latlon grid (s): ','s');
if (cs_latlon=='c') 
   ny_latlon=input('grid points in y in latlon grid: ' );
end;

% get the time intervals etc
deltaT=input('time step: ');
multi_dataset=input('multiple data sets (y/n)? ','s');
tstart=input('starting time step: ');

if isempty(multi_dataset); multi_dataset='y'; end;
if (multi_dataset == 'y')
   tend=input('ending time steps: ');
   dnout=input('Output interval in number of time steps: ');
else
   tend=tstart;
   dnout=1;
end;

% get all data needs to be processed
for k=1:(tend-tstart)/dnout+1
    nstep(k)=tstart+dnout*(k-1);
end;
alldata=round(nstep);

% get grid data
rac=rdmds([chdatapath,'RAC']);
dxc=rdmds([chdatapath,'DXC']);
dyc=rdmds([chdatapath,'DYC']);
xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);
rc=rdmds([chdatapath,'RC']); 
rc=squeeze(rc);

ndims=size(rac); nx=ndims(1); ny=ndims(2); nz=length(rc);

% define physics parameters for Titan
grv=1.358;  rsphere=2575.0E3;
grv=1.358;  atm_Po=1.5E5; atm_cp=1044.0; atm_kappa=0.27778;

% define some constants for calculating the solar angle l_s
p2si=15.95;         equinox_fraction= 0.2098;
planet_year=686.0;  zero_date=542.06;
eccentricity=0.056; semimajoraxis= 9.58256; 

omega=2.*pi/(p2si*86400.); degrad=pi/180.;

% calculate the solar longitude.
SMALL_VALUE=1.0e-6;
deleqn = equinox_fraction * planet_year;
qq = 2.0 * (pi * deleqn / planet_year);
er = sqrt( (1.0+eccentricity)/(1.0-eccentricity) );
%  determine true anomaly at equinox:  eq
%  Iteration for eq
e = 1.0;  cd0 = 1.0;
while (cd0 > SMALL_VALUE)
   ep = e - (e-eccentricity*sin(e)-qq) / (1.0-eccentricity*cos(e));
   cd0 = abs(e-ep); e = ep;
end;
eq = 2.0 * atan( er * tan(0.5*e) );

%For finding angular mom of atm at rest
mmrest=zeros(nx,ny,nz);
for k=1:nz
   mmrest(:,:,k)=omega*rsphere*rsphere*(cos(yc*degrad).^2.);
end;

sidx_out=zeros(4,length(alldata)); suprot_idx_out=zeros(nz,length(alldata));
% start mean loop to calculate superrotation indices and Ls
for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter),'%d');
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        time=allzero
        timed=alldata(niter);
        day(niter)=timed*deltaT/86400/p2si; % this is earth day, nomalized to sol later
        year(niter)=day(niter)/686.;
        % calculate ls  (solar longitude) -------
        julian=day(niter);  date_dbl = julian - zero_date;
        while (date_dbl < 0.) date_dbl=date_dbl+planet_year; end;
        while (date_dbl > planet_year) date_dbl=date_dbl-planet_year; end;

        % determine true anomaly at current date:  w
        % Iteration for w
        em = 2.0 * pi * date_dbl/planet_year;
        e  = 1.0; cd0 = 1.0;
       while (cd0 > SMALL_VALUE)
         ep = e-(e-eccentricity*sin(e)-em) / (1.0-eccentricity*cos(e));
         cd0 = abs(e-ep); e = ep;
       end;
       w = 2.0 * atan( er * tan(0.5*e) );
       % Radius vector ( astronomical units:  AU )
       als= (w - eq)*180.0/pi;      %Aerocentric Longitude
       if (als < 0.0)  als=als+360.0;  end;
       ls(niter) = als;

       % local dynamical variables
       ufile=sprintf('%s%s.%s',chdatapath,'U',     time);  u3d=rdmds(ufile);
       pefile=sprintf('%s%s.%s',chdatapath,'pedyn',time);  pe=rdmds(pefile);
       
       dp=pe(:,:,1:end-1)-pe(:,:,2:end); pc=(pe(:,:,1:end-1)+pe(:,:,2:end))/2.;

       [suprot_idx, sidx]=super_index(u3d, dp, pc, rc, grv, omega, degrad, rsphere, yc, rac, mmrest);

       suprot_idx_out(:,niter)=suprot_idx;   sidx_out(:,niter)=sidx;

end;
