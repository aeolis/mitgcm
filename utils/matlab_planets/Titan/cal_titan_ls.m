function [ls,year]=cal_titan_ls(basetime, niter, deltaT);

%{
bastime: base time to start the new simulation in seconds
niter:   Current iteration number
deltaT:  Time Step in seconds
%}

% define some constants for calculating the solar angle l_s
p2si=15.95;
equinox_fraction= 0.2098;
planet_year=686.0;
zero_date=542.06;
eccentricity=0.056;
semimajoraxis= 9.58256;

SMALL_VALUE=1.0e-6;
deleqn = equinox_fraction * planet_year;
qq = 2.0 * (pi * deleqn / planet_year);
er = sqrt( (1.0+eccentricity)/(1.0-eccentricity) );
%  determine true anomaly at equinox:  eq
%  Iteration for eq
e = 1.0;
cd0 = 1.0;
while (cd0 > SMALL_VALUE)
   ep = e - (e-eccentricity*sin(e)-qq) / (1.0-eccentricity*cos(e));
   cd0 = abs(e-ep);
   e = ep;
end;
eqx = 2.0 * atan( er * tan(0.5*e) );


% calculate ls -------
julian=(basetime+niter*deltaT)/(86400*p2si);
date_dbl = julian - zero_date;
while (date_dbl < 0.)
      date_dbl=date_dbl+planet_year;
end;
while (date_dbl > planet_year)
      date_dbl=date_dbl-planet_year;
end;


%  determine true anomaly at current date:  w
%  Iteration for w
em = 2.0 * pi * date_dbl/planet_year;
e  = 1.0;
cd0 = 1.0;
while (cd0 > SMALL_VALUE)
  ep = e-(e-eccentricity*sin(e)-em) / (1.0-eccentricity*cos(e));
  cd0 = abs(e-ep);
  e = ep;
end;
w = 2.0 * atan( er * tan(0.5*e) );
 
%  Radius vector ( astronomical units:  AU )
als= (w - eqx)*180.0/pi;      %Aerocentric Longitude
if (als < 0.0) 
    als=als+360.0;
end;
ls = als;
year=julian/planet_year;


