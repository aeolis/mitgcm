clear all;

% section 1: load grid files and data configurations
chdatapath=input('full path to load data /~/ : ','s');
ch4cycle=input('include CH4 cycle ? (y/n) ','s');
cs_latlon=input('cube-sphere grid (c) or latlon grid (s): ','s');
if (cs_latlon=='c')
   ny_latlon=input('grid points in y in latlon grid: ' );
end;

deltaT=input('time step: ');
useMultiRec=input('Use multiple-record data(y/n)? ','s');
if(isempty(useMultiRec)||useMultiRec~='y') useMultiRec='n'; end;

% find dump frequency
fid=fopen(sprintf('%s%s',chdatapath,'data'));
while ~feof(fid)
     aa=fgetl(fid); aa=strtrim(aa); xx=strfind(aa,'dumpFreq');
     if(~isempty(xx) && aa(1)~='#') bb=textscan(aa,'%s');
       cc=(strrep(bb{1},'dumpFreq=','')); dumpfreq=str2num(cc{1});
     end;
end;
fclose(fid); clear aa bb cc xx;

if (useMultiRec=='y')
   fid=fopen(sprintf('%s%s',chdatapath,'U.meta'));
else
   tstart=input('starting time step: ');
   if (isempty(tstart)) tstart=0; end;
   str0='0000000000'; str1=num2str(tstart);
   str0(length(str0)-length(str1)+1:end)=str1;
   fid=fopen(sprintf('%s%s%s%s',chdatapath,'U.',str0,'.meta'));
end;

while ~feof(fid)
     aa=fgetl(fid); xx1=strfind(aa,'nrecords'); xx2=strfind(aa,'timeStepNumber');
     if(~isempty(xx1)) bb1=textscan(aa,'%s%s%s%d%s'); nrecords=double(bb1{4}); end;
     if(~isempty(xx2)) bb2=textscan(aa,'%s%s%s%d%s'); tend=double(bb2{4}); end;
end;
fclose(fid); clear aa xx1 xx2 bb1 bb2;

if (useMultiRec=='y')
   tstart=tend-(nrecords-1)*dumpfreq/deltaT;
   myiter(1)=tstart;
   for i=2:nrecords myiter(i)=tstart+(i-1)*dumpfreq/deltaT; end;
   fprintf('%s%d\n','nrecords(end)=',nrecords);
   fprintf('%s%d\n','myIter(end)=',tend);
   dnout=floor((tend-tstart)/max(1,nrecords-1));
else
   dnout=max(1,floor(dumpfreq/deltaT));
end;

% get all data needs to be processed
for k=1:(tend-tstart)/dnout+1
    nstep(k)=tstart+dnout*(k-1);
end;
alldata=round(nstep);

% get grid data
rac=rdmds([chdatapath,'RAC']);
dxc=rdmds([chdatapath,'DXC']);
dyc=rdmds([chdatapath,'DYC']);
dxg=rdmds([chdatapath,'DXG']);
dyg=rdmds([chdatapath,'DYG']);
xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);
xg=rdmds([chdatapath,'XG']);
yg=rdmds([chdatapath,'YG']);
rc=rdmds([chdatapath,'RC']);
rF=rdmds([chdatapath,'RF']);
rc=squeeze(rc); rF=squeeze(rF);

if (cs_latlon=='c') [AngleCS,AngleSN] = cubeCalcAngle(yg,rac,dxg,dyg); end;

sigC=rdmds([chdatapath,'BHybSigC']); sigC=squeeze(sigC);
sigF=rdmds([chdatapath,'BHybSigF']); sigF=squeeze(sigF);
aHybC=rdmds([chdatapath,'AHybSigC']); aHybC=squeeze(aHybC);
aHybF=rdmds([chdatapath,'AHybSigF']); aHybF=squeeze(aHybF);
topoP=rdmds([chdatapath,'topo_P']);
topoZ=rdmds([chdatapath,'topo_H']);

rFullDepth=rF(1)-rF(end);

ndims=size(rac); nx=ndims(1); ny=ndims(2); nz=length(rc);

if (isempty(topoZ)) topoZ=zeros(nx,ny); end;

% define physics parameters
grv=1.358;
atm_Po=1.5E5;
atm_cp=1044.0;
atm_kappa=0.27778;

phys_para(1)=atm_Po;
phys_para(2)=atm_cp;
phys_para(3)=atm_kappa;
phys_para(4)=grv;

p2si=15.95;
omega=2.*pi/(p2si*86400.); degrad=pi/180.;

ndata=length(alldata);
u2dt=zeros(ny,nz,ndata);
v2dt=zeros(ny,nz,ndata);
T2dt=zeros(ny,nz,ndata);
z2dt=zeros(ny,nz,ndata);
p2dt=zeros(ny,nz,ndata);
if (ch4cycle == 'y')
   ptr2dt =zeros(ny,nz,ndata);
   ch4surf=zeros(nx,ny,ndata);
end;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%					   %
%      main loop to get zonal means        %
%					   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter),'%d');
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        time=allzero
        timed=alldata(niter);
	% local dynamical variables
       	if (useMultiRec~='y')
          ufile  =sprintf('%s%s.%s',chdatapath,'U',     time);  u3d=rdmds(ufile);
          vfile  =sprintf('%s%s.%s',chdatapath,'V',     time);  v3d=rdmds(vfile);
          Etafile=sprintf('%s%s.%s',chdatapath,'Eta',   time);  Eta=rdmds(Etafile);
	  tfile  =sprintf('%s%s.%s',chdatapath,'T',     time);  t3d=rdmds(tfile);
	  if (ch4cycle == 'y')
	     ptrfile=sprintf('%s%s.%s',chdatapath,'PTRACER01',   time);  ptr=rdmds(ptrfile);
	     ch4file=sprintf('%s%s.%s',chdatapath,'ch4surf',     time);  ch4surf(:,:,niter)=rdmds(ch4file);
	  end;
       	else
          ufile  =sprintf('%s%s',chdatapath,'U'  );  u3d=rdmds(ufile,'rec',niter);
          vfile  =sprintf('%s%s',chdatapath,'V'  );  v3d=rdmds(vfile,'rec',niter);
          Etafile=sprintf('%s%s',chdatapath,'Eta');  Eta=rdmds(Etafile,'rec',niter);
	  tfile  =sprintf('%s%s',chdatapath,'T'  );  t3d=rdmds(tfile,'rec',niter);
	  if (ch4cycle == 'y')
	     ptrfile=sprintf('%s%s',chdatapath,'PTRACER01');  ptr=rdmds(ptrfile,'rec',niter);
	     ch4file=sprintf('%s%s',chdatapath,'ch4surf'  );  c4s=rdmds(ch4file,'rec',niter); ch4surf(:,:,niter)=c4s;
	  end;
       	end;
       	if (cs_latlon=='c')  [uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C'); u3d=uE; v3d=vN; end;

	for k=1:nz
            pe3d(:,:,k)=aHybF(k)*rFullDepth+sigF(k)*(topoP+Eta);
            pc3d(:,:,k)=aHybC(k)*rFullDepth+sigC(k)*(topoP+Eta);
        end;
        pe3d(:,:,nz+1)=sigF(nz+1)*(topoP+Eta);
        dp3d(:,:,1:nz)=pe3d(:,:,1:nz)-pe3d(:,:,2:nz+1);
	[u2d,v2d,T2d,z2d,dz2d,p2d,y2d]=calc_zon_mean_latlon(u3d,v3d,t3d,pe3d,pc3d,yc,phys_para);

	u2dt(:,:,niter)=u2d;
	v2dt(:,:,niter)=v2d;
	T2dt(:,:,niter)=T2d;
	z2dt(:,:,niter)=z2d;
	p2dt(:,:,niter)=p2d;
	if (ch4cycle=='y')
   	   ptr2dt(:,:,niter)=squeeze(sum(ptr,1)/nx);
	end;

	[Ls(niter),year(niter)]=cal_titan_ls(0, alldata(niter), deltaT);
end;
