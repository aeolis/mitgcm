function [suprot_idx, sidx]=super_index(u3d, dp, press, rc, grv, omega, degrad, radius, yc, rac, mmrest);

ndims=size(u3d);
nx=ndims(1); ny=ndims(2); nz=ndims(3);

mm=zeros(nx,ny,nz);
for k=1:nz
  mm(:,:,k)=omega*radius*radius*(cos(yc*degrad).^2.) + u3d(:,:,k)*radius.*cos(yc*degrad);
end;

%Now want to find superrotation index for each gridpoint

psuprot=zeros(nx,ny,nz);
psuprot=mm./mmrest;

% mass weighted superrotation indices
suprot_mass=zeros(nx,ny,nz); tot_mass(1:nz)=0.0; suprot_idx(1:nz)=0.0;
for k=1:nz
 % angular momentum in each grid points (with mass)
 suprot_mass(:,:,k)=psuprot(:,:,k).*dp(:,:,k).*rac;
 % total mass in each vertical layer
 tot_mass(k)=sum(sum(squeeze(dp(:,:,k)).*rac));
 % mass weighted angular momentum
 suprot_idx(k)=sum(sum(squeeze(suprot_mass(:,:,k))))/tot_mass(k); 
end;

%Then find mass-weighted (mass of layer that is) averages over:
%1) 0-2mbar      => 0-200 Pa
%2) 2-20mbar     => 200-2e3 Pa
%3) 20-200mbar   => 2e3-2e4 Pa
%4) 200-1440mbar => 2e4 Pa - surface (??)

% replace rc with press if there is topography and add loop for i,j
layer1=min(find(rc<=200)); 
layer2=min(find(rc<=2e3));
layer3=min(find(rc<=2e4));
layer4=1; % from surface level (or ksurf if there is topography)

% calculate mass weighted superrotation indices for four pressure ranges
% the top range excludes the top three damped layers
sidx(4)=sum(sum(sum(suprot_mass(:,:,layer4:layer3-1))))/sum(tot_mass(layer4:layer3-1));
sidx(3)=sum(sum(sum(suprot_mass(:,:,layer3:layer2-1))))/sum(tot_mass(layer3:layer2-1));
sidx(2)=sum(sum(sum(suprot_mass(:,:,layer2:layer1-1))))/sum(tot_mass(layer2:layer1-1));
sidx(1)=sum(sum(sum(suprot_mass(:,:,layer1:    nz-3))))/sum(tot_mass(layer1:    nz-3));

%print,'Top layer has pressure range',press(ktop-1),'   to ',press(k1),'Pa'
%print,'Second layer has pressure range',press(k1-1),'   to ',press(k2),'Pa'
%print,'Third layer has pressure range',press(k2-1),'   to ',press(k3),'Pa'
%print,'Bottom layer has pressure range',press(k3-1),'   to ',press(k4),'Pa'


