clear all;

% section 1: load grid files and data configurations
chdatapath=input('full path to load data /~/ : ','s');
cs_latlon=input('cube-sphere grid (c) or latlon grid (s): ','s');
if (cs_latlon=='c') 
   ny_latlon=input('grid points in y in latlon grid: ' );
end;

deltaT=input('time step: ');
useMultiRec=input('Use multiple-record data(y/n)? ','s');
if(isempty(useMultiRec)||useMultiRec~='y') useMultiRec='n'; end;

% find dump frequency
fid=fopen(sprintf('%s%s',chdatapath,'data'));
while ~feof(fid)
     aa=fgetl(fid); aa=strtrim(aa); xx=strfind(aa,'dumpFreq');
     if(~isempty(xx) && aa(1)~='#') bb=textscan(aa,'%s');
       cc=(strrep(bb{1},'dumpFreq=','')); dumpfreq=str2num(cc{1});
     end;
end;
fclose(fid); clear aa bb cc xx;

if (useMultiRec=='y')
   fid=fopen(sprintf('%s%s',chdatapath,'U.meta'));
else
   tstart=input('starting time step: ');
   if (isempty(tstart)) tstart=0; end;
   str0='0000000000'; str1=num2str(tstart);
   str0(length(str0)-length(str1)+1:end)=str1;
   fid=fopen(sprintf('%s%s%s%s',chdatapath,'U.',str0,'.meta'));
end;

while ~feof(fid)
     aa=fgetl(fid); xx1=strfind(aa,'nrecords'); xx2=strfind(aa,'timeStepNumber');
     if(~isempty(xx1)) bb1=textscan(aa,'%s%s%s%d%s'); nrecords=double(bb1{4}); end;
     if(~isempty(xx2)) bb2=textscan(aa,'%s%s%s%d%s'); tend=double(bb2{4}); end;
end;
fclose(fid); clear aa xx1 xx2 bb1 bb2;
if (useMultiRec=='y')
   tstart=tend-(nrecords-1)*dumpfreq/deltaT;
   myiter(1)=tstart;
   for i=2:nrecords myiter(i)=tstart+(i-1)*dumpfreq/deltaT; end;
   fprintf('%s%d\n','nrecords(end)=',nrecords);
   fprintf('%s%d\n','myIter(end)=',tend);
   dnout=floor((tend-tstart)/max(1,nrecords-1));
else
   dnout=max(1,floor(dumpfreq/deltaT));
end;

% get all data needs to be processed
for k=1:(tend-tstart)/dnout+1
    nstep(k)=tstart+dnout*(k-1);
end;
alldata=round(nstep);

% get grid data
rac=rdmds([chdatapath,'RAC']);
dxc=rdmds([chdatapath,'DXC']);
dyc=rdmds([chdatapath,'DYC']);
dxg=rdmds([chdatapath,'DXG']);
dyg=rdmds([chdatapath,'DYG']);
xc=rdmds([chdatapath,'XC']);
yc=rdmds([chdatapath,'YC']);
xg=rdmds([chdatapath,'XG']);
yg=rdmds([chdatapath,'YG']);
rc=rdmds([chdatapath,'RC']); 
rF=rdmds([chdatapath,'RF']);
rc=squeeze(rc); rF=squeeze(rF);

if (cs_latlon=='c') [AngleCS,AngleSN] = cubeCalcAngle(yg,rac,dxg,dyg); end;

sigC=rdmds([chdatapath,'BHybSigC']); sigC=squeeze(sigC);
sigF=rdmds([chdatapath,'BHybSigF']); sigF=squeeze(sigF);
aHybC=rdmds([chdatapath,'AHybSigC']); aHybC=squeeze(aHybC);
aHybF=rdmds([chdatapath,'AHybSigF']); aHybF=squeeze(aHybF);
topoP=rdmds([chdatapath,'topo_P']);
topoZ=rdmds([chdatapath,'topo_H']);

rFullDepth=rF(1)-rF(end);

ndims=size(rac); nx=ndims(1); ny=ndims(2); nz=length(rc);

if (isempty(topoZ)) topoZ=zeros(nx,ny); end;

% define physics parameters for Titan
grv=1.358;  rsphere=2575.0E3;
grv=1.358;  atm_Po=1.5E5; atm_cp=1044.0; atm_kappa=0.27778;

% define some constants for calculating the solar angle l_s
p2si=15.95;         equinox_fraction= 0.2098;
planet_year=686.0;  zero_date=542.06;
eccentricity=0.056; semimajoraxis= 9.58256; 

omega=2.*pi/(p2si*86400.); degrad=pi/180.;

% calculate the solar longitude.
SMALL_VALUE=1.0e-6;
deleqn = equinox_fraction * planet_year;
qq = 2.0 * (pi * deleqn / planet_year);
er = sqrt( (1.0+eccentricity)/(1.0-eccentricity) );
%  determine true anomaly at equinox:  eq
%  Iteration for eq
e = 1.0;  cd0 = 1.0;
while (cd0 > SMALL_VALUE)
   ep = e - (e-eccentricity*sin(e)-qq) / (1.0-eccentricity*cos(e));
   cd0 = abs(e-ep); e = ep;
end;
eq = 2.0 * atan( er * tan(0.5*e) );

%For finding angular mom of atm at rest
mmrest=zeros(nx,ny,nz);
for k=1:nz
   mmrest(:,:,k)=omega*rsphere*rsphere*(cos(yc*degrad).^2.);
end;

sidx_out=zeros(4,length(alldata)); suprot_idx_out=zeros(nz,length(alldata));
% start mean loop to calculate superrotation indices and Ls
for niter=1:length(alldata)
         allzero='0000000000';
         n2s=num2str(alldata(niter),'%d');
         %replace string with different data
         for i=1:length(n2s)
           allzero(length(allzero)-(i-1))=n2s(length(n2s)-(i-1));
         end;

        time=allzero
        timed=alldata(niter);
        day(niter)=timed*deltaT/86400/p2si; % this is earth day, nomalized to sol later
        year(niter)=day(niter)/686.;
        % calculate ls  (solar longitude) -------
        julian=day(niter);  date_dbl = julian - zero_date;
        while (date_dbl < 0.) date_dbl=date_dbl+planet_year; end;
        while (date_dbl > planet_year) date_dbl=date_dbl-planet_year; end;

        % determine true anomaly at current date:  w
        % Iteration for w
        em = 2.0 * pi * date_dbl/planet_year;
        e  = 1.0; cd0 = 1.0;
       while (cd0 > SMALL_VALUE)
         ep = e-(e-eccentricity*sin(e)-em) / (1.0-eccentricity*cos(e));
         cd0 = abs(e-ep); e = ep;
       end;
       w = 2.0 * atan( er * tan(0.5*e) );
       % Radius vector ( astronomical units:  AU )
       als= (w - eq)*180.0/pi;      %Aerocentric Longitude
       if (als < 0.0)  als=als+360.0;  end;
       ls(niter) = als;

       % local dynamical variables
       if (useMultiRec~='y')
          ufile  =sprintf('%s%s.%s',chdatapath,'U',     time);  u3d=rdmds(ufile);
          vfile  =sprintf('%s%s.%s',chdatapath,'V',     time);  v3d=rdmds(vfile);
          Etafile=sprintf('%s%s.%s',chdatapath,'Eta',   time);  Eta=rdmds(Etafile);
       else
          ufile  =sprintf('%s%s',chdatapath,'U'  );  u3d=rdmds(ufile,'rec',niter);
          vfile  =sprintf('%s%s',chdatapath,'V'  );  v3d=rdmds(vfile,'rec',niter);
          Etafile=sprintf('%s%s',chdatapath,'Eta');  Eta=rdmds(Etafile,'rec',niter);
       end;
       if (cs_latlon=='c')  [uE,vN] = rotate_uv2uvEN(u3d,v3d,AngleCS,AngleSN,'C'); u3d=uE; v3d=vN; end;
    
       for k=1:nz
            pedyn(:,:,k)=aHybF(k)*rFullDepth+sigF(k)*(topoP+Eta);
            pcdyn(:,:,k)=aHybC(k)*rFullDepth+sigC(k)*(topoP+Eta);
        end;
        pedyn(:,:,nz+1)=sigF(nz+1)*(topoP+Eta);
        dpdyn(:,:,1:nz)=pedyn(:,:,1:nz)-pedyn(:,:,2:nz+1);      
 
       [suprot_idx, sidx]=super_index(u3d, dpdyn, pcdyn, rc, grv, omega, degrad, rsphere, yc, rac, mmrest);

       suprot_idx_out(:,niter)=suprot_idx;   sidx_out(:,niter)=sidx;

end;
