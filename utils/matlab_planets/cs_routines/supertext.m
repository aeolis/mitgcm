function [ax,h]=supertext(varargin)
%
%SUPERTEXT(X,Y,'string') adds the text in the quotes to location (X,Y)
%    on the global page axes, where (X,Y) is in the range 0 to 1.
%    If X and Y are vectors, TEXT writes the text at all locations
%    given. If 'string' is an array the same number of rows as the
%    length of X and Y, TEXT marks each point with the corresponding row
%    of the 'string' array.
%
%Returns a handle to the title and the handle to an axis.
% [ax,h]=supertext(text)
%           returns handles to both the axis and the title.
% ax=supertext(text)
%           returns a handle to the axis only.
%
%See TEXT for more help.

%ax=axes('Units','Normal','Position',[.075 .075 .85 .85],'Visible','off');
xmg=0.03;
ymg=0.03;
ax=axes('Units','Normal','Position',[xmg ymg 1-2*xmg 1-2*ymg],'Visible','off');
set(get(ax,'Title'),'Visible','on')
text(varargin{:})
if (nargout < 2)
  return
end
h=get(ax,'Title');
