function [c,h]=mycoutourf(x,y,a,ci,varargin)
% mycontourf(x,y,a,ci)


% Choose contour levels
if prod(size(ci))==1
 amin=min(min(a));
 amax=max(max(a));
 cmin=floor(amin/ci)*ci;
 cmax=ceil(amax/ci)*ci;
 if cmin==cmax
  error(sprintf('cmin=cmax=%g',cmin));
 end
 if cmin<0 & cmax>0
  cmax=max(abs([cmin cmax]));
  cmin=-cmax;
 end
 CI=cmin:ci:cmax;
else
 CI=ci;
end
cmin=min(CI);
cmax=max(CI);

[c,h]=contourf(x,y,a,CI,varargin{:});
caxis([cmin cmax]);
