% f77write(filename,array,options) writes an array to filename
%  filename - is a string containing the name of the file to be read
%  array    - is a array containing of arbitrary dimensions
%  options  - (optional) specifies the data-type or the binary format
%
%  Default data-type and binary format are 'real*8' and 'native'.
%
% Other data types can be one of:
%       'real*4' - floating point, 32 bits.
%       'real*8' - floating point, 64 bits.
%
% Other binary formats can be one of:
%    'native'      or 'n' - local machine format - the default
%    'ieee-le'     or 'l' - IEEE floating point with little-endian
%                           byte ordering
%    'ieee-be'     or 'b' - IEEE floating point with big-endian
%                           byte ordering
%    'cray'        or 'c' - Cray floating point with big-endian
%                           byte ordering
%    'ieee-le.l64' or 'a' - IEEE floating point with little-endian
%                           byte ordering and 64 bit long data type
%    'ieee-be.l64' or 's' - IEEE floating point with big-endian byte
%                           ordering and 64 bit long data type.
%
%
% eg.  Write a real*8 dataset of size 160 x 10 x 16
%        f77write('T.bin',A);
%
% eg.  Write a real*4 dataset of size 120 x 45 in Little endian format
%        f77write('T.bin',B,'real*4','l');
%
function [] = read3d(file,arr,varargin)

% Defaults
rtype='real*8'; % This is the default type of data
ieee='n'; % This is the default binary representation

% Check optional arguments
args=char(varargin);
while (size(args,1) > 0)
 if deblank(args(1,:)) == 'real*4'
  rtype='real*4';
 elseif deblank(args(1,:)) == 'real*8'
  rtype='real*8';
 elseif deblank(args(1,:)) == 'n' | deblank(args(1,:)) == 'native'
  ieee='n';
 elseif deblank(args(1,:)) == 'l' | deblank(args(1,:)) == 'ieee-le'
  ieee='l';
 elseif deblank(args(1,:)) == 'b' | deblank(args(1,:)) == 'ieee-be'
  ieee='b';
 elseif deblank(args(1,:)) == 'c' | deblank(args(1,:)) == 'cray'
  ieee='c';
 elseif deblank(args(1,:)) == 'a' | deblank(args(1,:)) == 'ieee-le.l64'
  ieee='a';
 elseif deblank(args(1,:)) == 's' | deblank(args(1,:)) == 'ieee-be.l64'
  ieee='s';
 else
  sprintf(['Optional argument ' args(1,:) ' is unknown'])
  return
 end
 args=args(2:end,:);
end

if rtype == 'real*4'
 wordsize=4;
elseif rtype == 'real*8'
 wordsize=8;
else
 sprintf('Somehow, rtype has an illegal value...!')
 return
end
fid=fopen(file,'w',ieee);
% write the initial 4 bytes used in f77_unformatted
fwrite(fid,prod(size(arr))*wordsize,'uint32');
% write the data
fwrite(fid,arr,rtype);
% write the last 4 bytes used in f77_unformatted
fwrite(fid,prod(size(arr))*wordsize,'uint32');
% close the file
st=fclose(fid);
