function [Q]=rescale_coordinate(q,varargin)
% [Q]=rescale_coordinate(q)
% [Q]=rescale_coordinate(q,'conf')
% [Q]=rescale_coordinate(q,'q=1')
% [Q]=rescale_coordinate(q,'q=0')
% [Q]=rescale_coordinate(q,'tan')
% [Q]=rescale_coordinate(q,'tan2')

nxf=prod(size(q));
nx=round((nxf-1)/2)+1;

% Generate gridded face using pure conformal mapping
%q=-min(q):2/(nxf-1):max(q); %q=sign(q).*(abs(q).^(1.2));

% Calculate dxg along edge for original (unscaled) conformal cube
dxg=conf_dx(q);

if nargin > 1
 switch varargin{1}
 case 'q=0'
  D=max(dxg,[],2);
 case 'q=1/2'
  D=dxg(:,round(nx/2));
 case 'q=78'
  D=dxg(:,round(nx/8));
 case 'q=1'
  D=min(dxg,[],2);
 case 'q=i3'
  D=dxg(:,3);
 otherwise
  D=dxg(:,1);
 end
 s=cumsum([0 D']);
 dS=max(s)/(nxf-1)+0*D;
 S=cumsum([0 dS']);
end


if nargin > 1
 switch varargin{1}
 case 'conf'
  Q=q;
 case {'q=0','q=1','q=1/2','q=7/8','q=i3'}
  Q=interp1(s,q,S);
 case 'tan'
  Q=tan(2/3*q)/tan(2/3*max(abs(q)));
 case 'tan2'
  Q=tan(1/5*q)/tan(1/5*max(abs(q)));
 case 'new'
  dq=ones(1,nxf);
  dq(1)=0;
  dq(2)=1.5;
  dq=dq/sum(dq)*(q(end)-q(1));
  Q=q(1)+cumsum(dq);
 otherwise
  disp('Unknown method')
 end
else
 Q=q;
end
