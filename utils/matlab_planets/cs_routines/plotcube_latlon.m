function [] = plotcube_latlon(XX,YY,C)
% plotcube_latlon(x,y,c)
%
% Plots cubed-sphere data in 3D on sphere. (x,y) are
% coordinates, c is cell-centered scalar to be plotted.

XX(end+1)=XX(1);
YY(end+1)=NaN;

[cx,cy]=meshgrid(XX,YY);

XX=cx';
YY=cy';

z=sin(YY*pi/180);
x=cos(YY*pi/180).*cos(XX*pi/180);
y=cos(YY*pi/180).*sin(XX*pi/180);

surf(x(:,:),y(:,:),z(:,:),C(:,:),'linestyle','none')


