function [] = plotcube(X,Y,Z,C)

n=size(X,3);

surf(X(:,:,1),Y(:,:,1),Z(:,:,1),C(:,:,1))
hold on
for j=2:n
 surf(X(:,:,j),Y(:,:,j),Z(:,:,j),C(:,:,j))
end
hold off
xlabel('X');
ylabel('Y');
zlabel('Z');
