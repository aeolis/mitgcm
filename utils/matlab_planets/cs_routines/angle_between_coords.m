function [angle]=angle_between_coords(X1,Y1,Z1,X2,Y2,Z2)
% [angle]=angle_between_coords(X1,Y1,Z1,X2,Y2,Z2);

vector_prod=X1.*X2+Y1.*Y2+Z1.*Z2;

nrm1=X1.*X1+Y1.*Y1+Z1.*Z1;
nrm2=X2.*X2+Y2.*Y2+Z2.*Z2;

angle=acos( vector_prod./sqrt(nrm1.*nrm2) );
