function [X,Y,Z]=rotate_about_xaxis(lX,lY,lZ,angle);
% [X,Y,Z]=rotate_about_xaxis(lX,lY,lZ,angle);
% Rotate about X axis by "angle".


s=sin(angle);
c=cos(angle);
if c<1e-9
 c=0;
 s=sign(s);
end

X=lX;
Y=c*lY-s*lZ;
Z=s*lY+c*lZ;
