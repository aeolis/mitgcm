function [A] = expand(a)
% [A] = expand(a);
%
% A(:,:,j) = a  for j=1:6

for j=1:6;
 A(:,:,j)=a;
end
