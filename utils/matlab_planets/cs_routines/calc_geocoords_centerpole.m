function [lat,lon] = calc_geocoords_centerpole(lx,ly,tile)
% [lat,lon] = calc_geocoords_centerpole(lx,ly,tile);
%
% Calculates geographic coordinates for faces of cube
% with curvilinear coordinates (lx,ly)
%
% Meant to be used for quarter tile only

nx=size(lx,1);
nxf=2*nx-1;

% 3D coordinates on unit sphere
[lX,lY,lZ]=map_xy2xyz(lx,ly);

% Use symmetry to expand to full cube
%lX(nx:nxf,:)=-lX(nx:-1:1,:); lX(:,nx:nxf)=lX(:,nx:-1:1);
%lY(nx:nxf,:)=lY(nx:-1:1,:); lY(:,nx:nxf)=-lY(:,nx:-1:1);
%lX=(lX+lY')/2; lY=lX';
%lZ=(lZ+lZ')/2;
%lZ(nx:nxf,:)=lZ(nx:-1:1,:); lZ(:,nx:nxf)=lZ(:,nx:-1:1);

% Polar tile(s)
[lonP,latP]=map_xyz2lonlat(lX,lY,lZ);
% Enforce symmetry
latP=(latP+latP')/2;	% Does nothing
lonP( find(lonP>=pi) )=lonP( find(lonP>=pi) )-2*pi;
lonP=(lonP-3/2*pi-lonP')/2;
for j=1:nx; lonP(j,j)=-3/4*pi; end;
% Use symmetry to expand to full cube
latP(nx:nxf,:)=latP(nx:-1:1,:);
latP(:,nx:nxf)=latP(:,nx:-1:1);
lonP(nx:nxf,:)=-pi-lonP(nx:-1:1,:);
lonP(:,nx:nxf)=-lonP(:,nx:-1:1);

[lX,lY,lZ]=rotate_about_xaxis(lX,lY,lZ,pi/2);
[lonE,latE]=map_xyz2lonlat(lX,lY,lZ);
% Enforce symmetry
lonE(1,:)=-3/4*pi;
latE(:,end)=0;
latE(:,1)=-latP(1:nx,1);
% Use symmetry to expand to full cube
latE(nx:nxf,:)=latE(nx:-1:1,:);
latE(:,nx:nxf)=-latE(:,nx:-1:1);
lonE(nx:nxf,:)=-pi-lonE(nx:-1:1,:);
lonE(:,nx:nxf)=lonE(:,nx:-1:1);

% Convert to geographic coordinates
switch tile
 case {1}
   lat=latE;
   lon=lonE-pi/2; lon(find(lon<=-pi))=lon(find(lon<=-pi))+2*pi;
 case {2}
   lat=latE;
   lon=lonE;
 case {3}
   lat=latP;
   lon=lonP;
 case {4}
   lat=latE(:,end:-1:1)';
   lon=lonE'+pi/2;
 case {5}
   lat=latE(:,end:-1:1)';
   lon=lonE'+pi;
 case {6}
   lat=-latP;
   lon=lonP(end:-1:1,end:-1:1)';
 otherwise
  tile
  error('Illegal value for tile')
end
