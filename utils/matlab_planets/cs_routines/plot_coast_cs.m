function [L] = plot_coast_cs(Radius)

load('~/matlab/bin/m_coasts.mat');

  X=ncst(:,1); Y=ncst(:,2);
  for i=1:length(k)-1,
    xl=X(k(i)+1:k(i+1)-1);
    fk=finite(xl);
    if any(fk),
      yl=Y(k(i)+1:k(i+1)-1);
      nx=length(xl);     
      zL=Radius*sin(yl*pi/180);
      xL=Radius*cos(yl*pi/180).*cos(xl*pi/180);
      yL=Radius*cos(yl*pi/180).*sin(xl*pi/180);  
     %line(xL,yL,zL,'color',[0 0 0]);
      L(i)=line(xL,yL,zL);
     end
  end
% set(L,'Color',[0 0 0],'LineWidth',2);
% set(L,'LineStyle','-'); 
return
