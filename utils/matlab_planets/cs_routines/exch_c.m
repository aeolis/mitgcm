function [theta] = exch_c(Theta)

theta=NaN*zeros([ size(Theta,1)+2 size(Theta,2)+2 size(Theta,3) ]);
theta(2:end-1,2:end-1,:)=Theta;

theta(end,:,[1 3 5])=theta(2,:,[2 4 6]);
theta(1,:,[1 3 5])=theta(end:-1:1,end-1,[5 1 3]);
theta(:,end,[1 3 5])=theta(2,end:-1:1,[3 5 1]);
theta(:,1,[1 3 5])=theta(:,end-1,[6 2 4]);
theta(:,end,[2 4 6])=theta(:,2,[3 5 1]);
theta(:,1,[2 4 6])=theta(end-1,end:-1:1,[6 2 4]);
theta(end,:,[2 4 6])=theta(end:-1:1,2,[4 6 2]);
theta(1,:,[2 4 6])=theta(end-1,:,[1 3 5]);
