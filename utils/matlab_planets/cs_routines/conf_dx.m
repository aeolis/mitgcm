function [dxg] = conf_d(qq);
% calculate dx along edge of conformal cube (used for re-scaling coordinate)

nx=prod(size(qq));
nxf=2*(nx-1)+1;
q(1:2:nxf)=qq;
q(2:2:nxf-1)=(q(1:2:nxf-1)+q(2:2:nxf))/2;

j=1:nx-1;jm1=j-1;jm1(1)=nx-1;
J=1:nx;Jm1=J-1;Jm1(1)=nx-1;
J=j;

% Generate gridded face using pure conformal mapping
[lx,ly]=ndgrid( q, q );	% Local grid coordinate
[lX,lY,lZ]=map_xy2xyz(lx,ly);	% 3D coordinates on unit sphere

% Symmetrize
%lX=(lX-lX(end:-1:1,:))/2; lX=(lX+lX(:,end:-1:1))/2;
%lY=(lY+lY(end:-1:1,:))/2; lY=(lY-lY(:,end:-1:1))/2;
%lZ=(lZ+lZ(end:-1:1,:))/2; lZ=(lZ+lZ(:,end:-1:1))/2;

% Use "vector" data type
XG=lX(1:2:end,1:2:end);YG=lY(1:2:end,1:2:end);ZG=lZ(1:2:end,1:2:end);
Vertices=coord2vector(XG,YG,ZG);
dxg=angle_between_vectors(Vertices(j,J,:),Vertices(j+1,J,:));
