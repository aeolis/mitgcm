function [X,Y,Z] = cmap_xy2xyz(x,y);
% Conformal mapping of a cube onto a sphere
% maps (x,y) on the north-pole face of a cube
% to (X,Y,Z) coordinates in physical space
%
% Based on f77 code from Jim Purser & Misha Rancic.
%
% Face is oriented normal to Z-axis with
% X and Y increasing with x and y
%
% valid ranges:  -1 < x < 1   -1 < y < 1

if size(x) ~= size(y)
 error('Arguments x,y should be same size');
end

xc=abs(x);
yc=abs(y);

kx=find(x<0);
ky=find(y<0);
kxy=find(yc>xc);

X=xc;
Y=yc;
xc=1-xc;
yc=1-yc;
xc(kxy)=1-Y(kxy);
yc(kxy)=1-X(kxy);

z=((xc+i*yc)/2).^4;
W=WofZ(z);

thrd=1/3;
i3=i^thrd;
ra=sqrt(3)-1;
cb=i-1;
cc=ra*cb/2;

W=i3*(W*i).^thrd;
W=(W-ra)./(cb+cc*W);
X=real(W);
Y=imag(W);
H=2./(1+X.^2+Y.^2);
X=X.*H;
Y=Y.*H;
Z=H-1;

T=X;
X(kxy)=Y(kxy);
Y(kxy)=T(kxy);

Y(ky)=-Y(ky);
X(kx)=-X(kx);

% Fix truncation for x=0 or y=0 (aja)
X(find(x==0))=0;
Y(find(y==0))=0;

function [w] = WofZ(z);
% evaluates the Taylor series W(z)

A=Acoeffs;

w=0;
for j=30:-1:1,
 w=(w+A(j)).*z;
end
stats(abs(z))

function [A] = Acoeffs;
% Taylor coefficients for W(z)
%aja changed  -0.00895884801823 to -0.01895884801823 (coeff #4)
A=[1.47713057321600
  -0.38183513110512
  -0.05573055466344
  -0.01895884801823
  -0.00791314396396
  -0.00486626515498
  -0.00329250387158
  -0.00235482619663
  -0.00175869000970
  -0.00135682443774
  -0.00107458043205
  -0.00086946107050
  -0.00071604933286
  -0.00059869243613
  -0.00050696402446
  -0.00043418115349
  -0.00037537743098
  -0.00032745130951
  -0.00028769063795
  -0.00025464473946
  -0.00022659577923
  -0.00020297175587
  -0.00018247947703
  -0.00016510295548
  -0.00014967258633
  -0.00013660647356
  -0.00012466390509
  -0.00011468147908
  -0.00010518717478
  -0.00009749136078
];

function [B] = Bcoeffs
% Taylor coefficients for Z(w)
B=[0.67698822171341
   0.11847295533659
   0.05317179075349
   0.02965811274764
   0.01912447871071
   0.01342566129383
   0.00998873721022
   0.00774869352561
   0.00620347278164
   0.00509011141874
   0.00425981415542
   0.00362309163280
   0.00312341651697
   0.00272361113245
   0.00239838233411
   0.00213002038153
   0.00190581436893
   0.00171644267546
   0.00155493871562
   0.00141600812949
   0.00129556691848
   0.00119042232809
   0.00109804804853
   0.00101642312253
   0.00094391466713
   0.00087919127990
   0.00082115825576
   0.00076890854394
   0.00072168520663
   0.00067885239089
];
