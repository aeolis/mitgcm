% Similar to pcolor except that pcol() doesn't drop the last column
% or row of data (ie. doesn't interpolated). It uses the shading flat
% method by default.
%
% See also  PCOLOR, IMAGESC
function [hh] = pcol(varargin);

%cmap=colormap;
%if cmap(size(cmap,1),:)==[0 0 0]
%else
% if sum(sum(isnan(data)))~=0
%  colormap( [colormap' [0 0 0]']')
% end
%end

% Scale data to fit colormap
%clim=[min(min(data)) max(max(data))];
%data=(data-clim(1))/(clim(2)-clim(1))*(size(colormap,1)-1)+1;
%data(find(isnan(data)==1))=size(colormap,1)+1;

if nargin == 1
%hh=imagesc(data);
 data=varargin{1};
 pcolor(data([1:end 1],[1:end 1]))
else
%hh=imagesc(varargin{1:2},data);
 x=varargin{1};x=x(:);
 y=varargin{2};y=y(:);
 data=varargin{3};
 pcolor([x' 2*x(end)-x(end-1)],...
        [y' 2*y(end)-y(end-1)],...
        data([1:end 1],[1:end 1]))
end
%set(gca,'YDir','normal')
shading flat;
