function [X,Y,Z]=map_lonlat2xyz(lon,lat)
% [X,Y,Z]=map_lonlat2xyz(lon,lat);
%
% Convert spherical coordinates [lon,lat] to 3-D coordinates [x,y,z]
%
% Assumes "lat" is positive with "z", equatorial plane
% falls at z=0  and "lon" is measured anti-clockwise (eastward)
% from x-axis (y=0) about z-axis.

% Latitude
Req=cos(lat);
Z=sin(lat);
X=Req.*cos(lon);
Y=Req.*sin(lon);
