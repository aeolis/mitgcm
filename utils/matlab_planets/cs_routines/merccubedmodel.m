function [] = merccube(XX,YY,C)

[nnx ny]=size(C);

if max(max(max(YY)))-min(min(min(YY))) < 3*pi
 fac=180/pi;
else
 fac=1;
end
X(1:ny,1:ny,1)=fac*XX(0*ny+(1:ny),1:ny);
X(1:ny,1:ny,2)=fac*XX(1*ny+(1:ny),1:ny);
X(1:ny,1:ny,3)=fac*XX(2*ny+(1:ny),1:ny);
X(1:ny,1:ny,4)=fac*XX(3*ny+(1:ny),1:ny);
X(1:ny,1:ny,5)=fac*XX(4*ny+(1:ny),1:ny);
X(1:ny,1:ny,6)=fac*XX(5*ny+(1:ny),1:ny);
X(end+1,:,:)=2*X(end,:,:)-X(end-1,:,:);
X(:,end+1,:)=2*X(:,end,:)-X(:,end-1,:);
Y(1:ny,1:ny,1)=fac*YY(0*ny+(1:ny),1:ny);
Y(1:ny,1:ny,2)=fac*YY(1*ny+(1:ny),1:ny);
Y(1:ny,1:ny,3)=fac*YY(2*ny+(1:ny),1:ny);
Y(1:ny,1:ny,4)=fac*YY(3*ny+(1:ny),1:ny);
Y(1:ny,1:ny,5)=fac*YY(4*ny+(1:ny),1:ny);
Y(1:ny,1:ny,6)=fac*YY(5*ny+(1:ny),1:ny);
Y(end+1,:,:)=2*Y(end,:,:)-Y(end-1,:,:);
Y(:,end+1,:)=2*Y(:,end,:)-Y(:,end-1,:);

Q(1:ny,1:ny,1)=C(0*ny+(1:ny),1:ny);
Q(1:ny,1:ny,2)=C(1*ny+(1:ny),1:ny);
Q(1:ny,1:ny,3)=C(2*ny+(1:ny),1:ny);
Q(1:ny,1:ny,4)=C(3*ny+(1:ny),1:ny);
Q(1:ny,1:ny,5)=C(4*ny+(1:ny),1:ny);
Q(1:ny,1:ny,6)=C(5*ny+(1:ny),1:ny);
Q(end+1,:,:)=0;
Q(:,end+1,:)=0;

[nx ny nt]=size(X)

hnx=ceil(nx/2);
hny=ceil(ny/2);

k=1;
pcolor(X(:,:,k),Y(:,:,k),Q(:,:,k))

axis([min(min(min(X))) max(max(max(X))) min(min(min(Y))) max(max(max(Y)))]);
hold on

k=2;
pcolor(X(:,:,k),Y(:,:,k),Q(:,:,k))

k=3;
pcolor(mod(X(:,1:hnx,k)+360,360),Y(:,1:hnx,k),Q(:,1:hnx,k))
x=mod(X(:,hnx:nx,k)+359.999,360)-359.999;
x(find(x<-180))=x(find(x<-180))+180;
pcolor(x,Y(:,hnx:nx,k),Q(:,hnx:nx,k))

k=4;
pcolor(mod(X(:,:,k)+360,360),Y(:,:,k),Q(:,:,k))
pcolor(mod(X(:,:,k)+360,360)-360,Y(:,:,k),Q(:,:,k))

k=5;
pcolor(X(:,:,k),Y(:,:,k),Q(:,:,k))

k=6;
x=mod(X(1:hnx,:,k)+359.999,360)-359.999;
x(find(x<-180))=x(find(x<-180))+180;
pcolor(x,Y(1:hnx,:,k),Q(1:hnx,:,k))
pcolor(mod(X(hnx:nx,:,k)+360,360),Y(hnx:nx,:,k),Q(hnx:nx,:,k))

hold off
