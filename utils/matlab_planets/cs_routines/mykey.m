% key.m
%
% key(linestyle1,label1, [linestyle2,label2,] ...)
%
function [] = key(varargin)
nargs=nargin;
if mod(nargs,2) ~= 0
 sprintf('Error: needs even number of arguments')
 return
end

ax=axis;
dx=1/15*(ax(2)-ax(1));
rm=ax(2)-dx/2;
lm=rm-4*dx;
dy=1/15*(ax(4)-ax(3));
bm=ax(3)+dy/2;
tm=bm+nargs/2*dy;

hold on
patch([lm-1.1*dx rm rm lm-1.1*dx],[tm+dy/2 tm+dy/2 tm-nargs/2*dy tm-nargs/2*dy],'w')
hold off

hold on
for j=1:2:nargs-1;
 y=tm-(j-0.5)*dy/2;
 plot([lm-dx lm-0.1*dx],y*[1 1],varargin{j})
 text(lm,y,varargin{j+1})
end
hold off
