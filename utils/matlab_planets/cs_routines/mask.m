function [msk] = mask( T )
%
% msk=mask(T);
%
% return array of same shape as T with 0/1 indicating non-zero elements
% of T.

msk=0*T;
msk(find(T~=0))=1;
msk(isnan(T))=0;
