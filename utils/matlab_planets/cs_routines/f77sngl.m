% read3d(filename,nx,ny,nz) returns an array size(nx,ny,nz) read
% from a f77_unformatted the binary file (double precision) named
% by the string filename.
%
% eg.   temp=read3d('T.bin',160,10,16);
%
function [arr] = read3d(file,nx,ny,nz)
fid=fopen(file,'r','n');
q=fread(fid,1,'uint32'); % read the initial 8 bytes used in f77_unformatted
arr=fread(fid,nx*ny*nz,'float32');
arr=reshape(arr,nx,ny,nz);
st=fclose(fid);
