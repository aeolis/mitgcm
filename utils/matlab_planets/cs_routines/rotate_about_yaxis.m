function [X,Y,Z]=rotate_about_yaxis(lX,lY,lZ,angle);
% [X,Y,Z]=rotate_about_yaxis(lX,lY,lZ,angle);
% Rotate about Y axis by "angle".

s=sin(angle);
c=cos(angle);
if c<1e-9
 c=0;
 s=sign(s);
end


X=c*lX+s*lZ;
Y=lY;
Z=-s*lX+c*lZ;
