%path('/home/dimitri/matlab/adcroft/bin',path);

%for typ={'conf','q=0','q=1','q=1/2','q=7/8','q=i3','tan','tan2','new'}

for typ={'tan'}

[dxg,dyg,dxf,dyf,dxc,dyc,dxv,dyu,Ec,Eu,Ev,Ez,latC,lonC,latG,lonG,...
 Q11,Q22,Q12, TUu,TUv,TVu,TVv ]=gengrid_fn(360,4,typ{1},'c',0,1);
theta=zeros(180,180);
for i=1:180
 for j=1:180
  lat=[latG(i,j) latG(i+1,j)]*180/pi;
  lon=[lonG(i,j) lonG(i+1,j)]*180/pi;
  a=dist(lat,lon,'sphere');
  lat=[latG(i,j) latG(i,j+1)]*180/pi;
  lon=[lonG(i,j) lonG(i,j+1)]*180/pi;
  b=dist(lat,lon,'sphere');
  lat=[latG(i+1,j) latG(i,j+1)]*180/pi;
  lon=[lonG(i+1,j) lonG(i,j+1)]*180/pi;
  c=dist(lat,lon,'sphere');
  theta(i,j)=acos((a^2+b^2-c^2)/2/a/b);
 end
end
disp(typ{1})
disp(minmax(dxg*6371))
disp(minmax(dxg(1:180,1:180)./dyg(1:180,1:180)))
tmp=theta*180/pi; tmp(1,1)=nan;
disp(minmax(tmp))
mypcolor(theta*180/pi),colorbar
title(typ{1})
tmp=typ{1}; tmp(strfind(tmp,'/'))='_';
eval(['print -dpsc FIG' tmp])

end
