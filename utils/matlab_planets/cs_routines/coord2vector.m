function [vec]=coord2vector(x,y,z)
% [vec]=coord2vector(x,y,z);

vec(:,:,:,1)=x;
vec(:,:,:,2)=y;
vec(:,:,:,3)=z;
