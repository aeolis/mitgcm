function [lon,lat]=map_xyz2lonlat(x,y,z)
% Convert 3-D coordinates [x,y,z] to [lon,lat]
%
% Assumes "lat" is positive with "z", equatorial plane
% falls at z=0  and "lon" is measured anti-clockwise (eastward)
% from x-axis (y=0) about z-axis.

% Latitude
Req=sqrt( x.^2 + y.^2 );
Z=z;
ii=find(Req == 0);
Z(ii)=Z(ii)*Inf;
Req(ii)=1;
lat=atan( Z ./ Req );

% Longitude
X=x;
Y=y;
ii=find(x == 0);
Y(ii)=Inf;
X(ii)=1;
lon=atan( Y ./ X );

ii=find(x<0 & y>=0);
lon(ii)=pi+lon(ii);
ii=find(x<=0 & y<0);
lon(ii)=-pi+lon(ii);
