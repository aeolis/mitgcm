function [] = write_tiles(fn,A,varargin)
% Write tiled field to 6 files "fn.00?.001.data".
%
% Data is in IEEE-bigendian real*8 format by default.
%
% Dimensions of A must be (n,n,6)
%
% Usage:
% >>  write_tiles('LATC',latC);
% >>  write_tiles('LATC',latC,'real*4');

% Default precision
prec='real*8';
ieee='b';

% Check optional arguments
args=char(varargin);
while (size(args,1) > 0)
 if deblank(args(1,:)) == 'real*4' | deblank(args(1,:)) == 'float32'
  prec='real*4';
 elseif deblank(args(1,:)) == 'real*8' | deblank(args(1,:)) == 'float64'
  prec='real*8';
 else   
  sprintf(['Optional argument ' args(1,:) ' is unknown'])
  return
 end    
 args=args(2:end,:);
end     

for n=1:6,

fid=fopen(sprintf('%s.%3.3i.bin',fn,n),'w',ieee);
if fid<0
  error('Unable to create file')
end
fwrite(fid,A(:,:,n),prec);
fclose(fid);

end
