function [dxg,dxc,dxf,dxv]=reduce_dx(dx,nratio);
% [dxg,dxc,dxf,dxv]=reduce_dx(dx,nratio);
%
% Sum dx on fine grid -> dxg,dxc,dxf,dxv
% nratio is ratio of grid sizes (e.g. nratio ~ dxg/dx)

if nratio==1
 dxg=dx;
 dxf=(dx(:,1:end-1)+dx(:,2:end))/2;
 dxv=(dx([end 1:end],:)+dx([1:end 1],:))/2;
 dxc=(dxf([end 1:end],:)+dxf([1:end 1],:))/2;
 return
end

[nxf]=size(dx,2);

if nratio/2 ~= floor(nratio/2)
 nratio
 error('nratio must be multiple of 2 to be able to reduce grid');
end

kg=1:nratio:nxf;
kc=1+nratio/2:nratio:nxf;
jg=1:nratio:nxf-1;
jc=[nxf-nratio/2 1+nratio/2:nratio:nxf];
dxg=dx(jg,kg);
dxf=dx(jg,kc);
dxc=dx(jc,kc);
dxv=dx(jc,kg);
for n=2:nratio;
 jg=mod(jg,nxf-1)+1;
 jc=mod(jc,nxf-1)+1;
 dxg=dxg+dx(jg,kg);
 dxf=dxf+dx(jg,kc);
 dxc=dxc+dx(jc,kc);
 dxv=dxv+dx(jc,kg);
end

% Force more symmetry
dxf=(dxf+dxf(end:-1:1,:))/2;
dxf=(dxf+dxf(:,end:-1:1))/2;
dxg=(dxg+dxg(end:-1:1,:))/2;
dxg=(dxg+dxg(:,end:-1:1))/2;
dxc=(dxc+dxc(end:-1:1,:))/2;
dxc=(dxc+dxc(:,end:-1:1))/2;
dxv=(dxv+dxv(end:-1:1,:))/2;
dxv=(dxv+dxv(:,end:-1:1))/2;
