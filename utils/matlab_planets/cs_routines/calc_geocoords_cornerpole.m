function [lat,lon] = calc_geocoords_cornerpole(lx,ly,tile)
% [lat,lon] = calc_geocoords_cornerpole(lx,ly,tile);
%
% Calculates geographic coordinates for faces of cube
% with curvilinear coordinates (lx,ly)
%
% Meant to be used for quarter tile only

nx=size(lx,1);
nxf=2*nx-1;

% 3D coordinates on unit sphere
[lX,lY,lZ]=map_xy2xyz(lx,ly);

% Use symmetry to expand to full cube
lX(nx:nxf,:)=-lX(nx:-1:1,:); lX(:,nx:nxf)=lX(:,nx:-1:1);
lY(nx:nxf,:)=lY(nx:-1:1,:); lY(:,nx:nxf)=-lY(:,nx:-1:1);
lX=(lX+lY')/2; lY=lX';
lZ=(lZ+lZ')/2;
lZ(nx:nxf,:)=lZ(nx:-1:1,:); lZ(:,nx:nxf)=lZ(:,nx:-1:1);

% First tile
[lX,lY,lZ]=rotate_about_zaxis(lX,lY,lZ,-pi/4);
[lX,lY,lZ]=rotate_about_yaxis(lX,lY,lZ,atan(sqrt(2)));

% Force symmetry again
lX=(lX+lX')/2;
lY=(lY-lY')/2;
lZ=(lZ+lZ')/2;

[lonP,latP]=map_xyz2lonlat(lX,lY,lZ);

% Enforce symmetry again
for j=1:nx; lonP(j,j)=0; end;
lonP=(lonP-lonP')/2;
latP=(latP+latP')/2;

latP=latP(:,end:-1:1);
lonP=-lonP(:,end:-1:1);

% Convert to geographic coordinates
switch tile
 case {1}
   lat=latP(end:-1:1,:);
   lon=-lonP(end:-1:1,:)-2/3*pi;
 case {2}
   lat=latP;
   lon=lonP;
 case {3}
   lat=latP(:,end:-1:1);
   lon=-lonP(:,end:-1:1)+2/3*pi;
 case {4}
   lat=-latP(end:-1:1,:);
   lon=lonP(end:-1:1,:)+1/3*pi;
 case {5}
   lat=-latP(end:-1:1,end:-1:1);
   lon=-lonP(end:-1:1,end:-1:1)+pi;
 case {6}
   lat=-latP(:,end:-1:1);
   lon=lonP(:,end:-1:1)-1/3*pi;
 otherwise
  tile
  error('Illegal value for tile')
end

lon(find(lon>pi))=lon(find(lon>pi))-2*pi;
lon(find(lon<=-pi))=lon(find(lon<=-pi))+2*pi;
