function [a,x,y,t] = rdnetcdf(filename)

nc=netcdf(filename,'nowrite');
v=var(nc)

at=att( nc{ name(v{4}) } )
for j=1:length(at)
 at{j}
 eval( sprintf( '%s=at{j}(:);', name( at{j} ) ) )
end

x=nc{'X'}(:);
y=nc{'Y'}(:);
t=nc{'T'}(:);
a=nc{name(v{4})}(:);
a(find(a==missing_value))=NaN;
a=permute(a,[3 2 1])*scale_factor+add_offset;

disp(['Variable name: ''' long_name ''' Units: ''' units ''' '])
disp( sprintf('Missing value: %g  Scale_factor: %g  Offset: %g', ...
  missing_value,scale_factor,add_offset) )
