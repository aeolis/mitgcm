more off

plotfigs=1;
writedata=1;

%for map={'conf' 'tan' 'q=1' 'q=0'}
for map={'conf'}
 for ornt={'c'}
  for nx=1+[32]
% for n=5:5
%  nx=1+2^n;
%  nratmax=max(1,8-n);
%  for nrat=1:nratmax
   for nrat=[1]
    nratio=2^nrat;
    disp(sprintf('%s %s %i %i',char(map),char(ornt),nx,nratio))
    gengrid_fn(nx,nratio,char(map),char(ornt),plotfigs,writedata);
   end
  end
 end
end
