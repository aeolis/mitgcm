function [X,Y,Z]=rotate_about_zaxis(lX,lY,lZ,angle);
% [X,Y,Z]=rotate_about_zaxis(lX,lY,lZ,angle);
% Rotate about Z axis by "angle".

s=sin(angle);
c=cos(angle);
if c<1e-9
 c=0;
 s=sign(s);
end


X=c*lX-s*lY;
Y=s*lX+c*lY;
Z=lZ;
