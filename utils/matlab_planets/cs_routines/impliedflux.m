function [F] = impliedflux(GRID,Q);

Q( find(isnan(Q)) )=0;
F=mean(Q,3).*GRID.mskc(:,:,1).*GRID.rac;
F=-cumsum([0 squeeze( sum(F,1) )]);
