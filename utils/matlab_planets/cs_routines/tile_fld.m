function DXC=tile_fld(PN,fn,nx)

DXC=zeros(nx,6,nx);
switch fn
case {'DXF','DYF','RA'}, tmp=readbin([PN fn '.bin'],[nx nx],1,'real*8');
case {'DXC','DYG','RAW'}, tmp=readbin([PN fn '.bin'],[nx+1 nx],1,'real*8');
case {'DYC','DXG','RAS'}, tmp=readbin([PN fn '.bin'],[nx nx+1],1,'real*8');
case {'DXV','DYU','RAZ'}, tmp=readbin([PN fn '.bin'],[nx+1 nx+1],1,'real*8');
otherwise, error('field not recognized')
end

for i=1:6
 DXC(:,i,:)=tmp(1:nx,1:nx);
end
