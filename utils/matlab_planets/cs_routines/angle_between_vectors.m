function [angle]=angle_between_vectors(vec1,vec2)
% [angle]=angle_between_vectors(vec1,vec2);

if ndims(vec1)==4
 vector_prod=vec1(:,:,:,1).*vec2(:,:,:,1) ...
            +vec1(:,:,:,2).*vec2(:,:,:,2) ...
            +vec1(:,:,:,3).*vec2(:,:,:,3);
 nrm1=vec1(:,:,:,1).^2+vec1(:,:,:,2).^2+vec1(:,:,:,3).^2;
 nrm2=vec2(:,:,:,1).^2+vec2(:,:,:,2).^2+vec2(:,:,:,3).^2;
elseif ndims(vec1)==3
 vector_prod=vec1(:,:,1).*vec2(:,:,1) ...
            +vec1(:,:,2).*vec2(:,:,2) ...
            +vec1(:,:,3).*vec2(:,:,3);
 nrm1=vec1(:,:,1).^2+vec1(:,:,2).^2+vec1(:,:,3).^2;
 nrm2=vec2(:,:,1).^2+vec2(:,:,2).^2+vec2(:,:,3).^2;
else
 error('Number of dimensions are not coded for');
end

angle=acos( vector_prod./sqrt(nrm1.*nrm2) );
