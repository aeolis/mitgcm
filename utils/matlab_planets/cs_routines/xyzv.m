function [ijk] = xyzv(N,iii)
% Converts a vector index into an n-dimensional array index.
% eg. xyzv( size(a) , find(a < -1) )

ijk=[];
for j=1:prod(size(iii)),

 ii=iii(j)-1;
 ijkk=[];
 
 for k=1:prod(size(N)),
  nn=N(k);
  ij=rem( ii , nn );
  ijkk=[ijkk ij];
  ii=ii-ij;
  ii=ii/nn;
 end
 
 ijk=[ijk ijkk'];
end
