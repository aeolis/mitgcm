function [qs]=jup_saturation(T,P,LH,RV,T0ref,es0,mgas,mH2);

epsilon=mgas/mH2;

esT=es0*exp(-LH/RV*(1.0./T-1.0/T0ref));

qs=epsilon*esT./(P-esT);

%for k=1:length(P)
%   if P(k)>7.0e5 
%      qs(k)=0.03; %30 for jupiter
%   end 
%end

% g/kg to kg/kg

%qs=qs/1.e3;
