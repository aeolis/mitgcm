function [pc, pe, dpe, theta, T]=jup_find_T_P(cp,kappa,gravity,Po,ps,dz,p_tropo,T_tropo,nlay);
% some gas constant in SI units
Rd=cp*kappa;
% pressure at the bottom of the domain
pe(1)=ps;
% hight coordinate starts from bottom
ze(1)=0.0;

% initialize vertical coordinate

% addiabatic deep interior

theta_deep=T_tropo*(Po/p_tropo)^kappa;

% calculate the layer thickness in deep atmosphere
% hydrostatic==> dz=-1/(kappa*Po^kappa)*Rd/gravity*theta*dp^kappa
% ==> dp^kappa=-g*dz*kappa*Po^kappa/(Rd*theta)

% first, find dp for entire atmosphere
for k=1:nlay
dp_kappa(k)=-gravity*dz*kappa*Po^kappa/(Rd*theta_deep);
end;

% pe(k+1)^kappa - pe(k)^kappa = dp_kappa(k)
for k=1:nlay
pe(k+1)=( pe(k)^kappa + dp_kappa(k) ).^(1./kappa);
end;

% find the layer number for tropopause n0
n_tropo=find(pe < p_tropo); n0=n_tropo(1);

% calculate the layer thickness above tropopause
% dz=-d(ln(p))*Rd*T/gravity ==>
% d(ln(p))=-gravity*dz/(Rd*T) ==>
% pe(k+1)=pe(k)*exp(-gravity*dz/(Rd*T))
for k=n0:nlay
pe(k+1)=pe(k)*exp(-gravity*dz/(Rd*T_tropo));
end;

% find layer thickness in pressure [Pa]
% make them positive
dpe=pe(1:end-1)-pe(2:end); 
% find pressure at the center of the vertical grid
pc=(pe(1:end-1)+pe(2:end))/2;

% find the reference temperature profile (theta)
theta(1:n0)=theta_deep;
theta(n0:nlay)=T_tropo*(Po./pc(n0:nlay)).^kappa;

% reference temperature prfile (T)
T=theta.*(pc/Po).^kappa;

% check layer thickness in meters again (should be equal to dz)
dz_check=dpe*Rd.*T./pc/gravity;

