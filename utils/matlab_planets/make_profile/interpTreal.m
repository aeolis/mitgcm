%calculate rho and T from galileo measurement

clear all;

rkb = 1.3807E-23;
amu = 1.673E-27;
mass = 2.16;
grv = 22.60;
mol=6.022e23;
%rgas = rkb*mol/mass;
rgas = rkb/amu/mass;

dz=1.0; % in km

load asimean.dat;

% extrapolate the temperature profile from 0--1200km
% z is the altitude, temp is the temperature, rho is density, Nref is number density
z=100:dz:1100-1;
temp = interp1(asimean(:,1),asimean(:,2),z,'linear','extrap');
%P = interp1(asimean(:,1),asimean(:,8),z,'spline','extrap');

for i=1:length(z)
 if z(i)>949
    temp(i)=temp(850);
 end;
end;

% calculate Pressure from z and T

dzT=zeros(1,length(z));

dzT(1)=dz/temp(1);

for i=2:length(z)
    dzT(i)=dzT(i-1)+dz/temp(i);
end;

% calculate the pressure P=P(z), reference P0 at temp(1) is

P=zeros(1,length(z));

P0= 6.655208e03*0.1; % in pascal

H1(1)=grv/rgas/temp(i)*dz*1e3;

for i=2:length(z)
H1(i)=grv/rgas/temp(i)*dz*1e3+H1(i-1);
end;

for i=1:length(z)
    P(i)=P0*exp(-H1(i));
end;

% calculate the density rhoT and number density Nref

rhoT=P./(rgas*temp);

Nref=P./(rkb*temp);


%calculate the corresponding hh
hh = rgas*temp(1:size(z,2))/grv;
%write hh and y to disk, in mltlay.f90, zdl is caculated by dx*hh

for i=1:size(z,2)
    b(i,1)=z(i)*1.0e5; % in cm
    b(i,2)=temp(i);       % in K
    b(i,3)=rhoT(i)*1e-3;     % in g/cm^3
    b(i,4)=Nref(i)/1e6;      % in #/cm^3
    b(i,5)=P(i)*10.0;        % in microbar=dyne/cm^2
end;


save('asimeant.dat','b', '-ASCII');

figure; subplot(2,1,1);
plot(temp,z,'+r')
hold on;
plot(asimean(:,2),asimean(:,1));
subplot(2,1,2);
plot(asimean(:,8),asimean(:,1));
set(gca,'xscale','log');

figure;subplot(3,1,1);
plot(temp,z,'+r')
hold on;
plot(asimean(:,2),asimean(:,1));
title('T [K] VS. Z [km]');

subplot(3,1,2);
plot(rhoT*1e-3,z,'+r')
hold on;
plot(asimean(:,9),asimean(:,1));
set(gca,'xscale','log');
title('\rho [kg/m^3] VS. Z [km]');

subplot(3,1,3);
plot(P,z,'r');
set(gca,'xscale','log');
title('P [pascal] VS. Z [km]');

figure;
plot(Nref/1.0e6,z,'r');
set(gca,'xscale','log');
title('Number density [#/cm^3] VS. Z [km]');
