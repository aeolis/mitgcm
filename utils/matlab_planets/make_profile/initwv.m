  Runiv=8314;
%  RWV = 461.0;
  es0H2O = 609.140;
  T0refH2O = 273.0;
  LHWV = 2.5e6;

  es0NH3 = 4.29e5;
  T0refNH3 = 273.0;
  LHNH3=1.369e6;

  es0CH4 = 2.6664e6;
  T0refCH4 = 173;
  LHCH4=5.1e6;

  mH2O=18;
  mNH3=17;
  mCH4=16;
  mH2=2;
  
  RWV = Runiv/mH2O;
  RNH3 = Runiv/mNH3;
  RCH4 = Runiv/mCH4;


  cas=input('choose planet, E 1, U 2, N 3, J 4: ');
  casC=input('choose gas type, H2O 1, NH3 2, CH4 3: ');
  
  if cas==1
     load earth/TPprofile35-1bar;
     TP=TPprofile35_1bar;
  elseif cas==2
     load uranus/TPprofile35-1bar;  
     TP=TPprofile35_1bar;
  elseif cas==3
     load TPprofile38lay1bar.dat; 
     TP=TPprofile38lay1bar;
     kappa=0.305;
  elseif cas==4
%     load TPprofile35-1bar;
%     TP=TPprofile35_1bar;
     load TP_Jup_65L.dat;
     TP=TP_Jup_65L;
     kappa=0.29;
  end 

  theta=TP(:,2);
  P=TP(:,1);
  T=(P/1e5).^kappa.*theta;

  if casC==1
     LH=LHWV; RV=RWV; epsilon=mH2O/mH2; T0ref=T0refH2O; es0=es0H2O;
  elseif casC==2
     LH=LHNH3; RV=RNH3; epsilon=mNH3/mH2; T0ref=T0refNH3; es0=es0NH3;
  elseif casC==3
     LH=LHCH4; RV=RCH4; epsilon=mCH4/mH2; T0ref=T0refCH4; es0=es0CH4;
  end;

     esT=es0*exp(-LH/RV*(1.0./T-1.0/T0ref));

% set vapor pressure above tropopause to be zero
%  esT(length(P)-5:length(P))=0.0;

     qs=epsilon*esT./(P-esT);

  figure
  plot(esT,P)
  set(gca,'yscale','log')
  set(gca,'ydir','reverse')
  set(gca,'fontsize',14);
  title('Saturated vapor pressure [Pa]');
  figure
  qs=9*esT./(P); %kg/kg to g/kg
  qs1=qs;
%  for k=1:length(P)
%   if P(k)>7.0e5 
%      qs(k)=30; %30 for jupiter
%   end 
%  end
  plot(qs,P)
  set(gca,'ydir','reverse')
  set(gca,'yscale','log')
  set(gca,'xscale','log');
  set(gca,'fontsize',14);
  title('Specific humidity [kg/kg]');

