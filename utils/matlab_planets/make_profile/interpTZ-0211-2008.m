%calculate rho and T from galileo measurement

clear all;

rkb = 1.3807E-23;
amu = 1.673E-27;
mass = 2.16;
grv = 22.60;
mol=6.022e23;
%rgas = rkb*mol/mass;
rgas = rkb/amu/mass;
cp=14.58*1000.0; %in J/kgK

dz=1.0; % in km

load asimean.dat;

% extrapolate the temperature profile from 0--1200km
% z is the altitude, temp is the temperature, rho is density, Nref is number density
%z1=98:1:100;
%z2=101:1:1501;

%z=z1;
%z(length(z1)+1:length(z1)+length(z2))=z2;

%dz(1:length(z1))=0.01;
%dz(length(z1)+1:length(z1)+length(z2))=1.0;

z=98:dz:1501;

%temp = interp1(asimean(:,1),asimean(:,2),z,'linear','extrap');
%P = interp1(asimean(:,1),asimean(:,8),z,'spline','extrap');

for i=1:length(z)
temp(i)=((exp((z(i)-200)/200-1.5)-exp(-(z(i)-200)/200+1.5))/(exp((z(i)-200)/200-1.5)+exp(-(z(i)-200)/200+1.5))+1)*780/2+120;
end;

%for i=1:length(z)
% if z(i)>949
%    temp(i)=temp(850);
% end;
%end;

% calculate Pressure from z and T

dzT=zeros(1,length(z));

dzT(1)=dz/temp(1);

for i=2:length(z)
    dzT(i)=dzT(i-1)+dz/temp(i);
end;

% calculate the pressure P=P(z), reference P0 at temp(1) is

P=zeros(1,length(z));

P0= 6.9e3*0.1; % in pascal

H1(1)=grv/rgas/temp(1)*dz*1e3;

for i=2:length(z)
  H1(i)=grv/rgas/temp(i)*dz*1e3+H1(i-1);
end;

for i=1:length(z)
    P(i)=P0*exp(-H1(i));
end;

% calculate the density rhoT and number density Nref

rhoT=P./(rgas*temp);

Nref=P./(rkb*temp);


%calculate the corresponding hh
%hh = rgas*temp(1:size(z,2))/grv;

% calculate the density scale height 1/H=-1/rho*d(rho)/dz

for i=2:length(z)-1
 H11=-1.0/rhoT(i)*((rhoT(i+1)-rhoT(i-1)))/(2*dz*1.0e3);
 HH(i)=1.0/H11;
end;

%HH(length(z))=1.0 / (-1.0/rhoT(length(z))*((0.0-rhoT(length(z))))/(dz*1.0e3));
HH(length(z))=HH(length(z)-1);
HH(1)=HH(2);

dhdz=zeros(length(z));
d2hdz2=zeros(length(z));

for i=2:length(z)-1
 dhdz(i)=(HH(i+1)-HH(i-1))/(2*dz*1.0e3);
end;

for i=2:length(z)-1
 d2hdz2(i)=(HH(i+1)-2*HH(i)+HH(i-1))/(dz*1.0e3)^2;
end;


%write hh and y to disk, in mltlay.f90, zdl is caculated by dx*hh

for i=1:size(z,2)
    b(i,1)=z(i)*1.0e5; % in cm
    b(i,2)=temp(i);       % in K
    b(i,3)=rhoT(i)*1e-3;     % in g/cm^3
    b(i,4)=Nref(i)/1e6;      % in #/cm^3
    b(i,5)=P(i)*10.0;        % in microbar=dyne/cm^2
    b(i,6)=HH(i)*1.0e2;      % in cm
    b(i,7)=dhdz(i);
    b(i,8)=d2hdz2(i)*1e-2;   % in cm^{-1}
end;


save('asimeant.dat','b', '-ASCII');


figure;subplot(3,1,1);
plot(temp,z,'+r')
hold on;
plot(asimean(:,2),asimean(:,1));
title('T [K] VS. Z [km]');

subplot(3,1,2);
plot(rhoT*1e-3,z,'+r')
hold on;
plot(asimean(:,9),asimean(:,1));
set(gca,'xscale','log');
title('\rho [g/cm^3] VS. Z [km]');

subplot(3,1,3);
plot(P,z,'r');
set(gca,'xscale','log');
title('P [pascal] VS. Z [km]');
hold on;
plot(asimean(:,8)/10,asimean(:,1));

figure;
subplot(2,1,1);
plot(Nref/1.0e6,z,'r');
set(gca,'xscale','log');
title('Number density [#/cm^3] VS. Z [km]');

figure;
subplot(2,1,2);
plot(HH/1e3,z,'r');
title('Density scale height [km] VS. Z [km]');
