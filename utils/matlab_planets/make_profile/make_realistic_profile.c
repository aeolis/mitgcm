/* * * * * * * * * * * * * -*-Text-*- * * * * * * * * ** * * * * * *
 *                                                                 *
 * Copyright (C) 1998 Adam P. Showman                              *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of the GNU General Public License     *
 * as published by the Free Software Foundation; either version 2  *
 * of the License, or (at your option) any later version.          *
 * A copy of this License is in the file:                          *
 *   $EPIC_PATH/License.txt                                        *
 *                                                                 *
 * This program is distributed in the hope that it will be useful, *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            *
 *                                                                 *
 * You should have received a copy of the GNU General Public       *
 * License along with this program; if not, write to the Free      *
 * Software Foundation, Inc., 59 Temple Place - Suite 330,         *
 * Boston, MA  02111-1307, USA.                                    *
 *                                                                 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  This is EPIC 3.9: Isentropic layer model.  Adapted from EPIC   *
 *  3.8 by Adam Showman.  Similar to 3.8 with various changes.     *
 *								   *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/******************************************************************
 * Make_realistic_profile(): Create a T(p) profile which
 *    approximately matches what we expect for giant planets.  Have
 *    an isothermal stratosphere and a somewhat stable 
 *    troposphere (the latter to model the water cloud).
 *    (There is no adiabatic region, because we assume the
 *    bottom active layer when we run EPIC is at the base of the
 *    stable layer and the final (abyssal) layer is the adiabatic one.)
 *
 *    The created file can then be transfered to (say) t_vs_p.jupiter
 *    for use in initializing a Jupiter simulation using 
 *    initial.LINUX (i.e. epic_change.c).  
 *
 *    The profile is isothermal, with temperature T0, at pressures
 *    less than p0.  At p>p0, the profile has a constant brunt
 *    vaisala frequency which is input by the user.
 *
 *    Date: September 1999
 *    Author: Adam Showman
 ******************************************************************/


/*
 * Compile with (Linux): gcc make_const_N_profile.c -lm
 * 
 */

#include <stdio.h>
#include <math.h>

main()
{
  char   
    header[128];
  int
    kk,          /* counter */
    ntp;         /* number of T vs. p points for the profile we will create */

  double
    p0,          /* ref pressure (Pa); transition between const N and T regions */
    T0,          /* T in isothermal region (and at p0) (K) */ 
    TN,		 /* parameter involving brunt vaisala freq (K) */
    N0,		 /* brunt freq. for const-N region (1/sec) */
    kappa,       /* R/cp = (gamma-1)/gamma for the air.  Assumed constant. No units. */
    R,           /* Gas constant in units J/kg K (=Runiversal/m).  */
    gravity,     /* m/sec^2 */
    x_h2,        /* mole fraction of H2 */
    x_he,        /* mole fraction of He */
    x_3,	 /* mole fraction of a third component with 5 degrees of freedom */
    Ncalculated, /* We calculate (g/T)*(g/cp + dT/dz) for reference */
    rho,	 /* density, used to calculate Ncalculated */
    In_between_p, /* average of p at i and i-1 */
    In_between_temp, /* average of T at i and i-1 */
    Nactual_inbetween,  /* Brunt freq at In_between_p  */
    plo,         /* lowest pressure of the profile.   Pa. */
    phi,         /* highest pressure of the profile.  Pa. */
    delta_logp,  /* increment in log p between the points. Unitless. */
    *Thetadat,    /*array of Potential temperature in the profile */
    *pdat,       /* array of pressures in the profile */
    *Tdat;       /* array of temperatures in the profile */
    
  double 
    Temp;

  FILE 
    *t_vs_p;


  /* Set the size of the arrays */
  ntp = 20;

  /*
   * Allocate memory:
   */
  pdat     = (double *)calloc(ntp,sizeof(double));
  Tdat     = (double *)calloc(ntp,sizeof(double));
  Thetadat = (double *)calloc(ntp,sizeof(double));


 /*
   * Inquire about the reference p0 and T0:
   */
  fprintf(stderr,"\nInput the ref pressure p0 (in Pa), above which T=const: ");
  fscanf(stdin,"%lf",&p0);
  fprintf(stderr,"\nInput the reference temperature T0 for p < p0: ");
  fscanf(stdin,"%lf",&T0);
  fprintf(stderr,"\nInput the value of N for p > p0");
  fprintf(stderr,"\n(Profile is const N at p > p0 and isothermal at p < p0)");
  fscanf(stdin,"%lf",&N0);
  fprintf(stderr,"\nInput the lowest pressure (in Pa): ");
  fscanf(stdin,"%lf",&plo);
  fprintf(stderr,"\nInput the  highest pressure (in Pa): ");
  fscanf(stdin,"%lf",&phi);

  if (phi <= plo) {
    fprintf(stderr,"ERROR: Your highest pressure was not greater than\n");
    fprintf(stderr,"       your lowest pressure.\n");
    exit(1);
  }
  fprintf(stderr,"\nThe file created is t_vs_p_realistic.  It contains %d (T,p) pairs\n",
	  ntp);

  /***** Set some params *********/
/*  x_h2 = 0.864;
  x_he = 0.136;
*/

  x_h2 = 1.0;
  x_he = 0.0;  /* FUDGE */

  kappa = (x_he + x_h2)/(2.5*x_he + 3.5*x_h2);
     /* This assumes the following degrees of freedom:
      *	    He :  3 translational
      *	    H2 :  3 translational + 2 rotational, as is approximately valid in
      *		    reality from ~300-800 K and in Sept. 1999 EPIC for all
      *		    T > 300 K or so.  At lower T, the rotational degrees of
      *		    freedom start to get inhibited and ortho-para conversion
      *		    occurs (which is in EPIC), and at higher T two vibrational
      *		    degrees of freedom kick in (which is not in EPIC).
      */
  fprintf(stderr,"kappa=%f\n",kappa);
  R = 287.;     /* This is what EPIC seems to use for Jupiter.  EPIC does not
		    calculate it to be consistent with composition, but simply
		    inputs the value in the inital planet structure. */
  gravity = 9.81;
  TN = kappa * gravity * gravity/(N0 * N0 * R);

  /*
   * Create T(p) profile:
   *
   */
  pdat[0] = plo;
  pdat[ntp-1]=phi;
  delta_logp = (log(phi) - log(plo))/(double)(ntp-1);
  if (pdat[0] <= p0) Tdat[0] = T0;
  if (pdat[0] > p0) {
    Tdat[0] = T0 * TN * pow( pdat[0]/p0, kappa) / 
	   ( TN + T0 * pow (pdat[0]/p0, kappa) - T0);
  }
  Thetadat[0] = pow(phi/pdat[0],kappa)*Tdat[0];
  for (kk = 1; kk <= ntp-1; kk++) {
    pdat[kk] = pdat[kk-1]*exp(delta_logp);       /* even spacing in log p */
    if (pdat[kk] <= p0) {
      Temp = T0;
    } 
    if (pdat[kk] > p0) {
      Temp = T0 * TN * pow( pdat[kk]/p0, kappa) / 
	   ( TN + T0 * pow (pdat[kk]/p0, kappa) - T0);
    }

    Tdat[kk] = Temp;
    In_between_p = 0.5*(pdat[kk] + pdat[kk-1]);
    In_between_temp = 0.5*(Tdat[kk] + Tdat[kk-1]);

    /* formula for ideal gas is N^2 = (g^2/T) * [1/cp - rho dT/dp]   */
    rho = 0.5*(pdat[kk] + pdat[kk-1])/(R*0.5*(Tdat[kk] + Tdat[kk-1]));
    Ncalculated = gravity * sqrt( (kappa/R - rho *
		  (Tdat[kk] - Tdat[kk-1]) / (pdat[kk] - pdat[kk-1]))/In_between_temp);
    /* calculate potential temperature according to real temperature */
    Thetadat[kk] = pow(phi/pdat[kk],kappa)*Tdat[kk];
    
    fprintf(stderr,"%d   p=%f  T=%f  Theta=%f Ncalc=%f \n",kk,pdat[kk],
			    Temp,Thetadat[kk],Ncalculated);
  }

  /* Open t_vs_p.jupiter: */
  t_vs_p = fopen("t_vs_p_realistic","w");

  /* Write the header: */
  fprintf(t_vs_p,
        " Temperature versus pressure for Jupiter. Isothermal with T0=%f K above\n",
	T0);
  fprintf(t_vs_p," p0=%f Pa and has const N=%f/sec below.  Uses R=%f and kappa=%f.\n",
			 p0,N0, R,kappa);
  fprintf(t_vs_p," The lowest and highest pressures are %f Pa and %f Pa.\n",plo,phi);

  fprintf(t_vs_p," There are %d points.  File created by make_realistic_profile.c.\n",ntp);
 
  fprintf(t_vs_p,"\n");
  fprintf(t_vs_p,"#   p[mbar]    T[K]     Theta[K]\n");

  /* Write number of data points: */
  fprintf(t_vs_p,"%d\n",ntp);

  /* Write data (saving pdat as mbar rather than Pa): */
  for (kk = 0; kk <= ntp-1; kk++) {
    fprintf(t_vs_p,"   %10.5e  %6.5f  %6.5f \n",0.01*pdat[kk],Tdat[kk],Thetadat[kk]);
  }
  fclose(t_vs_p);

  /*
   * Free allocated memory:
   */
  free(pdat); 
  free(Tdat);
  free(Thetadat);

  return;
}
