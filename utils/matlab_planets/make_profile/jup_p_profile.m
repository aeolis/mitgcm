% this script make jupiter T-P profile. In vertical 
% each scale height is resolved by three grid points roughly 
% grid scale facor is close to 2^.5 ~ 1/exp(-1/3)
clear all;
ps=input('pressure at the bottom of the domain [Pa]: ');
% some gas constant in SI units
cp=13000.;
kappa=0.29;
Rd=cp*kappa;
gravity=22.88;
Po=1.E5;

% properties of condensible gases
Runiv=8314;

es0H2O = 609.140;
T0refH2O = 273.0;
LHWV = 2.5e6;

es0NH3 = 4.29e5;
T0refNH3 = 273.0;
LHNH3=1.369e6;

mH2O=18;
mNH3=17;
mH2=2;

RH2O = Runiv/mH2O;
RNH3 = Runiv/mNH3;


% pressure at the bottom of the domain
%ps=10.e5;
% hight coordinate starts from bottom
ze(1)=0.0;
% layer thickness in meters
dz=6.e3; % 1 [km]
% pressure at tropopause, 0.3 bar for jupiter
p_tropo=0.2E5;
% temperature at tropopause in [K]
T_tropo=110.;
% set an initial layer number
% layer number 
nlay=60; 

[pc, pe, dpe, theta, T]=jup_find_T_P(cp,kappa,gravity,Po,ps,dz,p_tropo,T_tropo,nlay);

p_top=pe(end);

% ===/// start the main loop to find the top of the atmosphere ///===
% find T-P profile topped at 10 to 20Pa (taking the round off error due to the 
% formating of input
while (p_top < 10.0)
  clear pc pe dpe theta T;
  dz=dz/1.01;
  [pc, pe, dpe, theta, T]=jup_find_T_P(cp,kappa,gravity,Po,ps,dz,p_tropo,T_tropo,nlay);
  p_top=pe(end);
end;

while (p_top > 20.0)
  clear pc pe dpe theta T;
  dz=dz*1.01;
  [pc, pe, dpe, theta, T]=jup_find_T_P(cp,kappa,gravity,Po,ps,dz,p_tropo,T_tropo,nlay);
  p_top=pe(end);
end;

qsH2O=jup_saturation(T,pc,LHWV,RH2O,T0refH2O,es0H2O,mH2O,mH2);
qsNH3=jup_saturation(T,pc,LHNH3,RNH3,T0refNH3,es0NH3,mNH3,mH2);

% check layer thickness in meters again (should be equal to dz)
dz_check=dpe*Rd.*T./pc/gravity;

% format output for MITgcm data file
dp0=reshape(dpe,[5 nlay/5]); dp0=dp0';
th0=reshape(theta,[5 nlay/5]); th0=th0';
qswv0=reshape(qsH2O,[5 nlay/5]); qswv0=qswv0';
qsam0=reshape(qsNH3,[5 nlay/5]); qsam0=qsam0';
% convert numbers to string
dp0s=num2str(dp0,'%5.3e, %5.3e, %5.3e, %5.3e, %5.3e, \n');
th0s=num2str(th0,'%5.3e, %5.3e, %5.3e, %5.3e, %5.3e, \n');
qswv0s=num2str(qswv0,'%5.3e, %5.3e, %5.3e, %5.3e, %5.3e, \n');
qsam0s=num2str(qsam0,'%5.3e, %5.3e, %5.3e, %5.3e, %5.3e, \n');

clear dp0 th0 qswv0 qsam0; 
