%calculate rho and T from galileo measurement, 
% the interpolation for temperature follows 
% Hickey et al., 2000, in Appendix 2

clear all;

constT=input('constant temperature? 1--> ture:  ');
Tinterp=input('temperature interpolation method-->RVY 1, MPH 2: ');

rkb = 1.3807E-23;
amu = 1.673E-27;
mass = 2.16;
grv = 22.60;
mol=6.022e23;
omg=1.6e-3;
%omg=1.0e-4;
%rgas = rkb*mol/mass;
rgas = rkb/amu/mass;
%cp=14.58*1000.0; %in J/kgK
cp0=1.4582E8;
dz=1.0; % 1.0 in km

load asimean.dat;
load vcT.dat;  % vcT(:,1)--T, vcT(:,12)--vsc, vcT(:,13)--cdn, vcT(:,9)--cp
%load freq.dat;

% extrapolate the temperature profile from 0--1200km
% z is the altitude, temp is the temperature, rho is density, Nref is number density

z=100:dz:1499;

if (Tinterp == 1)
   fac0=1.0;  % 1.0 control initail position of left end
   fac1=1.5;  % 1.5 control slope of left end
   fac2=200;  % 200 control the data points on left or right ends.
   fac3=700;  % 780 control width of the profile.
   fac4=120;  % 120 control initail position of left end

   for i=1:length(z)
       temp(i)=((exp((z(i)-fac2)/fac2-fac1)-exp(-(z(i)-fac2)/fac2+fac1))/ ... 
       (exp((z(i)-fac2)/fac2-fac1)+exp(-(z(i)-fac2)/fac2+fac1))+fac0)*fac3/2+fac4;
    end;
elseif (Tinterp == 2)
   klev=find(z==200);
   if (klev > 0)
      temp(1:klev)=150.0;
   else
      klev=1;
   end;

   zx=400;
   Tx=360;
   Bx=5e-6;
   beta=2;
   Tinf=900;
   Gx=2.5;
   Ar=2*(Tinf-Tx)/pi;

   c1=2.5;
   c2=0;
   c3=-1.0e-4;
   c4=-3.4375e-7;
   c5=1.8750e-10;
   c6=1.5625e-12;

   for k=klev:length(z)
       if (z(k)<=zx)
       temp(k)=Tx+c1*(z(k)-zx)^1+c2*(z(k)-zx)^2+ ... 
               c3*(z(k)-zx)^3+c4*(z(k)-zx)^4+ ...
               c5*(z(k)-zx)^5+c6*(z(k)-zx)^6;
       else
       temp(k)=Tx+Ar*atan(Gx/Ar*(z(k)-zx)* ... 
              (1+Bx*(z(k)-zx)^beta));
       end;
   end;
else
   'no option';   
end;

%interpolate viscosity and thermal conductivity

vscDyn=interp1(vcT(:,1),vcT(:,12),temp,'linear','extrap');
cnduct=interp1(vcT(:,1),vcT(:,13),temp,'linear','extrap');
%cp0=interp1(vcT(:,1),vcT(:,9),temp,'linear')*1e4/(2.0*1.67e-27*6.023e23);

% convert from SI to CGS units

vscDyn=vscDyn*10.0;
cnduct=cnduct*1.e5;

% calculate Pressure from z and T

P=zeros(1,length(z));

Pinter = interp1(asimean(:,1),asimean(:,8),z(1:100),'linear','extrap');

P0= Pinter(1)/10.0; %6.9e3*0.1; % in pascal

P(1)=P0;
for i=1:length(z)-1
  P(i+1)=P(i)*exp(-grv/rgas/temp(i)*dz*1e3);
end;

% calculate the density rhoT and number density Nref

rhoT=P./(rgas*temp);

Nref=P./(rkb*temp);


% calculate the density scale height 1/H=-1/rho*d(rho)/dz

   HH=rgas*temp/grv;

% calculate kinetic viscosity and thermal diffusivity

vscK=vscDyn./(rhoT*1e-3);
diff=cnduct./(rhoT*1e-3)/cp0;

%write hh and y to disk, in mltlay.f90, zdl is caculated by dx*hh

for i=1:length(z)
    b(i,1)=z(i)*1.0e5; % in cm
    b(i,2)=temp(i);       % in K
    b(i,3)=rhoT(i)*1e-3;     % in g/cm^3
    b(i,4)=Nref(i)/1e6;      % in #/cm^3
    b(i,5)=P(i)*10.0;        % in microbar=dyne/cm^2
    b(i,6)=HH(i)*1.0e2;      % in cm
end;


%write viscosity and diffusivity to disk

dvscdz(1:length(z))=0.0;

dvscdz(2:length(z)-1)=(vscK(3:length(z))-vscK(1:length(z)-2))/(2.0*dz*1.0e5);

for i=1:length(z)
    c(i,1)=b(i,1);
    c(i,2)=vscDyn(i); %vscK(i);  % in cgs cm^2s^{-1} 
    c(i,3)=cnduct(i); %diff(i);  % in cgs cm^2s^{-1}
end;

save('vdcoeff.dat','c','-ASCII');
save('asimeant.dat','b', '-ASCII');

%fid = fopen('N2.dat', 'wt');
%    for i = 1:length(z)
%        fprintf(fid,'(%e, %e)  ',real(N2(i)),imag(N2(i)));
%        fprintf(fid,'\n');
%    end
%fclose(fid);

figure;subplot(3,1,1);
plot(temp,z,'+r')
hold on;
plot(asimean(:,2),asimean(:,1));
title('T [K] VS. Z [km]');

subplot(3,1,2);
plot(rhoT*1e-3,z,'+r')
hold on;
plot(asimean(:,9),asimean(:,1));
set(gca,'xscale','log');
title('\rho [g/cm^3] VS. Z [km]');

subplot(3,1,3);
plot(P,z,'r');
set(gca,'xscale','log');
title('P [pascal] VS. Z [km]');
hold on;
plot(asimean(:,8)/10,asimean(:,1));

figure;
subplot(2,1,1);
plot(Nref/1.0e6,z,'r');
set(gca,'xscale','log');
title('Number density [#/cm^3] VS. Z [km]');

subplot(2,1,2);
plot(HH/1e3,z,'r');
title('Density scale height [km] VS. Z [km]');
