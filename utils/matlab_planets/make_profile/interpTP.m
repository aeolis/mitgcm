%calculate rho and T from galileo measurement

clear all;

rkb = 1.3807E-23;
amu = 1.66E-27;
mass = 2.16;
grv = 22.60;
mol=6.022e23;
%rgas = rkb*mol/mass;
rgas = rkb/amu/mass;

dz=0.5; % in km

load asimean.dat;

% extrapolate the temperature profile from 0--1200km
% z is the altitude, temp is the temperature, rho is density, Nref is number density
% curve interpolation is using spline instead of linear, 10/17/2007

z=100:dz:900-0.5;
temp = interp1(asimean(:,1),asimean(:,2),z,'spline','extrap');
P = interp1(asimean(:,1),asimean(:,8),z,'spline','extrap');
Hscale = interp1(asimean(:,1),asimean(:,6),z,'spine','extrap');

for i=1:length(z)
temp(i)=((exp((z(i)-200)/200-1.5)-exp(-(z(i)-200)/200+1.5))/(exp((z(i)-200)/200-1.5)+exp(-(z(i)-200)/200+1.5))+1)*780/2+120;
end;


P=P*1e-6*1e5; % in pascal

% calculate Pressure from z and T

dzT=zeros(1,length(z));

dzT(1)=dz/temp(1);

for i=2:length(z)
    dzT(i)=dzT(i-1)+dz/temp(i);
end;

% calculate the density rhoT and number density Nref

rhoT=P./(rgas*temp);

Nref=rhoT/(amu*mass);


%calculate the corresponding hh
hh = rgas*temp(1:size(z,2))/grv;
%write hh and y to disk, in mltlay.f90, zdl is caculated by dx*hh

for i=1:size(z,2)
    b(i,1)=z(i)*1.0e5; % in cm
    b(i,2)=temp(i);       % in K
    b(i,3)=rhoT(i)*1e-3;     % in g/cm^3
    b(i,4)=Nref(i)/1e6;      % in #/cm^3
    b(i,5)=P(i)*10.0;        % in microbar=dyne/cm^2
    b(i,6)=Hscale(i)*1.0e5; %  interpolated scale height in cm
end;


save('asimeant.dat','b', '-ASCII');

figure; subplot(2,1,1);
plot(temp,z,'+r')
hold on;
plot(asimean(:,2),asimean(:,1));
subplot(2,1,2);
plot(asimean(:,9),asimean(:,1));
set(gca,'xscale','log');

figure;subplot(3,1,1);
plot(temp,z,'+r')
hold on;
plot(asimean(:,2),asimean(:,1));
title('T [K] VS. Z [km]');

subplot(3,1,2);
plot(b(:,3),z,'+r')
hold on;
plot(asimean(:,9),asimean(:,1));
set(gca,'xscale','log');
title('\rho [g/m^3] VS. Z [km]');

subplot(3,1,3);
plot(asimean(:,8),asimean(:,1));
hold on
plot(b(:,5),z,'+r');
set(gca,'xscale','log');
title('P [dyne/cm^2] VS. Z [km]');

figure;
plot(Nref/1.0e6,z,'r');
set(gca,'xscale','log');
title('Number density [#/cm^3] VS. Z [km]');
