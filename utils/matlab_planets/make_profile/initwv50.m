  RWV = 461.0;
  es0 = 609.140;
  T0ref = 273.0;
  LHWV = 2.5e6;

  load jupiter/TPprofile50layer;
  theta=TPprofile50layer(:,2);
  P=TPprofile50layer(:,1);
  T=(P/1e5).^0.29.*theta;
  esT=es0*exp(-LHWV/RWV*(1.0./T-1.0/T0ref));
% set vapor pressure above tropopause to be zero
  for k=1:50
   if P(k)<0.2e5
      esT(k)=0.0;
   end;
  end;

  qs=9*esT./P;
  figure
  plot(esT,P)
  set(gca,'yscale','log')
  set(gca,'ydir','reverse')
  figure
  qs=9*esT./P*1000; %kg/kg to g/kg
  for k=1:length(P)
   if P(k)>7.0e5 
      qs(k)=30; %30 for jupiter
   end 
  end
  plot(qs,P)
  set(gca,'ydir','reverse')
  set(gca,'yscale','log')

