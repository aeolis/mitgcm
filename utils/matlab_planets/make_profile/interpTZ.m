%calculate rho and T from galileo measurement

clear all;

constT=input('constant temperature? 1--> ture:  ');

rkb = 1.3807E-23;
amu = 1.673E-27;
mass = 2.16;
grv = 22.60;
mol=6.022e23;
omg=1.6e-3;
%omg=1.0e-4;
%rgas = rkb*mol/mass;
rgas = rkb/amu/mass;
%cp=14.58*1000.0; %in J/kgK
cp0=1.4582E8;
dz=1.0; % in km

load asimean.dat;
load vcT.dat;  % vcT(:,1)--T, vcT(:,12)--vsc, vcT(:,13)--cdn, vcT(:,9)--cp
%load freq.dat;

% extrapolate the temperature profile from 0--1200km
% z is the altitude, temp is the temperature, rho is density, Nref is number density

z=98:dz:1499;

coeff_lin=polyfit(asimean(:,1),asimean(:,2),1);

%temp = interp1(asimean(:,1),asimean(:,2),z,'linear','extrap');
%P = interp1(asimean(:,1),asimean(:,8),z,'spline','extrap');

fac0=1.0;  % 1.0 control initail position of left end
fac1=1.5;  % 1.5 control slope of left end
fac2=200;  % 200 control the data points on left or right ends.
fac3=700;  % 780 control width of the profile.
fac4=120;  % 120 control initail position of left end

aa=1.0;

for i=1:length(z)
temp(i)=((exp((z(i)-fac2)/fac2-fac1)-exp(-(z(i)-fac2)/fac2+fac1))/(exp((z(i)-fac2)/fac2-fac1)+exp(-(z(i)-fac2)/fac2+fac1))+fac0)*fac3/2+fac4;
end;

%temp=coeff_lin(1)*z+coeff_lin(2);

%interpolate viscosity and thermal conductivity

vscDyn=interp1(vcT(:,1),vcT(:,12),temp,'linear','extrap');
cnduct=interp1(vcT(:,1),vcT(:,13),temp,'linear','extrap');
%cp0=interp1(vcT(:,1),vcT(:,9),temp,'linear')*1e4/(2.0*1.67e-27*6.023e23);

% convert from SI to CGS units

vscDyn=vscDyn*10.0;
cnduct=cnduct*1.e5;

% calculate Pressure from z and T

P=zeros(1,length(z));

Pinter = interp1(asimean(:,1),asimean(:,8),z(1:100),'linear','extrap');

P0= Pinter(1)/10.0; %6.9e3*0.1; % in pascal

P(1)=P0;
for i=1:length(z)-1
  P(i+1)=P(i)*exp(-grv/rgas/temp(i)*dz*1e3);
end;

% calculate the density rhoT and number density Nref

rhoT=P./(rgas*temp);

Nref=P./(rkb*temp);


%calculate the corresponding hh
%hh = rgas*temp(1:size(z,2))/grv;

% calculate the density scale height 1/H=-1/rho*d(rho)/dz

for i=2:length(z)-1
 H11=grv/rgas/temp(i) +1.0/temp(i)*((temp(i+1)-temp(i-1)))/((dz+dz)*1.0e3);
 HH(i)=1.0/H11;
end;

%HH(length(z))=1.0 / (-1.0/rhoT(length(z))*((0.0-rhoT(length(z))))/(dz*1.0e3));
HH(length(z))=HH(length(z)-1);
HH(1)=HH(2);

if constT==1
   HH=rgas*temp/grv;
end;

dhdz(1:length(z))=0.0;
d2hdz2(1:length(z))=0.0;

for i=2:length(z)-1
 dhdz(i)=(HH(i+1)-HH(i-1))/(2*dz*1.0e3);
end;

for i=2:length(z)-1
 d2hdz2(i)=(HH(i+1)-2*HH(i)+HH(i-1))/(dz*1.0e3)^2;
end;


% calculate kinetic viscosity and thermal diffusivity

vscK=vscDyn./(rhoT*1e-3);
diff=cnduct./(rhoT*1e-3)/cp0;

%write hh and y to disk, in mltlay.f90, zdl is caculated by dx*hh

for i=1:size(z,2)
    b(i,1)=z(i)*1.0e5; % in cm
    b(i,2)=temp(i);       % in K
    b(i,3)=rhoT(i)*1e-3;     % in g/cm^3
    b(i,4)=Nref(i)/1e6;      % in #/cm^3
    b(i,5)=P(i)*10.0;        % in microbar=dyne/cm^2
    b(i,6)=HH(i)*1.0e2;      % in cm
    b(i,7)=dhdz(i);
    b(i,8)=d2hdz2(i)*1e-2;   % in cm^{-1}
end;


%write viscosity and diffusivity to disk

dvscdz(1:length(z))=0.0;

dvscdz(2:length(z)-1)=(vscK(3:length(z))-vscK(1:length(z)-2))/(2.0*dz*1.0e5);

for i=1:size(vscK,2)
    c(i,1)=b(i,1);
    c(i,2)=vscK(i);  % in cgs cm^2s^{-1} 
    c(i,3)=diff(i);  % in cgs cm^2s^{-1}
    c(i,4)=dvscdz(i); % vertical gradient of kinetic viscosity 
end;

% calculate the gradiant and curvature of cp/cp0

ii=0.0+1.0i;

%cp(1:length(z))=cp0; cp=cp';
%cp=(cp0+(freq(:,1)/omg)*cp0) + ii*((freq(:,2)/omg)*cp0);

% calculate Brunt frequency

dTdz(1:length(z))=0.0;
d2Tdz2(1:length(z))=0.0;
dTdz(2:length(z)-1)=(temp(3:length(z))-temp(1:length(z)-2))/(2.0*dz*1.0e5);
d2Tdz2(2:length(z)-1)=(temp(3:length(z))-2*temp(2:length(z)-1)+temp(1:length(z)-2))/(dz*1.0e5)^2;

%N2=grv*100.0*(1.0./temp.*dTdz + grv*100.0./(cp'.*temp));

dTdz=dTdz';
d2Tdz2=d2Tdz2';

save('dTdz.dat','dTdz','-ASCII');
save('d2Tdz2.dat','d2Tdz2','-ASCII');
save('vdcoeff.dat','c','-ASCII');
save('asimeant.dat','b', '-ASCII');

%fid = fopen('N2.dat', 'wt');
%    for i = 1:length(z)
%        fprintf(fid,'(%e, %e)  ',real(N2(i)),imag(N2(i)));
%        fprintf(fid,'\n');
%    end
%fclose(fid);

figure;subplot(3,1,1);
plot(temp,z,'+r')
hold on;
plot(asimean(:,2),asimean(:,1));
title('T [K] VS. Z [km]');

subplot(3,1,2);
plot(rhoT*1e-3,z,'+r')
hold on;
plot(asimean(:,9),asimean(:,1));
set(gca,'xscale','log');
title('\rho [g/cm^3] VS. Z [km]');

subplot(3,1,3);
plot(P,z,'r');
set(gca,'xscale','log');
title('P [pascal] VS. Z [km]');
hold on;
plot(asimean(:,8)/10,asimean(:,1));

figure;
subplot(2,1,1);
plot(Nref/1.0e6,z,'r');
set(gca,'xscale','log');
title('Number density [#/cm^3] VS. Z [km]');

subplot(2,1,2);
plot(HH/1e3,z,'r');
title('Density scale height [km] VS. Z [km]');
