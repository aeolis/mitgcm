%plot all modes B1, B2, B3, B4, (B5)

%20layers as standard

clear all;

totlev=20;
deg2rad=pi/180;


xC=-179:2:180;yC=-89:2:90;
sizey=size(yC);
Maxi=sizey(2);


thetaEq(1:Maxi,1:totlev)=0.0;

path('/home/lian/matlab/uranus',path);
load '/home/lian/matlab/uranus/TPprofile';

rF=TPprofile(:,6);

%B1, B2, B3, B4, B5 case

for k=1:totlev
 for i=1:Maxi
    B1(i)=1.0-(cos(yC(i)*deg2rad))^2.0;
    B2(i)=(cos(yC(i)*deg2rad))^2.0;
    B3(i)=(cos(3*yC(i)*deg2rad))^2.0*cos(yC(i)*deg2rad);
    B4(i)=(cos(8*yC(i)*deg2rad))^2.0*cos(yC(i)*deg2rad);
    
    if yC(i) < -30
       B5(i)=cos(1.5*yC(i)*deg2rad+7/4*pi)^2;
    elseif yC(i) > 30
       B5(i)=cos(1.5*yC(i)*deg2rad+1/4*pi)^2;
    else
       B5(i)=cos(3*yC(i)*deg2rad)^2;
    end;
  
  if ((k >= totlev-15)&&(k < totlev-5))

    thetaEq1(i,k)=(log10(rF(k))-log10(1.0E6))/ ...
             (log10(1.0e5)-log10(1.0e6))* ...
             B1(i);  
    thetaEq2(i,k)=(log10(rF(k))-log10(1.0E6))/ ...
             (log10(1.0e5)-log10(1.0e6))* ...
             B2(i);
    thetaEq3(i,k)=(log10(rF(k))-log10(1.0E6))/ ...
             (log10(1.0e5)-log10(1.0e6))* ...
             B3(i);
    thetaEq4(i,k)=(log10(rF(k))-log10(1.0E6))/ ...
             (log10(1.0e5)-log10(1.0e6))* ...
             B4(i);
    thetaEq5(i,k)=(log10(rF(k))-log10(1.0E6))/ ...
             (log10(1.0e5)-log10(1.0e6))* ...
             B5(i);

   elseif (k < totlev-15)
    
    thetaEq1(i,k)=0;
    thetaEq2(i,k)=0;
    thetaEq3(i,k)=0;
    thetaEq4(i,k)=0;
    thetaEq5(i,k)=0;
   else
    thetaEq1(i,k)=B1(i);
    thetaEq2(i,k)=B2(i);
    thetaEq3(i,k)=B3(i);
    thetaEq4(i,k)=B4(i);
    thetaEq5(i,k)=B5(i);
   
    end;
 end;
end; 

figure;
set(gca,'FontSize',12);

subplot(2,2,1);
set(gca,'FontSize',12);
plot(yC,B1);
xlabel('Latitude');
set(gca,'Tickdir','out');
h=title('B_1');
set(h,'FontSize',12);

subplot(2,2,2);
set(gca,'FontSize',12);
%[C,h]=contour(yC,rF/1e5,thetaEq1');
contour(yC,rF/1e5,thetaEq1');
set(gca,'YDir','reverse');
set(gca,'YScale','log');
set(gca,'Tickdir','out');
xlabel('Latitude');
ylabel('Pressure (bars)');
title('A(p)B_1');
%clabel(C,h,'labelspacing',72,'FontSize',10);
colorbar;

subplot(2,2,3);
set(gca,'FontSize',12);
plot(yC,B2);
xlabel('Latitude');
h=title('B_2');
set(h,'FontSize',12);
set(gca,'Tickdir','out');

subplot(2,2,4);
set(gca,'FontSize',12);
%[C,h]=contour(yC,rF/1e5,thetaEq2');
contour(yC,rF/1e5,thetaEq2');
set(gca,'YDir','reverse');
set(gca,'YScale','log');
set(gca,'Tickdir','out');
xlabel('Latitude');
ylabel('Pressure (bars)');
title('A(p)B_2');
%clabel(C,h,'labelspacing',72,'FontSize',10);
colorbar;

figure;
set(gca,'FontSize',12);

subplot(2,2,1);
set(gca,'FontSize',12);
plot(yC,B3);
xlabel('Latitude');
h=title('B_3');
set(h,'FontSize',12);
set(gca,'Tickdir','out');

subplot(2,2,2);
set(gca,'FontSize',12);
%[C,h]=contour(yC,rF/1e5,thetaEq3');
contour(yC,rF/1e5,thetaEq3');
set(gca,'YDir','reverse');
set(gca,'YScale','log');
set(gca,'Tickdir','out');
xlabel('Latitude');
ylabel('Pressure (bars)');
title('A(p)B_3');
%clabel(C,h,'labelspacing',72,'FontSize',10);
colorbar;

subplot(2,2,3);
set(gca,'FontSize',12);
plot(yC,B4);
xlabel('Latitude');
h=title('B_4');
set(h,'FontSize',12);
set(gca,'Tickdir','out');

subplot(2,2,4);
set(gca,'FontSize',12);
%[C,h]=contour(yC,rF/1e5,thetaEq4');
contour(yC,rF/1e5,thetaEq4');
set(gca,'YDir','reverse');
set(gca,'YScale','log');
set(gca,'Tickdir','out');
xlabel('Latitude');
ylabel('Pressure (bars)');
title('A(p)B_4');
%clabel(C,h,'labelspacing',72,'FontSize',10);
colorbar;


figure;
set(gca,'FontSize',12);

subplot(2,2,1);
set(gca,'FontSize',12);
plot(yC,B5);
xlabel('Latitude');
h=title('B_5');
set(h,'FontSize',12);
set(gca,'Tickdir','out');

subplot(2,2,2);
set(gca,'FontSize',12);
%[C,h]=contour(yC,rF/1e5,thetaEq5');
contour(yC,rF/1e5,thetaEq5');
set(gca,'YDir','reverse');
set(gca,'YScale','log');
set(gca,'Tickdir','out');
xlabel('Latitude');
ylabel('Pressure (bars)');
title('A(p)B_5');
%clabel(C,h,'labelspacing',72,'FontSize',10);
colorbar;
