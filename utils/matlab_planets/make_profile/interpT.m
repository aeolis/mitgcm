%calculate rho and T from galileo measurement

clear all;

rkb = 1.3807E-23;
amu = 1.673E-24;
mass = 2.16;
grv = 2260.0;
mol=6.022e23;
%rgas = rkb*mol/mass;
rgas = rkb/amu/mass;
rho0=1.e-6;

dz=1.0; % in km

load asimean.dat;

% extrapolate the temperature profile from 0--1200km
% z is the altitude, temp is the temperature, rho is density, Nref is number density
z=300:dz:1100-1;
temp = interp1(asimean(:,1),asimean(:,2),z,'linear','extrap');


for i=1:1000
zz(i)=100+(i-1);
%TT(i)=((exp((zz(i)-99)/200-2.5)-exp(-(zz(i)-99)/200+2.5))/(exp((zz(i)-99)/200-2.5)+exp(-(zz(i)-99)/200+2.5))+1)*750/2+150;
TT(i)=((exp((zz(i)-200)/200-1.5)-exp(-(zz(i)-200)/200+1.5))/(exp((zz(i)-200)/200-1.5)+exp(-(zz(i)-200)/200+1.5))+1)*780/2+120;
end;


% calculate Pressure from z and T

dzT=zeros(1,length(zz));

dzT(1)=dz/TT(1);

% convert dz from km to cm ---> dz*1.0e5
for i=2:length(zz)
    dzT(i)=dzT(i-1)+dz*1.0e3/TT(i);
end;

% calculate the pressure P=P(z), reference P0 at temp(1) is

P=zeros(1,length(zz));

P0= 7.308893e3*0.1; % in pascal

for i=1:length(zz)
    P(i)=P0*exp(-grv/rgas*dzT(i));
end;

% calculate the density rhoT and number density Nref

rhoT=P./(rgas*TT);

Nref=P./(rkb*TT);


%calculate the corresponding hh
hh = rgas*temp(1:size(z,2))/grv;
%write hh and y to disk, in mltlay.f90, zdl is caculated by dx*hh

for i=1:size(zz,2)
    b(i,1)=zz(i)*1.0e5; % in cm
    b(i,2)=TT(i);       % in K
    b(i,3)=rhoT(i)/1000.0;     % in g/cm^3
    b(i,4)=Nref(i)/1.0e6;      % in #/cm^3
    b(i,5)=P(i)*10.0;        % in microbar=dyne/cm^2
end;


save('asimeant.dat','b', '-ASCII');

figure; subplot(2,1,1);
plot(temp,z,'+r')
hold on;
plot(asimean(:,2),asimean(:,1));
subplot(2,1,2);
plot(asimean(:,9),asimean(:,1));
set(gca,'xscale','log');

figure;subplot(3,1,1);
plot(TT,zz,'+r')
hold on;
plot(asimean(:,2),asimean(:,1));
title('T [K] VS. Z [km]');

subplot(3,1,2);
plot(rhoT,zz,'+r')
hold on;
plot(asimean(:,9)*1000.0,asimean(:,1));
set(gca,'xscale','log');
title('\rho [kg/m^3] VS. Z [km]');

subplot(3,1,3);
plot(P,zz,'r');
set(gca,'xscale','log');
title('P [pascal] VS. Z [km]');

figure;
plot(Nref/1.0e6,zz,'r');
set(gca,'xscale','log');
title('Number density [#/cm^3] VS. Z [km]');
