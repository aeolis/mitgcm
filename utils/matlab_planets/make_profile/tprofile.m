clear all
p0=1e5;
T0=300;
cp=1004;
N=0.01;
g=9.8;
c=p0^(2/7)*(1/T0-cp*N^2/g^2);
layers=input('vertical layers:');
dp=p0/layers;
for i=1:layers
 p(i)=p0-(i-1)*dp;
 T(i)=1/(cp*N^2/g^2+c/p(i)^(2/7));
 theta(i)=T(i)*(p0/p(i))^(2/7);
end
fprintf('pressure p and potential temperature theta')
