to use the diagnostics package

1. start matlab in "workdir" or "$matlab"
2. run >> setpath
3. run >> read_diag_files (or similarly run read_nodiag_files, read_diag_files_new 
   depending on the available data type).
4. run diagnotics tools. These tools are all functions.
